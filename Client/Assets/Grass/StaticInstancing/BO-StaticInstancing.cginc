﻿#define MAX_INSTANCES 32

#define DECLARE_STATIC_INSTANCING_PROPERTIES \
        float4x4 _Object2WorldDeltas[MAX_INSTANCES]; \
        float4 _Colors[MAX_INSTANCES];


#define READ_PER_INSTANCE_PROPERTIES \
        v.vertex = mul(_Object2WorldDeltas[v.texcoord.z], v.vertex); \
        o.color = _Colors[v.texcoord.z];
