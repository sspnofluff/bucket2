
Shader "Grass" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_WindMask ("Wind Mask (RGB)", 2D) = "white" {}
		_Color ("Color", Color) = (1, 1, 1, 1)

		_Color2 ("Grass tint", Color) = (0.5,0.8,0.5,0)
        _ColorGlobal ("Global tint", Color) = (0.5,0.5,0.5,0) 

        _Brightness ("Brightness", Float) = 1.0
        _Saturation ("Saturation", Float) = 1.0

        _TintPower("Tint power", Float) = 0
        _TintFrequency("Tint frequency", Float) = 0.1

        _Coeff("Coeff", Range (0.0, 1.0)) = 1.0
        _Coeff2("Coeff2", Range (0.0, 1.0)) = 1.0

		_Cutoff ("Cutoff", Range(0,1)) = 0.5
	}
	SubShader {
		Tags
		{
			"Queue"="Geometry-152"
			"RenderType"="Opaque"
		}
		LOD 200

		Cull Off

		CGPROGRAM

		#pragma surface surf Grass vertex:vert alphatest:_Cutoff addshadow halfasview interpolateview nolightmap noforwardadd
		#pragma multi_compile STATIC_INSTANCING_ON __
		#pragma multi_compile ENABLE_WIND_ZONES __

    #if STATIC_INSTANCING_ON
		#undef INSTANCING_ON
	#endif

		#include "Assets/VacuumShaders/Curved World/Shaders/cginc/CurvedWorld_Base.cginc"
		#include "StaticInstancing/BO-StaticInstancing.cginc"

		#pragma target 3.0
		#pragma shader_feature TINT_ON
		sampler2D _MainTex;
		sampler2D _WindMask;

		//uniform float4 _InteractPosition[10];
		//uniform int _InteractCount;
		uniform float4 _InteractPosition;

		float _TintPower; 
           	float _TintFrequency;
	
		float3 vPos;
		float3 _InteractPos;
		float3 _Color2;
       	float _Coeff;
       	float _Coeff2;
       	float3 _ColorGlobal;
       	float _Brightness;
       	float _Saturation;

		UNITY_INSTANCING_CBUFFER_START(Props)
		UNITY_DEFINE_INSTANCED_PROP(float4, _Color)
		UNITY_INSTANCING_CBUFFER_END

    #if STATIC_INSTANCING_ON
        DECLARE_STATIC_INSTANCING_PROPERTIES
    #endif
		struct Input {
			float2 uv_MainTex;
			float4 color;
			float3 worldPos;
		};

        #include "Wind/BO-Wind.cginc"


        void vert(inout appdata_full v, out Input o)
        {
            UNITY_INITIALIZE_OUTPUT(Input, o);
            V_CW_TransformPoint(v.vertex);

        #if !STATIC_INSTANCING_ON
            o.color = v.color * UNITY_ACCESS_INSTANCED_PROP(_Color);
        #else
            READ_PER_INSTANCE_PROPERTIES
        #endif

            float windValue = tex2Dlod(_WindMask, half4(v.texcoord.xy, 0, 0)).r;

            float4 worldVertex = mul(unity_ObjectToWorld, v.vertex);

            float3 wind = SAMPLE_WIND_VALUE_LEAVES(Grass, worldVertex);
            wind = wind.z * _WindDirectionForward + wind.x * _WindDirectionRight + float3(0, 1, 0) * wind.y;
            worldVertex.xyz += wind * saturate(v.texcoord.y-0.2f);

           
			//float3 SpeedFac = float3(0,0,0);  //	SpeedFac =  _InteractSpeed;  

			//for (int i =0; i<_InteractCount; i++)
			//{

            float dist =  distance(_InteractPosition.xyz,worldVertex.xyz); 
                 
            if( dist < _InteractPosition.w)
            { 
            	float distA =  dist / (_InteractPosition.w); 
             		if( v.texcoord.y > 0.1)
                	{
                		worldVertex.x += clamp(worldVertex.x - _InteractPosition.x,-0.3,0.3)/distA;
                		worldVertex.z += clamp(worldVertex.z - _InteractPosition.z,-0.3,0.3)/distA;
						worldVertex.y = worldVertex.y - (1-distA)*1*(v.texcoord.y-0.5)*sin(worldVertex.z+_Time.y) ;
					}             
 	        	}
           // }

            vPos = mul(unity_WorldToObject, worldVertex);


            v.vertex.xyz = vPos;

		}
		
		half4 LightingGrass(SurfaceOutput s, half3 lightDir, half atten) 
		{
			half NdotL = lightDir.y;
			half4 c;
			c.rgb = s.Albedo * _LightColor0.rgb * (NdotL * atten);
			c.a = s.Alpha;

			return c;

		}

		void surf (Input IN, inout SurfaceOutput o) {
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb * IN.color.rgb;

			float uvOffset = (IN.uv_MainTex.y);
            float3 finalColor =  c.r * 2 * _Coeff * IN.color.xyz;

            half colDesat=dot(finalColor,0.33333);
            finalColor=lerp(colDesat.xxx, finalColor, _Saturation);
			finalColor*= _Brightness;

            finalColor = lerp(finalColor, o.Albedo, _Coeff2*uvOffset);
        #if TINT_ON
            finalColor = lerp(finalColor, saturate(finalColor) *_Color2,_TintPower*uvOffset*(0.9+0.6*cos(IN.worldPos.x*2*_TintFrequency)+0.6*sin(IN.worldPos.z*3*_TintFrequency)+0.6*sin(IN.worldPos.z*1*_TintFrequency+0.1)));
        #endif
            o.Albedo = finalColor; 
            o.Alpha = c.a;


		}


		ENDCG
	} 
	FallBack "Diffuse"
	CustomEditor "GrassShaderEditor"
}
