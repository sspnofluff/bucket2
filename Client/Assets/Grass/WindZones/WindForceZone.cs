﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class WindForceZone : MonoBehaviour
{
	[SerializeField]
	public NormalizedAnimationCurve _intensityCurve = new NormalizedAnimationCurve();

	public float _birthTime;

	public Mesh _mesh;

	public Material _material;

	void Start()
	{
		_mesh = GetComponent<MeshFilter>().sharedMesh;
	}

	void OnEnable()
	{
		_birthTime = Time.time;

		WindForceZonesRenderer.AddZone(this);
	}

	void OnDisable()
	{
		WindForceZonesRenderer.RemoveZone(this);

		DestroyImmediate(_material);
	}

	public void Render(Material material)
	{
		if (_material == null)
		{
			_material = Instantiate(material);
			_material.CopyPropertiesFromMaterial(material);
		}

		//transform.GetComponent<MeshRenderer> ().sharedMaterial = _material;
		_material.SetFloat ("_Intensity", _intensityCurve.Evaluate(Time.time - _birthTime));
		_material.SetVector("_Origin", transform.position);

		if (_material.SetPass(0))
		{
			Graphics.DrawMeshNow(_mesh, transform.localToWorldMatrix);
		}
	}
}
