﻿using System;
using UnityEngine;
using System.Collections.Generic;
using JetBrains.Annotations;
using Object = UnityEngine.Object;

[Serializable]
public class WindForceZonesRenderer
{
	public static List<WindForceZone> _zones = new List<WindForceZone>();

	public RenderTexture _windZonesTexture;
	public Material _material;

	public static void AddZone(WindForceZone zone)
	{
		_zones.Add(zone);
		Debug.Log ("Add");
	}

	public static void RemoveZone(WindForceZone zone)
	{
		_zones.Remove(zone);
		Debug.Log ("Remove");
	}

	public void OnEnable()
	{
		_windZonesTexture = new RenderTexture(Mathf.RoundToInt(256), Mathf.RoundToInt(256), 0, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Linear);

		_material = new Material(Shader.Find("Hidden/WindForceZone"));
		_material.SetVector("_RenderTargetSize", new Vector2(_windZonesTexture.width, _windZonesTexture.height));

		Shader.EnableKeyword("ENABLE_WIND_ZONES");

	}

	public void OnDisable()
	{
		Object.DestroyImmediate(_windZonesTexture);
		Object.DestroyImmediate(_material);

		Shader.DisableKeyword("ENABLE_WIND_ZONES");
	}

	public void OnRenderObject()
	{
		var activeRenderTarget = RenderTexture.active;

		Graphics.SetRenderTarget(_windZonesTexture);
		GL.Clear(true, true, Color.gray);

		foreach (var each in _zones)
		{
			each.Render(_material);
		}

		RenderTexture.active = activeRenderTarget;

		Shader.SetGlobalTexture("_WindZonesTexture", _windZonesTexture);
	}
}