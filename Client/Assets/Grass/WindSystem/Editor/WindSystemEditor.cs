﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(WindSystem))]
public class WindSystemEditor : Editor
{
	private SerializedProperty _windInfo;

	private Editor _windInfoEditor;

	void OnEnable()
	{
		_windInfo = serializedObject.FindProperty("_windInfo");

		_windInfoEditor = CreateEditor(_windInfo.objectReferenceValue);
	}

	void OnDisable()
	{
		_windInfo.Dispose();
	}

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		serializedObject.Update();

		if (_windInfoEditor != null)
		{
			if (_windInfoEditor.target != _windInfo.objectReferenceValue)
			{
				_windInfoEditor = CreateEditor(_windInfo.objectReferenceValue);
			}
		}

		if (_windInfoEditor != null)
		{
			EditorGUILayout.Space();

			EditorGUI.BeginChangeCheck();

			_windInfoEditor.OnInspectorGUI();
			if (EditorGUI.EndChangeCheck())
			{
				(target as WindSystem).OnValidate();
			}
		}

		serializedObject.ApplyModifiedProperties();
	}
}