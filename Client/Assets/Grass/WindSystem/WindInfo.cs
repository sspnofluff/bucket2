﻿using System;
using UnityEngine;

[CreateAssetMenu(menuName = "Visuals/Wind Info")]
public class WindInfo : ScriptableObject {

	[Serializable]
	public class WindSettings
	{
		public Vector2 LargeDetailScale;
		public Vector3 LargeCurveAxisWeights;

		public Vector2 SmallDetailScale;
		public Vector3 SmallCurveAxisWeights;
	}

	public Texture2D LargeDetail;
	public Texture2D SmallDetail;

	[Space]

	public WindSettings TreeWindSettings;
	public float TreeTrunkSwayOffset;

	[Space]
	public WindSettings GrassWindSettings;
}
