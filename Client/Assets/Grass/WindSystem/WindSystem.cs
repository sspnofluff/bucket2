﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class WindSystem : MonoBehaviour
{
	[Range(0, 360)]
	[SerializeField] private float _angle;

	[SerializeField]
	private WindInfo _windInfo;

	[SerializeField]
	private WindForceZonesRenderer _windForceZonesRenderer;

	void OnEnable()
	{
		_windForceZonesRenderer = new WindForceZonesRenderer();

		_windForceZonesRenderer.OnEnable();
	}

	void OnDisable()
	{
		_windForceZonesRenderer.OnDisable();
	}

	void OnRenderObject()
	{
		_windForceZonesRenderer.OnRenderObject();
	}

	public void OnValidate()
	{
		if (_windInfo == null)
		{
			return;
		}

		Shader.SetGlobalTexture("_WindDetailLarge", _windInfo.LargeDetail);
		Shader.SetGlobalTexture("_WindDetailSmall", _windInfo.SmallDetail);

		var right = Vector3.right;
		var forward = Vector3.forward;

		GetRotatedUVBasis(_angle, out right, out forward);

		Shader.SetGlobalVector("_WindDirectionForward", forward);
		Shader.SetGlobalVector("_WindDirectionRight", right);

		Shader.SetGlobalFloat("_TreeTrunkSwayOffset", _windInfo.TreeTrunkSwayOffset);

		SetupWindSettings("Tree", _windInfo.TreeWindSettings);
		SetupWindSettings("Grass", _windInfo.GrassWindSettings);
	}

	public void LoadDefaultSettings()
	{
		_windInfo = Resources.Load<WindInfo>("Weather System/WindInfo/CalmWind");

		OnValidate();
	}

	private void GetRotatedUVBasis(float angle, out Vector3 right, out Vector3 forward)
	{
		var rotator = Quaternion.AngleAxis(angle, Vector3.up);

		right = rotator * Vector3.right;
		forward = rotator * Vector3.forward;
	}

	private void SetupWindSettings(string name, WindInfo.WindSettings windSettings)
	{
		Shader.SetGlobalVector("_" + name + "LargeWindCurveAxisWeights", windSettings.LargeCurveAxisWeights);
		Shader.SetGlobalVector("_" + name + "SmallWindCurveAxisWeights", windSettings.SmallCurveAxisWeights);

		Shader.SetGlobalVector("_" + name + "WindScaleLarge", windSettings.LargeDetailScale);
		Shader.SetGlobalVector("_" + name + "WindScaleSmall", windSettings.SmallDetailScale);
	}
}