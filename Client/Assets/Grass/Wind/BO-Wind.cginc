﻿
#define DECLARE_WIND_SETTINGS(name) \
    uniform float2 _##name##WindScaleLarge; \
    uniform float2 _##name##WindScaleSmall; \
    uniform float3 _##name##LargeWindCurveAxisWeights; \
    uniform float3 _##name##SmallWindCurveAxisWeights;

uniform float3 _WindDirectionForward;
uniform float3 _WindDirectionRight;

uniform sampler2D _WindDetailLarge;
uniform sampler2D _WindDetailSmall;

uniform float _TreeTrunkSwayOffset;

DECLARE_WIND_SETTINGS(Tree)
DECLARE_WIND_SETTINGS(Grass)

uniform float3 _TerrainSize;

#if ENABLE_WIND_ZONES
uniform sampler2D _WindZonesTexture;
#endif

float3 SampleWindValueLeaves(float3 worldPos, float2 windScaleLarge, float2 windScaleSmall, float3 largeWindCurveAxisWeights, float3 smallWindCurveAxisWeights)
{
    float3 largeDetail = tex2Dlod(_WindDetailLarge, float4((_Time.yy + worldPos.xz) / windScaleLarge, 0, 0)).xyz * 2 - 1;
    float3 smallDetail = tex2Dlod(_WindDetailSmall, float4((_Time.yy + worldPos.xz) / windScaleSmall, 0, 0)).xyz * 2 - 1;

    return (largeDetail * largeWindCurveAxisWeights + smallDetail * smallWindCurveAxisWeights);
}

#define SAMPLE_WIND_VALUE_LEAVES(name, worldPos) \
    SampleWindValueLeaves(worldPos, _##name##WindScaleLarge, _##name##WindScaleSmall, _##name##LargeWindCurveAxisWeights, _##name##SmallWindCurveAxisWeights)

float3 SampleWindValueTrunk(float3 worldPos, float2 windScaleLarge, float3 largeWindCurveAxisWeights)
{
    float3 largeDetail = tex2Dlod(_WindDetailLarge, float4((_Time.yy + worldPos.xz) / windScaleLarge, 0, 5)).xyz * 2 - 1;

    return (largeDetail * largeWindCurveAxisWeights) * (1 + _TreeTrunkSwayOffset);
}

#define SAMPLE_WIND_VALUE_TRUNK(name, worldPos) \
    SampleWindValueTrunk(worldPos, _##name##WindScaleLarge, _##name##LargeWindCurveAxisWeights)

float3 SampleWindZoneValue(float3 worldPos)
{
float2 ts;
ts.x = 256;
ts.y = 256;
#if ENABLE_WIND_ZONES
    float4 windZoneCoord = float4(worldPos.xz / _TerrainSize.xy, 0, 0);
    float3 windZoneValue = tex2Dlod(_WindZonesTexture, windZoneCoord).xyz * 2 - 1;

    return windZoneValue;
#else
    return 0;
#endif
}