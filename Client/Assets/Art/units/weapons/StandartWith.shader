﻿ Shader "Custom/StandardVertex" {
     Properties {
         _Color ("Color", Color) = (1,1,1,1)
         _MainTex ("Albedo (RGB)", 2D) = "white" {}
		 _NormTex ("Normal (RGB)", 2D) = "white" {}
		 _DecalTex ("Decal (RGB)", 2D) = "gray" {}
		 _Normal ("Normal", Range(-5,5)) = 0.5
         _Glossiness ("Smoothness", Range(0,1)) = 0.5
         _Metallic ("Metallic", Range(0,1)) = 0.0
		 _Cutoff("Cutoff", Range(0,1)) = 0.5
		 _Tess ("Tessellation", Range(1,32)) = 4
		  _Displacement ("Displacement", Range(0, 1.0)) = 0.3
     }
     SubShader {
         Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="TransparentCutout" }
        LOD 300
		Cull Off
         
         CGPROGRAM
// Upgrade NOTE: excluded shader from DX11 and Xbox360; has structs without semantics (struct appdata members texcoord)
#pragma exclude_renderers d3d11 xbox360
         #pragma surface surf Standard  fullforwardshadows alphatest:_Cutoff vertex:disp tessellate:tessDistance  
         #pragma target 5.0
         #include "Tessellation.cginc"
         
		 struct appdata {
                float4 vertex : POSITION;
                float4 tangent : TANGENT;
                float3 normal : NORMAL;
                float2 texcoord;
            };

		float _Tess;

            float4 tessDistance (appdata v0, appdata v1, appdata v2) {
                float minDist = 10.0;
                float maxDist = 25.0;
                return UnityDistanceBasedTess(v0.vertex, v1.vertex, v2.vertex, minDist, maxDist, _Tess);
            }

 
         sampler2D _MainTex;
		 sampler2D _NormTex;
		 sampler2D _DecalTex;
 
         half _Glossiness;
         half _Metallic;
         fixed4 _Color;
		 float _Normal;
		 float _Displacement;

		

		void disp (inout appdata v)
            {
                float d = tex2Dlod(_DecalTex, float4(v.texcoord,0,0)).r * _Displacement;
                v.vertex.xyz += v.normal * d;
            }

		struct Input
		  {
             float2 uv_MainTex;
			 float2 uv2_DecalTex;

         };

		

         void surf (Input IN, inout SurfaceOutputStandard o) 
         {
             fixed3 d = tex2D (_DecalTex, IN.uv2_DecalTex).rgb;

             fixed3 c = tex2D (_MainTex, IN.uv_MainTex).rgb * _Color * d.r;
             o.Albedo = c.rgb;
			 
			 o.Normal =  UnpackScaleNormal(tex2D (_NormTex, IN.uv_MainTex), (1.0f-d.r) * _Normal);
			 o.Alpha = d.r * _Color.a;
             // Metallic and smoothness come from slider variables
             o.Metallic = _Metallic;
             o.Smoothness = _Glossiness;
             
         }
         ENDCG
     } 
     FallBack "Transparent/Cutout/Diffuse"
 }