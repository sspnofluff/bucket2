public class ConversationOption  {
    public  int     id;                     
    public  string  optionText = "";
    public  string  postEvent = "";

    public ConversationOption(int id, string optionText, string postEvent) {
        this.id = id;
        this.optionText = optionText;
        this.postEvent = postEvent;
    }
}
