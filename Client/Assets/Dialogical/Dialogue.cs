using UnityEngine;
using Dialogical;
using System.Collections;
using System.Collections.Generic;
using System;

/*
Your methods for events should all have a signature like below;
If your checks pass you don't have to set anything.
If your checks fail you have to set static field Dialogue.lastCheckPassed to false.
Take note that, in ChoiceNodes, the first choice that is Evaluated to passed (nothing set) will be picked.
We do things this way because SendMessage can not return values.
Also, your postEvents don't have to set anything, while their signature is the same.

void Node1Option1Condition() {
    // Your conditions setup and checking.
    
    if (! myConditionsPassed) Dialogue.lastCheckPassed = false;
}
*/

public class Dialogue : MonoBehaviour {
    // Settings    
    /// <summary>
    /// Should we play audio clips, if they are attached.
    /// </summary>
	/// 

    public bool         doPlayAudio     = true;
    private SendMessageOptions requireReceiver = SendMessageOptions.DontRequireReceiver;         

    // Interface    
    /// <summary>
    /// Signal if the callback from your method returned true of false. Consult documentation for more.
    /// </summary>
    public static bool  lastCheckPassed = true;

    /// <summary>
    /// Set this to a string array in an event that fires imediately before using the text. 
    /// </summary>
    public static string[] substituteTextParams;

    /// <summary>
    /// Delay to add before playing every node's audio. Useful if you have an animation that brings up next node.
    /// Node-specific delay will be added to this.
    /// </summary>
    public static float normalPlayDelay = 0f;
    /// <summary>
    /// Index of the language for display. Zero-starting index displayed in Manage Languages dialog.
    /// </summary>
    public static int currentLanguageIndex;

    private bool    _convcersationIsOver;
    /// <summary>
    /// Gets a value indicating whether [conversation is over].
    /// </summary>
    /// <value>
    ///   <c>true</c> if [conversation is over]; otherwise, <c>false</c>.
    /// </value>
    public bool conversationIsOver {
        get { return _convcersationIsOver;  }
        private set {
            _convcersationIsOver = value;
            if(_convcersationIsOver == true) {
                if(ConversationOverStatic != null) ConversationOverStatic();
                if(ConversationOver != null) ConversationOver();
            }
        }
    }

    public delegate void DialogueDelegate();

    /// <summary>
    /// Methods called when any conversation is over.
    /// </summary>
    public static DialogueDelegate ConversationOverStatic;
    /// <summary>
    /// Methods called when this conversation is over.
    /// </summary>
    public DialogueDelegate ConversationOver;

    /// <summary>
    /// Methods called when any conversation is started.
    /// </summary>
    public static DialogueDelegate ConversationStartedStatic;
    /// <summary>
    /// Methods called when this conversation is started.
    /// </summary>
    public DialogueDelegate ConversationStarted;

    /// <summary>
    /// Static method that is called when the node is ready to display - we have text and we can fetch options for drawing.
    /// </summary>
    public static DialogueDelegate NextConversationNodeStatic;
    /// <summary>
    /// Method that is called when the node is ready to display - we have text and we can fetch options for drawing.
    /// </summary>
    public DialogueDelegate NextConversationNode;

    /// <summary>
    /// Last started Dialogue
    /// </summary>
    public static Dialogue Instance;

    // Data    
    /// <summary>
    /// Attached Dialgue Tree created through Dialogical interface.
    /// </summary>
    public DialogueTree tree;

    private DialogueNode _currentNode;
    private DialogueNode currentNode {
        get { return _currentNode; }
        set { _currentNode = value;
            // First we activate the events. This allows to call SetAutoChoiceDelay inside them.
            if(!string.IsNullOrEmpty(_currentNode.globalEvent)) SendMessage(_currentNode.globalEvent, requireReceiver);
            if(!string.IsNullOrEmpty(_currentNode.localEvent)) SendMessage(_currentNode.localEvent, requireReceiver);

            if(substituteTextParams != null) {
                _nodeText = String.Format(_currentNode.Text[currentLanguageIndex], substituteTextParams);
            } else {
                _nodeText = _currentNode.Text[currentLanguageIndex];
            }
            substituteTextParams = null;

			leftPortrait = _currentNode.leftPortrait;
			rightPortrait = _currentNode.rightPortrait;

			leftName = _currentNode.leftName;
			rightName = _currentNode.rightName;

			isLeftSpeak = _currentNode.isleftPersonSpeak;

            _autoChoiceDelay = (_autoChoiceDelay != 0f) ? _autoChoiceDelay : _currentNode.autoChoiceDelay;
            if(_currentNode.autoChoiceIndex > 0 && _autoChoiceDelay > 0f) {
                int index = _currentNode.autoChoiceIndex-1;
                string text = "";
                string postEvent = "";
                DialogueNodeOption option = _currentNode.options[index] as DialogueNodeOption;
                if (option != null) {
                    text = option.Text[currentLanguageIndex];
                    postEvent = option.postEvent;
                } else {
                    throw new InvalidCastException("Something invalid found in DialogueNode's options array");
                }
                ConversationOption optionToCall = new ConversationOption(index, text, postEvent);
                _coroutine = AutoActivation(_autoChoiceDelay, optionToCall);
                StartCoroutine(_coroutine);
            }
            _autoChoiceDelay = 0f;

            if(NextConversationNode != null) NextConversationNode();
            if(NextConversationNodeStatic != null) NextConversationNodeStatic();
        }
    }
    private InstantiableNode _workingNode;
    private InstantiableNode workingNode {
        get { return _workingNode; }
        set {
            _workingNode = value;
            if(value == null) conversationIsOver = true;
            else EvaluateWorkingNode();
        }
    }

    private AudioSource _audio;
    private IEnumerator _coroutine;
    private string  _nodeText = "";
    private string  _optionText = "";
    private float   _autoChoiceDelay;
	private Sprite leftPortrait = null;
	private Sprite rightPortrait = null;
	private string leftName = "";
	private string rightName = "";
	private bool isLeftSpeak = true;

    void Start () {
        if(doPlayAudio) {
            _audio = gameObject.GetComponent<AudioSource>();
            if(_audio == null) _audio = gameObject.AddComponent<AudioSource>();
        } 
    }

    /// <summary>
    /// Call this when the dialogue should start (from the Start node).
    /// </summary>
    public void Activate() {
        if(tree == null) {
            Debug.Log("No conversation tree attached at:" + gameObject.transform.parent.name + " > " + gameObject.name);
            conversationIsOver = true;
            return;
        }

        conversationIsOver = false;
        Instance = this;
        if(ConversationStartedStatic != null)   ConversationStartedStatic();
        //if(ConversationStarted != null)         ConversationStarted();

        InstantiableNode firstNode = tree.nodes[0].options[0].target;
        if(firstNode == null) Debug.Log("Start node not connected to any other node.");
        workingNode = firstNode;
    }
    
    private void EvaluateWorkingNode() {
        DialogueChoiceNode choiceNode = workingNode as DialogueChoiceNode;
        if (choiceNode != null) {
            EvaluateChoiceNode(choiceNode);
        } else {
            DialogueNode dialogueNode = workingNode as DialogueNode;
            if (dialogueNode != null) {
                currentNode = dialogueNode;
            } else {
                throw new InvalidCastException("Something invalid found in tree.nodes array.");
            }
        }
    }

    private void EvaluateChoiceNode(DialogueChoiceNode choiceNode) {
        List<DialogueChoiceOption> conditionalNodes     = new List<DialogueChoiceOption>();
        List<DialogueChoiceOption> nonConditionalNodes  = new List<DialogueChoiceOption>();

        foreach(var option in choiceNode.options) {
            DialogueChoiceOption properOption = option as DialogueChoiceOption;
            if (properOption == null) throw new InvalidCastException("Something invalid found in ChoiceNode's options array");

            if (!string.IsNullOrEmpty(properOption.condition)) {
                conditionalNodes.Add(properOption);
            } else {
                nonConditionalNodes.Add(properOption);
            }
        }

        foreach(var option in conditionalNodes) {
            if (CheckChoiceOptionPrerequisite(option)) {
                workingNode = option.target;
                return;
            }
        }

        // If no Conditional Nodes satisfy
        if(nonConditionalNodes.Count > 0) {
            int randomIndex = UnityEngine.Random.Range(0, nonConditionalNodes.Count);
            workingNode = nonConditionalNodes[randomIndex].target;
            return;
        }

        workingNode = null;
    }

    IEnumerator AutoActivation(float time, ConversationOption optionToCall) {
        yield return new WaitForSeconds(time);
        CallOption(optionToCall);
    }

    /// <summary>
    /// Gets the node text.
    /// </summary>
    public string GetNodeText() {
        if(currentNode == null) Activate();

        return _nodeText;
    }

	public Sprite GetLeftPortrait() {
		if(currentNode == null) Activate();

		return leftPortrait;
	}

	public Sprite GetRightPortrait() {
		if(currentNode == null) Activate();

		return rightPortrait;
	}

	public string GetLeftName() {
		if(currentNode == null) Activate();

		return leftName;
	}

	public string GetRightName() {
		if(currentNode == null) Activate();

		return rightName;
	}

	public bool GetIsLeftSpeak() {
		if(currentNode == null) Activate();

		return isLeftSpeak;
	}

    /// <summary>
    /// Auxiliary method that returns current node options number.
    /// </summary>
    public int GetNodeOptionsNumber() {
        if(currentNode == null) Activate();

        return currentNode.options.Count;
    }

    /// <summary>
    /// Gets the node options.
    /// </summary>
    public List<ConversationOption> GetNodeOptions() {
        if(currentNode == null) Activate();
        var result = new List<ConversationOption>();
        
        for(int i = 0; i < currentNode.options.Count; i++) {
            DialogueNodeOption nodeOption = currentNode.options[i] as DialogueNodeOption;
            if(nodeOption == null) throw new InvalidCastException("Something invalid found in DialogueNode's options array");

            if(!CheckNodeOptionPrerequisite(nodeOption)) continue;

            if(substituteTextParams != null) {
                _optionText = String.Format(nodeOption.Text[currentLanguageIndex], substituteTextParams);
            } else {
                _optionText = nodeOption.Text[currentLanguageIndex];
            }
            substituteTextParams = null;

            result.Add(new ConversationOption(i, _optionText, nodeOption.postEvent));
        }

        return result;
    }

    /// <summary>
    /// Auxiliary method that returns a specific available ConversationOption.
    /// </summary>
    /// <param name="index">The index in GetNodeOptions.</param>
    public ConversationOption GetNodeOptionAt(int index) {
        return GetNodeOptions()[index];
    }

    /// <summary>
    /// Calls the option, going to next node.
    /// </summary>
    /// <param name="option">The option.</param>
    public void CallOption(ConversationOption option) {
        if(_coroutine != null) StopCoroutine(_coroutine);
        if(_audio.isPlaying) _audio.Stop();

        FireOptionPostEvent(option);

        workingNode = currentNode.options[option.id].target;
    }

    private bool CheckNodeOptionPrerequisite(DialogueNodeOption option) {
        if(string.IsNullOrEmpty(option.preCondition)) return true;

        lastCheckPassed = true;
        SendMessage(option.preCondition, requireReceiver);

        return lastCheckPassed;
    }

    private bool CheckChoiceOptionPrerequisite(DialogueChoiceOption option) {
        lastCheckPassed = true;
        SendMessage(option.condition, requireReceiver);

        return lastCheckPassed;
    }

    private void FireOptionPostEvent(ConversationOption option) {
        if(!string.IsNullOrEmpty(option.postEvent))  SendMessage(option.postEvent, requireReceiver);
    }

    /// <summary>
    /// Plays the audio of the current node. The delay is static <see cref="Dialogue.normalPlayDelay"/> + specific delay.
    /// </summary>
    public void PlayNodesAudio () {
        if (doPlayAudio && currentNode.Audio[currentLanguageIndex] != null) {
            _audio.clip = currentNode.Audio[currentLanguageIndex];
            _audio.PlayDelayed(normalPlayDelay + currentNode.AudioDelay[currentLanguageIndex]);
        }
    }

    /// <summary>
    /// Auxiliary method that returns the AudioClip attached to Current Node. You can automatically play the audio with PlayNodesAudio
    /// </summary>
    public AudioClip GetNodesAudioClip() {
        return currentNode.Audio[currentLanguageIndex];
    }

    void NotShown() {
        Dialogue.lastCheckPassed = false;
    }

    public void SetAutoChoiceDelay(float delay) {
        _autoChoiceDelay = delay;
    }

	public void MarkWaypoint(int number)
	{
		Debug.Log ("FF");
		Main.instance.globalMap.waypoints[number].isExplored = true;
		Main.instance.globalMap.ReCalculateWaypoints();
	}

	void ChangePortrait1() {
		Debug.Log ("FF");
		Main.instance.globalMap.waypoints [2].isExplored = true;
		Main.instance.globalMap.ReCalculateWaypoints ();

	}
}
