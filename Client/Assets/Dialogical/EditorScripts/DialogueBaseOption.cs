using UnityEngine;
using System;

namespace Dialogical {
    [Serializable]
    public class DialogueBaseOption : ScriptableObject {
        // Setup
        public  static readonly Vector2 initialBaseOptionSize   = new Vector2(18, 22);

        // Selection & Display
        public  static bool inConnectingMode;
        public  static DialogueBaseOption lastSelection;
        private static DialogueBaseOption _selectedOption;
        public static DialogueBaseOption selection {
            get { return _selectedOption; }
            set {
                _selectedOption = value;
                if(_selectedOption == null) inConnectingMode = false;
            }
        }

        // Data
        [SerializeField]
        public    Rect backgroundRect = new Rect(0, 0, initialBaseOptionSize.x, initialBaseOptionSize.y);
        [SerializeField]
        public InstantiableNode target;
        [SerializeField]
        protected   BaseNode        _parent;
        protected   NodeGraphics    _nodeOptionGraphics;
        public      bool            showRedMarker;
        public      Rect            redMarkerRect;

        protected int currentLanguageIndex;

        // Positioning
        [SerializeField]
        protected Rect _nodeConnectorRect = new Rect(0,0,0,0);

#if UNITY_EDITOR
        // CTors
        void OnEnable() {
            hideFlags = HideFlags.HideInHierarchy;
        }

        public virtual void Init(NodeGraphics nodeOptionGraphics, BaseNode parent) {
            _parent = parent;
            _nodeOptionGraphics = nodeOptionGraphics;
            _nodeConnectorRect.width = nodeOptionGraphics.nodeConnectedTexture.width;
            _nodeConnectorRect.height = nodeOptionGraphics.nodeConnectedTexture.height;

            for(int i = 0; i < nodeOptionGraphics.languages.GetLanguagesCount(); i++) CreateLanguage();
        }

        public virtual void UpdatePositioning(Vector2 startingPoint) {
            backgroundRect.x = startingPoint.x; backgroundRect.y = startingPoint.y;
            _nodeConnectorRect = backgroundRect;
        }

        public void SetNodeGraphics(NodeGraphics nodeOptionGraphics) {
            _nodeOptionGraphics = nodeOptionGraphics;
        }

        // Languages
        public virtual void CreateLanguage() { }

        public virtual void DeleteLanguageAt(int index) {
            if(currentLanguageIndex == index) {
                currentLanguageIndex = 0;
            } else if(currentLanguageIndex > index) {
                currentLanguageIndex--;
            }
        }
        public void SwitchToLanguage(int index) { currentLanguageIndex = index; }

        protected virtual void OnDestroy() {
            _parent = null;
            _nodeOptionGraphics = null;
            target = null;
        }


        private Rect _ongui_mousePosition = new Rect(0,0,0,0);
        public virtual void OnGUI() {
            GUI.DrawTexture(_nodeConnectorRect, (target != null) ? _nodeOptionGraphics.nodeConnectedTexture : _nodeOptionGraphics.nodeNotConnectedTexture);

            // Event Handling
            switch(Event.current.type) {

                case EventType.mouseDown:
                if(_nodeConnectorRect.Contains(Event.current.mousePosition)) {      // Work with connection mode
                    selection = this;

                    if(Event.current.button == 0) inConnectingMode = true;
                    Event.current.Use();
                }
                break;

                case EventType.mouseDrag:
                if(selection == this) {                                  // If doing a mouse drag with this component selected, and in connect mode...
                    Event.current.Use();                                 // ... just use the event as we'll be painting the new connection
                }
                break;

                case EventType.mouseUp:                                  // Prevent forwarding of Mouse Up event on the control itself.
                if(selection == this && _nodeConnectorRect.Contains(Event.current.mousePosition)) {
                    SetTargetOfSelection(null);
                    Event.current.Use();
                }
                break;

                case EventType.repaint:
                if(selection == this && inConnectingMode) {              // New connection originates from this node.
                    _ongui_mousePosition.x = Event.current.mousePosition.x;
                    _ongui_mousePosition.y = Event.current.mousePosition.y;
                    Helpers.DrawConnection(_nodeConnectorRect, _ongui_mousePosition, true, false);
                }
                break;
            }
        }

        protected void AddHeight(float addedHeight) {
            backgroundRect.height += addedHeight;
            _parent.AddHeight(addedHeight);
        }

        public void DrawNodeConnection(bool isChoiceNode) {
            if(target != null) Helpers.DrawConnection(_nodeConnectorRect, target.nodeRect, false, isChoiceNode);
        }

        public static void SetTargetOfSelection(InstantiableNode connectTo) {
            if(connectTo != null) {
                if(DialogueBaseOption.selection != null && DialogueBaseOption.selection.target != null) {                   // Existing connection breaks.
                    DialogueBaseOption.selection.target.numConnectedTo--;
                    DialogueBaseOption.selection._parent.numConnectedFrom--;
                }

                DialogueBaseOption.selection.target = connectTo;
                DialogueBaseOption.selection._parent.numConnectedFrom++;
            }
            DialogueBaseOption.selection = null;
            DialogueBaseOption.inConnectingMode = false;
        }

        public BaseNode GetParent() {
            return _parent;
        }
#endif
    }
}