#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Dialogical {
    public class DialogueEditorWindow : EditorWindow {
        // Settings
        private static readonly Vector2 _minEditorSize       = new Vector2(800, 600);
        private static readonly bool    _dblClickCreatesNode = true;
        private static readonly bool    _showAllMenuButtons = true;
        private static readonly string  _assetCreationPath  = "Assets/Dialogical/Dialogues/";

        // Repaint frequency (apply settings here from minimap): 0 - very low (no repaint before focus), 1 - low (10fps), 2 - normal (100fps); Lower this if you're having performance issues.
        private static readonly int     _performanceMode    = 2;                                             

        //Setup
        private static readonly string  _resourcesFullPath = "Assets/Dialogical/Resources/";
        private static bool             _debugMode = false;

        // Display
        private Texture     _background;
        private Rect        _loadedTreeRect, _bgOffsetRect;

        private int         _numTilesX, _numTilesY;
        private Vector2     _prevScreenResolution;
        private NodeGraphics _nodeGraphics;
        private bool        _alternativeButtons;
        private bool        _initializationComplete;

        // Interface
        private Texture[]   _arrowsInactive = new Texture[4];
        private Texture[]   _arrowsActive   = new Texture[4];
        private Rect        _navigationGroup;
        private Rect        _arrowUpRect = new Rect(40, 0, 30, 40), _arrowRightRect = new Rect(70, 40, 40, 30), _arrowDownRect = new Rect(40, 70, 30, 40), _arrowLeftRect = new Rect(0, 40, 40, 30);
        private bool        _moreElementsUp, _moreElementsRight, _moreElementsDown, _moreElementsLeft;
        private Rect        _newTreeRect = new Rect(5, 5, 90, 17), _saveTreeRect = new Rect(95, 5, 90, 17), _loadTreeRect = new Rect(185, 5, 90, 17);
        private Rect        _newNodeRect = new Rect(300, 5, 100, 17), _newChoiceRect = new Rect(400, 5, 100, 17), _deleteNodeRect = new Rect(500, 5, 100, 17), _deleteOptionRect = new Rect(600, 5, 100, 17), _deleteOptionPos;
        private bool        _tempIsChoiceNode;

        // Windows
        private DialogueSaveWindow      _saveWindow;
        private DialogueLoadWindow      _loadWindow;
        private LanguageManagerWindow   _langWindow;
        private DialogueMinimapWindow   _minimapWindow;
        private DiscardChangesDialog    _discardChanges;
        private Rect                    _innerClip = new Rect(0, 27, 0, 0);         // Clips the area where nodes are drawn.
        public  static DeleteDialog     deleteDialog;
        public  bool                    minimapWindowExists;                        // See other comments in this one's use.
        private string                  _lastLoadForViewing = "";

        // Language
        private Rect        _languageLabelRect, _languageChoiceRect, _languageManagerButtonRect;
        private int         _previousSelectedLanguageIndex;
        public  int         selectedLanguageIndex;

        // Minimap
        private Rect        _minimapButtonRect, _hiddenButtonRect;
        public  Rect        visibleArea;

        // Data
        private DialogueTree _dialogueTree;
        private string       _loadedTreeName;
        private Languages    _langs;

        // CTors.
        private static DialogueEditorWindow _editor;

        void OnDisable() {
            if(_saveWindow != null) _saveWindow.Close();
            if(_loadWindow != null) _loadWindow.Close();
            if(_langWindow != null) _langWindow.Close();
            if(deleteDialog != null) deleteDialog.Close();
            if(_discardChanges != null) _discardChanges.Close();
            if(_minimapWindow != null) _minimapWindow.Close();

            SaveTree();
        }

        void OnEnable() {
            if(_initializationComplete && minimapWindowExists) ShowMinimap();           // Show minimap for play mode.
        }

        [MenuItem("Window/Dialogical")]
        public static void CreateEditor() {
            bool editorWasNull = (_editor == null);
            _editor = GetWindow<DialogueEditorWindow>("Dialogical", true);

            if(editorWasNull) {
                _editor.minSize = _minEditorSize;
                _editor.Init();
            }
        }

        void Init() {
            
            _langs = AssetDatabase.LoadAssetAtPath(_assetCreationPath + "_languages.asset", typeof(Languages)) as Languages;
            if(_langs == null) {                                                                                     // Langs somehow got deleted. Recreate.
                _langs = ScriptableObject.CreateInstance<Languages>();
                _langs.CreateLanguage("English");
                AssetDatabase.CreateAsset(_langs, _assetCreationPath + "_languages.asset");
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }

            // Load graphics.
            _background = AssetDatabase.LoadAssetAtPath(_resourcesFullPath + "bg-stripes.png", typeof(Texture)) as Texture;
            if(_background == null) {
                Debug.Log("Can't find required asset. Please place this extension (Dialogical folder) inside Assets/Plugins.");
                _editor.Close();
                return;
            }
            _bgOffsetRect = new Rect(0,0, _background.width, _background.height);

            _nodeGraphics = new NodeGraphics();
            _nodeGraphics.skin = AssetDatabase.LoadAssetAtPath(_resourcesFullPath + "DialoguerSkin.guiskin", typeof(GUISkin)) as GUISkin;
            for(int i = 0; i < 9; i++) {
                _nodeGraphics.nodeSprite[i] = AssetDatabase.LoadAssetAtPath(_resourcesFullPath + "NodeGraphic/NodeGraphic_" + i + ".png", typeof(Texture)) as Texture;
            }
            _nodeGraphics.nodeOptionTexture         = AssetDatabase.LoadAssetAtPath(_resourcesFullPath + "NodeOptionGraphic.psd", typeof(Texture)) as Texture;
            _nodeGraphics.choiceOptionTexture       = AssetDatabase.LoadAssetAtPath(_resourcesFullPath + "OptionChoiceGraphic.psd", typeof(Texture)) as Texture;
            _nodeGraphics.nodeConnectedTexture      = AssetDatabase.LoadAssetAtPath(_resourcesFullPath + "NodeConnectedGraphic.psd", typeof(Texture)) as Texture;
            _nodeGraphics.choiceConnectedTexture    = AssetDatabase.LoadAssetAtPath(_resourcesFullPath + "ChoiceConnectedGraphic.psd", typeof(Texture)) as Texture;
            _nodeGraphics.nodeNotConnectedTexture   = AssetDatabase.LoadAssetAtPath(_resourcesFullPath + "NodeNotConnectedGraphic.psd", typeof(Texture)) as Texture;

            if (EditorStyles.textField == null) {                   // Protecting against layout switching which is not supported because we're using Editor Styles.
                Debug.Log("NOK");
                this.Close();   
            }

            _nodeGraphics.textAreaStyle = new GUIStyle(EditorStyles.textField);
            _nodeGraphics.textAreaStyle.wordWrap = true;
            _nodeGraphics.textAreaStyle.stretchWidth = false;
            _nodeGraphics.textAreaStyle.stretchHeight = true;

            _arrowsInactive[0] = AssetDatabase.LoadAssetAtPath(_resourcesFullPath + "Arrows/Arrow-Up-Inactive.psd", typeof(Texture)) as Texture;
            _arrowsInactive[1] = AssetDatabase.LoadAssetAtPath(_resourcesFullPath + "Arrows/Arrow-Right-Inactive.psd", typeof(Texture)) as Texture;
            _arrowsInactive[2] = AssetDatabase.LoadAssetAtPath(_resourcesFullPath + "Arrows/Arrow-Down-Inactive.psd", typeof(Texture)) as Texture;
            _arrowsInactive[3] = AssetDatabase.LoadAssetAtPath(_resourcesFullPath + "Arrows/Arrow-Left-Inactive.psd", typeof(Texture)) as Texture;
            _arrowsActive[0] = AssetDatabase.LoadAssetAtPath(_resourcesFullPath + "Arrows/Arrow-Up-Active.psd", typeof(Texture)) as Texture;
            _arrowsActive[1] = AssetDatabase.LoadAssetAtPath(_resourcesFullPath + "Arrows/Arrow-Right-Active.psd", typeof(Texture)) as Texture;
            _arrowsActive[2] = AssetDatabase.LoadAssetAtPath(_resourcesFullPath + "Arrows/Arrow-Down-Active.psd", typeof(Texture)) as Texture;
            _arrowsActive[3] = AssetDatabase.LoadAssetAtPath(_resourcesFullPath + "Arrows/Arrow-Left-Active.psd", typeof(Texture)) as Texture;

            RecalculateTilingForBG();
            _initializationComplete = true;
        }

        void CreateNewTree() {
            _dialogueTree = ScriptableObject.CreateInstance<DialogueTree>();
            _loadedTreeName = "newTree";
            _nodeGraphics.pathToAsset = _assetCreationPath + _loadedTreeName + ".asset";
            _nodeGraphics.languages = _langs;
            _dialogueTree.SetNodeGraphics(_nodeGraphics);

            AssetDatabase.CreateAsset(_dialogueTree, _nodeGraphics.pathToAsset);

            _dialogueTree.CreateStartNode(new Vector2(75, 45));

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            BaseNode.lastSelection = null;
            DialogueBaseOption.lastSelection = null;
        }

        void RecreateNewTree() {
            DestroyImmediate(_dialogueTree, true);
            AssetDatabase.SaveAssets();
            CreateNewTree();
        }

        void SaveTree() {
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        public void SaveNewTree(string name) {
            string fullNewPath = _assetCreationPath + name + ".asset";
            AssetDatabase.DeleteAsset(fullNewPath);                     // Delete if it's there - otherwize we'd get in trouble in play mode saving.
            AssetDatabase.CopyAsset(_assetCreationPath + _loadedTreeName + ".asset", fullNewPath);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            LoadTree(name);                                             // We have to switch to this new tree.
        }

        public void LoadTree(string name) {
            bool reopenMinimap = false;

            if(_minimapWindow != null) {
                reopenMinimap = true;
                _minimapWindow.Close();
            }

            _dialogueTree = AssetDatabase.LoadAssetAtPath(_assetCreationPath + name + ".asset", typeof(DialogueTree)) as DialogueTree;
            _loadedTreeName = name;
            _nodeGraphics.pathToAsset = _assetCreationPath + _loadedTreeName + ".asset";
            _nodeGraphics.languages = _langs;
            _dialogueTree.SetNodeGraphics(_nodeGraphics);
            selectedLanguageIndex = _dialogueTree.currentLanguageIndex;

            if(reopenMinimap) {
                ShowMinimap();
            }

            _dialogueTree.FakeLastSelection();
        }

        public void Load4Viewing(string name) {
            AssetDatabase.DeleteAsset(_assetCreationPath + "newTree.asset");
            AssetDatabase.CopyAsset(_assetCreationPath + name + ".asset", _assetCreationPath + "newTree.asset");
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            _lastLoadForViewing = name;
            LoadTree("newTree");
        }

        // Methods
        void RecalculateTilingForBG() {
            _numTilesX = Mathf.CeilToInt(position.width / _background.width);
            _numTilesY = Mathf.CeilToInt(position.height / _background.height);
            _prevScreenResolution.x = position.width;
            _prevScreenResolution.y = position.height;

            // Recalculate other stuff
            _navigationGroup        = new Rect(position.width - 120, 5, 120, 120);
            _languageLabelRect      = new Rect(5, position.height - 20, 50, 20);
            _languageChoiceRect     = new Rect(75, position.height - 22, 100, 20);
            _languageManagerButtonRect = new Rect(185, position.height - 22, 80, 15);
            _loadedTreeRect         = new Rect(290, position.height - 20, 500, 20);
            _innerClip              = new Rect(0, 27, position.width, position.height - 52);
            visibleArea             = new Rect(0,0,_innerClip.width, _innerClip.height);
            _minimapButtonRect      = new Rect(position.width - 90, position.height - 22, 80, 15);
            _hiddenButtonRect       = new Rect(position.width - 180, position.height - 22, 80, 15);
        }

        void Update() {
            if (_performanceMode == 2) Repaint();
        }

        void OnInspectorUpdate() {
            if(_performanceMode == 1) Repaint();
        }

        public void OnGUI() {
            if(_nodeGraphics == null) Init();
            if(_dialogueTree == null) CreateNewTree();                                          // Have to do this here - situation with opening Unity and this window already open -- asset saving issue.
            if(minimapWindowExists && _minimapWindow == null) minimapWindowExists = false;      // We have to know if minimap was closed manually, or on entering play mode. This means it got closed manually, so note that.
            
            // Draw background
            for(int x = 0; x < _numTilesX; x++) {
                for(int y = 0; y < _numTilesY; y++) {
                    _bgOffsetRect.x = x * _background.width;
                    _bgOffsetRect.y = y * _background.height;
                    GUI.DrawTexture(_bgOffsetRect, _background);
                }
            }

            // Prevent interaction in delete mode
            if (deleteDialog != null && (Event.current.type == EventType.mouseDown || Event.current.type == EventType.mouseUp || Event.current.type == EventType.mouseDrag)) {
                Event.current.Use();
            }
            
            // Drawing the Menu
            if(GUI.Button(_newTreeRect, "New Tree", EditorStyles.miniButtonLeft)) {
                if(_loadedTreeName == "newTree") {
                    if(_dialogueTree.nodes.Count > 1) {                                 // We haven't saved, and we have created nodes other then Start.
                        _discardChanges = EditorWindow.GetWindow<DiscardChangesDialog>(true, "Discard Changes");
                        _discardChanges.Init(RecreateNewTree);
                    } else {
                        RecreateNewTree();                                              // Even if the tree is blank, recreate.
                    }
                } else {                                                                // This has been saved previously
                    SaveTree();
                    CreateNewTree();
                }
                _lastLoadForViewing = "";
            };

            if(_loadedTreeName == "newTree" || _alternativeButtons) {
                if(GUI.Button(_saveTreeRect, "Save Tree As...", EditorStyles.miniButtonMid)) {
                    _saveWindow = GetWindow<DialogueSaveWindow>(true, "Save Tree");
                    _saveWindow.Init(this, _lastLoadForViewing);
                    _saveWindow.Show();
                    _lastLoadForViewing = "";
                };
            } else {
                if(GUI.Button(_saveTreeRect, "Save Tree", EditorStyles.miniButtonMid)) {
                    SaveTree();
                    _lastLoadForViewing = "";
                };
            }

            if(GUI.Button(_loadTreeRect, _alternativeButtons? "Load 4 Viewing" : "Load Tree", EditorStyles.miniButtonRight)) {
                _loadWindow = GetWindow<DialogueLoadWindow>(true, _alternativeButtons ? "Load for Viewing" : "Load Tree");
                _loadWindow.Init(this, _assetCreationPath, _alternativeButtons);
                _loadWindow.Show();
            };

            if(_showAllMenuButtons) {
                if(GUI.Button(_newNodeRect, "New Node", EditorStyles.miniButtonLeft)) {
                    _dialogueTree.CreateNode(new Vector2(position.width / 2, position.height / 2));
                };

                if(GUI.Button(_newChoiceRect, "New Choice", (BaseNode.lastSelection != null || DialogueBaseOption.lastSelection != null) ? EditorStyles.miniButtonMid : EditorStyles.miniButtonRight)) {
                    _dialogueTree.CreateChoiceNode(new Vector2(position.width / 2, position.height / 2));
                };

                if(BaseNode.lastSelection != null) {
                    if(_deleteNodeRect.Contains(Event.current.mousePosition) && deleteDialog == null) {                                 // Hover over delete node                      
                        BaseNode.lastSelection.showRedMarker = true;
                        BaseNode.lastSelection.redMarkerRect = BaseNode.lastSelection.nodeRect;
                    } else if(deleteDialog == null) {
                        BaseNode.lastSelection.showRedMarker = false;
                    }

                    if(GUI.Button(_deleteNodeRect, "Delete Node", (DialogueBaseOption.lastSelection != null)? EditorStyles.miniButtonMid : EditorStyles.miniButtonRight)) {
                        InstantiableNode deleteableNode = BaseNode.lastSelection as InstantiableNode;           // Se we don't delete start node.
                        if(deleteableNode != null) {
                            deleteDialog = EditorWindow.GetWindow<DeleteDialog>(true, "Delete NODE");
                            deleteDialog.Init4Node(deleteableNode, _dialogueTree);
                        }
                    }
                }

                if(DialogueBaseOption.lastSelection != null) {
                    _deleteOptionPos = (BaseNode.lastSelection != null) ? _deleteOptionRect : _deleteNodeRect;

                    if(_deleteOptionPos.Contains(Event.current.mousePosition) && deleteDialog == null) {                                 // Hover over delete option                      
                        DialogueBaseOption.lastSelection.showRedMarker = true;
                        DialogueBaseOption.lastSelection.redMarkerRect = DialogueBaseOption.lastSelection.backgroundRect;
                    } else if(deleteDialog == null) {
                        DialogueBaseOption.lastSelection.showRedMarker = false;
                    }

                    if(GUI.Button(_deleteOptionPos, "Delete Option", EditorStyles.miniButtonRight)) {
                        GUI.FocusControl("");
                        deleteDialog = EditorWindow.GetWindow<DeleteDialog>(true, "Delete OPTION");
                        deleteDialog.Init4Option(DialogueBaseOption.lastSelection, DialogueBaseOption.lastSelection.GetParent());
                    }
                }
            }
            
            // Drawing navigation arrows
            _moreElementsLeft = _moreElementsUp = _moreElementsRight = _moreElementsDown = false;
            foreach(var node in _dialogueTree.nodes) {
                if(node.nodeRect.x + node.nodeRect.width < 0) _moreElementsLeft = true;
                if(node.nodeRect.y + node.nodeRect.height < 0) _moreElementsUp = true;
                if(node.nodeRect.x > position.width) _moreElementsRight = true;
                if(node.nodeRect.y > position.height) _moreElementsDown = true;
            }
            
            GUI.BeginGroup(_navigationGroup);
            {
                GUI.DrawTexture(_arrowUpRect, _moreElementsUp ? _arrowsActive[0] : _arrowsInactive[0]);
                GUI.DrawTexture(_arrowRightRect, _moreElementsRight ? _arrowsActive[1] : _arrowsInactive[1]);
                GUI.DrawTexture(_arrowDownRect, _moreElementsDown ? _arrowsActive[2] : _arrowsInactive[2]);
                GUI.DrawTexture(_arrowLeftRect, _moreElementsLeft ? _arrowsActive[3] : _arrowsInactive[3]);
            }
            GUI.EndGroup();

            // Bottom Left.
            GUI.Label(_languageLabelRect, "Language:", _nodeGraphics.skin.GetStyle("BodyLabel"));
            selectedLanguageIndex = EditorGUI.Popup(_languageChoiceRect, selectedLanguageIndex, _langs.GetLanguages().ToArray());
            if(selectedLanguageIndex != _previousSelectedLanguageIndex) {
                GUI.FocusControl("");
                _previousSelectedLanguageIndex = selectedLanguageIndex;
                _dialogueTree.SwitchToLanguage(selectedLanguageIndex);
            }

            if(GUI.Button(_languageManagerButtonRect, "Manage", EditorStyles.miniButton)) {
                _langWindow = GetWindow<LanguageManagerWindow>(true, "Manage Languages");
                _langWindow.Init(this, _langs, _dialogueTree);
                _langWindow.Show();
            }

            GUI.Label(_loadedTreeRect, "Loaded Dialogue Tree: " + _loadedTreeName, _nodeGraphics.skin.GetStyle("BodyLabel"));

            if(GUI.Button(_minimapButtonRect, "Minimap", EditorStyles.miniButton)) {
                ShowMinimap();
            }

            if (_debugMode && GUI.Button(_hiddenButtonRect, "Hidden button", EditorStyles.miniButton)) {
                
            }
            
            
            GUI.BeginClip(_innerClip);

            // Draw connections
            if(Event.current.type == EventType.repaint) {
                foreach(var node in _dialogueTree.nodes) {
                    _tempIsChoiceNode = node is DialogueChoiceNode;
                    foreach(var option in node.options) {
                        option.DrawNodeConnection(_tempIsChoiceNode);
                    }
                }
            }
            GUI.changed = false;

            // Draw nodes themselves; and process their events first
            foreach(var node in _dialogueTree.nodes) {
                node.OnGUI();
            }

            GUI.EndClip();
            

            // If we have a selection, we're doing an operation which requires an update each mouse move
            wantsMouseMove = BaseNode.lastSelection != null;

            switch(Event.current.type) {
                case EventType.mouseUp:                                                     // If we had a mouse up event which was not handled by the nodes, clear our selection
                DialogueBaseOption.SetTargetOfSelection(null);
                Event.current.Use();
                break;

                case EventType.mouseDrag:                                                    // We're panning
                MoveAllNodes(Event.current.delta);
                break;

                case EventType.mouseDown:
                BaseNode.selection = null;
                DialogueBaseOption.selection = null;
                GUI.FocusControl("");
                if(_dblClickCreatesNode && Event.current.clickCount == 2) {                 // If we double-click and no node handles the event, create a new node there
                    if(Event.current.button == 0) {
                        _dialogueTree.CreateNode(Event.current.mousePosition);              // LMB Creates Normal Node, RMB Creates Choice Node
                    } else if(Event.current.button == 1) {
                        _dialogueTree.CreateChoiceNode(Event.current.mousePosition);
                    }
                    Event.current.Use();
                }
                break;

                case EventType.keyDown:
                if(Event.current.keyCode == KeyCode.LeftControl || Event.current.keyCode == KeyCode.RightControl) {
                    _alternativeButtons = true;
                    GUI.changed = true;
                }
                break;

                case EventType.keyUp:
                if(Event.current.keyCode == KeyCode.LeftControl || Event.current.keyCode == KeyCode.RightControl) {
                    _alternativeButtons = false;
                    GUI.changed = true;
                }
                break;
            }

            if (_dialogueTree.nodesToDelete.Count > 0) _dialogueTree.DeleteMarkedNodes();

            // Have we resized the window? If so, recalculate the tiling for Background.
            if(_prevScreenResolution.x != position.width || _prevScreenResolution.y != position.height) {
                RecalculateTilingForBG();
            }

            if(GUI.changed) {
                Repaint();
                EditorUtility.SetDirty(_dialogueTree);
            }
        }

        public void MoveAllNodes(Vector2 delta) {
            foreach(var node in _dialogueTree.nodes) {
                node.Move(delta);
            }
            GUI.changed = true;
        }

        private void ShowMinimap() {
            _minimapWindow = GetWindow<DialogueMinimapWindow>(true, "Dialogue Minimap");
            _minimapWindow.Init(this, _dialogueTree, _background);
            _minimapWindow.Show();
            minimapWindowExists = true;
        }

        public void ResetAltButtons() {
            _alternativeButtons = false;
        }

        void OnFocus() {
            ResetAltButtons();
        }
    }

    public class DialogueSaveWindow : EditorWindow {
        private Vector2 _size           = new Vector2(250, 70);
        private Rect _labelRect         = new Rect(10, 10, 100, 20);
        private Rect _inputFieldRect    = new Rect(110, 10, 125, 17);
        private Rect _buttonRect        = new Rect(75, 40, 100, 18);
        private DialogueEditorWindow _parent;
        private string _saveName = "";
        private Regex rgx = new Regex("[^a-zA-Z0-9-_ ]");

        public void Init(DialogueEditorWindow parent, string lastLoadForViewing) {
            _parent = parent;
            _saveName = lastLoadForViewing;
            minSize = maxSize = _size;
        }

        void OnGUI() {
            GUI.Label(_labelRect, "Save Tree As:");
            _saveName = GUI.TextField(_inputFieldRect, _saveName);
            _saveName = rgx.Replace(_saveName, "");

            if(string.IsNullOrEmpty(_saveName) || _saveName.ToLower() == "newtree" || _saveName.ToLower() == "_languages") GUI.enabled = false;
            if(GUI.Button(_buttonRect, "Save")) {
                _parent.SaveNewTree(_saveName);

                this.Close();
            }
        }

        void OnDisable() {
            _parent.Show();
            _parent.Repaint();
            _parent.Focus();
        }
    }

    public class DialogueLoadWindow : EditorWindow {
        private Vector2         _size = new Vector2(250, 250);
        private DialogueEditorWindow _parent;
        private Rect            _notificationRect = new Rect(25,10,200,20);
        private string[]        _filePaths;
        private List<string>    _onlyFilenameNoExt = new List<string>();
        private Vector2         _scrollPosition;
        private bool            _load4Viewing;

        public void Init(DialogueEditorWindow parent, string pathToLoad, bool load4viewing = false) {
            _onlyFilenameNoExt.Clear();
            _parent = parent;
            _load4Viewing = load4viewing;

            position = new Rect(parent.position.x + 100, parent.position.y + 100, _size.x, _size.y);
            string fullPath = Application.dataPath.Substring(0, Application.dataPath.Length - 6) + pathToLoad;          // We remove 'Assets', then add the rest of the path
            _filePaths = Directory.GetFiles(fullPath);

            foreach(var file in _filePaths) {
                if(file.EndsWith(".asset") && !file.EndsWith("newTree.asset") && !file.EndsWith("_languages.asset")) {
                    string onlyFilename = file.Substring(fullPath.Length);
                    _onlyFilenameNoExt.Add(onlyFilename.Substring(0, onlyFilename.Length - 6));
                }
            }

            Array.Clear(_filePaths, 0, _filePaths.Length);
        }

        void OnGUI() {
            if(_onlyFilenameNoExt.Count == 0) {
                GUI.Label(_notificationRect, "No conversations saved yet.");
            }

            _scrollPosition = GUILayout.BeginScrollView(_scrollPosition, GUILayout.Width(position.width - 5), GUILayout.Height(position.height - 5));

            foreach(var file in _onlyFilenameNoExt) {
                if(GUILayout.Button(file)) {

                    if(!_load4Viewing)  _parent.LoadTree(file);
                    else                _parent.Load4Viewing(file);
                    
                    this.Close();
                }
            }
            GUILayout.EndScrollView();
        }

        void OnDisable() {
            _parent.Show();
            _parent.Repaint();
            _parent.Focus();
        }

    }

    public class LanguageManagerWindow : EditorWindow {
        private Vector2 _size = new Vector2(250, 70);
        private Rect _editArea = new Rect(5,5,0,0);
        private Rect _addButtonRect = new Rect(0,0,120,20);
        private Languages _langs;
        private List<int> _langsToDelete = new List<int>();
        private DialogueTree _tree;
        private string _tempLangName;
        private DialogueEditorWindow _parent;

        public void Init(DialogueEditorWindow parent, Languages langs, DialogueTree tree) {
            _parent = parent;
            _langs = langs;
            _tree = tree;
            position = new Rect(parent.position.x + 100, parent.position.y + 100, _size.x, _size.y);
        }

        void OnGUI() {
            _editArea.width = position.width - 10;
            _editArea.height = position.height - 10;
            GUILayout.BeginArea(_editArea);
            
            for(int i = 0; i < _langs.GetLanguagesCount(); i++) {
                GUILayout.BeginHorizontal();
                _tempLangName = GUILayout.TextField(_langs.GetLang(i));
                if(_tempLangName != _langs.GetLang(i)) _langs.SetLangName(i, _tempLangName);

                if (i > 0 && GUILayout.Button("-", GUILayout.Width(19), GUILayout.Height(14))) {
                    if (EditorUtility.DisplayDialog("Sure to delete?", "Are you sure you want to delete this language and associated data from all the nodes? This action is irreversible.", "OK", "Cancel")) {
                        MarkForDeletion(i);
                    }
                }
                GUILayout.EndHorizontal();
            }
            
            GUILayout.EndArea();

            _addButtonRect.x = (position.width - _addButtonRect.width) / 2;
            _addButtonRect.y = position.height - 25;
            if (GUI.Button(_addButtonRect, "Add Language")) {
                AddLanguage();
            }

            if (GUI.changed) {
                DeleteMarkedLanguages();
                EditorUtility.SetDirty(_langs);
            }
        }

        void AddLanguage() {
            int newID = _langs.CreateLanguage("");
            _tree.CreateLanguage(newID);
        }

        void MarkForDeletion (int i) {
            if(!_langsToDelete.Contains(i)) _langsToDelete.Add(i);
        }

        void DeleteMarkedLanguages() {
            foreach(var index in _langsToDelete) {
                _langs.RemoveLanguage(index);
                _tree.DeleteLanguageAt(index);
                if(_parent.selectedLanguageIndex == index)      _parent.selectedLanguageIndex = 0;
                else if(_parent.selectedLanguageIndex > index)  _parent.selectedLanguageIndex--;
            }
            _langsToDelete.Clear();
        }

        void OnDisable() {
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

    }
}
#endif