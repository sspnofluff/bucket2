using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;
using System;
using System.Linq;

namespace Dialogical {
    [Serializable]
    public class DialogueTree : ScriptableObject {
#if UNITY_EDITOR
        // Setup
        private static readonly bool    _debugMode = false;
        private static readonly float   _completeRectMargins = 30f;
#endif

        // Data
        [SerializeField]
        public List<BaseNode> nodes = new List<BaseNode>();
        [SerializeField]
        public List<InstantiableNode> nodesToDelete = new List<InstantiableNode>();
        [SerializeField]
        public int currentLanguageIndex;
        [SerializeField]
        public List<int> languageKeys = new List<int>();
        [SerializeField]
        public Rect completeRectOriginal = new Rect();                                   // Rectangle in original dimensions that would contain all nodes with some extra spacing.
        public delegate void OnCompleteRectChangeDelegate();
        public OnCompleteRectChangeDelegate OnCompleteRectChange;

#if UNITY_EDITOR
        private NodeGraphics _nodeGraphics;
        private int          _langsToCreateInNodes;
        private Vector2      _infinity = new Vector2(Mathf.Infinity, Mathf.Infinity), _minusInfinity = new Vector2(-Mathf.Infinity, -Mathf.Infinity);
        private Vector2      _topLeftPoint, _bottomRightPoint;

        public void SetNodeGraphics(NodeGraphics nodeGraphics) {            // Serves as Init.
            _nodeGraphics = nodeGraphics;

            SyncLanguages();

            foreach(var node in nodes) {
                node.SetNodeGraphics(_nodeGraphics);
                node.SwitchToLanguage(currentLanguageIndex);
            }
        }

        private void SyncLanguages() {
            List<int> trueKeys = _nodeGraphics.languages.GetKeys();
            List<int> extraKeys = languageKeys.Except(trueKeys).ToList();
            List<int> missingKeys = trueKeys.Except(languageKeys).ToList();

            foreach(var key in extraKeys) {
                int index = languageKeys.FindIndex(x => x.Equals(key));
                DeleteLanguageAt(index);
            }

            foreach(var key in missingKeys) {
                CreateLanguage(key);
            }
        }

        public void CreateLanguage(int newID) {
            languageKeys.Add(newID);

            foreach(var node in nodes) {
                node.CreateLanguage();
            }
        }
        public void DeleteLanguageAt(int index) {
            languageKeys.RemoveAt(index);
            foreach(var node in nodes) {
                node.DeleteLanguageAt(index);
            }
        }
        public void SwitchToLanguage(int index) {
            currentLanguageIndex = index;
            foreach(var node in nodes) {
                node.SwitchToLanguage(index);
            }
        }

        public void CreateNode(Vector2 where) {
            DialogueNode createdNode = ScriptableObject.CreateInstance<DialogueNode>();
            nodes.Add(createdNode);
            createdNode.Init("Node " + nodes.Count, where, _nodeGraphics, this);

            AssetDatabase.AddObjectToAsset(createdNode, _nodeGraphics.pathToAsset);

            if(nodes.Count == 2) {                                          // If only Start exists so far, connect Start to this node.
                nodes[0].options[0].target = createdNode;
                createdNode.numConnectedTo++;
            }

            BaseNode.lastSelection = createdNode;
        }

        public void CreateChoiceNode(Vector2 where) {
            DialogueChoiceNode createdNode = ScriptableObject.CreateInstance<DialogueChoiceNode>();
            nodes.Add(createdNode);
            createdNode.Init("Choice " + nodes.Count, where, _nodeGraphics, this);

            AssetDatabase.AddObjectToAsset(createdNode, _nodeGraphics.pathToAsset);

            if(nodes.Count == 2) {                                          // If only Start exists so far, connect Start to this node.
                nodes[0].options[0].target = createdNode;
                createdNode.numConnectedTo++;
            }

            BaseNode.lastSelection = createdNode;
        }

        public void CreateStartNode(Vector2 where) {
            DialogueStartNode startNode = ScriptableObject.CreateInstance<DialogueStartNode>();
            nodes.Add(startNode);
            startNode.Init(where, _nodeGraphics, this);

            AssetDatabase.AddObjectToAsset(startNode, _nodeGraphics.pathToAsset);
        }

        public void MarkForDeletion(InstantiableNode node) {
            if(!nodesToDelete.Contains(node)) nodesToDelete.Add(node);
        }

        public void DeleteMarkedNodes() {
            bool doFakelastSelection = false;
            foreach(var nodeToDelete in nodesToDelete) {
                if(nodeToDelete == BaseNode.lastSelection) doFakelastSelection = true;
                foreach(var node in nodes) {
                    foreach(var option in node.options) {
                        if(option.target == nodeToDelete) option.target = null;
                    }
                }

                if(nodes.Contains(nodeToDelete)) nodes.Remove(nodeToDelete);
                DestroyImmediate(nodeToDelete, true);
            }

            nodesToDelete.Clear();
            RecalculateCompleteRectOriginal();
            if(doFakelastSelection) FakeLastSelection();
        }

        void OnDestroy() {
            if(_debugMode) Debug.Log("Destroying tree.");

            foreach(var node in nodes) {
                DestroyImmediate(node, true);
            }
            nodes.Clear();

            nodesToDelete.Clear();

            _nodeGraphics = null;
        }

        public void RecalculateCompleteRectOriginal() {
            _topLeftPoint = _infinity;
            _bottomRightPoint = _minusInfinity;

            foreach(var node in nodes) {
                if(node.nodeRect.x < _topLeftPoint.x) _topLeftPoint.x = node.nodeRect.x;
                if(node.nodeRect.y < _topLeftPoint.y) _topLeftPoint.y = node.nodeRect.y;
                if(node.nodeRect.x + node.nodeRect.width > _bottomRightPoint.x) _bottomRightPoint.x = node.nodeRect.x + node.nodeRect.width;
                if(node.nodeRect.y + node.nodeRect.height > _bottomRightPoint.y) _bottomRightPoint.y = node.nodeRect.y + node.nodeRect.height;
            }

            completeRectOriginal.x = _topLeftPoint.x - _completeRectMargins;
            completeRectOriginal.y = _topLeftPoint.y - _completeRectMargins;
            completeRectOriginal.width  = _bottomRightPoint.x - _topLeftPoint.x + 2 * _completeRectMargins;
            completeRectOriginal.height = _bottomRightPoint.y - _topLeftPoint.y + 2 * _completeRectMargins;

            if(OnCompleteRectChange != null) OnCompleteRectChange();
        }

        public void FakeLastSelection() {
            int i = nodes.Count - 1;
            
            BaseNode.lastSelection = (i > 0)? BaseNode.lastSelection = nodes[i] : null;

            while (nodes[i].options.Count == 0) {
                i--;
                if(i <= 0) break;                                                             
            }

            DialogueBaseOption.lastSelection = (i > 0) ? nodes[i].options[0] : null;
        }
#endif
    }
}
