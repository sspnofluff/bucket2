using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;

namespace Dialogical {

    [Serializable]
    public class DialogueStartNode : BaseNode {
#if UNITY_EDITOR
        // Setup
        private static readonly Vector2 _initialStartNodeSize   = new Vector2(95, 70);

        // Data
        [SerializeField]
        private Rect _startLabelRect = new Rect(25,8,70,30);

        [SerializeField]
        private Rect _nodeRct;
        [SerializeField]
        public override Rect nodeRect {
            get { return _nodeRct; }
            protected set {
                _nodeRct = value;
                _startLabelRect.x = _nodeRct.x + 25; _startLabelRect.y = _nodeRct.y + 8;
                if(options.Count > 0) {                                                                            // This is only 0 when calling Init().
                    options[0].UpdatePositioning(new Vector2(_nodeRct.x + _nodeRct.width - _nodeGraphics.nodeConnectedTexture.width - 10, _nodeRct.y + 38));
                }

                _parentTree.RecalculateCompleteRectOriginal();
            }
        }

        // CTors
        public void Init(Vector2 position, NodeGraphics nodeGraphics, DialogueTree parentTree) {
            _parentTree = parentTree;
            title = "Start";
            _nodeGraphics = nodeGraphics;
            nodeRect = new Rect(position.x - _initialStartNodeSize.x / 2, position.y - _initialStartNodeSize.y / 2, _initialStartNodeSize.x, _initialStartNodeSize.y);

            CreateOption();
        }

        protected override void CreateOption() {                                                                    // This is only called once, on node creation.
            DialogueBaseOption option = ScriptableObject.CreateInstance<DialogueBaseOption>();
            option.Init(_nodeGraphics, this);
            // option.UpdatePositioning(new Vector2(_nodeRct.x + _nodeRct.width - _nodeGraphics.nodeConnectedTexture.width - 10, _nodeRct.y + 38));

            AssetDatabase.AddObjectToAsset(option, _nodeGraphics.pathToAsset);
            options.Add(option);

            AddHeight(0);                                                                                            // This calls nodeRect, which updates positioning of options immediatelly.
        }

        public override void OnGUI() {
            Helpers.DrawSprite(nodeRect, _nodeGraphics.nodeSprite);

            if(lastSelection == this) {
                _nodeGraphics.skin.label.normal.textColor = _nodeTitleSelectedColor;
            } else {
                _nodeGraphics.skin.label.normal.textColor = _nodeTitleBaseColor;
            }

            GUI.Label(_startLabelRect, "Start", _nodeGraphics.skin.label);

            options[0].OnGUI();

            switch(Event.current.type) {
                case EventType.mouseDown:
                if(nodeRect.Contains(Event.current.mousePosition)) {    // Select this node if we clicked it
                    selection = this;
                    Event.current.Use();
                }
                break;

                case EventType.mouseUp:
                if(selection == this) {                                
                    selection = null;
                    Event.current.Use();
                }
                break;

                case EventType.mouseDrag:
                if(selection == this) {                                 // If doing a mouse drag with this component selected drag the component                     
                    Move(Event.current.delta);
                    Event.current.Use();
                }
                break;
            }
        }

        void OnDestroy() {
            _nodeGraphics = null;
        }

        public override bool IsConnectedFrom() {
            return (options[0].target != null);
        }

        public override bool IsConnectedTo() {
            return false;
        }
#endif
    }
}