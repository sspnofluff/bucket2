using UnityEngine;

public static class ExtensionMethods {
    public static void DestroyChildren(this Transform root) {
        foreach(Transform child in root) {
            GameObject.Destroy(child.gameObject);
        }
    }
}
