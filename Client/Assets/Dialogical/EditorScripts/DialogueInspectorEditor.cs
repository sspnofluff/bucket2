#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using Dialogical;
using System.Reflection;
using System.Collections.Generic;

[CustomEditor(typeof(Dialogue)), CanEditMultipleObjects]
public class DialogueInspectorEditor : Editor {
    // Settings
    private double   _displayTime = 5d;

    // Data
    private Dialogue        _castTarget;
    private DialogueTree    _tree;
    private string          _message = "";
    private bool            _foundError;
    private MessageType     _messageType;
    private double          _hideHelpBoxTime = 0d;
    private bool            _waitingForTimer;
    public List<string>    _methods;

    void OnEnable() {
        _castTarget = (Dialogue)target;
    }

    public override void OnInspectorGUI() {
        DrawDefaultInspector();
        
        EditorGUILayout.Space();
        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        if(GUILayout.Button("Validate Tree", GUILayout.Width(Screen.width / 2), GUILayout.MaxHeight(17))) {
            ValidateTree();
            SetTimer();
        }
        
        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();

        if(_message != "") {
            EditorGUILayout.HelpBox(_message, _messageType, true);
        }

        if(_waitingForTimer && EditorApplication.timeSinceStartup > _hideHelpBoxTime) {
            _message = "";
        }
    }

    void ValidateTree() {
        _foundError = false;

        _tree = _castTarget.tree;
        if (_tree == null) {
            _message = "There is no tree attached to this script.";
            _messageType = MessageType.Warning;
            return;
        }

        if (_tree.nodes[0].options[0].target == null) {
            _message = "Start node is not connected to any node.";
            _messageType = MessageType.Warning;
            return;
        }

        _methods = GetMethodsOnThisGO();

        foreach(var node in _tree.nodes) {
            CheckNode(node);
        }

        if(_foundError) {
            _message = "Errors found, logged to console.";
            _messageType = MessageType.Error;
        } else { 
            _message = "Success: Attached Dialogue Tree is valid.";
            _messageType = MessageType.Info;
        }
    }

    void CheckNode(BaseNode node) {
        DialogueNode dialogueNode = node as DialogueNode;
        if (dialogueNode != null) {

            CheckMethod(dialogueNode.globalEvent, dialogueNode.title);
            CheckMethod(dialogueNode.localEvent, dialogueNode.title);

            foreach (var option in dialogueNode.options) {
                DialogueNodeOption correctOption = option as DialogueNodeOption;
                if (correctOption != null) {

                    CheckMethod(correctOption.preCondition, dialogueNode.title);
                    CheckMethod(correctOption.postEvent, dialogueNode.title);
                    
                } else {
                    Debug.Log("Something invalid found in options array in node '" + dialogueNode.title + "' options! \n");
                }
            }
        }

        DialogueChoiceNode choiceNode = node as DialogueChoiceNode;
        if(choiceNode != null) {
            foreach(var option in choiceNode.options) {
                DialogueChoiceOption correctOption = option as DialogueChoiceOption;
                if(correctOption != null) {
                    CheckMethod(correctOption.condition, choiceNode.title);

                } else {
                    Debug.Log("Something invalid found in options array in node '" + choiceNode.title + "' options! \n");
                }
            }
        }
    }
    
    List<string> GetMethodsOnThisGO() {
        List<MethodInfo> methods = new List<MethodInfo>();
        List<string> results = new List<string>();
        var monoBehaviors = _castTarget.gameObject.GetComponents<MonoBehaviour>();

        var flags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Default;
        foreach(var monoBehavior in monoBehaviors) {
            methods.AddRange(monoBehavior.GetType().GetMethods(flags));
        }

        foreach(var method in methods) {
            results.Add(method.ToString());
        }

        return results;
    }

    void CheckMethod(string method, string parentNodeTitle) {
        if(!string.IsNullOrEmpty(method) && !_methods.Contains("Void " + method + "()")) {
            Debug.Log("Method '" + method + "' on node '" + parentNodeTitle + "' is missing or with incorrect signature!");
            _foundError = true;
        }
    }

    void SetTimer() {
        _hideHelpBoxTime = EditorApplication.timeSinceStartup + _displayTime;
        _waitingForTimer = true;
    }
}
#endif
