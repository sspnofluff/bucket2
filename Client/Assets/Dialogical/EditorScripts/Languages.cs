using System;
using UnityEngine;
using System.Collections.Generic;

namespace Dialogical {
    [Serializable]
    public class Languages : ScriptableObject {
        [SerializeField]
        private int uniqueLanguageID;
        [SerializeField]
        private List<string> languagesValues = new List<string>();
        [SerializeField]
        private List<int> languagesKeys = new List<int>();

        public int CreateLanguage(string name) {
            int result = ++uniqueLanguageID;
            languagesKeys.Add(result);
            languagesValues.Add(name);
            return result;
        }

        public List<string> GetLanguages() {
            return languagesValues;
        }

        public List<int> GetKeys() {
            return languagesKeys;
        }

        public void RemoveLanguage(int index) {
            languagesValues.RemoveAt(index);
            languagesKeys.RemoveAt(index);
        }
        
        public int GetLanguagesCount() {
            return languagesKeys.Count;
        }

        public string GetLang(int index) {
            return languagesValues[index];
        }

        public void SetLangName(int index, string newName) {
            languagesValues[index] = newName;
        }

        public int GetLangIndex(string languageName) {
            return languagesValues.FindIndex(x => x.Equals(languageName));
        }
    }
}
