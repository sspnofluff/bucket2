#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace Dialogical {
    public class DeleteDialog : EditorWindow {
        // Settings
        public  static readonly Color deletionColor = new Color(0.5f, 0, 0, 0.2f);

        // Setup
        private Vector2 _size = new Vector2(240,75);
        private Rect    _labelRect = new Rect (5,5,230,70), _okButtonRect = new Rect(15,50,90,17), _cancelButtonRect = new Rect(135,50,90,17);
        private string  _body;
        private string  _okText = "OK";
        private string  _cancelText = "Cancel";

        private InstantiableNode    _node;
        private DialogueTree        _parentTree;

        private DialogueBaseOption  _option;
        private BaseNode            _parentNode;

        private Rect    _redMarkerRect;
        
        public void Init4Node(InstantiableNode node, DialogueTree parentTree) {
            _node = node;
            _redMarkerRect = node.nodeRect;
            _parentTree = parentTree;
            minSize = maxSize = _size;
            _body = "Are you sure you want to delete the\nselected NODE?";

            _node.showRedMarker = true;
            _node.redMarkerRect = _redMarkerRect;
            this.Show();
        }

        public void Init4Option(DialogueBaseOption option, BaseNode parentNode) {
            _option = option;
            _redMarkerRect = option.backgroundRect;
            _parentNode = parentNode;
            minSize = maxSize = _size;
            _body = "Are you sure you want to delete the\nselected OPTION?";

            _option.showRedMarker = true;
            _option.redMarkerRect = _redMarkerRect;
            this.Show();
        }

        void OnGUI() {
            this.Focus();

            GUI.Label(_labelRect, _body);
            if (GUI.Button(_okButtonRect, _okText, EditorStyles.miniButton)) {
                if (_node != null) {                                // Node variation
                    _parentTree.MarkForDeletion(_node);
                    this.Close();
                } 
                
                else if( _option != null) {                         // Option variation
                    _parentNode.MarkOptionForDeletion(_option);
                    GUI.FocusControl("");
                    this.Close();
                }
            }

            if(GUI.Button(_cancelButtonRect, _cancelText, EditorStyles.miniButton)) {
                this.Close();
            }
        }
        
        void OnDisable() {
            if(_node != null)   _node.showRedMarker = false;
            if(_option != null) _option.showRedMarker = false;
        }

    }
}
#endif
