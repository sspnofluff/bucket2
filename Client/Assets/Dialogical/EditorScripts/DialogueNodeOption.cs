using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;
using System.Collections.Generic;

namespace Dialogical {
    [Serializable]
    public class DialogueNodeOption : DialogueBaseOption {
#if UNITY_EDITOR
        // Settings
        private  static readonly bool    _allowCopyPasteOptionText = true;     // If we allow Copy/Paste of Node text, then on first click inside the node everyting is selected.

        // Setup
        private static readonly float   _conditionTextInitialHeight = 30f;
        public  static readonly Vector2 initialNodeOptionSize   = new Vector2(307, 65);
        private static readonly bool    _debugMode = false;

        // Selection & Display
        [SerializeField]
        private float _previousTextHeight = _conditionTextInitialHeight;
#endif

        // Data
        [SerializeField]
        public string preCondition = "";
        [SerializeField]
        public List<string> Text = new List<string>();
        [SerializeField]
        public  string postEvent = "";
        // target and _parent are in DialogueBaseNode

#if UNITY_EDITOR
        // Positioning
        private Rect _conditionEventLabelRect   = new Rect(0,0,50,50);
        private Rect _contionEventInputRect     = new Rect(0,0,200,20);
        [SerializeField]
        private Rect _optionTextRect            = new Rect(0,0,297,_conditionTextInitialHeight);
        private Rect _optionPostEventLabelRect  = new Rect(0,0,50,50);
        private Rect _optionPostEventInputRect  = new Rect(0,0,200,20);

        // CTors
        protected override void OnDestroy() {
            if(_debugMode) Debug.Log("Destroying option:" + Text[currentLanguageIndex]);
            base.OnDestroy();
        }

        public override void Init(NodeGraphics nodeOptionGraphics, BaseNode parent) {
            base.Init(nodeOptionGraphics, parent);
            backgroundRect = new Rect(0, 0, initialNodeOptionSize.x, initialNodeOptionSize.y);
        }

        public override void CreateLanguage() {
            Text.Add("");
        }

        public override void DeleteLanguageAt(int index) {
            base.DeleteLanguageAt(index);
            Text.RemoveAt(index);
        }

        // Methods
        public override void UpdatePositioning(Vector2 startingPoint) {
            backgroundRect.x            = startingPoint.x + 10;  backgroundRect.y           = startingPoint.y + 1;
            _conditionEventLabelRect.x  = startingPoint.x + 20;  _conditionEventLabelRect.y = startingPoint.y;
            _contionEventInputRect.x    = startingPoint.x + 120; _contionEventInputRect.y   = startingPoint.y;
            _optionTextRect.x           = startingPoint.x + 20;  _optionTextRect.y          = startingPoint.y + 18;
            _optionPostEventLabelRect.x = startingPoint.x + 20;  _optionPostEventLabelRect.y = startingPoint.y + 23 + _optionTextRect.height;
            _optionPostEventInputRect.x = startingPoint.x + 120; _optionPostEventInputRect.y = startingPoint.y + 22 + _optionTextRect.height;
            _nodeConnectorRect.x = backgroundRect.x + backgroundRect.width;
            _nodeConnectorRect.y = backgroundRect.y + (backgroundRect.height - _nodeConnectorRect.height) / 2;
        }

        private Rect _ongui_mousePos = new Rect(0,0,0,0);
        public override void OnGUI() {
            GUI.DrawTexture(backgroundRect, _nodeOptionGraphics.nodeOptionTexture);

            if(Event.current.type == EventType.mouseDown && backgroundRect.Contains(Event.current.mousePosition)) {     // We have to catch this event first so other controls don't swallow it.
                lastSelection = this;                                                                                   // Mark lastSelection so we know what option was last worked on (for Deletion from Main Menu)
            }

            GUI.Label(_conditionEventLabelRect, "Pre-Condition:", _nodeOptionGraphics.skin.GetStyle("BodyLabel"));
            GUI.SetNextControlName("PreConditionInput");
            preCondition = EditorGUI.TextField(_contionEventInputRect, preCondition, _nodeOptionGraphics.skin.GetStyle("BodyInput"));

            float height = _nodeOptionGraphics.textAreaStyle.CalcHeight(new GUIContent(Text[currentLanguageIndex]), _optionTextRect.width);
            _optionTextRect.height = Mathf.Max(height, _conditionTextInitialHeight);
            if(_optionTextRect.height != _previousTextHeight) {
                AddHeight(_optionTextRect.height - _previousTextHeight);
                _previousTextHeight = _optionTextRect.height;
            }

            if(_allowCopyPasteOptionText) {
                Text[currentLanguageIndex] = EditorGUI.TextArea(_optionTextRect, Text[currentLanguageIndex], _nodeOptionGraphics.textAreaStyle);
            } else {
                Text[currentLanguageIndex] = GUI.TextArea(_optionTextRect, Text[currentLanguageIndex], _nodeOptionGraphics.textAreaStyle);
            }

            GUI.Label(_optionPostEventLabelRect, "Post-Event:", _nodeOptionGraphics.skin.GetStyle("BodyLabel"));
            GUI.SetNextControlName("PostOptionEventInput");
            postEvent = EditorGUI.TextField(_optionPostEventInputRect, postEvent, _nodeOptionGraphics.skin.GetStyle("BodyInput"));

            GUI.DrawTexture(_nodeConnectorRect, (target != null) ? _nodeOptionGraphics.nodeConnectedTexture : _nodeOptionGraphics.nodeNotConnectedTexture);

            // Event Handling
            switch(Event.current.type) {

                case EventType.mouseDown:
                if(_nodeConnectorRect.Contains(Event.current.mousePosition)) {      // Work with connection mode
                    selection = this;

                    if(Event.current.button == 0) inConnectingMode = true;
                    if(Event.current.button == 1) {                                 // Deletes connection: RMB on Connector Point.
                        if (target != null) {
                            target.numConnectedTo--;
                            _parent.numConnectedFrom--;
                        }
                        target = null;
                    }        

                    Event.current.Use();
                } else if(Event.current.button == 1 && backgroundRect.Contains(Event.current.mousePosition)) {          // RMB on node option deletes it.
                    DialogueEditorWindow.deleteDialog = EditorWindow.GetWindow<DeleteDialog>(true, "Delete OPTION");
                    DialogueEditorWindow.deleteDialog.Init4Option(this, _parent);
                    Event.current.Use();
                }
                break;

                case EventType.mouseDrag:
                if(selection == this) {                                  // If doing a mouse drag with this component selected, and in connect mode...
                    Event.current.Use();                                 // ... just use the event as we'll be painting the new connection
                }
                break;

                case EventType.mouseUp:                                  // Prevent forwarding of Mouse Up event on the control itself.
                if(selection == this && _nodeConnectorRect.Contains(Event.current.mousePosition)) {
                    SetTargetOfSelection(null);
                    Event.current.Use();
                }
                break;

                case EventType.repaint:
                if(selection == this && inConnectingMode) {              // New connection originates from this node.
                    _ongui_mousePos.x = Event.current.mousePosition.x;
                    _ongui_mousePos.y = Event.current.mousePosition.y;
                    Helpers.DrawConnection(_nodeConnectorRect, _ongui_mousePos, true, false);
                }
                break;
            }

            if(showRedMarker) {
                Color prevColor = GUI.backgroundColor;
                GUI.backgroundColor = DeleteDialog.deletionColor;
                GUI.Box(redMarkerRect, "");
                GUI.backgroundColor = prevColor;
            }
        }
#endif
    }
}