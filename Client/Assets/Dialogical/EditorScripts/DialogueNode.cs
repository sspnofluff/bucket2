using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;
using System.Collections.Generic;

namespace Dialogical {
    [Serializable]
    public class DialogueNode : InstantiableNode {
#if UNITY_EDITOR

        // Settings
        private static readonly bool    _createDefaultOption = true;
        public  static readonly bool    _allowCopyPasteNodeText = true;     // If we allow Copy/Paste of Node text, then on first click inside the node's text everyting is selected.

        // Setup
        private static readonly Vector2 _initialNodeSize        = new Vector2(345, 372);
        private static readonly float   _textInputInitialHeight = 100f;

        // Selection & Display
        [SerializeField]
        private float _previousTextHeight = _textInputInitialHeight;
#endif

        // Data
        // title is in BaseNode
        [SerializeField]
        public string  globalEvent  = "";
        [SerializeField]
        public string  localEvent   = "";
        [SerializeField]
        public List<string> Text    = new List<string>();
        [SerializeField]
        public List<AudioClip> Audio= new List<AudioClip>();
        [SerializeField]
        public List<float> AudioDelay= new List<float>();
        // dialogNodeOptions are in BaseNode
        [SerializeField]
        public float    autoChoiceDelay;
        [SerializeField]
        public int    autoChoiceIndex;

		[SerializeField]
		public Sprite leftPortrait = null;

		[SerializeField]
		public Sprite rightPortrait = null;

		[SerializeField]
		public string  leftName  = "";

		[SerializeField]
		public string  rightName   = "";

		[SerializeField]
		public bool isleftPersonSpeak = true;

#if UNITY_EDITOR
        private string[] _autoChoiceOptions = new string[0];

        // Positioning
        // _titleLabelRect is in InstantiableNodes.
        // _titleInputRect is in InstantiableNodes.
        private Rect _gEventLabelRect = new Rect(0,0,50,50);
        private Rect _gEventInputRect = new Rect(0,0,200,20);
        private Rect _lEventLabelRect = new Rect(0,0,50,50);
        private Rect _lEventInputRect = new Rect(0,0,200,20);
        private Rect _textLabelRect   = new Rect(0,0,50,50);
		private Rect _portraitLeftRect   = new Rect(0,0,50,50);
		private Rect _portraitRightRect   = new Rect(0,0,50,50);
		private Rect _portraitLeftLabelRect   = new Rect(0,0,50,50);
		private Rect _portraitRightLabelRect   = new Rect(0,0,50,50);
		private Rect _portraitLeftNameRect   = new Rect(0,0,50,50);
		private Rect _portraitRightNameRect   = new Rect(0,0,50,50);
        [SerializeField]
        private Rect _textInputRect   = new Rect(0,0,300,_textInputInitialHeight);
        private Rect _audioLabelRect  = new Rect(0,0,50,50);
        private Rect _audioInputRect  = new Rect(0,0,170,17);
        // _buttonAddOptionRect is in InstantiableNodes.
        private Rect _audioDelayRect  = new Rect(0,0,25,17);
        private Rect _autoChoiceLabelRect = new Rect(0,0,300,17);
        private Rect _autoChoicePopupRect = new Rect(0,0,55,17);
        private Rect _autoChoiceInputRect = new Rect(0,0,25,17);

        [SerializeField]
        private Rect _nodeRect;                             // Rectangle to draw the node. From top left point, to node height x width.
        [SerializeField]
        public override Rect nodeRect {
            get { return _nodeRect;  }
            protected set { _nodeRect = value;
                _titleLabelRect.x  = _nodeRect.x + 25;  _titleLabelRect.y   = _nodeRect.y + 8;
                _titleInputRect.x  = _nodeRect.x + 80;  _titleInputRect.y   = _nodeRect.y + 10;
                _gEventLabelRect.x = _nodeRect.x + 20;  _gEventLabelRect.y  = _nodeRect.y + 47;
                _gEventInputRect.x = _nodeRect.x + 120; _gEventInputRect.y  = _nodeRect.y + 47;
                _lEventLabelRect.x = _nodeRect.x + 20;  _lEventLabelRect.y  = _nodeRect.y + 69;
                _lEventInputRect.x = _nodeRect.x + 120; _lEventInputRect.y  = _nodeRect.y + 69;
				_portraitRightRect.x = _nodeRect.x + 270; _portraitRightRect.y = _nodeRect.y + 91;
				_portraitRightLabelRect.x = _nodeRect.x + 170; _portraitRightLabelRect.y = _nodeRect.y + 91;
				_portraitRightNameRect.x = _nodeRect.x + 170; _portraitRightNameRect.y = _nodeRect.y + 117;
				_portraitLeftRect.x = _nodeRect.x + 100; _portraitLeftRect.y = _nodeRect.y + 91;
				_portraitLeftLabelRect.x = _nodeRect.x + 20; _portraitLeftLabelRect.y = _nodeRect.y + 91;
				_portraitLeftNameRect.x = _nodeRect.x + 20; _portraitLeftNameRect.y = _nodeRect.y + 117;
                _textLabelRect.x   = _nodeRect.x + 20;  _textLabelRect.y    = _nodeRect.y + 143;
                _textInputRect.x   = _nodeRect.x + 20;  _textInputRect.y    = _nodeRect.y + 160;
                _audioLabelRect.x  = _nodeRect.x + 20;  _audioLabelRect.y   = _textInputRect.y + _textInputRect.height + 11;
                _audioInputRect.x  = _nodeRect.x + 120; _audioInputRect.y   = _textInputRect.y + _textInputRect.height + 9;
                _audioDelayRect.x  = _nodeRect.x + 294; _audioDelayRect.y   = _textInputRect.y + _textInputRect.height + 9;
                _autoChoiceLabelRect.x = _nodeRect.x + 20;  _autoChoiceLabelRect.y = _textInputRect.y + _textInputRect.height + 36;
                _autoChoicePopupRect.x = _nodeRect.x + 145; _autoChoicePopupRect.y = _textInputRect.y + _textInputRect.height + 35;
                _autoChoiceInputRect.x = _nodeRect.x + 235; _autoChoiceInputRect.y = _textInputRect.y + _textInputRect.height + 34;

                Vector2 startingPoint = new Vector2(_nodeRect.x, _nodeRect.y + 222 + _textInputRect.height);
                for(int i = 0; i < options.Count; i++) {
                    options[i].UpdatePositioning(startingPoint);
                    startingPoint.y += options[i].backgroundRect.height + 12;
                }

                _buttonAddOptionRect.x = _nodeRect.x + 115;
                _buttonAddOptionRect.y = _nodeRect.y + _nodeRect.height - 35;

                if (_autoChoiceOptions.Length != options.Count + 1) {           // Regenerate AutoChoice Dropdown
                    List<string> choices = new List<string>();
                    choices.Add("None");

                    for(int i = 1; i <= options.Count; i++) {
                        choices.Add(i.ToString());
                    }
                    _autoChoiceOptions = choices.ToArray();
                }

                _parentTree.RecalculateCompleteRectOriginal();

				//AddHeight (52);
            }
        }

        // CTors.
        public void Init(string title, Vector2 position, NodeGraphics nodeGraphics, DialogueTree parentTree) {
            base.Init(title, nodeGraphics, parentTree);

            // Creating Node in the middle of selection.
            nodeRect = new Rect(position.x - _initialNodeSize.x / 2, position.y - _initialNodeSize.y / 2, _initialNodeSize.x, _initialNodeSize.y);

            if(_createDefaultOption) {
                CreateOption();
                (options[0] as DialogueNodeOption).Text[0] = "Continue";
            }
        }


        protected override void CreateOption() {
            DialogueNodeOption option = ScriptableObject.CreateInstance<DialogueNodeOption>();
            option.Init(_nodeGraphics, this);

            AssetDatabase.AddObjectToAsset(option, _nodeGraphics.pathToAsset);
 
            options.Add(option);

            AddHeight(DialogueNodeOption.initialNodeOptionSize.y + 12);                 // This calls nodeRect, which updates positioning of options immediatelly.
        }

        public override void CreateLanguage() {
            base.CreateLanguage();
            Text.Add("");
            Audio.Add(null);
            AudioDelay.Add(0);
        }

        public override void DeleteLanguageAt(int index) {
            base.DeleteLanguageAt(index);
            Text.RemoveAt(index);
            Audio.RemoveAt(index);
            AudioDelay.RemoveAt(index);
        }

        public override void DeleteMarkedOptions() {
            for(int i = 0; i < _optionsForDeletion.Count; i++) {
                var option = _optionsForDeletion[i];
                var index  = options.FindIndex(x => x == option);
                if (autoChoiceIndex - 1 == index) {
                    autoChoiceIndex = 0;
                } else if (autoChoiceIndex - 1 > index) {
                    autoChoiceIndex--;
                }
                if(option.target != null) {
                    option.target.numConnectedTo--;
                    numConnectedFrom--;
                }
                option.target = null;
                options.Remove(option);
                DestroyImmediate(option, true);
                AddHeight(-option.backgroundRect.height - 12);
            }

            _optionsForDeletion.Clear();
        }

        // Drawing the node itself.
        public override void OnGUI() {
            base.OnGUI_Top();

            GUI.Label(_gEventLabelRect, "Global Event:", _nodeGraphics.skin.GetStyle("BodyLabel"));
            GUI.SetNextControlName("GlobalEventInput");
            globalEvent = EditorGUI.TextField(_gEventInputRect, globalEvent, _nodeGraphics.skin.GetStyle("BodyInput"));

            GUI.Label(_lEventLabelRect, "Local Event:", _nodeGraphics.skin.GetStyle("BodyLabel"));
            GUI.SetNextControlName("LocalEventInput");
            localEvent = EditorGUI.TextField(_lEventInputRect, localEvent, _nodeGraphics.skin.GetStyle("BodyInput"));

			GUI.SetNextControlName("Portraits");
			leftName = EditorGUI.TextField(_portraitLeftNameRect, leftName, _nodeGraphics.skin.GetStyle("BodyInput"));
			rightName = EditorGUI.TextField(_portraitRightNameRect, rightName, _nodeGraphics.skin.GetStyle("BodyInput"));


			isleftPersonSpeak = GUI.Toggle (_portraitLeftLabelRect, isleftPersonSpeak, "Left:");//, _nodeGraphics.skin.GetStyle ("BodyLabel"));  
			//GUI.Label(_portraitLeftLabelRect, , );

			//isleftPersonSpeak = GUI.Toggle (_portraitRightLabelRect, !isleftPersonSpeak, "Right:");

			GUI.Label(_portraitRightLabelRect, "Right:", _nodeGraphics.skin.GetStyle("BodyLabel"));

			leftPortrait = EditorGUI.ObjectField (_portraitLeftRect, leftPortrait, typeof(Sprite), true) as Sprite;
			rightPortrait = EditorGUI.ObjectField (_portraitRightRect, rightPortrait, typeof(Sprite), true) as Sprite;

            GUI.Label(_textLabelRect, "Text to Speak:", _nodeGraphics.skin.GetStyle("BodyLabel"));

            float height = _nodeGraphics.textAreaStyle.CalcHeight(new GUIContent(Text[currentLanguageIndex]), _textInputRect.width);
            _textInputRect.height = Mathf.Max(height, _textInputInitialHeight);
            if(_textInputRect.height != _previousTextHeight) {
                AddHeight(_textInputRect.height - _previousTextHeight);
                _previousTextHeight = _textInputRect.height;
            }

            GUI.SetNextControlName("TextInput");
            if(_allowCopyPasteNodeText) {
                Text[currentLanguageIndex] = EditorGUI.TextArea(_textInputRect, Text[currentLanguageIndex], _nodeGraphics.textAreaStyle);
            } else {
                Text[currentLanguageIndex] = GUI.TextArea(_textInputRect, Text[currentLanguageIndex], _nodeGraphics.textAreaStyle);
            }

            GUI.Label(_audioLabelRect, "Audio & Delay:", _nodeGraphics.skin.GetStyle("BodyLabel"));
            GUI.SetNextControlName("AudioInput");
            Audio[currentLanguageIndex] = EditorGUI.ObjectField(_audioInputRect, Audio[currentLanguageIndex], typeof(AudioClip), true) as AudioClip;
            GUI.SetNextControlName("AudioDelay");
            AudioDelay[currentLanguageIndex] = EditorGUI.FloatField(_audioDelayRect, AudioDelay[currentLanguageIndex]);

            GUI.Label(_autoChoiceLabelRect, "Auto-Choose Option:                  in             seconds.", _nodeGraphics.skin.GetStyle("BodyLabel"));
            autoChoiceIndex = EditorGUI.Popup(_autoChoicePopupRect, autoChoiceIndex, _autoChoiceOptions);
            autoChoiceDelay = EditorGUI.FloatField(_autoChoiceInputRect, autoChoiceDelay);

            base.OnGUI_Bottom();
        }
#endif
    }
}