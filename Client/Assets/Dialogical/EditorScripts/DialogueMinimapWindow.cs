#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace Dialogical {
    public class DialogueMinimapWindow : EditorWindow {
        // Settings
        private static readonly Color   _connectedColor     = new Color(0, 0, 1f, 0.4f);
        private static readonly Color   _connectedFromColor = new Color(0, 1f, 1f, 0.4f);
        private static readonly Color   _connectedToColor   = new Color(1f, 0, 0, 0.4f);
        private static readonly Color   _notConnectedColor  = new Color(1f, 1f, 0, 0.4f);
        private static readonly Color   _choiceNodeColor    = new Color(0, 1f, 0, 0.4f);
        private static readonly Color   _visibleAreaColor   = new Color(1, 1, 1, 0.3f);
        private static readonly Color   _canvasAreaColor    = new Color(1, 1, 1, 0.3f);

        // Repaint resolution (apply settings here from main view): 0 - very low (no repaint before focus), 1 - low (10fps), 2 - normal (100fps); Lower this if you're having performance issues.
        private static readonly int     _performanceMode    = 2;

        // Setup
        private Vector2                 _size = new Vector2(490, 100);
        private DialogueEditorWindow    _parent;
        private DialogueTree            _tree;

        private Rect _zoomOutButtonRect         = new Rect(5, 5, 20, 17);
        private Rect _fitWidthButtonRect        = new Rect(25, 5, 60, 17);
        private Rect _zoomInButtonRect          = new Rect(85, 5, 20, 17);
        private Rect _showNodeNamesRect         = new Rect(115, 5, 120, 17);
        private Rect _showNodeOptionsCountRect  = new Rect(235, 5, 120, 17);
        private Rect _showLegendRect            = new Rect(355, 5, 120, 17);

        private bool _showingNodeNames      = true;
        private bool _showingOptionsCount   = true;
        private bool _showingLegend         = false;

        private int     _numTilesX, _numTilesY;
        private Texture _background;
        private Rect    _bgOffsetRect;
        private Rect    _innerClip;                     // Rectangle that clips the drawable area.
        private Rect    _displayRect;                   // Rectangle that just fits a Scroll view inside Inner Clip.
        private Rect    _displayRectBars;               // Rectangle inside _displayRect wihtout the bars.
        private Rect    _displayRectHBar;               // Rectangle inside _displayRect only with a horizontal bar.
        private Rect    _displayRectVBar;               // Rectangle inside _displayRect only with a vertical bar.
        private Rect    _completeRect = new Rect();     // Rectangle that contains drawing for all the nodes in original proportions, in new dimensions. 
        private Rect    _completeRectNP = new Rect();   // Complete Rect in new proportions. Used for centering Complete Rect.
        private Rect    _legendGroup, _legendBox;
        private Color   _prevColor;
        private Vector2 _prevScreenResolution;
        private Vector2 _scrollPosition;
        private Rect    _temp, _tempNode;
        private bool    _tempIsChoice;
        private float   _completeRectScale;
        private GUIStyle _labelStyle;

        public void Init(DialogueEditorWindow parent, DialogueTree tree, Texture background) {
            _parent = parent;
            minSize = _size;
            _tree = tree;
            _background = background;
            _bgOffsetRect = new Rect(0,0,_background.width, _background.height);
            _tree.OnCompleteRectChange -= OnCompleteRectChange;         // Remove if it's alrady there.
            _tree.OnCompleteRectChange += OnCompleteRectChange;

            _labelStyle = new GUIStyle();
            _labelStyle.alignment = TextAnchor.MiddleCenter;
            _labelStyle.clipping = TextClipping.Overflow;
            _labelStyle.fontSize = 8;

            RecalculateTilingForBG();
            BestFit();
        }
        
        void OnDisable() {
            _tree.OnCompleteRectChange -= OnCompleteRectChange;
        }

        void Update() {
            if(_performanceMode == 2) Repaint();
        }

        void OnInspectorUpdate() {
            if(_performanceMode == 1) Repaint();
        }

        void OnGUI() {
            // Events
            switch(Event.current.type) {
                
                case EventType.mouseDrag:
                if(Event.current.button == 0 && _displayRectBars.Contains(Event.current.mousePosition)) {                         // LMB drags view
                    _parent.MoveAllNodes(- Event.current.delta / _completeRectScale );
                }
               
                if(Event.current.button == 1) {                         // RMB pans the minimap
                    _scrollPosition.x += Event.current.delta.x;
                    _scrollPosition.y += Event.current.delta.y;
                } 

                break;
            }

            // Background
            for(int x = 0; x < _numTilesX; x++) {
                for(int y = 0; y < _numTilesY; y++) {
                    _bgOffsetRect.x = x * _background.width;
                    _bgOffsetRect.y = y * _background.height;
                    GUI.DrawTexture(_bgOffsetRect, _background);
                }
            }

            // Top Left
            if(GUI.Button(_zoomOutButtonRect, "-", EditorStyles.miniButtonLeft))                            ZoomOut();
            if(GUI.Button(_fitWidthButtonRect, "Best Fit", EditorStyles.miniButtonMid))                     BestFit();
            if(GUI.Button(_zoomInButtonRect, "+", EditorStyles.miniButtonRight))                            ZoomIn();
            if(GUI.Button(_showNodeNamesRect, "Toggle Node Names", EditorStyles.miniButtonLeft))            _showingNodeNames = !_showingNodeNames;
            if(GUI.Button(_showNodeOptionsCountRect, "Toggle Options Count", EditorStyles.miniButtonMid))   _showingOptionsCount = !_showingOptionsCount;
            if(GUI.Button(_showLegendRect, "Toggle Legend", EditorStyles.miniButtonRight))                  _showingLegend = !_showingLegend;

            // Inner Clip
            GUI.BeginGroup(_innerClip);
            {
                _scrollPosition = GUI.BeginScrollView(_displayRect, _scrollPosition, _completeRectNP);
                _prevColor = GUI.backgroundColor;

                // Complete Rect
                GUI.backgroundColor = _canvasAreaColor;
                GUI.Box(_completeRect, "");

                GUI.BeginGroup(_completeRect);
                {
                    // Nodes & connections
                    foreach(var node in _tree.nodes) {
                        GUI.backgroundColor = DetermineColor(node);
                        _tempNode = ScaleRect(node.nodeRect);
                        _tempIsChoice = node is DialogueChoiceNode;
                        GUI.Box(_tempNode, "");
                        foreach(var option in node.options) {
                            if(option.target != null) Helpers.DrawConnection(_tempNode, ScaleRect(option.target.nodeRect), false, _tempIsChoice, true, _completeRectScale);
                        }

                        if(_showingNodeNames) {
                            GUI.Label(_tempNode, (_showingOptionsCount) ? node.title : node.title + "\n" + node.options.Count + ((node.options.Count > 1) ? " options" : " option"), _labelStyle);
                        }
                    }

                    // Visible Area Rect
                    GUI.backgroundColor = _visibleAreaColor;
                    GUI.Box(ScaleRect(_parent.visibleArea), "");
                }
                GUI.EndGroup();
                GUI.EndScrollView();

                GUI.backgroundColor = _prevColor;
            }
            GUI.EndGroup();

            // Lergend
            if(_showingLegend) {
                GUI.BeginGroup(_legendGroup);
                {
                    GUI.Box(_legendBox, "");
                    _prevColor = GUI.backgroundColor;
                    GUILayout.BeginArea(_legendBox);
                    {
                        LegendLine("Connected Node", _connectedColor);
                        LegendLine("Connected from only", _connectedFromColor);
                        LegendLine("Connected to only", _connectedToColor);
                        LegendLine("Not Connected", _notConnectedColor);
                        LegendLine("Choice Node", _choiceNodeColor);
                    }
                    GUILayout.EndArea();
                    GUI.backgroundColor = _prevColor;
                }
                GUI.EndGroup();
            }

            // Have we resized the window? If so, recalculate the tiling for Background.
            if(_prevScreenResolution.x != position.width || _prevScreenResolution.y != position.height) {
            RecalculateTilingForBG();
            }

            if(GUI.changed) {
                Repaint();
            }
        }

        void LegendLine(string text, Color _color) {
            GUILayout.Space(3);
            GUI.backgroundColor = _color;
            GUILayout.BeginHorizontal();
            GUILayout.Space(5);
            GUILayout.Box("", GUILayout.Width(15), GUILayout.Height(15));
            GUILayout.Label(text);
            GUILayout.EndHorizontal();
        }

        void RecalculateTilingForBG() {
            _numTilesX = Mathf.CeilToInt(position.width / _background.width);
            _numTilesY = Mathf.CeilToInt(position.height / _background.height);

            _innerClip       = new Rect(2,24,position.width-4, position.height-26);
            _displayRect     = new Rect(0, 0, _innerClip.width, _innerClip.height);
            _displayRectBars = new Rect(0, 0, _innerClip.width - 20, _innerClip.height - 20);
            _displayRectHBar = new Rect(0, 0, _innerClip.width - 20, _innerClip.height);
            _displayRectVBar = new Rect(0, 0, _innerClip.width, _innerClip.height - 20);

            _legendGroup = new Rect(position.width - 155, position.height - 115, 150, 110);
            _legendBox   = new Rect(0, 0, _legendGroup.width, _legendGroup.height);

            BestFitNP();
            CenterCompleteRect();
        }

        void BestFit() {
            float originalProportions = _tree.completeRectOriginal.width / _tree.completeRectOriginal.height;
            float newProportions = _displayRect.width / _displayRect.height;

            if (originalProportions >= newProportions) {                                        // Width is the limit.
                _completeRect.width = _displayRect.width * 0.98f;
                _completeRect.height = _displayRect.width / originalProportions;

            } else {                                                                            // Height is the limit.
                _completeRect.height = _displayRect.height * 0.98f;
                _completeRect.width = _completeRect.height * originalProportions;
            }

            _completeRectScale = _completeRect.width / _tree.completeRectOriginal.width;

            BestFitNP();
            CenterCompleteRect();
        }

        void BestFitNP() {
            Rect whichDisplayRect;

            if(_completeRect.width <= _displayRect.width && _completeRect.height <= _displayRect.height ) {
                whichDisplayRect = _displayRect;
            } else if(_completeRect.width > _displayRect.width) {
                whichDisplayRect = _displayRectVBar;
            } else if(_completeRect.height > _displayRect.height) {
                whichDisplayRect = _displayRectHBar;
            } else {
                whichDisplayRect = _displayRectBars;
            }

            _completeRectNP.width = Mathf.Max(_completeRect.width, whichDisplayRect.width);     //_completeRectNP never grows smaller then _displayRect, protecting the centering.
            _completeRectNP.height = Mathf.Max(_completeRect.height, whichDisplayRect.height);
        }

        void CenterCompleteRect() {
            _completeRect.x = (_completeRectNP.width - _completeRect.width) / 2;
            _completeRect.y = (_completeRectNP.height - _completeRect.height) / 2;
        }

        void ZoomOut() {
            _completeRect.width = 0.8f * _completeRect.width;
            _completeRect.height = 0.8f * _completeRect.height;
            _completeRectScale = _completeRect.width / _tree.completeRectOriginal.width;
            BestFitNP();
            CenterCompleteRect();
        }

        void ZoomIn() {
            _completeRect.width = 1.2f * _completeRect.width;
            _completeRect.height = 1.2f * _completeRect.height;
            _completeRectScale = _completeRect.width / _tree.completeRectOriginal.width;
            BestFitNP();
            CenterCompleteRect();
        }

        Rect ScaleRect(Rect original) {
            _temp.x = (original.x - _tree.completeRectOriginal.x) * _completeRectScale;
            _temp.y = (original.y - _tree.completeRectOriginal.y) * _completeRectScale;
            _temp.width = original.width * _completeRectScale;
            _temp.height = original.height * _completeRectScale; 

            return _temp;
        }

        Color DetermineColor(BaseNode node) {
            if(node.IsConnectedFrom() && node.IsConnectedTo()) {
                if(node is DialogueChoiceNode) return _choiceNodeColor;
                return _connectedColor;
            }
            else if(node.IsConnectedFrom()) return _connectedFromColor;
            else if(node.IsConnectedTo()) return _connectedToColor;
            else return _notConnectedColor;
        }

        void OnCompleteRectChange() {
            _completeRect.width = _completeRectScale * _tree.completeRectOriginal.width;
            _completeRect.height = _completeRectScale * _tree.completeRectOriginal.height;

            BestFitNP();
            CenterCompleteRect();
        }

        
    }
}
#endif
