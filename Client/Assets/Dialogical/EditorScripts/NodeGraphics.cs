using UnityEngine;
using System;

namespace Dialogical {
    [Serializable]
    public class NodeGraphics {
        public GUISkin      skin;
        public Texture[]    nodeSprite = new Texture[9];
        public Texture      nodeOptionTexture;
        public Texture      choiceOptionTexture;
        public Texture      nodeConnectedTexture;
        public Texture      choiceConnectedTexture;
        public Texture      nodeNotConnectedTexture;
        public GUIStyle     textAreaStyle = new GUIStyle();
        public string       pathToAsset;
        public Languages    languages;
    }
}