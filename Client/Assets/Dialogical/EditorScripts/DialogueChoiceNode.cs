using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;

namespace Dialogical {
    [Serializable]
    public class DialogueChoiceNode : InstantiableNode {
#if UNITY_EDITOR
        // Setup
        private static readonly Vector2 _initialNodeSize    = new Vector2(250, 85);

        [SerializeField]
        private Rect _nodeRt;
        [SerializeField]
        public override Rect nodeRect {
            get { return _nodeRt; }
            protected set {
                _nodeRt = value;
                _titleLabelRect.x = _nodeRt.x + 25; _titleLabelRect.y = _nodeRt.y + 8;
                _titleInputRect.x = _nodeRt.x + 80; _titleInputRect.y = _nodeRt.y + 10;

                Vector2 startingPoint = new Vector2(nodeRect.x, nodeRect.y + 40);
                for(int i = 0; i < options.Count; i++) {
                    options[i].UpdatePositioning(startingPoint);
                    startingPoint.y += options[i].backgroundRect.height + 12;
                }

                _buttonAddOptionRect.x = _nodeRt.x + 67;
                _buttonAddOptionRect.y = _nodeRt.y + _nodeRt.height - 35;

                _parentTree.RecalculateCompleteRectOriginal();
            }
        }

        // CTors.
        public void Init(string title, Vector2 position, NodeGraphics nodeGraphics, DialogueTree parentTree) {
            base.Init(title, nodeGraphics, parentTree);

            // Creating Node in the middle of selection.
            nodeRect = new Rect(position.x - _initialNodeSize.x / 2, position.y - _initialNodeSize.y / 2, _initialNodeSize.x, _initialNodeSize.y);
        }

        protected override void CreateOption() {
            DialogueChoiceOption option = ScriptableObject.CreateInstance<DialogueChoiceOption>();
            option.Init(_nodeGraphics, this);

            AssetDatabase.AddObjectToAsset(option, _nodeGraphics.pathToAsset);

            options.Add(option);

            AddHeight(DialogueChoiceOption.initialNodeOptionSize.y + 12);            // This calls nodeRect, which updates positioning of options immediatelly.
        }

        public override void DeleteMarkedOptions() {
            foreach(var option in _optionsForDeletion) {
                if(option.target != null) {
                    option.target.numConnectedTo--;
                    numConnectedFrom--;
                }
                option.target = null;
                options.Remove(option);
                DestroyImmediate(option, true);
                AddHeight(-option.backgroundRect.height - 12);
            }

            _optionsForDeletion.Clear();
        }

        // Drawing the node itself.
        public override void OnGUI() {
            base.OnGUI_Top();

            base.OnGUI_Bottom();
        }
#endif
    }
}