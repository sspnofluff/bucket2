using System;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace Dialogical {
    [Serializable]
    public class BaseNode : ScriptableObject {          // Abstract in fact, didn't do that due to Unity Serialization practices. 
        // Settings
        protected static readonly Color   _nodeTitleBaseColor = Color.grey, _nodeTitleSelectedColor = new Color(244,244,244);

        // Data
        [SerializeField]
        public string  title = "";
        [SerializeField]
        public int  numConnectedFrom;
        [SerializeField]
        public int  numConnectedTo;
        [SerializeField]
        protected DialogueTree _parentTree;

        // Selection & Display
        public      static BaseNode lastSelection;
        protected   static BaseNode _selection;
        public static BaseNode selection {
            get { return _selection; }
            set { _selection = value; if(_selection != null) lastSelection = _selection; }
        }

        protected NodeGraphics _nodeGraphics;
        protected int currentLanguageIndex;
        public bool showRedMarker;
        public Rect redMarkerRect;

        private Rect _nodeR;                             // Rectangle to draw the node. From top left point, to node height x width.
        public virtual Rect nodeRect {
            get { return _nodeR; }
            protected set { _nodeR = value; }
        }

        [SerializeField]
        public  List<DialogueBaseOption> options = new List<DialogueBaseOption>();

#if UNITY_EDITOR
        void OnEnable() {
            hideFlags = HideFlags.HideInHierarchy;
        }

        protected virtual void CreateOption() { }                                          // All 3 are abstract in fact.
        public virtual void MarkOptionForDeletion(DialogueBaseOption option) { }
        public virtual void DeleteMarkedOptions() { }

        // Languages
        public virtual void CreateLanguage() {
            foreach(var option in options) option.CreateLanguage();
        }

        public virtual void DeleteLanguageAt(int index) {
            if (currentLanguageIndex == index) {
                currentLanguageIndex = 0;
            } else if (currentLanguageIndex > index) {
                currentLanguageIndex--;
            }

            foreach(var option in options) option.DeleteLanguageAt(index);
        }

        public void SwitchToLanguage(int index) {
            currentLanguageIndex = index;
            foreach(var option in options) option.SwitchToLanguage(index);
        }

        public void SetNodeGraphics(NodeGraphics nodeGraphics) {
            _nodeGraphics = nodeGraphics;
            nodeRect = new Rect(nodeRect);
            foreach(var option in options) option.SetNodeGraphics(_nodeGraphics);
        }

        public virtual void OnGUI() { }

        public void Move(Vector2 delta) {
            nodeRect = new Rect(nodeRect.x + delta.x, nodeRect.y + delta.y, nodeRect.width, nodeRect.height);
        }

        public void AddHeight(float addedHeight) {
            nodeRect = new Rect(nodeRect.x, nodeRect.y, nodeRect.width, nodeRect.height + addedHeight);
        }

        public void RecalculateConnectedStatus() {
            numConnectedTo = 0;
            foreach(var node in _parentTree.nodes) {
                foreach(var option in node.options) {
                    if(option.target == this) {
                        numConnectedTo++;
                    }
                }
            }

            numConnectedFrom = 0;
            foreach(var opt in options) {
                if(opt.target != null) {
                    numConnectedFrom ++;
                }
            }
        }

        public virtual bool IsConnectedFrom() {
            return (numConnectedFrom > 0);
        }

        public virtual bool IsConnectedTo() {
            return (numConnectedTo > 0);
        }
#endif
    }


    [Serializable]
    public class InstantiableNode : BaseNode {                                          // Abstract in fact.
        // Setup
        protected static readonly bool _debugMode = false;

        // Positioning
        protected Rect  _titleLabelRect  = new Rect(0,0,50,50);
        protected Rect  _titleInputRect  = new Rect(0,0,200,20);
        protected Rect  _buttonAddOptionRect = new Rect(0,0,115,20);

#if UNITY_EDITOR
        // CTors.
        public void Init(string title, NodeGraphics nodeGraphics, DialogueTree parentTree) {
            _parentTree = parentTree;
            this.title = title;
            _nodeGraphics = nodeGraphics;

            for(int i = 0; i < nodeGraphics.languages.GetLanguagesCount(); i++) CreateLanguage();
        }

        protected bool InConnectedMode() {
            return (DialogueBaseOption.selection != null && DialogueBaseOption.inConnectingMode);
        }

        // Deletion
        protected List<DialogueBaseOption> _optionsForDeletion = new List<DialogueBaseOption>();
        public override void MarkOptionForDeletion(DialogueBaseOption option) {
            if(!_optionsForDeletion.Contains(option)) _optionsForDeletion.Add(option);
        }

        protected void OnGUI_Top () {
            if(Event.current.type == EventType.mouseDown && nodeRect.Contains(Event.current.mousePosition)) {          // We have to catch this event first so other controls don't swallow it.
                lastSelection = this;                                                                                  // Mark lastSelection so we know what option was last worked on (for Deletion purpose from Main Menu)
            }

            // Time to draw the node sprite.
            Helpers.DrawSprite(nodeRect, _nodeGraphics.nodeSprite);

            if(lastSelection == this) {
                _nodeGraphics.skin.label.normal.textColor = _nodeTitleSelectedColor;
            } else {
                _nodeGraphics.skin.label.normal.textColor = _nodeTitleBaseColor;
            }

            GUI.Label(_titleLabelRect, "Title:", _nodeGraphics.skin.label);
            GUI.SetNextControlName("TitleInput");
            title = EditorGUI.TextField(_titleInputRect, title, _nodeGraphics.skin.textField);
        }

        protected void OnGUI_Bottom() {
            // Draw GUI of options and proces their events first.
            foreach(var option in options) {
                option.OnGUI();
            }

            if(GUI.Button(_buttonAddOptionRect, "Add Option")) {
                CreateOption();
            }

            switch(Event.current.type) {
                case EventType.mouseDown:
                if(nodeRect.Contains(Event.current.mousePosition)) {    // Select this node if we clicked it
                    selection = this;
                    Event.current.Use();
                }

                if(selection == this && Event.current.button == 1) {
                    DialogueEditorWindow.deleteDialog = EditorWindow.GetWindow<DeleteDialog>(true, "Delete NODE");
                    DialogueEditorWindow.deleteDialog.Init4Node(this, _parentTree);
                }
                break;

                case EventType.mouseUp:                                 // If we released the mouse button...
                if(selection == this) {                                 // ... if this node was active selection, clear the selection
                    selection = null;
                    Event.current.Use();
                }
                                                                        // ... over this component while in connect mode, connect selection to this node and clear selection
                else if(InConnectedMode() && nodeRect.Contains(Event.current.mousePosition)) {
                    DialogueBaseOption.SetTargetOfSelection(this);
                    numConnectedTo++;
                    selection = null;
                    Event.current.Use();
                }
                break;

                case EventType.mouseDrag:
                if(selection == this) {                                 // If doing a mouse drag with this component selected drag the component                     
                    Move(Event.current.delta);
                    Event.current.Use();
                }
                break;
            }

            if(showRedMarker) {
                Color prevColor = GUI.backgroundColor;
                GUI.backgroundColor =  DeleteDialog.deletionColor;
                GUI.Box(redMarkerRect, "");
                GUI.backgroundColor = prevColor;
            }

            if (_optionsForDeletion.Count > 0) DeleteMarkedOptions();
        }

        void OnDestroy() {
            if(_debugMode) Debug.Log("Destroying node:" + title);

            foreach(var option in options) {
                DestroyImmediate(option, true);
            }
            options.Clear();

            _parentTree = null;
            _nodeGraphics = null;
        }
#endif
    }
}