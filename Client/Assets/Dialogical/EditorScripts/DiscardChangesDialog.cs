#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace Dialogical {
    public class DiscardChangesDialog : EditorWindow {

        // Setup
        private Vector2 _size = new Vector2(260,75);
        private Rect    _labelRect = new Rect (5,5,250,70), _okButtonRect = new Rect(25,50,90,17), _cancelButtonRect = new Rect(145,50,90,17);
        private string  _body = "Are you sure you want to discard changes\n to this tree and create a new tree?";
        private string  _okText = "OK";
        private string  _cancelText = "Cancel";

        public delegate void Action();
        private Action  _action;

        public void Init(Action action) {
            _action = action;
            minSize = maxSize = _size;
        }

        void OnGUI() {
            GUI.Label(_labelRect, _body);
            if(GUI.Button(_okButtonRect, _okText, EditorStyles.miniButton)) {
                _action();
                this.Close();
            }

            if(GUI.Button(_cancelButtonRect, _cancelText, EditorStyles.miniButton)) {
                this.Close();
            }
        }

        void OnDisable() {
            _action = null;
        }
    }
}
#endif