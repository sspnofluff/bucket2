#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace Dialogical {
    public static class Helpers {
#if UNITY_EDITOR
        //Setings
        private static Color _connectingColor = Color.red;
        private static Color _nodeConnectionColor = Color.blue;
        private static Color _choiceConnectionColor = new Color(0.03f,0.76f,0.03f);
        private static Color _minimapNodeLinkColor = new Color(0,0,1,0.5f);
        private static Color _minimapChoiceLinkColor = new Color(0.03f,0.76f,0.03f,0.7f);

        private static Rect p1 = new Rect(), p2 = new Rect(), p3 = new Rect(), p4 = new Rect(),
                            p5 = new Rect(), p6 = new Rect(), p7 = new Rect(), p8 = new Rect(), p9 = new Rect();
        public static void DrawSprite(Rect desiredPosition, Texture[] sprite) {
            // I only allow sprite expansion.
            if(sprite.Length != 9 || 
                (sprite[0].width + sprite[1].width + sprite[2].width + 1) > desiredPosition.width ||
                (sprite[3].width + sprite[4].width + sprite[5].width + 1) > desiredPosition.width ||
                (sprite[6].width + sprite[7].width + sprite[8].width + 1) > desiredPosition.width ||
                (sprite[0].height + sprite[3].height + sprite[6].height + 1) > desiredPosition.height ||
                (sprite[1].height + sprite[4].height + sprite[7].height + 1) > desiredPosition.height ||
                (sprite[2].height + sprite[5].height + sprite[8].height + 1) > desiredPosition.height
                )
                return;

            float flexibleWidth = desiredPosition.width - sprite[0].width - sprite[2].width;
            float flexibleHeight = desiredPosition.height - sprite[0].height - sprite[6].height;

            p1.x = desiredPosition.x; p1.y = desiredPosition.y; p1.width = sprite[0].width; p1.height = sprite[0].height;
            GUI.DrawTexture(p1, sprite[0]);

            p2.x = desiredPosition.x + sprite[0].width; p2.y = desiredPosition.y; p2.width = flexibleWidth; p2.height = sprite[1].height;
            GUI.DrawTexture(p2, sprite[1]);

            p3.x = desiredPosition.x + desiredPosition.width - sprite[2].width; p3.y = desiredPosition.y; p3.width = sprite[2].width; p3.height = sprite[2].height;
            GUI.DrawTexture(p3, sprite[2]);

            p4.x = desiredPosition.x; p4.y = desiredPosition.y + sprite[0].height; p4.width = sprite[3].width; p4.height = flexibleHeight;
            GUI.DrawTexture(p4, sprite[3]);

            p5.x = desiredPosition.x + sprite[3].width; p5.y = desiredPosition.y + sprite[1].height; p5.width = flexibleWidth; p5.height = flexibleHeight;
            GUI.DrawTexture(p5, sprite[4]);

            p6.x = desiredPosition.x + sprite[3].width + flexibleWidth; p6.y = desiredPosition.y + sprite[2].height; p6.width = sprite[5].width; p6.height = flexibleHeight;
            GUI.DrawTexture(p6, sprite[5]);
            
            p7.x = desiredPosition.x; p7.y = desiredPosition.y + sprite[0].height + flexibleHeight; p7.width = sprite[6].width; p7.height = sprite[6].height;
            GUI.DrawTexture(p7, sprite[6]);
            
            p8.x = desiredPosition.x + sprite[6].width; p8.y = desiredPosition.y + sprite[1].height + flexibleHeight; p8.width = flexibleWidth; p8.height = sprite[7].height;
            GUI.DrawTexture(p8, sprite[7]);
            
            p9.x = desiredPosition.x + sprite[6].width + flexibleWidth; p9.y = desiredPosition.y + sprite[2].height + flexibleHeight; p9.width = sprite[8].width; p9.height = sprite[8].height;
            GUI.DrawTexture(p9, sprite[8]);
        }

        private static Vector3 _dct_startPoint = new Vector3(), _dct_endPoint = new Vector3();
        public static void DrawConnection(Rect fromRect, Rect toRect, bool inConnectionMode, bool fromChoiceNode, bool minimap = false, float minimapScale = 0.2f) {
            _dct_startPoint.x = fromRect.x + fromRect.width;
            _dct_startPoint.y = fromRect.y + fromRect.height / 2;
            _dct_endPoint.x = toRect.x + 1;
            _dct_endPoint.y = toRect.y + toRect.height / 2;

            if(!minimap) {
                Handles.DrawBezier(_dct_startPoint, _dct_endPoint, _dct_startPoint + Vector3.right * 50f, _dct_endPoint + Vector3.left * 50f,
                    inConnectionMode ? _connectingColor : ((!fromChoiceNode)? _nodeConnectionColor : _choiceConnectionColor), null, 4.0f);
            } else {
                Handles.DrawBezier(_dct_startPoint, _dct_endPoint, _dct_startPoint + Vector3.right * minimapScale * 50f, _dct_endPoint + Vector3.left * minimapScale * 50f,
                    (! fromChoiceNode)? _minimapNodeLinkColor : _minimapChoiceLinkColor, null, 2.0f);
            }
        }
#endif
    }

}
