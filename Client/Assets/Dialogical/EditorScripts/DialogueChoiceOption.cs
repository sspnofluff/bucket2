using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;

namespace Dialogical {
    [Serializable]
    public class DialogueChoiceOption : DialogueBaseOption {
        // Setup
        public  static readonly Vector2 initialNodeOptionSize   = new Vector2(210, 20);

        // Data
        [SerializeField]
        public  string condition = "";

#if UNITY_EDITOR
        // Positioning
        [SerializeField]
        private Rect _conditionLabelRect    = new Rect(0,0,50,20);
        [SerializeField]
        private Rect _contionInputRect      = new Rect(0,0,130,20);

        //CTors.
        public override void Init(NodeGraphics nodeOptionGraphics, BaseNode parent) {
            base.Init(nodeOptionGraphics, parent);
            backgroundRect = new Rect(0, 0, initialNodeOptionSize.x, initialNodeOptionSize.y);
        }

        public override void UpdatePositioning(Vector2 startingPoint) {
            backgroundRect.x = startingPoint.x + 10; backgroundRect.y = startingPoint.y + 51;
            _conditionLabelRect.x = startingPoint.x + 20; _conditionLabelRect.y = startingPoint.y + 55;
            _contionInputRect.x = startingPoint.x + 90; _contionInputRect.y = startingPoint.y + 54;
            _nodeConnectorRect.x = backgroundRect.x + backgroundRect.width;
            _nodeConnectorRect.y = backgroundRect.y;
        }

        private Rect _ongui_mouseP = new Rect(0,0,0,0);
        public override void OnGUI() {
            GUI.DrawTexture(backgroundRect, _nodeOptionGraphics.choiceOptionTexture);

            if(Event.current.type == EventType.mouseDown && backgroundRect.Contains(Event.current.mousePosition)) {     // We have to catch this event first so other controls don't swallow it.
                lastSelection = this;                                                                                   // Mark lastSelection so we know what option was last worked on (for Deletion from Main Menu)
            }

            GUI.Label(_conditionLabelRect, "223Condition:", _nodeOptionGraphics.skin.GetStyle("BodyLabel"));
            GUI.SetNextControlName("ConditionInput");
            condition = EditorGUI.TextField(_contionInputRect, condition, _nodeOptionGraphics.skin.GetStyle("BodyInput"));

            GUI.DrawTexture(_nodeConnectorRect, (target != null) ? _nodeOptionGraphics.choiceConnectedTexture : _nodeOptionGraphics.nodeNotConnectedTexture);

            // Event Handling
            switch(Event.current.type) {

                case EventType.mouseDown:
                if(_nodeConnectorRect.Contains(Event.current.mousePosition)) {      // Work with connection mode
                    selection = this;

                    if(Event.current.button == 0) inConnectingMode = true;
                    if(Event.current.button == 1) {                                // Deletes connection: RMB on Connector Point.
                        if(target != null) {
                            target.numConnectedTo--;
                            _parent.numConnectedFrom--;
                        }
                        target = null;
                    }

                    Event.current.Use();
                } else if(Event.current.button == 1 && backgroundRect.Contains(Event.current.mousePosition)) {          // RMB on node option deletes it.
                    DialogueEditorWindow.deleteDialog = EditorWindow.GetWindow<DeleteDialog>(true, "Delete OPTION");
                    DialogueEditorWindow.deleteDialog.Init4Option(this, _parent);

                    Event.current.Use();
                }

                break;

                case EventType.mouseDrag:
                if(selection == this) {                                  // If doing a mouse drag with this component selected, and in connect mode...
                    Event.current.Use();                                 // ... just use the event as we'll be painting the new connection
                }
                break;

                case EventType.mouseUp:                                  // Prevent forwarding of Mouse Up event on the control itself.
                if(selection == this && _nodeConnectorRect.Contains(Event.current.mousePosition)) {
                    SetTargetOfSelection(null);
                    Event.current.Use();
                }
                break;

                case EventType.repaint:
                if(selection == this && inConnectingMode) {              // New connection originates from this node.
                    _ongui_mouseP.x = Event.current.mousePosition.x;
                    _ongui_mouseP.y = Event.current.mousePosition.y;
                    Helpers.DrawConnection(_nodeConnectorRect, _ongui_mouseP, true, true);
                }
                break;

            }

            if(showRedMarker) {
                Color prevColor = GUI.backgroundColor;
                GUI.backgroundColor = DeleteDialog.deletionColor;
                GUI.Box(redMarkerRect, "");
                GUI.backgroundColor = prevColor;
            }
        }
#endif
    }
}