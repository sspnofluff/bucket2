﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class TempMeshSaver : MonoBehaviour {

	#if UNITY_EDITOR
	[ContextMenu("Do")]
	public void Do()
	{
		MeshFilter MF = transform.GetComponent<MeshFilter> ();
		string pathParent = AssetDatabase.GetAssetPath(PrefabUtility.GetPrefabParent(transform.gameObject));
		int count = pathParent.LastIndexOf('/');
		//string _folder = pathParent.Substring(0, count);
		string pathNew = "Assets/Art/" + gameObject.name;

		string meshMainPath = pathNew + "Mesh.asset";
		CreateOrReplaceAsset(MF.sharedMesh, meshMainPath);
	}

	T CreateOrReplaceAsset<T> (T asset, string path) where T:UnityEngine.Object{
		T existingAsset = AssetDatabase.LoadAssetAtPath<T>(path);

		if (existingAsset == null){
			AssetDatabase.CreateAsset(asset, path);
			existingAsset = asset;
		}
		else{
			EditorUtility.CopySerialized(asset, existingAsset);
		}

		return existingAsset;
	}
	#endif
}
