// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

// Upgrade NOTE: unity_Scale shader variable was removed; replaced 'unity_Scale.w' with '1.0'

// uncomment when you've got problems with DX9 compilation under Unity5
#define DISABLE_DX9_HLSL_BRANCHING_LEVEL1
// if above LEVEL1 disable is not enough you can uncomment below one as well
#define DISABLE_DX9_HLSL_BRANCHING_LEVEL2

#ifdef RTP_STANDALONE
	#define LIGHTDIR_UNAVAILABLE
#endif

#if !defined(OVERWRITE_RTPBASE_DEFINES)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// you might want to tweak below defines by hand (commenting/uncommenting, but it's recommended to leave it for RTP_manager)
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// (we don't tessellate terrain in geom blend base)
#if !defined(COLOR_EARLY_EXIT) && defined(UNITY_CAN_COMPILE_TESSELLATION)

	#define TESSELLATION
#endif

#if defined(UNITY_PI) && !defined(_4LAYERS) && defined(FAR_ONLY)
	#undef FAR_ONLY
#endif


#define RTP_DISTANCE_ONLY_UV_BLEND
// usage of normals from blended layer at far distance
#define RTP_NORMALS_FOR_REPLACE_UV_BLEND
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// UV blend routing defines section
//
// DON'T touch defines below... (unless you know exactly what you're doing) - lines 31-53
#if !defined(_4LAYERS) || defined(RTP_USE_COLOR_ATLAS)
	#define UV_BLEND_SRC_0 (tex2Dlod(_SplatAtlasA, float4(uvSplat01M.xy, _MixMipActual.xx)).rgba)
	#define UV_BLEND_SRC_1 (tex2Dlod(_SplatAtlasA, float4(uvSplat01M.zw, _MixMipActual.yy)).rgba)
	#define UV_BLEND_SRC_2 (tex2Dlod(_SplatAtlasA, float4(uvSplat23M.xy, _MixMipActual.zz)).rgba)
	#define UV_BLEND_SRC_3 (tex2Dlod(_SplatAtlasA, float4(uvSplat23M.zw, _MixMipActual.ww)).rgba)
	#define UV_BLEND_SRC_4 (tex2Dlod(_SplatAtlasB, float4(uvSplat01M.xy, _MixMipActual.xx)).rgba)
	#define UV_BLEND_SRC_5 (tex2Dlod(_SplatAtlasB, float4(uvSplat01M.zw, _MixMipActual.yy)).rgba)
	#define UV_BLEND_SRC_6 (tex2Dlod(_SplatAtlasB, float4(uvSplat23M.xy, _MixMipActual.zz)).rgba)
	#define UV_BLEND_SRC_7 (tex2Dlod(_SplatAtlasB, float4(uvSplat23M.zw, _MixMipActual.ww)).rgba)
#else
	#define UV_BLEND_SRC_0 (tex2Dlod(_SplatA0, float4(uvSplat01M.xy, _MixMipActual.xx)).rgba)
	#define UV_BLEND_SRC_1 (tex2Dlod(_SplatA1, float4(uvSplat01M.zw, _MixMipActual.yy)).rgba)
	#define UV_BLEND_SRC_2 (tex2Dlod(_SplatA2, float4(uvSplat23M.xy, _MixMipActual.zz)).rgba)
	#define UV_BLEND_SRC_3 (tex2Dlod(_SplatA3, float4(uvSplat23M.zw, _MixMipActual.ww)).rgba)
#endif
#define UV_BLENDMIX_SRC_0 (_MixScale0123.x)
#define UV_BLENDMIX_SRC_1 (_MixScale0123.y)
#define UV_BLENDMIX_SRC_2 (_MixScale0123.z)
#define UV_BLENDMIX_SRC_3 (_MixScale0123.w)
#define UV_BLENDMIX_SRC_4 (_MixScale4567.x)
#define UV_BLENDMIX_SRC_5 (_MixScale4567.y)
#define UV_BLENDMIX_SRC_6 (_MixScale4567.z)
#define UV_BLENDMIX_SRC_7 (_MixScale4567.w)
// As we've got defined some shader parts, you can tweak things in following lines
////////////////////////////////////////////////////////////////////////

// for example, when you'd like layer 3 to be source for uv blend on layer 0 you'd set it like this:
//   #define UV_BLEND_ROUTE_LAYER_0 UV_BLEND_SRC_3
// HINT: routing one layer into all will boost performance as only 1 additional texture fetch will be performed in shader (instead of up to 8 texture fetches in default setup)
//
#define UV_BLEND_ROUTE_LAYER_0 UV_BLEND_SRC_0
#define UV_BLEND_ROUTE_LAYER_1 UV_BLEND_SRC_1
#define UV_BLEND_ROUTE_LAYER_2 UV_BLEND_SRC_2
#define UV_BLEND_ROUTE_LAYER_3 UV_BLEND_SRC_3
#define UV_BLEND_ROUTE_LAYER_4 UV_BLEND_SRC_4
#define UV_BLEND_ROUTE_LAYER_5 UV_BLEND_SRC_5
#define UV_BLEND_ROUTE_LAYER_6 UV_BLEND_SRC_6
#define UV_BLEND_ROUTE_LAYER_7 UV_BLEND_SRC_7
// below routing shiould be exactly the same as above
#define UV_BLENDMIX_ROUTE_LAYER_0 UV_BLENDMIX_SRC_0
#define UV_BLENDMIX_ROUTE_LAYER_1 UV_BLENDMIX_SRC_1
#define UV_BLENDMIX_ROUTE_LAYER_2 UV_BLENDMIX_SRC_2
#define UV_BLENDMIX_ROUTE_LAYER_3 UV_BLENDMIX_SRC_3
#define UV_BLENDMIX_ROUTE_LAYER_4 UV_BLENDMIX_SRC_4
#define UV_BLENDMIX_ROUTE_LAYER_5 UV_BLENDMIX_SRC_5
#define UV_BLENDMIX_ROUTE_LAYER_6 UV_BLENDMIX_SRC_6
#define UV_BLENDMIX_ROUTE_LAYER_7 UV_BLENDMIX_SRC_7
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define COLOR_MAP
// if not defined global color map will be blended (lerp)
//#define COLOR_MAP_BLEND_MULTIPLY
// advanced colormap blending per layer
//#define ADV_COLOR_MAP_BLENDING

// global normal map 
#ifndef TESSELLATION
//#define RTP_NORMALGLOBAL
#endif

// global trees/shadow map - used with Terrain Composer / World Composer by Nathaniel Doldersum
//#define RTP_TREESGLOBAL

// global ambient emissive map
//#define RTP_AMBIENT_EMISSIVE_MAP

// heightblend fake AO
#define RTP_HEIGHTBLEND_AO

#define RTP_HIDE_EDGE_HARD_CROSSPASS

// define for harder heightblend edges
#define SHARPEN_HEIGHTBLEND_EDGES_PASS1
#define SHARPEN_HEIGHTBLEND_EDGES_PASS2

#define RTP_47SHADING_SIMPLE

// fog type (in Unity 5 it's defined via shader variant)
// RTP_FOG_EXP2, RTP_FOG_EXPONENTIAL, RTP_FOG_LINEAR
#ifndef UNITY_PI
#define RTP_FOG_EXP2
#endif

#define RTP_DEFERRED_PBL_NORMALISATION

#define RTP_SKYSHOP_SKY_ROTATION

#define IBL_HDR_RGBM

#define SS_USE_BUMPMAPS

#else
	// tessellation for standalones
	#if defined(TESSELLATION) && !defined(UNITY_CAN_COMPILE_TESSELLATION)
		#undef TESSELLATION
	#endif
	#if defined(TESSELLATION) && defined(RTP_NORMALGLOBAL)
		#undef RTP_NORMALGLOBAL
	#endif
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// we're using mapped shadows for best performance (available only in 4 layers mode when using atlas)
// not implemented yet
//#define RTP_MAPPED_SHADOWS

#if defined(WNORMAL_COVERAGE_XZ_Ypos_Yneg) || defined(WNORMAL_COVERAGE_X_Z_YposYneg)
	#ifdef USE_2_LAYERS_ONLY
		#undef USE_2_LAYERS_ONLY
	#endif
	#ifndef USE_3_LAYERS_ONLY
		#define USE_3_LAYERS_ONLY
	#endif
#endif
#if defined(WNORMAL_COVERAGE_X_Z_Ypos_Yneg)
	#ifdef USE_2_LAYERS_ONLY
		#undef USE_2_LAYERS_ONLY
	#endif
	#ifdef USE_3_LAYERS_ONLY
		#undef USE_3_LAYERS_ONLY
	#endif
#endif
#if defined(WNORMAL_COVERAGE_XZ_YposYneg)
	#ifndef USE_2_LAYERS_ONLY
		#define USE_2_LAYERS_ONLY
	#endif
	#ifdef USE_3_LAYERS_ONLY
		#undef USE_3_LAYERS_ONLY
	#endif
#endif


#if !defined(_4LAYERS) && defined(USE_2_LAYERS_ONLY)
	#undef USE_2_LAYERS_ONLY
#endif
#if !defined(_4LAYERS) && defined(USE_3_LAYERS_ONLY)
	#undef USE_3_LAYERS_ONLY
#endif

#ifdef _4LAYERS
	#ifdef RTP_HARD_CROSSPASS
		#undef RTP_HARD_CROSSPASS
	#endif
#endif

#if defined(COLOR_EARLY_EXIT) || !defined(APPROX_TANGENTS)
	#ifdef RTP_CUT_HOLES
		#undef RTP_CUT_HOLES
	#endif
#endif

#ifdef COLOR_MAP
	#ifdef GEOM_BLEND
		#define  COLOR_DAMP_VAL (global_color_value.a*(1-IN.color.a))
	#else
		#define  COLOR_DAMP_VAL (global_color_value.a)
	#endif
#else
	#define  COLOR_DAMP_VAL 1
#endif

#if defined(UNITY_PASS_SHADOWCASTER)
	// turn off higher shading
	#if defined(RTP_POM_SHADING_HI)
		#undef RTP_POM_SHADING_HI
	#elif defined(RTP_POM_SHADING_MED)
		#undef RTP_POM_SHADING_MED
	#elif defined(RTP_POM_SHADING_LO)
		#undef RTP_POM_SHADING_LO
	#endif
	
	// turn off snow
	#if defined(RTP_SNOW)
		#undef RTP_SNOW
	#endif
	
	#if defined(RTP_WETNESS)
		#undef RTP_WETNESS
	#endif
	
	#if defined(RTP_CAUSTICS)
		#undef RTP_CAUSTICS
	#endif
	
	// turn off reflection additional stuff that needs INTERNAL_DATA to compute world normal / reflection
	#if defined(RTP_REFLECTION)
		#undef RTP_REFLECTION
	#endif
	
	#if defined(RTP_IBL_SPEC)
		#undef RTP_IBL_SPEC
	#endif
	
	#if defined(RTP_IBL_DIFFUSE)
		#undef RTP_IBL_DIFFUSE
	#endif
	
	#if defined(RTP_SKYSHOP_SYNC)
		#undef RTP_SKYSHOP_SYNC
	#endif
#endif


// dodefiniuj RTP_USE_COLOR_ATLAS dla triplanar w 8layers mode
#ifndef _4LAYERS
	#ifdef RTP_TRIPLANAR
		#ifndef RTP_USE_COLOR_ATLAS
			#define RTP_USE_COLOR_ATLAS
		#endif
	#endif
#endif



// not used anymore in RTP3.1 due to new functionality that breaks it (replacement UV blend that depends on far distance)
// (przed poniższym define musi być przynajmniej jedna spacje aby LOD manager tego nie psuł)
//#ifdef RTP_SIMPLE_SHADING
//	#ifndef RTP_DISTANCE_ONLY_UV_BLEND
//         #define RTP_DISTANCE_ONLY_UV_BLEND
//	#endif
//#endif

#define GETrtp_snow_TEX rtp_snow_color_tex.rgb=csnow.rgb;rtp_snow_gloss=lerp(saturate(csnow.a*rtp_snow_gloss*2), 1, saturate((rtp_snow_gloss-0.5)*2));

#if defined(DISABLE_DX9_HLSL_BRANCHING_LEVEL2) && defined(RTP_HARD_CROSSPASS)
#undef RTP_HARD_CROSSPASS
#endif

// wyłącz kolor selction kiedy zabraknie samplerów
#if defined(_4LAYERS) && (!defined(RTP_USE_COLOR_ATLAS) || defined(RTP_MAPPED_SHADOWS))
#if ( defined(RTP_SNOW) && !defined(UNITY_PASS_SHADOWCASTER) ) && (defined(RTP_VERTICAL_TEXTURE) && !defined(RTP_VERTALPHA_CAUSTICS)) && ( defined(RTP_WETNESS) && !defined(UNITY_PASS_SHADOWCASTER) ) && defined(RTP_WET_RIPPLE_TEXTURE)
	#ifdef RTP_SNW_CHOOSEN_LAYER_COLOR_4
		#undef RTP_SNW_CHOOSEN_LAYER_COLOR_4
	#endif 
	#ifdef RTP_SNW_CHOOSEN_LAYER_COLOR_5
		#undef RTP_SNW_CHOOSEN_LAYER_COLOR_5
	#endif 
	#ifdef RTP_SNW_CHOOSEN_LAYER_COLOR_6
		#undef RTP_SNW_CHOOSEN_LAYER_COLOR_6
	#endif 
	#ifdef RTP_SNW_CHOOSEN_LAYER_COLOR_7
		#undef RTP_SNW_CHOOSEN_LAYER_COLOR_7
	#endif 
#endif
#endif
// wyłącz wet ripple kiedy mamy inne opcje wszystkie powlaczane
#if defined(_4LAYERS) && defined(RTP_MAPPED_SHADOWS) && (defined(RTP_VERTICAL_TEXTURE) && !defined(RTP_VERTALPHA_CAUSTICS)) && ( defined(RTP_WETNESS) && !defined(UNITY_PASS_SHADOWCASTER) ) && defined(RTP_WET_RIPPLE_TEXTURE)
	#undef RTP_WET_RIPPLE_TEXTURE
#endif
// potrzebujemy miejsca na dodatkowe samplery cieni
#if defined(RTP_MAPPED_SHADOWS)
	#if !defined(_4LAYERS)
		#undef RTP_MAPPED_SHADOWS
	#else
		//#define RTP_USE_COLOR_ATLAS
	#endif
#endif

#if ( defined(RTP_WETNESS) && !defined(UNITY_PASS_SHADOWCASTER) ) || defined(RTP_AMBIENT_EMISSIVE_MAP) || (defined(COLOR_MAP) && defined(ADV_COLOR_MAP_BLENDING))
	#define NEED_LOCALHEIGHT
#endif

#ifndef RTP_PBL_FRESNEL
	#define pbl_fresnel_term 1
#endif
#ifndef RTP_PBL_VISIBILITY_FUNCTION
	#define pbl_visibility_term 1
#endif
	
#if defined(SHADER_API_D3D11) 
CBUFFER_START(rtpConstants)
#endif

#ifdef TEX_SPLAT_REDEFINITION
	// texture samplers redefinitions (due to Unity bug that makes Unity4 terrain material unusable)
	#define _Control1 _Control
	#define _ColorMapGlobal _Splat0
	#define _NormalMapGlobal _Splat1
	#define _Control2 _Splat2
	#if defined(RTP_AMBIENT_EMISSIVE_MAP)
		#define _AmbientEmissiveMapGlobal _Splat2
	#else
		#define _TreesMapGlobal _Splat2
	#endif
	#define _BumpMapGlobal _Splat3
	sampler2D _Control;
#else
	sampler2D _Control, _Control1, _Control2;
#endif

#ifdef RTP_STANDALONE
float4 _TERRAIN_PosSize;
#endif
float3 terrainTileSize;


#include "Assets/VacuumShaders/Curved World/Shaders/cginc/CurvedWorld_Base.cginc"

sampler2D _SplatAtlasA, _SplatAtlasB;
sampler2D _SplatA0, _SplatA1, _SplatA2, _SplatA3;
sampler2D _SplatB0, _SplatB1, _SplatB2, _SplatB3;
sampler2D _BumpMap01, _BumpMap23, _BumpMap45, _BumpMap67;

float2 _terrain_size;
float _BumpMapGlobalScale;

//float _GlobalColorMapSaturationByPerlin;
float RTP_DeferredAddPassSpec;

float4 _MixScale0123, _MixBlend0123;
float4 _MixScale4567, _MixBlend4567;

float4  _LayerBrightness0123, _LayerSaturation0123, _LayerBrightness2Spec0123, _LayerAlbedo2SpecColor0123;
float4  _LayerBrightness4567, _LayerSaturation4567, _LayerBrightness2Spec4567, _LayerAlbedo2SpecColor4567;
float4 _MixSaturation0123, _MixBrightness0123, _MixReplace0123;
float4 _MixSaturation4567, _MixBrightness4567, _MixReplace4567;

float4 _Spec0123;
float4 _Spec4567;
float4 _MIPmult0123;
float4 _MIPmult4567;

sampler2D _TERRAIN_HeightMap, _TERRAIN_HeightMap2;
float4 _TERRAIN_HeightMap_TexelSize;
float4 _TERRAIN_HeightMap2_TexelSize;
float4 _SplatAtlasA_TexelSize;
float4 _SplatAtlasB_TexelSize;
float4 _SplatA0_TexelSize;
float4 _BumpMapGlobal_TexelSize;
float4 _TERRAIN_ReliefTransform;
float _TERRAIN_ReliefTransformTriplanarZ;
float _TERRAIN_DIST_STEPS;
float _TERRAIN_WAVELENGTH;

float _blend_multiplier;

float _TERRAIN_ExtrudeHeight;
float _TERRAIN_LightmapShading;

float _TERRAIN_SHADOW_STEPS;
float _TERRAIN_WAVELENGTH_SHADOWS;
//float _TERRAIN_SHADOW_SMOOTH_STEPS; // not used sice RTP3.2d
float _TERRAIN_SelfShadowStrength;
float _TERRAIN_ShadowSmoothing;
float _TERRAIN_ShadowSoftnessFade; // new in RTP3.2d

float rtp_mipoffset_color;
float rtp_mipoffset_bump;
float rtp_mipoffset_height;
float rtp_mipoffset_superdetail;
float rtp_mipoffset_flow;
float rtp_mipoffset_ripple;
float rtp_mipoffset_globalnorm;
float rtp_mipoffset_caustics;

half3 TERRAIN_VertexColorBlend;

///////////////////////////////////////////

float _TERRAIN_distance_start;
float _TERRAIN_distance_transition;

float _TERRAIN_distance_start_bumpglobal;
float _TERRAIN_distance_transition_bumpglobal;
float rtp_perlin_start_val;
float4 _BumpMapGlobalStrength0123;
float4 _BumpMapGlobalStrength4567;

// used by TerraVol vertex interpolation		
float _TERRAIN_distance_start_vertexinterpolation;
float _TERRAIN_distance_transition_vertexinterpolation;

sampler2D _NormalMapGlobal;

float4 _NormalMapGlobal_TexelSize;
sampler2D _TreesMapGlobal;
sampler2D _AmbientEmissiveMapGlobal;
float _AmbientEmissiveMultiplier;
float _AmbientEmissiveRelief;
float4 _TERRAIN_trees_shadow_values;
float4 _TERRAIN_trees_pixel_values;

float _RTP_MIP_BIAS;

float4 PER_LAYER_HEIGHT_MODIFIER0123;
float4 PER_LAYER_HEIGHT_MODIFIER4567;

float _SuperDetailTiling;
float4 _SuperDetailStrengthMultA0123;
float4 _SuperDetailStrengthMultA4567;
float4 _SuperDetailStrengthMultB0123;
float4 _SuperDetailStrengthMultB4567;
float4 _SuperDetailStrengthNormal0123;
float4 _SuperDetailStrengthNormal4567;

float4 _SuperDetailStrengthMultASelfMaskNear0123;
float4 _SuperDetailStrengthMultASelfMaskNear4567;
float4 _SuperDetailStrengthMultASelfMaskFar0123;
float4 _SuperDetailStrengthMultASelfMaskFar4567;
float4 _SuperDetailStrengthMultBSelfMaskNear0123;
float4 _SuperDetailStrengthMultBSelfMaskNear4567;
float4 _SuperDetailStrengthMultBSelfMaskFar0123;
float4 _SuperDetailStrengthMultBSelfMaskFar4567;

float _TransitionSlider;

float4 _VerticalTexture_TexelSize;
sampler2D _VerticalTexture;
float _VerticalTextureTiling;
float _VerticalTextureGlobalBumpInfluence;
float4 _VerticalTexture0123;
float4 _VerticalTexture4567;

float rtp_global_color_brightness_to_snow;
float rtp_snow_slope_factor;
float rtp_snow_edge_definition;
float4 rtp_snow_strength_per_layer0123;
float4 rtp_snow_strength_per_layer4567;
float rtp_snow_height_treshold;
float rtp_snow_height_transition;
float rtp_snow_fresnel;
float rtp_snow_diff_fresnel;
float rtp_snow_IBL_DiffuseStrength;
float rtp_snow_IBL_SpecStrength;

fixed4 rtp_snow_color;
float rtp_snow_gloss;
float rtp_snow_specular;
float rtp_snow_deep_factor;


float _Phong;
float4 _QOffset;
float _Dist;
float _DistanceMin;
float _DistanceMax;
float _EdgeLength;
float _DispOffset;
float4 _TessStrenght0123;
float4 _TessStrenght4567;


///////////////////////////////////////////////////////////////////////////////////////////////
// lighting & IBL
///////////////////////////////////////////////////////////////////////////////////////////////
half3 rtp_customAmbientCorrection;

#if defined(RTP_STANDALONE)
	float RTP_BackLightStrength;
	float RTP_ReflexLightDiffuseSoftness;
	float RTP_ReflexLightSpecularity;
	
	// pixel trees / emissive map shadows
	float _shadow_distance_start;
	float _shadow_distance_transition;
	float _shadow_value;
#else
	#define RTP_BackLightStrength RTP_LightDefVector.x
	#define RTP_ReflexLightDiffuseSoftness RTP_LightDefVector.y
	#define RTP_ReflexLightSpecSoftness RTP_LightDefVector.z
	#define RTP_ReflexLightSpecularity RTP_LightDefVector.w
	float4 RTP_LightDefVector;
#endif
half4 RTP_ReflexLightDiffuseColor1;
half4 RTP_ReflexLightDiffuseColor2;
half4 RTP_ReflexLightSpecColor;

samplerCUBE _CubemapDiff;
samplerCUBE _CubemapSpec;
float4x4	_SkyMatrix;// set globaly by skyshop

// PBL / IBL
half4 RTP_gloss2mask0123, RTP_gloss2mask4567;
half4 RTP_gloss_mult0123, RTP_gloss_mult4567;
half4 RTP_gloss_shaping0123, RTP_gloss_shaping4567;
half4 RTP_Fresnel0123, RTP_Fresnel4567;
half4 RTP_FresnelAtten0123, RTP_FresnelAtten4567;
half4 RTP_DiffFresnel0123, RTP_DiffFresnel4567;
// IBL
half4 RTP_IBL_bump_smoothness0123, RTP_IBL_bump_smoothness4567;
half4 RTP_IBL_DiffuseStrength0123, RTP_IBL_DiffuseStrength4567;
half4 RTP_IBL_SpecStrength0123, RTP_IBL_SpecStrength4567;

half4 TERRAIN_WaterIBL_SpecWetStrength0123, TERRAIN_WaterIBL_SpecWetStrength4567;
half4 TERRAIN_WaterIBL_SpecWaterStrength0123, TERRAIN_WaterIBL_SpecWaterStrength4567;

half TERRAIN_IBL_DiffAO_Damp;
half TERRAIN_IBLRefl_SpecAO_Damp;
//
///////////////////////////////////////////////////////////////////////////////////////////////

float RTP_AOamp;
float4 RTP_AO_0123, RTP_AO_4567;
float RTP_AOsharpness;

//
// planets specific
//

//
// geom blend specific
//
#ifdef GEOM_BLEND
float4 _TERRAIN_Tiling;
sampler2D _TERRAIN_Control;
#endif

#if defined(SHADER_API_D3D11) 
CBUFFER_END
#endif

float rtp_snow_strength;

#ifdef UNITY_PASS_PREPASSFINAL
uniform float4 _WorldSpaceLightPosCustom;
#endif

#ifdef UNITY_PASS_META
	float4 _MainTex_ST;
#endif

struct Input {
	float2 uv_Control; // texcoords from mesh
	float2 uv2_SplatA0;
	float3 worldPos;
	float3 viewDir;
	float3 worldNormal;
	float3 worldRefl;
	INTERNAL_DATA
	float4 color:COLOR;
};


inline float3 BlendNormalsRTP(float3 global_norm, float3 detail) {
	float3 _t = global_norm;
	_t.z += 1;
	float3 _u = float3(-detail.xy, detail.z);
	return normalize(_t*dot(_t, _u) - _u*_t.z);
}

struct RTPSurfaceOutput {
	fixed3 Albedo;
	fixed3 Normal;
	fixed3 Emission;
	half Specular;
	fixed Alpha;
	float2 RTP;
	half3 SpecColor;
	float distance; // for fog handling
	#ifdef RTP_PLANET
		float4 lightDir;
	#endif
};

#if defined(RTP_PLANET)
fixed3 _FColor;
#ifdef RTP_PLANET
fixed3 _FColor2;
fixed4 _FColor3;
#endif
float _Fdensity;
float _Fstart,_Fend;
#if defined(RTP_FOG_LINEAR)
	void customFog (Input IN, RTPSurfaceOutput o, inout fixed4 color) {
		float realFogDistance=(o.distance-max(0.0, AtmosphereDistance(o.distance)));
		float f=saturate((_Fend-realFogDistance)/(_Fend-_Fstart));
		#ifdef UNITY_PASS_FORWARDADD
			color.rgb*=f;
		#else		
			float lightScatterValue=o.lightDir.z*2+1; // lightDir.z for planet has dot(light, surface normal)
			f=lerp(f, 1, saturate( (o.lightDir.w-_PlanetRadius) / _PlanetAtmosphereLength ) );
			float3 _FColorBack=lerp( lerp(color.rgb, _FColor3.rgb, _FColor3.a), _FColor2, saturate(lightScatterValue));
			color.rgb=lerp( lerp(_FColorBack, _FColor, saturate(o.lightDir.z*2)), color.rgb, f);
		#endif
	}
#else
	#if defined(RTP_FOG_EXP2)
		void customFog (Input IN, RTPSurfaceOutput o, inout fixed4 color) {
			float realFogDistance=(o.distance-max(0.0,AtmosphereDistance(o.distance)));
			float val=_Fdensity*realFogDistance;
			float f=exp2(-val*val*1.442695);
			#ifdef UNITY_PASS_FORWARDADD
				color.rgb*=f;
			#else
				float lightScatterValue=o.lightDir.z*2+1; // lightDir.z for planet has dot(light, surface normal)
				f=lerp(f, 1, saturate( (o.lightDir.w-_PlanetRadius) / _PlanetAtmosphereLength ) );
				float3 _FColorBack=lerp( lerp(color.rgb, _FColor3.rgb, _FColor3.a), _FColor2, saturate(lightScatterValue));
				color.rgb=lerp( lerp(_FColorBack, _FColor, saturate(o.lightDir.z*2)), color.rgb, f);
			#endif
		}
	#else
		#if defined(RTP_FOG_EXPONENTIAL)
			void customFog (Input IN, RTPSurfaceOutput o, inout fixed4 color) {
				float g=log2(1-_Fdensity);
				float realFogDistance=(o.distance-max(0.0,AtmosphereDistance(o.distance)));
				float f=exp2(g*realFogDistance);
				#ifdef UNITY_PASS_FORWARDADD
					color.rgb*=f;
				#else				
					float lightScatterValue=o.lightDir.z*2+1; // lightDir.z for planet has dot(light, surface normal)
					f=lerp(f, 1, saturate( (o.lightDir.w-_PlanetRadius) / _PlanetAtmosphereLength ) );
					float3 _FColorBack=lerp( lerp(color.rgb, _FColor3.rgb, _FColor3.a), _FColor2, saturate(lightScatterValue));
					color.rgb=lerp( lerp(_FColorBack, _FColor, saturate(o.lightDir.z*2)), color.rgb, f);
				#endif
			}
		#else
			void customFog (Input IN, RTPSurfaceOutput o, inout fixed4 color) {
			}
		#endif
	#endif
#endif
#endif // PLANET

#if !defined(RTP_PLANET)
fixed3 _FColor;
float _Fdensity;
float _Fstart,_Fend;
void customFog (Input IN, RTPSurfaceOutput o, inout fixed4 color) {
	#ifdef UNITY_PI
		//
		// Unity 5 fog
		//
		// we use distance alone, even for mobile
		#ifdef UNITY_APPLY_FOG_COLOR
			#undef UNITY_APPLY_FOG_COLOR
			#if defined(FOG_LINEAR) || defined(FOG_EXP) || defined(FOG_EXP2)
				// always compute fog factor per pixel (so RTP has chance to work on mobile, too)
				#define UNITY_APPLY_FOG_COLOR(coord,col,fogCol) UNITY_CALC_FOG_FACTOR(coord); UNITY_FOG_LERP_COLOR(col,fogCol,unityFogFactor)
			#else
				#define UNITY_APPLY_FOG_COLOR(coord,col,fogCol)
			#endif
		#endif
	
		UNITY_APPLY_FOG(o.distance, color);
	#else
		//
		// Unity3/4 custom fog
		//
		#if defined(RTP_FOG_LINEAR)
			float f=saturate((_Fend-o.distance)/(_Fend-_Fstart));
			#ifdef UNITY_PASS_FORWARDADD
				color.rgb*=f;
			#else		
				color.rgb=lerp(_FColor, color.rgb, f);
			#endif
		#else
			#if defined(RTP_FOG_EXP2)
				float val=_Fdensity*o.distance;
				float f=exp2(-val*val*1.442695);
				#ifdef UNITY_PASS_FORWARDADD
					color.rgb*=f;
				#else
					color.rgb=lerp(_FColor, color.rgb, f);
				#endif
			#else
				#if defined(RTP_FOG_EXPONENTIAL)
					float g=log2(1-_Fdensity);
					float f=exp2(g*o.distance);
					#ifdef UNITY_PASS_FORWARDADD
						color.rgb*=f;
					#else				
						color.rgb=lerp(_FColor, color.rgb, f);
					#endif
				#endif
			#endif
		#endif

	#endif
}
#endif

#include "Assets/ReliefPack/Shaders/CustomLighting.cginc"
#include "Tessellation.cginc"
inline float3 myObjSpaceLightDir( in float4 v )
{
	#ifdef UNITY_PASS_PREPASSFINAL
		float4 lpos=_WorldSpaceLightPosCustom;
	#else
		float4 lpos=_WorldSpaceLightPos0;
	#endif	
	float3 objSpaceLightPos = mul(unity_WorldToObject, lpos).xyz;
	#ifndef USING_LIGHT_MULTI_COMPILE
		#ifdef UNITY_PASS_PREPASSFINAL
			return objSpaceLightPos.xyz * 1.0 - v.xyz * lpos.w;
		#else
			return objSpaceLightPos.xyz * 1.0 - v.xyz * lpos.w;
		#endif
	#else
		#ifndef USING_DIRECTIONAL_LIGHT
		return objSpaceLightPos.xyz * 1.0 - v.xyz;
		#else
		return objSpaceLightPos.xyz;
		#endif
	#endif
}

/////////////////////////////////
//
// optional sampling heightmap / normalmap & tessellation
//

    struct appdata {
        float4 vertex : POSITION;
        float4 tangent : TANGENT;
        float3 normal : NORMAL;
        float2 texcoord : TEXCOORD0;
        float2 texcoord1 : TEXCOORD1;
        #ifdef UNITY_PI
        // dynamic lightmaps
        float2 texcoord2 : TEXCOORD2;
        #endif
        float4 color : COLOR;
    };

    float _TessSubdivisions;
    float _TessSubdivisionsFar;
    float _TessYOffset;
	
	float interpolate_bicubic(float x, float y, out float3 norm) {
		//x=frac(x);
		//y=frac(y);
		x*=_NormalMapGlobal_TexelSize.z;
		y*=_NormalMapGlobal_TexelSize.w;
		// transform the coordinate from [0,extent] to [-0.5, extent-0.5]
		float2 coord_grid = float2(x - 0.5, y - 0.5);
		float2 index = floor(coord_grid);
		float2 fraction = coord_grid - index;
		float2 one_frac = 1.0 - fraction;
		float2 one_frac2 = one_frac * one_frac;
		float2 fraction2 = fraction * fraction;
		float2 w0 = 1.0/6.0 * one_frac2 * one_frac;
		float2 w1 = 2.0/3.0 - 0.5 * fraction2 * (2.0-fraction);
		float2 w2 = 2.0/3.0 - 0.5 * one_frac2 * (2.0-one_frac);
		float2 w3 = 1.0/6.0 * fraction2 * fraction;
		float2 g0 = w0 + w1;
		float2 g1 = w2 + w3;
		// h0 = w1/g0 - 1, move from [-0.5, extent-0.5] to [0, extent]
		float2 h0 = (w1 / g0) - 0.5 + index;
		float2 h1 = (w3 / g1) + 1.5 + index;
		h0*=_NormalMapGlobal_TexelSize.x;
		h1*=_NormalMapGlobal_TexelSize.y;
		// fetch the four linear interpolations
		float4 val;
		val= tex2Dlod(_NormalMapGlobal, float4(h0.x, h0.y,0,0));
		float tex00 = val.g/255+val.r;
		float2 tex00_N = val.ba;
		val = tex2Dlod(_NormalMapGlobal, float4(h1.x, h0.y,0,0)); 
		float tex10 = val.g/255+val.r;
		float2 tex10_N = val.ba;
		val = tex2Dlod(_NormalMapGlobal, float4(h0.x, h1.y,0,0));
		float tex01 = val.g/255+val.r;
		float2 tex01_N = val.ba;
		val = tex2Dlod(_NormalMapGlobal, float4(h1.x, h1.y,0,0));
		float tex11 = val.g/255+val.r;
		float2 tex11_N = val.ba;

		// normal
		// weigh along the y-direction
		tex00_N = lerp(tex01_N, tex00_N, g0.y);
		tex10_N = lerp(tex11_N, tex10_N, g0.y);
		// weigh along the x-direction
		tex00_N = lerp(tex10_N, tex00_N, g0.x)*2-1;
		norm=float3(tex00_N.x, sqrt(1 - saturate(dot(tex00_N, tex00_N))), tex00_N.y);

		// weigh along the y-direction
		tex00 = lerp(tex01, tex00, g0.y);
		tex10 = lerp(tex11, tex10, g0.y);
		// weigh along the x-direction
		return lerp(tex10, tex00, g0.x);
	}  


	
    float4 tessEdge (appdata v0, appdata v1, appdata v2)
    {
		float3 pos0 = mul(unity_ObjectToWorld,v0.vertex).xyz;
		float3 pos1 = mul(unity_ObjectToWorld,v1.vertex).xyz;
		float3 pos2 = mul(unity_ObjectToWorld,v2.vertex).xyz;
		float4 tess;
		// distance to edge center
		float3 edge_dist;
		edge_dist.x = distance (0.5 * (pos1+pos2), _WorldSpaceCameraPos);
		edge_dist.y = distance (0.5 * (pos2+pos0), _WorldSpaceCameraPos);
		edge_dist.z = distance (0.5 * (pos0+pos1), _WorldSpaceCameraPos);
		float3 dist = saturate((edge_dist.xyz - _TERRAIN_distance_start_bumpglobal) / _TERRAIN_distance_transition_bumpglobal);
		float4 tessFactor;
		tessFactor.x = lerp(_TessSubdivisions, _TessSubdivisionsFar, dist.x);
		tessFactor.y = lerp(_TessSubdivisions, _TessSubdivisionsFar, dist.y);
		tessFactor.z = lerp(_TessSubdivisions, _TessSubdivisionsFar, dist.z);
		tessFactor.w = (tessFactor.x+tessFactor.y+tessFactor.z)/3;
		return tessFactor;
    }

	

	
/////////////////////////////////

float4 _Splat0_ST;
float2 RTP_CustomTiling;



	void vert (inout appdata v) 
	{
	


		#ifdef TESS
		{	
				float3 wPos=mul(unity_ObjectToWorld, v.vertex).xyz;
				float2 _INPUT_uv=mul(unity_WorldToObject, float4(wPos.xyz,1)).xzy ;


				float _INPUT_distance=distance(_WorldSpaceCameraPos, wPos);
				float _uv_Relief_z=saturate((_INPUT_distance - _TERRAIN_distance_start) / _TERRAIN_distance_transition);
				float detailMIPlevel=_uv_Relief_z*5;
				_uv_Relief_z=1-_uv_Relief_z;

				float2 gUV = v.texcoord.xy *(1-_SplatA0_TexelSize.xy * 2) + _SplatA0_TexelSize.xy;

				float4 splat_controlA = tex2Dlod(_Control1, float4(gUV ,0,0));

		#ifndef _4LAYERS
				float4 splat_controlB = tex2Dlod(_Control2, float4(gUV ,0,0));
		#endif 
				float4 rev = tex2Dlod(_TERRAIN_HeightMap, float4(v.texcoord1.xy, 0,detailMIPlevel));
				float lenght = sqrt(rev.r*rev.r + rev.g*rev.g + rev.b*rev.b + rev.a*rev.a)/8;

				float4 tHA=saturate(rev+0.001);
				float4 tHB=0;

				float4 splat_control1 = splat_controlA;

		#ifndef TRANSITION

				

				splat_control1 *= tHA;
		
				float energy = _TessStrenght0123.x * splat_control1.x;
				      energy += _TessStrenght0123.y * splat_control1.y;
					  energy += _TessStrenght0123.z * splat_control1.z;
					  energy += _TessStrenght0123.w * splat_control1.w;
		#else
				float transitionParam = 1-v.color.y*_TransitionSlider;
				float4 rev2 = tex2Dlod(_TERRAIN_HeightMap2, float4(v.texcoord1.xy, 0,detailMIPlevel));
				float lenght2 = sqrt(rev2.r*rev2.r + rev2.g*rev2.g + rev2.b*rev2.b + rev2.a*rev2.a)/8;

				tHB=saturate(rev2+0.001);

				splat_control1 *= lerp(tHA, tHB, transitionParam);

				float energy = lerp(_TessStrenght0123.x,_TessStrenght4567.x,transitionParam) * splat_control1.x;
				      energy += lerp(_TessStrenght0123.y,_TessStrenght4567.y,transitionParam) * splat_control1.y;
					  energy += lerp(_TessStrenght0123.z,_TessStrenght4567.z,transitionParam) * splat_control1.z;
					  energy += lerp(_TessStrenght0123.w,_TessStrenght4567.w,transitionParam) * splat_control1.w;
		#endif

		#ifdef _4LAYERS
				float4 splat_control1_mid=splat_control1*splat_control1;
				splat_control1_mid/=max(0.001, dot(splat_control1_mid,1));
				float4 splat_control1_close=splat_control1_mid*splat_control1_mid;

				splat_control1_close/=max(0.001, dot(splat_control1_close,1));
				splat_control1=lerp(splat_control1_mid, splat_control1_close, _uv_Relief_z);	
		#else
				float4 splat_control1_mid=splat_control1*splat_control1;
				float4 splat_control2 = splat_controlB;

				float4 rev2 = tex2Dlod(_TERRAIN_HeightMap2, float4(v.texcoord1.xy, 0,detailMIPlevel));
				tHB=saturate(rev2+0.001);
				splat_control2 *= tHB;

				float lenght2 = sqrt(rev2.r*rev2.r + rev2.g*rev2.g + rev2.b*rev2.b + rev2.a*rev2.a)/8;

				float energyB = _TessStrenght4567.x * splat_control2.x;
				      energyB += _TessStrenght4567.y * splat_control2.y;
					  energyB += _TessStrenght4567.z * splat_control2.z;
					  energyB += _TessStrenght4567.w * splat_control2.w;

				float4 splat_control2_mid = splat_control2*splat_control2;
				float norm_sum = max(0.001, dot(splat_control1_mid,1)+ dot(splat_control2_mid,1));
				splat_control1_mid/=norm_sum;
				splat_control2_mid/=norm_sum;

				float4 splat_control1_close=splat_control1_mid*splat_control1_mid;
				float4 splat_control2_close=splat_control2_mid*splat_control2_mid;

				norm_sum = max(0.001, dot(splat_control1_close,1)+ dot(splat_control2_close,1));
				splat_control1_close/=norm_sum;
				splat_control2_close/=norm_sum;
				splat_control1=lerp(splat_control1_mid, splat_control1_close, _uv_Relief_z);
				splat_control2=lerp(splat_control2_mid, splat_control2_close, _uv_Relief_z);
		#endif

		float actH = 0;

		#ifdef _4LAYERS
			#ifndef TRANSITION
				actH = dot(splat_control1, tHA);
			#else
				actH = dot(splat_control1, lerp(tHA,tHB, transitionParam));
			#endif
		#else
				actH += dot(splat_control2, tHB);
		#endif


		#ifdef _4LAYERS
			#ifndef TRANSITION
				float energy2 = _TessStrenght0123.x * splat_control1.x;
					  energy2 += _TessStrenght0123.y * splat_control1.y;
					  energy2 += _TessStrenght0123.z * splat_control1.z;
					  energy2 += _TessStrenght0123.w * splat_control1.w;
				float current = lerp(energy,energy2, lenght);
			#else
				float energy2 = lerp(_TessStrenght0123.x,_TessStrenght4567.x,transitionParam) * splat_control1.x;
					  energy2 += lerp(_TessStrenght0123.y,_TessStrenght4567.y,transitionParam) * splat_control1.y;
					  energy2 += lerp(_TessStrenght0123.z,_TessStrenght4567.z,transitionParam) * splat_control1.z;
					  energy2 += lerp(_TessStrenght0123.w,_TessStrenght4567.w,transitionParam) * splat_control1.w;
				float current = lerp(energy,energy2, lerp(lenght,lenght2,transitionParam));
			#endif
		#else
				float energy2 = _TessStrenght0123.x * splat_control1.x;
					  energy2 += _TessStrenght0123.y * splat_control1.y;
					  energy2 += _TessStrenght0123.z * splat_control1.z;
					  energy2 += _TessStrenght0123.w * splat_control1.w;
				float energy3 = lerp(energy,energy2, lenght);

				float energy2B = _TessStrenght4567.x * splat_control2.x;
					  energy2B += _TessStrenght4567.y * splat_control2.y;
					  energy2B += _TessStrenght4567.z * splat_control2.z;
					  energy2B += _TessStrenght4567.w * splat_control2.w;
				float energy3B = lerp(energyB,energy2B, lenght2);

				float maximum = max(energy3,energy3B);
				float current = clamp(0,maximum,energy3 + energy3B);
		#endif

				v.vertex.xyz += v.normal * current * actH * _TessYOffset;
		}
		#endif

		V_CW_TransformPoint(v.vertex);

		}

// tex2D helper
inline fixed4 tex2Dp(sampler2D tex, float2 uv, float2 mddx, float2 mddy) {
	#if (defined(RTP_STANDALONE) || defined(GEOM_BLEND)) && !SHADER_API_OPENGL
		return fixed4(tex2D(tex, uv, mddx, mddy));
	#else
		return fixed4(tex2D(tex, uv));
	#endif
}
inline fixed4 tex2Dd(sampler2D tex, float2 uv, float2 mddx, float2 mddy) {
	#if !SHADER_API_GLES
		return fixed4(tex2D(tex, uv, mddx, mddy));
	#else
		// webGL on firefox doesn't like derivate texture calls
		return fixed4(tex2D(tex, uv));
	#endif
}

void surf (Input IN, inout RTPSurfaceOutput o) { // RTPSurfaceOutput
	o.Normal=float3(0,0,1); o.Albedo=0;	o.Specular=RTP_DeferredAddPassSpec; o.Alpha=0;
	o.Emission=0;
	o.RTP.xy=float2(0,1); o.SpecColor=0;
	half o_Gloss=0;
	
	
	float2 _Control2uv = IN.uv2_SplatA0.xy;
	float2 GlobalUV = mul(unity_WorldToObject, float4(IN.worldPos.xyz,1)).xzy / _TERRAIN_ReliefTransformTriplanarZ;
	
	float3 _INPUT_uv=0;

	_INPUT_uv.xy=_Control2uv;
	
	
	float _INPUT_distance=distance(_WorldSpaceCameraPos, IN.worldPos);
	o.distance=_INPUT_distance; // needed for fog
	
    	float3 worldNormalFlat;
    	{
			#ifdef UNITY_PASS_META	
    			float2 gUV = (IN.uv_Control.xy*_MainTex_ST.xy+_MainTex_ST.zw)*(1-_NormalMapGlobal_TexelSize.xy) + _NormalMapGlobal_TexelSize.xy*0.5;
			#else
	    		float2 gUV = IN.uv_Control*(1-_NormalMapGlobal_TexelSize.xy) + _NormalMapGlobal_TexelSize.xy*0.5;
			#endif
	    	#ifdef HEIGHTMAP_SAMPLE_BICUBIC
	    		float3 norm=0;
				interpolate_bicubic(gUV.x, gUV.y, /*out*/ norm);
				norm.xz*=_TERRAIN_trees_shadow_values.w;
				worldNormalFlat=normalize(norm.xyz);
			#else
		    	fixed4 HNMap=tex2Dlod(_NormalMapGlobal, float4(gUV.xy,0,0));
				float3 norm;
				norm.xz=HNMap.ba*2-1;
				norm.y=sqrt(1-saturate(dot(norm.xz, norm.xz)));
				norm.xz*=_TERRAIN_trees_shadow_values.w;
				worldNormalFlat=normalize(norm.xyz);
			#endif	
		}

	float3 vertexNorm=worldNormalFlat;
	#if !defined(APPROX_TANGENTS) || defined(RTP_STANDALONE)
		vertexNorm=normalize(mul(unity_WorldToObject, float4(vertexNorm,0)).xyz);
	#else
		// terrains are not rotated - world normal = object normal then
	#endif
	float3 tangentBase = cross( vertexNorm, cross(float3(1, 0, 0.0001), vertexNorm) );
	tangentBase /= max(0.0001, length(tangentBase)); // get rid of floating point div by zero warning
	float3 binormalBase = -cross( vertexNorm, tangentBase );
	

		#if defined(RTP_STANDALONE) || defined(GEOM_BLEND)
			float2 globalUV=(IN.worldPos.xz-_TERRAIN_PosSize.xy)/_TERRAIN_PosSize.zw;
			#define IN_uv_Control (globalUV)
			// used in tex2Dp() but compiled out anyway
			float2 _ddxGlobal=ddx(IN_uv_Control);
			float2 _ddyGlobal=ddy(IN_uv_Control);
			
			#if defined(GEOM_BLEND) && defined(BLENDING_HEIGHT)
				float2 tiledUV=(IN.worldPos.xz-_TERRAIN_PosSize.xy+_TERRAIN_Tiling.zw)/_TERRAIN_Tiling.xy;
				float4 tHeightmapTMP = tex2D(_TERRAIN_HeightMap, tiledUV); // potrzebujemy tego tymczasowo na końcu
			#endif						
		#else
			#ifdef UNITY_PASS_META	
				//IN.uv_Control.xy=IN.uv_Control.xy*_MainTex_ST.xy+_MainTex_ST.zw;
			#endif		
			#define IN_uv_Control (IN.uv_Control)
			// used in tex2Dp() but compiled out anyway
			#define _ddxGlobal float2(0,0)
			#define _ddyGlobal float2(0,0)
		#endif

	float tmpBias=exp2(-_RTP_MIP_BIAS/(0.75*2));
	float3 _ddxMain=ddx(_INPUT_uv.xyz)/tmpBias;
	float3 _ddyMain=ddy(_INPUT_uv.xyz)/tmpBias;	
		
	float2 mip_selector;
	float2 IN_uv=_INPUT_uv.xy*1024*(1+_RTP_MIP_BIAS);
	float2 dx = ddx( IN_uv);
	float2 dy = ddy( IN_uv);


		float4 splat_controlA = tex2Dp(_Control1, IN.uv_Control.xy, _ddxGlobal, _ddyGlobal);
		
	 	float total_coverage=dot(splat_controlA, 1);
		#ifdef _4LAYERS

		#else
			float4 splat_controlB = tex2Dp(_Control2, IN.uv_Control.xy, _ddxGlobal, _ddyGlobal);
		 	total_coverage+=dot(splat_controlB, 1);

		#endif
	
	
	float _uv_Relief_w=saturate((_INPUT_distance - _TERRAIN_distance_start_bumpglobal) / _TERRAIN_distance_transition_bumpglobal);
	#ifdef FAR_ONLY
		#define _uv_Relief_z 0
		float _uv_Relief_wz_no_overlap=_uv_Relief_w;
	#else
		float _uv_Relief_z=saturate((_INPUT_distance - _TERRAIN_distance_start) / _TERRAIN_distance_transition);
		float _uv_Relief_wz_no_overlap=_uv_Relief_w*_uv_Relief_z;
		_uv_Relief_z=1-_uv_Relief_z;
	#endif
	
	// main MIP selector
	float d = max( dot( dx, dx ), dot( dy, dy ) );
	mip_selector=0.5*log2(d);
	
	IN.viewDir/=max(0.0001, length(IN.viewDir)); // get rid of floating point div by zero warning
	IN.viewDir.z=saturate(IN.viewDir.z); // czasem wystepuja problemy na krawedziach widocznosci (viewDir.z nie powinien byc jednak ujemny)
		
		
	float4 tHA;
	float4 tHB=0;
	float4 splat_control1 = splat_controlA;

		#ifdef USE_EXTRUDE_REDUCTION
			tHA=saturate(lerp(tex2Dd(_TERRAIN_HeightMap, _INPUT_uv.xy, _ddxMain.xy, _ddyMain.xy), 1, PER_LAYER_HEIGHT_MODIFIER0123)+0.001);
		#else
			tHA=saturate(tex2Dd(_TERRAIN_HeightMap, _Control2uv, _ddxMain.xy, _ddyMain.xy)+0.001);
		#endif	
	
	splat_control1 *= tHA;
	#ifdef TRANSITION
		
		float4 splat_control1_mid=splat_control1*splat_control1;
		splat_control1_mid/=dot(splat_control1_mid,1);
		float4 splat_control1_close=splat_control1_mid*splat_control1_mid;
		#ifdef SHARPEN_HEIGHTBLEND_EDGES_PASS1
			splat_control1_close*=splat_control1_close;
		#endif
		#ifdef SHARPEN_HEIGHTBLEND_EDGES_PASS2
			splat_control1_close*=splat_control1_close;
		#endif
		splat_control1_close/=dot(splat_control1_close,1);
		splat_control1=lerp(splat_control1_mid, splat_control1_close, _uv_Relief_z);
		#ifdef NOSPEC_BLEED
			float4 splat_control1_nobleed=saturate(splat_control1-float4(0.5,0.5,0.5,0.5))*2;
		#else
			float4 splat_control1_nobleed=splat_control1;
		#endif

		float4 splat_control2 = splat_controlA;
		#ifdef USE_EXTRUDE_REDUCTION
			tHB=saturate(lerp(tex2Dd(_TERRAIN_HeightMap2, _INPUT_uv.xy, _ddxMain.xy, _ddyMain.xy), 1, PER_LAYER_HEIGHT_MODIFIER4567)+0.001);
			splat_control2 *= tHB;
		#else
			tHB=saturate(tex2Dd(_TERRAIN_HeightMap2, _INPUT_uv.xy, _ddxMain.xy, _ddyMain.xy)+0.001);
			splat_control2 *= tHB;
		#endif

		float4 splat_control2_mid=splat_control2*splat_control2;
		splat_control2_mid/=dot(splat_control2_mid,1);
		float4 splat_control2_close=splat_control2_mid*splat_control2_mid;
		#ifdef SHARPEN_HEIGHTBLEND_EDGES_PASS1
			splat_control2_close*=splat_control2_close;
		#endif
		#ifdef SHARPEN_HEIGHTBLEND_EDGES_PASS2
			splat_control2_close*=splat_control2_close;
		#endif
		splat_control2_close/=dot(splat_control2_close,1);
		splat_control2=lerp(splat_control2_mid, splat_control2_close, _uv_Relief_z);
		#ifdef NOSPEC_BLEED
			float4 splat_control2_nobleed=saturate(splat_control2-float4(0.5,0.5,0.5,0.5))*2;
		#else
			float4 splat_control2_nobleed=splat_control2;
		#endif
	#else
	#ifdef _4LAYERS
		float4 splat_control1_mid=splat_control1*splat_control1;
		splat_control1_mid/=dot(splat_control1_mid,1);
		float4 splat_control1_close=splat_control1_mid*splat_control1_mid;
		#ifdef SHARPEN_HEIGHTBLEND_EDGES_PASS1
			splat_control1_close*=splat_control1_close;
		#endif
		#ifdef SHARPEN_HEIGHTBLEND_EDGES_PASS2
			splat_control1_close*=splat_control1_close;
		#endif
		splat_control1_close/=dot(splat_control1_close,1);
		splat_control1=lerp(splat_control1_mid, splat_control1_close, _uv_Relief_z);
		#ifdef NOSPEC_BLEED
			float4 splat_control1_nobleed=saturate(splat_control1-float4(0.5,0.5,0.5,0.5))*2;
		#else
			float4 splat_control1_nobleed=splat_control1;
		#endif
	#else
		float4 splat_control1_mid=splat_control1*splat_control1;
		float4 splat_control2 = splat_controlB;
		#ifdef USE_EXTRUDE_REDUCTION
			tHB=saturate(lerp(tex2Dd(_TERRAIN_HeightMap2, _INPUT_uv.xy, _ddxMain.xy, _ddyMain.xy), 1, PER_LAYER_HEIGHT_MODIFIER4567)+0.001);
			splat_control2 *= tHB;
		#else
			tHB=saturate(tex2Dd(_TERRAIN_HeightMap2, _INPUT_uv.xy, _ddxMain.xy, _ddyMain.xy)+0.001);
			splat_control2 *= tHB;
		#endif	
		float4 splat_control2_mid=splat_control2*splat_control2;
		float norm_sum=dot(splat_control1_mid,1) + dot(splat_control2_mid,1);
		splat_control1_mid/=norm_sum;
		splat_control2_mid/=norm_sum;
		
		float4 splat_control1_close=splat_control1_mid*splat_control1_mid;
		float4 splat_control2_close=splat_control2_mid*splat_control2_mid;
		#ifdef SHARPEN_HEIGHTBLEND_EDGES_PASS1
			splat_control1_close*=splat_control1_close;
			splat_control2_close*=splat_control2_close;
		#endif
		#ifdef SHARPEN_HEIGHTBLEND_EDGES_PASS2
			splat_control1_close*=splat_control1_close;
			splat_control2_close*=splat_control2_close;
		#endif
		norm_sum=dot(splat_control1_close,1) + dot(splat_control2_close,1);
		splat_control1_close/=norm_sum;
		splat_control2_close/=norm_sum;
		splat_control1=lerp(splat_control1_mid, splat_control1_close, _uv_Relief_z);
		splat_control2=lerp(splat_control2_mid, splat_control2_close, _uv_Relief_z);
		#ifdef NOSPEC_BLEED
			float4 splat_control1_nobleed=saturate(splat_control1-float4(0.5,0.5,0.5,0.5))*2;
			float4 splat_control2_nobleed=saturate(splat_control2-float4(0.5,0.5,0.5,0.5))*2;
		#else
			float4 splat_control1_nobleed=splat_control1;
			float4 splat_control2_nobleed=splat_control2;
		#endif
	#endif
	#endif


	float splat_controlA_coverage=dot(splat_control1, 1);
	#ifndef _4LAYERS
	float splat_controlB_coverage=dot(splat_control2, 1);
	#endif
	
	// layer emission - init step
		
	#if defined(NEED_LOCALHEIGHT)
		float actH=dot(splat_control1, tHA);
		#ifndef _4LAYERS
			actH+=dot(splat_control2, tHB);
		#endif
	#endif
	
	// simple fresnel rim (w/o bumpmapping)
	float diffFresnel = exp2(SchlickFresnelApproxExp2Const*IN.viewDir.z); // ca. (1-x)^5
	#ifdef APPROX_TANGENTS
		// dla RTP na meshach (przykladowa scena)  normalne sa plaskie i ponizszy tweak dziala b. zle
		//diffFresnel = (diffFresnel>0.999) ? ((1-diffFresnel)/0.001) : (diffFresnel/0.999);
	#endif
	
	#if ( defined(RTP_REFLECTION) && !defined(UNITY_PASS_SHADOWCASTER) ) || ( defined(RTP_IBL_SPEC) && !defined(UNITY_PASS_SHADOWCASTER) ) || defined(RTP_PBL_FRESNEL)
	#ifdef _4LAYERS
		// gloss vs. fresnel dependency
		float fresnelAtten=dot(splat_control1, RTP_FresnelAtten0123);
		o.RTP.x=dot(splat_control1, RTP_Fresnel0123);
	#else
		float fresnelAtten=dot(splat_control1, RTP_FresnelAtten0123)+dot(splat_control2, RTP_FresnelAtten4567);
		o.RTP.x=dot(splat_control1, RTP_Fresnel0123)+dot(splat_control2, RTP_Fresnel4567);
	#endif	
	#endif
		
   
	fixed3 col=0;
	fixed3 colAlbedo=0; // bazowy kolor z suchej tekstury bez śniegu i żadnych modyfikacji jasności i nasycenia
	
	float3 norm_far=float3(0,0,1);
	

		
	norm_far.z = sqrt(1 - saturate(dot(norm_far.xy, norm_far.xy)));
	

		
	float2 IN_uv_Relief_Offset;
	
	
		
	float4 MIPmult0123=_MIPmult0123*_uv_Relief_w;
	#ifndef _4LAYERS
		float4 MIPmult4567=_MIPmult4567*_uv_Relief_w;
	#endif
	
	#ifdef FAR_ONLY
		if (false) {
	#else
		#if defined(SHADER_API_D3D9) && !defined(_4LAYERS) && defined(UNITY_PI) && defined(DISABLE_DX9_HLSL_BRANCHING_LEVEL1)
			// TODO - HLSL for D3D9 seems to be buggy and can't compile RTP shaders (Cg in U<=4.6 did it...)
			if (true) {
		#else
			if (_uv_Relief_z>0) {
		#endif
	#endif
 		//////////////////////////////////
 		//
 		// close
 		//
 		//////////////////////////////////
		#if !defined(_4LAYERS) || defined(RTP_USE_COLOR_ATLAS)
			float _off=16*_SplatAtlasA_TexelSize.x;
			float _mult=_off*-2+0.5;
			float4 _offMix=_off;
			float4 _multMix=_mult;
			float4 uvSplat01, uvSplat23;
			float4 uvSplat01M, uvSplat23M;
		#endif

	 	float4 rayPos = float4(_Control2uv, 1, clamp((mip_selector.x+rtp_mipoffset_height), 0, 6) );
		
		#ifdef TRANSITION

			///////////////////////////////////////////////
	 		//
	 		// splats 0-7 close combined transition
	 		//
	 		///////////////////////////////////////////////
			
			 #ifdef RTP_SHOW_OVERLAPPED
	 		o.Emission.r=1;
	 		#endif
			
			uvSplat01=frac(rayPos.xy).xyxy*_mult+_off;
			uvSplat01.zw+=float2(0.5,0);
			uvSplat23=uvSplat01.xyxy+float4(0,0.5,0.5,0.5);
		
		
			fixed4 c;
			float4 gloss=0;
			fixed3 col2 = 0;

			float _MipActual=min(mip_selector.x+rtp_mipoffset_color,6);
			c = tex2Dlod(_SplatAtlasA, float4(uvSplat01.xy, _MipActual.xx)); col = splat_control1.x * c.rgb; gloss.r = c.a;
			c = tex2Dlod(_SplatAtlasA, float4(uvSplat01.zw, _MipActual.xx)); col += splat_control1.y * c.rgb; gloss.g = c.a;
			c = tex2Dlod(_SplatAtlasA, float4(uvSplat23.xy, _MipActual.xx)); col += splat_control1.z * c.rgb; gloss.b = c.a;
			c = tex2Dlod(_SplatAtlasA, float4(uvSplat23.zw, _MipActual.xx)); col += splat_control1.w * c.rgb; gloss.a = c.a;
			
			float glcombinedA = dot(gloss, splat_control1);
							
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat01.xy, _MipActual.xx)); col2 = splat_control2.x * c.rgb; gloss.r = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat01.zw, _MipActual.xx)); col2 += splat_control2.y * c.rgb; gloss.g = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat23.xy, _MipActual.xx)); col2 += splat_control2.z * c.rgb; gloss.b = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat23.zw, _MipActual.xx)); col2 += splat_control2.w * c.rgb; gloss.a = c.a;

			float glcombinedB = dot(gloss, splat_control2);
									
					
			
			glcombinedA=saturate(glcombinedA - glcombinedA*dot(splat_control1, RTP_AO_0123) * saturate((mip_selector.x+rtp_mipoffset_color)*0.2) );
			glcombinedB=saturate(glcombinedB - glcombinedB*dot(splat_control2, RTP_AO_4567) * saturate((mip_selector.x+rtp_mipoffset_color)*0.2) );
			
			//glcombined = lerp(glcombinedB,glcombinedA,IN.uv_Control.x);	
		//first
			float RTP_gloss2mask = dot(splat_control1, RTP_gloss2mask0123);
			float _Spec = dot(splat_control1_nobleed, _Spec0123); // anti-bleed subtraction
			float RTP_gloss_mult = dot(splat_control1, RTP_gloss_mult0123);
			float RTP_gloss_shaping = dot(splat_control1, RTP_gloss_shaping0123);
			float gls = saturate(glcombinedA * RTP_gloss_mult);
			
			float GlossA = lerp(1, gls, RTP_gloss2mask) * _Spec;
			
			float2 gloss_shaped=float2(gls, 1-gls);
			gloss_shaped=gloss_shaped*gloss_shaped*gloss_shaped;
			gls=lerp(gloss_shaped.x, 1-gloss_shaped.y, RTP_gloss_shaping);
			
			float SpecularA = saturate(gls);
			
						
			half colDesatA=dot(col,0.33333);
			float brightness2SpecA=dot(splat_control1, _LayerBrightness2Spec0123);
			GlossA*=lerp(1, colDesatA, brightness2SpecA);
			fixed3 colAlbedoA = col;
			
			col=lerp(colDesatA.xxx, col, dot(splat_control1, _LayerSaturation0123));
			col*=dot(splat_control1, _LayerBrightness0123);
		//second
		
			RTP_gloss2mask = dot(splat_control2, RTP_gloss2mask4567);
			_Spec = dot(splat_control2_nobleed, _Spec4567); // anti-bleed subtraction
			RTP_gloss_mult = dot(splat_control2, RTP_gloss_mult4567);
			RTP_gloss_shaping = dot(splat_control2, RTP_gloss_shaping4567);
			gls = saturate(glcombinedB * RTP_gloss_mult);
			
			float GlossB = lerp(1, gls, RTP_gloss2mask) * _Spec;
			
			float2 gloss_shaped2=float2(gls, 1-gls);
			gloss_shaped2=gloss_shaped2*gloss_shaped2*gloss_shaped2;
			gls=lerp(gloss_shaped2.x, 1-gloss_shaped2.y, RTP_gloss_shaping);
			
			float SpecularB = saturate(gls);
			
					
			half colDesatB=dot(col2,0.33333);
			float brightness2SpecB=dot(splat_control2, _LayerBrightness2Spec4567);
			GlossB*=lerp(1, colDesatB, brightness2SpecB);
			fixed3 colAlbedoB = col2;
			
			col2=lerp(colDesatB.xxx, col2, dot(splat_control2, _LayerSaturation4567));
			col2*=dot(splat_control2, _LayerBrightness4567);
			
		//merge
			
		
			o.Specular =  lerp(SpecularA,SpecularB,1-IN.color.y*_TransitionSlider);	
			o_Gloss =  lerp(GlossA,GlossB,1-IN.color.y*_TransitionSlider);	
			col = lerp(col,col2,1-IN.color.y*_TransitionSlider);	
			//o.Gloss = o_Gloss;
			
			

			float3 n;
			float3 n2;
			float4 normals_combined;
			float4 normals_combined2;
			rayPos.w=mip_selector.x+rtp_mipoffset_bump;
				
			normals_combined = tex2Dlod(_BumpMap01, rayPos.xyww).rgba*splat_control1.rrgg;
			normals_combined+=tex2Dlod(_BumpMap23, rayPos.xyww).rgba*splat_control1.bbaa;
			normals_combined2 = tex2Dlod(_BumpMap45, rayPos.xyww).rgba*splat_control2.rrgg;
			normals_combined2+=tex2Dlod(_BumpMap67, rayPos.xyww).rgba*splat_control2.bbaa;
			n.xy=(normals_combined.rg+normals_combined.ba)*2-1;
			n.xy*=_uv_Relief_z;
			n.z = sqrt(1 - saturate(dot(n.xy, n.xy)));

			n2.xy=(normals_combined2.rg+normals_combined2.ba)*2-1;
			n2.xy*=_uv_Relief_z;
			n2.z = sqrt(1 - saturate(dot(n2.xy, n2.xy)));

			o.Normal=lerp(n,n2,1-IN.color.y*_TransitionSlider);	
			
			IN_uv_Relief_Offset.xy=rayPos.xy;


			

		#endif	
		
	#ifndef TRANSITION 		
		#ifndef _4LAYERS

			#if defined(SHADER_API_D3D9) && !defined(_4LAYERS) && defined(UNITY_PI) && defined(DISABLE_DX9_HLSL_BRANCHING_LEVEL2)
		 		if (true) {
			 #else
			 	if (splat_controlA_coverage>0.01 && splat_controlB_coverage>0.01) {
			 #endif
	 
	 		///////////////////////////////////////////////
	 		//
	 		// splats 0-7 close combined
	 		//
	 		///////////////////////////////////////////////
	 		#ifdef RTP_SHOW_OVERLAPPED
	 		o.Emission.r=1;
	 		#endif

									
			uvSplat01=frac(rayPos.xy).xyxy*_mult+_off;
			uvSplat01.zw+=float2(0.5,0);
			uvSplat23=uvSplat01.xyxy+float4(0,0.5,0.5,0.5);
			
	 	
			fixed3 col2 = 0;
			fixed4 c;
			float4 gloss=0;
			float _MipActual=min(mip_selector.x+rtp_mipoffset_color,6);

							
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat01.xy, _MipActual.xx)); col = splat_control2.x * c.rgb; gloss.r = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat01.zw, _MipActual.xx)); col += splat_control2.y * c.rgb; gloss.g = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat23.xy, _MipActual.xx)); col += splat_control2.z * c.rgb; gloss.b = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat23.zw, _MipActual.xx)); col += splat_control2.w * c.rgb; gloss.a = c.a;

			float glcombined = dot(gloss, splat_control2);

			c = tex2Dlod(_SplatAtlasA, float4(uvSplat01.xy, _MipActual.xx)); col += splat_control1.x * c.rgb; gloss.r = c.a;
			c = tex2Dlod(_SplatAtlasA, float4(uvSplat01.zw, _MipActual.xx)); col += splat_control1.y * c.rgb; gloss.g = c.a;
			c = tex2Dlod(_SplatAtlasA, float4(uvSplat23.xy, _MipActual.xx)); col += splat_control1.z * c.rgb; gloss.b = c.a;
			c = tex2Dlod(_SplatAtlasA, float4(uvSplat23.zw, _MipActual.xx)); col += splat_control1.w * c.rgb; gloss.a = c.a;
			
			glcombined += dot(gloss, splat_control1);		
	
			float RTP_gloss2mask = dot(splat_control1, RTP_gloss2mask0123)+dot(splat_control2, RTP_gloss2mask4567)			;
			float _Spec = dot(splat_control1_nobleed, _Spec0123) + dot(splat_control2_nobleed, _Spec4567); // anti-bleed subtraction
			float RTP_gloss_mult = dot(splat_control1, RTP_gloss_mult0123)+dot(splat_control2, RTP_gloss_mult4567);
			float RTP_gloss_shaping = dot(splat_control1, RTP_gloss_shaping0123)+dot(splat_control2, RTP_gloss_shaping4567);
			float gls = saturate(glcombined * RTP_gloss_mult);
			o_Gloss = lerp(1, gls, RTP_gloss2mask) * _Spec; // przemnóż przez damping dla warstw 4567
			
			float2 gloss_shaped=float2(gls, 1-gls);
			gloss_shaped=gloss_shaped*gloss_shaped*gloss_shaped;
			gls=lerp(gloss_shaped.x, 1-gloss_shaped.y, RTP_gloss_shaping);
			o.Specular = saturate(gls);
			// gloss vs. fresnel dependency

			half colDesat=dot(col,0.33333);
			float brightness2Spec=dot(splat_control1, _LayerBrightness2Spec0123) + dot(splat_control2, _LayerBrightness2Spec4567);
			o_Gloss*=lerp(1, colDesat, brightness2Spec);
			colAlbedo=1;
			col=lerp(colDesat.xxx, col, dot(splat_control1, _LayerSaturation0123) + dot(splat_control2, _LayerSaturation4567));
			col*=dot(splat_control1, _LayerBrightness0123) + dot(splat_control2, _LayerBrightness4567);  
			//col = col;//lerp(col2,col,IN.uv_Control.x);//+15)/15;//lerp(col,col2, (IN.uv_Control.x+15)/15);
			float3 n;
			float4 normals_combined;
			rayPos.w=mip_selector.x+rtp_mipoffset_bump;
				
			normals_combined = tex2Dlod(_BumpMap01, rayPos.xyww).rgba*splat_control1.rrgg;
			normals_combined+=tex2Dlod(_BumpMap23, rayPos.xyww).rgba*splat_control1.bbaa;
			normals_combined+=tex2Dlod(_BumpMap45, rayPos.xyww).rgba*splat_control2.rrgg;
			normals_combined+=tex2Dlod(_BumpMap67, rayPos.xyww).rgba*splat_control2.bbaa;
			n.xy=(normals_combined.rg+normals_combined.ba)*2-1;
			n.xy*=_uv_Relief_z;
			n.z = sqrt(1 - saturate(dot(n.xy, n.xy)));
			o.Normal=n;
	        			

			IN_uv_Relief_Offset.xy=rayPos.xy;
			

	 	} 
		else 
			if (splat_controlA_coverage>splat_controlB_coverage)
	 #endif // !_4LAYERS 		
	 	{
	 		//////////////////////////////////
	 		//
	 		// splats 0-3 close
	 		//
	 		//////////////////////////////////
			
			#if !defined(_4LAYERS) || defined(RTP_USE_COLOR_ATLAS)
				#if !defined(RTP_TRIPLANAR)
					uvSplat01=frac(rayPos.xy).xyxy*_mult+_off;
					uvSplat01.zw+=float2(0.5,0);
					uvSplat23=uvSplat01.xyxy+float4(0,0.5,0.5,0.5);
				#endif
			#endif			

		
			fixed4 c;
			float4 gloss=0;
			fixed3 col2 = 0;

				#if !defined(_4LAYERS) || defined(RTP_USE_COLOR_ATLAS)

					float _MipActual=min(mip_selector.x + rtp_mipoffset_color, 6);
					c = tex2Dlod(_SplatAtlasA, float4(uvSplat01.xy, _MipActual.xx)); col = splat_control1.x * c.rgb; gloss.r = c.a;
					c = tex2Dlod(_SplatAtlasA, float4(uvSplat01.zw, _MipActual.xx)); col += splat_control1.y * c.rgb; gloss.g = c.a;
					c = tex2Dlod(_SplatAtlasA, float4(uvSplat23.xy, _MipActual.xx)); col += splat_control1.z * c.rgb; gloss.b = c.a;
					c = tex2Dlod(_SplatAtlasA, float4(uvSplat23.zw, _MipActual.xx)); col += splat_control1.w * c.rgb; gloss.a = c.a;



				#else

					rayPos.w=mip_selector.x+rtp_mipoffset_color;
					c = tex2Dd(_SplatA0, rayPos.xy, _ddxMain.xy, _ddyMain.xy); col = splat_control1.x * c.rgb; gloss.r = c.a;
					c = tex2Dd(_SplatA1, rayPos.xy, _ddxMain.xy, _ddyMain.xy); col += splat_control1.y * c.rgb; gloss.g = c.a;
					c = tex2Dd(_SplatA2, rayPos.xy, _ddxMain.xy, _ddyMain.xy); col += splat_control1.z * c.rgb; gloss.b = c.a;
					c = tex2Dd(_SplatA3, rayPos.xy, _ddxMain.xy, _ddyMain.xy); col += splat_control1.w * c.rgb; gloss.a = c.a;
						
				#endif

			float glcombined = dot(gloss, splat_control1);
									
		
			glcombined=saturate(glcombined - glcombined*dot(splat_control1, RTP_AO_0123) * saturate((mip_selector.x+rtp_mipoffset_color)*0.2) );
			float RTP_gloss2mask = dot(splat_control1, RTP_gloss2mask0123);
			float _Spec = dot(splat_control1_nobleed, _Spec0123); // anti-bleed subtraction
			float RTP_gloss_mult = dot(splat_control1, RTP_gloss_mult0123);
			float RTP_gloss_shaping = dot(splat_control1, RTP_gloss_shaping0123);
			float gls = saturate(glcombined * RTP_gloss_mult);
			o_Gloss =  lerp(1, gls, RTP_gloss2mask) * _Spec;
			
			float2 gloss_shaped=float2(gls, 1-gls);
			gloss_shaped=gloss_shaped*gloss_shaped*gloss_shaped;
			gls=lerp(gloss_shaped.x, 1-gloss_shaped.y, RTP_gloss_shaping);
			o.Specular = saturate(gls);
			// gloss vs. fresnel dependency

			half colDesat=dot(col,0.33333);
			float brightness2Spec=dot(splat_control1, _LayerBrightness2Spec0123);
			o_Gloss*=lerp(1, colDesat, brightness2Spec);
			colAlbedo=col;
			col=lerp(colDesat.xxx, col, dot(splat_control1, _LayerSaturation0123));
			col*=dot(splat_control1, _LayerBrightness0123);  
			//col = lerp(col2,col,IN.uv_Control.x);
			//col = col;
			#if !defined(RTP_TRIPLANAR)
				float3 n;
				float4 normals_combined;
				rayPos.w=mip_selector.x+rtp_mipoffset_bump;
							
				normals_combined = tex2Dd(_BumpMap01, rayPos.xy, _ddxMain.xy, _ddyMain.xy).rgba*splat_control1.rrgg;
				#if !defined(USE_2_LAYERS_ONLY)
				normals_combined+=tex2Dd(_BumpMap23, rayPos.xy, _ddxMain.xy, _ddyMain.xy).rgba*splat_control1.bbaa;
				#endif							
				n.xy=(normals_combined.rg+normals_combined.ba)*2-1;
				n.xy*=_uv_Relief_z;
				n.z = sqrt(1 - saturate(dot(n.xy, n.xy)));
				o.Normal=n;
			#else
				// normalne wyliczone powyżej
			#endif
			
		
			
			IN_uv_Relief_Offset.xy=rayPos.xy;

	 	}
		#ifndef _4LAYERS 		
	 	else 
		{
	 		//////////////////////////////////
	 		//
	 		// splats 4-7 close
	 		//
	 		//////////////////////////////////
	 				

			uvSplat01=frac(rayPos.xy).xyxy*_mult+_off;
			uvSplat01.zw+=float2(0.5,0);
			uvSplat23=uvSplat01.xyxy+float4(0,0.5,0.5,0.5);
 	
			float4 c;
			float4 gloss;
			float _MipActual=min(mip_selector.x + rtp_mipoffset_color,6);
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat01.xy, _MipActual.xx)); col = splat_control2.x * c.rgb; gloss.r = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat01.zw, _MipActual.xx)); col += splat_control2.y * c.rgb; gloss.g = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat23.xy, _MipActual.xx)); col += splat_control2.z * c.rgb; gloss.b = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat23.zw, _MipActual.xx)); col += splat_control2.w * c.rgb ; gloss.a = c.a;


			float glcombined = dot(gloss, splat_control2);		
			
			
			float RTP_gloss2mask = dot(splat_control2, RTP_gloss2mask4567);
			float _Spec = dot(splat_control2_nobleed, _Spec4567); // anti-bleed subtraction
			float RTP_gloss_mult = dot(splat_control2, RTP_gloss_mult4567);
			float RTP_gloss_shaping = dot(splat_control2, RTP_gloss_shaping4567);
			float gls = saturate(glcombined * RTP_gloss_mult);
			o_Gloss =  lerp(1, gls, RTP_gloss2mask) * _Spec;
			float2 gloss_shaped=float2(gls, 1-gls);
			gloss_shaped=gloss_shaped*gloss_shaped*gloss_shaped;
			gls=lerp(gloss_shaped.x, 1-gloss_shaped.y, RTP_gloss_shaping);
			o.Specular = saturate(gls);
			// gloss vs. fresnel dependency
			
			half colDesat=dot(col,0.33333);
			float brightness2Spec=dot(splat_control2, _LayerBrightness2Spec4567);
			o_Gloss*=lerp(1, colDesat, brightness2Spec);
			colAlbedo=col;
			col=lerp(colDesat.xxx, col, dot(splat_control2, _LayerSaturation4567));	
			col*=dot(splat_control2, _LayerBrightness4567);  
			//col = lerp(0,col,IN.uv_Control.x);
			//col = col;
			float3 n;
			float4 normals_combined;
			rayPos.w=mip_selector.x+rtp_mipoffset_bump;

			normals_combined = tex2Dlod(_BumpMap45, rayPos.xyww).rgba*splat_control2.rrgg;
			normals_combined+=tex2Dlod(_BumpMap67, rayPos.xyww).rgba*splat_control2.bbaa;
			n.xy=(normals_combined.rg+normals_combined.ba)*2-1;
			n.xy*=_uv_Relief_z;
			n.z = sqrt(1 - saturate(dot(n.xy, n.xy)));
			o.Normal=n;


		 	IN_uv_Relief_Offset.xy=rayPos.xy;
		 	
	 	
	 	}
		#endif //!_4LAYERS 
		
	
		#if defined(SIMPLE_FAR) && defined(COLOR_MAP)
			o_Gloss*=(1-_uv_Relief_w);
			o.Specular=lerp(o.Specular, 0, _uv_Relief_w);
		#endif			
	#endif
	} else {
 		//////////////////////////////////
 		//
 		// far
 		//
 		//////////////////////////////////
				
		#if !defined(_4LAYERS) || defined(RTP_USE_COLOR_ATLAS)
		float _off=16*_SplatAtlasA_TexelSize.x;
		float _mult=_off*-2+0.5;
		float4 _offMix=_off;
		float4 _multMix=_mult;
		
		float4 uvSplat01, uvSplat23;
		#if !defined(RTP_TRIPLANAR) || !defined(_4LAYERS)
			float hi_mip_adjust=(exp2(min(mip_selector.x+rtp_mipoffset_color,6)))*_SplatAtlasA_TexelSize.x;
			_mult-=hi_mip_adjust;
			_off+=0.5*hi_mip_adjust;
			
			uvSplat01=frac(_INPUT_uv.xy).xyxy*_mult+_off;
			uvSplat01.zw+=float2(0.5,0);
			uvSplat23=uvSplat01.xyxy+float4(0,0.5,0.5,0.5);
		#endif		
		
		float4 uvSplat01M, uvSplat23M;
		#endif
		
		
	
			
	#ifdef TRANSITION
		
		//	if (splat_controlA_coverage>0.01 && splat_controlB_coverage>0.01) {
		
			///////////////////////////////////////////////
	 		//
	 		// splats 0-7 far combined transition
	 		//
	 		///////////////////////////////////////////////
			
	 		//#ifdef RTP_SHOW_OVERLAPPED
	 		//o.Emission.r=1;
	 	//	#endif
	 		
		
		//#if defined(SIMPLE_FAR) && defined(COLOR_MAP)
		//	if (_uv_Relief_w<1) {
		//#endif
		
		
			fixed3 col2 = 0;
			float4 _MipActual=min(mip_selector.x + rtp_mipoffset_color+MIPmult0123, 6);
			half4 c;
			float4 gloss;
			
			c = tex2Dlod(_SplatAtlasA, float4(uvSplat01.xy, _MipActual.xx)); col = splat_control1.x * c.rgb; gloss.r = c.a;
			c = tex2Dlod(_SplatAtlasA, float4(uvSplat01.zw, _MipActual.yy)); col += splat_control1.y * c.rgb; gloss.g = c.a;
			c = tex2Dlod(_SplatAtlasA, float4(uvSplat23.xy, _MipActual.zz)); col += splat_control1.z * c.rgb; gloss.b = c.a;
			c = tex2Dlod(_SplatAtlasA, float4(uvSplat23.zw, _MipActual.ww)); col += splat_control1.w * c.rgb; gloss.a = c.a;
			
			float glcombinedA = dot(gloss, splat_control1);					
						
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat01.xy, _MipActual.xx)); col2 = splat_control2.x * c.rgb; gloss.r = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat01.zw, _MipActual.yy)); col2 += splat_control2.y * c.rgb; gloss.g = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat23.xy, _MipActual.zz)); col2 += splat_control2.z * c.rgb; gloss.b = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat23.zw, _MipActual.ww)); col2 += splat_control2.w * c.rgb; gloss.a = c.a;
			
			float glcombinedB = dot(gloss, splat_control2);
				
			
			glcombinedA=saturate(glcombinedA - glcombinedA*dot(splat_control1, RTP_AO_0123) * saturate((mip_selector.x+rtp_mipoffset_color)*0.2) );
			glcombinedB=saturate(glcombinedB - glcombinedB*dot(splat_control2, RTP_AO_4567) * saturate((mip_selector.x+rtp_mipoffset_color)*0.2) );
			
			//float glcombined = dot(gloss, splat_control1);
	
		//first	
			float RTP_gloss2mask = dot(splat_control1, RTP_gloss2mask0123);
			float _Spec = dot(splat_control1_nobleed, _Spec0123); // anti-bleed subtraction
			float RTP_gloss_mult = dot(splat_control1, RTP_gloss_mult0123);
			float RTP_gloss_shaping = dot(splat_control1, RTP_gloss_shaping0123);
			float gls = saturate(glcombinedA * RTP_gloss_mult);
			
			float GlossA = lerp(1, gls, RTP_gloss2mask) * _Spec;
			
			
			float2 gloss_shaped=float2(gls, 1-gls);
			gloss_shaped=gloss_shaped*gloss_shaped*gloss_shaped;
			gls=lerp(gloss_shaped.x, 1-gloss_shaped.y, RTP_gloss_shaping);
			
			float SpecularA = saturate(gls);
			
			half colDesatA=dot(col,0.33333);
			float brightness2SpecA=dot(splat_control1, _LayerBrightness2Spec0123);
			GlossA*=lerp(1, colDesatA, brightness2SpecA);
			fixed3 colAlbedoA = col;
			
			col=lerp(colDesatA.xxx, col, dot(splat_control1, _LayerSaturation0123));
			col*=dot(splat_control1, _LayerBrightness0123);
		//second
			RTP_gloss2mask = dot(splat_control2, RTP_gloss2mask4567);
			_Spec = dot(splat_control2_nobleed, _Spec4567); // anti-bleed subtraction
			RTP_gloss_mult = dot(splat_control2, RTP_gloss_mult4567);
			RTP_gloss_shaping = dot(splat_control2, RTP_gloss_shaping4567);
			gls = saturate(glcombinedB * RTP_gloss_mult);
			
			float GlossB = lerp(1, gls, RTP_gloss2mask) * _Spec;
			
			float2 gloss_shaped2=float2(gls, 1-gls);
			gloss_shaped2=gloss_shaped2*gloss_shaped2*gloss_shaped2;
			gls=lerp(gloss_shaped.x, 1-gloss_shaped.y, RTP_gloss_shaping);
			
			float SpecularB = saturate(gls);
			
			half colDesatB=dot(col2,0.33333);
			float brightness2SpecB=dot(splat_control2, _LayerBrightness2Spec4567);
			GlossB*=lerp(1, colDesatB, brightness2SpecB);
			fixed3 colAlbedoB = col2;
			
			col2=lerp(colDesatB.xxx, col2, dot(splat_control2, _LayerSaturation4567));
			col2*=dot(splat_control2, _LayerBrightness4567);
			
			
			o.Specular =  lerp(SpecularA,SpecularB,1-IN.color.y*_TransitionSlider);
			
			col = lerp(col,col2,1-IN.color.y*_TransitionSlider);
			o_Gloss =  lerp(GlossA,GlossB,1-IN.color.y*_TransitionSlider);
			
			
			//col = (0,col,IN.uv_Control.x);	
			//col = col;
				
		//#if defined(SIMPLE_FAR) && defined(COLOR_MAP)
		//	o_Gloss*=(1-_uv_Relief_w);
		//	o.Specular=lerp(o.Specular, 0, _uv_Relief_w);
		//	}
		//#endif		
			
	#endif
	
	#ifndef TRANSITION
		
		#if !defined(_4LAYERS) 

		 	if (splat_controlA_coverage>0.01 && splat_controlB_coverage>0.01) { 	
	 		//////////////////////////////////////////////
	 		//
	 		// splats 0-7 far combined
	 		//
	 		///////////////////////////////////////////////
	 		#ifdef RTP_SHOW_OVERLAPPED
	 		o.Emission.r=1;
	 		#endif
	 		
		
		#if defined(SIMPLE_FAR) && defined(COLOR_MAP)
			if (_uv_Relief_w<1) {
		#endif
				fixed3 col2;
			float4 _MipActual=min(mip_selector.x + rtp_mipoffset_color+MIPmult0123, 6);
			half4 c;
			float4 gloss;
			c = tex2Dlod(_SplatAtlasA, float4(uvSplat01.xy, _MipActual.xx)); col = splat_control1.x * c.rgb; gloss.r = c.a;
			c = tex2Dlod(_SplatAtlasA, float4(uvSplat01.zw, _MipActual.yy)); col += splat_control1.y * c.rgb; gloss.g = c.a;
			c = tex2Dlod(_SplatAtlasA, float4(uvSplat23.xy, _MipActual.zz)); col += splat_control1.z * c.rgb; gloss.b = c.a;
			c = tex2Dlod(_SplatAtlasA, float4(uvSplat23.zw, _MipActual.ww)); col += splat_control1.w * c.rgb; gloss.a = c.a;
			
			float glcombined = dot(gloss, splat_control1);
							
			
						
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat01.xy, _MipActual.xx)); col += splat_control2.x * c.rgb; gloss.r = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat01.zw, _MipActual.yy)); col += splat_control2.y * c.rgb; gloss.g = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat23.xy, _MipActual.zz)); col += splat_control2.z * c.rgb; gloss.b = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat23.zw, _MipActual.ww)); col += splat_control2.w * c.rgb; gloss.a = c.a;
			glcombined += dot(gloss, splat_control2);
						
		
			float RTP_gloss2mask = dot(splat_control1, RTP_gloss2mask0123)+dot(splat_control2, RTP_gloss2mask4567);
			float _Spec = dot(splat_control1_nobleed, _Spec0123) + dot(splat_control2_nobleed, _Spec4567); // anti-bleed subtraction
			float RTP_gloss_mult = dot(splat_control1, RTP_gloss_mult0123)+dot(splat_control2, RTP_gloss_mult4567);
			float RTP_gloss_shaping = dot(splat_control1, RTP_gloss_shaping0123)+dot(splat_control2, RTP_gloss_shaping4567);
			float gls = saturate(glcombined * RTP_gloss_mult);
			o_Gloss =  lerp(1, gls, RTP_gloss2mask) * _Spec; // *heightblend_AO // przemnóż przez damping dla warstw 4567
			float2 gloss_shaped=float2(gls, 1-gls);
			gloss_shaped=gloss_shaped*gloss_shaped*gloss_shaped;
			gls=lerp(gloss_shaped.x, 1-gloss_shaped.y, RTP_gloss_shaping);
			o.Specular = saturate(gls);
			// gloss vs. fresnel dependency
			#if ( defined(RTP_REFLECTION) && !defined(UNITY_PASS_SHADOWCASTER) ) || ( defined(RTP_IBL_SPEC) && !defined(UNITY_PASS_SHADOWCASTER) ) || defined(RTP_PBL_FRESNEL)
			o.RTP.x*=lerp(1, 1-fresnelAtten, o.Specular*0.9+0.1);
			#endif
			half colDesat=dot(col,0.33333);
			float brightness2Spec=dot(splat_control1, _LayerBrightness2Spec0123) + dot(splat_control2, _LayerBrightness2Spec4567);
			o_Gloss*=lerp(1, colDesat, brightness2Spec);
			colAlbedo=col;
			col=lerp(colDesat.xxx, col, dot(splat_control1, _LayerSaturation0123) + dot(splat_control2, _LayerSaturation4567));
			col*=dot(splat_control1, _LayerBrightness0123) + dot(splat_control2, _LayerBrightness4567);  
			//col = lerp(col2,col,IN.uv_Control.x);	
			//col = col;			
		#if defined(SIMPLE_FAR) && defined(COLOR_MAP)
			o_Gloss*=(1-_uv_Relief_w);
			o.Specular=lerp(o.Specular, 0, _uv_Relief_w);
			}
		#endif

						
	 	} else if (splat_controlA_coverage>splat_controlB_coverage)
		#endif // !_4LAYERS
	 	{
	 		//////////////////////////////////////////////
	 		//
	 		// splats 0-3 far
	 		//
	 		///////////////////////////////////////////////
		
		#if defined(SIMPLE_FAR) && defined(COLOR_MAP)
			if (_uv_Relief_w<1) {
		#endif			

			half4 c;
			float4 gloss=0;			
	
			

				#if !defined(_4LAYERS) || defined(RTP_USE_COLOR_ATLAS)
					float4 _MixMipActual=min(mip_selector.xxxx + rtp_mipoffset_color+MIPmult0123, float4(6,6,6,6));
					c = tex2Dlod(_SplatAtlasA, float4(uvSplat01.xy, _MixMipActual.xx)); col = splat_control1.x * c.rgb; gloss.r = c.a;
					c = tex2Dlod(_SplatAtlasA, float4(uvSplat01.zw, _MixMipActual.yy)); col += splat_control1.y * c.rgb; gloss.g = c.a;
					#if !defined(USE_2_LAYERS_ONLY)
						c = tex2Dlod(_SplatAtlasA, float4(uvSplat23.xy, _MixMipActual.zz)); col += splat_control1.z * c.rgb; gloss.b = c.a;
						#if !defined(USE_3_LAYERS_ONLY)
							c = tex2Dlod(_SplatAtlasA, float4(uvSplat23.zw, _MixMipActual.ww)); col += splat_control1.w * c.rgb; gloss.a = c.a;
						#endif
					#endif							
				#else
					float4 _MixMipActual=mip_selector.xxxx + rtp_mipoffset_color+MIPmult0123;
					float4 mipTweak=exp2(MIPmult0123);
					c = tex2Dd(_SplatA0, _Control2uv, _ddxMain.xy*mipTweak.x, _ddyMain.xy*mipTweak.x); col = splat_control1.x * c.rgb; gloss.r = c.a;
					c = tex2Dd(_SplatA1, _Control2uv, _ddxMain.xy*mipTweak.y, _ddyMain.xy*mipTweak.y); col += splat_control1.y * c.rgb; gloss.g = c.a;
					#if !defined(USE_2_LAYERS_ONLY)
						c = tex2Dd(_SplatA2, _Control2uv, _ddxMain.xy*mipTweak.z, _ddyMain.xy*mipTweak.z); col += splat_control1.z * c.rgb; gloss.b = c.a;
						#if !defined(USE_3_LAYERS_ONLY)
							c = tex2Dd(_SplatA3, _Control2uv, _ddxMain.xy*mipTweak.w, _ddyMain.xy*mipTweak.w); col += splat_control1.w * c.rgb; gloss.a = c.a;				
						#endif
					#endif							
				#endif
			


			
			
			float glcombined = dot(gloss, splat_control1);
	
			
			glcombined=saturate(glcombined - glcombined*dot(splat_control1, RTP_AO_0123) * saturate((mip_selector.x+rtp_mipoffset_color)*0.2) );
			float RTP_gloss2mask = dot(splat_control1, RTP_gloss2mask0123);
			float _Spec = dot(splat_control1_nobleed, _Spec0123); // anti-bleed subtraction
			float RTP_gloss_mult = dot(splat_control1, RTP_gloss_mult0123);
			float RTP_gloss_shaping = dot(splat_control1, RTP_gloss_shaping0123);
			float gls = saturate(glcombined * RTP_gloss_mult);
			o_Gloss =  lerp(1, gls, RTP_gloss2mask) * _Spec;
			float2 gloss_shaped=float2(gls, 1-gls);
			gloss_shaped=gloss_shaped*gloss_shaped*gloss_shaped;
			gls=lerp(gloss_shaped.x, 1-gloss_shaped.y, RTP_gloss_shaping);
			o.Specular = saturate(gls);
			// gloss vs. fresnel dependency
			#if ( defined(RTP_REFLECTION) && !defined(UNITY_PASS_SHADOWCASTER) ) || ( defined(RTP_IBL_SPEC) && !defined(UNITY_PASS_SHADOWCASTER) ) || defined(RTP_PBL_FRESNEL)
			o.RTP.x*=lerp(1, 1-fresnelAtten, o.Specular*0.9+0.1);		
			#endif
			half colDesat=dot(col,0.33333);
			float brightness2Spec=dot(splat_control1, _LayerBrightness2Spec0123);
			o_Gloss*=lerp(1, colDesat, brightness2Spec);
			colAlbedo=col;
			col=lerp(colDesat.xxx, col, dot(splat_control1, _LayerSaturation0123) );
			col*=dot(splat_control1, _LayerBrightness0123);  
			//col = (0,col,IN.uv_Control.x);	
			//col = col;
		#if defined(SIMPLE_FAR) && defined(COLOR_MAP)
			o_Gloss*=(1-_uv_Relief_w);
			o.Specular=lerp(o.Specular, 0, _uv_Relief_w);
			}
		#endif		
		
						
	 	}
	 	
	 	#ifndef _4LAYERS
	 	else {
	 		//////////////////////////////////////////////
	 		//
	 		// splats 4-7 far
	 		//
	 		///////////////////////////////////////////////
	 		
		

			#if defined(SIMPLE_FAR) && defined(COLOR_MAP)
			if (_uv_Relief_w<1) {
			#endif	
			float4 _MixMipActual=min(mip_selector.xxxx + rtp_mipoffset_color+MIPmult4567, float4(6,6,6,6));

			half4 c;
			float4 gloss;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat01.xy, _MixMipActual.xx)); col = splat_control2.x * c.rgb; gloss.r = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat01.zw, _MixMipActual.yy)); col += splat_control2.y * c.rgb; gloss.g = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat23.xy, _MixMipActual.zz)); col += splat_control2.z * c.rgb; gloss.b = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat23.zw, _MixMipActual.ww)); col += splat_control2.w * c.rgb; gloss.a = c.a;
								
			float glcombined = dot(gloss, splat_control2);
		

			float RTP_gloss2mask = dot(splat_control2, RTP_gloss2mask4567);
			float _Spec = dot(splat_control2_nobleed, _Spec4567); // anti-bleed subtraction
			float RTP_gloss_mult = dot(splat_control2, RTP_gloss_mult4567);
			float RTP_gloss_shaping = dot(splat_control2, RTP_gloss_shaping4567);
			float gls = saturate(glcombined * RTP_gloss_mult);
			o_Gloss =  lerp(1, gls, RTP_gloss2mask) * _Spec;
			float2 gloss_shaped=float2(gls, 1-gls);
			gloss_shaped=gloss_shaped*gloss_shaped*gloss_shaped;
			gls=lerp(gloss_shaped.x, 1-gloss_shaped.y, RTP_gloss_shaping);
			o.Specular = saturate(gls);

			half colDesat=dot(col,0.33333);
			float brightness2Spec=dot(splat_control2, _LayerBrightness2Spec4567);
			o_Gloss*=lerp(1, colDesat, brightness2Spec);
			colAlbedo=col;
			col=lerp(colDesat.xxx, col, dot(splat_control2, _LayerSaturation4567) );
			col*=dot(splat_control2, _LayerBrightness4567);  
			//col = lerp(col,0,IN.uv_Control.x);
			//col = col;			
			#if defined(SIMPLE_FAR) && defined(COLOR_MAP)
			o_Gloss*=(1-_uv_Relief_w);
			o.Specular=lerp(o.Specular, 0, _uv_Relief_w);
			}
			#endif						
	 	}
		#endif
	#endif	

	 	IN_uv_Relief_Offset.xy=_INPUT_uv.xy;
	}
	float3 norm_snowCov=o.Normal;
	
	// far distance normals from uv blend scale
	
	float _BumpMapGlobalStrengthPerLayer=1;
	
	#ifdef TRANSITION
	float _BumpMapGlobalStrengthPerLayer2=1;
		_BumpMapGlobalStrengthPerLayer=dot(_BumpMapGlobalStrength0123, splat_control1);
		_BumpMapGlobalStrengthPerLayer2=dot(_BumpMapGlobalStrength4567, splat_control2);
	#else
		#ifdef _4LAYERS
			_BumpMapGlobalStrengthPerLayer=dot(_BumpMapGlobalStrength0123, splat_control1);
		#else
			_BumpMapGlobalStrengthPerLayer=dot(_BumpMapGlobalStrength0123, splat_control1)+dot(_BumpMapGlobalStrength4567, splat_control2);
		#endif
	#endif
	

		float3 combinedNormal = BlendNormalsRTP(norm_far, o.Normal);
	#ifdef TRANSITION
		o.Normal = lerp(o.Normal, combinedNormal, saturate(lerp(rtp_perlin_start_val,1, _uv_Relief_w) * lerp(_BumpMapGlobalStrengthPerLayer,_BumpMapGlobalStrengthPerLayer2,1-IN.color.y*_TransitionSlider)));
	#else	
		o.Normal = lerp(o.Normal, combinedNormal, saturate(lerp(rtp_perlin_start_val,1, _uv_Relief_w) * _BumpMapGlobalStrengthPerLayer));
	#endif
	//#ifdef TRANSITION
	
	//#else
	#ifdef TRANSITION
		o_Gloss=lerp(lerp( saturate(o_Gloss+4*dot(splat_control1_nobleed, 1)), o_Gloss, (1-_uv_Relief_w)*(1-_uv_Relief_w) ),lerp( saturate(o_Gloss+4*dot(splat_control2_nobleed, 1)), o_Gloss, (1-_uv_Relief_w)*(1-_uv_Relief_w) ),1-IN.color.y*_TransitionSlider);
	#else	
	#ifdef _4LAYERS
	o_Gloss=lerp( saturate(o_Gloss+4*dot(splat_control1_nobleed, 1)), o_Gloss, (1-_uv_Relief_w)*(1-_uv_Relief_w) );
	#else
	o_Gloss=lerp( saturate(o_Gloss+4*(dot(splat_control1_nobleed, 1) + dot(splat_control2_nobleed, 1))), o_Gloss, (1-_uv_Relief_w)*(1-_uv_Relief_w) );
	#endif
	#endif
	//#endif
	#if ( defined(RTP_IBL_DIFFUSE) && !defined(UNITY_PASS_SHADOWCASTER) )
		half3 colBrightnessNotAffectedByColormap=col.rgb;
	#endif

	o.Albedo=col;
	o.Normal=normalize(o.Normal);
	
	// używane ew. w triplanar standalone
	#if defined(VERTEX_COLOR_AO_DAMP)
		o.RTP.y*=VERTEX_COLOR_AO_DAMP;
	#endif
	#if defined(VERTEX_COLOR_BLEND)
		o.Albedo=lerp(o.Albedo*TERRAIN_VertexColorBlend*2, o.Albedo, VERTEX_COLOR_BLEND);
	#endif
	
	// heightblend AO
	#ifdef RTP_HEIGHTBLEND_AO
		#if defined(TRANSITION)
			float heightblend_AO=saturate(dot(0.5-abs(lerp(splat_control1_mid, splat_control1_close, RTP_AOsharpness)-0.5), lerp(RTP_AO_0123,RTP_AO_4567,1-IN.color.y))*RTP_AOamp);
		#else
		#if defined(_4LAYERS)
			float heightblend_AO=saturate(dot(0.5-abs(lerp(splat_control1_mid, splat_control1_close, RTP_AOsharpness)-0.5), RTP_AO_0123)*RTP_AOamp);
		#else
			float heightblend_AO=saturate(( dot(0.5-abs(lerp(splat_control1_mid, splat_control1_close, RTP_AOsharpness)-0.5), RTP_AO_0123) + dot(0.5-abs(lerp(splat_control2_mid, splat_control2_close, RTP_AOsharpness)-0.5), RTP_AO_4567) )*RTP_AOamp);
		#endif	
		#if ( defined(RTP_SNOW) && !defined(UNITY_PASS_SHADOWCASTER) )
			heightblend_AO*=snow_damp;
		#endif
		#endif
		//o.RTP.y*=1-heightblend_AO; // mnożone później (po emission glow, bo ten tłumi AO)
	#endif		
	
	// emission of layer (glow)
		
	#ifdef RTP_HEIGHTBLEND_AO
		o.RTP.y*=1-heightblend_AO;
	#endif		
	
	#ifdef _4LAYERS
		float IBL_bump_smoothness=dot(splat_control1, RTP_IBL_bump_smoothness0123);
		#if ( defined(RTP_IBL_DIFFUSE) && !defined(UNITY_PASS_SHADOWCASTER) )
			float RTP_IBL_DiffStrength=dot(splat_control1, RTP_IBL_DiffuseStrength0123);
		#endif
		#if ( defined(RTP_IBL_SPEC) && !defined(UNITY_PASS_SHADOWCASTER) ) || ( defined(RTP_REFLECTION) && !defined(UNITY_PASS_SHADOWCASTER) )
			// anti-bleed subtraction
			float RTP_IBL_SpecStrength=dot(splat_control1_nobleed, RTP_IBL_SpecStrength0123);
		#endif
	#else
		float IBL_bump_smoothness=dot(splat_control1, RTP_IBL_bump_smoothness0123)+dot(splat_control2, RTP_IBL_bump_smoothness4567);
		#if ( defined(RTP_IBL_DIFFUSE) && !defined(UNITY_PASS_SHADOWCASTER) )
			float RTP_IBL_DiffStrength=dot(splat_control1, RTP_IBL_DiffuseStrength0123)+dot(splat_control2, RTP_IBL_DiffuseStrength4567);
		#endif
		#if ( defined(RTP_IBL_SPEC) && !defined(UNITY_PASS_SHADOWCASTER) ) || ( defined(RTP_REFLECTION) && !defined(UNITY_PASS_SHADOWCASTER) )
			// anti-bleed subtraction
			float RTP_IBL_SpecStrength=dot(splat_control1_nobleed, RTP_IBL_SpecStrength0123)+dot(splat_control2_nobleed, RTP_IBL_SpecStrength4567);
//			float RTP_IBL_SpecStrength=dot(splat_control1, RTP_IBL_SpecStrength0123)+dot(splat_control2, RTP_IBL_SpecStrength4567);
		#endif
	#endif	
	// lerp IBL values with wet / snow
	#if ( defined(RTP_IBL_DIFFUSE) && !defined(UNITY_PASS_SHADOWCASTER) )
		#if ( defined(RTP_SNOW) && !defined(UNITY_PASS_SHADOWCASTER) )
			RTP_IBL_DiffStrength=lerp(RTP_IBL_DiffStrength, rtp_snow_IBL_DiffuseStrength, snow_val);
		#endif
	#endif
	#if ( defined(RTP_IBL_SPEC) && !defined(UNITY_PASS_SHADOWCASTER) ) || ( defined(RTP_REFLECTION) && !defined(UNITY_PASS_SHADOWCASTER) )
		#if ( defined(RTP_WETNESS) && !defined(UNITY_PASS_SHADOWCASTER) )
			RTP_IBL_SpecStrength=lerp(RTP_IBL_SpecStrength, TERRAIN_WaterIBL_SpecWetStrength, TERRAIN_LayerWetStrength);
			RTP_IBL_SpecStrength=lerp(RTP_IBL_SpecStrength, TERRAIN_WaterIBL_SpecWaterStrength, p*p);
		#endif
		#if ( defined(RTP_SNOW) && !defined(UNITY_PASS_SHADOWCASTER) )
			RTP_IBL_SpecStrength=lerp(RTP_IBL_SpecStrength, rtp_snow_IBL_SpecStrength, snow_val);
		#endif
	#endif

	#if defined(UNITY_PASS_SHADOWCASTER)
		float3 normalW=float3(0,0,1);// dummy value in caster pass (we don't have an access to WorldNormalVector due to lack of INTERNAL data)
	#else
		float3 normalW=WorldNormalVector(IN, o.Normal);
	#endif
	
//	o.Emission=normalW;
//	o.Albedo=0;
	#ifdef NO_SPECULARITY
	o.SpecColor=0;
	o.Specular=0;
	#endif
//	return;
	
	float3 IBLNormal=lerp(o.Normal, float3(0,0,1), IBL_bump_smoothness);
	#if defined(UNITY_PASS_SHADOWCASTER)
		float3 reflVec=float3(0,0,1);// dummy value in caster pass (we don't have an access to WorldNormalVector due to lack of INTERNAL data)
	#else
		float3 reflVec=WorldReflectionVector(IN, IBLNormal);
	#endif
	{
	#if ( defined(RTP_REFLECTION) && !defined(UNITY_PASS_SHADOWCASTER) ) || ( defined(RTP_IBL_SPEC) && !defined(UNITY_PASS_SHADOWCASTER) )
			#if ( defined(RTP_SKYSHOP_SYNC) && !defined(UNITY_PASS_SHADOWCASTER) ) && defined(RTP_SKYSHOP_SKY_ROTATION)
			 reflVec = _SkyMatrix[0].xyz*reflVec.x + _SkyMatrix[1].xyz*reflVec.y + _SkyMatrix[2].xyz*reflVec.z;
			#endif		
		#endif
   		#if ( defined(RTP_SKYSHOP_SYNC) && !defined(UNITY_PASS_SHADOWCASTER) ) && defined(RTP_SKYSHOP_SKY_ROTATION)
			normalW = _SkyMatrix[0].xyz*normalW.x + _SkyMatrix[1].xyz*normalW.y + _SkyMatrix[2].xyz*normalW.z;
		#endif	
	}
	
	// ^4 shaped diffuse fresnel term for soft surface layers (grass)
	float _DiffFresnel=0;
	#ifdef _4LAYERS
		_DiffFresnel=dot(splat_control1, RTP_DiffFresnel0123);
	#else
		_DiffFresnel=dot(splat_control1, RTP_DiffFresnel0123)+dot(splat_control2, RTP_DiffFresnel4567);
	#endif
	// diffuse fresnel term for snow
	#if ( defined(RTP_SNOW) && !defined(UNITY_PASS_SHADOWCASTER) )
		_DiffFresnel=lerp(_DiffFresnel, rtp_snow_diff_fresnel, snow_val);
	#endif
	float diffuseScatteringFactor=1.0 + diffFresnel*_DiffFresnel;
	o.Albedo *= diffuseScatteringFactor;
	#if ( defined(RTP_IBL_DIFFUSE) && !defined(UNITY_PASS_SHADOWCASTER) )
		colBrightnessNotAffectedByColormap *= diffuseScatteringFactor;
	#endif
	
	// spec color from albedo (metal tint)
	#ifdef _4LAYERS
		float Albedo2SpecColor=dot(splat_control1, _LayerAlbedo2SpecColor0123);
	#else
		float Albedo2SpecColor=dot(splat_control1, _LayerAlbedo2SpecColor0123) + dot(splat_control2, _LayerAlbedo2SpecColor4567);
	#endif
	#if ( defined(RTP_SNOW) && !defined(UNITY_PASS_SHADOWCASTER) )
		Albedo2SpecColor*=snow_damp;
	#endif
	#if ( defined(RTP_WETNESS) && !defined(UNITY_PASS_SHADOWCASTER) )
		colAlbedo=lerp(colAlbedo, o.Albedo, p);
	#endif
	// colAlbedo powinno być "normalizowane" (aby nie było za ciemne) jako tinta dla spec color
	float colAlbedoRGBmax=max(max(colAlbedo.r, colAlbedo.g), colAlbedo.b);
	colAlbedoRGBmax=max(colAlbedoRGBmax, 0.01);
	float3 colAlbedoNew=lerp(half3(1,1,1), colAlbedo.rgb/colAlbedoRGBmax.xxx, saturate(colAlbedoRGBmax*4)*Albedo2SpecColor);
	half3 SpecColor=_SpecColor.rgb*o_Gloss*colAlbedoNew*colAlbedoNew; // spec color for IBL/Refl
	o.SpecColor=SpecColor;

	

		
	#if defined(RTP_AMBIENT_EMISSIVE_MAP)
		float4 eMapVal=tex2Dp(_AmbientEmissiveMapGlobal, IN_uv_Control, _ddxGlobal, _ddyGlobal);
		o.Emission+=o.Albedo*eMapVal.rgb*_AmbientEmissiveMultiplier*lerp(1, saturate(o.Normal.z*o.Normal.z-(1-actH)*(1-o.Normal.z*o.Normal.z)), _AmbientEmissiveRelief);
	
		#ifdef RTP_STANDALONE
			float pixel_trees_shadow_val=saturate((_INPUT_distance - _shadow_distance_start) / _shadow_distance_transition);
			pixel_trees_shadow_val=lerp(1, eMapVal.a, pixel_trees_shadow_val);
			float o_RTP_y_without_shadowmap_distancefade=o.RTP.y*lerp(eMapVal.a, 1, _shadow_value);
			o.RTP.y*=lerp(_shadow_value, 1, pixel_trees_shadow_val);
		#else
			float pixel_trees_shadow_val=saturate((_INPUT_distance - _TERRAIN_trees_shadow_values.x) / _TERRAIN_trees_shadow_values.y);
			pixel_trees_shadow_val=lerp(1, eMapVal.a, pixel_trees_shadow_val);
			float o_RTP_y_without_shadowmap_distancefade=o.RTP.y*lerp(eMapVal.a, 1, _TERRAIN_trees_shadow_values.z);
			o.RTP.y*=lerp(_TERRAIN_trees_shadow_values.z, 1, pixel_trees_shadow_val);
		#endif		
	#endif	
	
	#if !defined(RTP_TREESGLOBAL) && !defined(RTP_AMBIENT_EMISSIVE_MAP)
		#define o_RTP_y_without_shadowmap_distancefade (o.RTP.y)
	#endif
	
	#if ( defined(RTP_IBL_DIFFUSE) && !defined(UNITY_PASS_SHADOWCASTER) )
		#if ( defined(RTP_SKYSHOP_SYNC) && !defined(UNITY_PASS_SHADOWCASTER) )
			half3 IBLDiffuseCol = SHLookup(normalW)*RTP_IBL_DiffStrength;
		#else
			#ifdef UNITY_PI
				half3 IBLDiffuseCol = ShadeSH9(float4(normalW,1))*RTP_IBL_DiffStrength;
				//IBLDiffuseCol=RTP_IBL_DiffStrength;
			#else
				half3 IBLDiffuseCol = DecodeRGBM(texCUBElod(_CubemapDiff, float4(normalW,0)))*RTP_IBL_DiffStrength;
			#endif
		#endif
		IBLDiffuseCol*=colBrightnessNotAffectedByColormap * lerp(1, o_RTP_y_without_shadowmap_distancefade, TERRAIN_IBL_DiffAO_Damp);
		#if ( defined(RTP_SKYSHOP_SYNC) && !defined(UNITY_PASS_SHADOWCASTER) )
			IBLDiffuseCol*=_ExposureIBL.x;
		#endif		
		#ifndef RTP_IBL_SPEC
		o.Emission += IBLDiffuseCol.rgb;
		#else
		// dodamy za chwilę poprzez introplację, która zachowa energie
		#endif
	#endif

	// kompresuję odwrotnie mip blur (łatwiej osiągnąć "lustro")
	float o_SpecularInvSquared = (1-o.Specular)*(1-o.Specular);
	
	#if ( defined(RTP_IBL_SPEC) && !defined(UNITY_PASS_SHADOWCASTER) )
		#define RTP_IBLSPEC_BUMPED
		#ifdef RTP_IBLSPEC_BUMPED
			float n_dot_v = saturate(dot(IBLNormal, IN.viewDir));
			float exponential = exp2(SchlickFresnelApproxExp2Const*n_dot_v);
		#else
			float exponential=diffFresnel;
		#endif
		// skyshop fit (I'd like people to get similar results in gamma / linear)
		#if defined(RTP_COLORSPACE_LINEAR)
			exponential=0.03+0.97*exponential;
		#else
			exponential=0.25+0.75*exponential;
		#endif
		float spec_fresnel = lerp (1.0f, exponential, o.RTP.x);
		
		// U5 reflection probe
		#ifdef UNITY_PI
			half4 rgbm = SampleCubeReflection( unity_SpecCube0, reflVec, o_SpecularInvSquared*(6-exponential*o.RTP.x*3) );
			half3 IBLSpecCol = DecodeHDR_NoLinearSupportInSM2 (rgbm, unity_SpecCube0_HDR) * RTP_IBL_SpecStrength;
		#else
	        // U4 reflection probe
   			half3 IBLSpecCol = DecodeRGBM(texCUBElod (_CubemapSpec, float4(reflVec, o_SpecularInvSquared*(6-exponential*o.RTP.x*3))))*RTP_IBL_SpecStrength;
		#endif 			
		
		IBLSpecCol.rgb*=spec_fresnel * SpecColor * lerp(1, o_RTP_y_without_shadowmap_distancefade, TERRAIN_IBLRefl_SpecAO_Damp);
		#if ( defined(RTP_SKYSHOP_SYNC) && !defined(UNITY_PASS_SHADOWCASTER) )
			IBLSpecCol.rgb*=_ExposureIBL.y;
		#endif		
		#if ( defined(RTP_IBL_DIFFUSE) && !defined(UNITY_PASS_SHADOWCASTER) )
			// link difuse and spec IBL together with energy conservation
			o.Emission += saturate(1-IBLSpecCol.rgb) * IBLDiffuseCol + IBLSpecCol.rgb;
		#else
			o.Emission+=IBLSpecCol.rgb;
		#endif
	#endif

    #if ( defined(RTP_REFLECTION) && !defined(UNITY_PASS_SHADOWCASTER) ) && !defined(RTP_SHOW_OVERLAPPED)
	#if defined(RTP_SIMPLE_SHADING)	
	
	#ifdef FAR_ONLY
	if (false) {
	#else
		#if defined(SHADER_API_D3D9) && !defined(_4LAYERS) && defined(UNITY_PI) && defined(DISABLE_DX9_HLSL_BRANCHING_LEVEL1)
			// TODO - HLSL for D3D9 seems to be buggy and can't compile RTP shaders (Cg in U<=4.6 did it...)
			if (true) {
		#else
			if (_uv_Relief_z>0) {
		#endif
	#endif

	#endif
		float2 mip_selectorRefl=o_SpecularInvSquared*(8-diffFresnel*o.RTP.x*4);
		#ifdef RTP_ROTATE_REFLECTION
			float3 refl_rot;
			refl_rot.x=sin(_Time.x*TERRAIN_ReflectionRotSpeed);
			refl_rot.y=cos(_Time.x*TERRAIN_ReflectionRotSpeed);
			refl_rot.z=-refl_rot.x;
			float2 tmpRefl;
			tmpRefl.x=dot(reflVec.xz, refl_rot.yz);
			tmpRefl.y=dot(reflVec.xz, refl_rot.xy);
			float t=tex2Dlod(_BumpMapGlobal, float4(tmpRefl*0.5+0.5, mip_selectorRefl)).a;
		#else
			float t=tex2Dlod(_BumpMapGlobal, float4(reflVec.xz*0.5+0.5, mip_selectorRefl)).a;
		#endif	
		#if ( defined(RTP_IBL_SPEC) && !defined(UNITY_PASS_SHADOWCASTER) )
			half rim=spec_fresnel;
		#else
			#define RTP_REFLSPEC_BUMPED
			#ifdef RTP_REFLSPEC_BUMPED
				float n_dot_v = saturate(dot(IBLNormal, IN.viewDir));
				float exponential = exp2(SchlickFresnelApproxExp2Const*n_dot_v);
				half rim = lerp(1, exponential, o.RTP.x);
			#else
				#if defined(RTP_COLORSPACE_LINEAR)
					diffFresnel=0.03+0.97*diffFresnel;
				#else
					diffFresnel=0.25+0.75*diffFresnel;
				#endif
		        half rim= lerp(1, diffFresnel, o.RTP.x);
			#endif
	    #endif
	    float downSideEnvelope=saturate(reflVec.y*3);
	    t *= downSideEnvelope;
	    rim *= downSideEnvelope*0.7+0.3;
		#if defined(RTP_SIMPLE_SHADING)
			rim*=RTP_IBL_SpecStrength*_uv_Relief_z;
		#else
			rim*=RTP_IBL_SpecStrength;
		#endif
		rim-=o_SpecularInvSquared*rim*TERRAIN_ReflGlossAttenuation; // attenuate low gloss
		
		half3 reflCol;
		reflCol=lerp(TERRAIN_ReflColorB.rgb, TERRAIN_ReflColorC.rgb, saturate(TERRAIN_ReflColorCenter-t) / TERRAIN_ReflColorCenter );
		reflCol=lerp(reflCol.rgb, TERRAIN_ReflColorA.rgb, saturate(t-TERRAIN_ReflColorCenter) / (1-TERRAIN_ReflColorCenter) );
		o.Emission += reflCol * SpecColor * lerp(1, o_RTP_y_without_shadowmap_distancefade, TERRAIN_IBLRefl_SpecAO_Damp) * rim * 2;
	#if defined(RTP_SIMPLE_SHADING)	
	}
	#endif
	#endif

	float diff;
	
	
	
	

	//o.Specular = 0;
	
	// EOF regular mode

	// przeniesione, norm_edge zamienione na o.Normal w obl. reflection
	#if defined(RTP_NORMALGLOBAL) && !defined(RTP_PLANET)
		o.Normal = lerp(o.Normal, BlendNormalsRTP(global_norm, o.Normal), lerp(1, _uv_Relief_w, _TERRAIN_trees_pixel_values.w));
	#endif	
	
	#if ( !defined(UNITY_PASS_PREPASSBASE) && !defined(UNITY_PASS_PREPASSFINAL) ) || (!defined(RTP_DEFERRED_PBL_NORMALISATION))
		//o.Specular=1-o_SpecularInvSquared;
	#endif

	#if defined(UNITY_PASS_PREPASSBASE)	
		 #if defined(RTP_DEFERRED_PBL_NORMALISATION) && defined(RTP_COLORSPACE_LINEAR)
			o.Specular*=o.Specular;
		#endif
	#endif

	#if defined(UNITY_PASS_PREPASSBASE)	 || defined(UNITY_PASS_PREPASSFINAL)
		o.Specular=lerp(RTP_DeferredAddPassSpec, o.Specular, total_coverage);
		#if defined(COLOR_EARLY_EXIT)
		// chcemy mieć przejście do docelowego Speculara osiągnięte wolniej
		float cBiased=(1-IN.color.a);
		cBiased*=cBiased;
		cBiased*=cBiased;
		o.Specular=lerp(o.Specular, RTP_DeferredAddPassSpec, cBiased);
		#endif
		o.Specular=max(0.01, o.Specular);
	#endif

	#if defined(UNITY_PASS_PREPASSFINAL)	
		#if defined(RTP_DEFERRED_PBL_NORMALISATION)
			o.Specular=1-o.Specular;
			o.Specular*=o.Specular;
			o.Specular=1-o.Specular;
			// hacking spec normalisation to get quiet a dark spec for max roughness (will be 0.25/16)
			float specular_power=exp2(10*o.Specular+1) - 1.75;
			float normalisation_term = specular_power / (8.0f*60); // 60 - handpicked value - wizualnie daje najlepszy fit
			o.SpecColor*=normalisation_term;
		#endif
		#if defined(RTP_PBL_FRESNEL) && !defined(SUPER_SIMPLE) && !defined(LIGHTDIR_UNAVAILABLE)
			{
			half3 h = normalize (IN.color.xyz + IN.viewDir.xyz);
			float n_dot_l = max(0, dot (o.Normal, IN.color.xyz));
			float h_dot_l = dot (h, IN.color.xyz);
			float exponential=exp2(SchlickFresnelApproxExp2Const*h_dot_l);
			// skyshop fit (I'd like people to get similar results in gamma / linear)
			#if defined(RTP_COLORSPACE_LINEAR)
				exponential=0.01+0.99*exponential;
			#else
				exponential=0.1+0.9*exponential;
			#endif
			o.SpecColor *= lerp (1.0f, exponential, o.RTP.x)*n_dot_l; // o.RTP.x - _Fresnel
			}
		#endif
	#endif

	// used on triplanar standalone shader as geom blend
	#if defined(GEOM_BLEND)
		#if defined(BLENDING_HEIGHT)
			float4 terrain_coverage=tex2Dp(_TERRAIN_Control, IN_uv_Control, _ddxGlobal, _ddyGlobal);
			float4 splat_control1HB=terrain_coverage * tHeightmapTMP * IN.color.a;
			float4 splat_control2HB=splat_control1 * (1-IN.color.a);

			float blend_coverage=dot(terrain_coverage, 1);
			if (blend_coverage>0.1) {

				splat_control1HB*=splat_control1HB;
				splat_control1HB*=splat_control1HB;
				splat_control1HB*=splat_control1HB;
				splat_control2HB*=splat_control2HB;
				splat_control2HB*=splat_control2HB;
				splat_control2HB*=splat_control2HB;

				float normalize_sum=dot(splat_control1HB, 1)+dot(splat_control2HB, 1);
				splat_control1HB /= normalize_sum;
				splat_control2HB /= normalize_sum;

				o.Alpha=dot(splat_control2HB,1);
				o.Alpha=lerp(1-IN.color.a, o.Alpha, saturate((blend_coverage-0.1)*4) );
			} else {
				o.Alpha=1-IN.color.a;
			}
		#else
			o.Alpha=1-IN.color.a;
		#endif

		#if defined(UNITY_PASS_PREPASSFINAL)
			o.SpecColor*=o.Alpha;
		#endif		
	#endif


	// HACK-ish workaround - Unity skip's passing color if IN.color is not explicitly used here in shader
	o.Albedo+=IN.color.xyz*0.0001;	
	
	//o.SpecColor = 1;
	//o.Specular = 1;
	//o.Albedo = IN.color.yyy;
		
}