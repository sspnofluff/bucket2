
Shader "Relief Pack/Road_Transition" 
{
    Properties 
	{
		
		_Road ("Road", 2D) = "black" {}
		_Cracks ("Cracks", 2D) = "black" {}
		_Markup ("Markup", 2D) = "black" {}
		
		_Road02 ("Road 2", 2D) = "black" {}
		_Cracks02 ("Cracks 2", 2D) = "black" {}
		_Markup02 ("Markup 2", 2D) = "black" {}
		
		_Border("Border", 2D) = "white" {}
		_Border_n("Border Normal", 2D) = "grey" {}
		_Border_h ("Border Height", 2D) = "white" {}

		_BorderB("Border B", 2D) = "white" {}
		_BorderB_n("Border B Normal", 2D) = "grey" {}
		_BorderB_h ("Border B Height", 2D) = "white" {}

		_Mask("_Mask", 2D) = "white" {}	
		
		
		_SpecColorA ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
		_SpecColorB ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
		_Shininess ("Shininess", Range (0.01, 1)) = 0.078125
		_BlendingBorder("Blending Border", Range(-5,35)) = 14
		_ScaleBorder("Scale Border", Range(-5,15)) = 1.5 

		_BrightSpec ("Brightness/Saturation", Vector) = (0.5, 0.5, 0.5, 0.5)

    }
   SubShader {
	Tags {
		"Queue"="Geometry+10"
		"RenderType" = "Transparent"
	}
	
	Offset -15,-15
	//ZTest Always	
	CGPROGRAM
	#pragma surface surf CustomBlinnPhong vertex:vert decal:blend
	#pragma only_renderers d3d9 opengl d3d11
	#pragma multi_compile RTP_SIMPLE_SHADING
	//#pragma target 5.0

	float _Shininess;	
	float4 _Markup_ST;
	float2 _Road_ST;

	struct Input 
	{
		float4 tc_Markup;
		float2 tc_Road;
		float4 color:COLOR;
		float4 color2:COLOR2;
		INTERNAL_DATA
	};
	
	float _BlendingBorder;
	float _ScaleBorder;
	float4 _BrightSpec;
	float4 _SpecColorA;
	float4 _SpecColorB;
	
	sampler2D _Markup;
	sampler2D _Road;
	sampler2D _Cracks;
	
	sampler2D _Markup02;
	sampler2D _Road02;
	sampler2D _Cracks02;

	sampler2D _Border;
	sampler2D _Border_n;
	sampler2D _Border_h;

	sampler2D _BorderB;
	sampler2D _BorderB_n;
	sampler2D _BorderB_h;

	sampler2D _Mask;

	#include "Assets/ReliefPack/Shaders/CustomLightingLegacy.cginc"
	#include "Assets/VacuumShaders/Curved World/Shaders/cginc/CurvedWorld_Base.cginc"

	void vert (inout appdata_full v, out Input o) {
	    #if defined(SHADER_API_D3D11) || defined(SHADER_API_D3D11_9X) || defined(UNITY_PI)
			UNITY_INITIALIZE_OUTPUT(Input, o);
		#endif

		V_CW_TransformPointAndNormal(v.vertex, v.normal, v.tangent);
		v.texcoord = mul(UNITY_MATRIX_TEXTURE0, v.texcoord);

		o.tc_Markup.xy=v.texcoord.xy;
		o.tc_Markup.zw=v.texcoord1.xy;
		o.tc_Road.xy=v.texcoord2.xy;

	}
	
	void surf (Input IN, inout SurfaceOutput o) 
	{
				
      	o.Gloss=float3(0,0,1);
		o.Normal=float3(0,0,1);    	
      	o.Specular=float3(0,0,1); 	

		float2 markup_uv = IN.tc_Markup.xy;
		float2 border_uv = markup_uv *_ScaleBorder;
		float2 mask_uv = IN.tc_Markup.zw;
		float2 road_uv = markup_uv.yx;
		
		float3 borderA_d = tex2D(_Border, border_uv.yx).rgb;
		float borderA_h = tex2D(_Border_h, border_uv.yx);
		float3 borderA_n = UnpackNormal(tex2D(_Border_n, border_uv.yx));

		float3 borderB_d = tex2D(_BorderB, border_uv.yx).rgb;
		float borderB_h = tex2D(_BorderB_h, border_uv.yx);
		float3 borderB_n = UnpackNormal(tex2D(_BorderB_n, border_uv.yx));
		
		borderA_d = borderA_d * _BrightSpec.x;  
		borderB_d = borderB_d * _BrightSpec.z;		
		
		float3 intensity = dot(borderA_d, float3(0.299,0.587,0.114));
		borderA_d = lerp(intensity, borderA_d, _BrightSpec.y);
		
		intensity = dot(borderB_d, float3(0.299,0.587,0.114));
		borderB_d = lerp(intensity, borderB_d, _BrightSpec.w);
		
		float mask = tex2D(_Mask, mask_uv);
		
		float markup01 = tex2D(_Markup, markup_uv);
		float4 road01 = tex2D(_Road, road_uv);
		float4 cracks01 = tex2D(_Cracks, road_uv);
		
		float markup02 = tex2D(_Markup02, markup_uv);
		float4 road02 = tex2D(_Road02, road_uv);
		float4 cracks02 = tex2D(_Cracks02, road_uv);
		
		float3 border_d = lerp(borderA_d,borderB_d,IN.tc_Road.x);
		float border_h = lerp(borderA_h,borderB_h,IN.tc_Road.x);
		float3 border_n = lerp(borderA_n,borderB_n,IN.tc_Road.x);
		
		_SpecColor = lerp(_SpecColorA,_SpecColorB,IN.tc_Road.x);
		
		float markup = lerp(markup01,markup02,IN.tc_Road.y);
		float4 road = lerp(road01,road02,IN.tc_Road.x);
		float4 cracks = lerp(cracks01,cracks02,IN.tc_Road.x);
		
		float blendThis = lerp(-3.5,-2.1,IN.color.b);
		float factor = saturate(mask*15 - border_h * blendThis * blendThis);

		o.Albedo = lerp(border_d, lerp(road,cracks,1-IN.color.r),factor) + 0.35 * lerp(0,markup.r,IN.color.a);

		o.Normal = lerp(border_n ,float3(0,0,1), factor);
		o.Specular = 0.078125;;
		o.Gloss = lerp(0,road.a,factor) ;

		o.Alpha = max(mask * 0.4f * _BlendingBorder * (1-factor) * (1-IN.color.b), mask) +  0.45 * lerp(0,markup.r,IN.color.a);
			

	}
	ENDCG
      
    } 
	FallBack "Diffuse"
}
