
Shader "Road_Timelapse" {
    Properties {
		_Decal ("Texture Decal", 2D) = "black" {}

    }
    SubShader {
	Tags {
		"Queue"="Geometry+10"
		"RenderType" = "Transparent"
	}
	
	Offset -3,-3
	//ZTest Always	
	CGPROGRAM
	#pragma surface surf CustomBlinnPhong vertex:vert decal:blend
	#pragma only_renderers d3d9 opengl d3d11
	#pragma multi_compile RTP_SIMPLE_SHADING
	#pragma target 3.0

		
	float2 _Decal_ST;

	struct Input 
	{
		float2 tc_Decal;
		float4 color:COLOR;
	};
	
	sampler2D _Decal;

	#include "Assets/ReliefPack/Shaders/CustomLightingLegacy.cginc"
	#include "Assets/VacuumShaders/Curved World/Shaders/cginc/CurvedWorld_Base.cginc"

	void vert (inout appdata_full v, out Input o) {
	    #if defined(SHADER_API_D3D11) || defined(SHADER_API_D3D11_9X) || defined(UNITY_PI)
			UNITY_INITIALIZE_OUTPUT(Input, o);
		#endif

		V_CW_TransformPointAndNormal(v.vertex, v.normal, v.tangent);
		v.texcoord = mul(UNITY_MATRIX_TEXTURE0, v.texcoord);

		float3 Wpos=mul(unity_ObjectToWorld, v.vertex);
		o.tc_Decal.xy=Wpos.xz/10;
	}
	
	void surf (Input IN, inout SurfaceOutput o) 
	{
				
      	o.Gloss=float3(0,0,1);
		o.Normal=float3(0,0,1);    	
      	o.Specular=float3(0,0,1); 

		

		float2 uv1 = IN.tc_Decal.xy;
		float decal=tex2D(_Decal, uv1);
		o.Albedo = decal;

		o.Alpha = 0.4f;
			

	}
	ENDCG
      
    } 
	FallBack "Diffuse"
}
