Shader "Relief Pack/Road_8LayersDarken" {
    Properties {
		_MainColor ("Main Color (RGB)", Color) = (0.5, 0.5, 0.5, 1)		
		_SpecColor ("Specular Color (RGBA)", Color) = (0.5, 0.5, 0.5, 1)
		_ExtrudeHeight ("Extrude Height", Range(0.001,0.1)) = 0.04


		_MainTex ("Texture", 2D) = "white" {}
		_HeightMap ("Heightmap", 2D) = "black" {}

		_Decal ("Texture Decal 01", 2D) = "white" {}
		_Decal2 ("Texture Decal 02", 2D) = "white" {}
		
		_TERRAIN_PosSize ("Terrain pos (xz to XY) & size(xz to ZW)", Vector) = (0,0,1000,1000)
		_TERRAIN_Tiling ("Terrain tiling (XY) & offset(ZW)", Vector) = (3,3,0,0)
		_TERRAIN_ReliefTransform ("", Vector) = (1,1,1,1)
		_TERRAIN_HeightMapA ("Terrain HeightMap A (combined)", 2D) = "white" {}
		_TERRAIN_HeightMapB ("Terrain HeightMap B (combined)", 2D) = "white" {}
		_TERRAIN_ControlA ("Terrain splat controlMap A", 2D) = "white" {}
		_TERRAIN_ControlB ("Terrain splat controlMap B", 2D) = "white" {}
		_HeightParam("Height Param", Range(0.1,1.0)) = 0.037
    }
    SubShader {
	Tags {
		"Queue"="Geometry+10"
		"RenderType" = "Transparent"
	}
	
	Offset -1,-1
	//ZTest Always		
	CGPROGRAM
	#pragma surface surf CustomBlinnPhong vertex:vert decal:blend
	#pragma only_renderers d3d9 opengl d3d11
	#pragma multi_compile RTP_SIMPLE_SHADING
	#pragma target 3.0
	
	// remove it (comment if you don't use complemenatry lighting in your scene)
	#define RTP_COMPLEMENTARY_LIGHTS

	#define BLENDING_HEIGHT
	
	float4 _MainTex_ST;
	float4 _Decal_ST;
	struct Input {
		float2 tc_MainTex;
		float3 aux;
		float2 globalUV;
		float4 tc_Decal;
	};
	
	half3 _MainColor;		
	float _ExtrudeHeight;
	float _HeightParam;

	sampler2D _MainTex;
	sampler2D _HeightMap;
	sampler2D _Decal;
	sampler2D _Decal2;

	float4 _TERRAIN_PosSize;
	float4 _TERRAIN_Tiling;

	sampler2D _TERRAIN_HeightMapA;
	sampler2D _TERRAIN_HeightMapB;
	sampler2D _TERRAIN_ControlA;
	sampler2D _TERRAIN_ControlB;

	// Custom&reflexLighting
	#include "Assets/ReliefPack/Shaders/CustomLightingLegacy.cginc"
	#include "Assets/VacuumShaders/Curved World/Shaders/cginc/CurvedWorld_Base.cginc"

	void vert (inout appdata_full v, out Input o) {
	    #if defined(SHADER_API_D3D11) || defined(SHADER_API_D3D11_9X) || defined(UNITY_PI)
			UNITY_INITIALIZE_OUTPUT(Input, o);
		#endif

		V_CW_TransformPointAndNormal(v.vertex, v.normal, v.tangent);
		v.texcoord = mul(UNITY_MATRIX_TEXTURE0, v.texcoord);

		o.tc_MainTex.xy=TRANSFORM_TEX(v.texcoord, _MainTex);
		o.tc_Decal.xy=TRANSFORM_TEX(v.texcoord2, _Decal);
		o.tc_Decal.zw=v.texcoord1.xy;
		//o.tc_MainTex.zw = v.texcoord2.xy;
		float3 Wpos=mul(unity_ObjectToWorld, v.vertex);
		o.aux.xy=Wpos.xz-_TERRAIN_PosSize.xy+_TERRAIN_Tiling.zw;
		o.aux.xy/=_TERRAIN_Tiling.xy;
		o.aux.z=length(_WorldSpaceCameraPos.xyz-Wpos);
		
		o.globalUV=Wpos.xz-_TERRAIN_PosSize.xy;
		o.globalUV/=_TERRAIN_PosSize.zw;

			
		//float far=saturate((o._uv_Relief.w - _TERRAIN_distance_start_bumpglobal) / _TERRAIN_distance_transition_bumpglobal);
		//v.normal.xyz=lerp(v.normal.xyz, float3(0,1,0), far*_FarNormalDamp);
	}
	
	void surf (Input IN, inout SurfaceOutput o) {


      	float tH=tex2D(_HeightMap, IN.tc_MainTex.xy).a;
      
      	float control=(tH+0.01);
      
		o.Gloss=float3(0,0,1);

		o.Normal=float3(0,0,1);    	
		
      	o.Specular=float3(0,0,1); 


      	#if defined(BLENDING_HEIGHT)
			float4 terrain_coverageA=tex2D(_TERRAIN_ControlA, IN.globalUV);
			float4 splat_control1A=terrain_coverageA * tex2D(_TERRAIN_HeightMapA, IN.aux.xy);// * IN.color.a;
			float4 splat_control2A=float4(control, 0, 0, 0);// * (1-IN.color.a);

			float4 terrain_coverageB=tex2D(_TERRAIN_ControlB, IN.globalUV);
			float4 splat_control1B=terrain_coverageB * tex2D(_TERRAIN_HeightMapB, IN.aux.xy);// * IN.color.a;
			float4 splat_control2B=float4(control, 0, 0, 0);// * (1-IN.color.a);
			
			float blend_coverageA=dot(terrain_coverageA, 1);
			float blend_coverageB=dot(terrain_coverageB, 1);

			float AlphaA;
			if (blend_coverageA>0.1) 
			{
				splat_control1A*=splat_control1A;
				splat_control1A=splat_control1A;
				splat_control2A*=splat_control2A;
				splat_control2A*=splat_control2A;
				
				float normalize_sum=dot(splat_control1A, 1)+dot(splat_control2A, 1);
				splat_control1A /= normalize_sum;
				splat_control2A /= normalize_sum;		
				
				AlphaA=dot(splat_control2A,1);
				AlphaA=lerp(0, AlphaA, saturate((blend_coverageA-0.1)*4) );
			} 
			else
			{
				AlphaA=1;
			}

			float AlphaB;
			if (blend_coverageB>0.1) 
			{
				splat_control1B*=splat_control1B;
				splat_control1B=splat_control1B;
				splat_control2B*=splat_control2B;
				splat_control2B*=splat_control2B;
				
				float normalize_sum=dot(splat_control1B, 1)+dot(splat_control2B, 1);
				splat_control1B /= normalize_sum;
				splat_control2B /= normalize_sum;		
				
				AlphaB=dot(splat_control2B,1);
				AlphaB=lerp(1-1, AlphaB, saturate((blend_coverageB-0.1)*4) );
			} 
			else
			{
				AlphaB=1-1;
			}

		
			o.Alpha = dot(AlphaA,blend_coverageA) + dot(AlphaB,blend_coverageB);
		#endif


   

		float4 splatA = tex2D(_TERRAIN_ControlA, IN.tc_Decal.zw);
		float4 splatB = tex2D(_TERRAIN_ControlB, IN.tc_Decal.zw);
		float height = tex2D(_TERRAIN_HeightMapA, IN.aux.xy).b;


		fixed4 decal=tex2D(_Decal, IN.tc_MainTex.xy);
		fixed decal2=tex2D(_Decal2, IN.tc_Decal.xy);

		float num = splatA.b;
		if (num<_HeightParam)
		num *= height;
		//num += decal2.r;

		o.Albedo.rgb = decal.rgb * decal2.r;
		o.Albedo.rgb += _MainColor * o.Alpha * 0.3;

		o.Alpha = num * 0.5f;

	}
	ENDCG
      
    } 
	FallBack "Diffuse"
}
