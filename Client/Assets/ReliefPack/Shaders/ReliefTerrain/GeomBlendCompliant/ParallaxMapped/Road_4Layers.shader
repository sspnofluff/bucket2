
Shader "Relief Pack/Road_4Layers" {
    Properties {
		
		_Road ("Road", 2D) = "black" {}
		_Cracks ("Cracks", 2D) = "black" {}
		_Markup ("Markup", 2D) = "black" {}
		
		_Border("Border", 2D) = "white" {}
		_Border_n("Border Normal", 2D) = "grey" {}
		_Border_h ("Border Height", 2D) = "white" {}

		_Border02("Border 2", 2D) = "white" {}
		_Border02_n("Border 2 Normal", 2D) = "grey" {}
		_Border02_h ("Border 2 Height", 2D) = "white" {}

		_Mask("_Mask", 2D) = "white" {}	
		
		_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
		//_Shininess ("Shininess", Range (0.01, 1)) = 0.078125
		_BlendingBorder("Blending Border", Range(-5,35)) = 14
		_ScaleBorder("Scale Border", Range(-5,15)) = 1.5 
		
		_BrightSpec ("Brightness/Saturation", Vector) = (0.5, 0.5, 0.5, 0.5)
    }
    SubShader {
	Tags {
		"Queue"="Geometry+10"
		"RenderType" = "Transparent"
	}
	
	Offset -15,-15
	//ZTest Always	
	CGPROGRAM
	#pragma surface surf CustomBlinnPhong vertex:vert decal:blend
	#pragma only_renderers d3d9 opengl d3d11
	#pragma multi_compile RTP_SIMPLE_SHADING
	#pragma target 3.0

	//float _Shininess;	
	float4 _Markup_ST;

	struct Input 
	{
		float4 tc_Markup;
		//float3 aux;
		float4 color:COLOR;
	};
	
	float _BlendingBorder;
	float _ScaleBorder;
	float4 _BrightSpec;

	sampler2D _Markup;
	sampler2D _Road;
	sampler2D _Cracks;

	sampler2D _Border;
	sampler2D _Border_n;
	sampler2D _Border_h;

	sampler2D _Border02;
	sampler2D _Border02_n;
	sampler2D _Border02_h;

	sampler2D _Mask;

	#include "Assets/ReliefPack/Shaders/CustomLightingLegacy.cginc"
	#include "Assets/VacuumShaders/Curved World/Shaders/cginc/CurvedWorld_Base.cginc"

	void vert (inout appdata_full v, out Input o) {
	    #if defined(SHADER_API_D3D11) || defined(SHADER_API_D3D11_9X) || defined(UNITY_PI)
			UNITY_INITIALIZE_OUTPUT(Input, o);
		#endif

		V_CW_TransformPointAndNormal(v.vertex, v.normal, v.tangent);
		v.texcoord = mul(UNITY_MATRIX_TEXTURE0, v.texcoord);

		o.tc_Markup.xy=v.texcoord.xy;
		o.tc_Markup.zw=v.texcoord1.xy;

	}
	
	void surf (Input IN, inout SurfaceOutput o) 
	{
				
      	o.Gloss=float3(0,0,1);
		o.Normal=float3(0,0,1);    	
      	o.Specular=float3(0,0,1); 	

		float2 markup_uv = IN.tc_Markup.xy;
		float2 border_uv = markup_uv *_ScaleBorder;
		float2 mask_uv = IN.tc_Markup.zw;
		float2 road_uv = markup_uv.yx;

		float3 border_d = tex2D(_Border, border_uv.yx).rgb;
		float border_h = tex2D(_Border_h, border_uv.yx);
		float3 border_n = UnpackNormal(tex2D(_Border_n, border_uv.yx));

		float3 border02_d = tex2D(_Border02, border_uv.yx).rgb;
		float border02_h = tex2D(_Border02_h, border_uv.yx);
		float3 border02_n = UnpackNormal(tex2D(_Border02_n,border_uv.yx));

		border_d = border_d * _BrightSpec.x;  
		border02_d = border02_d * _BrightSpec.z;		
		
		float3 intensity = dot(border_d, float3(0.299,0.587,0.114));
		border_d = lerp(intensity, border_d, _BrightSpec.y);
		
		intensity = dot(border02_d, float3(0.299,0.587,0.114));
		border02_d = lerp(intensity, border02_d, _BrightSpec.w);
		
		float mask = tex2D(_Mask, mask_uv);
		float markup = tex2D(_Markup, markup_uv);

		float4 road = tex2D(_Road, road_uv);
		float4 cracks = tex2D(_Cracks, road_uv);

		float blendThis = lerp(-3.5,-2.1,IN.color.b);
		float factor = saturate(mask*15 - lerp(border_h,border02_h,IN.color.g) * blendThis * blendThis);

		o.Albedo = lerp(lerp(border_d,border02_d,IN.color.g), lerp(road,cracks,1-IN.color.r),factor) + 0.35 * lerp(0,markup.r,IN.color.a);

		o.Normal = lerp(lerp(border_n,border02_n,IN.color.g) ,float3(0,0,1), factor);
		o.Specular = 0.078125;//_Shininess;
		o.Gloss = lerp(0,road.a,factor);

		o.Alpha = max(mask * 0.4f * _BlendingBorder * (1-factor) * (1-IN.color.b), mask) +  0.45 * lerp(0,markup.r,IN.color.a);
			

	}
	ENDCG
      
    } 
	FallBack "Diffuse"
}
