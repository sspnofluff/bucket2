
Shader "Relief Pack/RoadBaseSmall" {
    Properties {
		_MainColor ("Main Color (RGB)", Color) = (0.5, 0.5, 0.5, 1)		
		_SpecColor ("Specular Color (RGBA)", Color) = (0.5, 0.5, 0.5, 1)
		_ExtrudeHeight ("Extrude Height", Range(0.001,0.1)) = 0.04


		_MainTex ("Texture", 2D) = "white" {}
		_HeightMap ("Heightmap (A)", 2D) = "black" {}
		_Decal ("Texture Decal 01", 2D) = "black" {}
		_Decal2 ("Texture Decal 02", 2D) = "black" {}
		
		_TERRAIN_PosSize ("Terrain pos (xz to XY) & size(xz to ZW)", Vector) = (0,0,1000,1000)
		_TERRAIN_Tiling ("Terrain tiling (XY) & offset(ZW)", Vector) = (3,3,0,0)
		_TERRAIN_ReliefTransform ("", Vector) = (1,1,1,1)
		_TERRAIN_HeightMap ("Terrain HeightMap (combined)", 2D) = "white" {}
		_TERRAIN_Control ("Terrain splat controlMap", 2D) = "white" {}

    }
    SubShader {
	Tags {
		"Queue"="Geometry+10"
		"RenderType" = "Transparent"
	}
	
	Offset -1,-1
	ZTest Always	
	CGPROGRAM
	#pragma surface surf CustomBlinnPhong vertex:vert decal:blend
	#pragma only_renderers d3d9 opengl d3d11
	#pragma multi_compile RTP_SIMPLE_SHADING
	#pragma target 3.0
	
	// remove it (comment if you don't use complemenatry lighting in your scene)
	#define RTP_COMPLEMENTARY_LIGHTS

	#define AOBASE
	// edges can be heightblended
	#define BLENDING_HEIGHT
	
		
	float4 _MainTex_ST;
	float4 _Decal_ST;

	struct Input {
		float2 tc_MainTex;
		float3 aux;
		float2 globalUV;
		float3 viewDir;
		float4 tc_Decal;

		float4 color:COLOR;
	};
	
	half3 _MainColor;		
	float _ExtrudeHeight;
	
	sampler2D _MainTex;
	sampler2D _HeightMap;
	sampler2D _Decal;
	sampler2D _Decal2;

	float4 _TERRAIN_PosSize;
	float4 _TERRAIN_Tiling;

	sampler2D _TERRAIN_HeightMap;
	sampler2D _TERRAIN_Control;


	// Custom&reflexLighting
	#include "Assets/ReliefPack/Shaders/CustomLightingLegacy.cginc"
	#include "Assets/VacuumShaders/Curved World/Shaders/cginc/CurvedWorld_Base.cginc"

	void vert (inout appdata_full v, out Input o) {
	    #if defined(SHADER_API_D3D11) || defined(SHADER_API_D3D11_9X) || defined(UNITY_PI)
			UNITY_INITIALIZE_OUTPUT(Input, o);
		#endif

		V_CW_TransformPointAndNormal(v.vertex, v.normal, v.tangent);
		v.texcoord = mul(UNITY_MATRIX_TEXTURE0, v.texcoord);

		o.tc_MainTex.xy=TRANSFORM_TEX(v.texcoord, _MainTex);
		o.tc_Decal.xy=TRANSFORM_TEX(v.texcoord, _Decal);
		o.tc_Decal.zw=v.texcoord1.xy;

		float3 Wpos=mul(unity_ObjectToWorld, v.vertex);
		o.aux.xy=Wpos.xz-_TERRAIN_PosSize.xy+_TERRAIN_Tiling.zw;
		o.aux.xy/=_TERRAIN_Tiling.xy;
		o.aux.z=length(_WorldSpaceCameraPos.xyz-Wpos);
		
		o.globalUV=Wpos.xz-_TERRAIN_PosSize.xy;
		o.globalUV/=_TERRAIN_PosSize.zw;

			
		//float far=saturate((o._uv_Relief.w - _TERRAIN_distance_start_bumpglobal) / _TERRAIN_distance_transition_bumpglobal);
		//v.normal.xyz=lerp(v.normal.xyz, float3(0,1,0), far*_FarNormalDamp);
	}
	
	void surf (Input IN, inout SurfaceOutput o) 
	{
				
			float tH=tex2D(_HeightMap, IN.tc_MainTex.xy).a;
		#if !defined(RTP_SIMPLE_SHADING) 
	      	float2 uv=IN.tc_MainTex.xy + ParallaxOffset(tH, _ExtrudeHeight, IN.viewDir.xyz)*(1-IN.color.a);
      	#endif
      	float control=(tH+0.01);
		#if !defined(RTP_SIMPLE_SHADING) 
	      	float2 uvMixed=uv;
		#else
	      	float2 uvMixed=IN.tc_MainTex.xy;
		#endif
      	
      	fixed4 col=tex2D(_MainTex, uvMixed);
      	o.Albedo=col.rgb;

      	o.Gloss=float3(0,0,1);

		o.Normal=float3(0,0,1);    	
		
      	o.Specular=float3(0,0,1); 


		#if defined(BLENDING_HEIGHT)
			float4 terrain_coverage=tex2D(_TERRAIN_Control, IN.globalUV);
			float4 splat_control1=terrain_coverage * tex2D(_TERRAIN_HeightMap, IN.aux.xy) * IN.color.a;
			float4 splat_control2=float4(control, 0, 0, 0) * (1-IN.color.a);
			
			float blend_coverage=dot(terrain_coverage, 1);
			if (blend_coverage>0.1) {
			
				splat_control1*=splat_control1;
				splat_control1*=splat_control1;
				splat_control2*=splat_control2;
				splat_control2*=splat_control2;
				
				float normalize_sum=dot(splat_control1, 1)+dot(splat_control2, 1);
				splat_control1 /= normalize_sum;
				splat_control2 /= normalize_sum;		
				
				o.Alpha=dot(splat_control2,1);
				o.Alpha=lerp(1-IN.color.a, o.Alpha, saturate((blend_coverage-0.1)*4) );


			} else {
				o.Alpha=1-IN.color.r;
			}

			#else
			o.Alpha=1-IN.color.r;
		#endif



		float4 splat = tex2D(_TERRAIN_Control, IN.tc_Decal.zw);
		float height = tex2D(_TERRAIN_HeightMap, IN.aux.xy).b;

		fixed4 decal=tex2D(_Decal, IN.tc_Decal.xy);
		fixed decal2=tex2D(_Decal2, IN.tc_Decal.xy);

		o.Albedo.rgb *= decal.rgb;
		o.Albedo.rgb += _MainColor * decal2.r * IN.color.g * o.Alpha;

		o.Alpha = splat.b;


			

	}
	ENDCG
      
    } 
	FallBack "Diffuse"
}
