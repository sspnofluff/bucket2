// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

//
// Relief Terrain  -  Parallax mapped material with height blending
// Tomasz Stobierski 2013
//
// distances have the same meaning like in RTP inspector
//
// we're using position (x,z), size (x,z) and tiling of underlying terrain to get right coords of global perlin map and global colormap
// 
Shader "Relief Pack/ManualDecal" {
    Properties {
		_MainColor ("Main Color (RGB)", Color) = (0.5, 0.5, 0.5, 1)		
		_SpecColor ("Specular Color (RGBA)", Color) = (0.5, 0.5, 0.5, 1)
		_Shininess ("Shininess", Range (0.01, 1)) = 0.5
		//_ExtrudeHeight ("Extrude Height", Range(0.001,0.1)) = 0.04

		_Brightness ("Brightness", Float) = 1.0
        _Saturation ("Saturation", Float) = 1.0

		_MainTex ("Texture", 2D) = "white" {}
		_BumpMap ("Bumpmap", 2D) = "bump" {}
		_HeightMap ("Heightmap (A)", 2D) = "black" {}

		//_BumpMapPerlin ("Perlin global (special combined RG)", 2D) = "black" {}
		//_BumpMapPerlinBlend ("Perlin normal", Range(0,2)) = 0.3
		//_BumpMapGlobalScale ("Tiling scale", Range( 0.01,0.25) ) = 0.1
		//_TERRAIN_distance_start ("Near distance start", Float) = 2
		//_TERRAIN_distance_transition ("Near fade length", Float) = 20
		//_TERRAIN_distance_start_bumpglobal ("Far distance start", Float) = 22
		//_TERRAIN_distance_transition_bumpglobal ("Far fade length", Float) = 40
		//rtp_perlin_start_val ("Perlin start val", Range(0,1)) = 0.3

				
		//_ColorMapGlobal ("Global colormap", 2D) = "black" {}
		// uncomment when used w/o ReliefTerrain script component (for example with VoxelTerrains)
		//_GlobalColorMapBlendValues ("Global colormap blending (XYZ)", Vector) = (0.3,0.6,0.8,0)
		//_GlobalColorMapSaturation ("Global colormap saturation", Range(0,1)) = 0.8
		
		_TERRAIN_PosSize ("Terrain pos (xz to XY) & size(xz to ZW)", Vector) = (0,0,1000,1000)
		_TERRAIN_Tiling ("Terrain tiling (XY) & offset(ZW)", Vector) = (3,3,0,0)
		_TERRAIN_HeightMap ("Terrain HeightMap (combined)", 2D) = "white" {}
		_TERRAIN_Control ("Terrain splat controlMap", 2D) = "white" {}
    }
    SubShader {
	Tags {
		"Queue"="Geometry+12"
		"RenderType" = "Opaque"
	}
	
	Offset -1,-1
	ZTest LEqual	
	CGPROGRAM
	#pragma surface surf CustomBlinnPhong vertex:vert decal:blend
	#pragma only_renderers d3d9 opengl d3d11
	#pragma multi_compile RTP_PM_SHADING RTP_SIMPLE_SHADING
	#pragma target 3.0
	
	// remove it (comment if you don't use complemenatry lighting in your scene)
	//#define RTP_COMPLEMENTARY_LIGHTS

	#define BLENDING_HEIGHT
	
	// we'll use global colormap
	//#define COLOR_MAP
	//#define COLOR_MAP_BLEND_MULTIPLY
	// we'll use global perlin
	//#define GLOBAL_PERLIN
	// we'll show only global colormap at far distance (no detail)
	//#define SIMPLE_FAR
		
	float4 _MainTex_ST;

	struct Input {
		float2 tc_MainTex;
		float3 aux;
		float2 globalUV;
		float3 viewDir;
		float4 tc_Decal;
		
		float4 color:COLOR;
	};
	
	half3 _MainColor;		
	float _Shininess;
	//float _ExtrudeHeight;
	
	sampler2D _MainTex;
	sampler2D _BumpMap;
	sampler2D _HeightMap;

	//sampler2D _ColorMapGlobal;
	//sampler2D _BumpMapPerlin;
	//float _BumpMapGlobalScale;
	//float _BumpMapPerlinBlend;
	//
	float _Brightness;
    float _Saturation;

	//float rtp_perlin_start_val;		
	//float _TERRAIN_distance_start;
	//float _TERRAIN_distance_transition;
	//float _TERRAIN_distance_start_bumpglobal;
	//float _TERRAIN_distance_transition_bumpglobal;
	
	float4 _TERRAIN_PosSize;
	float4 _TERRAIN_Tiling;

	// set globaly by ReliefTerrain script
	float4 _GlobalColorMapBlendValues;
	float _GlobalColorMapSaturation;
	
	sampler2D _TERRAIN_HeightMap;
	sampler2D _TERRAIN_Control;
	
	// Custom&reflexLighting
	#include "Assets/ReliefPack/Shaders/CustomLightingLegacy.cginc"
	#include "Assets/VacuumShaders/Curved World/Shaders/cginc/CurvedWorld_Base.cginc"
	
	void vert (inout appdata_full v, out Input o) {
	    #if defined(SHADER_API_D3D11) || defined(SHADER_API_D3D11_9X) || defined(UNITY_PI)
			UNITY_INITIALIZE_OUTPUT(Input, o);
		#endif

		V_CW_TransformPointAndNormal(v.vertex, v.normal, v.tangent);
		o.tc_MainTex.xy=TRANSFORM_TEX(v.texcoord, _MainTex);

		float3 Wpos=mul(unity_ObjectToWorld, v.vertex);
		o.aux.xy=Wpos.xz-_TERRAIN_PosSize.xy+_TERRAIN_Tiling.zw;
		o.aux.xy/=_TERRAIN_Tiling.xy;
		o.aux.z=length(_WorldSpaceCameraPos.xyz-Wpos);
		
		o.globalUV=Wpos.xz-_TERRAIN_PosSize.xy;
		o.globalUV/=_TERRAIN_PosSize.zw;
		
		//float far=saturate((o._uv_Relief.w - _TERRAIN_distance_start_bumpglobal) / _TERRAIN_distance_transition_bumpglobal);
		//v.normal.xyz=lerp(v.normal.xyz, float3(0,1,0), far*_FarNormalDamp);
	}
	
	void surf (Input IN, inout SurfaceOutput o) 
	{
		
      	float tH=tex2D(_HeightMap, IN.tc_MainTex.xy).a;
		float control=(tH+0.01);
	    float2 uvMixed=IN.tc_MainTex.xy;
		     	
      	fixed4 col=tex2D(_MainTex, uvMixed);
      	o.Albedo=col.rgb;

      	o.Gloss=col.a;
      	
		o.Normal=UnpackNormal(tex2D(_BumpMap,uvMixed));
      	
		
      	o.Specular=_Shininess;

		#if defined(BLENDING_HEIGHT)
			float4 terrain_coverage=tex2D(_TERRAIN_Control, IN.globalUV);
			float4 splat_control1=terrain_coverage * tex2D(_TERRAIN_HeightMap, IN.aux.xy) * IN.color.a;
			float4 splat_control2=float4(control, 0, 0, 0) * (1-IN.color.a);
			
			float blend_coverage=dot(terrain_coverage, 1);
			if (blend_coverage>0.1) {
			
				splat_control1*=splat_control1;
				splat_control1*=splat_control1;
				splat_control2*=splat_control2;
				splat_control2*=splat_control2;
				
				float normalize_sum=dot(splat_control1, 1)+dot(splat_control2, 1);
				splat_control1 /= normalize_sum;
				splat_control2 /= normalize_sum;		
				
				o.Alpha=dot(splat_control2,1);
				o.Alpha=lerp(1-IN.color.a, o.Alpha, saturate((blend_coverage-0.1)*4) );
				//o.Alpha = 0;

			} else {
				o.Alpha=1-IN.color.a;
			}


		#else

			o.Alpha=1-IN.color.a;
		#endif

		half colDesat=dot(o.Albedo,0.33333);
            o.Albedo=lerp(colDesat.xxx, o.Albedo, _Saturation);
			o.Albedo*= _Brightness;

		#if defined(UNITY_PASS_PREPASSFINAL)
			o.Gloss*=o.Alpha;
		#endif			
		//o.Albedo*=_MainColor*2;		
	}
	ENDCG
      
    } 
	FallBack "Diffuse"
}
