Shader "Relief Pack/Terrain2Geometry4Layers" {
Properties {
	[HideInInspector]
	_Control ("Control1 (RGBA)", 2D) = "red" {} 

	_Control1 ("Control1 (RGBA)", 2D) = "red" {}  

	_SplatA0 ("Detailmap 0 (RGB+A spec)", 2D) = "black" {}
	_SplatA1 ("Detailmap 1 (RGB+A spec)", 2D) = "black" {}
	_SplatA2 ("Detailmap 2 (RGB+A spec)", 2D) = "black" {}
	_SplatA3 ("Detailmap 3 (RGB+A spec)", 2D) = "black" {}

	_BumpMap01 ("Bumpmap combined 0+1 (RG+BA)", 2D) = "grey" {}
	_BumpMap23 ("Bumpmap combined 2+3 (RG+BA)", 2D) = "grey" {}

	_TERRAIN_HeightMap ("Heightmap combined (RGBA - layers 0-3)", 2D) = "white" {}	

		
	terrainTileSize ("terrainTileSize", Vector) = (600,200,600,1)


	
	_TERRAIN_ReliefTransform ("TERRAIN ReliefTransform", Vector) = (1,1,1,1)

	_TERRAIN_distance_start ("TERRAIN distance start", Float) = 1
	_TERRAIN_distance_transition ("TERRAIN_distance transition", Float) = 1

	_RTP_MIP_BIAS ("RTP MIP BIAS", Float) = 1

	RTP_DeferredAddPassSpec ("RTP_DeferredAddPassSpec", Float) = 1

	RTP_AOamp ("RTP AOamp", Float) = 1
	RTP_AOsharpness ("RTP AOsharpness", Float) = 1

	_TessYOffset ("Tess Y Offset", Range(0.1,10)) = 3.0
	_TessSubdivisions ("Tess Subdivisions", Range(-50,50)) = 15
	_TessSubdivisionsFar ("Tes Subdivisions Far", Range(-50,50)) = 15
	_SpecColor("_SpecColor", Color) = (0.8,0.8,0.8,1.0)

	_GlobalColorPerLayer0123 ("Global Color Per Layer (0123)", Vector) = (1,1,1,1)
	_LayerBrightness0123  ("Layer Brightness (0123)", Vector) = (1,1,1,1)
	_LayerSaturation0123 ("Layer Saturation (0123)", Vector) = (1,1,1,1)
	_LayerBrightness2Spec0123 ("Layer Brightness2 Spec (0123)", Vector) = (1,1,1,1)
	_LayerAlbedo2SpecColor0123 ("Layer Albedo2 Spec Color (0123)", Vector) = (1,1,1,1)


	_Spec0123 ("Spec (0123)", Vector) = (1,1,1,1)
	_MIPmult0123 ("MIPmult (0123)", Vector) = (1,1,1,1)
	_TessStrenght0123 ("TessStrenght (0123)", Vector) = (1,1,1,1)

	RTP_gloss2mask0123 ("RTP gloss2mask (0123)", Vector) = (1,1,1,1)
	RTP_gloss_mult0123 ("RTP gloss mult (0123)", Vector) = (1,1,1,1)
	RTP_gloss_shaping0123 ("RTP gloss shaping (0123)", Vector) = (1,1,1,1)

	RTP_AO_0123 ("RTP AO (0123)", Vector) = (1,1,1,1)



}


SubShader {
	Tags {
		"Queue" = "Geometry+2"
		"RenderType" = "Opaque"
	}
	LOD 700
	Fog { Mode Off }
	//Offset -1,-1
	CGPROGRAM
	#pragma surface surf CustomBlinnPhong vertex:vert tessellate:tessEdge tessphong:_Phong nolightmap
	// U5 fog handling
	#pragma multi_compile_fog	
	#pragma target 5.0
	#pragma glsl
	#pragma only_renderers d3d11
	#pragma multi_compile RTP_SIMPLE_SHADING

	#define _4LAYERS
	#define CURVE
	#define TESS

	#include "UnityCG.cginc"
	#include "RTP_Base.cginc"
	ENDCG
}	




}