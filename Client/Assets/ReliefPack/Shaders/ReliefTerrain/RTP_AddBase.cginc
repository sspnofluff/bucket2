// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

// Upgrade NOTE: unity_Scale shader variable was removed; replaced 'unity_Scale.w' with '1.0'

#ifdef RTP_STANDALONE
	#define LIGHTDIR_UNAVAILABLE
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// you might want to tweak below defines by hand (commenting/uncommenting, but it's recommended to leave it for RTP_manager)
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// (we don't tessellate terrain in geom blend base)
#if !defined(COLOR_EARLY_EXIT) && defined(UNITY_CAN_COMPILE_TESSELLATION)
	// optional tessellation switch
	#define TESSELLATION
	// do we sample height&normal texture ? (if undefined tessellation benefits come only from phong smoothing)
	//#define SAMPLE_TEXTURE_TESSELLATION
	// when we're in tessellation we can additionally sample heightmap with bicubic (instead of hardware bilinear) filtering
	// helps smoothening a lot when we've got aburpt heightmap changes
	//#define HEIGHTMAP_SAMPLE_BICUBIC
	// sampling detail textures ?
	//#define DETAIL_HEIGHTMAP_SAMPLE
#endif

// layer count switch (add pass - we can't undef this !)
#define _4LAYERS

// turn on when you need to skip all specularity PBL lighting (can gain up to 2-3ms per pass on Intel HD4000)
//#define NO_SPECULARITY

// U5 8layers mode workaround
#if defined(UNITY_PI) && !defined(_4LAYERS) && defined(FAR_ONLY)
	#undef FAR_ONLY
#endif

// ATLASING in 4 layers mode to save 3 texture samplers
//#define RTP_USE_COLOR_ATLAS

// if you're using this shader on arbitrary mesh you can control splat coverage via vertices colors
// note that you won't be able to blend objects when VERTEX_COLOR_CONTROL is defined
// (add pass - we can't def this !)
//#define VERTEX_COLOR_CONTROL

// to compute far color basing only on global colormap
#define SIMPLE_FAR

// uv blending
//#define RTP_UV_BLEND
//#define RTP_DISTANCE_ONLY_UV_BLEND
//#define RTP_NORMALS_FOR_REPLACE_UV_BLEND
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// UV blend routing defines section
//
// DON'T touch defines below... (unless you know exactly what you're doing) - lines 30-52
#if !defined(_4LAYERS) || defined(RTP_USE_COLOR_ATLAS)
	#define UV_BLEND_SRC_0 (tex2Dlod(_SplatAtlasC, float4(uvSplat01M.xy, _MixMipActual.xx)).rgba)
	#define UV_BLEND_SRC_1 (tex2Dlod(_SplatAtlasC, float4(uvSplat01M.zw, _MixMipActual.yy)).rgba)
	#define UV_BLEND_SRC_2 (tex2Dlod(_SplatAtlasC, float4(uvSplat23M.xy, _MixMipActual.zz)).rgba)
	#define UV_BLEND_SRC_3 (tex2Dlod(_SplatAtlasC, float4(uvSplat23M.zw, _MixMipActual.ww)).rgba)
	#define UV_BLEND_SRC_4 (tex2Dlod(_SplatAtlasB, float4(uvSplat01M.xy, _MixMipActual.xx)).rgba)
	#define UV_BLEND_SRC_5 (tex2Dlod(_SplatAtlasB, float4(uvSplat01M.zw, _MixMipActual.yy)).rgba)
	#define UV_BLEND_SRC_6 (tex2Dlod(_SplatAtlasB, float4(uvSplat23M.xy, _MixMipActual.zz)).rgba)
	#define UV_BLEND_SRC_7 (tex2Dlod(_SplatAtlasB, float4(uvSplat23M.zw, _MixMipActual.ww)).rgba)
#else
	#define UV_BLEND_SRC_0 (tex2Dlod(_SplatC0, float4(uvSplat01M.xy, _MixMipActual.xx)).rgba)
	#define UV_BLEND_SRC_1 (tex2Dlod(_SplatC1, float4(uvSplat01M.zw, _MixMipActual.yy)).rgba)
	#define UV_BLEND_SRC_2 (tex2Dlod(_SplatC2, float4(uvSplat23M.xy, _MixMipActual.zz)).rgba)
	#define UV_BLEND_SRC_3 (tex2Dlod(_SplatC3, float4(uvSplat23M.zw, _MixMipActual.ww)).rgba)
#endif
#define UV_BLENDMIX_SRC_0 (_MixScale89AB.x)
#define UV_BLENDMIX_SRC_1 (_MixScale89AB.y)
#define UV_BLENDMIX_SRC_2 (_MixScale89AB.z)
#define UV_BLENDMIX_SRC_3 (_MixScale89AB.w)
#define UV_BLENDMIX_SRC_4 (_MixScale4567.x)
#define UV_BLENDMIX_SRC_5 (_MixScale4567.y)
#define UV_BLENDMIX_SRC_6 (_MixScale4567.z)
#define UV_BLENDMIX_SRC_7 (_MixScale4567.w)
// As we've got defined some shader parts, you can tweak things in following lines
////////////////////////////////////////////////////////////////////////

// for example, when you'd like layer 3 to be source for uv blend on layer 0 you'd set it like this:
//   #define UV_BLEND_ROUTE_LAYER_0 UV_BLEND_SRC_3
// HINT: routing one layer into all will boost performance as only 1 additional texture fetch will be performed in shader (instead of up to 8 texture fetches in default setup)
//
#define UV_BLEND_ROUTE_LAYER_0 UV_BLEND_SRC_0
#define UV_BLEND_ROUTE_LAYER_1 UV_BLEND_SRC_1
#define UV_BLEND_ROUTE_LAYER_2 UV_BLEND_SRC_2
#define UV_BLEND_ROUTE_LAYER_3 UV_BLEND_SRC_3
#define UV_BLEND_ROUTE_LAYER_4 UV_BLEND_SRC_4
#define UV_BLEND_ROUTE_LAYER_5 UV_BLEND_SRC_5
#define UV_BLEND_ROUTE_LAYER_6 UV_BLEND_SRC_6
#define UV_BLEND_ROUTE_LAYER_7 UV_BLEND_SRC_7
// below routing shiould be exactly the same as above
#define UV_BLENDMIX_ROUTE_LAYER_0 UV_BLENDMIX_SRC_0
#define UV_BLENDMIX_ROUTE_LAYER_1 UV_BLENDMIX_SRC_1
#define UV_BLENDMIX_ROUTE_LAYER_2 UV_BLENDMIX_SRC_2
#define UV_BLENDMIX_ROUTE_LAYER_3 UV_BLENDMIX_SRC_3
#define UV_BLENDMIX_ROUTE_LAYER_4 UV_BLENDMIX_SRC_4
#define UV_BLENDMIX_ROUTE_LAYER_5 UV_BLENDMIX_SRC_5
#define UV_BLENDMIX_ROUTE_LAYER_6 UV_BLENDMIX_SRC_6
#define UV_BLENDMIX_ROUTE_LAYER_7 UV_BLENDMIX_SRC_7
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//// comment below detail when not needed
//#define RTP_SUPER_DETAIL
//#define RTP_SUPER_DTL_MULTS
// comment below if you don't use snow features
//#define RTP_SNOW
// layer number taken as snow normal for near distance (for deep snow cover)
//#define RTP_SNW_CHOOSEN_LAYER_NORM_0
// layer number taken as snow color/gloss for near distance
//#define RTP_SNW_CHOOSEN_LAYER_COLOR_0

// comment if you don't need global color map
#define COLOR_MAP
// if not defined global color map will be blended (lerp)
//#define COLOR_MAP_BLEND_MULTIPLY
// advanced colormap blending per layer
//#define ADV_COLOR_MAP_BLENDING

// global normal map 
#ifndef TESSELLATION
//#define RTP_NORMALGLOBAL
#endif

// global trees/shadow map - used with Terrain Composer / World Composer by Nathaniel Doldersum
//#define RTP_TREESGLOBAL

// global ambient emissive map
//#define RTP_AMBIENT_EMISSIVE_MAP

// heightblend fake AO
#define RTP_HEIGHTBLEND_AO

//  layer emissiveness
//#define RTP_EMISSION
// when wetness is defined and fuild on surface is emissive we can mod its emisiveness by output normal (wrinkles of flowing "water")
// below define change the way we treat output normals (works fine for "lava" like emissive fuilds)
#define RTP_FUILD_EMISSION_WRAP
// with optional reafractive distortion to emulate hot air turbulence
//#define RTP_HOTAIR_EMISSION

// when defined you can see where layers 0-3 overlap layers 4-7 in 8 per pass mode. These areas costs higher
//  (note that when RTP_HARD_CROSSPASS is defined you won't see any overlapping areas)
//#define RTP_SHOW_OVERLAPPED
// when defined we don't calculate overlapping 0-3 vs 4-7 layers in 8 layers mode, but take "higher"
// it's recommended to use this define for significantly better performance
// undef it only when you really need smooth transitions between overlapping groups
//#define RTP_HARD_CROSSPASS

// define for harder heightblend edges
#define SHARPEN_HEIGHTBLEND_EDGES_PASS1
//#define SHARPEN_HEIGHTBLEND_EDGES_PASS2

// firstpass triplanar (handles PM only - no POM - in 8 layers mode triplanar only for first 4 layers)
//#define RTP_TRIPLANAR

// vertical texture
//#define RTP_VERTICAL_TEXTURE

// we use wet (can't be used with superdetail as globalnormal texture BA channels are shared)
//#define RTP_WETNESS
// water droplets
//#define RTP_WET_RIPPLE_TEXTURE
// if defined water won't handle flow nor refractions
//#define SIMPLE_WATER

//#define RTP_CAUSTICS
// when we use caustics and vertical texture - with below defined we will store vertical texture and caustics together (RGB - vertical texture, A - caustics) to save texture sampler
//#define RTP_VERTALPHA_CAUSTICS

// reflection map
//#define RTP_REFLECTION
#define RTP_ROTATE_REFLECTION

// if you don't use extrude reduction in layers properties (equals 0 everywhere)
// you can comment below - POM will run a bit faster
//#define USE_EXTRUDE_REDUCTION

// cutting holes functionality (make global colormap alpha channel completely black to cut)
//#define RTP_CUT_HOLES

//
// in 8 layers mode we can use simplier shading for not overplapped (RTP_HARD_CROSSPASS must be defined) 4-7 layers
// available options are:
// RTP_47SHADING_SIMPLE
// RTP_47SHADING_PM
// RTP_47SHADING_POM_LO (POM w/o shadows)
// RTP_47SHADING_POM_MED (POM with hard shadows)
// RTP_47SHADING_POM_HI (POM with soft shadows)
//

// in presence of 2 passes we can do heightblend between passes
#define RTP_CROSSPASS_HEIGHTBLEND

// must be defined when we use 12 layers
#define _12LAYERS

// fog type (in Unity 5 it's defined via shader variant)
// RTP_FOG_EXP2, RTP_FOG_EXPONENTIAL, RTP_FOG_LINEAR
#ifndef UNITY_PI
#define RTP_FOG_EXP2
#endif

//
// below setting isn't reflected in LOD manager, it's only available here (and in RTP_ADDPBase.cginc)
// you can use it to control snow coverage from wet mask (special combined texture channel B)
//
//#define RTP_SNW_COVERAGE_FROM_WETNESS

// indepentent tiling switch (for multiple terrains that are of different size or we'd like to use different tiling on separate terrains)
//#define RTP_INDEPENDENT_TILING

// complementary lights
//#define RTP_COMPLEMENTARY_LIGHTS
// complementary lights with spec (active only with above define)
//#define RTP_SPEC_COMPLEMENTARY_LIGHTS

// physically based shading - use fresnel (if you ask - in IBL we use fresnel by default)
// works fine in forward, in deferred it's calculated for one light only (this set via ReliefShader_applyLightForDeferred.cs)
//#define RTP_PBL_FRESNEL
// physically based shading - visibility function (enhance a bit specularity)
//#define RTP_PBL_VISIBILITY_FUNCTION
// should be left defined unless you're using 3rd party PBL/PBS shading solutions that redefines hidden internal prepass shader for deferred
// (for example if you install Lux open source package - dig forum for more info - you should comment below define)
#define RTP_DEFERRED_PBL_NORMALISATION

// use IBL diffuse cubemap
//#define RTP_IBL_DIFFUSE
// use IBL specular cubemap
//#define RTP_IBL_SPEC

// we're working in LINEAR / GAMMA (used in IBL  fresnel , PBL fresnel and gloss calcs)
// if not defined we're rendering in GAMMA
//#define RTP_COLORSPACE_LINEAR

// if defined we'll use cubemap defined by skyshop in its "Sky" GameObject
//#define RTP_SKYSHOP_SYNC
// define if you'd like to to sync RTP cubemap sampling with skyshop's rotated cubemaps
#define RTP_SKYSHOP_SKY_ROTATION

// helper for cross layer specularity / IBL / Refl bleeding
//#define NOSPEC_BLEED

// if not defined we will decode LDR cubemaps (RGB only)
#define IBL_HDR_RGBM
	
/////////////////////////////////////////////////////////////////////
//
// massive terrain - super simple mode
//
// if defined we're using very simple mode (4 layers only !)
// uses global color, global normal (optionaly), pixel trees / shadows (optionaly)
//#define SUPER_SIMPLE

// for super simple mode above:
// use grayscale detail colors (will be colorized by global colormap)
#define SS_GRAYSCALE_DETAIL_COLORS
// use bumpmapping
//#define SS_USE_BUMPMAPS
// use perlin
//#define SS_USE_PERLIN

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// don't touch below defines
//
// we're using mapped shadows for best performance (available only in 4 layers mode when using atlas)
// not implemented yet
//#define RTP_MAPPED_SHADOWS

#ifdef _4LAYERS
	#ifdef RTP_HARD_CROSSPASS
		#undef RTP_HARD_CROSSPASS
	#endif
#endif

#if defined(COLOR_EARLY_EXIT) || !defined(APPROX_TANGENTS)
	#ifdef RTP_CUT_HOLES
		#undef RTP_CUT_HOLES
	#endif
#endif

#ifdef COLOR_MAP
	#define  COLOR_DAMP_VAL (global_color_value.a)
#else
	#define  COLOR_DAMP_VAL 1
#endif

#if defined(UNITY_PASS_SHADOWCASTER)
	// turn off higher shading
	#if defined(RTP_POM_SHADING_HI)
		#undef RTP_POM_SHADING_HI
	#elif defined(RTP_POM_SHADING_MED)
		#undef RTP_POM_SHADING_MED
	#elif defined(RTP_POM_SHADING_LO)
		#undef RTP_POM_SHADING_LO
	#endif
	
	// turn off snow
	#if defined(RTP_SNOW)
		#undef RTP_SNOW
	#endif
	
	#if defined(RTP_WETNESS)
		#undef RTP_WETNESS
	#endif
	
	#if defined(RTP_CAUSTICS)
		#undef RTP_CAUSTICS
	#endif
	
	// turn off reflection additional stuff that needs INTERNAL_DATA to compute world normal / reflection
	#if defined(RTP_REFLECTION)
		#undef RTP_REFLECTION
	#endif
	
	#if defined(RTP_IBL_SPEC)
		#undef RTP_IBL_SPEC
	#endif
	
	#if defined(RTP_IBL_DIFFUSE)
		#undef RTP_IBL_DIFFUSE
	#endif
	
	#if defined(RTP_SKYSHOP_SYNC)
		#undef RTP_SKYSHOP_SYNC
	#endif
#endif

#ifdef RTP_POM_SHADING_HI
	#define RTP_POM_SHADING
	#define RTP_SOFT_SHADOWS
#endif
#ifdef RTP_POM_SHADING_MED
	#define RTP_POM_SHADING
	#define RTP_HARD_SHADOWS
#endif
#ifdef RTP_POM_SHADING_LO
	#define RTP_POM_SHADING
	#define RTP_NO_SHADOWS
#endif




// not used anymore in RTP3.1 due to new functionality that breaks it (replacement UV blend that depends on far distance)
// (przed poniższym define musi być przynajmniej jedna spacje aby LOD manager tego nie psuł)
//#ifdef RTP_SIMPLE_SHADING
//	#ifndef RTP_DISTANCE_ONLY_UV_BLEND
//         #define RTP_DISTANCE_ONLY_UV_BLEND
//	#endif
//#endif

#define GETrtp_snow_TEX rtp_snow_color_tex.rgb=csnow.rgb;rtp_snow_gloss=lerp(saturate(csnow.a*rtp_snow_gloss*2), 1, saturate((rtp_snow_gloss-0.5)*2));

// wyłącz kolor selction kiedy zabraknie samplerów
#if defined(_4LAYERS) && (!defined(RTP_USE_COLOR_ATLAS) || defined(RTP_MAPPED_SHADOWS))
#if ( defined(RTP_SNOW) && !defined(UNITY_PASS_SHADOWCASTER) ) && (defined(RTP_VERTICAL_TEXTURE) && !defined(RTP_VERTALPHA_CAUSTICS)) && ( defined(RTP_WETNESS) && !defined(UNITY_PASS_SHADOWCASTER) ) && defined(RTP_WET_RIPPLE_TEXTURE)
	#ifdef RTP_SNW_CHOOSEN_LAYER_COLOR_4
		#undef RTP_SNW_CHOOSEN_LAYER_COLOR_4
	#endif 
	#ifdef RTP_SNW_CHOOSEN_LAYER_COLOR_5
		#undef RTP_SNW_CHOOSEN_LAYER_COLOR_5
	#endif 
	#ifdef RTP_SNW_CHOOSEN_LAYER_COLOR_6
		#undef RTP_SNW_CHOOSEN_LAYER_COLOR_6
	#endif 
	#ifdef RTP_SNW_CHOOSEN_LAYER_COLOR_7
		#undef RTP_SNW_CHOOSEN_LAYER_COLOR_7
	#endif 
#endif
#endif
// wyłącz wet ripple kiedy mamy inne opcje wszystkie powlaczane
#if defined(_4LAYERS) && defined(RTP_MAPPED_SHADOWS) && (defined(RTP_VERTICAL_TEXTURE) && !defined(RTP_VERTALPHA_CAUSTICS)) && ( defined(RTP_WETNESS) && !defined(UNITY_PASS_SHADOWCASTER) ) && defined(RTP_WET_RIPPLE_TEXTURE)
	#undef RTP_WET_RIPPLE_TEXTURE
#endif
// potrzebujemy miejsca na dodatkowe samplery cieni
#if defined(RTP_MAPPED_SHADOWS)
	#if !defined(_4LAYERS)
		#undef RTP_MAPPED_SHADOWS
	#else
		//#define RTP_USE_COLOR_ATLAS
	#endif
#endif

#if ( defined(RTP_WETNESS) && !defined(UNITY_PASS_SHADOWCASTER) ) || defined(RTP_AMBIENT_EMISSIVE_MAP) || (defined(COLOR_MAP) && defined(ADV_COLOR_MAP_BLENDING))
	#define NEED_LOCALHEIGHT
#endif

#ifndef RTP_PBL_FRESNEL
	#define pbl_fresnel_term 1
#endif
#ifndef RTP_PBL_VISIBILITY_FUNCTION
	#define pbl_visibility_term 1
#endif
	
#if defined(SHADER_API_D3D11) 
CBUFFER_START(rtpConstants)
#endif

#ifdef TEX_SPLAT_REDEFINITION
	// texture samplers redefinitions (due to Unity bug that makes Unity4 terrain material unusable)
	#define _Control3 _Control
	#define _ColorMapGlobal _Splat0
	#define _NormalMapGlobal _Splat1
	#define _Control1 _Splat2
	#if defined(RTP_AMBIENT_EMISSIVE_MAP)
		#define _AmbientEmissiveMapGlobal _Splat2
	#else
		#define _TreesMapGlobal _Splat2
	#endif
	#define _BumpMapGlobal _Splat3
	sampler2D _Control;
#else
	sampler2D _Control, _Control3, _Control2, _Control1;
#endif


float3 terrainTileSize;

sampler2D _SplatAtlasC, _SplatAtlasB;
sampler2D _SplatC0, _SplatC1, _SplatC2, _SplatC3;
sampler2D _SplatB0, _SplatB1, _SplatB2, _SplatB3;
sampler2D _BumpMap89, _BumpMapAB, _BumpMap45, _BumpMap67;
sampler2D _ColorMapGlobal;

sampler2D _BumpMapGlobal;
sampler2D _SSColorCombinedB; 
float2 _terrain_size;
float _BumpMapGlobalScale;
float3 _GlobalColorMapBlendValues;
float _GlobalColorMapSaturation;
float _GlobalColorMapSaturationFar;
float _GlobalColorMapBrightness;
float _GlobalColorMapBrightnessFar;
float _GlobalColorMapNearMIP;
float _GlobalColorMapDistortByPerlin;
//float _GlobalColorMapSaturationByPerlin;
float EmissionRefractFiltering;
float EmissionRefractAnimSpeed;
float RTP_DeferredAddPassSpec;

float4 _MixScale89AB, _MixBlend89AB;
float4 _MixScale4567, _MixBlend4567;
float4 _GlobalColorPerLayer89AB, _GlobalColorPerLayer4567;
float4  _LayerBrightness89AB, _LayerSaturation89AB, _LayerBrightness2Spec89AB, _LayerAlbedo2SpecColor89AB;
float4  _LayerBrightness4567, _LayerSaturation4567, _LayerBrightness2Spec4567, _LayerAlbedo2SpecColor4567;
float4 _MixSaturation89AB, _MixBrightness89AB, _MixReplace89AB;
float4 _MixSaturation4567, _MixBrightness4567, _MixReplace4567;
float4 _LayerEmission89AB, _LayerEmission4567;
float4 _LayerEmissionRefractStrength89AB, _LayerEmissionRefractHBedge89AB;
float4 _LayerEmissionRefractStrength4567, _LayerEmissionRefractHBedge4567;
half4 _LayerEmissionColorR89AB;
half4 _LayerEmissionColorR4567;
half4 _LayerEmissionColorG89AB;
half4 _LayerEmissionColorG4567;
half4 _LayerEmissionColorB89AB;
half4 _LayerEmissionColorB4567;
half4 _LayerEmissionColorA89AB;
half4 _LayerEmissionColorA4567;
// adv global colormap blending
half4 _GlobalColorBottom89AB, _GlobalColorBottom4567;
half4 _GlobalColorTop89AB, _GlobalColorTop4567;
half4 _GlobalColorColormapLoSat89AB, _GlobalColorColormapLoSat4567;
half4 _GlobalColorColormapHiSat89AB, _GlobalColorColormapHiSat4567;
half4 _GlobalColorLayerLoSat89AB, _GlobalColorLayerLoSat4567;
half4 _GlobalColorLayerHiSat89AB, _GlobalColorLayerHiSat4567;
half4 _GlobalColorLoBlend89AB, _GlobalColorLoBlend4567;
half4 _GlobalColorHiBlend89AB, _GlobalColorHiBlend4567;

float4 _Spec89AB;
float4 _Spec4567;
float4 _MIPmult89AB;
float4 _MIPmult4567;

sampler2D _TERRAIN_HeightMap3, _TERRAIN_HeightMap2, _TERRAIN_HeightMap;
float4 _TERRAIN_HeightMap3_TexelSize;
float4 _TERRAIN_HeightMap2_TexelSize;
float4 _SplatAtlasC_TexelSize;
float4 _SplatAtlasB_TexelSize;
float4 _SplatC0_TexelSize;
float4 _BumpMapGlobal_TexelSize;
float4 _TERRAIN_ReliefTransform;
float _TERRAIN_ReliefTransformTriplanarZ;
float _TERRAIN_DIST_STEPS;
float _TERRAIN_WAVELENGTH;

float _blend_multiplier;

float _TERRAIN_ExtrudeHeight;
float _TERRAIN_LightmapShading;

float _TERRAIN_SHADOW_STEPS;
float _TERRAIN_WAVELENGTH_SHADOWS;
//float _TERRAIN_SHADOW_SMOOTH_STEPS; // not used sice RTP3.2d
float _TERRAIN_SelfShadowStrength;
float _TERRAIN_ShadowSmoothing;
float _TERRAIN_ShadowSoftnessFade; // new in RTP3.2d

float rtp_mipoffset_color;
float rtp_mipoffset_bump;
float rtp_mipoffset_height;
float rtp_mipoffset_superdetail;
float rtp_mipoffset_flow;
float rtp_mipoffset_ripple;
float rtp_mipoffset_globalnorm;
float rtp_mipoffset_caustics;

// caustics
float TERRAIN_CausticsAnimSpeed;
half4 TERRAIN_CausticsColor;
float TERRAIN_CausticsWaterLevel;
float TERRAIN_CausticsWaterLevelByAngle;
float TERRAIN_CausticsWaterDeepFadeLength;
float TERRAIN_CausticsWaterShallowFadeLength;
float TERRAIN_CausticsTilingScale;
sampler2D TERRAIN_CausticsTex;

// for vert texture stored in alpha channel of caustics
float4 TERRAIN_CausticsTex_TexelSize;

///////////////////////////////////////////
//
// reflection
//
half4 TERRAIN_ReflColorA;
half4 TERRAIN_ReflColorB;
float TERRAIN_ReflectionRotSpeed; // 0-2, 0.3
float TERRAIN_ReflGlossAttenuation; // 0..1
half4 TERRAIN_ReflColorC;
float TERRAIN_ReflColorCenter; // 0.1 - 0.9

//
// water/wet
//
// global
float TERRAIN_GlobalWetness; // 0-1

sampler2D TERRAIN_RippleMap;
float4 TERRAIN_RippleMap_TexelSize;
float TERRAIN_RippleScale; // 4
float TERRAIN_FlowScale; // 1
float TERRAIN_FlowSpeed; // 0 - 3 (0.5)
float TERRAIN_FlowCycleScale; // 0.5 - 4 (1)
float TERRAIN_FlowMipOffset; // 0

float TERRAIN_RainIntensity; // 1
float TERRAIN_DropletsSpeed; // 10
float TERRAIN_WetDarkening;
float TERRAIN_WetDropletsStrength; // 0-1
float TERRAIN_WetHeight_Treshold;
float TERRAIN_WetHeight_Transition;

float TERRAIN_mipoffset_flowSpeed; // 0-5

// per layer
float4 TERRAIN_LayerWetStrength89AB; // 0 - 1 (1)
float4 TERRAIN_LayerWetStrength4567;

float4 TERRAIN_WaterLevel89AB; // 0 - 2 (0.5)
float4 TERRAIN_WaterLevel4567;
float4 TERRAIN_WaterLevelSlopeDamp89AB; // 0.25 - 32 (2)
float4 TERRAIN_WaterLevelSlopeDamp4567;
float4 TERRAIN_WaterEdge89AB; // 1 - 16 (2)
float4 TERRAIN_WaterEdge4567;

float4 TERRAIN_WaterOpacity89AB; // 0 - 1 (0.3)
float4 TERRAIN_WaterOpacity4567;
float4 TERRAIN_Refraction89AB; // 0 - 0.04 (0.01)
float4 TERRAIN_Refraction4567; 
float4 TERRAIN_WetRefraction89AB; // 0 - 1 (0.25)
float4 TERRAIN_WetRefraction4567; 
float4 TERRAIN_Flow89AB; // 0 - 1 (0.1)
float4 TERRAIN_Flow4567;
float4 TERRAIN_WetSpecularity89AB; // -1 - 1 (0)
float4 TERRAIN_WetSpecularity4567;

float4 TERRAIN_WetFlow89AB; // 0 - 1 (0.1)
float4 TERRAIN_WetFlow4567;
float4 TERRAIN_WetGloss89AB; // -1 - 1 (0)
float4 TERRAIN_WetGloss4567;
float4 TERRAIN_WaterSpecularity89AB; // -1 - 1 (0)
float4 TERRAIN_WaterSpecularity4567;
float4 TERRAIN_WaterGloss89AB; // -1 - 1 (0)
float4 TERRAIN_WaterGloss4567;
float4 TERRAIN_WaterGlossDamper89AB; // 0 - 1
float4 TERRAIN_WaterGlossDamper4567;
float4 TERRAIN_WaterEmission89AB; // 0-1 (0)
float4 TERRAIN_WaterEmission4567;
half4 TERRAIN_WaterColorR89AB;
half4 TERRAIN_WaterColorR4567;
half4 TERRAIN_WaterColorG89AB;
half4 TERRAIN_WaterColorG4567;
half4 TERRAIN_WaterColorB89AB;
half4 TERRAIN_WaterColorB4567;
half4 TERRAIN_WaterColorA89AB;
half4 TERRAIN_WaterColorA4567;

///////////////////////////////////////////

float _TERRAIN_distance_start;
float _TERRAIN_distance_transition;

float _TERRAIN_distance_start_bumpglobal;
float _TERRAIN_distance_transition_bumpglobal;
float rtp_perlin_start_val;
float4 _BumpMapGlobalStrength89AB;
float4 _BumpMapGlobalStrength4567;

sampler2D _NormalMapGlobal;
float4 _NormalMapGlobal_TexelSize;
sampler2D _TreesMapGlobal;
sampler2D _AmbientEmissiveMapGlobal;
float _AmbientEmissiveMultiplier;
float _AmbientEmissiveRelief;
float4 _TERRAIN_trees_shadow_values;
float4 _TERRAIN_trees_pixel_values;

float _RTP_MIP_BIAS;

float4 PER_LAYER_HEIGHT_MODIFIER89AB;
float4 PER_LAYER_HEIGHT_MODIFIER4567;

float _SuperDetailTiling;
float4 _SuperDetailStrengthMultA89AB;
float4 _SuperDetailStrengthMultA4567;
float4 _SuperDetailStrengthMultB89AB;
float4 _SuperDetailStrengthMultB4567;
float4 _SuperDetailStrengthNormal89AB;
float4 _SuperDetailStrengthNormal4567;

float4 _SuperDetailStrengthMultASelfMaskNear89AB;
float4 _SuperDetailStrengthMultASelfMaskNear4567;
float4 _SuperDetailStrengthMultASelfMaskFar89AB;
float4 _SuperDetailStrengthMultASelfMaskFar4567;
float4 _SuperDetailStrengthMultBSelfMaskNear89AB;
float4 _SuperDetailStrengthMultBSelfMaskNear4567;
float4 _SuperDetailStrengthMultBSelfMaskFar89AB;
float4 _SuperDetailStrengthMultBSelfMaskFar4567;

float4 _VerticalTexture_TexelSize;
sampler2D _VerticalTexture;
float _VerticalTextureTiling;
float _VerticalTextureGlobalBumpInfluence;
float4 _VerticalTexture89AB;
float4 _VerticalTexture4567;

float rtp_global_color_brightness_to_snow;
float rtp_snow_slope_factor;
float rtp_snow_edge_definition;
float4 rtp_snow_strength_per_layer89AB;
float4 rtp_snow_strength_per_layer4567;
float rtp_snow_height_treshold;
float rtp_snow_height_transition;

float rtp_snow_fresnel;
float rtp_snow_diff_fresnel;
float rtp_snow_IBL_DiffuseStrength;
float rtp_snow_IBL_SpecStrength;

fixed4 rtp_snow_color;
float rtp_snow_gloss;
float rtp_snow_specular;
float rtp_snow_deep_factor;

float _Phong;
float4 _QOffset;
float _Dist;
float _EdgeLength;
///////////////////////////////////////////////////////////////////////////////////////////////
// lighting & IBL
///////////////////////////////////////////////////////////////////////////////////////////////
half3 rtp_customAmbientCorrection;

#define RTP_BackLightStrength RTP_LightDefVector.x
#define RTP_ReflexLightDiffuseSoftness RTP_LightDefVector.y
#define RTP_ReflexLightSpecSoftness RTP_LightDefVector.z
#define RTP_ReflexLightSpecularity RTP_LightDefVector.w
float4 RTP_LightDefVector;
half4 RTP_ReflexLightDiffuseColor1;
half4 RTP_ReflexLightDiffuseColor2;
half4 RTP_ReflexLightSpecColor;

#if ( defined(RTP_SKYSHOP_SYNC) && !defined(UNITY_PASS_SHADOWCASTER) )
	#define _CubemapSpec _SpecCubeIBL
	
	// SH IBL lighting taken under permission from Skyshop MarmosetCore.cginc
	uniform float3		_SH0;
	uniform float3		_SH1;
	uniform float3		_SH2;
	uniform float3		_SH3;
	uniform float3		_SH4;
	uniform float3		_SH5;
	uniform float3		_SH6;
	uniform float3		_SH7;
	uniform float3		_SH8;	
	float3 SHLookup(float3 dir) {
		//l = 0 band (constant)
		float3 result = _SH0.xyz;

		//l = 1 band
		result += _SH1.xyz * dir.y;
		result += _SH2.xyz * dir.z;
		result += _SH3.xyz * dir.x;

		//l = 2 band
		float3 swz = dir.yyz * dir.xzx;
		result += _SH4.xyz * swz.x;
		result += _SH5.xyz * swz.y;
		result += _SH7.xyz * swz.z;
		float3 sqr = dir * dir;
		result += _SH6.xyz * ( 3.0*sqr.z - 1.0 );
		result += _SH8.xyz * ( sqr.x - sqr.y );
		
		return abs(result);
	}	
	float4 _ExposureIBL;	
#endif
samplerCUBE _CubemapDiff;
samplerCUBE _CubemapSpec;
float4x4	_SkyMatrix;// set globaly by skyshop

// PBL / IBL
half4 RTP_gloss2mask89AB, RTP_gloss2mask4567;
half4 RTP_gloss_mult89AB, RTP_gloss_mult4567;
half4 RTP_gloss_shaping89AB, RTP_gloss_shaping4567;
half4 RTP_Fresnel89AB, RTP_Fresnel4567;
half4 RTP_FresnelAtten89AB, RTP_FresnelAtten4567;
half4 RTP_DiffFresnel89AB, RTP_DiffFresnel4567;
// IBL
half4 RTP_IBL_bump_smoothness89AB, RTP_IBL_bump_smoothness4567;
half4 RTP_IBL_DiffuseStrength89AB, RTP_IBL_DiffuseStrength4567;
half4 RTP_IBL_SpecStrength89AB, RTP_IBL_SpecStrength4567;
// used in deferred - add pass only
half4 _DeferredSpecDampAddPass89AB;

half4 TERRAIN_WaterIBL_SpecWetStrength89AB, TERRAIN_WaterIBL_SpecWetStrength4567;
half4 TERRAIN_WaterIBL_SpecWaterStrength89AB, TERRAIN_WaterIBL_SpecWaterStrength4567;

half TERRAIN_IBL_DiffAO_Damp;
half TERRAIN_IBLRefl_SpecAO_Damp;
//
///////////////////////////////////////////////////////////////////////////////////////////////

float RTP_AOamp;
float4 RTP_AO_89AB, RTP_AO_4567, RTP_AO_0123;
float RTP_AOsharpness;
#if defined(SHADER_API_D3D11) 
CBUFFER_END
#endif

float rtp_snow_strength;

#ifdef UNITY_PASS_PREPASSFINAL
uniform float4 _WorldSpaceLightPosCustom;
#endif

#ifdef UNITY_PASS_META
	float4 _MainTex_ST;
#endif

struct Input {
	
	float2 uv_Control; // texcoords from mesh
	float3 worldPos;
	float3 viewDir;
	float3 worldNormal;
	float3 worldRefl;
	INTERNAL_DATA
	fixed4 color:COLOR;
};

inline float3 BlendNormalsRTP(float3 global_norm, float3 detail) {
	float3 _t = global_norm;
	_t.z += 1;
	float3 _u = float3(-detail.xy, detail.z);
	return normalize(_t*dot(_t, _u) - _u*_t.z);
}

struct RTPSurfaceOutput {
	fixed3 Albedo;
	fixed3 Normal;
	fixed3 Emission;
	half Specular;
	fixed Alpha;
	float2 RTP;
	half3 SpecColor;
	float distance; // for fog handling
};

fixed3 _FColor;
float _Fdensity;
float _Fstart,_Fend;
void customFog (Input IN, RTPSurfaceOutput o, inout fixed4 color) {
	#ifdef UNITY_PI
		//
		// Unity 5 fog
		//
		// we use distance alone, even for mobile
		#ifdef UNITY_APPLY_FOG_COLOR
			#undef UNITY_APPLY_FOG_COLOR
			#if defined(FOG_LINEAR) || defined(FOG_EXP) || defined(FOG_EXP2)
				// always compute fog factor per pixel (so RTP has chance to work on mobile, too)
				#define UNITY_APPLY_FOG_COLOR(coord,col,fogCol) UNITY_CALC_FOG_FACTOR(coord); UNITY_FOG_LERP_COLOR(col,fogCol,unityFogFactor)
			#else
				#define UNITY_APPLY_FOG_COLOR(coord,col,fogCol)
			#endif
		#endif
	
		UNITY_APPLY_FOG(o.distance, color);
	#else
		//
		// Unity3/4 custom fog
		//
		#if defined(RTP_FOG_LINEAR)
			float f=saturate((_Fend-o.distance)/(_Fend-_Fstart));
			#ifdef UNITY_PASS_FORWARDADD
				color.rgb*=f;
			#else		
				color.rgb=lerp(_FColor, color.rgb, f);
			#endif
		#else
			#if defined(RTP_FOG_EXP2)
				float val=_Fdensity*o.distance;
				float f=exp2(-val*val*1.442695);
				#ifdef UNITY_PASS_FORWARDADD
					color.rgb*=f;
				#else
					color.rgb=lerp(_FColor, color.rgb, f);
				#endif
			#else
				#if defined(RTP_FOG_EXPONENTIAL)
					float g=log2(1-_Fdensity);
					float f=exp2(g*o.distance);
					#ifdef UNITY_PASS_FORWARDADD
						color.rgb*=f;
					#else				
						color.rgb=lerp(_FColor, color.rgb, f);
					#endif
				#endif
			#endif
		#endif

	#endif
}

#include "Assets/ReliefPack/Shaders/CustomLighting.cginc"

inline float3 myObjSpaceLightDir( in float4 v )
{
	#ifdef UNITY_PASS_PREPASSFINAL
		float4 lpos=_WorldSpaceLightPosCustom;
	#else
		float4 lpos=_WorldSpaceLightPos0;
	#endif	
	float3 objSpaceLightPos = mul(unity_WorldToObject, lpos).xyz;
	#ifndef USING_LIGHT_MULTI_COMPILE
		#ifdef UNITY_PASS_PREPASSFINAL
			return objSpaceLightPos.xyz * 1.0 - v.xyz * lpos.w;
		#else
			return objSpaceLightPos.xyz * 1.0 - v.xyz * lpos.w;
		#endif
	#else
		#ifndef USING_DIRECTIONAL_LIGHT
		return objSpaceLightPos.xyz * 1.0 - v.xyz;
		#else
		return objSpaceLightPos.xyz;
		#endif
	#endif
}

/////////////////////////////////
//
// optional sampling heightmap / normalmap & tessellation
//

    struct appdata {
        float4 vertex : POSITION;
        float4 tangent : TANGENT;
        float3 normal : NORMAL;
        float2 texcoord : TEXCOORD0;
        float2 texcoord1 : TEXCOORD1;
        #ifdef UNITY_PI
        // dynamic lightmaps
        float2 texcoord2 : TEXCOORD2;
        #endif
        fixed4 color : COLOR;
    };

    float _TessSubdivisions;
    float _TessSubdivisionsFar;
    float _TessYOffset;
	
	float interpolate_bicubic(float x, float y, out float3 norm) {
		//x=frac(x);
		//y=frac(y);
		x*=_NormalMapGlobal_TexelSize.z;
		y*=_NormalMapGlobal_TexelSize.w;
		// transform the coordinate from [0,extent] to [-0.5, extent-0.5]
		float2 coord_grid = float2(x - 0.5, y - 0.5);
		float2 index = floor(coord_grid);
		float2 fraction = coord_grid - index;
		float2 one_frac = 1.0 - fraction;
		float2 one_frac2 = one_frac * one_frac;
		float2 fraction2 = fraction * fraction;
		float2 w0 = 1.0/6.0 * one_frac2 * one_frac;
		float2 w1 = 2.0/3.0 - 0.5 * fraction2 * (2.0-fraction);
		float2 w2 = 2.0/3.0 - 0.5 * one_frac2 * (2.0-one_frac);
		float2 w3 = 1.0/6.0 * fraction2 * fraction;
		float2 g0 = w0 + w1;
		float2 g1 = w2 + w3;
		// h0 = w1/g0 - 1, move from [-0.5, extent-0.5] to [0, extent]
		float2 h0 = (w1 / g0) - 0.5 + index;
		float2 h1 = (w3 / g1) + 1.5 + index;
		h0*=_NormalMapGlobal_TexelSize.x;
		h1*=_NormalMapGlobal_TexelSize.y;
		// fetch the four linear interpolations
		float4 val;
		val= tex2Dlod(_NormalMapGlobal, float4(h0.x, h0.y,0,0));
		float tex00 = val.g/255+val.r;
		float2 tex00_N = val.ba;
		val = tex2Dlod(_NormalMapGlobal, float4(h1.x, h0.y,0,0)); 
		float tex10 = val.g/255+val.r;
		float2 tex10_N = val.ba;
		val = tex2Dlod(_NormalMapGlobal, float4(h0.x, h1.y,0,0));
		float tex01 = val.g/255+val.r;
		float2 tex01_N = val.ba;
		val = tex2Dlod(_NormalMapGlobal, float4(h1.x, h1.y,0,0));
		float tex11 = val.g/255+val.r;
		float2 tex11_N = val.ba;

		// normal
		// weigh along the y-direction
		tex00_N = lerp(tex01_N, tex00_N, g0.y);
		tex10_N = lerp(tex11_N, tex10_N, g0.y);
		// weigh along the x-direction
		tex00_N = lerp(tex10_N, tex00_N, g0.x)*2-1;
		norm=float3(tex00_N.x, sqrt(1 - saturate(dot(tex00_N, tex00_N))), tex00_N.y);

		// weigh along the y-direction
		tex00 = lerp(tex01, tex00, g0.y);
		tex10 = lerp(tex11, tex10, g0.y);
		// weigh along the x-direction
		return lerp(tex10, tex00, g0.x);
	} 

//    float4 tessEdge (appdata v0, appdata v1, appdata v2)
//    {
//        return UnityEdgeLengthBasedTess (v0.vertex, v1.vertex, v2.vertex, _TessSubdivisions);
//    }	
    float4 tessEdge (appdata v0, appdata v1, appdata v2)
    {
		float3 pos0 = mul(unity_ObjectToWorld,v0.vertex).xyz;
		float3 pos1 = mul(unity_ObjectToWorld,v1.vertex).xyz;
		float3 pos2 = mul(unity_ObjectToWorld,v2.vertex).xyz;
		float4 tess;
		// distance to edge center
		float3 edge_dist;
		edge_dist.x = distance (0.5 * (pos1+pos2), _WorldSpaceCameraPos);
		edge_dist.y = distance (0.5 * (pos2+pos0), _WorldSpaceCameraPos);
		edge_dist.z = distance (0.5 * (pos0+pos1), _WorldSpaceCameraPos);
		float3 dist = saturate((edge_dist.xyz - _TERRAIN_distance_start_bumpglobal) / _TERRAIN_distance_transition_bumpglobal);
		float4 tessFactor;
		tessFactor.x = lerp(_TessSubdivisions, _TessSubdivisionsFar, dist.x);
		tessFactor.y = lerp(_TessSubdivisions, _TessSubdivisionsFar, dist.y);
		tessFactor.z = lerp(_TessSubdivisions, _TessSubdivisionsFar, dist.z);
		tessFactor.w = (tessFactor.x+tessFactor.y+tessFactor.z)/3;
		return tessFactor;
    }

/////////////////////////////////

float4 _Splat0_ST;
float2 RTP_CustomTiling;


	void vert (inout appdata v, Input o) {
		
		#if defined(CURVE)
		o.worldPos = mul(unity_ObjectToWorld, v.vertex);

			float4 vPos = mul (UNITY_MATRIX_MV, v.vertex);
			float zOff = vPos.z/_Dist;
			vPos += _QOffset*zOff*zOff;
			//v.vertex.xyz = 
			v.vertex = mul (vPos, UNITY_MATRIX_IT_MV);



		#endif
		////////////////////////////
		//
		// tessellation
		#ifdef TESS

				float3 wPos=mul(unity_ObjectToWorld, v.vertex).xyz;

				float2 _INPUT_uv=wPos.xz / _TERRAIN_ReliefTransform.xy + _TERRAIN_ReliefTransform.zw;

				float _INPUT_distance=distance(_WorldSpaceCameraPos, wPos);
					float _uv_Relief_z=saturate((_INPUT_distance - _TERRAIN_distance_start) / _TERRAIN_distance_transition);
					float detailMIPlevel=_uv_Relief_z*5;
					_uv_Relief_z=1-_uv_Relief_z;
					
		 		float d = saturate(lerp(tex2Dlod(_TERRAIN_HeightMap, float4(_INPUT_uv.xy,0,detailMIPlevel)), 1, PER_LAYER_HEIGHT_MODIFIER0123)+0.001);
                d = d * 0.5 - 0.5 + 1.0f;
                v.vertex.xyz += v.normal * d;
         #endif
		
		
		//
		//
		///////////////////////////		

		

		// for v.normal.x=1 or -1 tangent would be not resolvable so adding a tiny bit to z (0.0001) makes it less probable to happen
		float3 wTangent = float3(1, 0, 0.0001);
		v.tangent.xyz = normalize( cross(v.normal.xyz, cross(wTangent, v.normal) ) );
		v.tangent.w = -1.0;


		#if !defined(LIGHTDIR_UNAVAILABLE)		
			float3 binormal = cross( v.normal, v.tangent.xyz ) * v.tangent.w;
			float3x3 rotation = float3x3( v.tangent.xyz, binormal, v.normal.xyz );

			float3 objSpaceLDir=myObjSpaceLightDir(v.vertex); // wyznaczone powyżej
			float3 lightDir = mul (rotation, objSpaceLDir);
			v.color.rgb = normalize(lightDir.xyz); // beware - SM2.0 this is clamped to 0..1 !
		#endif


					
}

// tex2D helper
inline fixed4 tex2Dp(sampler2D tex, float2 uv, float2 mddx, float2 mddy) {
	#if defined(RTP_STANDALONE) || defined(GEOM_BLEND)
		return fixed4(tex2D(tex, uv, mddx, mddy));
	#else
		return fixed4(tex2D(tex, uv));
	#endif
}
inline fixed4 tex2Dd(sampler2D tex, float2 uv, float2 mddx, float2 mddy) {
	#if !SHADER_API_GLES
		return fixed4(tex2D(tex, uv, mddx, mddy));
	#else
		// webGL on firefox doesn't like derivate texture calls
		return fixed4(tex2D(tex, uv));
	#endif
}

void surf (Input IN, inout RTPSurfaceOutput o) {
	o.Normal=float3(0,0,1); o.Albedo=0;	o.Specular=RTP_DeferredAddPassSpec; o.Alpha=0;
	o.Emission=0;
	o.RTP.xy=float2(0,1); o.SpecColor=0;
	half o_Gloss=0;
	
//	o.Albedo=0.5;//IN.color.rgb;//+IN.viewDir+frac(IN.worldPos);
//	return;
	
	float3 _INPUT_uv=0;


	_INPUT_uv.xy=IN.worldPos.xz / _TERRAIN_ReliefTransform.xy + _TERRAIN_ReliefTransform.zw;

	float _INPUT_distance=distance(_WorldSpaceCameraPos, IN.worldPos);
	o.distance=_INPUT_distance; // needed for fog
	
	

		#ifdef COLOR_EARLY_EXIT
			float3 worldNormalFlat=WorldNormalVector(IN, float3(0,0,1)); // WORLD norm w/o bumpmaps	
		#else
			#if defined(UNITY_PASS_SHADOWCASTER)
				float3 worldNormalFlat=float3(0,0,1);// dummy value in caster pass (we don't have an access to WorldNormalVector due to lack of INTERNAL data)
			#else
				float3 worldNormalFlat=WorldNormalVector(IN, float3(0,0,1)); // WORLD norm w/o bumpmaps	
			#endif
		#endif

	
	
	//
	// world to tangent basis (used in global normalmap and when we need vertical vector (0,1,0) in tangent space	
	//
	float3 vertexNorm=worldNormalFlat;
	#if !defined(APPROX_TANGENTS) || defined(RTP_STANDALONE)
		vertexNorm=normalize(mul(unity_WorldToObject, float4(vertexNorm,0)).xyz);
	#else
		// terrains are not rotated - world normal = object normal then
	#endif
	float3 tangentBase = normalize( cross(vertexNorm, cross(float3(1, 0, 0.0001), vertexNorm) ) );
	float3 binormalBase = -cross( vertexNorm, tangentBase );
	
	#if defined(RTP_STANDALONE) || defined(GEOM_BLEND)
		float2 globalUV=(IN.worldPos.xz-_TERRAIN_PosSize.xy)/_TERRAIN_PosSize.zw;
		#define IN_uv_Control (globalUV)
		// used in tex2Dp() but compiled out anyway
		float2 _ddxGlobal=ddx(IN_uv_Control);
		float2 _ddyGlobal=ddy(IN_uv_Control);
	#else
		#ifdef UNITY_PASS_META	
			IN.uv_Control.xy=IN.uv_Control.xy*_MainTex_ST.xy+_MainTex_ST.zw;
		#endif	
		#define IN_uv_Control (IN.uv_Control)
		// used in tex2Dp() but compiled out anyway
		#define _ddxGlobal float2(0,0)
		#define _ddyGlobal float2(0,0)
	#endif
	float tmpBias=exp2(-_RTP_MIP_BIAS/(0.75*2));
	float3 _ddxMain=ddx(_INPUT_uv.xyz)/tmpBias;
	float3 _ddyMain=ddy(_INPUT_uv.xyz)/tmpBias;
	
	
	
	#if defined(SUPER_SIMPLE)

		
	#else
	//
	// regular mode
	//	
	float2 mip_selector;
	float2 IN_uv=_INPUT_uv.xy*1024*(1+_RTP_MIP_BIAS);
	float2 dx = ddx( IN_uv);
	float2 dy = ddy( IN_uv);
	
	#ifdef RTP_STANDALONE
		//
		// RTP standalone shader
		//
	#else
		//
		// RTP script controlled shader
		//
		#if defined(COLOR_EARLY_EXIT) && !defined(RTP_TRIPLANAR)
			if (IN.color.a<0.002) return;
		#endif
		
		#ifdef VERTEX_COLOR_CONTROL
			float4 splat_controlA = IN.color;
		#else
			float4 splat_controlA = tex2Dp(_Control3, IN_uv_Control, _ddxGlobal, _ddyGlobal);
		#endif
	 	float total_coverage=dot(splat_controlA, 1);
		#ifdef _4LAYERS
			//float4 splat_controlA_normalized=splat_controlA/total_coverage;
		#else
			float4 splat_controlB = tex2Dp(_Control2, IN_uv_Control, _ddxGlobal, _ddyGlobal);
		 	total_coverage+=dot(splat_controlB, 1);
			//float4 splat_controlA_normalized=splat_controlA/total_coverage;
			//float4 splat_controlB_normalized=splat_controlB/total_coverage;
		#endif
	#endif
	
	float _uv_Relief_w=saturate((_INPUT_distance - _TERRAIN_distance_start_bumpglobal) / _TERRAIN_distance_transition_bumpglobal);
	#ifdef FAR_ONLY
		#define _uv_Relief_z 0
		float _uv_Relief_wz_no_overlap=_uv_Relief_w;
	#else
		float _uv_Relief_z=saturate((_INPUT_distance - _TERRAIN_distance_start) / _TERRAIN_distance_transition);
		float _uv_Relief_wz_no_overlap=_uv_Relief_w*_uv_Relief_z;
		_uv_Relief_z=1-_uv_Relief_z;
	#endif
	
	// main MIP selector
	float d = max( dot( dx, dx ), dot( dy, dy ) );
	mip_selector=0.5*log2(d);
	
	#if !defined(RTP_TRIPLANAR) 
		float4 global_bump_val=tex2Dlod(_BumpMapGlobal, float4(_INPUT_uv.xy*_BumpMapGlobalScale, mip_selector+rtp_mipoffset_globalnorm));
		global_bump_val.rg=global_bump_val.rg*0.6 + tex2Dlod(_BumpMapGlobal, float4(_INPUT_uv.xy*_BumpMapGlobalScale*8, mip_selector+rtp_mipoffset_globalnorm+3)).rg*0.4;
	#endif
		
	IN.viewDir=normalize(IN.viewDir);
	IN.viewDir.z=saturate(IN.viewDir.z); // czasem wystepuja problemy na krawedziach widocznosci (viewDir.z nie powinien byc jednak ujemny)
		
	
		
	#ifdef COLOR_MAP
		float global_color_blend=lerp( lerp(_GlobalColorMapBlendValues.y, _GlobalColorMapBlendValues.x, _uv_Relief_z*_uv_Relief_z), _GlobalColorMapBlendValues.z, _uv_Relief_w);
		
			#if defined(RTP_SIMPLE_SHADING) || defined(COLOR_EARLY_EXIT)
				float4 global_color_value=tex2Dp(_ColorMapGlobal, IN_uv_Control, _ddxGlobal, _ddyGlobal);
			#else
				float4 global_color_value=tex2Dp(_ColorMapGlobal, IN_uv_Control+(global_bump_val.rg-float2(0.5f, 0.5f))*_GlobalColorMapDistortByPerlin, _ddxGlobal, _ddyGlobal);
			#endif
			
		
		// bez colormapy nie ma optymalizacji (ale najczesciej ją mamy)
		#if !defined(RTP_PLANET)
			if (total_coverage<0.001) return;
		#endif
		
		#if defined(RTP_STANDALONE) && !defined(RTP_PLANET)
			global_color_value.rgb=lerp(tex2Dlod(_ColorMapGlobal, float4(IN_uv_Control+(global_bump_val.rg-float2(0.5f, 0.5f))*_GlobalColorMapDistortByPerlin, _GlobalColorMapNearMIP.xx)).rgb, global_color_value.rgb, _uv_Relief_w);
		#else
			global_color_value=lerp(tex2Dlod(_ColorMapGlobal, float4(IN_uv_Control+(global_bump_val.rg-float2(0.5f, 0.5f))*_GlobalColorMapDistortByPerlin, _GlobalColorMapNearMIP.xx)), global_color_value, _uv_Relief_w);
		#endif
		
	
		float GlobalColorMapSaturationByPerlin = lerp(_GlobalColorMapSaturation, _GlobalColorMapSaturationFar, _uv_Relief_w);
		global_color_value.rgb=lerp(dot(global_color_value.rgb,0.35).xxx, global_color_value.rgb, GlobalColorMapSaturationByPerlin);
		global_color_value.rgb*=lerp(_GlobalColorMapBrightness, _GlobalColorMapBrightnessFar, _uv_Relief_w);
	#endif

	float4 tHA;
	float4 tHB=0;
	float4 splat_control1 = splat_controlA;
	
		#ifdef USE_EXTRUDE_REDUCTION
			tHA=saturate(lerp(tex2Dd(_TERRAIN_HeightMap3, _INPUT_uv.xy, _ddxMain.xy, _ddyMain.xy), 1, PER_LAYER_HEIGHT_MODIFIER89AB)+0.001);
		#else
			tHA=saturate(tex2Dd(_TERRAIN_HeightMap3, _INPUT_uv.xy, _ddxMain.xy, _ddyMain.xy)+0.001);
		#endif	
	
	splat_control1 *= tHA;
	
	#ifdef _4LAYERS
		float4 splat_control1_mid=splat_control1*splat_control1;
		splat_control1_mid/=dot(splat_control1_mid,1);
		float4 splat_control1_close=splat_control1_mid*splat_control1_mid;
		#ifdef SHARPEN_HEIGHTBLEND_EDGES_PASS1
			splat_control1_close*=splat_control1_close;
		#endif
		#ifdef SHARPEN_HEIGHTBLEND_EDGES_PASS2
			splat_control1_close*=splat_control1_close;
		#endif
		splat_control1_close/=dot(splat_control1_close,1);
		splat_control1=lerp(splat_control1_mid, splat_control1_close, _uv_Relief_z);
		#ifdef NOSPEC_BLEED		
			float4 splat_control1_nobleed=saturate(splat_control1-float4(0.5,0.5,0.5,0.5))*2;
		#else
			float4 splat_control1_nobleed=splat_control1;
		#endif
	#else
		float4 splat_control1_mid=splat_control1*splat_control1;
		float4 splat_control2 = splat_controlB;
		#ifdef USE_EXTRUDE_REDUCTION
			tHB=saturate(lerp(tex2Dd(_TERRAIN_HeightMap2, _INPUT_uv.xy, _ddxMain.xy, _ddyMain.xy), 1, PER_LAYER_HEIGHT_MODIFIER4567)+0.001);
			splat_control2 *= tHB;
		#else
			tHB=saturate(tex2Dd(_TERRAIN_HeightMap2, _INPUT_uv.xy, _ddxMain.xy, _ddyMain.xy)+0.001);
			splat_control2 *= tHB;
		#endif	
		float4 splat_control2_mid=splat_control2*splat_control2;
		float norm_sum=dot(splat_control1_mid,1) + dot(splat_control2_mid,1);
		splat_control1_mid/=norm_sum;
		splat_control2_mid/=norm_sum;
		
		float4 splat_control1_close=splat_control1_mid*splat_control1_mid;
		float4 splat_control2_close=splat_control2_mid*splat_control2_mid;
		#ifdef SHARPEN_HEIGHTBLEND_EDGES_PASS1
			splat_control1_close*=splat_control1_close;
			splat_control2_close*=splat_control2_close;
		#endif
		#ifdef SHARPEN_HEIGHTBLEND_EDGES_PASS2
			splat_control1_close*=splat_control1_close;
			splat_control2_close*=splat_control2_close;
		#endif
		norm_sum=dot(splat_control1_close,1) + dot(splat_control2_close,1);
		splat_control1_close/=norm_sum;
		splat_control2_close/=norm_sum;
		splat_control1=lerp(splat_control1_mid, splat_control1_close, _uv_Relief_z);
		splat_control2=lerp(splat_control2_mid, splat_control2_close, _uv_Relief_z);
		#ifdef NOSPEC_BLEED		
			float4 splat_control1_nobleed=saturate(splat_control1-float4(0.5,0.5,0.5,0.5))*2;
			float4 splat_control2_nobleed=saturate(splat_control2-float4(0.5,0.5,0.5,0.5))*2;
		#else
			float4 splat_control1_nobleed=splat_control1;
			float4 splat_control2_nobleed=splat_control2;
		#endif
	#endif
	
	float splat_controlA_coverage=dot(splat_control1, 1);
	#ifndef _4LAYERS
	float splat_controlB_coverage=dot(splat_control2, 1);
	#endif
	
	// layer emission - init step
		
	#if defined(NEED_LOCALHEIGHT)
		float actH=dot(splat_control1, tHA);
		#ifndef _4LAYERS
			actH+=dot(splat_control2, tHB);
		#endif
	#endif
	
	// simple fresnel rim (w/o bumpmapping)
	float diffFresnel = exp2(SchlickFresnelApproxExp2Const*IN.viewDir.z); // ca. (1-x)^5
	#ifdef APPROX_TANGENTS
		// dla RTP na meshach (przykladowa scena)  normalne sa plaskie i ponizszy tweak dziala b. zle
		//diffFresnel = (diffFresnel>0.999) ? ((1-diffFresnel)/0.001) : (diffFresnel/0.999);
	#endif
	
	    
 	
		
	fixed3 col=0;
	fixed3 colAlbedo=0;
	
	float3 norm_far=float3(0,0,1);
	
			
	norm_far.xy = global_bump_val.rg*3-1.5;
	norm_far.z = sqrt(1 - saturate(dot(norm_far.xy, norm_far.xy)));
	
	
	
	float2 IN_uv_Relief_Offset;
	
	
	float4 MIPmult89AB=_MIPmult89AB*_uv_Relief_w;
	#ifndef _4LAYERS
		float4 MIPmult4567=_MIPmult4567*_uv_Relief_w;
	#endif
	
	#ifdef FAR_ONLY
	if (false) {
	#else
	if (_uv_Relief_z>0) {
	#endif
 		//////////////////////////////////
 		//
 		// close
 		//
 		//////////////////////////////////
		#if !defined(_4LAYERS) || defined(RTP_USE_COLOR_ATLAS)
			float _off=16*_SplatAtlasC_TexelSize.x;
			float _mult=_off*-2+0.5;
			float4 _offMix=_off;
			float4 _multMix=_mult;

			float4 uvSplat01, uvSplat23;
			float4 uvSplat01M, uvSplat23M;
		#endif

	 	float4 rayPos = float4(_INPUT_uv.xy, 1, clamp((mip_selector.x+rtp_mipoffset_height), 0, 6) );

	 	#ifdef TRANSITION

			///////////////////////////////////////////////
	 		//
	 		// splats 0-7 close combined transition
	 		//
	 		///////////////////////////////////////////////

	 	#endif
	 			
		#ifndef _4LAYERS 		

		 	if (splat_controlA_coverage>0.01 && splat_controlB_coverage>0.01) {
	 	
	 		//////////////////////////////////////////////
	 		//
	 		// splats 0-7 close combined
	 		//
	 		///////////////////////////////////////////////
	 		#ifdef RTP_SHOW_OVERLAPPED
	 		o.Emission.r=1;
	 		#endif

	 		
				#ifdef RTP_PM_SHADING
					rayPos.xy += ParallaxOffset(dot(splat_control1, tHA)+dot(splat_control2, tHB), _TERRAIN_ExtrudeHeight*_uv_Relief_z*COLOR_DAMP_VAL, IN.viewDir.xyz);
				#endif
				#if defined(NEED_LOCALHEIGHT)
					actH=lerp(actH, dot(splat_control1, tHA)+dot(splat_control2, tHB), _uv_Relief_z);
				#endif
				
									
			uvSplat01=frac(rayPos.xy).xyxy*_mult+_off;
			uvSplat01.zw+=float2(0.5,0);
			uvSplat23=uvSplat01.xyxy+float4(0,0.5,0.5,0.5);
			
 	

			fixed4 c;
			float4 gloss;
			float _MipActual=min(mip_selector.x+rtp_mipoffset_color,6);
			c = tex2Dlod(_SplatAtlasC, float4(uvSplat01.xy, _MipActual.xx)); col = splat_control1.x * c.rgb; gloss.r = c.a;
			c = tex2Dlod(_SplatAtlasC, float4(uvSplat01.zw, _MipActual.xx)); col += splat_control1.y * c.rgb; gloss.g = c.a;
			c = tex2Dlod(_SplatAtlasC, float4(uvSplat23.xy, _MipActual.xx)); col += splat_control1.z * c.rgb; gloss.b = c.a;
			c = tex2Dlod(_SplatAtlasC, float4(uvSplat23.zw, _MipActual.xx)); col += splat_control1.w * c.rgb; gloss.a = c.a;
			
			float glcombined = dot(gloss, splat_control1);
							
			
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat01.xy, _MipActual.xx)); col += splat_control2.x * c.rgb; gloss.r = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat01.zw, _MipActual.xx)); col += splat_control2.y * c.rgb; gloss.g = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat23.xy, _MipActual.xx)); col += splat_control2.z * c.rgb; gloss.b = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat23.zw, _MipActual.xx)); col += splat_control2.w * c.rgb; gloss.a = c.a;

			glcombined += dot(gloss, splat_control2);
						
			

			float RTP_gloss2mask = dot(splat_control1, RTP_gloss2mask89AB)+dot(splat_control2, RTP_gloss2mask4567)			;
			float _Spec = dot(splat_control1_nobleed, _Spec89AB) + dot(splat_control2_nobleed, _Spec4567); // anti-bleed subtraction
			float RTP_gloss_mult = dot(splat_control1, RTP_gloss_mult89AB)+dot(splat_control2, RTP_gloss_mult4567);
			float RTP_gloss_shaping = dot(splat_control1, RTP_gloss_shaping89AB)+dot(splat_control2, RTP_gloss_shaping4567);
			float gls = saturate(glcombined * RTP_gloss_mult);

			o_Gloss = lerp(1, gls, RTP_gloss2mask) * _Spec; // przemnóż przez damping dla warstw 4567
			
			float2 gloss_shaped=float2(gls, 1-gls);
			gloss_shaped=gloss_shaped*gloss_shaped*gloss_shaped;
			gls=lerp(gloss_shaped.x, 1-gloss_shaped.y, RTP_gloss_shaping);
			o.Specular = 0;//saturate(gls);
			// gloss vs. fresnel dependency
	
			half colDesat=dot(col,0.33333);
			float brightness2Spec=dot(splat_control1, _LayerBrightness2Spec89AB) + dot(splat_control2, _LayerBrightness2Spec4567);
			o_Gloss*=lerp(1, colDesat, brightness2Spec);
			//colAlbedo=col;
			//col=lerp(colDesat.xxx, col, dot(splat_control1, _LayerSaturation89AB) + dot(splat_control2, _LayerSaturation4567));
			//col*=dot(splat_control1, _LayerBrightness89AB) + dot(splat_control2, _LayerBrightness4567);  
			col = 0;
			float3 n;
			float4 normals_combined;
			rayPos.w=mip_selector.x+rtp_mipoffset_bump;
				
			normals_combined = tex2Dlod(_BumpMap89, rayPos.xyww).rgba*splat_control1.rrgg;
			normals_combined+=tex2Dlod(_BumpMapAB, rayPos.xyww).rgba*splat_control1.bbaa;
			normals_combined+=tex2Dlod(_BumpMap45, rayPos.xyww).rgba*splat_control2.rrgg;
			normals_combined+=tex2Dlod(_BumpMap67, rayPos.xyww).rgba*splat_control2.bbaa;
			n.xy=(normals_combined.rg+normals_combined.ba)*2-1;
			n.xy*=_uv_Relief_z;
			n.z = sqrt(1 - saturate(dot(n.xy, n.xy)));
			o.Normal=n;
	        			
					
			IN_uv_Relief_Offset.xy=rayPos.xy;
			
		 	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		 	//
		 	// self shadowing 
		 	//
	 			
	 	} else if (splat_controlA_coverage>splat_controlB_coverage)
	 	#endif // !_4LAYERS 		
	 	{
	 		//////////////////////////////////
	 		//
	 		// splats 0-3 close
	 		//
	 		//////////////////////////////////
	 		
					
			#if !defined(_4LAYERS) || defined(RTP_USE_COLOR_ATLAS)
				#if !defined(RTP_TRIPLANAR)
					uvSplat01=frac(rayPos.xy).xyxy*_mult+_off;
					uvSplat01.zw+=float2(0.5,0);
					uvSplat23=uvSplat01.xyxy+float4(0,0.5,0.5,0.5);
				#endif
			#endif			
			
	 		
			fixed4 c;
			float4 gloss=0;
			

				#if !defined(_4LAYERS) || defined(RTP_USE_COLOR_ATLAS)
					float _MipActual=min(mip_selector.x + rtp_mipoffset_color, 6);
					c = tex2Dlod(_SplatAtlasC, float4(uvSplat01.xy, _MipActual.xx)); col = splat_control1.x * c.rgb; gloss.r = c.a;
					c = tex2Dlod(_SplatAtlasC, float4(uvSplat01.zw, _MipActual.xx)); col += splat_control1.y * c.rgb; gloss.g = c.a;
					c = tex2Dlod(_SplatAtlasC, float4(uvSplat23.xy, _MipActual.xx)); col += splat_control1.z * c.rgb; gloss.b = c.a;
					c = tex2Dlod(_SplatAtlasC, float4(uvSplat23.zw, _MipActual.xx)); col += splat_control1.w * c.rgb; gloss.a = c.a;
				#else
					rayPos.w=mip_selector.x+rtp_mipoffset_color;
					c = tex2Dd(_SplatC0, rayPos.xy, _ddxMain.xy, _ddyMain.xy); col = splat_control1.x * c.rgb; gloss.r = c.a;
					c = tex2Dd(_SplatC1, rayPos.xy, _ddxMain.xy, _ddyMain.xy); col += splat_control1.y * c.rgb; gloss.g = c.a;
					c = tex2Dd(_SplatC2, rayPos.xy, _ddxMain.xy, _ddyMain.xy); col += splat_control1.z * c.rgb; gloss.b = c.a;
					c = tex2Dd(_SplatC3, rayPos.xy, _ddxMain.xy, _ddyMain.xy); col += splat_control1.w * c.rgb; gloss.a = c.a;
				#endif


			float glcombined = dot(gloss, splat_control1);
						
			
			float RTP_gloss2mask = dot(splat_control1, RTP_gloss2mask89AB);
			float _Spec = dot(splat_control1_nobleed, _Spec89AB); // anti-bleed subtraction
			float RTP_gloss_mult = dot(splat_control1, RTP_gloss_mult89AB);
			float RTP_gloss_shaping = dot(splat_control1, RTP_gloss_shaping89AB);
			float gls = saturate(glcombined * RTP_gloss_mult);
			o_Gloss =  lerp(1, gls, RTP_gloss2mask) * _Spec;
			
			float2 gloss_shaped=float2(gls, 1-gls);
			gloss_shaped=gloss_shaped*gloss_shaped*gloss_shaped;
			gls=lerp(gloss_shaped.x, 1-gloss_shaped.y, RTP_gloss_shaping);
			o.Specular = 0;//saturate(gls);
			// gloss vs. fresnel dependency
		
			half colDesat=dot(col,0.33333);
			float brightness2Spec=dot(splat_control1, _LayerBrightness2Spec89AB);
			o_Gloss*=lerp(1, colDesat, brightness2Spec);
			//colAlbedo=col;
			//col=lerp(colDesat.xxx, col, dot(splat_control1, _LayerSaturation89AB));
			//col*=dot(splat_control1, _LayerBrightness89AB);  
			col = 0;
	
			IN_uv_Relief_Offset.xy=rayPos.xy;
	 		
	 	}
		#ifndef _4LAYERS 		
	 	else {
	 		//////////////////////////////////
	 		//
	 		// splats 4-7 close
	 		//
	 		//////////////////////////////////
	 				
			uvSplat01=frac(rayPos.xy).xyxy*_mult+_off;
			uvSplat01.zw+=float2(0.5,0);
			uvSplat23=uvSplat01.xyxy+float4(0,0.5,0.5,0.5);

 	
			float4 c;
			float4 gloss;
			float _MipActual=min(mip_selector.x + rtp_mipoffset_color,6);
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat01.xy, _MipActual.xx)); col = splat_control2.x * c.rgb; gloss.r = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat01.zw, _MipActual.xx)); col += splat_control2.y * c.rgb; gloss.g = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat23.xy, _MipActual.xx)); col += splat_control2.z * c.rgb; gloss.b = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat23.zw, _MipActual.xx)); col += splat_control2.w * c.rgb; gloss.a = c.a;
			
			float glcombined = dot(gloss, splat_control2);
						
			float RTP_gloss2mask = dot(splat_control2, RTP_gloss2mask4567);
			float _Spec = dot(splat_control2_nobleed, _Spec4567); // anti-bleed subtraction
			float RTP_gloss_mult = dot(splat_control2, RTP_gloss_mult4567);
			float RTP_gloss_shaping = dot(splat_control2, RTP_gloss_shaping4567);
			float gls = saturate(glcombined * RTP_gloss_mult);
			o_Gloss =  lerp(1, gls, RTP_gloss2mask) * _Spec;
			float2 gloss_shaped=float2(gls, 1-gls);
			gloss_shaped=gloss_shaped*gloss_shaped*gloss_shaped;
			gls=lerp(gloss_shaped.x, 1-gloss_shaped.y, RTP_gloss_shaping);
			o.Specular = 0;//saturate(gls);
			// gloss vs. fresnel dependency

			half colDesat=dot(col,0.33333);
			float brightness2Spec=dot(splat_control2, _LayerBrightness2Spec4567);
			o_Gloss*=lerp(1, colDesat, brightness2Spec);
			//colAlbedo=col;
			//col=lerp(colDesat.xxx, col, dot(splat_control2, _LayerSaturation4567));	
			//col*=dot(splat_control2, _LayerBrightness4567);  
			col = 0;
			float3 n;
			float4 normals_combined;
			rayPos.w=mip_selector.x+rtp_mipoffset_bump;
			normals_combined = tex2Dlod(_BumpMap45, rayPos.xyww).rgba*splat_control2.rrgg;
			normals_combined+=tex2Dlod(_BumpMap67, rayPos.xyww).rgba*splat_control2.bbaa;
			n.xy=(normals_combined.rg+normals_combined.ba)*2-1;
			n.xy*=_uv_Relief_z;
			n.z = sqrt(1 - saturate(dot(n.xy, n.xy)));
			o.Normal=n;
			
		

		 	IN_uv_Relief_Offset.xy=rayPos.xy;
		 	
		 	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		 	//
		 	// self shadowing 
		 	//

	 		
			//
		 	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
		 	
	 		// end of splats 4-7 close		 	
	 	}
		#endif //!_4LAYERS 
		
 		#if !defined(RTP_HARD_CROSSPASS) || ( defined(RTP_HARD_CROSSPASS) && (defined(RTP_47SHADING_POM_HI) || defined(RTP_47SHADING_POM_MED) || defined(RTP_47SHADING_POM_LO)) )
			#ifdef RTP_POM_SHADING
				o.RTP.y=shadow_atten;
			#endif		 			
		#endif
		
		#if defined(SIMPLE_FAR) && defined(COLOR_MAP)
			o_Gloss*=(1-_uv_Relief_w);
			o.Specular=0;//lerp(o.Specular, 0, _uv_Relief_w);
		#endif			
			
	} else {
 		//////////////////////////////////
 		//
 		// far
 		//
 		//////////////////////////////////
				
		#if !defined(_4LAYERS) || defined(RTP_USE_COLOR_ATLAS)
		float _off=16*_SplatAtlasC_TexelSize.x;
		float _mult=_off*-2+0.5;
		float4 _offMix=_off;
		float4 _multMix=_mult;
		
		float4 uvSplat01, uvSplat23;
	
		
		float4 uvSplat01M, uvSplat23M;
		#endif
		
		#if !defined(_4LAYERS) 

		 	if (splat_controlA_coverage>0.01 && splat_controlB_coverage>0.01) {
	 		//////////////////////////////////////////////
	 		//
	 		// splats 0-7 far combined
	 		//
	 		///////////////////////////////////////////////
	 		#ifdef RTP_SHOW_OVERLAPPED
	 		o.Emission.r=1;
	 		#endif
	 		
//#if ( defined(RTP_SNOW) && !defined(UNITY_PASS_SHADOWCASTER) )
//if (snow_MayBeNotFullyCovered_flag) {
//#endif			
#if defined(SIMPLE_FAR) && defined(COLOR_MAP)
			if (_uv_Relief_w<1) {
#endif
					
			float4 _MipActual=min(mip_selector.x + rtp_mipoffset_color+MIPmult89AB, 6);
			half4 c;
			float4 gloss;
			c = tex2Dlod(_SplatAtlasC, float4(uvSplat01.xy, _MipActual.xx)); col = splat_control1.x * c.rgb; gloss.r = c.a;
			c = tex2Dlod(_SplatAtlasC, float4(uvSplat01.zw, _MipActual.yy)); col += splat_control1.y * c.rgb; gloss.g = c.a;
			c = tex2Dlod(_SplatAtlasC, float4(uvSplat23.xy, _MipActual.zz)); col += splat_control1.z * c.rgb; gloss.b = c.a;
			c = tex2Dlod(_SplatAtlasC, float4(uvSplat23.zw, _MipActual.ww)); col += splat_control1.w * c.rgb; gloss.a = c.a;
			
			float glcombined = dot(gloss, splat_control1);
		
						
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat01.xy, _MipActual.xx)); col += splat_control2.x * c.rgb; gloss.r = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat01.zw, _MipActual.yy)); col += splat_control2.y * c.rgb; gloss.g = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat23.xy, _MipActual.zz)); col += splat_control2.z * c.rgb; gloss.b = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat23.zw, _MipActual.ww)); col += splat_control2.w * c.rgb; gloss.a = c.a;

			glcombined += dot(gloss, splat_control2);
						
	
			float RTP_gloss2mask = dot(splat_control1, RTP_gloss2mask89AB)+dot(splat_control2, RTP_gloss2mask4567);
			float _Spec = dot(splat_control1_nobleed, _Spec89AB) + dot(splat_control2_nobleed, _Spec4567); // anti-bleed subtraction
			float RTP_gloss_mult = dot(splat_control1, RTP_gloss_mult89AB)+dot(splat_control2, RTP_gloss_mult4567);
			float RTP_gloss_shaping = dot(splat_control1, RTP_gloss_shaping89AB)+dot(splat_control2, RTP_gloss_shaping4567);
			float gls = saturate(glcombined * RTP_gloss_mult);
			o_Gloss =  lerp(1, gls, RTP_gloss2mask) * _Spec; // *heightblend_AO // przemnóż przez damping dla warstw 4567
			float2 gloss_shaped=float2(gls, 1-gls);
			gloss_shaped=gloss_shaped*gloss_shaped*gloss_shaped;
			gls=lerp(gloss_shaped.x, 1-gloss_shaped.y, RTP_gloss_shaping);
			o.Specular = saturate(gls);
			// gloss vs. fresnel dependency

			half colDesat=dot(col,0.33333);
			float brightness2Spec=dot(splat_control1, _LayerBrightness2Spec89AB) + dot(splat_control2, _LayerBrightness2Spec4567);
			o_Gloss*=lerp(1, colDesat, brightness2Spec);
			//colAlbedo=col;
			//col=lerp(colDesat.xxx, col, dot(splat_control1, _LayerSaturation89AB) + dot(splat_control2, _LayerSaturation4567));
			//col*=dot(splat_control1, _LayerBrightness89AB) + dot(splat_control2, _LayerBrightness4567);  
			col = 0;
#if defined(SIMPLE_FAR) && defined(COLOR_MAP)
			o_Gloss*=(1-_uv_Relief_w);
			o.Specular=0;//lerp(o.Specular, 0, _uv_Relief_w);
			}
#endif
			
						

		
						
	 	} else if (splat_controlA_coverage>splat_controlB_coverage)
		#endif // !_4LAYERS
	 	{
	 		//////////////////////////////////////////////
	 		//
	 		// splats 0-3 far
	 		//
	 		///////////////////////////////////////////////
	 		
	
#if defined(SIMPLE_FAR) && defined(COLOR_MAP)
			if (_uv_Relief_w<1) {
#endif			

			half4 c;
			float4 gloss=0;			
			
			
				//
				// no triplanar
				//
				#if !defined(_4LAYERS) || defined(RTP_USE_COLOR_ATLAS)
					float4 _MixMipActual=min(mip_selector.xxxx + rtp_mipoffset_color+MIPmult89AB, float4(6,6,6,6));
					c = tex2Dlod(_SplatAtlasC, float4(uvSplat01.xy, _MixMipActual.xx)); col = splat_control1.x * c.rgb; gloss.r = c.a;
					c = tex2Dlod(_SplatAtlasC, float4(uvSplat01.zw, _MixMipActual.yy)); col += splat_control1.y * c.rgb; gloss.g = c.a;
					c = tex2Dlod(_SplatAtlasC, float4(uvSplat23.xy, _MixMipActual.zz)); col += splat_control1.z * c.rgb; gloss.b = c.a;
					c = tex2Dlod(_SplatAtlasC, float4(uvSplat23.zw, _MixMipActual.ww)); col += splat_control1.w * c.rgb; gloss.a = c.a;
				#else
					float4 _MixMipActual=mip_selector.xxxx + rtp_mipoffset_color+MIPmult89AB;
					float4 mipTweak=exp2(MIPmult89AB);
					c = tex2Dd(_SplatC0, _INPUT_uv.xy, _ddxMain.xy*mipTweak.x, _ddyMain.xy*mipTweak.x); col = splat_control1.x * c.rgb; gloss.r = c.a;
					c = tex2Dd(_SplatC1, _INPUT_uv.xy, _ddxMain.xy*mipTweak.y, _ddyMain.xy*mipTweak.y); col += splat_control1.y * c.rgb; gloss.g = c.a;
					c = tex2Dd(_SplatC2, _INPUT_uv.xy, _ddxMain.xy*mipTweak.z, _ddyMain.xy*mipTweak.z); col += splat_control1.z * c.rgb; gloss.b = c.a;
					c = tex2Dd(_SplatC3, _INPUT_uv.xy, _ddxMain.xy*mipTweak.w, _ddyMain.xy*mipTweak.w); col += splat_control1.w * c.rgb; gloss.a = c.a;				
				#endif
				
				//
				// EOF no triplanar
				//
			

						
			float glcombined = dot(gloss, splat_control1);

			float RTP_gloss2mask = dot(splat_control1, RTP_gloss2mask89AB);
			float _Spec = dot(splat_control1_nobleed, _Spec89AB); // anti-bleed subtraction
			float RTP_gloss_mult = dot(splat_control1, RTP_gloss_mult89AB);
			float RTP_gloss_shaping = dot(splat_control1, RTP_gloss_shaping89AB);
			float gls = saturate(glcombined * RTP_gloss_mult);
			o_Gloss =  lerp(1, gls, RTP_gloss2mask) * _Spec;
			float2 gloss_shaped=float2(gls, 1-gls);
			gloss_shaped=gloss_shaped*gloss_shaped*gloss_shaped;
			gls=lerp(gloss_shaped.x, 1-gloss_shaped.y, RTP_gloss_shaping);
			o.Specular = saturate(gls);
			// gloss vs. fresnel dependency

			half colDesat=dot(col,0.33333);
			float brightness2Spec=dot(splat_control1, _LayerBrightness2Spec89AB);
			o_Gloss*=lerp(1, colDesat, brightness2Spec);
			//colAlbedo=col;
			//col=lerp(colDesat.xxx, col, dot(splat_control1, _LayerSaturation89AB) );
			//col*=dot(splat_control1, _LayerBrightness89AB);  
			col = 0;
#if defined(SIMPLE_FAR) && defined(COLOR_MAP)
			o_Gloss*=(1-_uv_Relief_w);
			o.Specular=0;//lerp(o.Specular, 0, _uv_Relief_w);
			}
#endif		
	
			
			
			
									
	 	}
	 	
	 	#ifndef _4LAYERS
	 	else {
	 		//////////////////////////////////////////////
	 		//
	 		// splats 4-7 far
	 		//
	 		///////////////////////////////////////////////
	 		


#if defined(SIMPLE_FAR) && defined(COLOR_MAP)
			if (_uv_Relief_w<1) {
#endif	
			float4 _MixMipActual=min(mip_selector.xxxx + rtp_mipoffset_color+MIPmult4567, float4(6,6,6,6));
		
			half4 c;
			float4 gloss;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat01.xy, _MixMipActual.xx)); col = splat_control2.x * c.rgb; gloss.r = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat01.zw, _MixMipActual.yy)); col += splat_control2.y * c.rgb; gloss.g = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat23.xy, _MixMipActual.zz)); col += splat_control2.z * c.rgb; gloss.b = c.a;
			c = tex2Dlod(_SplatAtlasB, float4(uvSplat23.zw, _MixMipActual.ww)); col += splat_control2.w * c.rgb; gloss.a = c.a;
	
			float RTP_gloss2mask = dot(splat_control2, RTP_gloss2mask4567);
			float _Spec = dot(splat_control2_nobleed, _Spec4567); // anti-bleed subtraction
			float RTP_gloss_mult = dot(splat_control2, RTP_gloss_mult4567);
			float RTP_gloss_shaping = dot(splat_control2, RTP_gloss_shaping4567);
			float gls = saturate(glcombined * RTP_gloss_mult);
			o_Gloss =  lerp(1, gls, RTP_gloss2mask) * _Spec;
			float2 gloss_shaped=float2(gls, 1-gls);
			gloss_shaped=gloss_shaped*gloss_shaped*gloss_shaped;
			gls=lerp(gloss_shaped.x, 1-gloss_shaped.y, RTP_gloss_shaping);
			o.Specular = saturate(gls);
			// gloss vs. fresnel dependency
			#if ( defined(RTP_REFLECTION) && !defined(UNITY_PASS_SHADOWCASTER) ) || ( defined(RTP_IBL_SPEC) && !defined(UNITY_PASS_SHADOWCASTER) ) || defined(RTP_PBL_FRESNEL)
			o.RTP.x*=lerp(1, 1-fresnelAtten, o.Specular*0.9+0.1);
			#endif
			// desaturation/brightness per layer + spec driven by resultant albedo color
			half colDesat=dot(col,0.33333);
			float brightness2Spec=dot(splat_control2, _LayerBrightness2Spec4567);
			o_Gloss*=lerp(1, colDesat, brightness2Spec);
			//colAlbedo=col;
			//col=lerp(colDesat.xxx, col, dot(splat_control2, _LayerSaturation4567) );
			//col*=dot(splat_control2, _LayerBrightness4567);  
			col = 0;			
#if defined(SIMPLE_FAR) && defined(COLOR_MAP)
			o_Gloss*=(1-_uv_Relief_w);
			o.Specular=0;//lerp(o.Specular, 0, _uv_Relief_w);
			}
#endif	

				
	
					
							
	 	}
		#endif

	 	IN_uv_Relief_Offset.xy=_INPUT_uv.xy;
	}
	
	float3 norm_snowCov=o.Normal;
	
	// far distance normals from uv blend scale
	

	float _BumpMapGlobalStrengthPerLayer=1;
	#ifdef _4LAYERS
		_BumpMapGlobalStrengthPerLayer=dot(_BumpMapGlobalStrength89AB, splat_control1);
	#else
		_BumpMapGlobalStrengthPerLayer=dot(_BumpMapGlobalStrength89AB, splat_control1)+dot(_BumpMapGlobalStrength4567, splat_control2);
	#endif
	#if !defined(RTP_SIMPLE_SHADING)
		{
		float3 combinedNormal = BlendNormalsRTP(norm_far, o.Normal);
		o.Normal = lerp(o.Normal, combinedNormal, saturate(lerp(rtp_perlin_start_val,1, _uv_Relief_w)*_BumpMapGlobalStrengthPerLayer));
		}
	#else
		o.Normal+=norm_far*lerp(rtp_perlin_start_val,1, _uv_Relief_w)*_BumpMapGlobalStrengthPerLayer;	
	#endif	
	
	#ifdef _4LAYERS
	o_Gloss=lerp( saturate(o_Gloss+4*dot(splat_control1_nobleed, 1)), o_Gloss, (1-_uv_Relief_w)*(1-_uv_Relief_w) );
	#else
	o_Gloss=lerp( saturate(o_Gloss+4*(dot(splat_control1_nobleed, 1) + dot(splat_control2_nobleed, 1))), o_Gloss, (1-_uv_Relief_w)*(1-_uv_Relief_w) );
	#endif
	
	#ifdef COLOR_MAP
		float colBrightness=dot(col,1);
		#ifdef _4LAYERS
			global_color_blend *= dot(splat_control1, _GlobalColorPerLayer89AB);
		#else
				global_color_blend *= dot(splat_control1, _GlobalColorPerLayer89AB) + dot(splat_control2, _GlobalColorPerLayer4567);
		#endif
		
		#ifdef ADV_COLOR_MAP_BLENDING
			// advanced global colormap blending
			#ifdef _4LAYERS
				half advGlobalColorMapBottomLevel = dot(splat_control1, _GlobalColorBottom89AB);
				half advGlobalColorMapTopLevel = dot(splat_control1, _GlobalColorTop89AB);
				half colorMapLoSat = dot(splat_control1, _GlobalColorColormapLoSat89AB);
				half colorMapHiSat = dot(splat_control1, _GlobalColorColormapHiSat89AB);
				half colLoSat = dot(splat_control1, _GlobalColorLayerLoSat89AB);
				half colHiSat = dot(splat_control1, _GlobalColorLayerHiSat89AB);
				half advGlobalColorMapLoBlend = dot(splat_control1, _GlobalColorLoBlend89AB);
				half advGlobalColorMapHiBlend = dot(splat_control1, _GlobalColorHiBlend89AB);
			#else
				half advGlobalColorMapBottomLevel = dot(splat_control1, _GlobalColorBottom89AB) + dot(splat_control2, _GlobalColorBottom4567);
				half advGlobalColorMapTopLevel = dot(splat_control1, _GlobalColorTop89AB) + dot(splat_control2, _GlobalColorTop4567);
				half colorMapLoSat = dot(splat_control1, _GlobalColorColormapLoSat89AB) + dot(splat_control2, _GlobalColorColormapLoSat4567);
				half colorMapHiSat = dot(splat_control1, _GlobalColorColormapHiSat89AB) + dot(splat_control2, _GlobalColorColormapHiSat4567);
				half colLoSat = dot(splat_control1, _GlobalColorLayerLoSat89AB) + dot(splat_control2, _GlobalColorLayerLoSat4567);
				half colHiSat = dot(splat_control1, _GlobalColorLayerHiSat89AB) + dot(splat_control2, _GlobalColorLayerHiSat4567);
				half advGlobalColorMapLoBlend = dot(splat_control1, _GlobalColorLoBlend89AB) + dot(splat_control2, _GlobalColorLoBlend4567);
				half advGlobalColorMapHiBlend = dot(splat_control1, _GlobalColorHiBlend89AB) + dot(splat_control2, _GlobalColorHiBlend4567);
			#endif
			
			half colorBlendValEnveloped = saturate( (actH - advGlobalColorMapBottomLevel) / (advGlobalColorMapTopLevel-advGlobalColorMapBottomLevel) );
			half3 globalColorValue = lerp(dot(global_color_value.rgb,0.3333).xxx, global_color_value.rgb, lerp(colorMapLoSat, colorMapHiSat, colorBlendValEnveloped) );
			half3 colValue = lerp(dot(col.rgb,0.3333).xxx, col.rgb, lerp(colLoSat, colHiSat, colorBlendValEnveloped) );
			col = lerp(col, colValue*globalColorValue.rgb*2, global_color_blend*lerp(advGlobalColorMapLoBlend, advGlobalColorMapHiBlend, colorBlendValEnveloped) );
		#else
			// basic global colormap blending
			#ifdef COLOR_MAP_BLEND_MULTIPLY
				col=lerp(col, col*global_color_value.rgb*2, global_color_blend);
			#else
				col=lerp(col, global_color_value.rgb, global_color_blend);
			#endif
		#endif
		#ifdef SIMPLE_FAR
			col=lerp(col, global_color_value.rgb, _uv_Relief_w);
		#endif		
		#if ( defined(RTP_IBL_DIFFUSE) && !defined(UNITY_PASS_SHADOWCASTER) )
			half3 colBrightnessNotAffectedByColormap=col.rgb*colBrightness/max(0.01, dot(col.rgb,float3(1,1,1)));
		#endif
	#else
		#if ( defined(RTP_IBL_DIFFUSE) && !defined(UNITY_PASS_SHADOWCASTER) )
			half3 colBrightnessNotAffectedByColormap=col.rgb;
		#endif	
	#endif
	
		

	
	// przeniesione pod emisję (która zależy od specular materiału _pod_ śniegiem)

	
	o.Albedo=col;
	o.Normal=normalize(o.Normal);
	
	// heightblend AO
	#ifdef RTP_HEIGHTBLEND_AO
		#if defined(_4LAYERS)
			float heightblend_AO=saturate(dot(0.5-abs(lerp(splat_control1_mid, splat_control1_close, RTP_AOsharpness)-0.5), RTP_AO_89AB)*RTP_AOamp);
		#else
			float heightblend_AO=saturate(( dot(0.5-abs(lerp(splat_control1_mid, splat_control1_close, RTP_AOsharpness)-0.5), RTP_AO_89AB) + dot(0.5-abs(lerp(splat_control2_mid, splat_control2_close, RTP_AOsharpness)-0.5), RTP_AO_4567) )*RTP_AOamp);
		#endif	

		//o.RTP.y*=1-heightblend_AO; // mnożone później (po emission glow, bo ten tłumi AO)
	#endif		
	
	// emission of layer (glow)
		
	#ifdef RTP_HEIGHTBLEND_AO
		o.RTP.y*=1-heightblend_AO;
	#endif		
	
	#ifdef _4LAYERS
		float IBL_bump_smoothness=dot(splat_control1, RTP_IBL_bump_smoothness89AB);
		#if ( defined(RTP_IBL_DIFFUSE) && !defined(UNITY_PASS_SHADOWCASTER) )
			float RTP_IBL_DiffStrength=dot(splat_control1, RTP_IBL_DiffuseStrength89AB);
		#endif
		#if ( defined(RTP_IBL_SPEC) && !defined(UNITY_PASS_SHADOWCASTER) ) || ( defined(RTP_REFLECTION) && !defined(UNITY_PASS_SHADOWCASTER) )
			// anti-bleed subtraction
			float RTP_IBL_SpecStrength=dot(splat_control1_nobleed, RTP_IBL_SpecStrength89AB);
		#endif
	#else
		float IBL_bump_smoothness=dot(splat_control1, RTP_IBL_bump_smoothness89AB)+dot(splat_control2, RTP_IBL_bump_smoothness4567);
		#if ( defined(RTP_IBL_DIFFUSE) && !defined(UNITY_PASS_SHADOWCASTER) )
			float RTP_IBL_DiffStrength=dot(splat_control1, RTP_IBL_DiffuseStrength89AB)+dot(splat_control2, RTP_IBL_DiffuseStrength4567);
		#endif
		#if ( defined(RTP_IBL_SPEC) && !defined(UNITY_PASS_SHADOWCASTER) ) || ( defined(RTP_REFLECTION) && !defined(UNITY_PASS_SHADOWCASTER) )
			// anti-bleed subtraction
			float RTP_IBL_SpecStrength=dot(splat_control1_nobleed, RTP_IBL_SpecStrength89AB)+dot(splat_control2_nobleed, RTP_IBL_SpecStrength4567);
//			float RTP_IBL_SpecStrength=dot(splat_control1, RTP_IBL_SpecStrength89AB)+dot(splat_control2, RTP_IBL_SpecStrength4567);
		#endif
	#endif	
	// lerp IBL values with wet / snow
	#if ( defined(RTP_IBL_DIFFUSE) && !defined(UNITY_PASS_SHADOWCASTER) )

	#endif
	#if ( defined(RTP_IBL_SPEC) && !defined(UNITY_PASS_SHADOWCASTER) ) || ( defined(RTP_REFLECTION) && !defined(UNITY_PASS_SHADOWCASTER) )
		#if ( defined(RTP_WETNESS) && !defined(UNITY_PASS_SHADOWCASTER) )
			RTP_IBL_SpecStrength=lerp(RTP_IBL_SpecStrength, TERRAIN_WaterIBL_SpecWetStrength, TERRAIN_LayerWetStrength);
			RTP_IBL_SpecStrength=lerp(RTP_IBL_SpecStrength, TERRAIN_WaterIBL_SpecWaterStrength, p*p);
		#endif
		
	#endif
	
	#if defined(UNITY_PASS_SHADOWCASTER)
		float3 normalW=float3(0,0,1);// dummy value in caster pass (we don't have an access to WorldNormalVector due to lack of INTERNAL data)
	#else
		float3 normalW=WorldNormalVector(IN, o.Normal);
	#endif
	
	#ifdef NO_SPECULARITY
	o.SpecColor=0;
	o.Specular=0;
	#endif	
	
	float3 IBLNormal=lerp(o.Normal, float3(0,0,1), IBL_bump_smoothness);
	#if defined(UNITY_PASS_SHADOWCASTER)
		float3 reflVec=float3(0,0,1);// dummy value in caster pass (we don't have an access to WorldNormalVector due to lack of INTERNAL data)
	#else
		float3 reflVec=WorldReflectionVector(IN, IBLNormal);
	#endif
	{
   		#if ( defined(RTP_SKYSHOP_SYNC) && !defined(UNITY_PASS_SHADOWCASTER) ) && defined(RTP_SKYSHOP_SKY_ROTATION)
			normalW = _SkyMatrix[0].xyz*normalW.x + _SkyMatrix[1].xyz*normalW.y + _SkyMatrix[2].xyz*normalW.z;
		#endif	
	}
	
	// ^4 shaped diffuse fresnel term for soft surface layers (grass)
	float _DiffFresnel=0;
	#ifdef _4LAYERS
		_DiffFresnel=dot(splat_control1, RTP_DiffFresnel89AB);
	#else
		_DiffFresnel=dot(splat_control1, RTP_DiffFresnel89AB)+dot(splat_control2, RTP_DiffFresnel4567);
	#endif
	// diffuse fresnel term for snow

	float diffuseScatteringFactor=1.0 + diffFresnel*_DiffFresnel;
	o.Albedo *= diffuseScatteringFactor;
	#if ( defined(RTP_IBL_DIFFUSE) && !defined(UNITY_PASS_SHADOWCASTER) )
		colBrightnessNotAffectedByColormap *= diffuseScatteringFactor;
	#endif
	
	// spec color from albedo (metal tint)
	#ifdef _4LAYERS
		float Albedo2SpecColor=dot(splat_control1, _LayerAlbedo2SpecColor89AB);
	#else
		float Albedo2SpecColor=dot(splat_control1, _LayerAlbedo2SpecColor89AB) + dot(splat_control2, _LayerAlbedo2SpecColor4567);
	#endif

	// colAlbedo powinno być "normalizowane" (aby nie było za ciemne) jako tinta dla spec color
	float colAlbedoRGBmax=max(max(colAlbedo.r, colAlbedo.g), colAlbedo.b);
	colAlbedoRGBmax=max(colAlbedoRGBmax, 0.01);
	float3 colAlbedoNew=lerp(half3(1,1,1), colAlbedo.rgb/colAlbedoRGBmax.xxx, saturate(colAlbedoRGBmax*4)*Albedo2SpecColor);
	half3 SpecColor=_SpecColor.rgb*o_Gloss*colAlbedoNew*colAlbedoNew; // spec color for IBL/Refl
	o.SpecColor=SpecColor;
		
	#if defined(RTP_AMBIENT_EMISSIVE_MAP)
		float4 eMapVal=tex2Dp(_AmbientEmissiveMapGlobal, IN.uv_Control, _ddxGlobal, _ddyGlobal);
		o.Emission+=o.Albedo*eMapVal.rgb*_AmbientEmissiveMultiplier*lerp(1, saturate(o.Normal.z*o.Normal.z-(1-actH)*(1-o.Normal.z*o.Normal.z)), _AmbientEmissiveRelief);
		float pixel_trees_shadow_val=saturate((_INPUT_distance - _TERRAIN_trees_shadow_values.x) / _TERRAIN_trees_shadow_values.y);
		pixel_trees_shadow_val=lerp(1, eMapVal.a, pixel_trees_shadow_val);
		float o_RTP_y_without_shadowmap_distancefade=o.RTP.y*lerp(eMapVal.a, 1, _TERRAIN_trees_shadow_values.z);
		o.RTP.y*=lerp(_TERRAIN_trees_shadow_values.z, 1, pixel_trees_shadow_val);
	#endif	
	

	#if ( defined(RTP_IBL_DIFFUSE) && !defined(UNITY_PASS_SHADOWCASTER) )
		#if ( defined(RTP_SKYSHOP_SYNC) && !defined(UNITY_PASS_SHADOWCASTER) )
			half3 IBLDiffuseCol = SHLookup(normalW)*RTP_IBL_DiffStrength;
		#else
			#ifdef UNITY_PI
				half3 IBLDiffuseCol = ShadeSH9(float4(normalW,1))*RTP_IBL_DiffStrength;
				//IBLDiffuseCol=RTP_IBL_DiffStrength;
			#else
				half3 IBLDiffuseCol = DecodeRGBM(texCUBElod(_CubemapDiff, float4(normalW,0)))*RTP_IBL_DiffStrength;
			#endif
		#endif
		IBLDiffuseCol*=colBrightnessNotAffectedByColormap * lerp(1, o_RTP_y_without_shadowmap_distancefade, TERRAIN_IBL_DiffAO_Damp);
		#if ( defined(RTP_SKYSHOP_SYNC) && !defined(UNITY_PASS_SHADOWCASTER) )
			IBLDiffuseCol*=_ExposureIBL.x;
		#endif		
		#ifndef RTP_IBL_SPEC
		o.Emission += IBLDiffuseCol.rgb;
		#else
		// dodamy za chwilę poprzez introplację, która zachowa energie
		#endif
	#endif

	// kompresuję odwrotnie mip blur (łatwiej osiągnąć "lustro")
	float o_SpecularInvSquared = (1-o.Specular)*(1-o.Specular);
	
	#if ( defined(RTP_IBL_SPEC) && !defined(UNITY_PASS_SHADOWCASTER) )
		#define RTP_IBLSPEC_BUMPED
		#ifdef RTP_IBLSPEC_BUMPED
			float n_dot_v = saturate(dot(IBLNormal, IN.viewDir));
			float exponential = exp2(SchlickFresnelApproxExp2Const*n_dot_v);
		#else
			float exponential=diffFresnel;
		#endif
		// skyshop fit (I'd like people to get similar results in gamma / linear)
		#if defined(RTP_COLORSPACE_LINEAR)
			exponential=0.03+0.97*exponential;
		#else
			exponential=0.25+0.75*exponential;
		#endif
		float spec_fresnel = lerp (1.0f, exponential, o.RTP.x);
		
		// U5 reflection probe
		#ifdef UNITY_PI
			half4 rgbm = SampleCubeReflection( unity_SpecCube0, reflVec, o_SpecularInvSquared*(6-exponential*o.RTP.x*3) );
			half3 IBLSpecCol = DecodeHDR_NoLinearSupportInSM2 (rgbm, unity_SpecCube0_HDR) * RTP_IBL_SpecStrength;
		#else
	        // U4 reflection probe
   			half3 IBLSpecCol = DecodeRGBM(texCUBElod (_CubemapSpec, float4(reflVec, o_SpecularInvSquared*(6-exponential*o.RTP.x*3))))*RTP_IBL_SpecStrength;
		#endif 			
		
		IBLSpecCol.rgb*=spec_fresnel * SpecColor * lerp(1, o_RTP_y_without_shadowmap_distancefade, TERRAIN_IBLRefl_SpecAO_Damp);
		#if ( defined(RTP_SKYSHOP_SYNC) && !defined(UNITY_PASS_SHADOWCASTER) )
			IBLSpecCol.rgb*=_ExposureIBL.y;
		#endif		
		#if ( defined(RTP_IBL_DIFFUSE) && !defined(UNITY_PASS_SHADOWCASTER) )
			// link difuse and spec IBL together with energy conservation
			o.Emission += saturate(1-IBLSpecCol.rgb) * IBLDiffuseCol + IBLSpecCol.rgb;
		#else
			o.Emission+=IBLSpecCol.rgb;
		#endif
	#endif

		
    #if ( defined(RTP_REFLECTION) && !defined(UNITY_PASS_SHADOWCASTER) ) && !defined(RTP_SHOW_OVERLAPPED)
	#if defined(RTP_SIMPLE_SHADING)	
	
	#ifdef FAR_ONLY
	if (false) {
	#else
	if (_uv_Relief_z>0) {
	#endif

	#endif
		float2 mip_selectorRefl=o_SpecularInvSquared*(8-diffFresnel*o.RTP.x*4);
		#ifdef RTP_ROTATE_REFLECTION
			float3 refl_rot;
			refl_rot.x=sin(_Time.x*TERRAIN_ReflectionRotSpeed);
			refl_rot.y=cos(_Time.x*TERRAIN_ReflectionRotSpeed);
			refl_rot.z=-refl_rot.x;
			float2 tmpRefl;
			tmpRefl.x=dot(reflVec.xz, refl_rot.yz);
			tmpRefl.y=dot(reflVec.xz, refl_rot.xy);
			float t=tex2Dlod(_BumpMapGlobal, float4(tmpRefl*0.5+0.5, mip_selectorRefl)).a;
		#else
			float t=tex2Dlod(_BumpMapGlobal, float4(reflVec.xz*0.5+0.5, mip_selectorRefl)).a;
		#endif	
		#if ( defined(RTP_IBL_SPEC) && !defined(UNITY_PASS_SHADOWCASTER) )
			half rim=spec_fresnel;
		#else
			#define RTP_REFLSPEC_BUMPED
			#ifdef RTP_REFLSPEC_BUMPED
				float n_dot_v = saturate(dot(IBLNormal, IN.viewDir));
				float exponential = exp2(SchlickFresnelApproxExp2Const*n_dot_v);
				half rim = lerp(1, exponential, o.RTP.x);
			#else
				#if defined(RTP_COLORSPACE_LINEAR)
					diffFresnel=0.03+0.97*diffFresnel;
				#else
					diffFresnel=0.25+0.75*diffFresnel;
				#endif
		        half rim= lerp(1, diffFresnel, o.RTP.x);
			#endif
	    #endif
	    float downSideEnvelope=saturate(reflVec.y*3);
	    t *= downSideEnvelope;
	    rim *= downSideEnvelope*0.7+0.3;
		#if defined(RTP_SIMPLE_SHADING)
			rim*=RTP_IBL_SpecStrength*_uv_Relief_z;
		#else
			rim*=RTP_IBL_SpecStrength;
		#endif
		rim-=o_SpecularInvSquared*rim*TERRAIN_ReflGlossAttenuation; // attenuate low gloss
		
		half3 reflCol;
		reflCol=lerp(TERRAIN_ReflColorB.rgb, TERRAIN_ReflColorC.rgb, saturate(TERRAIN_ReflColorCenter-t) / TERRAIN_ReflColorCenter );
		reflCol=lerp(reflCol.rgb, TERRAIN_ReflColorA.rgb, saturate(t-TERRAIN_ReflColorCenter) / (1-TERRAIN_ReflColorCenter) );
		o.Emission += reflCol * SpecColor * lerp(1, o_RTP_y_without_shadowmap_distancefade, TERRAIN_IBLRefl_SpecAO_Damp) * rim * 2;
	#if defined(RTP_SIMPLE_SHADING)	
	}
	#endif
	#endif

	float diff;
	#if !defined(LIGHTMAP_OFF) && defined (DIRLIGHTMAP_OFF) && !defined(LIGHTDIR_UNAVAILABLE) && !defined(RTP_PLANET)
		diff = ((dot (o.Normal, IN.color.xyz))*0.5+0.5);
	
		// w U5 ambient jest robiony inaczej w zal. od trybu lightmappy i ponizszy tweak nie dzialal
		#if !defined(UNITY_PI)
			// mnozenie albedo usuwa oswietlenie ambient - wyrownujemy to ponizej (wsp. dobrane recznie dla forward i deferred)
			#if defined(UNITY_PASS_PREPASSFINAL)
				// musimy polegac na RTP_ambLight przekazanym w RTPFogUpdate.cs bo deferred z lightmapami ma niezdefiniowane UNITY_LIGHTMODEL_AMBIENT
				// *0.5 daje taki sam rezultat w forward i deferred
				o.Emission+=o.Albedo*RTP_ambLight.rgb*(1-diff)*_TERRAIN_LightmapShading*0.5f;//*(1-_uv_Relief_z*0.5);
			#else
				o.Emission+=o.Albedo*UNITY_LIGHTMODEL_AMBIENT.rgb*(1-diff)*_TERRAIN_LightmapShading;
			#endif
		#endif
		diff = diff*_TERRAIN_LightmapShading+(1-_TERRAIN_LightmapShading);
		//o.RTP.y*=diff; // to nie zadziala, bo RTP.y jest uzywane tylko w customowych funkcjach oswietlajacych
		o.Albedo*=diff;
	#endif
	
	
	#if defined(RTP_CROSSPASS_HEIGHTBLEND)
		#if defined(_12LAYERS)
			// we've run out of samplers & heightblend would be very complex
			o.Alpha=total_coverage;
		#else
			splat_control1 = tHA*splat_controlA; // 2 (4-7)

			tHB=saturate(tex2Dd(_TERRAIN_HeightMap, _INPUT_uv.xy, _ddxMain.xy, _ddyMain.xy)+0.001); // 1 (0-3)
			float4 splat_controlB = tex2Dp(_Control1, IN.uv_Control, _ddxGlobal, _ddyGlobal);
			float4 splat_control2 = tHB*splat_controlB;
			
			splat_control1_mid=splat_control1*splat_control1;
			float4 splat_control2_mid=splat_control2*splat_control2;
			
			float norm_sum=dot(splat_control1_mid,1) + dot(splat_control2_mid,1);
			splat_control1_mid/=norm_sum;
			splat_control2_mid/=norm_sum;
			
			splat_control1_close=splat_control1_mid*splat_control1_mid;
			float4 splat_control2_close=splat_control2_mid*splat_control2_mid;
			#ifdef SHARPEN_HEIGHTBLEND_EDGES_PASS1
				splat_control1_close*=splat_control1_close;
				splat_control2_close*=splat_control2_close;
			#endif
			#ifdef SHARPEN_HEIGHTBLEND_EDGES_PASS2
				splat_control1_close*=splat_control1_close;
				splat_control2_close*=splat_control2_close;
			#endif
			norm_sum=dot(splat_control1_close,1) + dot(splat_control2_close,1);
			splat_control1_close/=norm_sum;
			splat_control2_close/=norm_sum;
			splat_control1=lerp(splat_control1_mid, splat_control1_close, _uv_Relief_z);
			splat_control2=lerp(splat_control2_mid, splat_control2_close, _uv_Relief_z);			
			
			splat_control1_close=splat_control1_mid*splat_control1_mid;
			splat_control1=lerp(lerp(splat_control1_mid, splat_control1, _uv_Relief_w), splat_control1_close, _uv_Relief_z);
			splat_control2_close=splat_control2_mid*splat_control2_mid;
			splat_control2=lerp(lerp(splat_control2_mid, splat_control2, _uv_Relief_w), splat_control2_close, _uv_Relief_z);
			
			float normalize_sum=dot(splat_control1, 1)+dot(splat_control2, 1);
			splat_control1 /= normalize_sum;
			splat_control2 /= normalize_sum;
			o.Alpha=dot(splat_control1,1);
			
			#ifdef RTP_HEIGHTBLEND_AO
			// 0-3			
			float hbAOCrossPass=(dot(splat_control1,RTP_AO_0123)+dot(splat_control2,RTP_AO_89AB))*RTP_AOamp*_uv_Relief_z*2;
			heightblend_AO=1-saturate(1-dot(saturate(abs(splat_control2-0.5)*(2+RTP_AOsharpness)),0.25))*hbAOCrossPass;
			o.RTP.y*=heightblend_AO;
			
			// 4-7
//			heightblend_AO=1-saturate(1-dot(saturate(abs(splat_control1-0.5)*(2+RTP_AOsharpness)),0.25))*hbAOCrossPass;
//			o.RTP.y*=heightblend_AO;
						
//			heightblend_AO=1-saturate(1-saturate(abs(o.Alpha-0.5)*(2+RTP_AOsharpness)))*RTP_AOamp*_uv_Relief_z;
//			o.RTP.y*=heightblend_AO;
			#endif
		#endif	
	#else
		o.Alpha=total_coverage;
	#endif			
	
	// EOF regular mode
	#endif
	
	// przeniesione, norm_edge zamienione na o.Normal w obl. reflection
	
	/*
	#if ( !defined(UNITY_PASS_PREPASSBASE) && !defined(UNITY_PASS_PREPASSFINAL) ) || (!defined(RTP_DEFERRED_PBL_NORMALISATION))
		//o.Specular=1-o_SpecularInvSquared;
	#endif
	#if defined(UNITY_PASS_PREPASSBASE)	
		 #if defined(RTP_DEFERRED_PBL_NORMALISATION) && defined(RTP_COLORSPACE_LINEAR)
			o.Specular*=o.Specular;
		#endif
	#endif
	
	#if defined(UNITY_PASS_PREPASSBASE)	 || defined(UNITY_PASS_PREPASSFINAL)
		o.Specular=lerp(RTP_DeferredAddPassSpec, o.Specular, total_coverage);
		#if defined(COLOR_EARLY_EXIT)
		// chcemy mieć przejście do docelowego Speculara osiągnięte wolniej
		float cBiased=(1-IN.color.a);
		cBiased*=cBiased;
		cBiased*=cBiased;
		o.Specular=lerp(o.Specular, RTP_DeferredAddPassSpec, cBiased);
		#endif
		o.Specular=max(0.01, o.Specular);
	#endif
	
	#if defined(UNITY_PASS_PREPASSFINAL)	
		#if defined(RTP_DEFERRED_PBL_NORMALISATION)
			o.Specular=1-o.Specular;
			o.Specular*=o.Specular;
			o.Specular=1-o.Specular;
			// hacking spec normalisation to get quiet a dark spec for max roughness (will be 0.25/16)
			float specular_power=exp2(10*o.Specular+1) - 1.75;
			float normalisation_term = specular_power / (8.0f*60); // 60 - handpicked value - wizualnie daje najlepszy fit
			o.SpecColor*=normalisation_term;
		#endif
		
	#endif
	#if defined(UNITY_PASS_PREPASSFINAL)
		o.SpecColor*=dot(splat_control1, _DeferredSpecDampAddPass89AB);
	#endif	
	*/
	

	o.Specular = 0;

	// HACK-ish workaround - Unity skip's passing color if IN.color is not explicitly used here in shader
	o.Albedo+=IN.color.xyz*0.0001;		
} 	