using UnityEngine;
using UnityEditor;
using System;
using System.Reflection;
using System.Collections;
using UnityEditor.Callbacks;

#if UNITY_EDITOR
using System.IO;
#endif



[CustomEditor (typeof(ReliefTerrain))]
public class ReliefTerrainEditor : Editor
{
	#if UNITY_EDITOR
	int progress_count_max;
	int progress_count_current;
	const int progress_granulation = 1;
	string progress_description = "";
	
	private bool dirtyFlag;
	private double changedTime = 0;
	public static Texture2D blank_white_tex;
	public static Texture2D blank_black_tex;

	int p1, p2;
	private bool isMeshMain = true;
	private bool isMatMain = true;
	private bool isMeshAdd = true;
	private bool isMatAdd = true;



	private static bool __editMode = false;
	private bool isEditing = false;
	private Vector3 brushPosition;    
	private bool started = false;

	
	private Texture icoPaintOn;
	private Texture icoPaintOff;
	private Texture icoMicroArrow;
	private Texture icoLayers;
	private Texture icoCombinedTexutres;
	private Texture icoSettings;



	private Texture icoPOM;
	private Texture icoLayersSmall;
	private Texture icoUVBlend;
	private Texture icoPerlinNormal;
	private Texture icoGlobalcolor;
	private Texture icoSuperdetail;
	private Texture icoWater;
	private Texture icoSnow;
	private Texture icoVerticalTexture;
	private Texture icoReflection;



	//public Presets[] presets;

	void OnEnable ()
	{
		
		ReliefTerrain _target = (ReliefTerrain)target;


		if (icoPaintOn == null)
			icoPaintOn = AssetDatabase.LoadAssetAtPath ("Assets/ReliefPack/Editor/ReliefTerrain/icons/icoPaintOn.png", typeof(Texture)) as Texture;
		if (icoPaintOff == null)
			icoPaintOff = AssetDatabase.LoadAssetAtPath ("Assets/ReliefPack/Editor/ReliefTerrain/icons/icoPaintOff.png", typeof(Texture)) as Texture;
		if (icoMicroArrow == null)
			icoMicroArrow = AssetDatabase.LoadAssetAtPath ("Assets/ReliefPack/Editor/ReliefTerrain/icons/icoMicroArrow.png", typeof(Texture)) as Texture;
		if (icoLayers == null)
			icoLayers = AssetDatabase.LoadAssetAtPath ("Assets/ReliefPack/Editor/ReliefTerrain/icons/icoLayers.png", typeof(Texture)) as Texture;
		if (icoCombinedTexutres == null)
			icoCombinedTexutres = AssetDatabase.LoadAssetAtPath ("Assets/ReliefPack/Editor/ReliefTerrain/icons/icoCombinedTextures.png", typeof(Texture)) as Texture;
		if (icoSettings == null)
			icoSettings = AssetDatabase.LoadAssetAtPath ("Assets/ReliefPack/Editor/ReliefTerrain/icons/icoSettings.png", typeof(Texture)) as Texture;

		if (icoPOM == null)
			icoPOM = AssetDatabase.LoadAssetAtPath ("Assets/ReliefPack/Editor/ReliefTerrain/icons/icoPOM.png", typeof(Texture)) as Texture;
		if (icoLayersSmall == null)
			icoLayersSmall = AssetDatabase.LoadAssetAtPath ("Assets/ReliefPack/Editor/ReliefTerrain/icons/icoLayersSmall.png", typeof(Texture)) as Texture;
		if (icoUVBlend == null)
			icoUVBlend = AssetDatabase.LoadAssetAtPath ("Assets/ReliefPack/Editor/ReliefTerrain/icons/icoUVBlend.png", typeof(Texture)) as Texture;
		if (icoGlobalcolor == null)
			icoGlobalcolor = AssetDatabase.LoadAssetAtPath ("Assets/ReliefPack/Editor/ReliefTerrain/icons/icoGlobalcolor.png", typeof(Texture)) as Texture;
		if (icoPerlinNormal == null)
			icoPerlinNormal = AssetDatabase.LoadAssetAtPath ("Assets/ReliefPack/Editor/ReliefTerrain/icons/icoPerlinNormal.png", typeof(Texture)) as Texture;
		if (icoSuperdetail == null)
			icoSuperdetail = AssetDatabase.LoadAssetAtPath ("Assets/ReliefPack/Editor/ReliefTerrain/icons/icoSuperdetail.png", typeof(Texture)) as Texture;
		if (icoWater == null)
			icoWater = AssetDatabase.LoadAssetAtPath ("Assets/ReliefPack/Editor/ReliefTerrain/icons/icoWater.png", typeof(Texture)) as Texture;
		if (icoSnow == null)
			icoSnow = AssetDatabase.LoadAssetAtPath ("Assets/ReliefPack/Editor/ReliefTerrain/icons/icoSnow.png", typeof(Texture)) as Texture;
		if (icoVerticalTexture == null)
			icoVerticalTexture = AssetDatabase.LoadAssetAtPath ("Assets/ReliefPack/Editor/ReliefTerrain/icons/icoVerticalTexture.png", typeof(Texture)) as Texture;
		if (icoReflection == null)
			icoReflection = AssetDatabase.LoadAssetAtPath ("Assets/ReliefPack/Editor/ReliefTerrain/icons/icoReflection.png", typeof(Texture)) as Texture;

		_target.Initialize ();

		_target.RefreshTextures (null, true);	 // check for weak references (global color and special combined not saved to disk));
		if (_target.globalSettingsHolder.BumpGlobal && _target.BumpGlobalCombined) {
			TextureImporter _importer = (TextureImporter)AssetImporter.GetAtPath (AssetDatabase.GetAssetPath (_target.globalSettingsHolder.BumpGlobal));
			if (_importer) {
				if (_importer.maxTextureSize != _target.BumpGlobalCombined.width) {
					Debug.LogWarning ("Due to rescaling special combined texture superdetail textures, water mask and reflection map have been cleared (you have to reassign them again).");
	
				}
			}
		}		

		// check for interfering replacement shaders
		if (_target.globalSettingsHolder != null && !_target.globalSettingsHolder.dont_check_for_interfering_terrain_replacement_shaders && !_target.globalSettingsHolder.useTerrainMaterial) {
	#if UNITY_5
			Shader first_shad = Shader.Find ("Nature/Terrain/Diffuse");
			Shader add_shad = Shader.Find ("Hidden/TerrainEngine/Splatmap/Diffuse-AddPass");
	#else
	Shader first_shad=Shader.Find ("Hidden/TerrainEngine/Splatmap/Lightmap-FirstPass");
	Shader add_shad=Shader.Find ("Hidden/TerrainEngine/Splatmap/Lightmap-AddPass");
	#endif
			string repl_firstPass = AssetDatabase.GetAssetPath (first_shad);
			string repl_addPass = AssetDatabase.GetAssetPath (add_shad);
			int res = -1;
			if (repl_firstPass != "Resources/unity_builtin_extra" && repl_firstPass != "" && repl_firstPass.IndexOf ("ReliefTerrain") == -1) {
				res = EditorUtility.DisplayDialogComplex ("RTP Warning", "Interfering terrain replacement shader found in your project ! Unless you're using materials on RTP terrain you should REMOVE another shaders.", "Show me", "Cancel", "Dont' bother me again");
				if (res == 0) {
					Selection.activeObject = first_shad;
				} else if (res == 2) {
					_target.globalSettingsHolder.dont_check_for_interfering_terrain_replacement_shaders = true;
				}
			}
			if (res == -1 && repl_addPass != "" && repl_addPass != "Resources/unity_builtin_extra" && repl_addPass.IndexOf ("ReliefTerrain") == -1) {
				res = EditorUtility.DisplayDialogComplex ("RTP Warning", "Interfering terrain replacement shader found in your project ! Unless you're using materials on RTP terrain you should REMOVE another shaders.", "Show me", "Cancel", "Dont' bother me again");
				if (res == 0) {
					Selection.activeObject = add_shad;
				} else if (res == 2) {
					_target.globalSettingsHolder.dont_check_for_interfering_terrain_replacement_shaders = true;
				}
			}
		}
		if (_target.globalSettingsHolder != null) {
			_target.globalSettingsHolder.SyncGlobalPropsAcrossTerrainGroups ();
		}

		_target.prev_tool = UnityEditor.Tools.current;
	}


	void OnDisable ()
	{
		ReliefTerrain _target = (ReliefTerrain)target;
		if (_target) {
			_target.RefreshTextures ();
			if (_target.globalSettingsHolder.paint_flag) {
				_target.globalSettingsHolder.paint_flag = false;
				SceneView.onSceneGUIDelegate -= ReliefTerrain._SceneGUI;
			}
			;
			if (_target.globalSettingsHolder != null) {
				_target.globalSettingsHolder.SyncGlobalPropsAcrossTerrainGroups ();
			}
			EditorUtility.SetDirty (target);
		}
		UnityEditor.Tools.current = _target.prev_tool;
	}

	public override void OnInspectorGUI ()
	{
		ReliefTerrain _targetRT = (ReliefTerrain)target;
		ReliefTerrainGlobalSettingsHolder _target = _targetRT.globalSettingsHolder;
		if (_target == null) {
			_targetRT.RefreshTextures ();
			_target = _targetRT.globalSettingsHolder;
		}
		dirtyFlag = false;
		
		RTP_LODmanager _RTP_LODmanagerScript = _target.Get_RTP_LODmanagerScript ();
		if (_RTP_LODmanagerScript != null) {
			if (_RTP_LODmanagerScript.gameObject.GetComponent (typeof(RTPFogUpdate)) == null) {
				_RTP_LODmanagerScript.gameObject.AddComponent (typeof(RTPFogUpdate));
			}
		}

		Terrain terrainComp = (Terrain)_targetRT.GetComponent (typeof(Terrain));
		if (terrainComp) {
			if (_target.numLayers != terrainComp.terrainData.splatPrototypes.Length) {
				// a layer removed or added
				_target.ReInit (terrainComp);
				ReliefTerrain[] script_objs = (ReliefTerrain[])GameObject.FindObjectsOfType (typeof(ReliefTerrain));
				for (int s = 0; s < script_objs.Length; s++) {
					script_objs [s].splat_layer_seq = new int[12] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
				}				
			}
			if (!_target.useTerrainMaterial && _target.numTiles > 1) {
				// U3 - multiple terrains or U4 w/o material - multiple terrains
				if (_target.numLayers != 4 && _target.numLayers != 8) {
					EditorGUILayout.HelpBox ("RTP on multiple terrains w/o materials (available in U4) works only if we have 4 or 8 layers only !", MessageType.Error, true);
					return;
				}
			}
		}
		if (_target.splats == null || _target.numLayers != _target.splats.Length) {
			EditorGUILayout.HelpBox ("Problem with number of layers, try to reassign ReliefTerrain component script on this terrain...", MessageType.Error, true);
			return;
		}
		int sw = 512;
		for (int i = 0; i < _target.numLayers; i++) {
			if (_target.splats [i]) {
				sw = _target.splats [i].width;
				break;
			}
		}
		for (int i = 0; i < _target.numLayers; i++) {
			if (_target.splats [i] == null) {
				//Debug.LogError ("Splat (detail) texture " + i + " was empty ! Assigning null texture");
				_target.splats [i] = new Texture2D (sw, sw, TextureFormat.ARGB32, true, QualitySettings.activeColorSpace == ColorSpace.Linear);
			}
		}		
		
		// >RTP3.1		
		// update-set to default
		if (_target.gloss_baked == null || _target.gloss_baked.Length == 0) {
			_target.gloss_baked = new RTPGlossBaked[12];
		}
		
		if (_target.CheckAndUpdate (ref _target.RTP_gloss2mask, 0.5f, _target.numLayers)) {
			for (int k = 0; k < _target.numLayers; k++) {
				_target.Spec [k] = 1; // zresetuj od razu mnożnik glossa (RTP3.1 - zmienna ma inne znaczenie)
			}
		}
		_target.CheckAndUpdate (ref _target.RTP_gloss_mult, 1f, _target.numLayers);
		_target.CheckAndUpdate (ref _target.RTP_gloss_shaping, 0.5f, _target.numLayers);
	
		_target.CheckAndUpdate (ref _target.LayerBrightness, 1.0f, _target.numLayers);
		_target.CheckAndUpdate (ref _target.LayerBrightness2Spec, 0.0f, _target.numLayers);
		_target.CheckAndUpdate (ref _target.LayerAlbedo2SpecColor, 0.0f, _target.numLayers);
		_target.CheckAndUpdate (ref _target.LayerSaturation, 1.0f, _target.numLayers);

		
		// update (RTP3.1)
		if (_target.splat_atlases.Length == 2) {
			Texture2D _atA = _target.splat_atlases [0];
			Texture2D _atB = _target.splat_atlases [1];
			_target.splat_atlases = new Texture2D[3];
			_target.splat_atlases [0] = _atA;
			_target.splat_atlases [1] = _atB;
		}
		
		bool atlasA_needed = false;
		bool atlasB_needed = false;
		bool atlasC_needed = false;
		
		bool _4LAYERS_SHADER_USED = false;
		if (_RTP_LODmanagerScript) {
			_target._4LAYERS_SHADER_USED = _4LAYERS_SHADER_USED = _RTP_LODmanagerScript.RTP_4LAYERS_MODE;
		}
		if (_RTP_LODmanagerScript)
			_RTP_LODmanagerScript.numLayers = _target.numLayers;
		
		if (_RTP_LODmanagerScript) {
			//if (_target.numLayers <= 4 && !_RTP_LODmanagerScript.RTP_4LAYERS_MODE) {
			//	EditorGUILayout.HelpBox ("Shaders are set to 8 layers mode while number of layers is less than 5. Add layers or recompile shaders in RTP_LODmanager to not use 8 layers in first pass.", MessageType.Error, true);
			//}
			atlasA_needed = !_4LAYERS_SHADER_USED || _RTP_LODmanagerScript.RTP_USE_COLOR_ATLAS_FIRST;
			atlasB_needed = (!_4LAYERS_SHADER_USED || _RTP_LODmanagerScript.RTP_USE_COLOR_ATLAS_ADD) && (_target.numLayers > 4);
			atlasC_needed = (!_4LAYERS_SHADER_USED && _RTP_LODmanagerScript.RTP_USE_COLOR_ATLAS_ADD) && (_target.numLayers > 8);
			
			bool atlas_made = false;
			if (atlasA_needed) {
				if (_target.splat_atlases [0] == null) {
					_targetRT.PrepareAtlases (0);
					_target.atlas_glossBaked [0] = null;
					for (int k = 0; k < ((_target.numLayers < 4) ? _target.numLayers : 4); k++) {
						_target.gloss_baked [k] = ScriptableObject.CreateInstance (typeof(RTPGlossBaked)) as RTPGlossBaked;
						_target.splats_glossBaked [k] = null;
					}
					atlas_made = true;
				}
			} else {
				_target.splat_atlases [0] = null;
				_target.atlas_glossBaked [0] = null;
			}
			if (atlasB_needed) {
				if (_target.splat_atlases [1] == null) {
					_targetRT.PrepareAtlases (1);
					_target.atlas_glossBaked [1] = null;
					for (int k = 4; k < ((_target.numLayers < 8) ? _target.numLayers : 8); k++) {
						_target.gloss_baked [k] = ScriptableObject.CreateInstance (typeof(RTPGlossBaked)) as RTPGlossBaked;
						_target.splats_glossBaked [k] = null;
					}
					atlas_made = true;
				}
			} else {
				_target.splat_atlases [1] = null;
				_target.atlas_glossBaked [1] = null;
			}
			if (atlasC_needed) {
				if (_target.splat_atlases [2] == null) {
					_targetRT.PrepareAtlases (2);
					_target.atlas_glossBaked [2] = null;
					for (int k = 12; k < ((_target.numLayers < 12) ? _target.numLayers : 12); k++) {
						_target.gloss_baked [k] = ScriptableObject.CreateInstance (typeof(RTPGlossBaked)) as RTPGlossBaked;
						_target.splats_glossBaked [k] = null;
					}
					atlas_made = true;
				}
			} else {
				_target.splat_atlases [2] = null;
				_target.atlas_glossBaked [2] = null;
			}
			if (atlas_made) {
				_target.Refresh (null, _targetRT);
			}
		}
		
		bool ADV_COLOR_MAP_BLENDING_ENABLED_FIRST = _RTP_LODmanagerScript && _RTP_LODmanagerScript.ADV_COLOR_MAP_BLENDING_FIRST;
		bool ADV_COLOR_MAP_BLENDING_ENABLED_ADD = _RTP_LODmanagerScript && _RTP_LODmanagerScript.ADV_COLOR_MAP_BLENDING_ADD;
		
		bool UV_BLEND_ENABLED_FIRST = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_UV_BLEND_FIRST;
		bool UV_BLEND_ENABLED_ADD = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_UV_BLEND_ADD;
		int[] UV_BLEND_ROUTE_NUM_FIRST;
		int[] UV_BLEND_ROUTE_NUM_ADD;
		if (_RTP_LODmanagerScript) {
			UV_BLEND_ROUTE_NUM_FIRST = _RTP_LODmanagerScript.UV_BLEND_ROUTE_NUM_FIRST;
			UV_BLEND_ROUTE_NUM_ADD = _RTP_LODmanagerScript.UV_BLEND_ROUTE_NUM_ADD;
		} else {
			UV_BLEND_ROUTE_NUM_FIRST = new int[8]{ 0, 1, 2, 3, 4, 5, 6, 7 };
			UV_BLEND_ROUTE_NUM_ADD = new int[8]{ 0, 1, 2, 3, 4, 5, 6, 7 };
		}
		
		bool RTP_INDEPENDENT_TILING = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_INDEPENDENT_TILING;
		
		bool RTP_CUT_HOLES = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_CUT_HOLES;
		bool RTP_PBL_FRESNEL = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_PBL_FRESNEL;
		
		bool RTP_USE_EXTRUDE_REDUCTION_FIRST = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_USE_EXTRUDE_REDUCTION_FIRST;
		bool RTP_USE_EXTRUDE_REDUCTION_ADD = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_USE_EXTRUDE_REDUCTION_ADD;
		
		bool RTP_VERTICAL_TEXTURE_FIRST = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_VERTICAL_TEXTURE_FIRST;
		bool RTP_VERTICAL_TEXTURE_ADD = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_VERTICAL_TEXTURE_ADD;
		
		bool RTP_NORMALGLOBAL = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_NORMALGLOBAL;
		bool RTP_TREESGLOBAL = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_TREESGLOBAL;
		bool RTP_AMBIENT_EMISSIVE_MAP = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_AMBIENT_EMISSIVE_MAP;

		bool RTP_TESSELLATION = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_TESSELLATION;
		bool RTP_TESSELLATION_SAMPLE_TEXTURE = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_TESSELLATION_SAMPLE_TEXTURE;
		bool RTP_HEIGHTMAP_SAMPLE_BICUBIC = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_HEIGHTMAP_SAMPLE_BICUBIC;

		bool RTP_SUPER_SIMPLE = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_SUPER_SIMPLE;
		bool RTP_SS_GRAYSCALE_DETAIL_COLORS_FIRST = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_SUPER_SIMPLE && _RTP_LODmanagerScript.RTP_SS_GRAYSCALE_DETAIL_COLORS_FIRST;
		bool RTP_SS_GRAYSCALE_DETAIL_COLORS_ADD = _target.numLayers > 4 && _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_SUPER_SIMPLE && _RTP_LODmanagerScript.RTP_SS_GRAYSCALE_DETAIL_COLORS_ADD;
		bool RTP_USE_PERLIN_FIRST = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_USE_PERLIN_FIRST;
		bool RTP_USE_PERLIN_ADD = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_USE_PERLIN_ADD;
		
		bool SUPER_DETAIL_ENABLED_FIRST = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_SUPER_DETAIL_FIRST;
		bool SUPER_DETAIL_ENABLED_ADD = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_SUPER_DETAIL_ADD;
		bool SUPER_DETAIL_MULTS_ENABLED_FIRST = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_SUPER_DETAIL_MULTS_FIRST;
		bool SUPER_DETAIL_MULTS_ENABLED_ADD = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_SUPER_DETAIL_MULTS_ADD;
		
		bool SNOW_ENABLED_FIRST = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_SNOW_FIRST;
		bool SNOW_ENABLED_ADD = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_SNOW_ADD;
		
		bool REFLECTION_ENABLED_FIRST = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_REFLECTION_FIRST;
		bool REFLECTION_ENABLED_ADD = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_REFLECTION_ADD;
		bool REFLECTION_ROTATION_ENABLED = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_ROTATE_REFLECTION;
		
		bool RTP_HEIGHTBLEND_AO_FIRST = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_HEIGHTBLEND_AO_FIRST;
		bool RTP_HEIGHTBLEND_AO_ADD = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_HEIGHTBLEND_AO_ADD;
		
		bool RTP_EMISSION_FIRST = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_EMISSION_FIRST;
		bool RTP_EMISSION_ADD = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_EMISSION_ADD;

		bool RTP_HOTAIR_EMISSION_FIRST = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_HOTAIR_EMISSION_FIRST;
		bool RTP_HOTAIR_EMISSION_ADD = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_HOTAIR_EMISSION_ADD;
		
		bool RTP_COMPLEMENTARY_LIGHTS = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_COMPLEMENTARY_LIGHTS;
		bool RTP_SPEC_COMPLEMENTARY_LIGHTS = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_SPEC_COMPLEMENTARY_LIGHTS;
		
		bool RTP_SKYSHOP_SYNC = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_SKYSHOP_SYNC;
		bool RTP_IBL_DIFFUSE_FIRST = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_IBL_DIFFUSE_FIRST;
		bool RTP_IBL_DIFFUSE_ADD = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_IBL_DIFFUSE_ADD;
		bool RTP_IBL_SPEC_FIRST = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_IBL_SPEC_FIRST;
		bool RTP_IBL_SPEC_ADD = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_IBL_SPEC_ADD;
		
		bool WATER_ENABLED_FIRST = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_WETNESS_FIRST;
		bool WATER_ENABLED_ADD = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_WETNESS_ADD;
		bool RIPPLEMAP_ENABLED_FIRST = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_WET_RIPPLE_TEXTURE_FIRST;
		bool RIPPLEMAP_ENABLED_ADD = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_WET_RIPPLE_TEXTURE_ADD;
		
		bool CAUSTICS_ENABLED_FIRST = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_CAUSTICS_FIRST;
		bool CAUSTICS_ENABLED_ADD = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_CAUSTICS_ADD;
		
		bool RTP_VERTALPHA_CAUSTICS = _RTP_LODmanagerScript && _RTP_LODmanagerScript.RTP_VERTALPHA_CAUSTICS && (RTP_VERTICAL_TEXTURE_FIRST || RTP_VERTICAL_TEXTURE_ADD);
		
		string[] toolbarStrings;
		GUIContent[] toolbarIcons = new GUIContent[3] {
			new GUIContent ("Main", icoSettings),
			new GUIContent ("Layers", icoLayers),
			new GUIContent ("Combined\ntexures", icoCombinedTexutres)
		};
		GUILayout.Space (10);			
		EditorGUILayout.BeginHorizontal ();
		GUILayout.FlexibleSpace ();
		_target.submenu = (ReliefTerrainMenuItems)(GUILayout.Toolbar ((int)_target.submenu, toolbarIcons, EditorStyles.miniButton, GUILayout.MaxWidth (370)));
		GUILayout.FlexibleSpace ();
		EditorGUILayout.EndHorizontal ();
		GUILayout.Space (10);			
		
		_target.show_details = _target.submenu == ReliefTerrainMenuItems.Details;
		//_target.show_controlmaps = _target.submenu==ReliefTerrainMenuItems.Control;
		_target.show_derivedmaps = _target.submenu == ReliefTerrainMenuItems.CombinedTextures;
		_target.show_settings = _target.submenu == ReliefTerrainMenuItems.GeneralSettings;
		
		if (_target.show_active_layer >= _target.numLayers) {
			_target.show_active_layer = _target.numLayers - 1;
		}			
		//Debug.Log (_target.show_active_layer+","+_target.numLayers);
		
		if (_target.show_details) {
			#region Detail maps unfold
			
			if (_target.numLayers > 0) {
				#region Detail maps unfold	- layers
				
				if (!terrainComp) {
					EditorGUILayout.BeginVertical ("Box");
					EditorGUILayout.BeginHorizontal ();
					EditorGUI.BeginDisabledGroup (_target.numLayers == 8);
					if (GUILayout.Button ("Add layer")) {
						_target.numLayers++;
						int j = _target.numLayers - 1;
						
						ReliefTerrain[] script_objs = (ReliefTerrain[])GameObject.FindObjectsOfType (typeof(ReliefTerrain));
						for (int s = 0; s < script_objs.Length; s++) {
							script_objs [s].splat_layer_seq [j] = j;
							script_objs [s].splat_layer_boost [j] = 1;
							script_objs [s].splat_layer_calc [j] = true;
							script_objs [s].splat_layer_masked [j] = false;
							script_objs [s].source_controls_invert [j] = false;
							script_objs [s].source_controls_mask_invert [j] = false;
						}
						
						_target.Bumps [j] = null;
						_target.Heights [j] = null;
						//#if !UNITY_WEBGL || UNITY_EDITOR
						//_target.Substances[j]=null;						
						//#endif						
						//_target.ReturnToDefaults("layer", j);
						
						Texture2D[] splats_new = new Texture2D[_target.numLayers];
						for (int i = 0; i < _target.splats.Length; i++)
							splats_new [i] = _target.splats [i];
						_target.splats = splats_new;
						_target.splats [_target.numLayers - 1] = _target.splats [((_target.numLayers - 2) >= 0) ? (_target.numLayers - 2) : 0];
					}
					EditorGUI.EndDisabledGroup ();
					EditorGUI.BeginDisabledGroup (_target.numLayers == 1);
					if (GUILayout.Button ("Remove layer")) {
						_target.numLayers--;
						Texture2D[] splats_new = new Texture2D[_target.numLayers];
						for (int i = 0; i < splats_new.Length; i++)
							splats_new [i] = _target.splats [i];
						_target.splats = splats_new;
						ReliefTerrain[] script_objs = (ReliefTerrain[])GameObject.FindObjectsOfType (typeof(ReliefTerrain));
						for (int s = 0; s < script_objs.Length; s++) {
							script_objs [s].splat_layer_seq = new int[12] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
						}
						if (_target.show_active_layer >= _target.numLayers) {
							_target.show_active_layer = _target.numLayers - 1;
						}								
					}
					EditorGUI.EndDisabledGroup ();
					EditorGUILayout.EndHorizontal ();
					EditorGUILayout.EndVertical ();
				}
				
				#if !UNITY_WEBGL || UNITY_EDITOR
				bool handle_substances = false;
				#if UNITY_4_1 || UNITY_4_2 || UNITY_4_3  || UNITY_4_4 || UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9 || UNITY_5_0 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6
				handle_substances = true;
				#else
				if (PlayerSettings.advancedLicense) {
					handle_substances=true;
				}
				#endif
				#endif

				EditorGUILayout.BeginVertical ("Box");
				Color skin_color = GUI.color;
				GUI.color = new Color (1, 1, 0.5f, 1);
				EditorGUILayout.LabelField ("Choose layer", EditorStyles.boldLabel);
				GUI.color = skin_color;
								
				GUISkin gs = EditorGUIUtility.GetBuiltinSkin (EditorSkin.Inspector);
				RectOffset ro1 = gs.label.padding;
				RectOffset ro2 = gs.label.margin;
				gs.label.padding = new  RectOffset (0, 0, 0, 0);
				gs.label.margin = new  RectOffset (3, 3, 3, 3);
				int thumb_size = 50;
				int per_row = Mathf.Max (4, (Screen.width) / thumb_size - 1);
				thumb_size = (Screen.width - 50 - 2 * per_row) / per_row;
				Color ccol = GUI.contentColor;
				for (int n = 0; n < _target.numLayers; n++) {
					if ((n % per_row) == 0)
						EditorGUILayout.BeginHorizontal ();
					Color bcol = GUI.backgroundColor;
					if (n == _target.show_active_layer) {
						GUI.contentColor = new Color (1, 1, 1, 1);
						GUI.backgroundColor = new Color (1, 1, 0, 1);
						EditorGUILayout.BeginHorizontal ("Box");
						if (_target.splats [n]) {
							#if !UNITY_3_5
							GUILayout.Label ((Texture2D)AssetPreview.GetAssetPreview (_target.splats [n]), GUILayout.Width (thumb_size - 8), GUILayout.Height (thumb_size - 8));
							#else
							GUILayout.Label((Texture2D)EditorUtility.GetAssetPreview(_target.splats[n]), GUILayout.Width(thumb_size-8), GUILayout.Height(thumb_size-8));
							#endif
						} else {
							GUILayout.Label (" ", GUILayout.Width (thumb_size - 8), GUILayout.Height (thumb_size - 8));
						}
					} else {
						GUI.contentColor = new Color (1, 1, 1, 0.5f);
						if (_target.splats [n]) {
							#if !UNITY_3_5
							if (GUILayout.Button ((Texture2D)AssetPreview.GetAssetPreview (_target.splats [n]), "Label", GUILayout.Width (thumb_size), GUILayout.Height (thumb_size))) {
								#else
								if (GUILayout.Button((Texture2D)EditorUtility.GetAssetPreview(_target.splats[n]), "Label", GUILayout.Width(thumb_size), GUILayout.Height(thumb_size))) {
								#endif
								_target.show_active_layer = n;
							}
						} else {
							if (GUILayout.Button (" ", "Label", GUILayout.Width (thumb_size), GUILayout.Height (thumb_size))) {
								_target.show_active_layer = n;
							}
						}
					}
					if (n == _target.show_active_layer) {
						EditorGUILayout.EndHorizontal ();
						GUI.backgroundColor = bcol;
					}
					if ((n % per_row) == (per_row - 1) || n == _target.numLayers - 1)
						EditorGUILayout.EndHorizontal ();
				}
				GUI.contentColor = ccol;
				gs.label.padding = ro1;
				gs.label.margin = ro2;					
				EditorGUILayout.EndVertical ();
					
				EditorGUILayout.Space ();
					
				EditorGUILayout.BeginVertical ("Box");
				skin_color = GUI.color;
				GUI.color = new Color (1, 1, 0.5f, 1);
				EditorGUILayout.LabelField ("Layer properties", EditorStyles.boldLabel);
				GUI.color = skin_color;
				EditorGUILayout.Space ();
				{
					int thW = (Screen.width - 60) / 3;
					EditorGUILayout.BeginHorizontal ();
					EditorGUILayout.LabelField ("Detail", EditorStyles.boldLabel, GUILayout.Width (thW));
					EditorGUILayout.LabelField ("Normal", EditorStyles.boldLabel, GUILayout.Width (thW));
					EditorGUILayout.LabelField ("Height (A)", EditorStyles.boldLabel, GUILayout.Width (thW));
					EditorGUILayout.EndHorizontal ();
				}
					
				{
					int n = _target.show_active_layer;
					EditorGUILayout.BeginHorizontal ();
						
					int thW = (Screen.width - 60) / 3;
					//Debug.Log (_target.splats.Length+"  "+_target.numLayers);
					Texture2D tex = _target.splats [n];
					Texture2D ntex = tex;
					checkChange (ref ntex, (Texture2D)EditorGUILayout.ObjectField (_target.splats [n], typeof(Texture2D), false, GUILayout.Width (thW), GUILayout.Height (thW)));
					if (ntex != tex && ntex) {
						_target.gloss_baked [n] = ScriptableObject.CreateInstance (typeof(RTPGlossBaked)) as RTPGlossBaked; // nie mamy inf. o zmodyfikowanym roughness dla tej tekstury
						_target.splats_glossBaked [n] = null; // nie mamy też tekstury ze zmodyfikowanymi MIP levelami (będa używane splats[i])
						_target.atlas_glossBaked [n / 4] = null; // używany atlas jest nieważny
						_target.splats [n] = ntex;
						int prep_atlas = -1;
						if (n < 4) {
							if (atlasA_needed) {
								prep_atlas = 0;
							}
						} else if (n < 8) {
							if (atlasB_needed) {
								prep_atlas = 1;
							}
						} else if (n < 12) {
							if (atlasC_needed) {
								prep_atlas = 2;
							}
						}
						if (prep_atlas >= 0) {
							if (EditorUtility.DisplayDialog ("RTP Notification", "You have to recalc atlas to see changes.", "OK, do it now", "Thanks, I'll do it later")) {
								_target.gloss_baked [n] = ScriptableObject.CreateInstance (typeof(RTPGlossBaked)) as RTPGlossBaked; // nie mamy inf. o zmodyfikowanym roughness dla tej tekstury
								_target.splats_glossBaked [n] = null; // nie mamy też tekstury ze zmodyfikowanymi MIP levelami (będa używane splats[i])
								_target.atlas_glossBaked [n / 4] = null; // używany atlas jest nieważny
								_targetRT.PrepareAtlases (prep_atlas);
							}
						}
						_target.Refresh (null, _targetRT);
					}
						
					tex = _target.Bumps [n];
					ntex = tex;
					checkChange (ref ntex, (Texture2D)EditorGUILayout.ObjectField (_target.Bumps [n], typeof(Texture2D), false, GUILayout.Width (thW), GUILayout.Height (thW)));
					if (ntex != tex) {
						_target.gloss_baked [n] = ScriptableObject.CreateInstance (typeof(RTPGlossBaked)) as RTPGlossBaked; // nie mamy inf. o zmodyfikowanym roughness dla tej tekstury
						_target.splats_glossBaked [n] = null; // nie mamy też tekstury ze zmodyfikowanymi MIP levelami (będa używane splats[i])
						_target.atlas_glossBaked [n / 4] = null; // używany atlas jest nieważny
						Texture2D tex_prev = _target.Bumps [n];
						_target.Bumps [n] = ntex;
						AssetImporter _importer = AssetImporter.GetAtPath (AssetDatabase.GetAssetPath (ntex));
						if (_importer) {
							TextureImporter tex_importer = (TextureImporter)_importer;
							bool changed = false;
							if (!tex_importer.isReadable) {
								Debug.LogWarning ("Normal texture " + n + " (" + ntex.name + ") has been reimported as readable.");
								tex_importer.isReadable = true;
								changed = true;
							}
							if (!tex_importer.normalmap) {
								Debug.LogWarning ("Normal texture " + n + " (" + ntex.name + ") has been reimported as normal map type.");
								tex_importer.normalmap = true;
								changed = true;
							}
							if (changed) {
								AssetDatabase.ImportAsset (AssetDatabase.GetAssetPath (ntex), ImportAssetOptions.ForceUpdate);
							}
						}							
						if (!_target.PrepareNormals ())
							_target.Bumps [n] = tex_prev;
						_target.Refresh (null, _targetRT);
					}
						
					tex = _target.Heights [n];
					ntex = tex;
					checkChange (ref ntex, (Texture2D)EditorGUILayout.ObjectField (_target.Heights [n], typeof(Texture2D), false, GUILayout.Width (thW), GUILayout.Height (thW)));
					if (ntex != tex) {
						Texture2D tex_prev = _target.Heights [n];
						_target.Heights [n] = ntex;
						AssetImporter _importer = AssetImporter.GetAtPath (AssetDatabase.GetAssetPath (ntex));
						if (_importer) {
							TextureImporter tex_importer = (TextureImporter)_importer;
							bool changed = false;
							if (!tex_importer.isReadable) {
								Debug.LogWarning ("Height texture " + n + " (" + ntex.name + ") has been reimported as readable.");
								tex_importer.isReadable = true;
								changed = true;
							}
							if (!tex_importer.linearTexture) {
								Debug.LogWarning ("Height texture " + n + " (" + ntex.name + ") has been reimported as linear.");
								tex_importer.linearTexture = true;
								changed = true;
							}
							if (changed) {
								AssetDatabase.ImportAsset (AssetDatabase.GetAssetPath (ntex), ImportAssetOptions.ForceUpdate);
							}
						}
						if (!_targetRT.PrepareHeights (n))
							_target.Heights [n] = tex_prev;
						_target.Refresh (null, _targetRT);
					}
					EditorGUILayout.EndHorizontal ();
					EditorGUILayout.BeginHorizontal ();
					int thW2 = (Screen.width - 60) / 3;
					// refresh detail (atlas)
					{
						int prep_atlas = -1;
						if (n < 4 && atlasA_needed) {
							prep_atlas = 0;
						} else if (n >= 4 && n < 8 && atlasB_needed) {
							prep_atlas = 1;
						} else if (n >= 8 && n < 12 && atlasC_needed) {
							prep_atlas = 2;
						}
						EditorGUI.BeginDisabledGroup (prep_atlas == -1);
						if (GUILayout.Button ("Refresh", GUILayout.Width (thW2))) 
						{
							_targetRT.RefreshDiffuse(_targetRT, n, prep_atlas);
						}
						EditorGUI.EndDisabledGroup ();
					}
					// refresh normal combined
					if (GUILayout.Button ("Refresh", GUILayout.Width (thW2))) 
					{
						_targetRT.RefreshNormal(_targetRT, n);
					}
					// refresh height combined
					if (GUILayout.Button ("Refresh", GUILayout.Width (thW2))) 
					{
						_targetRT.RefreshHeight(_targetRT, n);
					}

					EditorGUILayout.EndHorizontal ();


						
					
					GUILayout.Space (10);
						
					bool AddPass_flag = (_4LAYERS_SHADER_USED && n >= 4) || (n >= 8);
						
					EditorGUILayout.BeginVertical ("Box");
					EditorGUILayout.LabelField ("PBL / IBL properties", EditorStyles.boldLabel);
					EditorGUILayout.HelpBox ("Detail texture A channel defines gloss (how smooth or rough is the surface), not specularity (named spec here which defines brightness of specular reflection). Current gloss value is commonly named Shininess in Unity built-in shaders. Fresnel for direct lighting works when enabled and in forward ONLY.", MessageType.None, true);
					EditorGUILayout.BeginVertical ("Box");
					float nval;
					nval = _target.Spec [n];
					checkChange (ref nval, EditorGUILayout.Slider ("Layer " + n + " spec multiplier", _target.Spec [n], 0f, 4f, GUILayout.ExpandWidth (true)));
					_target.Spec [n] = nval;
					EditorGUI.BeginDisabledGroup (_target.gloss_baked [n] != null && _target.gloss_baked [n].baked);
					nval = _target.RTP_gloss_mult [n];
					checkChange (ref nval, EditorGUILayout.Slider ("   gloss multiplier", _target.RTP_gloss_mult [n], 0f, 4f, GUILayout.ExpandWidth (true)));
					_target.RTP_gloss_mult [n] = nval;
					EditorGUI.EndDisabledGroup ();
					nval = _target.RTP_gloss2mask [n];
					checkChange (ref nval, EditorGUILayout.Slider ("   spec mask from gloss", _target.RTP_gloss2mask [n], 0f, 1f, GUILayout.ExpandWidth (true)));
					_target.RTP_gloss2mask [n] = nval;
					nval = _target.LayerBrightness2Spec [n];
					checkChange (ref nval, EditorGUILayout.Slider ("   spec mask from albedo", _target.LayerBrightness2Spec [n], 0f, 1f, GUILayout.ExpandWidth (true)));
					_target.LayerBrightness2Spec [n] = nval;
					EditorGUI.BeginDisabledGroup (_target.gloss_baked [n] != null && _target.gloss_baked [n].baked);
					nval = _target.RTP_gloss_shaping [n];
					checkChange (ref nval, EditorGUILayout.Slider ("   gloss shaping", _target.RTP_gloss_shaping [n] * 2 - 1, -1f, 1f, GUILayout.ExpandWidth (true)) * 0.5f + 0.5f);
					_target.RTP_gloss_shaping [n] = nval;
					EditorGUI.EndDisabledGroup ();
			
					EditorGUILayout.EndVertical ();	
						
					// EOF gloss baking section
					GUILayout.Space (8);
						
					if (!RTP_SUPER_SIMPLE) {						
						EditorGUILayout.LabelField ("Layer adjustement", EditorStyles.boldLabel);
						EditorGUILayout.BeginHorizontal ();	
						EditorGUILayout.LabelField ("Layer Brightness", GUILayout.Width (127));
						nval = _target.LayerBrightness [n];
						checkChange (ref nval, EditorGUILayout.Slider (_target.LayerBrightness [n], 0f, 4f, GUILayout.ExpandWidth (true)));
						_target.LayerBrightness [n] = nval;
						EditorGUILayout.EndHorizontal ();
						EditorGUILayout.BeginHorizontal ();
						EditorGUILayout.LabelField ("Layer Saturation", GUILayout.Width (127));
						nval = _target.LayerSaturation [n];
						checkChange (ref nval, EditorGUILayout.Slider (_target.LayerSaturation [n], 0f, 2f, GUILayout.ExpandWidth (true)));
						_target.LayerSaturation [n] = nval;
						EditorGUILayout.EndHorizontal ();	
						EditorGUILayout.BeginHorizontal ();	
						EditorGUILayout.LabelField ("Far filtering", GUILayout.Width (127));
						nval = _target.MIPmult [n];
						checkChange (ref nval, EditorGUILayout.Slider (_target.MIPmult [n], 0f, 7f, GUILayout.ExpandWidth (true)));
						_target.MIPmult [n] = nval;
						EditorGUILayout.EndHorizontal ();	

						EditorGUILayout.BeginHorizontal ();	
						EditorGUILayout.LabelField ("Tess Strenght", GUILayout.Width (127));
						nval = _target.TessStrenght [n];
						checkChange (ref nval, EditorGUILayout.Slider (_target.TessStrenght [n], -3.0f, 3.0f, GUILayout.ExpandWidth (true)));
						_target.TessStrenght [n] = nval;
						EditorGUILayout.EndHorizontal ();

						if ((!AddPass_flag && RTP_HEIGHTBLEND_AO_FIRST) || (AddPass_flag && RTP_HEIGHTBLEND_AO_ADD)) {
							EditorGUILayout.BeginHorizontal ();	
							EditorGUILayout.LabelField ("Heightblend AO", GUILayout.Width (127));
							nval = _target.AO_strength [n];
							checkChange (ref nval, EditorGUILayout.Slider (_target.AO_strength [n], 0f, 1f, GUILayout.ExpandWidth (true)));
							_target.AO_strength [n] = nval;
							EditorGUILayout.EndHorizontal ();	
						}							
					
					} // EOF !super-simple
						
					EditorGUILayout.EndVertical ();
					EditorGUILayout.Space ();
				} // EOF !super-simple
					
					
				#endregion Detail maps unfold	- layers
			}
				
				
			#endregion Detail maps unfold
		}	
			
		if (_target.show_derivedmaps) {
			#region Derived textures
			Color skin_color;

				
			#region Derived textures - splats					
			GUILayout.Space (10);

			EditorGUILayout.BeginVertical ("Box");
			skin_color = GUI.color;
			GUI.color = new Color (1, 1, 0.5f, 1);
			EditorGUILayout.LabelField ("Splatmaps (control maps)", EditorStyles.boldLabel);
			GUI.color = skin_color;
			if (terrainComp) {
				if (GUILayout.Button ("Refresh Controlmap(s) shown from TerrainData")) {
					_targetRT.GetControlMaps ();
				}
			}


			EditorGUILayout.LabelField ("Splat 0-3", GUILayout.MaxWidth (65));
			EditorGUILayout.BeginHorizontal ();
			if (terrainComp != null) {
				bool tmp_dirtyFlag = dirtyFlag;
				checkChange (ref _targetRT.controlA, (Texture2D)EditorGUILayout.ObjectField (_targetRT.controlA, typeof(Texture2D), false, GUILayout.MinHeight (100), GUILayout.MinWidth (100), GUILayout.MaxWidth (100)));
				if (dirtyFlag && !tmp_dirtyFlag) {
					AssetImporter _importer = AssetImporter.GetAtPath (AssetDatabase.GetAssetPath (_targetRT.controlA));
					if (_importer) {
						TextureImporter tex_importer = (TextureImporter)_importer;
						if (!tex_importer.isReadable) {
							Debug.LogWarning ("Control texture 0 (" + _targetRT.controlA.name + ") has been reimported as readable.");
							tex_importer.isReadable = true;
							AssetDatabase.ImportAsset (AssetDatabase.GetAssetPath (_targetRT.controlA), ImportAssetOptions.ForceUpdate);
						}
					}					
					_targetRT.SetCustomControlMaps ();
				}
			} else {
				checkChange (ref _targetRT.controlA, (Texture2D)EditorGUILayout.ObjectField (_targetRT.controlA, typeof(Texture2D), false, GUILayout.MinHeight (100), GUILayout.MinWidth (100), GUILayout.MaxWidth (100)));
			}
			if (_target.numLayers > 4) {
				EditorGUILayout.LabelField ("Splat 4-7", GUILayout.MaxWidth (65));
				if (terrainComp != null) {
					bool tmp_dirtyFlag = dirtyFlag;
					checkChange (ref _targetRT.controlB, (Texture2D)EditorGUILayout.ObjectField (_targetRT.controlB, typeof(Texture2D), false, GUILayout.MinHeight (100), GUILayout.MinWidth (100), GUILayout.MaxWidth (100)));
					if (dirtyFlag && !tmp_dirtyFlag) {
						AssetImporter _importer = AssetImporter.GetAtPath (AssetDatabase.GetAssetPath (_targetRT.controlB));
						if (_importer) {
							TextureImporter tex_importer = (TextureImporter)_importer;
							if (!tex_importer.isReadable) {
								Debug.LogWarning ("Control texture 1 (" + _targetRT.controlB.name + ") has been reimported as readable.");
								tex_importer.isReadable = true;
								AssetDatabase.ImportAsset (AssetDatabase.GetAssetPath (_targetRT.controlB), ImportAssetOptions.ForceUpdate);
							}
						}					
						_targetRT.SetCustomControlMaps ();
					}
				} else {
					checkChange (ref _targetRT.controlB, (Texture2D)EditorGUILayout.ObjectField (_targetRT.controlB, typeof(Texture2D), false, GUILayout.MinHeight (100), GUILayout.MinWidth (100), GUILayout.MaxWidth (100)));
				}
			}
			EditorGUILayout.EndHorizontal ();

			EditorGUILayout.BeginHorizontal ();
			if (GUILayout.Button ("Save to file", GUILayout.MinWidth (170), GUILayout.MaxWidth (170))) {
				Texture2D tex = _targetRT.controlA;
				if (tex) {
					SaveTexture (ref tex, ref _targetRT.save_path_controlA, "terrain_splatMapA.png", 0, TextureImporterFormat.ARGB32, true, false, true);
				} else {
					EditorUtility.DisplayDialog ("?", "Can't get height texture...", "OK");
				}
			}								
			if (_target.numLayers > 4) {
				if (GUILayout.Button ("Save to file", GUILayout.MinWidth (170), GUILayout.MaxWidth (170))) {
					Texture2D tex = _targetRT.controlB;
					if (tex) {
						SaveTexture (ref tex, ref _targetRT.save_path_controlB, "terrain_splatMapB.png", 0, TextureImporterFormat.ARGB32, true, false, true);
					} else {
						EditorUtility.DisplayDialog ("?", "Can't get height texture...", "OK");
					}
				}								
			}
			EditorGUILayout.EndHorizontal ();

			if (_target.numLayers > 8) {
				EditorGUILayout.BeginHorizontal ();
				EditorGUILayout.LabelField ("Splat 8-11", GUILayout.MaxWidth (65));
				if (terrainComp != null) {
					bool tmp_dirtyFlag = dirtyFlag;
					checkChange (ref _targetRT.controlC, (Texture2D)EditorGUILayout.ObjectField (_targetRT.controlC, typeof(Texture2D), false, GUILayout.MinHeight (100), GUILayout.MinWidth (100), GUILayout.MaxWidth (100)));
					if (dirtyFlag && !tmp_dirtyFlag) {
						AssetImporter _importer = AssetImporter.GetAtPath (AssetDatabase.GetAssetPath (_targetRT.controlC));
						if (_importer) {
							TextureImporter tex_importer = (TextureImporter)_importer;
							if (!tex_importer.isReadable) {
								Debug.LogWarning ("Control texture 2 (" + _targetRT.controlC.name + ") has been reimported as readable.");
								tex_importer.isReadable = true;
								AssetDatabase.ImportAsset (AssetDatabase.GetAssetPath (_targetRT.controlC), ImportAssetOptions.ForceUpdate);
							}
						}					
						_targetRT.SetCustomControlMaps ();
					}
				} else {
					checkChange (ref _targetRT.controlC, (Texture2D)EditorGUILayout.ObjectField (_targetRT.controlC, typeof(Texture2D), false, GUILayout.MinHeight (100), GUILayout.MinWidth (100), GUILayout.MaxWidth (100)));
				}
				EditorGUILayout.EndHorizontal ();
			}

			if (_target.numLayers > 8) {
				if (GUILayout.Button ("Save to file", GUILayout.MinWidth (170), GUILayout.MaxWidth (170))) {
					Texture2D tex = _targetRT.controlC;
					if (tex) {
						SaveTexture (ref tex, ref _targetRT.save_path_controlC, "terrain_splatMapC.png", 0, TextureImporterFormat.ARGB32, true, false, true);
					} else {
						EditorUtility.DisplayDialog ("?", "Can't get height texture...", "OK");
					}
				}								
			}
			#endregion Derived textures - splats
			//case ReliefTerrainDerivedTexturesItems.Atlasing:
			#region Derived textures - Atlasing features
			GUILayout.Space (10);
					
			EditorGUILayout.BeginVertical ("Box");
			skin_color = GUI.color;
			GUI.color = new Color (1, 1, 0.5f, 1);
			EditorGUILayout.LabelField ("Color atlases", EditorStyles.boldLabel);
			GUI.color = skin_color;
					
			EditorGUI.BeginDisabledGroup ((!atlasA_needed) && (!atlasB_needed) && (!atlasC_needed));
			if (GUILayout.Button ("Prepare atlases from detail textures (may take a while)")) {
				//if (atlasA_needed)
				//	_targetRT.PrepareAtlases (0);
				//if (atlasB_needed)
				//	_targetRT.PrepareAtlases (1);
				//if (atlasC_needed)
				//	_targetRT.PrepareAtlases (2);
				_target.Refresh (null, _targetRT);
			}
			EditorGUI.EndDisabledGroup ();
					
			EditorGUILayout.BeginHorizontal ();
			if (_target.numLayers > 0) {
				EditorGUI.BeginDisabledGroup (!atlasA_needed);
				EditorGUILayout.BeginVertical ();
				EditorGUILayout.LabelField ("Layers 0-3", GUILayout.Width (100));
				checkChange (ref _target.splat_atlases [0], (Texture2D)EditorGUILayout.ObjectField (_target.splat_atlases [0], typeof(Texture2D), false, GUILayout.Height (100), GUILayout.Width (100)));
				if (SaveTexture (ref _target.splat_atlases [0], ref _target.save_path_atlasA, "atlas_texture_layers_0_to_3.png", 100, TextureImporterFormat.AutomaticCompressed, true)) {
					string path = AssetDatabase.GetAssetPath (_target.splat_atlases [0]);
					TextureImporter textureImporter = AssetImporter.GetAtPath (path) as TextureImporter;
					textureImporter.wrapMode = TextureWrapMode.Clamp;
					textureImporter.filterMode = FilterMode.Trilinear;
					AssetDatabase.ImportAsset (path, ImportAssetOptions.ForceUpdate);
				}
				EditorGUILayout.EndVertical ();
				EditorGUI.EndDisabledGroup ();
			}
					
			if (_target.numLayers > 4) {
				EditorGUI.BeginDisabledGroup (!atlasB_needed);
				EditorGUILayout.BeginVertical ();
				EditorGUILayout.LabelField ("Layers 4-7", GUILayout.Width (100));
				checkChange (ref _target.splat_atlases [1], (Texture2D)EditorGUILayout.ObjectField (_target.splat_atlases [1], typeof(Texture2D), false, GUILayout.Height (100), GUILayout.Width (100)));
				if (SaveTexture (ref _target.splat_atlases [1], ref _target.save_path_atlasB, "atlas_texture_layers_4_to_7.png", 100, TextureImporterFormat.AutomaticCompressed, true)) {
					_target.splat_atlases [1].wrapMode = TextureWrapMode.Clamp;
					_target.splat_atlases [1].filterMode = FilterMode.Trilinear;
				}
				EditorGUILayout.EndVertical ();
				EditorGUI.EndDisabledGroup ();
			}
					
			EditorGUILayout.EndHorizontal ();		
					
			if (_target.numLayers > 8) {
				EditorGUI.BeginDisabledGroup (!atlasC_needed);
				EditorGUILayout.BeginVertical ();
				EditorGUILayout.LabelField ("Layers 9-11", GUILayout.Width (100));
				checkChange (ref _target.splat_atlases [2], (Texture2D)EditorGUILayout.ObjectField (_target.splat_atlases [2], typeof(Texture2D), false, GUILayout.Height (100), GUILayout.Width (100)));
				if (SaveTexture (ref _target.splat_atlases [2], ref _target.save_path_atlasC, "atlas_texture_layers_8_to_11.png", 100, TextureImporterFormat.AutomaticCompressed, true)) {
					_target.splat_atlases [2].wrapMode = TextureWrapMode.Clamp;
					_target.splat_atlases [2].filterMode = FilterMode.Trilinear;
				}
				EditorGUILayout.EndVertical ();
				EditorGUI.EndDisabledGroup ();
			}
					
			EditorGUILayout.EndVertical ();
			#endregion Derived textures - Atlasing features		
			//break;

			//case ReliefTerrainDerivedTexturesItems.Heightmaps:
			#region Derived textures - Heightmaps
			GUILayout.Space (10);
					
			EditorGUILayout.BeginVertical ("Box");
			skin_color = GUI.color;
			GUI.color = new Color (1, 1, 0.5f, 1);
			EditorGUILayout.LabelField ("Combined heightmaps", EditorStyles.boldLabel);
			GUI.color = skin_color;
					
			EditorGUILayout.BeginHorizontal ();
					
			if (_target.numLayers > 0) {
				EditorGUILayout.BeginVertical ();
				EditorGUILayout.LabelField ("Heights 0-3", GUILayout.Width (80));
				checkChange (ref _target.HeightMap, (Texture2D)EditorGUILayout.ObjectField (_target.HeightMap, typeof(Texture2D), false, GUILayout.Height (80), GUILayout.Width (80)));
				SaveTexture (ref _target.HeightMap, ref _target.save_path_HeightMap, "heightmap_layers_0_to_3.png", 80, TextureImporterFormat.AutomaticCompressed, true, true, true);
				EditorGUILayout.EndVertical ();
			}
					
			if (_target.numLayers > 4) {
				EditorGUILayout.BeginVertical ();
				EditorGUILayout.LabelField ("Heights 4-7", GUILayout.Width (80));
				checkChange (ref _target.HeightMap2, (Texture2D)EditorGUILayout.ObjectField (_target.HeightMap2, typeof(Texture2D), false, GUILayout.Height (80), GUILayout.Width (80)));
				SaveTexture (ref _target.HeightMap2, ref _target.save_path_HeightMap2, "heightmap_layers_4_to_7.png", 80, TextureImporterFormat.AutomaticCompressed, true, true, true);
				EditorGUILayout.EndVertical ();
			}
					
			if (_target.numLayers > 8) {
				EditorGUILayout.BeginVertical ();
				EditorGUILayout.LabelField ("Heights 8-12", GUILayout.Width (80));
				checkChange (ref _target.HeightMap3, (Texture2D)EditorGUILayout.ObjectField (_target.HeightMap3, typeof(Texture2D), false, GUILayout.Height (80), GUILayout.Width (80)));
				SaveTexture (ref _target.HeightMap3, ref _target.save_path_HeightMap3, "heightmap_layers_8_to_11.png", 80, TextureImporterFormat.AutomaticCompressed, true, true, true);
				EditorGUILayout.EndVertical ();
			}
					
			EditorGUILayout.EndHorizontal ();						
					
			EditorGUILayout.EndVertical ();
			#endregion Derived textures - Heightmaps

			//case ReliefTerrainDerivedTexturesItems.Bumpmaps:
			#region Derived textures - Bumpmaps
			GUILayout.Space (10);
					
			EditorGUILayout.BeginVertical ("Box");
			skin_color = GUI.color;
			GUI.color = new Color (1, 1, 0.5f, 1);
			EditorGUILayout.LabelField ("Combined bumpmaps", EditorStyles.boldLabel);
			GUI.color = skin_color;
					

			EditorGUILayout.BeginHorizontal ();
			if (_target.numLayers > 0) {
				EditorGUILayout.BeginVertical ();
				EditorGUILayout.LabelField ("Bumps 0+1", GUILayout.Width (80));
				checkChange (ref _target.Bump01, (Texture2D)EditorGUILayout.ObjectField (_target.Bump01, typeof(Texture2D), false, GUILayout.Height (80), GUILayout.Width (80)));
				SaveTexture (ref _target.Bump01, ref _target.save_path_Bump01, "bumpmap_layers01.png", 80, TextureImporterFormat.ARGB32, true, true, true);
				EditorGUILayout.EndVertical ();
			}
					
			if (_target.numLayers > 2) {
				EditorGUILayout.BeginVertical ();
				EditorGUILayout.LabelField ("Bumps 2+3", GUILayout.Width (80));
				checkChange (ref _target.Bump23, (Texture2D)EditorGUILayout.ObjectField (_target.Bump23, typeof(Texture2D), false, GUILayout.Height (80), GUILayout.Width (80)));
				SaveTexture (ref _target.Bump23, ref _target.save_path_Bump23, "bumpmap_layers23.png", 80, TextureImporterFormat.ARGB32, true, true, true);
				EditorGUILayout.EndVertical ();
			}
					
			if (_target.numLayers > 4) {
				EditorGUILayout.BeginVertical ();
				EditorGUILayout.LabelField ("Bumps 4+5", GUILayout.Width (80));
				checkChange (ref _target.Bump45, (Texture2D)EditorGUILayout.ObjectField (_target.Bump45, typeof(Texture2D), false, GUILayout.Height (80), GUILayout.Width (80)));
				SaveTexture (ref _target.Bump45, ref _target.save_path_Bump45, "bumpmap_layers45.png", 80, TextureImporterFormat.ARGB32, true, true, true);
				EditorGUILayout.EndVertical ();
			}
					
			EditorGUILayout.EndHorizontal ();
			GUILayout.Space (10);
					
			if (_target.numLayers > 6) {
				EditorGUILayout.BeginHorizontal ();
				if (_target.numLayers > 6) {							
					EditorGUILayout.BeginVertical ();
					EditorGUILayout.LabelField ("Bumps 6+7", GUILayout.Width (80));
					checkChange (ref _target.Bump67, (Texture2D)EditorGUILayout.ObjectField (_target.Bump67, typeof(Texture2D), false, GUILayout.Height (80), GUILayout.Width (80)));
					SaveTexture (ref _target.Bump67, ref _target.save_path_Bump67, "bumpmap_layers67.png", 80, TextureImporterFormat.ARGB32, true, true, true);
					EditorGUILayout.EndVertical ();
				}
						
				if (_target.numLayers > 8) {
					EditorGUILayout.BeginVertical ();
					EditorGUILayout.LabelField ("Bumps 8+9", GUILayout.Width (80));
					checkChange (ref _target.Bump89, (Texture2D)EditorGUILayout.ObjectField (_target.Bump89, typeof(Texture2D), false, GUILayout.Height (80), GUILayout.Width (80)));
					SaveTexture (ref _target.Bump89, ref _target.save_path_Bump89, "bumpmap_layers89.png", 80, TextureImporterFormat.ARGB32, true, true, true);
					EditorGUILayout.EndVertical ();
				}
						
				if (_target.numLayers > 10) {
					EditorGUILayout.BeginVertical ();
					EditorGUILayout.LabelField ("Bumps 10+11", GUILayout.Width (80));
					checkChange (ref _target.BumpAB, (Texture2D)EditorGUILayout.ObjectField (_target.BumpAB, typeof(Texture2D), false, GUILayout.Height (80), GUILayout.Width (80)));
					SaveTexture (ref _target.BumpAB, ref _target.save_path_BumpAB, "bumpmap_layersAB.png", 80, TextureImporterFormat.ARGB32, true, true, true);
					EditorGUILayout.EndVertical ();
				}
						
				EditorGUILayout.EndHorizontal ();
				GUILayout.Space (10);
			}
					
			EditorGUILayout.EndVertical ();
			#endregion Derived textures - Bumpmaps
			//break;	

			//case ReliefTerrainDerivedTexturesItems.Controlmaps:
				

			EditorGUILayout.EndVertical ();
			// derived textures submenu switch
			#endregion Derived textures
		}
			
		if (_target.show_settings) {
			#region ALLSettings
			GUIContent[] toolbarIcons1 = new GUIContent[2] {
				new GUIContent ("Painting", icoGlobalcolor),
				new GUIContent ("Settings", icoLayersSmall)
			};
			ReliefTerrainSettingsItems prev_submenu_settings = _target.submenu_settings;
			GUILayout.Space (10);
			EditorGUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			_target.submenu_settings = (ReliefTerrainSettingsItems)(GUILayout.Toolbar ((int)_target.submenu_settings, toolbarIcons1, EditorStyles.miniButton, GUILayout.MaxWidth (370)));
			GUILayout.FlexibleSpace ();
			EditorGUILayout.EndHorizontal ();
			GUILayout.Space (5);

				
			if (prev_submenu_settings != _target.submenu_settings && _target.paint_flag) {
				_target.paint_flag = false;
				UnityEditor.Tools.current = _targetRT.prev_tool;
				SceneView.onSceneGUIDelegate -= ReliefTerrain._SceneGUI;
				EditorUtility.SetDirty (target);
			}
				
			Color skin_color;
			switch (_target.submenu_settings) {
			case ReliefTerrainSettingsItems.MainSettings:
					#region Settings	 - main settings

							
				EditorGUILayout.LabelField ("Near distance values", EditorStyles.boldLabel);
				EditorGUILayout.BeginVertical ("Box");
				skin_color = GUI.color;
					

					
				//EditorGUILayout.LabelField("Near distance values", EditorStyles.boldLabel);
				EditorGUILayout.BeginHorizontal ();
					//EditorGUILayout.LabelField("", GUILayout.MaxWidth(10));
				EditorGUILayout.BeginVertical ();
					#if !UNITY_3_5
				float prev_distance_start = _target.distance_start;
				float prev_distance_transition = _target.distance_transition;
					#endif
				EditorGUILayout.BeginHorizontal ();
				EditorGUILayout.LabelField ("Splat Counts");
				LayersNumber lnOld = _target.ln;
				_target.ln = (LayersNumber)EditorGUILayout.EnumPopup (_target.ln, GUILayout.Height (25));
				if (lnOld != _target.ln) {
					Debug.Log ("FF2");
					_targetRT.RefreshTextures ();
				}
				EditorGUILayout.EndHorizontal ();
				if (_target.distance_start_bumpglobal < _target.distance_start)
					_target.distance_start_bumpglobal = _target.distance_start;

				checkChange (ref _target.distance_start, EditorGUILayout.Slider ("Distance start", _target.distance_start, 0, 100));
				if (_target.distance_start_bumpglobal < _target.distance_start)
					_target.distance_start_bumpglobal = _target.distance_start;
				checkChange (ref _target.distance_transition, EditorGUILayout.Slider ("Fade length", _target.distance_transition, 0, 100));
					#if !UNITY_3_5
				if (_target.useTerrainMaterial) {
					if (prev_distance_start != _target.distance_start || prev_distance_transition != _target.distance_transition) {
						Terrain[] terrainComps = (Terrain[])GameObject.FindObjectsOfType (typeof(Terrain));
						for (int ter = 0; ter < terrainComps.Length; ter++) {
							// U5
							#if UNITY_5_0
									terrainComps[ter].basemapDistance=0;
							#else
							if (_target.super_simple_active) {
								terrainComps [ter].basemapDistance = _target.distance_start_bumpglobal + _target.distance_transition_bumpglobal;
							} else {
								terrainComps [ter].basemapDistance = _target.distance_start + _target.distance_transition;
							}
							#endif
						}
					}
				}
					#endif
					
				EditorGUILayout.EndVertical ();
				EditorGUILayout.EndHorizontal ();
					
				EditorGUILayout.Space ();	
				checkChange (ref _target.RTP_MIP_BIAS, EditorGUILayout.Slider ("MIP level bias", _target.RTP_MIP_BIAS, -0.75f, 0.75f));
				EditorGUILayout.EndVertical ();
					
				EditorGUILayout.Space ();
				GUI.color = new Color (1, 1, 0.5f, 1);						
				EditorGUILayout.LabelField ("Shading color adjustements", EditorStyles.boldLabel);
				GUI.color = skin_color;
				EditorGUILayout.BeginVertical ("Box");
				checkChange (ref _target._SpecColor, EditorGUILayout.ColorField ("Specular Color", _target._SpecColor));
				checkChange (ref _target.rtp_customAmbientCorrection, EditorGUILayout.ColorField ("Ambient correction", _target.rtp_customAmbientCorrection));
				checkChange (ref _target.MasterLayerBrightness, EditorGUILayout.Slider ("Layers detail brightness", _target.MasterLayerBrightness, 0, 2));
				checkChange (ref _target.MasterLayerSaturation, EditorGUILayout.Slider ("Layers detail saturation", _target.MasterLayerSaturation, 0, 2));
				EditorGUILayout.BeginHorizontal ();
				EditorGUILayout.LabelField ("Gloss for deferred AddPass", GUILayout.MinWidth (165), GUILayout.MaxWidth (165));
				checkChange (ref _target.RTP_DeferredAddPassSpec, EditorGUILayout.Slider (_target.RTP_DeferredAddPassSpec, 0, 1));
				EditorGUILayout.EndHorizontal ();
				EditorGUILayout.EndVertical ();
					
				if (RTP_IBL_DIFFUSE_FIRST || RTP_IBL_DIFFUSE_ADD || RTP_IBL_SPEC_FIRST || RTP_IBL_SPEC_ADD || REFLECTION_ENABLED_FIRST || REFLECTION_ENABLED_ADD) {
					EditorGUILayout.Space ();
					GUI.color = new Color (1, 1, 0.5f, 1);						
					EditorGUILayout.LabelField ("IBL / Reflections", EditorStyles.boldLabel);
					GUI.color = skin_color;
					EditorGUILayout.BeginVertical ("Box");
					EditorGUILayout.HelpBox ("AO damp below means AO that comes from POM self-shadowing or baked shadows map (alpha channel of ambient emissive or pixeltrees texture).", MessageType.None, true);
					if (RTP_IBL_DIFFUSE_FIRST || RTP_IBL_DIFFUSE_ADD) {
						checkChange (ref _target.TERRAIN_IBL_DiffAO_Damp, EditorGUILayout.Slider ("Diffuse IBL AO damp", _target.TERRAIN_IBL_DiffAO_Damp, 0, 1));
					}
					if (RTP_IBL_SPEC_FIRST || RTP_IBL_SPEC_ADD || REFLECTION_ENABLED_FIRST || REFLECTION_ENABLED_ADD) {
						checkChange (ref _target.TERRAIN_IBLRefl_SpecAO_Damp, EditorGUILayout.Slider ("Spec IBL / Refl AO damp", _target.TERRAIN_IBLRefl_SpecAO_Damp, 0, 1));
					}
					if (!RTP_SKYSHOP_SYNC && (RTP_IBL_DIFFUSE_FIRST || RTP_IBL_DIFFUSE_ADD || RTP_IBL_SPEC_FIRST || RTP_IBL_SPEC_ADD)) {
						EditorGUILayout.BeginHorizontal ();
						#if UNITY_5
						// we don't use cubemap in Unity5 (SH instead)
						EditorGUI.BeginDisabledGroup (true);
						#endif
						if (RTP_IBL_DIFFUSE_FIRST || RTP_IBL_DIFFUSE_ADD) {
							EditorGUILayout.BeginVertical ();
							EditorGUILayout.LabelField ("IBL diffuse map", EditorStyles.boldLabel, GUILayout.MinWidth (120), GUILayout.MaxWidth (120));
							checkChange (ref _target._CubemapDiff, (Cubemap)EditorGUILayout.ObjectField (_target._CubemapDiff, typeof(Cubemap), false, GUILayout.MinHeight (120), GUILayout.MinWidth (120), GUILayout.MaxWidth (120)));
							EditorGUILayout.EndVertical ();
						}
						#if UNITY_5
						EditorGUI.EndDisabledGroup ();
						#endif
						if (RTP_IBL_SPEC_FIRST || RTP_IBL_SPEC_ADD) {
							EditorGUILayout.BeginVertical ();
							EditorGUILayout.LabelField ("IBL specular map", EditorStyles.boldLabel, GUILayout.MinWidth (120), GUILayout.MaxWidth (120));
							checkChange (ref _target._CubemapSpec, (Cubemap)EditorGUILayout.ObjectField (_target._CubemapSpec, typeof(Cubemap), false, GUILayout.MinHeight (120), GUILayout.MinWidth (120), GUILayout.MaxWidth (120)));
							EditorGUILayout.EndVertical ();
						}
						EditorGUILayout.EndHorizontal ();
					}
						
					EditorGUILayout.EndVertical ();
				}
					
			
					
				bool showLightmapShading = false;
					#if UNITY_5
				if (terrainComp) {
					if ((terrainComp.lightmapIndex != 65535) && (terrainComp.lightmapIndex != 65534) && LightmapSettings.lightmapsMode == LightmapsMode.NonDirectional) {
						showLightmapShading = true;
					}
				} else {
					if ((_targetRT.GetComponent<Renderer> ().lightmapIndex != 65535) && (_targetRT.GetComponent<Renderer> ().lightmapIndex != 65534) && LightmapSettings.lightmapsMode != LightmapsMode.NonDirectional) {
						showLightmapShading = true;
					}
				}
					#else
					if (terrainComp) {
						if ((terrainComp.lightmapIndex!=255) && (terrainComp.lightmapIndex!=-1) && LightmapSettings.lightmapsMode!=LightmapsMode.Directional) {
							showLightmapShading=true;
						}
					} else {
						if ((_targetRT.GetComponent<Renderer>().lightmapIndex!=255) && (_targetRT.GetComponent<Renderer>().lightmapIndex!=-1) && LightmapSettings.lightmapsMode!=LightmapsMode.Directional) {
							showLightmapShading=true;
						}
					}
					#endif
										
				if (RTP_HEIGHTBLEND_AO_FIRST || RTP_HEIGHTBLEND_AO_ADD) {
					EditorGUILayout.Space ();	
					GUI.color = new Color (1, 1, 0.5f, 1);						
					EditorGUILayout.LabelField ("Heightblend AO", EditorStyles.boldLabel);
					GUI.color = skin_color;
					EditorGUILayout.BeginVertical ("Box");
					checkChange (ref _target.RTP_AOsharpness, EditorGUILayout.Slider ("Fake AO 2 HB sharpness", _target.RTP_AOsharpness, 0, 10));
					checkChange (ref _target.RTP_AOamp, EditorGUILayout.Slider ("Fake AO 2 HB value", _target.RTP_AOamp, 0, 2));
					EditorGUILayout.EndVertical ();
				}
				if ((RTP_HEIGHTBLEND_AO_FIRST && RTP_HOTAIR_EMISSION_FIRST) || (RTP_HEIGHTBLEND_AO_ADD && RTP_HOTAIR_EMISSION_ADD)) {
					EditorGUILayout.Space ();	
					GUI.color = new Color (1, 1, 0.5f, 1);						
					EditorGUILayout.LabelField ("Hot air refraction on emissive layers", EditorStyles.boldLabel);
					GUI.color = skin_color;
					EditorGUILayout.BeginVertical ("Box");
				
					EditorGUILayout.EndVertical ();
				}

				GUILayout.Space (10);
				EditorGUILayout.BeginVertical ("Box");
				skin_color = GUI.color;
				EditorGUILayout.BeginHorizontal ();
				GUI.color = new Color (1, 1, 0.5f, 1);

				EditorGUILayout.LabelField ("Tessellation settings", EditorStyles.boldLabel);

				EditorGUILayout.EndHorizontal ();
				GUI.color = skin_color;

				if (RTP_TESSELLATION) {

					if (!RTP_TESSELLATION_SAMPLE_TEXTURE) {
						_targetRT.NormalGlobal = null;
					}
					EditorGUILayout.BeginHorizontal ();


					EditorGUILayout.BeginVertical ();

					EditorGUI.BeginDisabledGroup (!RTP_TESSELLATION_SAMPLE_TEXTURE);
					EditorGUILayout.BeginHorizontal ();
					EditorGUILayout.LabelField ("Normal strength", GUILayout.MinWidth (150), GUILayout.MaxWidth (150));

					EditorGUILayout.EndHorizontal ();
					EditorGUI.EndDisabledGroup ();
					EditorGUILayout.BeginHorizontal ();
					EditorGUILayout.LabelField ("Tess. disp. offset", GUILayout.MinWidth (150), GUILayout.MaxWidth (150));
					if (checkChange (ref _target._TessYOffset, EditorGUILayout.Slider (_target._TessYOffset, 0.1f, 10.0f))) {
						_target._TessYOffset = _target._TessYOffset;
					}
					EditorGUILayout.EndHorizontal ();
					EditorGUILayout.BeginHorizontal ();
					EditorGUILayout.LabelField ("Tess. substeps (close)", GUILayout.MinWidth (150), GUILayout.MaxWidth (150));
					if (checkChange (ref _target._TessSubdivisions, EditorGUILayout.Slider (_target._TessSubdivisions, 1f, 63f))) {
						_target._TessSubdivisions = Mathf.Floor ((_target._TessSubdivisions - 1) / 2) * 2 + 1;
					}
					EditorGUILayout.EndHorizontal ();
					EditorGUILayout.BeginHorizontal ();
					EditorGUILayout.LabelField ("Tess. substeps (far)", GUILayout.MinWidth (150), GUILayout.MaxWidth (150));
					if (checkChange (ref _target._TessSubdivisionsFar, EditorGUILayout.Slider (_target._TessSubdivisionsFar, 1f, 63f))) {
						_target._TessSubdivisionsFar = Mathf.Floor ((_target._TessSubdivisionsFar - 1) / 2) * 2 + 1;
					}
					EditorGUILayout.EndHorizontal ();
					EditorGUI.BeginDisabledGroup (RTP_TESSELLATION_SAMPLE_TEXTURE);
					if (RTP_TESSELLATION_SAMPLE_TEXTURE)
						_target._Phong = 0;
					EditorGUILayout.BeginHorizontal ();
					EditorGUILayout.LabelField ("Phong smoothening", GUILayout.MinWidth (150), GUILayout.MaxWidth (150));
					checkChange (ref _target._Phong, EditorGUILayout.Slider (_target._Phong, 0f, 1f));
					EditorGUILayout.EndHorizontal ();
					EditorGUI.EndDisabledGroup ();

					Terrain terrain = _targetRT.gameObject.GetComponent<Terrain> ();
					if (terrain != null) {
						EditorGUILayout.BeginHorizontal ();
						EditorGUILayout.LabelField ("Terrain pixel error", GUILayout.MinWidth (150), GUILayout.MaxWidth (150));
						float nPixelError = EditorGUILayout.Slider (terrain.heightmapPixelError, 1f, 200f);
						if (terrain.heightmapPixelError != nPixelError) {
							terrain.heightmapPixelError = nPixelError;
							ReliefTerrain[] rtp_objs = (ReliefTerrain[])GameObject.FindObjectsOfType (typeof(ReliefTerrain));
							for (int i = 0; i < rtp_objs.Length; i++) {
								Terrain ter = rtp_objs [i].gameObject.GetComponent<Terrain> ();
								if (ter) {
									ter.heightmapPixelError = nPixelError;
								}
							}
						}
						EditorGUILayout.EndHorizontal ();
						EditorGUI.BeginDisabledGroup (_targetRT.NormalGlobal == null);
						if (GUILayout.Button ("Reapply height texture to terrain data")) {
							if (EditorUtility.DisplayDialog ("Warning", "Are you sure to overwrite terrain height data ?", "Yes", "Cancel")) {
								AssetImporter _importer = AssetImporter.GetAtPath (AssetDatabase.GetAssetPath (_targetRT.NormalGlobal));
								if (_importer) {
									TextureImporter tex_importer = (TextureImporter)_importer;
									if (!tex_importer.isReadable) {
										Debug.LogWarning ("Global normal texture (" + _targetRT.NormalGlobal.name + ") has been reimported as readable.");
										tex_importer.isReadable = true;
										AssetDatabase.ImportAsset (AssetDatabase.GetAssetPath (_targetRT.NormalGlobal), ImportAssetOptions.ForceUpdate);
									}
								}
								{
									TerrainData terrainData = terrain.terrainData;
									float[,] heights;
									float _ratio = 1.0f * _targetRT.NormalGlobal.width / terrainData.heightmapResolution;
									if (_ratio > 1.9f) {
										// sample from hi-res texture to lower res heightmap
										// we take max sample
										// this is used to conform lo-res terrain mesh with its occlusion data (useful in Unity5 where terrain surface can be rendered separately)
										heights = ReliefTerrain.ConformTerrain2Occlusion (terrainData, _targetRT.NormalGlobal);
									} else {
										// sample from hi-res texture to similar or higher res heightmap
										// we take exact sample
										// this is used to conform detail placement and collider fit
										heights = ReliefTerrain.ConformTerrain2Detail (terrainData, _targetRT.NormalGlobal, RTP_HEIGHTMAP_SAMPLE_BICUBIC);
									}
									terrainData.SetHeights (0, 0, heights);
								}
							}
						}
						EditorGUI.EndDisabledGroup ();									
					} else if (RTP_TESSELLATION_SAMPLE_TEXTURE) {
						EditorGUILayout.BeginHorizontal ();
						EditorGUILayout.LabelField ("Displacement height", GUILayout.MinWidth (150), GUILayout.MaxWidth (150));
						if (checkChange (ref _target.tessHeight, EditorGUILayout.FloatField (_target.tessHeight))) {
							MeshFilter mf = _targetRT.GetComponent<MeshFilter> ();
							if (mf && mf.sharedMesh) {
								mf.sharedMesh.RecalculateBounds ();
								Bounds bounds = mf.sharedMesh.bounds;
								Vector3 center = bounds.center;
								Vector3 extents = bounds.extents;
								center.y = (_target.tessHeight + _target._TessYOffset * 2) / _targetRT.transform.localScale.y * 0.5f;
								extents.y = _target.tessHeight / _targetRT.transform.localScale.y;
								extents.x *= 2;
								extents.z *= 2;
								mf.sharedMesh.bounds = new Bounds (center, extents);
							}
						}
						EditorGUILayout.EndHorizontal ();
						EditorGUILayout.BeginHorizontal ();
						EditorGUILayout.LabelField ("Y offset", GUILayout.MinWidth (150), GUILayout.MaxWidth (150));
						if (checkChange (ref _target._TessYOffset, EditorGUILayout.FloatField (_target._TessYOffset))) {
							MeshFilter mf = _targetRT.GetComponent<MeshFilter> ();
							if (mf && mf.sharedMesh) {
								mf.sharedMesh.RecalculateBounds ();
								Bounds bounds = mf.sharedMesh.bounds;
								Vector3 center = bounds.center;
								Vector3 extents = bounds.extents;
								center.y = (_target.tessHeight + _target._TessYOffset * 2) / _targetRT.transform.localScale.y * 0.5f;
								extents.y = _target.tessHeight / _targetRT.transform.localScale.y;
								extents.x *= 2;
								extents.z *= 2;
								mf.sharedMesh.bounds = new Bounds (center, extents);
							}
						}
						EditorGUILayout.EndHorizontal ();


					}
					EditorGUILayout.EndVertical ();

					EditorGUILayout.EndHorizontal ();	

				} else {

					if (RTP_NORMALGLOBAL) {
						EditorGUILayout.HelpBox ("Using global normal below we won't use mesh normals - will be treated flat (0,1,0), but you can greately improve look when lo-res heightmap or high pixelError settings are used.", MessageType.None, true);
						if (GUILayout.Button ("Disable global normal (rebuild shaders via RTP_LODmanager)")) {
							_RTP_LODmanagerScript.RTP_NORMALGLOBAL = false;
							if (EditorUtility.DisplayDialog ("", "Go to RTP manager now (to recompile shaders) ?", "Yes", "No")) {
								Selection.activeObject = _RTP_LODmanagerScript.gameObject;
								_RTP_LODmanagerScript.dont_sync = true;
							}
							//_RTP_LODmanagerScript.RefreshLODlevel();
							//EditorUtility.SetDirty(_targetRT);
						}							

						EditorGUILayout.BeginHorizontal ();
						if (checkChange (ref _targetRT.NormalGlobal, (Texture2D)EditorGUILayout.ObjectField (_targetRT.NormalGlobal, typeof(Texture2D), false, GUILayout.MinHeight (100), GUILayout.MinWidth (100), GUILayout.MaxWidth (100)))) {
							AssetImporter _importer = AssetImporter.GetAtPath (AssetDatabase.GetAssetPath (_targetRT.NormalGlobal));
							if (_importer) {
								TextureImporter tex_importer = (TextureImporter)_importer;
								if (tex_importer) {
									tex_importer.wrapMode = TextureWrapMode.Clamp;
									tex_importer.textureType = TextureImporterType.NormalMap;
									Debug.LogWarning ("Global normal texture (" + _targetRT.NormalGlobal.name + ") has been imported as normalmap type.");
									AssetDatabase.ImportAsset (AssetDatabase.GetAssetPath (_targetRT.NormalGlobal), ImportAssetOptions.ForceUpdate);
								}
							}							
						}

					} else {
						EditorGUILayout.HelpBox ("This feature is currently disabled.", MessageType.Warning, true);
						if (GUILayout.Button ("Enable global normal (rebuild shaders via RTP_LODmanager)")) {
							_RTP_LODmanagerScript.RTP_NORMALGLOBAL = true;
							if (EditorUtility.DisplayDialog ("", "Feature is DISABLED in shader until you recompile it.\nGo to RTP manager to do it now ?", "Yes", "No")) {
								Selection.activeObject = _RTP_LODmanagerScript.gameObject;
								_RTP_LODmanagerScript.dont_sync = true;
							}
							//_RTP_LODmanagerScript.RefreshLODlevel();
							//EditorUtility.SetDirty(_targetRT);
						}
					}

				}


				EditorGUILayout.EndVertical ();	
					// presets
				EditorGUILayout.Space ();	
				GUI.color = new Color (1, 1, 0.5f, 1);						
				EditorGUILayout.LabelField ("Presets", EditorStyles.boldLabel);
				GUI.color = skin_color;					
				EditorGUILayout.BeginVertical ("Box");
				if (_targetRT.presetHolders != null) {
					for (int k = 0; k < _targetRT.presetHolders.Length; k++) {
						EditorGUILayout.BeginHorizontal ();
						ReliefTerrainPresetHolder[] holders = null;
							
						if (_targetRT.presetHolders [k] != null) {
							if (GUILayout.Button ("restore", GUILayout.Width (60))) {
								if (EditorUtility.DisplayDialog ("Warning", "Restore to preset " + _targetRT.presetHolders [k].PresetName + " state ?", "Yes", "Cancel")) {
									#if UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2
										Undo.RegisterUndo((ReliefTerrain)target, "Undo relief terrain edit");
									#else
									Undo.RecordObject ((ReliefTerrain)target, "Undo relief terrain edit");
									#endif
									_targetRT.RestorePreset (_targetRT.presetHolders [k]);
									_targetRT.RefreshTextures ();				
									_target.Refresh (null, _targetRT);									
									EditorUtility.SetDirty (_targetRT);
								}
							}
							GUI.color = new Color (1, 0.4f, 0.2f, 1);
							if (GUILayout.Button ("del", GUILayout.Width (35))) {
								if (EditorUtility.DisplayDialog ("Warning", "Remove preset " + _targetRT.presetHolders [k].PresetName + " ?", "Yes", "Cancel")) {
									#if UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2
										Undo.RegisterUndo((ReliefTerrain)target, "Undo relief terrain edit");
									#else
									Undo.RecordObject ((ReliefTerrain)target, "Undo relief terrain edit");
									#endif
									holders = new ReliefTerrainPresetHolder[_targetRT.presetHolders.Length - 1];
									int l = 0;
									for (int r = 0; r < _targetRT.presetHolders.Length; r++) {
										if (r != k) {
											holders [l++] = _targetRT.presetHolders [r];
										}
									}
									_targetRT.presetHolders = holders;
									break;
								}
							}
							GUI.color = new Color (0.5f, 1, 0.5f, 1);
							if (GUILayout.Button ("save", GUILayout.Width (45))) {
								string path = EditorUtility.SaveFilePanel ("Save preset", "Assets", "RTPpreset", "asset");
								if (path != "") {	
									int idx = path.IndexOf ("/Assets/") + 1;
									if (idx > 0) {
										path = path.Substring (idx);
										ReliefTerrainPresetHolder savedPreset = UnityEngine.Object.Instantiate (_targetRT.presetHolders [k]) as ReliefTerrainPresetHolder;
										Debug.Log ("RTP " + _targetRT.presetHolders [k].name + " preset saved at " + path);
										if (AssetDatabase.LoadAssetAtPath (path, typeof(ReliefTerrainPresetHolder)) != null)
											AssetDatabase.DeleteAsset (path);
										AssetDatabase.CreateAsset (savedPreset, path);
										//_targetRT.presetHolders[k].CheckUnsavedTextures();
									} else {
										Debug.Log ("Nothing saved...");
									}
								}
							}
							GUI.color = skin_color;
							EditorGUILayout.LabelField ("Preset " + k + ": ", GUILayout.Width (60));
							EditorGUILayout.LabelField (_targetRT.presetHolders [k].PresetName, EditorStyles.boldLabel);
								
							if (holders != null) {
								_targetRT.presetHolders = holders;
							}
						}
						EditorGUILayout.EndHorizontal ();
					}
				}
				EditorGUI.BeginDisabledGroup (_targetRT.presetHolders != null && _targetRT.presetHolders.Length == 8);

				GUILayout.Label (" 4 Layers presets");
				GUILayout.Space (5);
				EditorGUILayout.BeginHorizontal ();
				GUILayout.Space (5);
				for (int i = 0; i < _targetRT.presets.Length; i++) {
					
					if (_targetRT.presets [i].ln == LayersNumber._4Layers) {
						EditorGUILayout.BeginVertical (GUILayout.Width (70));

						if (GUILayout.Button (_targetRT.presets [i].ico, GUILayout.Width (66), GUILayout.Height (66))) {

							LoadAsset (_targetRT.presets [i].path, _target, _targetRT, terrainComp);
							_targetRT.RefreshAll(_targetRT);
						}
						GUILayout.Label (_targetRT.presets [i].name, EditorStyles.boldLabel);
						EditorGUILayout.EndVertical ();
					}
				}
				EditorGUILayout.EndHorizontal ();
				GUILayout.Space (10);
				GUILayout.Label (" 8 Layers presets");
				GUILayout.Space (5);
				EditorGUILayout.BeginHorizontal ();
				GUILayout.Space (5);
				for (int i = 0; i < _targetRT.presets.Length; i++) {
					if (_targetRT.presets [i].ln == LayersNumber._8Layers) {
						EditorGUILayout.BeginVertical (GUILayout.Width (70));

						if (GUILayout.Button (_targetRT.presets [i].ico, GUILayout.Width (66), GUILayout.Height (66))) {
						
							LoadAsset (_targetRT.presets [i].path, _target, _targetRT, terrainComp);
							_targetRT.RefreshAll(_targetRT);
						}
						GUILayout.Label (_targetRT.presets [i].name, EditorStyles.boldLabel);
						EditorGUILayout.EndVertical ();
					}
				}

				EditorGUILayout.EndHorizontal ();
				EditorGUILayout.BeginHorizontal ();
				EditorGUILayout.LabelField ("New preset name: ", GUILayout.Width (120));	
				_target.newPresetName = EditorGUILayout.TextField (_target.newPresetName);
				if (GUILayout.Button ("+ Add new preset")) {
					#if UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2
						Undo.RegisterUndo((ReliefTerrain)target, "Undo relief terrain edit");
					#else
					Undo.RecordObject ((ReliefTerrain)target, "Undo relief terrain edit");
					#endif
					ReliefTerrainPresetHolder[] holders;
					int k = 0;
					if (_targetRT.presetHolders != null) {
						holders = new ReliefTerrainPresetHolder[_targetRT.presetHolders.Length + 1];
						for (; k < _targetRT.presetHolders.Length; k++) {
							holders [k] = _targetRT.presetHolders [k];
						}
					} else {
						holders = new ReliefTerrainPresetHolder[1];
					}
					holders [k] = ScriptableObject.CreateInstance (typeof(ReliefTerrainPresetHolder)) as ReliefTerrainPresetHolder;
					holders [k].Init (_target.newPresetName);
					_targetRT.SavePreset (ref holders [k]);
					holders [k].type = (terrainComp != null) ? "TERRAIN" : "MESH";
					_targetRT.presetHolders = holders;
				}
				EditorGUILayout.EndHorizontal ();
				EditorGUILayout.BeginHorizontal ();

				p1 = EditorGUILayout.IntField (p1, GUILayout.Width (50));
				p2 = EditorGUILayout.IntField (p2, GUILayout.Width (50));
				if (GUILayout.Button ("Merge two 4LAYERS to 8LAYERS")) {
					ReliefTerrainPresetHolder preset01 = AssetDatabase.LoadAssetAtPath (_targetRT.presets [p1].path, typeof(ReliefTerrainPresetHolder)) as ReliefTerrainPresetHolder;
					ReliefTerrainPresetHolder preset02 = AssetDatabase.LoadAssetAtPath (_targetRT.presets [p2].path, typeof(ReliefTerrainPresetHolder)) as ReliefTerrainPresetHolder;

					ReliefTerrainPresetHolder preset01Copy = UnityEngine.Object.Instantiate (preset01) as ReliefTerrainPresetHolder;
					ReliefTerrainPresetHolder preset02Copy = UnityEngine.Object.Instantiate (preset02) as ReliefTerrainPresetHolder;

					ReliefTerrainPresetHolder mergePreset = ScriptableObject.CreateInstance (typeof(ReliefTerrainPresetHolder)) as ReliefTerrainPresetHolder;
					mergePreset = mergePreset.Merge (preset01Copy.PresetName + preset02Copy.PresetName, preset01Copy, preset02Copy);
					mergePreset.type = "MESH";
					//ReliefTerrainPresetHolder savedPreset=UnityEngine.Object.Instantiate(mergePreset) as ReliefTerrainPresetHolder;
					if (mergePreset != null) {
						string path = EditorUtility.SaveFilePanel ("Load preset", "Assets", preset01Copy.PresetName + preset02Copy.PresetName, "asset");
						int idx = path.IndexOf ("/Assets/") + 1;
						if (idx > 0)
							path = path.Substring (idx);
						Debug.Log (path);
						if (AssetDatabase.LoadAssetAtPath (path, typeof(ReliefTerrainPresetHolder)) != null)
							AssetDatabase.DeleteAsset (path);
						AssetDatabase.CreateAsset (mergePreset, path);
					} else
						Debug.Log ("Merge preset is null");
					AssetDatabase.Refresh ();
					AssetDatabase.SaveAssets ();
				}
				EditorGUILayout.EndHorizontal ();
				if (GUILayout.Button ("Load & restore saved preset")) {
					string path = EditorUtility.OpenFilePanel ("Load preset", "Assets", "asset");
					if (path != "") {
						int idx = path.IndexOf ("/Assets/") + 1;
						if (idx > 0) {
							path = path.Substring (idx);
						}
						Debug.Log (path);
						LoadAsset (path, _target, _targetRT, terrainComp);
						_targetRT.RefreshAll(_targetRT);
							
					}
				}					
									
				EditorGUI.EndDisabledGroup ();
				EditorGUILayout.EndVertical ();						
					// EOF presets
					
				GUILayout.Space (6);
					

				EditorGUILayout.LabelField ("Saving", EditorStyles.boldLabel);
				EditorGUILayout.BeginVertical ("Box");

				GUILayout.Space (2);
				if (GUILayout.Button ("Get Global UV", GUILayout.Height (25))) {
					_targetRT.GetGlobalUV();
				}
				GUILayout.Space (6);
				EditorGUILayout.BeginHorizontal ();
				float width = EditorGUIUtility.currentViewWidth;
				EditorGUILayout.LabelField ("Type:", GUILayout.Width (50));
				_target.bakeType = EditorGUILayout.TextField (_target.bakeType, GUILayout.Width (width / 2 - 100)); 
				EditorGUILayout.LabelField ("Name", GUILayout.Width (50));
				_target.bakeName = EditorGUILayout.TextField (_target.bakeName, GUILayout.Width (width / 2 - 100)); 
				EditorGUILayout.EndHorizontal ();




				isMeshMain = EditorGUILayout.Toggle("Main mesh", isMeshMain);
				isMatMain = EditorGUILayout.Toggle("Main material", isMatMain);
				isMeshAdd = EditorGUILayout.Toggle("Additional meshes", isMeshAdd);
				isMatAdd = EditorGUILayout.Toggle("Additional materoals", isMatAdd);

				GUILayout.Space (6);
				if (GUILayout.Button ("Save Meshes && Materials", GUILayout.Height (25))) {
					//_targetRT.SaveMeshes(isMeshMain, isMatMain, isMeshAdd, isMatAdd);
				}

				GUILayout.Space (4);

				if (GUILayout.Button ("Bake to prefab", GUILayout.Height (25))) {
					
					//_targetRT.BakeAll();
					

				}

				EditorGUILayout.EndVertical ();	


										
					#endregion Settings - main settings
					
				break;
					
									
			case ReliefTerrainSettingsItems.GlobalColor:
					#region Settings - Global color
				_target.paint_wetmask = false;
				GUILayout.Space (6);
					
				Texture2D prev_globalColor = _targetRT.ColorGlobal;

				if (_targetRT.ColorGlobal == null)
					_targetRT.ColorGlobal = _targetRT.controlA;

											
					
				int thumb_size = 32;
					
					
					//
					// paint tools
					//
				GUILayout.Space (8);
				EditorGUILayout.BeginVertical ("Box");		
				skin_color = GUI.color;
				GUI.color = new Color (1, 1, 0.5f, 1);
				EditorGUILayout.LabelField ("Paint tools", EditorStyles.boldLabel);
				GUI.color = skin_color;
					
				bool prev_paint_flag = _target.paint_flag;
					
				if (!_target.paint_flag) {
					Color c = GUI.color;
					GUI.color = new Color (0.9f, 1, 0.9f);
					if (GUILayout.Button (new GUIContent ("Begin painting (M)", icoPaintOn, "Click to turn on painting"))) {
							_target.paint_flag = true;
						_target.paint_wetmask = false;
						//							if (!_target.prepare_tmpTexture(true)) {
						//								_target.paint_flag=false;
						//							}
					}
					if (!_targetRT.GetComponent<Collider> () || !_targetRT.GetComponent<Collider> ().enabled)
						_target.paint_flag = false;
					GUI.color = c;
				} else if (_target.paint_flag) {
					Color c = GUI.color;
					GUI.color = new Color (1, 0.9f, 0.9f);
					if (GUILayout.Button (new GUIContent ("End painting (M)", icoPaintOff, "Click to turn off painting"))) {
						_target.paint_flag = false;

					}
					GUI.color = c;
				}
				if (!prev_paint_flag && _target.paint_flag) {
					UnityEditor.Tools.current = Tool.View;
					ReliefTerrain._SceneGUI = new SceneView.OnSceneFunc (CustomOnSceneGUI);
					SceneView.onSceneGUIDelegate += ReliefTerrain._SceneGUI;
				} else if (prev_paint_flag && !_target.paint_flag) {
					UnityEditor.Tools.current = _targetRT.prev_tool;
					SceneView.onSceneGUIDelegate -= ReliefTerrain._SceneGUI;
				}
				if (prev_paint_flag != _target.paint_flag)
					EditorUtility.SetDirty (target);
				if (_target.paint_flag) {
					if (!_targetRT.GetComponent<Collider> () || !_targetRT.GetComponent<Collider> ().enabled)
						EditorGUILayout.HelpBox ("Object doesn't have collider (necessary for painting).", MessageType.Error, true);
						
					string[] modes;
					modes = new string[2] {"Height", "Layer" };
					_target.cut_holes = false;

					_target.paintMode = (PaintMode)GUILayout.SelectionGrid ((int)_target.paintMode, _targetRT.terrainBrush, modes.Length);
					EditorUtility.SetDirty (target);

					switch (_target.paintMode) {
					case PaintMode.extrude:

						//terrain = (MeshHeight)target as MeshHeight;

						EditorGUILayout.BeginHorizontal();
						GUIContent[] toolbarOptions2 = new GUIContent[4];
						toolbarOptions2[0] = new GUIContent(_targetRT.terrainAdd);
						toolbarOptions2[1] = new GUIContent(_targetRT.terrainCliff);
						toolbarOptions2[2] = new GUIContent(_targetRT.terrainSmooth);
						toolbarOptions2[3] = new GUIContent(_targetRT.terrainRestore);
						_targetRT.typeInt = GUILayout.Toolbar(_targetRT.typeInt, toolbarOptions2);
						EditorGUILayout.EndHorizontal();

						/*
						if (_targetRT.typeInt != 2)
						{
							EditorGUILayout.BeginHorizontal();
							GUIContent[] toolbarOptions = new GUIContent[2];
							toolbarOptions[0] = new GUIContent("Поднять");
							toolbarOptions[1] = new GUIContent("Опустить");
							_targetRT.editInt = GUILayout.Toolbar(_targetRT.editInt, toolbarOptions);
							EditorGUILayout.EndHorizontal();
						}


						EditorGUILayout.BeginHorizontal();
						GUILayout.Label("Сила: " + Mathf.Floor(_targetRT.brushValue * 100) / 100, GUILayout.Width(120));
						_targetRT.brushValue = GUILayout.HorizontalSlider(_targetRT.brushValue, 0.01f, 0.2f);
						GUILayout.Space(20);
						EditorGUILayout.EndHorizontal();
						EditorGUILayout.BeginHorizontal();
						GUILayout.Label("Радиус: " + _targetRT.brushRadius, GUILayout.Width(120));
						_targetRT.brushRadius = GUILayout.HorizontalSlider(_targetRT.brushRadius, 0.1f, 99f);
						GUILayout.Space(20);
						EditorGUILayout.EndHorizontal();
						if (_targetRT.typeInt == 1)
						{
							
						}
						*/


						break;
					
					case PaintMode.layer:
						{
							EditorGUILayout.BeginVertical ("Box");
							skin_color = GUI.color;
							GUI.color = new Color (1, 1, 0.5f, 1);
							//if (_target.paintColorSwatches!=null && _target.paintColorSwatches.Length>0) EditorGUILayout.LabelField("Color swatches", EditorStyles.boldLabel);
							GUI.color = skin_color;
							GUISkin gs = EditorGUIUtility.GetBuiltinSkin (EditorSkin.Inspector);
							RectOffset ro1 = gs.label.padding;
							RectOffset ro2 = gs.label.margin;
							gs.label.padding = new  RectOffset (0, 0, 0, 0);
							gs.label.margin = new  RectOffset (3, 3, 3, 3);
						

							int per_row = Mathf.Max (8, (Screen.width) / thumb_size - 1);
							thumb_size = 50;//(Screen.width-50-2*per_row)/per_row;
							Color ccol = GUI.contentColor;
							for (int n = 0; n < _target.numLayers; n++) {
								if ((n % per_row) == 0)
									EditorGUILayout.BeginHorizontal ();
								Color bcol = GUI.backgroundColor;
								if (n == _target.selectedLayer) {
									GUI.contentColor = new Color (1, 1, 1, 1);
									GUI.backgroundColor = new Color (1, 1, 0, 1);
									EditorGUILayout.BeginHorizontal ("Box");
									if (_target.splats [n]) {
										#if !UNITY_3_5
										GUILayout.Label ((Texture2D)AssetPreview.GetAssetPreview (_target.splats [n]), GUILayout.Width (thumb_size - 8), GUILayout.Height (thumb_size - 8));
										#else
									GUILayout.Label((Texture2D)EditorUtility.GetAssetPreview(_target.splats[n]), GUILayout.Width(thumb_size-8), GUILayout.Height(thumb_size-8));
										#endif
									} else {
										GUILayout.Label (" ", GUILayout.Width (thumb_size - 8), GUILayout.Height (thumb_size - 8));
									}
								} else {
									GUI.contentColor = new Color (1, 1, 1, 0.5f);
									if (_target.splats [n]) {
										#if !UNITY_3_5
										if (GUILayout.Button ((Texture2D)AssetPreview.GetAssetPreview (_target.splats [n]), "Label", GUILayout.Width (thumb_size), GUILayout.Height (thumb_size))) {
											#else
									if (GUILayout.Button((Texture2D)EditorUtility.GetAssetPreview(_target.splats[n]), "Label", GUILayout.Width(thumb_size), GUILayout.Height(thumb_size))) {
											#endif
											_target.selectedLayer = n;
										}
									} else {
										if (GUILayout.Button (" ", "Label", GUILayout.Width (thumb_size), GUILayout.Height (thumb_size))) {
											_target.selectedLayer = n;
										}
									}
								}
								if (n == _target.selectedLayer) {
									EditorGUILayout.EndHorizontal ();
									GUI.backgroundColor = bcol;
								}
								if ((n % per_row) == (per_row - 1) || n == _target.numLayers - 1)
									EditorGUILayout.EndHorizontal ();
							}


							EditorGUILayout.EndVertical ();

							GUILayout.Label ("Opacity", EditorStyles.label);
							_target.colorOpacity = EditorGUILayout.Slider (_target.colorOpacity, 0.0f, 1.0f);
							float other = 1.0f - _target.colorOpacity;
							switch (_target.selectedLayer) {
							case 0:
								_target.paintColor = new Color (_target.colorOpacity, other, other, other);
								_target.isFirst = true;
								break;
							case 1:
								_target.paintColor = new Color (other, _target.colorOpacity, other, other);
								_target.isFirst = true;
								break;
							case 2:
								_target.paintColor = new Color (other, other, _target.colorOpacity, other);
								_target.isFirst = true;
								break;
							case 3:
								_target.paintColor = new Color (other, other, other, _target.colorOpacity);
								_target.isFirst = true;
								break;
							case 4:
								_target.paintColor = new Color (_target.colorOpacity, other, other, other);
								_target.isFirst = false;
								break;
							case 5:
								_target.paintColor = new Color (other, _target.colorOpacity, other, other);
								_target.isFirst = false;
								break;
							case 6:
								_target.paintColor = new Color (other, other, _target.colorOpacity, other);
								_target.isFirst = false;
								break;
							case 7:
								_target.isFirst = false;
								_target.paintColor = new Color (other, other, other, _target.colorOpacity);
								break;
							}
						}
						break;
					}
					skin_color = GUI.color;
					GUI.color = new Color (1, 1, 0.5f, 1);
					EditorGUILayout.LabelField ("Layer properties", EditorStyles.boldLabel);
					GUI.color = skin_color;

					Texture2D tmp_tex = _targetRT.tmp_globalColorMap;
					if (tmp_tex && (tmp_tex.format != TextureFormat.Alpha8 && tmp_tex.format != TextureFormat.ARGB32)) 
					{
						EditorGUILayout.HelpBox ("Global colormap need to be readable and uncompressed for painting.", MessageType.Error, true);
					}

					if (_target.paintMode == PaintMode.layer)
					{
						GUILayout.BeginHorizontal ();
						GUILayout.Label ("Area size", EditorStyles.label);
						_target.paint_size = EditorGUILayout.Slider (_target.paint_size, 0.1f, 20);
						GUILayout.EndHorizontal ();	

						GUILayout.BeginHorizontal ();
						GUILayout.Label ("Area smoothness", EditorStyles.label);
						_target.paint_smoothness = EditorGUILayout.Slider (_target.paint_smoothness, 0.001f, 1);
						GUILayout.EndHorizontal ();
					}
					else
					{
						GUILayout.BeginHorizontal ();
						GUILayout.Label ("Area size", EditorStyles.label);
						_targetRT.brushSize = EditorGUILayout.Slider (_targetRT.brushSize, 0.1f, 20);
						GUILayout.EndHorizontal ();	
					}
					GUILayout.BeginHorizontal ();
					GUILayout.Label ("Opacity", EditorStyles.label);
					_target.paint_opacity = EditorGUILayout.Slider (_target.paint_opacity, 0, 1);
					GUILayout.EndHorizontal ();	

					if (_target.paintMode == PaintMode.extrude)
					{
						if (_targetRT.typeInt == 1)
						{
							GUILayout.BeginHorizontal();
							GUILayout.Label("Height: ", GUILayout.Width(120));
							string s = GUILayout.TextField(_targetRT.tgtHeight.ToString(), GUILayout.Width(120));
							float.TryParse(s, out _targetRT.tgtHeight);
							GUILayout.EndHorizontal();
						}

						if (_targetRT.typeInt == 2)
						{
							GUILayout.BeginHorizontal ();
							GUILayout.Label ("Radius", EditorStyles.label);
							_targetRT.brushRadius = EditorGUILayout.Slider (_targetRT.brushRadius, 0.1f, 30);
							GUILayout.EndHorizontal ();	
						}

						GUILayout.Space (10);
						if (GUILayout.Button("Restore Mesh"))
						{
							_targetRT.RestoreMesh();

						}

						if (GUILayout.Button("Save Mesh"))
						{
							_targetRT.SaveMesh();

						}
					}
					else
					{
						GUILayout.Space (10);
					}

				}
				EditorGUILayout.EndVertical ();	
					

					
					
					
					#endregion Settings - Global color
				break;
							
					
							
					
			}
				
				

			GUILayout.Space (8);
			//Begin_Global_Indent();
				
				
			#endregion ALLSettings
		}
			
		if (dirtyFlag) {
			_target.SyncGlobalPropsAcrossTerrainGroups ();
			EditorUtility.SetDirty (_targetRT);
			_targetRT.RefreshTextures ();				
			_target.Refresh (null, _targetRT);
		}
			
		GUILayout.Space (10);
		//DrawDefaultInspector();
		if (_target.activateObject) {
			Selection.activeObject = _target.activateObject;
			_target.activateObject = null;
		}
			
			
		Event current = Event.current;
		switch (current.type) {
		case EventType.keyDown:
			if (current.keyCode == KeyCode.M) {
				_target.paint_flag = !_target.paint_flag;
				if (!_targetRT.GetComponent<Collider> () || !_targetRT.GetComponent<Collider> ().enabled)
					_target.paint_flag = false;
				if (_target.paint_flag) {
					if (_target.submenu != ReliefTerrainMenuItems.GeneralSettings || (_target.submenu_settings != ReliefTerrainSettingsItems.GlobalColor)) {
						_target.paint_flag = false;
					}
					if (_target.submenu == ReliefTerrainMenuItems.GeneralSettings) {
							
						//							if (_target.submenu_settings==ReliefTerrainSettingsItems.GlobalColor) {
						//								if (!_target.prepare_tmpTexture(true)) {
						//									_target.paint_flag=false;
						//								}
						//							}							
					}
				}					
				if (_target.paint_flag) {
					UnityEditor.Tools.current = Tool.View;
					ReliefTerrain._SceneGUI = new SceneView.OnSceneFunc (CustomOnSceneGUI);
					SceneView.onSceneGUIDelegate += ReliefTerrain._SceneGUI;
				} else {
					UnityEditor.Tools.current = _targetRT.prev_tool;
					SceneView.onSceneGUIDelegate -= ReliefTerrain._SceneGUI;
				}
				EditorUtility.SetDirty (target);
			}
			break;
		}			
	}

	public static void CreateOrReplaceAsset<T> (T asset, string path) where T:UnityEngine.Object
	{
		string oldPath = AssetDatabase.GetAssetPath (asset);
		//T existingAsset = AssetDatabase.LoadAssetAtPath<T>(oldPath);

		if (!File.Exists (oldPath)) {
			AssetDatabase.CreateAsset (asset, path);

		} else {
			//PrefabUtility.DisconnectPrefabInstance (asset);
			//AssetDatabase.CreateAsset(asset, path);
			AssetDatabase.MoveAsset (oldPath, path);
			EditorUtility.SetDirty (asset);

		}
	}



	private void MakeCombinedGrayscale (int beg, ref Texture2D SSColorCombined)
	{
		ReliefTerrain _targetRT = (ReliefTerrain)target;
		ReliefTerrainGlobalSettingsHolder _target = _targetRT.globalSettingsHolder;
			
		for (int n = beg; n < _target.numLayers; n++) {
			int min_size = 9999;
			for (int m = (n / 4) * 4; (m < ((n / 4) * 4 + 4)) && (m < _target.numLayers); m++) {
				if (_target.splats [m]) { // czasem może byc błędnie wprowadzony jako null
					if (_target.splats [m].width < min_size)
						min_size = _target.splats [m].width;
				}
			}		
			AssetImporter _importer = AssetImporter.GetAtPath (AssetDatabase.GetAssetPath (_target.splats [n]));
			if (_importer) {
				TextureImporter tex_importer = (TextureImporter)_importer;
				bool reimport_flag = false;
				if (!tex_importer.isReadable) {
					Debug.LogWarning ("Detail texture " + n + " (" + _target.splats [n].name + ") has been reimported as readable.");
					tex_importer.isReadable = true;
					reimport_flag = true;
				}
				if (_target.splats [n] && _target.splats [n].width > min_size) {
					Debug.LogWarning ("Detail texture " + n + " (" + _target.splats [n].name + ") has been reimported with " + min_size + " size.");
					tex_importer.maxTextureSize = min_size;
					reimport_flag = true;
				}
				if (reimport_flag) {
					AssetDatabase.ImportAsset (AssetDatabase.GetAssetPath (_target.splats [n]), ImportAssetOptions.ForceUpdate);
				}
			}					
		}				
			
		int i;
		for (i = 0; i < _target.numLayers; i++) {
			try { 
				_target.splats [i].GetPixels (0, 0, 4, 4, 0);
			} catch (Exception e) {
				Debug.LogError ("Splat texture " + i + " has to be marked as isReadable...");
				Debug.LogError (e.Message);
				return;
			}
		}
		int w = _target.splats [0].width;
		for (i = 1; i < _target.numLayers; i++) {
			if (_target.splats [i].width != w) {
				Debug.LogError ("For performance reasons - all splats (detail textures) should have the same size");
				Debug.LogError ("Detail tex 0 size=" + w + " while detail tex " + i + " size=" + _target.splats [i].width);
				return;
			}
		}
			
		int num = _target.numLayers <= 8 ? _target.numLayers : 8;
		Texture2D[] splats = new Texture2D[4];
		for (i = 0; i < splats.Length; i++) {
			if (i < 4) {
				if (i < num) {
					splats [i] = _target.splats [i + beg];
				} else {
					splats [i] = new Texture2D (_target.splats [num - 1].width, _target.splats [num - 1].width);
				}
			}
		}
		Color[] tex0 = splats [0].GetPixels (0);
		Color[] tex1 = splats [1].GetPixels (0);
		Color[] tex2 = splats [2].GetPixels (0);
		Color[] tex3 = splats [3].GetPixels (0);
		Color[] result = new Color[tex0.Length];
		for (i = 0; i < tex0.Length; i++) {
			result [i] = new Color (tex0 [i].grayscale, tex1 [i].grayscale, tex2 [i].grayscale, tex3 [i].grayscale);
		}
		SSColorCombined = new Texture2D (splats [0].width, splats [0].height, TextureFormat.ARGB32, true, true);
		SSColorCombined.SetPixels (result);
		SSColorCombined.Apply (true, false);
		SSColorCombined.Compress (true);
	}





	private void BlendMip (Texture2D atlas, int mip, float blend, int pad_offset = 0)
	{
		Color32[] cols = atlas.GetPixels32 (mip);
		int size = Mathf.FloorToInt (Mathf.Sqrt ((float)cols.Length));
		int half_size = size / 2;
		int idx_offA, idx_offB;
		idx_offA = pad_offset * size;
		idx_offB = (half_size - 1 - pad_offset) * size;
		for (int i = 0; i < size; i++) {
			Color32 colA = cols [idx_offA];
			Color32 colB = cols [idx_offB];
			cols [idx_offA] = Color32.Lerp (colA, colB, blend);
			cols [idx_offB] = Color32.Lerp (colB, colA, blend);
			idx_offA++;
			idx_offB++;
		}
		idx_offA = (half_size + pad_offset) * size;
		idx_offB = (size - 1 - pad_offset) * size;
		for (int i = 0; i < size; i++) {
			Color32 colA = cols [idx_offA];
			Color32 colB = cols [idx_offB];
			cols [idx_offA] = Color32.Lerp (colA, colB, blend);
			cols [idx_offB] = Color32.Lerp (colB, colA, blend);
			idx_offA++;
			idx_offB++;
		}		
		idx_offA = pad_offset;
		idx_offB = half_size - 1 - pad_offset;
		for (int i = 0; i < size; i++) {
			Color32 colA = cols [idx_offA];
			Color32 colB = cols [idx_offB];
			cols [idx_offA] = Color32.Lerp (colA, colB, blend);
			cols [idx_offB] = Color32.Lerp (colB, colA, blend);
			idx_offA += size;
			idx_offB += size;
		}
		idx_offA = half_size + pad_offset;
		idx_offB = size - 1 - pad_offset;
		for (int i = 0; i < size; i++) {
			Color32 colA = cols [idx_offA];
			Color32 colB = cols [idx_offB];
			cols [idx_offA] = Color32.Lerp (colA, colB, blend);
			cols [idx_offB] = Color32.Lerp (colB, colA, blend);
			idx_offA += size;
			idx_offB += size;
		}
		atlas.SetPixels32 (cols, mip);
	}

	public void colorAlphaFromSplat (int splat_control_tex_idx)
	{
		ReliefTerrain _target = (ReliefTerrain)target;
		Color[] cols;
			
		if (!_target.ColorGlobal)
			return;
		_target.prepare_tmpTexture (true);
		if (!_target.ColorGlobal)
			return;
		if ((_target.ColorGlobal.format != TextureFormat.Alpha8) && (_target.ColorGlobal.format != TextureFormat.ARGB32)) {
			try { 
				_target.ColorGlobal.GetPixels (0, 0, 4, 4, 0);
			} catch (Exception e) {
				Debug.LogError ("Global ColorMap has to be marked as isReadable...");
				Debug.LogError (e.Message);
			}
			Texture2D tmp_globalColorMap;
			if (_target.ColorGlobal.format == TextureFormat.Alpha8) {
				tmp_globalColorMap = new Texture2D (_target.ColorGlobal.width, _target.ColorGlobal.height, TextureFormat.Alpha8, true); 
			} else {
				tmp_globalColorMap = new Texture2D (_target.ColorGlobal.width, _target.ColorGlobal.height, TextureFormat.ARGB32, true); 
			}
			cols = _target.ColorGlobal.GetPixels ();
			_target.ColorGlobal = tmp_globalColorMap;
		} else {
			cols = _target.ColorGlobal.GetPixels ();
		}
			
		if (splat_control_tex_idx < _target.globalSettingsHolder.numLayers) {
				
			Texture2D splatA = _target.controlA;
			Texture2D splatB = (_target.globalSettingsHolder.numLayers <= 8) ? _target.controlB : _target.controlC;
			float[,] blured_sum = new float[_target.ColorGlobal.width, _target.ColorGlobal.height];
			int idx;
				
			if (splatA.width != _target.ColorGlobal.width || splatA.height != _target.ColorGlobal.height) {
				ResamplingService resamplingService = new ResamplingService ();
				resamplingService.Filter = ResamplingFilters.Quadratic;
				ushort[][,] input = _target.ConvertTextureToArray ((Texture2D)splatA);
				ushort[][,] output = resamplingService.Resample (input, _target.ColorGlobal.width, _target.ColorGlobal.height);
				for (int i = 0; i < _target.ColorGlobal.width; i++) {
					for (int j = 0; j < _target.ColorGlobal.height; j++) {
						blured_sum [i, j] = (output [0] [i, j] + output [1] [i, j] + output [2] [i, j] + output [3] [i, j]) / 255.0f;
					}
				}
			} else {
				Color32[] cols_splat = splatA.GetPixels32 ();
				idx = 0;
				for (int i = 0; i < _target.ColorGlobal.width; i++) {
					for (int j = 0; j < _target.ColorGlobal.height; j++) {
						blured_sum [i, j] = (cols_splat [idx].r + cols_splat [idx].g + cols_splat [idx].b + cols_splat [idx].a) / 255.0f;
						idx++;
					}
				}
			}
			if (splatB.width != _target.ColorGlobal.width || splatB.height != _target.ColorGlobal.height) {
				ResamplingService resamplingService = new ResamplingService ();
				resamplingService.Filter = ResamplingFilters.Quadratic;
				ushort[][,] input = _target.ConvertTextureToArray ((Texture2D)splatB);
				ushort[][,] output = resamplingService.Resample (input, _target.ColorGlobal.width, _target.ColorGlobal.height);
				for (int i = 0; i < _target.ColorGlobal.width; i++) {
					for (int j = 0; j < _target.ColorGlobal.height; j++) {
						blured_sum [i, j] = Mathf.Max (blured_sum [i, j], (output [0] [i, j] + output [1] [i, j] + output [2] [i, j] + output [3] [i, j]) / 255.0f);
					}
				}
			} else {
				Color32[] cols_splat = splatB.GetPixels32 ();
				idx = 0;
				for (int i = 0; i < _target.ColorGlobal.width; i++) {
					for (int j = 0; j < _target.ColorGlobal.height; j++) {
						blured_sum [i, j] = Mathf.Max (blured_sum [i, j], (cols_splat [idx].r + cols_splat [idx].g + cols_splat [idx].b + cols_splat [idx].a) / 255.0f);
						idx++;
					}
				}
			}
			for (int i = 0; i < _target.ColorGlobal.width; i++) {
				for (int j = 0; j < _target.ColorGlobal.height; j++) {
					if (blured_sum [i, j] > 1)
						blured_sum [i, j] = 1;
					else
						blured_sum [i, j] = blured_sum [i, j];
				}
			}
				
			float[,] blured_sum2 = new float[_target.ColorGlobal.width, _target.ColorGlobal.height];
			float[,] tmp_blured_sum;
			for (int blur_cnt = 0; blur_cnt < 4; blur_cnt++) {
				for (int i = 1; i < _target.ColorGlobal.width - 1; i++) {
					for (int j = 1; j < _target.ColorGlobal.height - 1; j++) {
						blured_sum2 [i, j] = 0.7f * (blured_sum [i + 1, j] + blured_sum [i - 1, j] + blured_sum [i, j + 1] + blured_sum [i, j - 1]);
						blured_sum2 [i, j] += 0.3f * (blured_sum [i + 1, j + 1] + blured_sum [i + 1, j - 1] + blured_sum [i - 1, j + 1] + blured_sum [i - 1, j - 1]);
						blured_sum2 [i, j] *= 0.25f;
					}
				}
				tmp_blured_sum = blured_sum;
				blured_sum = blured_sum2;
				blured_sum2 = tmp_blured_sum;
			}
			idx = 0;
			for (int i = 0; i < _target.ColorGlobal.width; i++) {
				for (int j = 0; j < _target.ColorGlobal.height; j++) {
					cols [idx].a = blured_sum [i, j];
					cols [idx].a *= cols [idx].a;
					cols [idx].a *= cols [idx].a;
					if (cols [idx].a < 0.008f)
						cols [idx].a = 0.008f;
					idx++;
				}
			}
				
			_target.ColorGlobal.SetPixels (cols);
			_target.ColorGlobal.Apply (true, false);
		}
	}

	private bool checkChange (ref ProceduralMaterial val, ProceduralMaterial nval)
	{
		bool changed = (nval != val);
		if (changed) {
			if ((EditorApplication.timeSinceStartup - changedTime) > 0.5) {
				changedTime = EditorApplication.timeSinceStartup;
#if UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2
					Undo.RegisterUndo((ReliefTerrain)target, "Undo relief terrain edit");
#else
				Undo.RecordObject ((ReliefTerrain)target, "Undo relief terrain edit");
#endif
			}
		}
		dirtyFlag = dirtyFlag || changed;
		val = nval;
		return changed;
	}

	private bool checkChange (ref float val, float nval)
	{
		bool changed = (nval != val);
		if (changed) {
			if ((EditorApplication.timeSinceStartup - changedTime) > 0.5) {
				changedTime = EditorApplication.timeSinceStartup;
#if UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2
					Undo.RegisterUndo((ReliefTerrain)target, "Undo relief terrain edit");
#else
				Undo.RecordObject ((ReliefTerrain)target, "Undo relief terrain edit");
#endif
			}
		}
		dirtyFlag = dirtyFlag || changed;
		val = nval;
		return changed;
	}

	private bool checkChange (ref int val, int nval)
	{
		bool changed = (nval != val);
		if (changed) {
			if ((EditorApplication.timeSinceStartup - changedTime) > 0.5) {
				changedTime = EditorApplication.timeSinceStartup;
#if UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2
					Undo.RegisterUndo((ReliefTerrain)target, "Undo relief terrain edit");
#else
				Undo.RecordObject ((ReliefTerrain)target, "Undo relief terrain edit");
#endif
			}
		}
		dirtyFlag = dirtyFlag || changed;
		val = nval;
		return changed;
	}

	private bool checkChange (ref Texture2D val, Texture2D nval)
	{
		bool changed = (nval != val);
		if (changed) {
			if ((EditorApplication.timeSinceStartup - changedTime) > 0.5) {
				changedTime = EditorApplication.timeSinceStartup;
#if UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2
					Undo.RegisterUndo((ReliefTerrain)target, "Undo relief terrain edit");
#else
				Undo.RecordObject ((ReliefTerrain)target, "Undo relief terrain edit");
#endif
			}
		}
		dirtyFlag = dirtyFlag || changed;
		val = nval;
		return changed;
	}

	private bool checkChange (ref Cubemap val, Cubemap nval)
	{
		bool changed = (nval != val);
		if (changed) {
			if ((EditorApplication.timeSinceStartup - changedTime) > 0.5) {
				changedTime = EditorApplication.timeSinceStartup;
#if UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2
					Undo.RegisterUndo((ReliefTerrain)target, "Undo relief terrain edit");
#else
				Undo.RecordObject ((ReliefTerrain)target, "Undo relief terrain edit");
#endif
			}
		}
		dirtyFlag = dirtyFlag || changed;
		val = nval;
		return changed;
	}

	private bool checkChange (ref Color val, Color nval)
	{
		bool changed = (nval != val);
		if (changed) {
			if ((EditorApplication.timeSinceStartup - changedTime) > 0.5) {
				changedTime = EditorApplication.timeSinceStartup;
#if UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2
					Undo.RegisterUndo((ReliefTerrain)target, "Undo relief terrain edit");
#else
				Undo.RecordObject ((ReliefTerrain)target, "Undo relief terrain edit");
#endif
			}
		}
		dirtyFlag = dirtyFlag || changed;
		val = nval;
		return changed;
	}

	private bool checkChange (ref RTPColorChannels val, RTPColorChannels nval)
	{
		bool changed = (nval != val);
		if (changed) {
			if ((EditorApplication.timeSinceStartup - changedTime) > 0.5) {
				changedTime = EditorApplication.timeSinceStartup;
#if UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2
					Undo.RegisterUndo((ReliefTerrain)target, "Undo relief terrain edit");
#else
				Undo.RecordObject ((ReliefTerrain)target, "Undo relief terrain edit");
#endif
			}
		}
		dirtyFlag = dirtyFlag || changed;
		val = nval;
		return changed;
	}

	private bool checkChange (ref bool val, bool nval)
	{
		bool changed = (nval != val);
		if (changed) {
			if ((EditorApplication.timeSinceStartup - changedTime) > 0.5) {
				changedTime = EditorApplication.timeSinceStartup;
#if UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2
					Undo.RegisterUndo((ReliefTerrain)target, "Undo relief terrain edit");
#else
				Undo.RecordObject ((ReliefTerrain)target, "Undo relief terrain edit");
#endif
			}
		}
		dirtyFlag = dirtyFlag || changed;
		val = nval;
		return changed;
	}

	private bool checkChange (ref GameObject val, GameObject nval)
	{
		bool changed = (nval != val);
		if (changed) {
			if ((EditorApplication.timeSinceStartup - changedTime) > 0.5) {
				changedTime = EditorApplication.timeSinceStartup;
#if UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2
					Undo.RegisterUndo((ReliefTerrain)target, "Undo relief terrain edit");
#else
				Undo.RecordObject ((ReliefTerrain)target, "Undo relief terrain edit");
#endif
			}
		}
		dirtyFlag = dirtyFlag || changed;
		val = nval;
		return changed;
	}

	private bool checkChange (ref float valA, float nvalA, ref float valB, float nvalB)
	{
		bool changed = (nvalA != valA) || (nvalB != valB);
		if (changed) {
			if ((EditorApplication.timeSinceStartup - changedTime) > 0.5) {
				changedTime = EditorApplication.timeSinceStartup;
#if UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2
					Undo.RegisterUndo((ReliefTerrain)target, "Undo relief terrain edit");
#else
				Undo.RecordObject ((ReliefTerrain)target, "Undo relief terrain edit");
#endif
			}
		}
		dirtyFlag = dirtyFlag || changed;
		valA = nvalA;
		valB = nvalB;
		return changed;
	}

	#if !UNITY_WEBGL || UNITY_EDITOR
	private Texture2D GetSubstanceTex (string gen_name, string shad_name, int n, bool normal_flag = false)
	{
		Texture2D ntex;
		ReliefTerrain _targetRT = (ReliefTerrain)target;
		ReliefTerrainGlobalSettingsHolder _target = _targetRT.globalSettingsHolder;
#if UNITY_4_1 || UNITY_4_2 || UNITY_4_3  || UNITY_4_4 || UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9 || UNITY_5_0 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6									
		Texture[] gen_texs = _target.Substances [n].GetGeneratedTextures ();
		ProceduralTexture ptex = (ProceduralTexture)gen_texs [0];
		for (int tidx = 1; tidx < gen_texs.Length; tidx++) {
			if (gen_texs [tidx].name.IndexOf (gen_name) >= 0) {
				ptex = (ProceduralTexture)gen_texs [tidx];
				break;
			}
		}
		Color32[] cols = ptex.GetPixels32 (0, 0, ptex.width, ptex.height);
		if (normal_flag) {
			ntex = new Texture2D (ptex.width, ptex.height, TextureFormat.RGB24, false);
			for (int i = 0; i < cols.Length; i++) {
				int cz = Mathf.FloorToInt (255 - Mathf.Sqrt (cols [i].g * cols [i].g + cols [i].a * cols [i].a));
				if (cz < 0)
					cz = 0;
				else if (cz > 255)
					cz = 255;
				cols [i] = new Color32 (cols [i].a, cols [i].g, (byte)cz, 0);
			}
		} else {
			ntex = new Texture2D (ptex.width, ptex.height, TextureFormat.ARGB32, false);
		}
		ntex.SetPixels32 (cols);
		ntex.Apply (true, false);
#else
			ProceduralMaterial pm=_target.Substances[n];
			int _sizex=1<<(int)(pm.GetProceduralVector("$outputsize").x);
			int _sizey=1<<(int)(pm.GetProceduralVector("$outputsize").y);
			RenderTexture rt = RenderTexture.GetTemporary(_sizex, _sizey, 32);
			Graphics.SetRenderTarget(rt);
			Graphics.Blit(pm.GetTexture(shad_name), rt);
			Texture2D tmp_tex = new Texture2D(_sizex, _sizey);
			tmp_tex.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
			Color32[] cols=tmp_tex.GetPixels32();
			DestroyImmediate(tmp_tex); tmp_tex=null;
			if (normal_flag) {
				ntex = new Texture2D(_sizex, _sizey, TextureFormat.RGB24, false);
				for(int i=0; i<cols.Length; i++) {
					int cz=Mathf.FloorToInt(255-Mathf.Sqrt(cols[i].g*cols[i].g + cols[i].a*cols[i].a));
					if (cz<0) cz=0; else if (cz>255) cz=255;
					cols[i]=new Color32(cols[i].a, cols[i].g, (byte)cz, 0);
				}
			} else {
				ntex = new Texture2D(_sizex, _sizey, TextureFormat.ARGB32, false);
			}
			ntex.SetPixels32(cols);
			ntex.Apply(true,false);
			Graphics.SetRenderTarget(null);
			RenderTexture.ReleaseTemporary(rt);
#endif
			
		return ntex;
	}	
	#endif






	public bool SaveTexture (ref Texture2D tex, ref string save_path, string default_name, int buttonwidth, TextureImporterFormat textureFormat, bool mipmapEnabled, bool button_triggered = true, bool sRGB_flag = false)
	{
		//		if (tex==null) return;			
		//		if (AssetDatabase.GetAssetPath(tex)!="") return;
		EditorGUI.BeginDisabledGroup (tex == null || AssetDatabase.GetAssetPath (tex) != "");
		bool saved = false;
		bool cond;
		if (button_triggered) {
			cond = GUILayout.Button ("Save to file", GUILayout.MaxWidth (buttonwidth));
		} else {
			cond = true;
		}
		if (cond) {
			string directory;
			string file;
			if (save_path == "") {
				directory = Application.dataPath;
				file = default_name;
			} else {
				directory = Path.GetDirectoryName (save_path);
				file = Path.GetFileNameWithoutExtension (save_path) + ".png";
			}
			string path = EditorUtility.SaveFilePanel ("Save texture", directory, file, "png");
			if (path != "") {
				path = path.Substring (Application.dataPath.Length - 6);
				save_path = path;
				// kopia, bo mozemy miec skompresowana texture
				Texture2D ntex = new Texture2D (tex.width, tex.height, TextureFormat.ARGB32, true);
				for (int mip = 0; mip < tex.mipmapCount; mip++) {
					Color32[] cols = tex.GetPixels32 (mip);
					ntex.SetPixels32 (cols, mip);
					ntex.Apply (false, false);
				}					
				byte[] bytes = ntex.EncodeToPNG ();
				ntex = null;
				System.IO.File.WriteAllBytes (path, bytes);
				AssetDatabase.Refresh (ImportAssetOptions.ForceUpdate);
				TextureImporter textureImporter = AssetImporter.GetAtPath (path) as TextureImporter;
				textureImporter.textureFormat = textureFormat; 
				textureImporter.mipmapEnabled = mipmapEnabled; 
				textureImporter.linearTexture = sRGB_flag;					
				AssetDatabase.ImportAsset (path, ImportAssetOptions.ForceUpdate);
				tex = (Texture2D)AssetDatabase.LoadAssetAtPath (path, typeof(Texture2D));
				saved = true;
			}		
		}
		EditorGUI.EndDisabledGroup ();
		return saved;
	}

	public void OnSceneGUI ()
	{
		ReliefTerrain _targetRT = (ReliefTerrain)target;
		ReliefTerrainGlobalSettingsHolder _target = _targetRT.globalSettingsHolder;
			
		Event current = Event.current;
			
		if (Event.current.type == EventType.keyDown) {
			if (Event.current.keyCode == KeyCode.M) {
				_target.paint_flag = !_target.paint_flag;
				if (!_targetRT.GetComponent<Collider> () || !_targetRT.GetComponent<Collider> ().enabled)
					_target.paint_flag = false;
				if (_target.paint_flag) {
					if (_target.submenu != ReliefTerrainMenuItems.GeneralSettings || (_target.submenu_settings != ReliefTerrainSettingsItems.GlobalColor)) {
						_target.paint_flag = false;
					}
					if (_target.submenu == ReliefTerrainMenuItems.GeneralSettings) {
						
					}
				}						
				if (_target.paint_flag) {
					UnityEditor.Tools.current = Tool.View;
					ReliefTerrain._SceneGUI = new SceneView.OnSceneFunc (CustomOnSceneGUI);
					SceneView.onSceneGUIDelegate += ReliefTerrain._SceneGUI;
				} else {
					UnityEditor.Tools.current = _targetRT.prev_tool;
					SceneView.onSceneGUIDelegate -= ReliefTerrain._SceneGUI;					
				}
				EditorUtility.SetDirty (target);
			}
		}
			
		if (Event.current.type == EventType.keyDown && Event.current.keyCode == KeyCode.L) {
			current.Use ();
			if (_targetRT.GetComponent<Collider> ()) {
				Ray ray = HandleUtility.GUIPointToWorldRay (Event.current.mousePosition);
				_targetRT.paintHitInfo_flag = _targetRT.GetComponent<Collider> ().Raycast (ray, out _targetRT.paintHitInfo, Mathf.Infinity);
				if (_targetRT.paintHitInfo_flag) {
					int max_idx = 0;
					float max_val = 0;

					if (_targetRT.controlA) {
						Color c = _targetRT.controlA.GetPixelBilinear (_targetRT.paintHitInfo.textureCoord.x, _targetRT.paintHitInfo.textureCoord.y);
						if (c.r > max_val) {
							max_val = c.r;
							max_idx = 0;
						}
						if (c.g > max_val) {
							max_val = c.g;
							max_idx = 1;
						}
						if (c.b > max_val) {
							max_val = c.b;
							max_idx = 2;
						}
						if (c.a > max_val) {
							max_val = c.a;
							max_idx = 3;
						}
					}
					if (_target.numLayers > 4 && _targetRT.controlB) {
						Color c = _targetRT.controlB.GetPixelBilinear (_targetRT.paintHitInfo.textureCoord.x, _targetRT.paintHitInfo.textureCoord.y);
						if (c.r > max_val) {
							max_val = c.r;
							max_idx = 4;
						}
						if (c.g > max_val) {
							max_val = c.g;
							max_idx = 5;
						}
						if (c.b > max_val) {
							max_val = c.b;
							max_idx = 6;
						}
						if (c.a > max_val) {
							max_val = c.a;
							max_idx = 7;
						}
					}
					if (_target.numLayers > 8 && _targetRT.controlC) {
						Color c = _targetRT.controlC.GetPixelBilinear (_targetRT.paintHitInfo.textureCoord.x, _targetRT.paintHitInfo.textureCoord.y);
						if (c.r > max_val) {
							max_val = c.r;
							max_idx = 8;
						}
						if (c.g > max_val) {
							max_val = c.g;
							max_idx = 9;
						}
						if (c.b > max_val) {
							max_val = c.b;
							max_idx = 10;
						}
						if (c.a > max_val) {
							max_val = c.a;
							max_idx = 11;
						}
					}
						
					_target.submenu = ReliefTerrainMenuItems.Details;
					_target.show_active_layer = max_idx;
				}
			}					
		}
	}

	public void CustomOnSceneGUI (SceneView sceneview)
	{
		ReliefTerrain _targetRT = (ReliefTerrain)target;
		ReliefTerrainGlobalSettingsHolder _target = _targetRT.globalSettingsHolder;
			
		EditorWindow currentWindow = EditorWindow.mouseOverWindow;
		if (!currentWindow)
			return;
			
		Event current = Event.current;
			
		if (current.alt) {
			return;
		}		
		if (Event.current.button == 1) {
			HandleUtility.AddDefaultControl (GUIUtility.GetControlID (FocusType.Passive));
			return;
		}
			
		bool paintOff_flag = (UnityEditor.Tools.current != Tool.View);
		if (_target.paint_wetmask) {
			paintOff_flag = paintOff_flag || _target.submenu != ReliefTerrainMenuItems.GeneralSettings;
		} else {
			paintOff_flag = paintOff_flag || _target.submenu != ReliefTerrainMenuItems.GeneralSettings || _target.submenu_settings != ReliefTerrainSettingsItems.GlobalColor;
		}
		if (paintOff_flag) {
			_target.paint_flag = false;
			SceneView.onSceneGUIDelegate -= ReliefTerrain._SceneGUI;
			EditorUtility.SetDirty (target);
			return;
		}		
			
		if (current.type == EventType.layout) {
			HandleUtility.AddDefaultControl (GUIUtility.GetControlID (FocusType.Passive));
			return;
		}
			
		switch (current.type) {
		case EventType.keyDown:
			if (current.keyCode == KeyCode.Escape) {
				_target.paint_flag = false;
				UnityEditor.Tools.current = _targetRT.prev_tool;
				SceneView.onSceneGUIDelegate -= ReliefTerrain._SceneGUI;
				EditorUtility.SetDirty (target);
			}
			break;
		}
			
		if (current.control) {
			if (current.type == EventType.mouseMove) {
				if (_targetRT.control_down_flag) {
					_targetRT.control_down_flag = false;
					EditorUtility.SetDirty (target);
				}
			}
			return;
		}
		_targetRT.control_down_flag = true;
			
		switch (current.type) {
		case EventType.mouseDown:
			get_paint_coverage ();
				// Debug.Log(""+cover_verts_num + "  "+ paintHitInfo_flag + _target.prepare_tmpColorMap());
			if (_targetRT.paintHitInfo_flag) {
				if (_targetRT.prepare_tmpTexture (!_target.paint_wetmask)) {
#if UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2
						Undo.RegisterUndo(_targetRT.tmp_globalColorMap, "Geometry Blend Edit");
#else
					Undo.RegisterCompleteObjectUndo (_targetRT.tmp_globalColorMap, "Geometry Blend Edit");
#endif
					_targetRT.modify_blend (!current.shift);
						
				}
			} else {
				_target.undo_flag = true;
			}
			current.Use ();
			break;
		case EventType.mouseDrag:
			get_paint_coverage ();
			if (_targetRT.paintHitInfo_flag) {
				if (_target.undo_flag) {
					if (_targetRT.prepare_tmpTexture (!_target.paint_wetmask)) {
#if UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2
							Undo.RegisterUndo(_targetRT.tmp_globalColorMap, "Geometry Blend Edit");
#else
						Undo.RegisterCompleteObjectUndo (_targetRT.tmp_globalColorMap, "Geometry Blend Edit");
#endif
					}
					_target.undo_flag = false;
				}
			}
			if (_targetRT.paintHitInfo_flag) {
				if (_targetRT.prepare_tmpTexture (!_target.paint_wetmask)) {
					_targetRT.modify_blend (!current.shift);
					//current.Use();
				}
			}
			break;
		case EventType.mouseMove:
			get_paint_coverage ();
			break;
		}
			


		if (_targetRT.paintHitInfo_flag) {
			bool upflag = _target.paint_wetmask ? !current.shift : current.shift;
			float size = (_target.paintMode==PaintMode.layer) ? _target.paint_size : _targetRT.brushSize;
			if (upflag) {
					
				Handles.color = new Color (1, 0, 0, Mathf.Max (0.1f, _target.paint_opacity * 0.5f));
				Handles.DrawWireDisc (_targetRT.paintHitInfo.point, _targetRT.paintHitInfo.normal, size);
				Handles.color = new Color (1, 0, 0, Mathf.Max (0.6f, _target.paint_opacity));
				Handles.DrawWireDisc (_targetRT.paintHitInfo.point, _targetRT.paintHitInfo.normal, size * Mathf.Max (0.3f, 1 - _target.paint_smoothness));
			} else {
				Handles.color = new Color (0, 1, 0, Mathf.Max (0.1f, _target.paint_opacity * 0.5f));
				Handles.DrawWireDisc (_targetRT.paintHitInfo.point, _targetRT.paintHitInfo.normal, size);
				Handles.color = new Color (0, 1, 0, Mathf.Max (0.6f, _target.paint_opacity));
				Handles.DrawWireDisc (_targetRT.paintHitInfo.point, _targetRT.paintHitInfo.normal, size * Mathf.Max (0.3f, 1 - _target.paint_smoothness));
			}			
		}		
		if (_target.paintMode == PaintMode.layer)
		if (current.shift)
			current.Use ();
	}

	public void get_paint_coverage ()
	{
		ReliefTerrain _targetRT = (ReliefTerrain)target;

		if (EditorApplication.timeSinceStartup <  _targetRT.lCovTim)
			return;
		_targetRT.lCovTim = EditorApplication.timeSinceStartup + 0.02;

		ReliefTerrainGlobalSettingsHolder _target = _targetRT.globalSettingsHolder;
		if (!_targetRT.GetComponent<Collider> ())
			return;
			
		Ray ray = HandleUtility.GUIPointToWorldRay (Event.current.mousePosition);		
		_targetRT.paintHitInfo_flag = _targetRT.GetComponent<Collider> ().Raycast (ray, out _targetRT.paintHitInfo, Mathf.Infinity);
		_target.paintHitInfo = _targetRT.paintHitInfo;
		_target.paintHitInfo_flag = _targetRT.paintHitInfo_flag;
			
			
		EditorUtility.SetDirty (target);
	}

	private void ResetProgress (int progress_count, string _progress_description = "")
	{
		progress_count_max = progress_count;
		progress_count_current = 0;
		progress_description = _progress_description;
	}

	private void CheckProgress ()
	{
		if (((progress_count_current++) % progress_granulation) == (progress_granulation - 1)) {
			EditorUtility.DisplayProgressBar ("Processing...", progress_description, 1.0f * progress_count_current / progress_count_max);
		}
			
	}		



	#endif

	public static void LoadAsset (string path, ReliefTerrainGlobalSettingsHolder _target, ReliefTerrain _targetRT, Terrain terrainComp)
	{
		ReliefTerrainPresetHolder loadedPreset = AssetDatabase.LoadAssetAtPath (path, typeof(ReliefTerrainPresetHolder)) as ReliefTerrainPresetHolder;
		if (loadedPreset != null) {
			switch (_targetRT.ln)
			{
			case LayersNumber._4Layers:
				{
					_target.numLayers = 4;
					Debug.Log ("4_1");
					_targetRT.GetComponent<Renderer> ().sharedMaterial = new Material (Shader.Find ("Relief Pack/Terrain2Geometry4Layers"));
					_targetRT.RefreshTextures ();

				}
				break;
			case LayersNumber._8Layers:
				{
					_target.numLayers = 8;
					Debug.Log ("8_1");
					_targetRT.GetComponent<Renderer> ().sharedMaterial = new Material (Shader.Find ("Relief Pack/Terrain2Geometry8Layers"));
					_targetRT.RefreshTextures ();

				}
				break;
			case LayersNumber._Transition:
				{
					_target.numLayers = 8;
					Debug.Log ("T_1");
					_targetRT.GetComponent<Renderer> ().sharedMaterial = new Material (Shader.Find ("Relief Pack/Terrain2GeometryTransition"));
					_targetRT.RefreshTextures ();

				}
				break;
			}

			if ((loadedPreset.type == "TERRAIN" && terrainComp != null) || (loadedPreset.type == "MESH" && terrainComp == null)) {
				// to not deal with assets on disk but only with instances in mem saved in scene
				ReliefTerrainPresetHolder loadedPresetCopy = UnityEngine.Object.Instantiate (loadedPreset) as ReliefTerrainPresetHolder;
				//int dgres = EditorUtility.DisplayDialogComplex ("RTP Notification", "What would you like to restore current terrain object specific textures, too ?", "Yes", "No, only globals", "Cancel");
				/*
				if (dgres != 2) {
					//#if UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2
					//	Undo.RegisterUndo((ReliefTerrain)target, "Undo relief terrain edit");
					//#else
					//Undo.RecordObject ((ReliefTerrain)target, "Undo relief terrain edit");
					//#endif
					if (dgres == 0) {
						_targetRT.RestorePreset (loadedPresetCopy);
					} else {
						_target.RestorePreset (loadedPresetCopy);
					}
					_target.RefreshAll ();
				}
				*/
				//ReliefTerrainEditor[] editors = (ReliefTerrainEditor[])Resources.FindObjectsOfTypeAll (typeof(ReliefTerrainEditor));
				_target.RestorePreset (loadedPresetCopy);

				//for (int j=0; j<editors.Length; j++)
				//	editors [j].RefreshAll (_targetRT);

			} else
				EditorUtility.DisplayDialog ("Error", "Mismatched type - you can't load RTP preset that has been taken from mesh when you're using terrain and vice versa.", "OK");
			

		} else {
			EditorUtility.DisplayDialog ("Error", "No RTP preset available at specified location.", "OK");
		}
	}
}
	