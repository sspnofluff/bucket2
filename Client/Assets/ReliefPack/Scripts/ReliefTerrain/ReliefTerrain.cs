using UnityEngine;
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using Dest.Math;
#if UNITY_EDITOR

using System;
using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;

#region ResamplingService class

/// <summary>
/// A class for image resampling with custom filters.
/// </summary>
public class ResamplingService {

	#region Properties

	/// <summary>
	/// Gets or sets the resampling filter.
	/// </summary>
	public ResamplingFilters Filter {

		get {
			return this.filter;
		}
		set {
			this.filter = value;
		}
	}

	/// <summary>
	/// Gets or sets wheter the resampling process is stopping.
	/// </summary>
	public bool Aborting {

		get {
			return this.aborting;
		}
		set {
			this.aborting = value;
		}
	}

	/// <summary>
	/// Covered units. Progress can be computed in combination with ResamplingService.Total property.
	/// </summary>
	public int Covered {

		get {
			return this.covered;
		}
	}

	/// <summary>
	/// Total units. Progress can be computer in combination with ResamplingService.Covered property.
	/// </summary>
	public int Total {

		get {
			return this.total;
		}
	}

	#endregion

	#region Structures

	internal struct Contributor {

		public int pixel;
		public double weight;
	}

	internal struct ContributorEntry {

		public int n;
		public Contributor[] p;
		public double wsum;
	}

	#endregion

	#region Private Fields

	private bool aborting = false;
	private int covered = 0, total = 0;
	private ResamplingFilters filter = ResamplingFilters.Box;

	#endregion

	#region Public Methods

	/// <summary>
	/// Resamples input array to a new array using current resampling filter.
	/// </summary>
	/// <param name="input">Input array.</param>
	/// <param name="nWidth">Width of the output array.</param>
	/// <param name="nHeight">Height of the output array.</param>
	/// <returns>Output array.</returns>
	public ushort[][,] Resample(ushort[][,] input, int nWidth, int nHeight) {

		if (input == null || input.Length == 0 || nWidth <= 1 || nHeight <= 1)
			return null;

		ResamplingFilter filter = ResamplingFilter.Create(this.filter);

		int width = input[0].GetLength(0);
		int height = input[0].GetLength(1);
		int planes = input.Length;

		this.covered = 0;
		this.total = (nWidth + height);

		// create bitmaps
		ushort[][,] work = new ushort[planes][,];
		ushort[][,] output = new ushort[planes][,];
		int c = 0;

		for (c = 0; c < planes; c++) {

			work[c] = new ushort[nWidth, height];
			output[c] = new ushort[nWidth, nHeight];
		}

		double xScale = ((double)nWidth / width);
		double yScale = ((double)nHeight / height);

		ContributorEntry[] contrib = new ContributorEntry[nWidth];

		double wdth = 0, center = 0, weight = 0, intensity = 0;
		int left = 0, right = 0, i = 0, j = 0, k = 0;

		// horizontal downsampling
		if (xScale < 1.0) {

			// scales from bigger to smaller width
			wdth = (filter.defaultFilterRadius / xScale);

			for (i = 0; i < nWidth; i++) {

				contrib[i].n = 0;
				contrib[i].p = new Contributor[(int)Math.Floor(2 * wdth + 1)];
				contrib[i].wsum = 0;
				center = ((i + 0.5) / xScale);
				left = (int)(center - wdth);
				right = (int)(center + wdth);

				for (j = left; j <= right; j++) {

					weight = filter.GetValue((center - j - 0.5) * xScale);

					if ((weight == 0) || (j < 0) || (j >= width))
						continue;

					contrib[i].p[contrib[i].n].pixel = j;
					contrib[i].p[contrib[i].n].weight = weight;
					contrib[i].wsum += weight;
					contrib[i].n++;
				}

				if (aborting)
					goto End;
			}
		} else {

			// horizontal upsampling
			// scales from smaller to bigger width
			for (i = 0; i < nWidth; i++) {

				contrib[i].n = 0;
				contrib[i].p = new Contributor[(int)Math.Floor(2 * filter.defaultFilterRadius + 1)];
				contrib[i].wsum = 0;
				center = ((i + 0.5) / xScale);
				left = (int)Math.Floor(center - filter.defaultFilterRadius);
				right = (int)Math.Ceiling(center + filter.defaultFilterRadius);

				for (j = left; j <= right; j++) {

					weight = filter.GetValue(center - j - 0.5);

					if ((weight == 0) || (j < 0) || (j >= width))
						continue;

					contrib[i].p[contrib[i].n].pixel = j;
					contrib[i].p[contrib[i].n].weight = weight;
					contrib[i].wsum += weight;
					contrib[i].n++;          
				}

				if (aborting)
					goto End;
			}
		}

		// filter horizontally from input to work
		for (c = 0; c < planes; c++) {

			for (k = 0; k < height; k++) {

				for (i = 0; i < nWidth; i++) {

					intensity = 0;

					for (j = 0; j < contrib[i].n; j++) {

						weight = contrib[i].p[j].weight;

						if (weight == 0)
							continue;

						intensity += (input[c][contrib[i].p[j].pixel, k] * weight);
					}

					work[c][i, k] = (ushort)Math.Min(Math.Max(intensity / contrib[i].wsum, UInt16.MinValue), UInt16.MaxValue);
				}

				if (aborting)
					goto End;

				this.covered++;
			}
		}

		// pre-calculate filter contributions for a column
		contrib = new ContributorEntry[nHeight];

		// vertical downsampling
		if (yScale < 1.0) {

			// scales from bigger to smaller height
			wdth = (filter.defaultFilterRadius / yScale);

			for (i = 0; i < nHeight; i++) {

				contrib[i].n = 0;
				contrib[i].p = new Contributor[(int)Math.Floor(2 * wdth + 1)];
				contrib[i].wsum = 0;
				center = ((i + 0.5) / yScale);
				left = (int)(center - wdth);
				right = (int)(center + wdth);

				for (j = left; j <= right; j++) {

					weight = filter.GetValue((center - j - 0.5) * yScale);

					if ((weight == 0) || (j < 0) || (j >= height))
						continue;

					contrib[i].p[contrib[i].n].pixel = j;
					contrib[i].p[contrib[i].n].weight = weight;
					contrib[i].wsum += weight;
					contrib[i].n++;
				}

				if (aborting)
					goto End;
			}
		} else {

			// vertical upsampling
			// scales from smaller to bigger height
			for (i = 0; i < nHeight; i++) {

				contrib[i].n = 0;
				contrib[i].p = new Contributor[(int)Math.Floor(2 * filter.defaultFilterRadius + 1)];
				contrib[i].wsum = 0;
				center = ((i + 0.5) / yScale);
				left = (int)(center - filter.defaultFilterRadius);
				right = (int)(center + filter.defaultFilterRadius);

				for (j = left; j <= right; j++) {

					weight = filter.GetValue(center - j - 0.5);

					if ((weight == 0) || (j < 0) || (j >= height))
						continue;

					contrib[i].p[contrib[i].n].pixel = j;
					contrib[i].p[contrib[i].n].weight = weight;
					contrib[i].wsum += weight;
					contrib[i].n++;
				}

				if (aborting)
					goto End;
			}
		}

		// filter vertically from work to output
		for (c = 0; c < planes; c++) {

			for (k = 0; k < nWidth; k++) {

				for (i = 0; i < nHeight; i++) {

					intensity = 0;

					for (j = 0; j < contrib[i].n; j++) {

						weight = contrib[i].p[j].weight;

						if (weight == 0)
							continue;

						intensity += (work[c][k, contrib[i].p[j].pixel] * weight);
					}

					output[c][k, i] = (ushort)Math.Min(Math.Max(intensity, UInt16.MinValue), UInt16.MaxValue);
				}

				if (aborting)
					goto End;

				this.covered++;
			}
		}

		End:;

		work = null;

		return output;
	}

	#endregion
}

#endregion

#region ResamplingFilters enum

public enum ResamplingFilters {

	Box = 0,
	Triangle,
	Hermite,
	Bell,
	CubicBSpline,
	Lanczos3,
	Mitchell,
	Cosine,
	CatmullRom,
	Quadratic,
	QuadraticBSpline,
	CubicConvolution,
	Lanczos8
}

#endregion

#region ResamplingFilter class

public abstract class ResamplingFilter {

	public static ResamplingFilter Create(ResamplingFilters filter) {

		ResamplingFilter resamplingFilter = null;

		switch (filter) {
		case ResamplingFilters.Box:
			resamplingFilter = new BoxFilter();
			break;
		case ResamplingFilters.Triangle:
			resamplingFilter = new TriangleFilter();
			break;
		case ResamplingFilters.Hermite:
			resamplingFilter = new HermiteFilter();
			break;
		case ResamplingFilters.Bell:
			resamplingFilter = new BellFilter();
			break;
		case ResamplingFilters.CubicBSpline:
			resamplingFilter = new CubicBSplineFilter();
			break;
		case ResamplingFilters.Lanczos3:
			resamplingFilter = new Lanczos3Filter();
			break;
		case ResamplingFilters.Mitchell:
			resamplingFilter = new MitchellFilter();
			break;
		case ResamplingFilters.Cosine:
			resamplingFilter = new CosineFilter();
			break;
		case ResamplingFilters.CatmullRom:
			resamplingFilter = new CatmullRomFilter();
			break;
		case ResamplingFilters.Quadratic:
			resamplingFilter = new QuadraticFilter();
			break;
		case ResamplingFilters.QuadraticBSpline:
			resamplingFilter = new QuadraticBSplineFilter();
			break;
		case ResamplingFilters.CubicConvolution:
			resamplingFilter = new CubicConvolutionFilter();
			break;
		case ResamplingFilters.Lanczos8:
			resamplingFilter = new Lanczos8Filter();
			break;
		}

		return resamplingFilter;
	}

	public double defaultFilterRadius;
	public abstract double GetValue(double x);
}

internal class HermiteFilter : ResamplingFilter {

	public HermiteFilter() {

		defaultFilterRadius = 1;
	}

	public override double GetValue(double x) {

		if (x < 0) x = - x;
		if (x < 1) return ((2*x - 3)*x*x + 1);
		return 0;
	}
}

internal class BoxFilter : ResamplingFilter {

	public BoxFilter() {

		defaultFilterRadius = 0.5;
	}

	public override double GetValue(double x) {

		if (x < 0) x = - x;
		if (x <= 0.5) return 1;
		return 0;
	}
}

internal class TriangleFilter : ResamplingFilter {

	public TriangleFilter() {

		defaultFilterRadius = 1;
	}

	public override double GetValue(double x) {

		if (x < 0) x = - x;
		if (x < 1) return (1 - x);
		return 0;
	}
}

internal class BellFilter : ResamplingFilter {

	public BellFilter() {

		defaultFilterRadius = 1.5;
	}

	public override double GetValue(double x) {

		if (x < 0) x = - x;
		if (x < 0.5) return (0.75 - x*x);
		if (x < 1.5) return (0.5*Math.Pow(x - 1.5, 2));
		return 0;
	}
}

internal class CubicBSplineFilter : ResamplingFilter {

	double temp;

	public CubicBSplineFilter() {

		defaultFilterRadius = 2;
	}

	public override double GetValue(double x) {

		if (x < 0) x = - x;
		if (x < 1 ) {

			temp = x*x;
			return (0.5*temp*x - temp + 2f/3f);
		}
		if (x < 2) {

			x = 2f - x;
			return (Math.Pow(x, 3)/6f);
		}
		return 0;
	}
}

internal class Lanczos3Filter : ResamplingFilter {

	public Lanczos3Filter() {

		defaultFilterRadius = 3;
	}

	double SinC(double x) {

		if (x != 0) {

			x *= Math.PI;
			return (Math.Sin(x)/x);
		}
		return 1;
	}

	public override double GetValue(double x) {

		if (x < 0) x = - x;
		if (x < 3) return (SinC(x)*SinC(x/3f));
		return 0;
	}
}

internal class MitchellFilter : ResamplingFilter {

	const double C = 1/3;
	double temp;

	public MitchellFilter() {

		defaultFilterRadius = 2;
	}

	public override double GetValue(double x) {

		if (x < 0) x = - x;
		temp = x*x;
		if (x < 1) {

			x = (((12 - 9*C - 6*C)*(x*temp)) + ((- 18 + 12*C + 6*C)*temp) + (6 - 2*C));
			return (x/6);
		}
		if (x < 2) {

			x = (((- C - 6*C)*(x*temp)) + ((6*C + 30*C)*temp) + ((- 12*C - 48*C)*x) + (8*C + 24*C));
			return (x/6);
		}
		return 0;
	}
}

internal class CosineFilter : ResamplingFilter {

	public CosineFilter() {

		defaultFilterRadius = 1;
	}

	public override double GetValue(double x) {

		if ((x >= -1) && (x <= 1)) return ((Math.Cos(x*Math.PI) + 1)/2f);
		return 0;
	}
}

internal class CatmullRomFilter : ResamplingFilter {

	const double C = 1/2;
	double temp;

	public CatmullRomFilter() {

		defaultFilterRadius = 2;
	}

	public override double GetValue(double x) {

		if (x < 0) x = - x;
		temp = x*x;
		if (x <= 1) return (1.5*temp*x - 2.5*temp + 1);
		if (x <= 2) return (- 0.5*temp*x + 2.5*temp - 4*x + 2);
		return 0;
	}
}

internal class QuadraticFilter : ResamplingFilter {

	public QuadraticFilter() {

		defaultFilterRadius = 1.5;
	}

	public override double GetValue(double x) {

		if (x < 0) x = - x;
		if (x <= 0.5) return (- 2*x*x + 1);
		if (x <= 1.5) return (x*x - 2.5*x + 1.5);
		return 0;
	}
}

internal class QuadraticBSplineFilter : ResamplingFilter {

	public QuadraticBSplineFilter() {

		defaultFilterRadius = 1.5;
	}

	public override double GetValue(double x) {

		if (x < 0) x = - x;
		if (x <= 0.5) return (- x*x + 0.75);
		if (x <= 1.5) return (0.5*x*x - 1.5*x + 1.125);
		return 0;
	}
}

internal class CubicConvolutionFilter : ResamplingFilter {

	double temp;

	public CubicConvolutionFilter() {

		defaultFilterRadius = 3;
	}

	public override double GetValue(double x) {

		if (x < 0) x = - x;
		temp = x*x;
		if (x <= 1) return ((4f/3f)*temp*x - (7f/3f)*temp + 1);
		if (x <= 2) return (- (7f/12f)*temp*x + 3*temp - (59f/12f)*x + 2.5);
		if (x <= 3) return ((1f/12f)*temp*x - (2f/3f)*temp + 1.75*x - 1.5);
		return 0;
	}
}

internal class Lanczos8Filter : ResamplingFilter {    

	public Lanczos8Filter() {

		defaultFilterRadius = 8;
	}

	double SinC(double x) {

		if (x != 0) {

			x *= Math.PI;
			return (Math.Sin(x)/x);
		}
		return 1;
	}

	public override double GetValue(double x) {

		if (x < 0) x = - x;
		if (x < 8) return (SinC(x)*SinC(x/8f));
		return 0;
	}
}

#endregion
#endif



public enum RTPColorChannels {
	R, G, B, A
}

public enum ReliefTerrainMenuItems {
	GeneralSettings, Details, CombinedTextures 
}
public enum ReliefTerrainSettingsItems {
	GlobalColor, MainSettings
}

[System.Flags]
public enum LayersNumber {
	_4Layers = 0, 
	_8Layers = 1,
	_Transition = 2,
}

[System.Serializable]
public class Presets
{
	public string path;
	public Texture2D ico;
	public Texture2D atlas;
	public LayersNumber ln;
	public string name;

	public Presets (string _path, Texture2D _ico, Texture2D _atlas, LayersNumber _ln, string _name)
	{
		path = _path;
		ico = _ico;
		atlas = _atlas;
		ln = _ln;
		name = _name;
	}
}

[AddComponentMenu("Relief Terrain/Engine - Terrain or Mesh")]
[ExecuteInEditMode]
public class ReliefTerrain : MonoBehaviour {

	#if UNITY_EDITOR
	int progress_count_max;
	int progress_count_current;
	const int progress_granulation = 1;
	string progress_description = "";

	public int editInt = 0;
	public int typeInt = 0;
	//public GameObject myCursor;
	public float brushSize = 10.0f;
	public float brushRadius = 10.0f;
	public float brushValue = 0.1f;
	public Mesh currentTerrain;

	public float tgtHeight = 0f;



	private Texture rockPreset;
	private Texture desertPreset;
	private Texture forestPreset;
	private Texture snowPreset;
	private Texture rockDesertPreset;

	public Texture terrainAdd;
	public Texture terrainSmooth;
	public Texture terrainCliff;
	public Texture terrainRestore;
	public Texture[] terrainBrush;


	public int currentPresetNumber01 = -1;
	public int currentPresetNumber02 = -1;

	public bool control_down_flag = false;
	public Tool prev_tool;
	public double lCovTim = 0;

	public RaycastHit paintHitInfo;
	public bool paintHitInfo_flag;
	#endif

	public LayersNumber ln;

	public Texture2D controlA;
	public Texture2D controlB;
	public Texture2D controlC;
	public string save_path_controlA="";
	public string save_path_controlB="";
	public string save_path_controlC="";
	public string save_path_colormap="";
	public string save_path_BumpGlobalCombined="";
	public string save_path_WetMask="";

	public Presets[] presets;


	public Texture2D NormalGlobal;
	public Texture2D TreesGlobal;
	public Texture2D ColorGlobal;
	public Texture2D AmbientEmissiveMap;
	public Texture2D BumpGlobalCombined;


	public Texture2D tmp_globalColorMap;
	public Texture2D tmp_CombinedMap;
	public Texture2D tmp_WaterMap;
	public bool globalColorModifed_flag=false;
	public bool globalCombinedModifed_flag=false;
	public bool globalWaterModifed_flag=false;	
	
	public bool splat_layer_ordered_mode;
	public RTPColorChannels[] source_controls_channels;
	public int[] splat_layer_seq;
	public float[] splat_layer_boost;
	public bool[] splat_layer_calc;
	public bool[] splat_layer_masked;
	public RTPColorChannels[] source_controls_mask_channels;
	
	public Texture2D[] source_controls;
	public bool[] source_controls_invert;
	public Texture2D[] source_controls_mask;
	public bool[] source_controls_mask_invert;		
	
	public Vector2 customTiling=new Vector2(3,3);
	
	[SerializeField] public ReliefTerrainPresetHolder[] presetHolders;
	
	[SerializeField] public ReliefTerrainGlobalSettingsHolder globalSettingsHolder;
	
#if UNITY_EDITOR	
	[HideInInspector] public static SceneView.OnSceneFunc _SceneGUI;	
#endif

	public void GetGlobalSettingsHolder() {
#if UNITY_EDITOR
		if (globalSettingsHolder!=null) {
			// refresh num tiles in case we've just removed all except for this one
			bool IamTerrain=GetComponent(typeof(Terrain));
			if (IamTerrain) {
				ReliefTerrain[] script_objs=(ReliefTerrain[])GameObject.FindObjectsOfType(typeof(ReliefTerrain));
				globalSettingsHolder.numTiles=0;
				for(int p=0; p<script_objs.Length; p++) {
					if ((script_objs[p].transform.parent==transform.parent) && script_objs[p].globalSettingsHolder!=null) {
						if (script_objs[p].globalSettingsHolder!=globalSettingsHolder && script_objs[p].GetComponent(typeof(Terrain))!=null) {
							//Debug.Log("RTP assert - leaving one globalSettingsHolder...");
							globalSettingsHolder=script_objs[p].globalSettingsHolder;
						}
						if (IamTerrain && script_objs[p].GetComponent(typeof(Terrain))!=null) {
							globalSettingsHolder.numTiles++;
						}
					}
				}	
				if (globalSettingsHolder.numTiles==1 || globalSettingsHolder.useTerrainMaterial) {
					// we don't have to use texture redefinitions
					GetSplatsFromGlobalSettingsHolder();
				}
			}
		}
		//Debug.Log (""+globalSettingsHolder.numTiles+" "+globalSettingsHolder.useTerrainMaterial);
#endif
		if (globalSettingsHolder==null) {
			//Debug.Log("E"+name);
			ReliefTerrain[] script_objs=(ReliefTerrain[])GameObject.FindObjectsOfType(typeof(ReliefTerrain));
			bool IamTerrain=GetComponent(typeof(Terrain));
			for(int p=0; p<script_objs.Length; p++) {
				if ((script_objs[p].transform.parent==transform.parent) && script_objs[p].globalSettingsHolder!=null && ((IamTerrain && script_objs[p].GetComponent(typeof(Terrain))!=null) || (!IamTerrain && script_objs[p].GetComponent(typeof(Terrain))==null))) {
					//Debug.Log ("E2 "+script_objs[p].name);
					globalSettingsHolder=script_objs[p].globalSettingsHolder;
					if (globalSettingsHolder.Get_RTP_LODmanagerScript() && !globalSettingsHolder.Get_RTP_LODmanagerScript().RTP_WETNESS_FIRST && !globalSettingsHolder.Get_RTP_LODmanagerScript().RTP_WETNESS_ADD) {
						BumpGlobalCombined=script_objs[p].BumpGlobalCombined;
						globalCombinedModifed_flag=false;
					}
					break;
				}
			}
			if (globalSettingsHolder==null) {
				// there is no globalSettingsHolder object of my type (terrain/mesh) in the hierarchy - I'm first object
				globalSettingsHolder=new ReliefTerrainGlobalSettingsHolder();
				
				if (IamTerrain) {
					globalSettingsHolder.numTiles=0; // will be set to 1 with incrementation below
					Terrain terrainComp = (Terrain)GetComponent(typeof(Terrain));
					globalSettingsHolder.splats=new Texture2D[terrainComp.terrainData.splatPrototypes.Length];
					for(int i=0; i<terrainComp.terrainData.splatPrototypes.Length; i++) {
						globalSettingsHolder.splats[i]=terrainComp.terrainData.splatPrototypes[i].texture;
					}
				} else {				
					globalSettingsHolder.splats=new Texture2D[4];
				}
				globalSettingsHolder.numLayers=globalSettingsHolder.splats.Length;
				globalSettingsHolder.ReturnToDefaults();
			} else {
				if (IamTerrain) {
					GetSplatsFromGlobalSettingsHolder();
				}
			}
			
			source_controls_mask=new Texture2D[12];
			source_controls=new Texture2D[12];
			source_controls_channels=new RTPColorChannels[12];
			source_controls_mask_channels=new RTPColorChannels[12];
			#if UNITY_EDITOR
			terrainAdd = AssetDatabase.LoadAssetAtPath<Texture> ("Assets/Art/gui/editorElements/terrainAdd.png");
			terrainCliff = AssetDatabase.LoadAssetAtPath<Texture> ("Assets/Art/gui/editorElements/terrainCliff.png");
			terrainSmooth = AssetDatabase.LoadAssetAtPath<Texture> ("Assets/Art/gui/editorElements/terrainSmooth.png");
			terrainRestore = AssetDatabase.LoadAssetAtPath<Texture> ("Assets/Art/gui/editorElements/terrainRestore.png");
			terrainBrush = new Texture[2];
			terrainBrush[0] = AssetDatabase.LoadAssetAtPath<Texture> ("Assets/Art/gui/editorElements/terrainHeight.png");
			terrainBrush[1] = AssetDatabase.LoadAssetAtPath<Texture> ("Assets/Art/gui/editorElements/terrainBrush.png");
			#endif
			splat_layer_seq=new int[12] {0,1,2,3,4,5,6,7,8,9,10,11};	
			splat_layer_boost=new float[12] {1,1,1,1,1,1,1,1,1,1,1,1};
			splat_layer_calc=new bool[12];
			splat_layer_masked=new bool[12];
			source_controls_invert=new bool[12];
			source_controls_mask_invert=new bool[12];		
			
			if (IamTerrain) globalSettingsHolder.numTiles++;
		}
	}
	#if UNITY_EDITOR
	public static void ImportStandart(Transform tr, Texture2D tex)
	{
		ReliefTerrain RT = (ReliefTerrain)tr.gameObject.AddComponent<ReliefTerrain> ();
		MeshCollider RTCollider = (MeshCollider)RT.gameObject.AddComponent<MeshCollider> (); 

		RT.controlA = tex;

	}

	public static void ImportDouble(Transform tr, Texture2D tex, Texture2D tex2)
	{
		ReliefTerrain RT = (ReliefTerrain)tr.gameObject.AddComponent<ReliefTerrain> ();
		MeshCollider RTCollider = (MeshCollider)RT.gameObject.AddComponent<MeshCollider> (); 

		RT.controlA = tex;
		RT.controlB = tex2;
	}
	#endif
	private void GetSplatsFromGlobalSettingsHolder() {
		SplatPrototype[] splatPrototypes=new SplatPrototype[globalSettingsHolder.numLayers];
		for(int i=0; i<globalSettingsHolder.numLayers; i++) {
			//Debug.Log(""+globalSettingsHolder.splats[i]);
			splatPrototypes[i]=new SplatPrototype();
			splatPrototypes[i].tileSize=Vector2.one;
			splatPrototypes[i].tileOffset=new Vector2(1.0f / customTiling.x, 1.0f / customTiling.y);
			splatPrototypes[i].texture=globalSettingsHolder.splats[i];
		}
		Terrain terrainComp = (Terrain)GetComponent(typeof(Terrain));
		terrainComp.terrainData.splatPrototypes=splatPrototypes;
	}
	
	public void InitTerrainTileSizes() {
		Terrain terrainComp = (Terrain)GetComponent(typeof(Terrain));
		if (terrainComp) {
			globalSettingsHolder.terrainTileSize=terrainComp.terrainData.size;
		} else {
			globalSettingsHolder.terrainTileSize=GetComponent<Renderer>().bounds.size;
			globalSettingsHolder.terrainTileSize.y=globalSettingsHolder.tessHeight;
		}
	}

//	void OnDrawGizmos() {
//		MeshRenderer mr = GetComponent<MeshRenderer>();
//		if (mr) {
//			Gizmos.color = Color.yellow;
//			Gizmos.DrawWireCube (mr.bounds.center, mr.bounds.extents*2);
//		}
//	}

	void Awake () {
		UpdateBasemapDistance(false);
		RefreshTextures();
	}
	
	public void InitArrays() {
		RefreshTextures();
	}

	#if UNITY_EDITOR
	[ContextMenu("Bake")]
	public void Bake()
	{
		AssetDatabase.CreateAsset (this.GetComponent<MeshRenderer> ().sharedMaterial, "Assets/mainMaterial.mat");
	}

	#endif

	private void UpdateBasemapDistance(bool apply_material_if_applicable) {
		Terrain terrainComp = (Terrain)GetComponent(typeof(Terrain));
		if (terrainComp)  {
			#if UNITY_3_5
			terrainComp.basemapDistance=500000; // to let user not using simple shader (not accessible anyway) at far distance
			globalSettingsHolder.Refresh();
			#else	
			if (globalSettingsHolder!=null) {
				if (!globalSettingsHolder.useTerrainMaterial) {
					//
					// no material
					//
					terrainComp.basemapDistance=500000;
					#if UNITY_5_0
					terrainComp.materialType=Terrain.MaterialType.Custom;
					#endif
					terrainComp.materialTemplate=null;
				} else {
					//
					// material
					//
					if (globalSettingsHolder.super_simple_active) {
						terrainComp.basemapDistance=globalSettingsHolder.distance_start_bumpglobal+globalSettingsHolder.distance_transition_bumpglobal;
					} else {
						terrainComp.basemapDistance=globalSettingsHolder.distance_start+globalSettingsHolder.distance_transition;
					}
					// U5
					#if UNITY_5
						if (!globalSettingsHolder._4LAYERS_SHADER_USED) {
							// add pass is always fired when >4 layers used, so we can't use optimization and we need to use far only replacement at all distances
							terrainComp.basemapDistance=0;
						}
					#endif
					if (apply_material_if_applicable) {
						if (terrainComp.materialTemplate==null) {
							#if UNITY_5_0
							terrainComp.materialType=Terrain.MaterialType.Custom;
							#endif
							
							Material ter_mat;
							Shader ter_shad=Shader.Find("Relief Pack/ReliefTerrain-FirstPass");
							if (ter_shad) {
								ter_mat=new Material(ter_shad);
								ter_mat.name=gameObject.name+" material";
								terrainComp.materialTemplate=ter_mat;
							}
						} else {
							Material ter_mat=terrainComp.materialTemplate;
							terrainComp.materialTemplate=null;
							terrainComp.materialTemplate=ter_mat;
						}
					}
					// far shader setup might not fit (might be for multiple scenes with different scenarios)
					if (globalSettingsHolder!=null && globalSettingsHolder._RTP_LODmanagerScript!=null && globalSettingsHolder._RTP_LODmanagerScript.numLayersProcessedByFarShader!=globalSettingsHolder.numLayers) {
						// so - don't use it
						terrainComp.basemapDistance=500000;
					}
				}
				globalSettingsHolder.Refresh(terrainComp.materialTemplate);
			}
			#endif		
		}
	}
	
	public void RefreshTextures(Material mat=null, bool check_weak_references=false) { // mat used by geom blend to setup underlying mesh
		GetGlobalSettingsHolder();
		InitTerrainTileSizes();
		if (globalSettingsHolder!=null && BumpGlobalCombined!=null) globalSettingsHolder.BumpGlobalCombinedSize=BumpGlobalCombined.width;
		//Debug.Log ("E"+mat);
		
		// refresh distances & apply material if needed
		UpdateBasemapDistance(true);

		Terrain terrainComp = (Terrain)GetComponent(typeof(Terrain));
		#if UNITY_EDITOR	
		if (terrainComp) {
			GetControlMaps();
		}		
		#endif
		
		if (terrainComp && !globalSettingsHolder.useTerrainMaterial && globalSettingsHolder.numTiles>1 && !mat) {
			//
			// local (stored in splat textures)
			//
			SplatPrototype[] s=terrainComp.terrainData.splatPrototypes;
			if (s.Length<4) {
				Debug.Log("RTP must use at least 4 layers !");
				return;
			}
			#if UNITY_EDITOR	
				if (check_weak_references && !globalSettingsHolder.dont_check_weak_references) {
					string warn="";
					bool gc_weak=false;
					bool sc_weak=false;
					if (ColorGlobal) {
						if (AssetDatabase.GetAssetPath(ColorGlobal)=="") {
							gc_weak=true;
							warn="Your global colormap isn't saved to disk. It's referenced in terrainData and you can loose it  reusing the terrain object.";
						}
					}
					if (BumpGlobalCombined) {
						if (AssetDatabase.GetAssetPath(BumpGlobalCombined)=="") {
							sc_weak=true;
							if (gc_weak) {
								warn+="\n\n";
							}
							warn+="Your special combined texture isn't saved to disk. It's referenced in terrainData and you can loose it  reusing the terrain object.";
						}				
					}
					if (warn!="") {
						switch (EditorUtility.DisplayDialogComplex("RTP notification", warn, "OK, thanks", "Let me save", "Don't bother me again")) {
							case 0:
								break;
							case 1:
								if (gc_weak) {
									globalSettingsHolder.submenu=ReliefTerrainMenuItems.GeneralSettings;
//									globalSettingsHolder.submenu_settings=ReliefTerrainSettingsItems.GlobalColor;
								} else if (sc_weak) {
									globalSettingsHolder.submenu=ReliefTerrainMenuItems.CombinedTextures;
								}
								break;
							case 2:
								globalSettingsHolder.dont_check_weak_references=true;
							break;
						}
					}
				}
			#endif
			if (ColorGlobal) s[0].texture=ColorGlobal;
			if (NormalGlobal) s[1].texture=NormalGlobal;
			// splaty w 8 layers mode sa bezuzyteczne, wiec tryb jest nieuzywany kiedy redefiniujemy textury
			//if (!globalSettingsHolder._4LAYERS_SHADER_USED) {
			//	if (controlB) s[2].texture=controlB;
			//} else {
				if (TreesGlobal) s[2].texture=TreesGlobal;
				if (AmbientEmissiveMap) s[2].texture=AmbientEmissiveMap;
			//}
			if (BumpGlobalCombined) s[3].texture=BumpGlobalCombined;
			
			int add_offset=0;
			if (s.Length>=5 && s.Length<=8 && globalSettingsHolder._4LAYERS_SHADER_USED) {
				add_offset=4;
			} else if (s.Length>8) {
				add_offset=8;
			}
			if (add_offset>0) {
				// add pass present
				if (ColorGlobal) s[0+add_offset].texture=ColorGlobal;
				
				bool crosspass_heightblend=false;
				RTP_LODmanager manager=globalSettingsHolder.Get_RTP_LODmanagerScript();
				if (manager && manager.RTP_CROSSPASS_HEIGHTBLEND) crosspass_heightblend=true;
				if (NormalGlobal) s[1+add_offset].texture=NormalGlobal;
				if (globalSettingsHolder._RTP_LODmanagerScript.RTP_4LAYERS_MODE && crosspass_heightblend) {
					if (controlA) s[2+add_offset].texture=controlA;
				} else {
					if (TreesGlobal) s[2+add_offset].texture=TreesGlobal;
					if (AmbientEmissiveMap) s[2+add_offset].texture=AmbientEmissiveMap;
				}
				if (BumpGlobalCombined) s[3+add_offset].texture=BumpGlobalCombined;
			}
			terrainComp.terrainData.splatPrototypes=s;
		} else {
			globalSettingsHolder.use_mat=mat; // mat==null - 1 tile case (all global) or shader is on mesh
			if (!terrainComp && !mat) {
				// RTP shader on mesh (former ReliefTerrain2Geometry)
				if (GetComponent<Renderer>().sharedMaterial==null || GetComponent<Renderer>().sharedMaterial.name!="RTPMaterial")
				{
					switch (ln) {
					case LayersNumber._4Layers:
						
						GetComponent<Renderer>().sharedMaterial=new Material(Shader.Find("Relief Pack/Terrain2Geometry4Layers"));
						break;
					case LayersNumber._8Layers:
						
						GetComponent<Renderer>().sharedMaterial=new Material(Shader.Find("Relief Pack/Terrain2Geometry8Layers"));
						break;
					case LayersNumber._Transition:
						
						GetComponent<Renderer>().sharedMaterial=new Material(Shader.Find("Relief Pack/Terrain2GeometryTransition"));
						break;
					}

					GetComponent<Renderer>().sharedMaterial.name="RTPMaterial";
				}
				globalSettingsHolder.use_mat=GetComponent<Renderer>().sharedMaterial; // local params to mesh material
			}
			#if !UNITY_3_5
				if (terrainComp) {
					if (globalSettingsHolder.useTerrainMaterial) {
						if (terrainComp.materialTemplate!=null) {
							globalSettingsHolder.use_mat=terrainComp.materialTemplate;
							terrainComp.materialTemplate.SetVector("RTP_CustomTiling", new Vector4(1.0f / customTiling.x, 1.0f / customTiling.y, 0 ,0));
						}
					}
				}
			#endif

//			globalSettingsHolder.SetShaderParam("_ColorMapGlobal", ColorGlobal);
//			globalSettingsHolder.SetShaderParam("_NormalMapGlobal", NormalGlobal);
//			globalSettingsHolder.SetShaderParam("_TreesMapGlobal", TreesGlobal);
//			globalSettingsHolder.SetShaderParam("_BumpMapGlobal", BumpGlobalCombined);
			
			globalSettingsHolder.use_mat=null;
		}
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		// control maps
		//
		RefreshControlMaps(mat);	
		if (mat) {
			mat.SetVector("RTP_CustomTiling", new Vector4(1.0f / customTiling.x, 1.0f / customTiling.y, 0 ,0));
		} else if (terrainComp && !globalSettingsHolder.useTerrainMaterial) {
			if (globalSettingsHolder.numTiles==1) {
				Shader.SetGlobalVector("RTP_CustomTiling", new Vector4(1.0f / customTiling.x, 1.0f / customTiling.y, 0 ,0));
			} else {
				SplatPrototype[] splatPrototypes=terrainComp.terrainData.splatPrototypes;
				splatPrototypes[0].tileSize=Vector2.one;
				splatPrototypes[0].tileOffset=new Vector2(1.0f / customTiling.x, 1.0f / customTiling.y);
				terrainComp.terrainData.splatPrototypes=splatPrototypes;				
			}
		}
		//
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	}
	
	public void RefreshControlMaps(Material mat=null) {
		globalSettingsHolder.use_mat=mat; // mat==null - 1 tile case (all global) or shader is on mesh
		Terrain terrainComp = (Terrain)GetComponent(typeof(Terrain));		
		if (!terrainComp && !mat) {
			globalSettingsHolder.use_mat=GetComponent<Renderer>().sharedMaterial; // local params to mesh material (sharedMaterial made above if needed)
		}
		#if !UNITY_3_5
			if (terrainComp && !mat) {
				if (globalSettingsHolder.useTerrainMaterial) {
					if (terrainComp.materialTemplate!=null) {
						globalSettingsHolder.use_mat=terrainComp.materialTemplate;
					}
				}
			}
		#endif
		
		globalSettingsHolder.SetShaderParam("_Control1", controlA);
		if (globalSettingsHolder.numLayers>4) {
			globalSettingsHolder.SetShaderParam("_Control3", controlB);
			globalSettingsHolder.SetShaderParam("_Control2", controlB); // for FarOnly to work in 4+4 classic mode
//			if (globalSettingsHolder.Get_RTP_LODmanagerScript() && globalSettingsHolder.Get_RTP_LODmanagerScript().RTP_4LAYERS_MODE && terrainComp && !mat) {
//				globalSettingsHolder.SetShaderParam("_Control3", controlB);
//				globalSettingsHolder.SetShaderParam("_Control2", controlB); // for FarOnly to work in 4+4 classic mode
//			} else {
//				globalSettingsHolder.SetShaderParam("_Control2", controlB);
//			}
		}
		if (globalSettingsHolder.numLayers>8) {
			globalSettingsHolder.SetShaderParam("_Control3", controlC);
		}
		if (!terrainComp || globalSettingsHolder.useTerrainMaterial || globalSettingsHolder.numTiles<=1 || mat) {
			globalSettingsHolder.SetShaderParam("_ColorMapGlobal", ColorGlobal);
			globalSettingsHolder.SetShaderParam("_NormalMapGlobal", NormalGlobal);
			globalSettingsHolder.SetShaderParam("_TreesMapGlobal", TreesGlobal);
			globalSettingsHolder.SetShaderParam("_AmbientEmissiveMapGlobal", AmbientEmissiveMap);
			globalSettingsHolder.SetShaderParam("_BumpMapGlobal", BumpGlobalCombined);		
		}
		globalSettingsHolder.use_mat=null;		
	}
	
	public void GetControlMaps() {
		Terrain terrainComp = (Terrain)GetComponent(typeof(Terrain));
		if (!terrainComp) {
			Debug.Log("Can't fint terrain component !!!");
			return;
		}
		Type terrainDataType = terrainComp.terrainData.GetType();
		#if UNITY_5 && !UNITY_5_0_0 && !UNITY_5_0_1
		PropertyInfo info = terrainDataType.GetProperty("alphamapTextures", BindingFlags.Instance | BindingFlags.Public);
		#else
		PropertyInfo info = terrainDataType.GetProperty("alphamapTextures", BindingFlags.Instance | BindingFlags.NonPublic);
		#endif	
		if (info!=null) {
			Texture2D[] alphamapTextures=(Texture2D[])info.GetValue(terrainComp.terrainData, null);
			if (alphamapTextures.Length>0) controlA=alphamapTextures[0]; else controlA=null;
			if (alphamapTextures.Length>1) controlB=alphamapTextures[1]; else controlB=null;
			if (alphamapTextures.Length>2) controlC=alphamapTextures[2]; else controlC=null;
		} else{
			Debug.LogError("Can't access alphamapTexture directly...");
		}
	}
	
#if UNITY_EDITOR	
	public void Update() {
		//EditorApplication.playmodeStateChanged=cl;
		if (!Application.isPlaying) {
			if (controlA) {
				RefreshControlMaps();
			}
		}
	}
	
//	void cl() {
//		if (controlA) {
//			RefreshControlMaps(); 
//		}
//	}
	
    void OnApplicationPause(bool pauseStatus) {
		if (controlA) {
			RefreshControlMaps(); 
		}
    }	




	public void SetCustomControlMaps() {
		Terrain terrainComp = (Terrain)GetComponent(typeof(Terrain));
		if (!terrainComp) {
			Debug.Log("Can't fint terrain component !!!");
			return;
		}
		
		if (controlA==null) {
			return;
		}
		
		if (terrainComp.terrainData.alphamapResolution!=controlA.width) {
			Debug.LogError("Terrain controlmap resolution differs fromrequested control texture...");
			return;
		}
		if (!controlA) return;
		float[,,] splatData=terrainComp.terrainData.GetAlphamaps(0,0,terrainComp.terrainData.alphamapResolution, terrainComp.terrainData.alphamapResolution);
		Color[] cols_control=controlA.GetPixels();
		for(int n=0; n<terrainComp.terrainData.alphamapLayers; n++) {
			int idx=0;
			if (n==4) {
				if (!controlB) return;
				cols_control=controlB.GetPixels();
			} else if (n==8) {
				if (!controlC) return;
				cols_control=controlC.GetPixels();
			}
			int channel_idx=n&3;
			for(int i=0; i<terrainComp.terrainData.alphamapResolution; i++) {
				for(int j=0; j<terrainComp.terrainData.alphamapResolution; j++) {
					splatData[i,j,n]=cols_control[idx++][channel_idx];
				}
			}
		}
		terrainComp.terrainData.SetAlphamaps(0,0, splatData);
	}
	
	public void RecalcControlMaps() {
		Terrain terrainComp = (Terrain)GetComponent(typeof(Terrain));
		if (!terrainComp) {
			Debug.Log("Can't fint terrain component !!!");
			return;
		}
		
		globalSettingsHolder.RecalcControlMaps(terrainComp, this);
		RefreshTextures();
		globalSettingsHolder.Refresh();
	}
	
	public void RecalcControlMapsForMesh() {
		globalSettingsHolder.RecalcControlMapsForMesh(this);
		RefreshTextures();
		globalSettingsHolder.Refresh();
	}
	
	public void InvertChannel(Color[] cols, int channel_idx=-1) {
		if (channel_idx<0) {
			for(int idx=0; idx<cols.Length; idx++) {
				cols[idx].r = 1-cols[idx].r;
				cols[idx].g = 1-cols[idx].g;
				cols[idx].b = 1-cols[idx].b;
				cols[idx].a = 1-cols[idx].a;
			}		
		} else {
			for(int idx=0; idx<cols.Length; idx++) {
				cols[idx][channel_idx] = 1-cols[idx][channel_idx];
			}		
		}
	}
	
	public Texture2D GetSteepnessHeightDirectionTexture(int what=0, GameObject ref_obj=null) {
		Terrain terrainComp = (Terrain)GetComponent(typeof(Terrain));
		if (!terrainComp) {
			Debug.Log("Can't fint terrain component !!!");
			return null;
		}
		Texture2D tex=new Texture2D(terrainComp.terrainData.alphamapResolution, terrainComp.terrainData.alphamapResolution, TextureFormat.RGB24, false);
		int size=terrainComp.terrainData.alphamapResolution;
		Color32[] cols=new Color32[size*size];
		int idx=0;
		float sizef=1.0f/size;
		float[] val_raw_array=new float[size*size];
		if (what==0) {
			// steepness
			for(int j=0; j<size; j++) {
				for(int i=0; i<size; i++) {
					val_raw_array[idx++]=terrainComp.terrainData.GetSteepness(i*sizef, j*sizef);
				}
			}
		} else if (what==1) {
			// height
			float alpha2heightRatio=1.0f*(terrainComp.terrainData.heightmapResolution-1)/size;
			for(int j=0; j<size; j++) {
				for(int i=0; i<size; i++) {
					val_raw_array[idx++]=terrainComp.terrainData.GetHeight( Mathf.RoundToInt(alpha2heightRatio*i), Mathf.RoundToInt(alpha2heightRatio*j) );
				}
			}
		} else {
			// direction
			Vector3 ref_dir=Vector3.forward;
			if (ref_obj) {
				ref_dir=ref_obj.transform.forward;
				if (globalSettingsHolder.flat_dir_ref) {
					ref_dir.y=0;
					if (ref_dir.magnitude<0.0000001f) {
						ref_dir=Vector3.forward;
					} else {
						ref_dir.Normalize();
					}
				}
			}
			if (globalSettingsHolder.flip_dir_ref) ref_dir=-ref_dir;
			for(int j=0; j<size; j++) {
				for(int i=0; i<size; i++) {
					Vector3 dir=terrainComp.terrainData.GetInterpolatedNormal(i*sizef, j*sizef);
					if (globalSettingsHolder.flat_dir_ref) {
						dir.y=0;
						if (dir.magnitude<0.0000001f) {
							val_raw_array[idx++]=0;
						} else {
							dir.Normalize();
							val_raw_array[idx++]=Mathf.Clamp01(Vector3.Dot(dir, ref_dir));
						}
					} else {
						val_raw_array[idx++]=Mathf.Clamp01(Vector3.Dot(dir, ref_dir));
					}
				}
			}
		}
		float min=99999;
		float max=0;
		for(idx=0; idx<val_raw_array.Length; idx++) {
			if (val_raw_array[idx]<min) min=val_raw_array[idx];
			if (val_raw_array[idx]>max) max=val_raw_array[idx];
		}
		float norm_val=1.0f/(max-min);
		for(idx=0; idx<val_raw_array.Length; idx++) {
			float val_raw=(val_raw_array[idx]-min)*norm_val;
			byte val=(byte)Mathf.RoundToInt(255*val_raw);
			cols[idx]=new Color32(val,val,val,1);
		}
		tex.SetPixels32(cols);
		tex.Apply(false);
		return tex;
	}
	



	public int modify_blend(bool _upflag)
	{
		if (globalSettingsHolder.paintHitInfo_flag) {
			switch (globalSettingsHolder.paintMode) {
			case PaintMode.extrude:
				{
					bool isEditing = true;

					int controlId = GUIUtility.GetControlID (FocusType.Passive);
					switch (Event.current.GetTypeForControl (controlId)) {               

					case EventType.MouseDown:
						if (Event.current.button == 0) {                  
							GUIUtility.hotControl = controlId;
							isEditing = true;                        
							Event.current.Use ();
						}

							//Правая кнопка мыши
						if (Event.current.button == 1) {
							if (typeInt == 2) {
								RaycastHit hit;
								Ray ray = HandleUtility.GUIPointToWorldRay (Event.current.mousePosition);
								if (Physics.Raycast (ray, out hit)) {
									tgtHeight = hit.point.y;

								}
								Event.current.Use ();
							}
						}

						break;

					case EventType.MouseUp:
						if (Event.current.button == 0) {
							isEditing = false;
							GUIUtility.hotControl = 0;
							Event.current.Use ();
						}
						break;
					}

					if (isEditing) {                
						RaycastHit hit;

						Ray ray = HandleUtility.GUIPointToWorldRay (Event.current.mousePosition);

						if (Physics.Raycast (ray, out hit)) {

							MeshFilter MF = transform.GetComponent<MeshFilter> ();
							MF.sharedMesh.vertices = EditMesh (MF.sharedMesh, currentTerrain, hit, _upflag);
							MF.sharedMesh.RecalculateNormals();
							MF.sharedMesh.RecalculateBounds();
							transform.GetComponent<MeshCollider>().sharedMesh = MF.sharedMesh;



						}
					}
				}

				break;
			case PaintMode.layer:
				{
					if (prepare_tmpTexture (!globalSettingsHolder.paint_wetmask))
					{
						int w;
						int h;

						Texture2D tex;
						Texture2D tex2;

						tex = controlA;
						tex2 = controlA;		

						w = Mathf.RoundToInt (globalSettingsHolder.paint_size / globalSettingsHolder.terrainTileSize.x * tex.width);
						h = Mathf.RoundToInt (globalSettingsHolder.paint_size / globalSettingsHolder.terrainTileSize.z * tex.height);

							if (w < 1)
								w = 1;
							if (h < 1)
								h = 1;
							int _left = Mathf.RoundToInt (globalSettingsHolder.paintHitInfo.textureCoord.x * tex.width - w);
							if (_left < 0)
								_left = 0;
							w *= 2;
							if (_left + w >= tex.width)
								_left = tex.width - w;
							int _top = Mathf.RoundToInt (globalSettingsHolder.paintHitInfo.textureCoord.y * tex.height - h);
							if (_top < 0)
								_top = 0;
							h *= 2;
							if (_top + h >= tex.height)
								_top = tex.height - h;

						Color[] colsA = tex.GetPixels (_left, _top, w, h);				
						Color[] cols = tex.GetPixels (_left, _top, w, h);
						if (ln == LayersNumber._8Layers) 
						{
							tex2 = controlB;	
							colsA = tex2.GetPixels (_left, _top, w, h);
						}
						Color[] cols2 = new Color[1];

							int idx = 0;
							float d = _upflag ? -1f : 1f;
							float targetBrightness = (globalSettingsHolder.paintColor.r + globalSettingsHolder.paintColor.g + globalSettingsHolder.paintColor.b);
							for (int j = 0; j < h; j++) {
								idx = j * w;
								float disty = (2.0f * j / (h - 1) - 1.0f) * ((h - 1.0f) / h);
								for (int i = 0; i < w; i++) {
									float distx = (2.0f * i / (w - 1) - 1.0f) * ((w - 1.0f) / w);
									float dist = 1.0f - Mathf.Sqrt (distx * distx + disty * disty);
									if (dist < 0)
										dist = 0;
									dist = dist > globalSettingsHolder.paint_smoothness ? 1 : dist / globalSettingsHolder.paint_smoothness;

																								
									float other = 1.0f - globalSettingsHolder.colorOpacity;
									switch (globalSettingsHolder.selectedLayer) {
									case 0:
										globalSettingsHolder.paintColor = new Color (globalSettingsHolder.colorOpacity, other * cols [idx].g, other * cols [idx].b, other * cols [idx].a);
									if (ln == LayersNumber._8Layers)
										globalSettingsHolder.paintColor2 = other * colsA [idx];
										break;
									case 1:
										globalSettingsHolder.paintColor = new Color (other * cols [idx].r, globalSettingsHolder.colorOpacity, other * cols [idx].b, other * cols [idx].a);
									if (ln == LayersNumber._8Layers)
										globalSettingsHolder.paintColor2 = other * colsA [idx];
										break;
									case 2:
										globalSettingsHolder.paintColor = new Color (other * cols [idx].r, other * cols [idx].g, globalSettingsHolder.colorOpacity, other * cols [idx].a);
									if (ln == LayersNumber._8Layers)
										globalSettingsHolder.paintColor2 = other * colsA [idx];
										break;
									case 3:
										globalSettingsHolder.paintColor = new Color (other * cols [idx].r, other * cols [idx].g, other * cols [idx].b, globalSettingsHolder.colorOpacity);
									if (ln == LayersNumber._8Layers)
										globalSettingsHolder.paintColor2 = other * colsA [idx];
										break;

							
									case 4:
										globalSettingsHolder.paintColor2 = new Color (globalSettingsHolder.colorOpacity, other * colsA [idx].g, other * colsA [idx].b, other * colsA [idx].a);
										globalSettingsHolder.paintColor = other * cols [idx];
										break;
									case 5:
										globalSettingsHolder.paintColor2 = new Color (other * colsA [idx].r, globalSettingsHolder.colorOpacity, other * colsA [idx].b, other * colsA [idx].a);
										globalSettingsHolder.paintColor = other * cols [idx];
										break;
									case 6:
										globalSettingsHolder.paintColor2 = new Color (other * colsA [idx].r, other * colsA [idx].g, globalSettingsHolder.colorOpacity, other * colsA [idx].a);
										globalSettingsHolder.paintColor = other * cols [idx];
										break;
									case 7:
										globalSettingsHolder.paintColor2 = new Color (other * colsA [idx].r, other * colsA [idx].g, other * colsA [idx].b, globalSettingsHolder.colorOpacity);
										globalSettingsHolder.paintColor = other * cols [idx];
										break;

									}

									cols [idx] = Color.Lerp (cols [idx], globalSettingsHolder.paintColor, globalSettingsHolder.paint_opacity * dist);
								if (ln == LayersNumber._8Layers)
									colsA [idx] = Color.Lerp (colsA [idx], globalSettingsHolder.paintColor2, globalSettingsHolder.paint_opacity * dist);

								
									idx++;
									
								}
							}

							tex.SetPixels (_left, _top, w, h, cols);
							tex.Apply (true, false);
						if (ln == LayersNumber._8Layers) 
						{
							tex2.SetPixels (_left, _top, w, h, colsA);
							tex2.Apply (true, false);
						}


					} 
					else
					{
						return -2;
					}
				}
				break;
			}
			return 0;
		}
		return 0;
	}
	
	
	public bool prepare_tmpTexture(bool color_flag=true) {
		if (color_flag) {
			if (ColorGlobal) {
				Texture2D colorMap=ColorGlobal;
				if (tmp_globalColorMap!=colorMap) {
					AssetImporter _importer=AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(colorMap));
					bool sRGBflag=false;
					if (_importer) {
						TextureImporter tex_importer=(TextureImporter)_importer;
						sRGBflag=tex_importer.linearTexture;
						bool reimport_flag=false;
						if (!tex_importer.isReadable) {
							Debug.LogWarning("Texture ("+colorMap.name+") has been reimported as readable.");
							tex_importer.isReadable=true;
							reimport_flag=true;
						}
						if (tex_importer.textureFormat!=TextureImporterFormat.ARGB32) {
							Debug.LogWarning("Texture ("+colorMap.name+") has been reimported as as ARGB32.");
							tex_importer.textureFormat=TextureImporterFormat.ARGB32;
							reimport_flag=true;
						}
						if (reimport_flag) {
							AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(colorMap),  ImportAssetOptions.ForceUpdate);
							ColorGlobal=colorMap;
						}
					}
					try { 
						colorMap.GetPixels(0,0,4,4,0);
					} catch (Exception e) {
						Debug.LogError("Global ColorMap has to be marked as isReadable...");
						Debug.LogError(e.Message);
						return false;
					}
					if (colorMap.format==TextureFormat.Alpha8) {
						tmp_globalColorMap=new Texture2D(colorMap.width, colorMap.height, TextureFormat.Alpha8, true, sRGBflag); 
					} else {
						tmp_globalColorMap=new Texture2D(colorMap.width, colorMap.height, TextureFormat.ARGB32, true, sRGBflag); 
					}
					Color[] cols=colorMap.GetPixels();
					tmp_globalColorMap.SetPixels(cols);
					tmp_globalColorMap.Apply(true,false);
					tmp_globalColorMap.wrapMode=TextureWrapMode.Clamp;
					ColorGlobal=tmp_globalColorMap;
					globalColorModifed_flag=true;
					RefreshTextures();
				}
				return true;
			}
			return false;
		} else {
			

			if (BumpGlobalCombined) {
				Texture2D colorMap=BumpGlobalCombined;
				if (tmp_CombinedMap!=colorMap) {
					AssetImporter _importer=AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(colorMap));
					if (_importer) {
						TextureImporter tex_importer=(TextureImporter)_importer;
						bool reimport_flag=false;
						if (!tex_importer.isReadable) {
							Debug.LogWarning("Texture ("+colorMap.name+") has been reimported as readable.");
							tex_importer.isReadable=true;
							reimport_flag=true;
						}
						if (tex_importer.textureFormat!=TextureImporterFormat.ARGB32) {
							Debug.LogWarning("Texture ("+colorMap.name+") has been reimported as as ARGB32.");
							tex_importer.textureFormat=TextureImporterFormat.ARGB32;
							reimport_flag=true;
						}
						if (reimport_flag) {
							AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(colorMap),  ImportAssetOptions.ForceUpdate);
							BumpGlobalCombined=colorMap;
						}
					}
					try { 
						colorMap.GetPixels(0,0,4,4,0);
					} catch (Exception e) {
						Debug.LogError("Special texture (perlin+water/reflection) has to be marked as isReadable...");
						Debug.LogError(e.Message);
						return false;
					}
					tmp_CombinedMap=new Texture2D(colorMap.width, colorMap.height, TextureFormat.ARGB32, true, true);
					Color[] cols=colorMap.GetPixels();
					tmp_CombinedMap.SetPixels(cols);
					tmp_CombinedMap.Apply(true,false);
					tmp_CombinedMap.wrapMode=TextureWrapMode.Repeat;
					BumpGlobalCombined=tmp_CombinedMap;
					globalCombinedModifed_flag=true;
					RefreshTextures();
				}
				return true;
			}
			return false;			
		}
	}
	
	public void RestorePreset(ReliefTerrainPresetHolder holder) {
		controlA=holder.controlA;
		controlB=holder.controlB;
		controlC=holder.controlC;
	
		SetCustomControlMaps();
		
		ColorGlobal=holder.ColorGlobal;

		// local textures to splat textures
		RefreshTextures();
		
		// restore global settigns		
		globalSettingsHolder.RestorePreset(holder);
	}
	
	public void SavePreset(ref ReliefTerrainPresetHolder holder) {
		Terrain terrainComp = (Terrain)GetComponent(typeof(Terrain));
		if (terrainComp) {
			if (controlA) holder.controlA=UnityEngine.Object.Instantiate(controlA) as Texture2D;
			if (controlB) holder.controlB=UnityEngine.Object.Instantiate(controlB) as Texture2D;
			if (controlC) holder.controlC=UnityEngine.Object.Instantiate(controlC) as Texture2D;
		} else {
			holder.controlA=controlA;
			holder.controlB=controlB;
			holder.controlC=controlC;
		}
		
		holder.ColorGlobal=ColorGlobal;

		// store global settigns		
		globalSettingsHolder.SavePreset(ref holder);
	}	

	public static float[,] ConformTerrain2Detail(TerrainData terrainData, Texture2D hn_tex, bool bicubic_flag) {
		float[,] heights = new float[terrainData.heightmapResolution, terrainData.heightmapResolution];
		float ntexel_size_u = 1.0f / hn_tex.width;
		float ntexel_size_v = 1.0f / hn_tex.height;
		float mult_u = (1.0f / (terrainData.heightmapResolution-1)) * (1-ntexel_size_u);
		float mult_v = (1.0f / (terrainData.heightmapResolution-1)) * (1-ntexel_size_v);
		float off_u=ntexel_size_u*0.5f;
		float off_v=ntexel_size_v*0.5f;
		for(int _x = 0; _x<terrainData.heightmapResolution; _x++) {
			float u=_x*mult_u+off_u;
			for(int _z = 0; _z<terrainData.heightmapResolution; _z++) {
				float v=_z*mult_v+off_v;
				if (bicubic_flag) {
					Vector3 norm=Vector3.zero; // not used
					heights[_z,_x] = GeometryVsTerrainBlend.interpolate_bicubic(u,v, hn_tex, ref norm);
				} else {
					Color col = GeometryVsTerrainBlend.CustGetPixelBilinear(hn_tex, u,v);
					heights[_z,_x] = ((1.0f/255)*col.g + col.r);
				}
			}
		}
		return heights;
	}

	public static float[,] ConformTerrain2Occlusion(TerrainData terrainData, Texture2D hn_tex) {
		float[,] heights = new float[terrainData.heightmapResolution, terrainData.heightmapResolution];

		float _ratio = 1.0f*hn_tex.width/terrainData.heightmapResolution;
		int sampleSize = hn_tex.width/terrainData.heightmapResolution;
		if (sampleSize<1) sampleSize=1;
		for(int _x = 0; _x<terrainData.heightmapResolution; _x++) {
			int _xB=Mathf.FloorToInt(_x*_ratio);
			for(int _z = 0; _z<terrainData.heightmapResolution; _z++) {
				float gVal=0;
				float rVal=0;
				int _zB=Mathf.FloorToInt(_z*_ratio);
				for(int _xS = 0; _xS<sampleSize; _xS++) {
					for(int _zS = 0; _zS<sampleSize; _zS++) {
						Color col = hn_tex.GetPixel(_xB+_xS, _zB+_zS);
						if (gVal<col.g) gVal=col.g;
						if (rVal<col.r) rVal=col.r;
					}
				}
				heights[_z,_x] = ((1.0f/255)*gVal + rVal)+0.02f; // little offset to be sure about occlusion
			}
		}
		return heights;
	}

#endif

	public ReliefTerrainPresetHolder GetPresetByID(string PresetID) {
		if (presetHolders!=null) {
			for(int i=0; i<presetHolders.Length; i++) {
				if (presetHolders[i].PresetID==PresetID) {
					return presetHolders[i];
				}
			}
		}
		return null;
	}
	
	public ReliefTerrainPresetHolder GetPresetByName(string PresetName) {
		if (presetHolders!=null) {
			for(int i=0; i<presetHolders.Length; i++) {
				if (presetHolders[i].PresetName==PresetName) {
					return presetHolders[i];
				}
			}
		}
		return null;
	}	
	
	public bool InterpolatePresets(string PresetID1, string PresetID2, float t) {
		ReliefTerrainPresetHolder holderA=GetPresetByID(PresetID1);
		ReliefTerrainPresetHolder holderB=GetPresetByID(PresetID2);
		if (holderA==null || holderB==null || holderA.Spec==null || holderB.Spec==null || holderA.Spec.Length!=holderB.Spec.Length) {
			return false;
		}
		globalSettingsHolder.InterpolatePresets(holderA, holderB, t);
		return true;
	}	

	#if UNITY_EDITOR
	public void GetGlobalUV()
	{
		
		MeshFilter MF = transform.gameObject.GetComponent<MeshFilter> ();
		Mesh mesh = new Mesh ();
		mesh = MF.sharedMesh;
		mesh.name = "Mesh";
		mesh.uv2 = new Vector2[mesh.vertexCount];
		Vector2[] uv = new Vector2[mesh.vertexCount];
		for (int i = 0; i < mesh.vertexCount; i++) {
			uv [i] = new Vector2 (mesh.vertices [i].z / 10, mesh.vertices [i].x / 10);
		}
		mesh.uv2 = uv;
		MF.mesh = mesh;

		currentTerrain = new Mesh ();
		currentTerrain.vertices = mesh.vertices;
		currentTerrain.triangles =  mesh.triangles;
		currentTerrain.uv = mesh.uv;
		currentTerrain.uv2 = mesh.uv2;
		currentTerrain.normals = mesh.normals;
		currentTerrain.colors32 = mesh.colors32;
		currentTerrain.tangents = mesh.tangents;
		currentTerrain.subMeshCount = mesh.subMeshCount;
		for (int i = 0; i < mesh.subMeshCount; i++)
			currentTerrain.SetTriangles (mesh.GetTriangles(i), i);
		currentTerrain.RecalculateNormals ();



	}

	public void RefreshAll(ReliefTerrain targetRT)
	{
		switch (targetRT.ln) {
		case LayersNumber._4Layers:
			{
				//ReliefTerrainGlobalSettingsHolder target = targetRT.globalSettingsHolder;

				PrepareAtlases (0);
				targetRT.globalSettingsHolder.PrepareNormals ();
				PrepareHeights (0);

				Material mat = targetRT.GetComponent<Renderer> ().sharedMaterial;
				targetRT.RefreshTextures ();
				targetRT.globalSettingsHolder.Refresh (mat, targetRT);

			} 
			break;
		case LayersNumber._8Layers:
			{
				//ReliefTerrainGlobalSettingsHolder target = targetRT.globalSettingsHolder;
				for (int j = 0; j < 2; j++) {
					PrepareAtlases (j);
					targetRT.globalSettingsHolder.PrepareNormals ();
					PrepareHeights (j * 4);

				}
				Material mat = targetRT.GetComponent<Renderer> ().sharedMaterial;
				Debug.Log (mat.name);
				targetRT.RefreshTextures ();
				targetRT.globalSettingsHolder.Refresh (mat, targetRT);
			}
			break;
		case LayersNumber._Transition:
			{
				for (int j = 0; j < 2; j++) {
					PrepareAtlases (j);
					targetRT.globalSettingsHolder.PrepareNormals ();
					PrepareHeights (j * 4);

				}
				Material mat = targetRT.GetComponent<Renderer> ().sharedMaterial;
				Debug.Log (mat.name);
				targetRT.RefreshTextures ();
				targetRT.globalSettingsHolder.Refresh (mat, targetRT);
			}
			break;
		}
		//targetRT.globalSettingsHolder.RefreshAll ();

	}

	public void RefreshDiffuse(ReliefTerrain targetRT, int n, int atlas)
	{
		ReliefTerrainGlobalSettingsHolder target = targetRT.globalSettingsHolder;
		target.gloss_baked [n] = ScriptableObject.CreateInstance (typeof(RTPGlossBaked)) as RTPGlossBaked; // nie mamy inf. o zmodyfikowanym roughness dla tej tekstury
		target.splats_glossBaked [n] = null; // nie mamy też tekstury ze zmodyfikowanymi MIP levelami (będa używane splats[i])
		target.atlas_glossBaked [n / 4] = null; // używany atlas jest nieważny
		PrepareAtlases ( atlas);
		target.Refresh (null, targetRT);
	}

	public void RefreshNormal(ReliefTerrain targetRT, int n)
	{
		ReliefTerrainGlobalSettingsHolder target = targetRT.globalSettingsHolder;
		target.gloss_baked [n] = ScriptableObject.CreateInstance (typeof(RTPGlossBaked)) as RTPGlossBaked; // nie mamy inf. o zmodyfikowanym roughness dla tej tekstury
		target.splats_glossBaked [n] = null; // nie mamy też tekstury ze zmodyfikowanymi MIP levelami (będa używane splats[i])
		target.atlas_glossBaked [n / 4] = null; // używany atlas jest nieważny
		target.PrepareNormals ();
		target.Refresh (null, targetRT);
	}

	public void RefreshHeight(ReliefTerrain targetRT, int n)
	{
		ReliefTerrainGlobalSettingsHolder target = targetRT.globalSettingsHolder;
		PrepareHeights (n);
		target.Refresh (null, targetRT);
	}

	public void PrepareAtlases (int which)
	{
		ReliefTerrainGlobalSettingsHolder _target = globalSettingsHolder;

		int min_size = 9999;
		bool any_splat = false;
		for (int n = which * 4; (n < which * 4 + 4) && (n < _target.numLayers); n++) {
			if (_target.splats [n]) { // czasem może byc błędnie wprowadzony jako null
				if (_target.splats [n].width < min_size)
					min_size = _target.splats [n].width;
				any_splat = true;
			}		
		}
		if (!any_splat)
			min_size = 512;
		for (int n = which * 4; (n < which * 4 + 4) && (n < _target.numLayers); n++) {
			AssetImporter _importer = AssetImporter.GetAtPath (AssetDatabase.GetAssetPath (_target.splats [n]));
			if (_importer) {
				TextureImporter tex_importer = (TextureImporter)_importer;
				bool reimport_flag = false;
				if (!tex_importer.isReadable) {
					Debug.LogWarning ("Detail texture " + n + " (" + _target.splats [n].name + ") has been reimported as readable.");
					tex_importer.isReadable = true;
					reimport_flag = true;
				}
				if (_target.splats [n] && _target.splats [n].width > min_size) {
					Debug.LogWarning ("Detail texture " + n + " (" + _target.splats [n].name + ") has been reimported with " + min_size + " size.");
					tex_importer.maxTextureSize = min_size;
					reimport_flag = true;
				}
				if (reimport_flag) {
					AssetDatabase.ImportAsset (AssetDatabase.GetAssetPath (_target.splats [n]), ImportAssetOptions.ForceUpdate);
				}
			}					
		}				

		int i;
		for (i = 0; i < _target.numLayers; i++) {
			try { 
				_target.splats [i].GetPixels (0, 0, 4, 4, 0);
			} catch (Exception e) {
				Debug.LogError ("Splat texture " + i + " has to be marked as isReadable...");
				Debug.LogError (e.Message);
				return;
			}
		}
		int w = _target.splats [0].width;
		for (i = 1; i < _target.numLayers; i++) {
			if (_target.splats [i].width != w) {
				Debug.LogError ("For performance reasons - all splats (detail textures) should have the same size");
				Debug.LogError ("Detail tex 0 size=" + w + " while detail tex " + i + " size=" + _target.splats [i].width);
				return;
			}
		}

		ResetProgress ((4 + 8), "Create atlases " + which.ToString());

		int num = which * 4;
		Texture2D[] splats = new Texture2D[4];
		for (i = 0; i < splats.Length; i++) {
			if (num < _target.numLayers && _target.splats [num] != null) {
				splats [i] = _target.splats [num];
				splats [i] = PadTex (splats [i], 16);
				CheckProgress ();							
			} else {
				splats [i] = new Texture2D (min_size, min_size);
			}
			num++;
		}
		progress_description = "Packing textures";

		if (_target.splat_atlases [which] && AssetDatabase.GetAssetPath (_target.splat_atlases [which]) == "") {
			DestroyImmediate (_target.splat_atlases [which]);
		}

		_target.atlas_glossBaked [which] = null;
		_target.splat_atlases [which] = new Texture2D (splats [0].width * 2, splats [0].width * 2);
		_target.splat_atlases [which].wrapMode = TextureWrapMode.Clamp;
		_target.splat_atlases [which].PackTextures (new Texture2D[4] { splats [0], splats [1], splats [2], splats [3] }, 0, _target.splat_atlases [which].width, false);
		progress_description = "Blending corners";
		CheckProgress ();
		//if (splats[0].width<=128) BlendMip(_target.splat_atlases[0], 3, 0.5f);
		//if (splats[0].width<=256) BlendMip(_target.splat_atlases[0], 4, 0.5f);
		//if (splats[0].width<=512) BlendMip(_target.splat_atlases[0], 5, 0.5f);
		BlendMip (_target.splat_atlases [which], 0, 0.5f, 16);
		BlendMip (_target.splat_atlases [which], 0, 0.5f, 15);
		CheckProgress ();					
		BlendMip (_target.splat_atlases [which], 1, 0.5f, 8);
		BlendMip (_target.splat_atlases [which], 1, 0.5f, 7);
		CheckProgress ();					
		BlendMip (_target.splat_atlases [which], 2, 0.5f, 4);
		BlendMip (_target.splat_atlases [which], 2, 0.5f, 3);
		CheckProgress ();					
		BlendMip (_target.splat_atlases [which], 3, 0.5f, 2);
		BlendMip (_target.splat_atlases [which], 3, 0.5f, 1);
		CheckProgress ();					
		BlendMip (_target.splat_atlases [which], 4, 0.5f, 2);
		BlendMip (_target.splat_atlases [which], 4, 0.5f, 1);
		CheckProgress ();					
		BlendMip (_target.splat_atlases [which], 5, 0.5f, 1);
		BlendMip (_target.splat_atlases [which], 5, 0.5f, 0);
		CheckProgress ();					
		for (i = 6; i < _target.splat_atlases [which].mipmapCount - 2; i++)
			BlendMip (_target.splat_atlases [which], i, 0.5f);
		progress_description = "Packing textures";
		CheckProgress ();					
		_target.splat_atlases [which].Compress (true);
		_target.splat_atlases [which].Apply (false, false);
		_target.splat_atlases [which].filterMode = FilterMode.Trilinear;
		_target.splat_atlases [which].anisoLevel = 0;

		for (i = 0; i < splats.Length; i++) {
			DestroyImmediate (splats [i]);
		}
		EditorUtility.ClearProgressBar ();

	}


	private void ResetProgress (int progress_count, string _progress_description = "")
	{
		progress_count_max = progress_count;
		progress_count_current = 0;
		progress_description = _progress_description;
	}

	public Texture2D PadTex (Texture2D splat, int padding, bool updateMips = true)
	{
		int size = splat.width;
		int size_padded = size - padding * 2;

		ResamplingService resamplingService = new ResamplingService ();
		resamplingService.Filter = ResamplingFilters.Lanczos3;
		ushort[][,] input = ConvertTextureToArray ((Texture2D)splat);
		ushort[][,] output = resamplingService.Resample (input, size_padded, size_padded);
		for (int c = 0; c < 4; c++) {
			for (int i = 0; i < size_padded; i++) {
				for (int j = 0; j < size_padded; j++) {
					if (output [c] [i, j] > 255)
						output [c] [i, j] = 255;
				}
			}
		}

		Color32[] cols = new Color32[size * size];
		for (int j = 0; j < size; j++) {
			int j_idx1 = j * size;
			int j_idx2 = ((j + size_padded - padding) % size_padded);
			for (int i = 0; i < size; i++) {
				int i2 = (i + size_padded - padding) % size_padded;
				cols [j_idx1].r = (byte)output [0] [i2, j_idx2];
				cols [j_idx1].g = (byte)output [1] [i2, j_idx2];
				cols [j_idx1].b = (byte)output [2] [i2, j_idx2];
				cols [j_idx1].a = (byte)output [3] [i2, j_idx2];
				j_idx1++;
			}
		}
		if (updateMips) {
			Texture2D tex = new Texture2D (size, size, TextureFormat.ARGB32, true);
			tex.SetPixels32 (cols);
			tex.Apply (true, false);
			return tex;
		} else {
			Texture2D tex = new Texture2D (size, size, TextureFormat.ARGB32, false);
			tex.SetPixels32 (cols);
			tex.Apply (false, false);
			return tex;
		}
	}

	private void CheckProgress ()
	{
		if (((progress_count_current++) % progress_granulation) == (progress_granulation - 1)) {
			EditorUtility.DisplayProgressBar ("Processing...", progress_description, 1.0f * progress_count_current / progress_count_max);
		}

	}

	public ushort[][,] ConvertTextureToArray (Texture2D tex)
	{
		ushort[][,] tab = new ushort[4][,];
		for (int i = 0; i < 4; i++)
			tab [i] = new ushort[tex.width, tex.height];

		Color32[] cols = tex.GetPixels32 ();
		for (int j = 0; j < tex.height; j++) {
			int idx = j * tex.width;
			for (int i = 0; i < tex.width; i++) {
				tab [0] [i, j] = cols [idx].r;
				tab [1] [i, j] = cols [idx].g;
				tab [2] [i, j] = cols [idx].b;
				tab [3] [i, j] = cols [idx].a;
				idx++;
			}
		}
		return tab;
	}

	public void BlendMip (Texture2D atlas, int mip, float blend, int pad_offset = 0)
	{
		Color32[] cols = atlas.GetPixels32 (mip);
		int size = Mathf.FloorToInt (Mathf.Sqrt ((float)cols.Length));
		int half_size = size / 2;
		int idx_offA, idx_offB;
		idx_offA = pad_offset * size;
		idx_offB = (half_size - 1 - pad_offset) * size;
		for (int i = 0; i < size; i++) {
			Color32 colA = cols [idx_offA];
			Color32 colB = cols [idx_offB];
			cols [idx_offA] = Color32.Lerp (colA, colB, blend);
			cols [idx_offB] = Color32.Lerp (colB, colA, blend);
			idx_offA++;
			idx_offB++;
		}
		idx_offA = (half_size + pad_offset) * size;
		idx_offB = (size - 1 - pad_offset) * size;
		for (int i = 0; i < size; i++) {
			Color32 colA = cols [idx_offA];
			Color32 colB = cols [idx_offB];
			cols [idx_offA] = Color32.Lerp (colA, colB, blend);
			cols [idx_offB] = Color32.Lerp (colB, colA, blend);
			idx_offA++;
			idx_offB++;
		}		
		idx_offA = pad_offset;
		idx_offB = half_size - 1 - pad_offset;
		for (int i = 0; i < size; i++) {
			Color32 colA = cols [idx_offA];
			Color32 colB = cols [idx_offB];
			cols [idx_offA] = Color32.Lerp (colA, colB, blend);
			cols [idx_offB] = Color32.Lerp (colB, colA, blend);
			idx_offA += size;
			idx_offB += size;
		}
		idx_offA = half_size + pad_offset;
		idx_offB = size - 1 - pad_offset;
		for (int i = 0; i < size; i++) {
			Color32 colA = cols [idx_offA];
			Color32 colB = cols [idx_offB];
			cols [idx_offA] = Color32.Lerp (colA, colB, blend);
			cols [idx_offB] = Color32.Lerp (colB, colA, blend);
			idx_offA += size;
			idx_offB += size;
		}
		atlas.SetPixels32 (cols, mip);
	}

	public bool PrepareHeights (int num)
	{
		ReliefTerrainGlobalSettingsHolder _target = globalSettingsHolder;

		Texture2D[] Heights = _target.Heights;
		if (Heights == null)
			return false;

		for (int n = 0; n < ((Heights.Length < _target.numLayers) ? Heights.Length : _target.numLayers); n++) {
			int min_size = 9999;
			for (int m = (n / 4) * 4; (m < ((n / 4) * 4 + 4)) && (m < Heights.Length); m++) {
				if (Heights [m]) {
					if (Heights [m].width < min_size)
						min_size = Heights [m].width;
				}
			}		
			AssetImporter _importer = AssetImporter.GetAtPath (AssetDatabase.GetAssetPath (Heights [n]));
			if (_importer) {
				TextureImporter tex_importer = (TextureImporter)_importer;
				bool reimport_flag = false;
				if (!tex_importer.isReadable) {
					Debug.LogWarning ("Height texture " + n + " (" + Heights [n].name + ") has been reimported as readable.");
					tex_importer.isReadable = true;
					reimport_flag = true;
				}
				if (!tex_importer.DoesSourceTextureHaveAlpha () && !tex_importer.grayscaleToAlpha) {
					Debug.LogWarning ("Height texture " + n + " (" + Heights [n].name + ") has been reimported to have alpha channel.");
					tex_importer.grayscaleToAlpha = true;
					tex_importer.textureFormat = TextureImporterFormat.Alpha8;
					reimport_flag = true;
				}
				if (Heights [n] && Heights [n].width > min_size) {
					Debug.LogWarning ("Height texture " + n + " (" + Heights [n].name + ") has been reimported with " + min_size + " size.");
					tex_importer.maxTextureSize = min_size;
					reimport_flag = true;
				}
				if (reimport_flag) {
					AssetDatabase.ImportAsset (AssetDatabase.GetAssetPath (Heights [n]), ImportAssetOptions.ForceUpdate);
				}
			}					
		}			

		Texture2D[] heights = new Texture2D[4];
		int i;
		int _w = 256;
		int _w_idx = 0;
		int len = _target.numLayers < 12 ? _target.numLayers : 12;
		if (num >= len)
			return false;
		num = (num >> 2) * 4;
		for (i = num; i < num + 4; i++) {
			if (num < 4) {
				heights [i] = i < Heights.Length ? Heights [i] : null;
				if (heights [i]) {
					_w = heights [i].width;
					_w_idx = i;
				}
			} else if (num < 8) {
				heights [i - 4] = i < Heights.Length ? Heights [i] : null;
				if (heights [i - 4]) {
					_w = heights [i - 4].width;
					_w_idx = i;
				}
			} else {
				heights [i - 8] = i < Heights.Length ? Heights [i] : null;
				if (heights [i - 8]) {
					_w = heights [i - 8].width;
					_w_idx = i;
				}
			}
		}
		for (i = 0; i < 4; i++) {
			if (!heights [i]) {
				heights [i] = new Texture2D (_w, _w);
				FillTex (heights [i], new Color32 (255, 255, 255, 255));
			}
			if (heights [i]) {
				try { 
					heights [i].GetPixels (0, 0, 4, 4, 0);
				} catch (Exception e) {
					Debug.LogError ("Height texture " + i + " has to be marked as isReadable...");
					Debug.LogError (e.Message);
					_target.activateObject = heights [i];
					return false;
				}
				if (heights [i].width != _w) {
					Debug.LogError ("Height textures should have the same size ! (check layer " + _w_idx + " and " + (num + i) + ")");
					_target.activateObject = heights [i];
					return false;
				}
			} else {
				heights [i] = new Texture2D (_w, _w);
				FillTex (heights [i], new Color32 (255, 255, 255, 255));
			}
		}

		if (num < 4) {
			_target.HeightMap = CombineHeights (heights [0], heights [1], heights [2], heights [3]);
		} else if (num < 8) {
			_target.HeightMap2 = CombineHeights (heights [0], heights [1], heights [2], heights [3]);
		} else if (num < 12) {
			_target.HeightMap3 = CombineHeights (heights [0], heights [1], heights [2], heights [3]);
		}
		return true;
	}

	public static void FillTex (Texture2D tex, Color32 col)
	{
		Color32[] cols = tex.GetPixels32 ();
		for (int i = 0; i < cols.Length; i++) {
			cols [i].r = col.r;
			cols [i].g = col.g;
			cols [i].b = col.b;
			cols [i].a = col.a;
		}
		tex.SetPixels32 (cols);
	}

	private Texture2D CombineHeights (Texture2D source_tex0, Texture2D source_tex1, Texture2D source_tex2, Texture2D source_tex3)
	{
		Texture2D rendered_tex = new Texture2D (source_tex0.width, source_tex0.height, TextureFormat.ARGB32, true, true);
		byte[] colsR = get_alpha_channel (source_tex0);
		byte[] colsG = get_alpha_channel (source_tex1);
		byte[] colsB = get_alpha_channel (source_tex2);
		byte[] colsA = get_alpha_channel (source_tex3);
		int len = colsR.Length;
		if ((colsG.Length != len) || (colsB.Length != len) || (colsA.Length != len)) {
	#if UNITY_EDITOR
			EditorUtility.DisplayDialog ("Error", "All detail heightmaps should have THE SAME size. Check it and try again.", "OK");
	#endif
		}
		Color32[] cols = rendered_tex.GetPixels32 ();
		for (int i = 0; i < cols.Length; i++) {
			cols [i].r = colsR [i];
			cols [i].g = colsG [i];
			cols [i].b = colsB [i];
			cols [i].a = colsA [i];
		}
		rendered_tex.SetPixels32 (cols);
		rendered_tex.Apply (true, false);
		//rendered_tex.Compress(true);
		//rendered_tex.Apply(true, true); // (non readable robione przy publishingu)
		rendered_tex.filterMode = FilterMode.Trilinear;
		return rendered_tex;
	}

	private byte[] get_alpha_channel (Texture2D source_tex)
	{
		Color32[] cols = source_tex.GetPixels32 ();
		byte[] ret = new byte[cols.Length];
		for (int i = 0; i < cols.Length; i++)
			ret [i] = cols [i].a;
		return ret;
	}






	public void Initialize()
	{


		string[] look = new string[]{ "Assets/Editor/Presets" };
		string[] pathsPresets = AssetDatabase.FindAssets("t:ScriptableObject", look);
		presets = new Presets[pathsPresets.Length];
		for (int i = 0; i < pathsPresets.Length; i++) 
		{
			ReliefTerrainPresetHolder data = ScriptableObject.CreateInstance<ReliefTerrainPresetHolder> ();
			data = AssetDatabase.LoadAssetAtPath<ReliefTerrainPresetHolder>(AssetDatabase.GUIDToAssetPath(pathsPresets[i]));
			presets[i] = new Presets(AssetDatabase.GUIDToAssetPath(pathsPresets[i]), data.ico, data.spriteAtlas, data.ln, data.PresetName);

		}


	}

	public void get_paint_coverage ()
	{
		if (EditorApplication.timeSinceStartup < lCovTim)
			return;
		lCovTim = EditorApplication.timeSinceStartup + 0.02;
		ReliefTerrainGlobalSettingsHolder _target = globalSettingsHolder;
		if (!GetComponent<Collider> ())
			return;

		Ray ray = HandleUtility.GUIPointToWorldRay (Event.current.mousePosition);
		paintHitInfo_flag = GetComponent<Collider> ().Raycast (ray, out paintHitInfo, Mathf.Infinity);
		_target.paintHitInfo = paintHitInfo;
		_target.paintHitInfo_flag = paintHitInfo_flag;


		EditorUtility.SetDirty (this);
	}
	

	private Vector3[] EditMesh(Mesh mesh, Mesh current, RaycastHit hit, bool upflag)
	{
		//foreach (Transform trans in GoWithMesh)
		//{
		Vector3[] vertices = mesh.vertices;

		int sum = 0;
		float average = 0;
		float min = 0;
		float max = 0;

		if (typeInt == 2) {
			for (int j = 0; j < vertices.Length; j++) 
			{
				Vector2 pos = transform.TransformPoint (vertices [j]).ToVector2XZ ();
				float r = Vector2.Distance (pos, hit.point.ToVector2XZ ()) / brushRadius;
				if (r < 1.0f) 
				{
					average += vertices [j].y;
					sum++;
				}
			}	
			average /= sum;

		}
		for (int j = 0; j < vertices.Length; j++)
		{
			Vector2 pos = transform.TransformPoint (vertices [j]).ToVector2XZ ();
			float r = Vector2.Distance (pos, hit.point.ToVector2XZ ())/brushSize;
			if (r < 1.0f)
			{
				switch (typeInt)
				{
				case 0:
					vertices[j].y += globalSettingsHolder.paint_opacity * (1.0f-r) * (upflag == true ? 1 : -1);
					break;
				case 1:
					if (vertices[j].y > tgtHeight)
					{
						if (vertices[j].y - globalSettingsHolder.paint_opacity < tgtHeight) vertices[j].y = tgtHeight;
						else vertices[j].y -= globalSettingsHolder.paint_opacity;
					}
					else
					{
						if (vertices[j].y + globalSettingsHolder.paint_opacity > tgtHeight) vertices[j].y = tgtHeight;
						else vertices[j].y += globalSettingsHolder.paint_opacity;
					}

					break;
				case 2:
					vertices [j].y = Mathf.Lerp (vertices [j].y, average, (1.0f-r) * globalSettingsHolder.paint_opacity);
					break;

				
				case 3:
					vertices [j].y = Mathf.Lerp (vertices [j].y, current.vertices[j].y, (1.0f-r) * globalSettingsHolder.paint_opacity);
				break;
			
			}


			}
		}
		return vertices;

		
	


	}

	public void RestoreMesh()
	{
		MeshFilter MF = transform.gameObject.GetComponent<MeshFilter> ();
		MF.sharedMesh = new Mesh ();

		MF.sharedMesh.vertices = currentTerrain.vertices;
		MF.sharedMesh.triangles =  currentTerrain.triangles;
		MF.sharedMesh.uv = currentTerrain.uv;
		MF.sharedMesh.uv2 = currentTerrain.uv2;
		MF.sharedMesh.normals = currentTerrain.normals;
		MF.sharedMesh.colors32 = currentTerrain.colors32;
		MF.sharedMesh.tangents = currentTerrain.tangents;
		MF.sharedMesh.subMeshCount = currentTerrain.subMeshCount;
		for (int i = 0; i < currentTerrain.subMeshCount; i++)
			MF.sharedMesh.SetTriangles (currentTerrain.GetTriangles(i), i);
		MF.sharedMesh.RecalculateNormals ();

		PrefabCollector pc = GameObject.FindObjectOfType<PrefabCollector> ();
		pc.current.terrain = MF.sharedMesh;
		pc.SavePreset ();

	}

	public void SaveMesh()
	{
		MeshFilter MF = transform.gameObject.GetComponent<MeshFilter> ();

		PrefabCollector pc = GameObject.FindObjectOfType<PrefabCollector> ();
		pc.SaveMesh (MF.sharedMesh);



	}
#endif
}
