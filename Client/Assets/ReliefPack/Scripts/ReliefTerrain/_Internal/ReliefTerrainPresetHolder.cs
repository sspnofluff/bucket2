using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;

[System.Serializable]
public class ReliefTerrainPresetHolder:ScriptableObject {
	public string PresetID;
	public string PresetName;
	public string type;

	public LayersNumber ln;
	public Texture2D ico;
	public Texture2D spriteAtlas;
	
	public int numLayers;
	public Texture2D[] splats;
	public RTPGlossBaked[] gloss_baked=new RTPGlossBaked[12];		
	public Texture2D[] splat_atlases;
	
	public Texture2D controlA;
	public Texture2D controlB;
	public Texture2D controlC;
	
	public float RTP_MIP_BIAS;

	public Color _SpecColor;
	public float RTP_DeferredAddPassSpec=0.5f;
	
	public float MasterLayerBrightness=1;
	public float MasterLayerSaturation=1;

	public Texture2D Bump01;
	public Texture2D Bump23;
	public Texture2D Bump45;
	public Texture2D Bump67;
	public Texture2D Bump89;
	public Texture2D BumpAB;
	
	// per terrain object - not part of preset (preset stores only global, shared params)
	public Texture2D ColorGlobal;

	public float _FarNormalDamp;

	public float blendMultiplier;
	
	public Texture2D HeightMap;
	public Texture2D HeightMap2;
	public Texture2D HeightMap3;
	
	public Vector4 ReliefTransform;
	public float DIST_STEPS;
	public float ReliefBorderBlend;

	public float ExtrudeHeight;
	public float LightmapShading;
	
	public float SHADOW_STEPS;
	public float WAVELENGTH_SHADOWS;
	//public float SHADOW_SMOOTH_STEPS; // not used since RTP3.2d
	public float SelfShadowStrength;
	public float ShadowSmoothing;	
	public float ShadowSoftnessFade=0.7f; // sice RTP3.2d
	
	public float distance_start;
	public float distance_transition;


	public float tessHeight=300;
	public float _TessSubdivisions = 1;
	public float _TessSubdivisionsFar = 1;
	public float _TessYOffset = 0;
	

	public float RTP_AOamp;
	public float RTP_AOsharpness;
	
	//////////////////////
	// layer_dependent arrays
	//////////////////////
	public Texture2D[] Bumps;
	public float[] Spec;

	
	// RTP3.1
	public float[] RTP_gloss2mask;
	public float[] RTP_gloss_mult;
	public float[] RTP_gloss_shaping;

	public float[] _DeferredSpecDampAddPass;
		

	public float[] LayerBrightness;
	public float[] LayerBrightness2Spec;
	public float[] LayerAlbedo2SpecColor;
	public float[] LayerSaturation;

	
	public float[] AO_strength;
	public float[] TessStrenght;
	

	public Texture2D[] Heights;

	public float[] _snow_strength_per_layer;
	#if !UNITY_WEBGL || UNITY_EDITOR
	public ProceduralMaterial[] Substances;
	#endif
	

	


	public void Init(string name) {
		PresetID=""+Random.value+Time.realtimeSinceStartup;
		PresetName=name;
	}

	public ReliefTerrainPresetHolder Merge(string name, ReliefTerrainPresetHolder p1, ReliefTerrainPresetHolder p2)
	{
		ReliefTerrainPresetHolder pFinal = new ReliefTerrainPresetHolder ();
		pFinal.Init (name);

		pFinal.numLayers = 8;
		pFinal.splats = new Texture2D[8];
		pFinal.gloss_baked=new RTPGlossBaked[12];		
		pFinal.splat_atlases = new Texture2D[3];
		pFinal.splat_atlases [0] = p1.splat_atlases [0];
		pFinal.splat_atlases [1] = p2.splat_atlases [0];

		pFinal.controlA = p1.controlA;
		pFinal.controlB = p2.controlA;

		pFinal.RTP_MIP_BIAS = p1.RTP_MIP_BIAS;
		//public float TessStrenghtOne;
		pFinal._SpecColor = p1._SpecColor;
		pFinal.RTP_DeferredAddPassSpec = p1.RTP_DeferredAddPassSpec;

		pFinal.MasterLayerBrightness = p1.MasterLayerBrightness;
		pFinal.MasterLayerSaturation = p1.MasterLayerSaturation;

		pFinal.Bump01 = p1.Bump01;
		pFinal.Bump23 = p1.Bump23;
		pFinal.Bump45 = p2.Bump01;
		pFinal.Bump67 = p2.Bump23;

		pFinal.blendMultiplier = p1.blendMultiplier;

		pFinal.HeightMap = p1.HeightMap;
		pFinal.HeightMap2 = p2.HeightMap;

		pFinal.ReliefTransform = p1.ReliefTransform;
			pFinal.DIST_STEPS = p1.DIST_STEPS;
			pFinal.ReliefBorderBlend = p1.ReliefBorderBlend;

		pFinal.ExtrudeHeight = p1.ExtrudeHeight;
		pFinal.LightmapShading = p1.LightmapShading;

		pFinal.SHADOW_STEPS = p1.SHADOW_STEPS;
		pFinal.WAVELENGTH_SHADOWS = p1.WAVELENGTH_SHADOWS;
		//public float SHADOW_SMOOTH_STEPS; // not used since RTP3.2d
		pFinal.SelfShadowStrength = p1.SelfShadowStrength;
		pFinal.ShadowSmoothing = p1.ShadowSmoothing;
		pFinal.ShadowSoftnessFade = p1.ShadowSoftnessFade;

		pFinal.distance_start = p1.distance_start;
		pFinal.distance_transition = p1.distance_transition;
		pFinal.tessHeight = p1.tessHeight;
		pFinal._TessSubdivisions = p1._TessSubdivisions;
		pFinal._TessSubdivisionsFar = p1._TessSubdivisionsFar;
		pFinal._TessYOffset = p1._TessYOffset;

	

			pFinal.RTP_AOamp = p1.RTP_AOamp;
			pFinal.RTP_AOsharpness = p1.RTP_AOsharpness;

		pFinal.Bumps = new Texture2D[8];


		pFinal.Spec = new float[8];
	
		// RTP3.1
		pFinal.RTP_gloss2mask = new float[8];
		pFinal.RTP_gloss_mult = new float[8];
		pFinal.RTP_gloss_shaping = new float[8];

		pFinal._DeferredSpecDampAddPass = new float[8];

		pFinal.LayerBrightness = new float[8];
		pFinal.LayerBrightness2Spec = new float[8];
		pFinal.LayerAlbedo2SpecColor = new float[8];
		pFinal.LayerSaturation = new float[8];
	
		pFinal.AO_strength = new float[8];
		pFinal.TessStrenght = new float[8];

		pFinal.Heights = new Texture2D[8];

			for (int i=0; i<4; i++)
			{
			pFinal.splats [i] = p1.splats [i];
			pFinal.Bumps [i] = p1.Bumps [i];
			pFinal.Heights [i] = p1.Heights [i];
			pFinal.Spec [i] = p1.Spec [i];
		
				pFinal.RTP_gloss2mask[i] = p1.RTP_gloss2mask[i];
				pFinal.RTP_gloss_mult[i] = p1.RTP_gloss_mult[i];
				pFinal.RTP_gloss_shaping[i] = p1.RTP_gloss_shaping[i];

				pFinal._DeferredSpecDampAddPass[i] = p1._DeferredSpecDampAddPass[i];

				pFinal.LayerBrightness[i] = p1.LayerBrightness[i];
				pFinal.LayerBrightness2Spec[i] = p1.LayerBrightness2Spec[i];
				pFinal.LayerAlbedo2SpecColor[i] = p1.LayerAlbedo2SpecColor[i];
				pFinal.LayerSaturation[i] = p1.LayerSaturation[i];
		
				pFinal.AO_strength[i] = p1.AO_strength[i];
				pFinal.TessStrenght[i] = p1.TessStrenght[i];
			}

		for (int i=0; i<4; i++)
		{
			pFinal.splats [i+4] = p2.splats [i];
			pFinal.Bumps [i+4] = p2.Bumps [i];
			pFinal.Heights [i+4] = p2.Heights [i];
			pFinal.Spec [i+4] = p2.Spec [i];
		
			pFinal.RTP_gloss2mask[i+4] = p2.RTP_gloss2mask[i];
			pFinal.RTP_gloss_mult[i+4] = p2.RTP_gloss_mult[i];
			pFinal.RTP_gloss_shaping[i+4] = p2.RTP_gloss_shaping[i];

			pFinal._DeferredSpecDampAddPass[i+4] = p2._DeferredSpecDampAddPass[i];

			pFinal.LayerBrightness[i+4] = p2.LayerBrightness[i];
			pFinal.LayerBrightness2Spec[i+4] = p2.LayerBrightness2Spec[i];
			pFinal.LayerAlbedo2SpecColor[i+4] = p2.LayerAlbedo2SpecColor[i];
			pFinal.LayerSaturation[i+4] = p2.LayerSaturation[i];

			pFinal.AO_strength[i+4] = p2.AO_strength[i];
			pFinal.TessStrenght[i+4] = p2.TessStrenght[i];
		}
		// wet

		// snow


		return pFinal;
	}
	
#if UNITY_EDITOR
//	private void CheckAssetRef(Texture2D tex, string[] warning_msgs, ref int cnt, string msg) {
//		if (tex && AssetDatabase.GetAssetPath(tex)=="") {
//			warning_msgs[cnt]=msg;
//			cnt++;
//		}
//	}
//
//	public void CheckUnsavedTextures() {
//		string[] warning_msgs=new string[100];
//		int cnt=0;
//		if (splats!=null) {
//			for(int i=0; i<splats.Length; i++) {
//				CheckAssetRef(splats[i], warning_msgs, ref cnt, "Detail diffuse/spec texture for layer "+i);
//			}
//		}
//		if (splat_atlases!=null) {
//			for(int i=0; i<splat_atlases.Length; i++) {
//				CheckAssetRef(splat_atlases[i], warning_msgs, ref cnt, "Detail atlas texture "+((i==0) ? "A":"B"));
//			}
//		}
//		if (Bumps!=null) {
//			for(int i=0; i<Bumps.Length; i++) {
//				CheckAssetRef(Bumps[i], warning_msgs, ref cnt, "Normals texture for layer "+i);
//			}
//		}
//		if (Heights!=null) {
//			for(int i=0; i<Heights.Length; i++) {
//				CheckAssetRef(Heights[i], warning_msgs, ref cnt, "Heights texture for layer "+i);
//			}
//		}
//	
//		CheckAssetRef(controlA, warning_msgs, ref cnt, "Control (coverage) map 0");
//		CheckAssetRef(controlB, warning_msgs, ref cnt, "Control (coverage) map 1");
//		CheckAssetRef(controlC, warning_msgs, ref cnt, "Control (coverage) map 2");
//		
//		CheckAssetRef(Bump01, warning_msgs, ref cnt, "Combined normalmaps 0+1");
//		CheckAssetRef(Bump23, warning_msgs, ref cnt, "Combined normalmaps 2+3");
//		CheckAssetRef(Bump45, warning_msgs, ref cnt, "Combined normalmaps 4+5");
//		CheckAssetRef(Bump67, warning_msgs, ref cnt, "Combined normalmaps 6+7");
//		CheckAssetRef(Bump89, warning_msgs, ref cnt, "Combined normalmaps 8+9");
//		CheckAssetRef(BumpAB, warning_msgs, ref cnt, "Combined normalmaps 10+11");
//		
//		CheckAssetRef(ColorGlobal, warning_msgs, ref cnt, "Global colormap");
//		CheckAssetRef(NormalGlobal, warning_msgs, ref cnt, "Global normalmap");
//		CheckAssetRef(TreesGlobal, warning_msgs, ref cnt, "Global pixel trees/shadow map");
//	
//		CheckAssetRef(SSColorCombined, warning_msgs, ref cnt, "Simple mode combined greyscale details");
//	
//		CheckAssetRef(BumpGlobal, warning_msgs, ref cnt, "Perlin normals texture");
//		CheckAssetRef(BumpGlobalCombined, warning_msgs, ref cnt, "Special perlin combined texture (with optional water mask&reflections).");
//	
//		CheckAssetRef(VerticalTexture, warning_msgs, ref cnt, "Vertical texture");
//		
//		CheckAssetRef(HeightMap, warning_msgs, ref cnt, "Combined heightmaps 0");
//		CheckAssetRef(HeightMap2, warning_msgs, ref cnt, "Combined heightmaps 1");
//		CheckAssetRef(HeightMap3, warning_msgs, ref cnt, "Combined heightmaps 2");
//	
//		CheckAssetRef(SuperDetailA, warning_msgs, ref cnt, "Superdetail mult mask A");
//		CheckAssetRef(SuperDetailB, warning_msgs, ref cnt, "Superdetail mult mask B");
//	
//		CheckAssetRef(TERRAIN_ReflectionMap, warning_msgs, ref cnt, "Reflection map");
//		CheckAssetRef(TERRAIN_RippleMap, warning_msgs, ref cnt, "Water ripple animation texture");
//		CheckAssetRef(TERRAIN_WetMask, warning_msgs, ref cnt, "Water wetmask");
//				
//		CheckAssetRef(TERRAIN_CausticsTex, warning_msgs, ref cnt, "Caustics texture");
//
//		if (cnt>0) {
//			Debug.LogWarning("List of textures that are not saved as assets in project - they'll be missing in saved preset:");
//		}
//		for(int i=0; i<cnt; i++) {
//			Debug.LogWarning(warning_msgs[i]);
//		}
//	}
#endif
}
