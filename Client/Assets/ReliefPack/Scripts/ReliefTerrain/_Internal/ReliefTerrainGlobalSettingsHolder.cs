using System;
using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Flags]
public enum PaintMode
{
	
	extrude = 0,
	layer = 1,
}

[System.Serializable]
public class ReliefTerrainGlobalSettingsHolder {

	public LayersNumber ln;
	public bool useTerrainMaterial=true;
	public int numTiles=0;
	public string currentPreset = "";
	public int numLayers;

	[System.NonSerialized] public bool dont_check_weak_references=false;
	[System.NonSerialized] public bool dont_check_for_interfering_terrain_replacement_shaders=false;
	[System.NonSerialized] public Texture2D[] splats_glossBaked=new Texture2D[12];
	[System.NonSerialized] public Texture2D[] atlas_glossBaked=new Texture2D[3];
	
	public RTPGlossBaked[] gloss_baked=new RTPGlossBaked[12];
	
	public Texture2D[] splats;
	public Texture2D[] splat_atlases=new Texture2D[3];
	public string save_path_atlasA="";
	public string save_path_atlasB="";
	public string save_path_atlasC="";
	public string save_path_terrain_steepness="";
	public string save_path_terrain_height="";
	public string save_path_terrain_direction="";
	public string save_path_Bump01="";
	public string save_path_Bump23="";
	public string save_path_Bump45="";
	public string save_path_Bump67="";
	public string save_path_Bump89="";
	public string save_path_BumpAB="";
	public string save_path_HeightMap="";
	public string save_path_HeightMap2="";
	public string save_path_HeightMap3="";
	public string save_path_SSColorCombinedA="";
	public string save_path_SSColorCombinedB="";
	
	public string newPresetName="a preset name...";
	
	public Texture2D activateObject;
	private GameObject _RTP_LODmanager;
	
	public RTP_LODmanager _RTP_LODmanagerScript;
	
	public bool super_simple_active=false;
	public float RTP_MIP_BIAS=0;
	public float tessStrenght; 
	public Color _SpecColor;
	public float RTP_DeferredAddPassSpec=0.5f;
	
	public float MasterLayerBrightness=1;
	public float MasterLayerSaturation=1;

	public Texture2D Bump01;
	public Texture2D Bump23;
	public Texture2D Bump45;
	public Texture2D Bump67;
	public Texture2D Bump89;
	public Texture2D BumpAB;
	public Texture2D BumpGlobal;
	public int BumpGlobalCombinedSize=1024;
	
	public float _FarNormalDamp;
	
	public float blendMultiplier;
	
	public Vector3 terrainTileSize;

	public Texture2D HeightMap;
	public Vector4 ReliefTransform;
	public float DIST_STEPS;
	public float WAVELENGTH;
	public float ReliefBorderBlend;
	
	public float ExtrudeHeight;
	public float LightmapShading;
	
	public float RTP_AOsharpness;
	public float RTP_AOamp;
	public bool colorSpaceLinear;
	
	public float SHADOW_STEPS;
	public float WAVELENGTH_SHADOWS;
	//public float SHADOW_SMOOTH_STEPS; // not used in RTP3.2d
	public float SelfShadowStrength;
	public float ShadowSmoothing;
	public float ShadowSoftnessFade=0.8f; // new in RTP3.2d
	
	public float distance_start;
	public float distance_transition;
	public float distance_start_bumpglobal;
	public float distance_transition_bumpglobal;

	public float _Phong=0;
	public float tessHeight=300;
	public float _TessSubdivisions=1;
	public float _TessSubdivisionsFar=1;
	public float _TessYOffset=0;

	public Texture2D HeightMap2;
	public Texture2D HeightMap3;

//
	public Color rtp_customAmbientCorrection;
	public Cubemap _CubemapDiff;
	public float TERRAIN_IBL_DiffAO_Damp=0.25f;
	public Cubemap _CubemapSpec;
	public float TERRAIN_IBLRefl_SpecAO_Damp=0.5f;
	//	

	//////////////////////
	// layer_dependent arrays
	//////////////////////
	public Texture2D[] Bumps;
	public float[] Spec; // RTP3.1 - mnożnik o.Gloss (range 0-4)
	public float[] MIPmult;
	public float[] TessStrenght;

	
	///////////////////////////////////////////////////
	// RTP3.1
	public float[] RTP_gloss2mask;
	public float[] RTP_gloss_mult;
	public float[] RTP_gloss_shaping;

	public float[] _DeferredSpecDampAddPass;
	// adv global colormap blending
	///////////////////////////////////////////////////
	
	public float[] LayerBrightness;
	public float[] LayerBrightness2Spec;
	public float[] LayerAlbedo2SpecColor;
	public float[] LayerSaturation;

	
	public float[] AO_strength=new float[12]{1,1,1,1,1,1,1,1,1,1,1,1};
	
	public Texture2D[] Heights;
	
	#if !UNITY_WEBGL || UNITY_EDITOR
	public ProceduralMaterial[] Substances;
	#endif
	
	public bool _4LAYERS_SHADER_USED=false;
	
	public bool flat_dir_ref=true;
	public bool flip_dir_ref=true;
	public GameObject direction_object;
	
	public bool show_details=false;
	public bool show_details_main=false;
	public bool show_details_atlasing=false;
	public bool show_details_layers=false;
	public bool show_details_uv_blend=false;
	
	public bool show_controlmaps=false;
	public bool show_controlmaps_build=false;
	public bool show_controlmaps_helpers=false;
	public bool show_controlmaps_highcost=false;
	public bool show_controlmaps_splats=false;
	
	public bool show_vert_texture=false;
	
	public bool show_global_color=false;
	
	public bool show_snow=false;
	
	public bool show_global_bump=false;
	public bool show_global_bump_normals=false;
	public bool show_global_bump_superdetail=false;
	
	public ReliefTerrainMenuItems submenu=(ReliefTerrainMenuItems)(0);
	public ReliefTerrainSettingsItems submenu_settings=(ReliefTerrainSettingsItems)(0);
	public bool show_global_wet_settings=false;
	public bool show_global_reflection_settings=false;
	public int show_active_layer=0;
	
	public bool show_derivedmaps=false;
	
	public bool show_settings=false;
	
	// paint
	public bool undo_flag=false;
	public bool paint_flag=false;

	public float paint_size=0.5f;
	public float paint_smoothness=0;
	public float paint_opacity=1;
	public Color paintColor=new Color(0.5f,0.3f,0,0);
	public Color paintColor2=new Color(0.5f,0.3f,0,0);
	public bool preserveBrightness=true;
	//public bool paint_alpha_flag=false;
	public bool paint_wetmask=false;
	public PaintMode paintMode = PaintMode.layer;
	public bool isFirst = true;
	public int selectedLayer = 0;
	public float colorOpacity;
	//private Transform underlying_transform;
	//private MeshRenderer underlying_renderer;
	public RaycastHit paintHitInfo;
	public bool paintHitInfo_flag;
	public string bakeName = "";
	public string bakeType = "";

	public bool cut_holes=false;
	
	private Texture2D dumb_tex;
	
	public Color[] paintColorSwatches;
	public Color[] paintColorSwatches2;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// constructor - init arrays
	//
	public ReliefTerrainGlobalSettingsHolder() {
		const int cnt=12;
		
		gloss_baked=new RTPGlossBaked[cnt];
		
		Bumps=new Texture2D[cnt];
		Heights=new Texture2D[cnt];
		
		Spec=new float[cnt];
		MIPmult=new float[cnt];	
		TessStrenght=new float[cnt];	

		
		// RTP3.1
		RTP_gloss2mask=new float[cnt];
		RTP_gloss_mult=new float[cnt];
		RTP_gloss_shaping=new float[cnt];
		_DeferredSpecDampAddPass=new float[cnt];

		LayerBrightness=new float[cnt];
		LayerBrightness2Spec=new float[cnt];
		LayerAlbedo2SpecColor=new float[cnt];
		LayerSaturation=new float[cnt];

		#if !UNITY_WEBGL || UNITY_EDITOR
		Substances=new ProceduralMaterial[cnt];
		#endif


		AO_strength=new float[cnt];
	
		#if UNITY_EDITOR
		ReturnToDefaults();
		#endif
	}	
	
	public void ReInit(Terrain terrainComp) {
		if (terrainComp.terrainData.splatPrototypes.Length>numLayers) {
			Texture2D[] splats_new=new Texture2D[terrainComp.terrainData.splatPrototypes.Length];
			for(int i=0; i<splats.Length; i++) splats_new[i]=splats[i];
			splats=splats_new;
			splats[terrainComp.terrainData.splatPrototypes.Length-1]=terrainComp.terrainData.splatPrototypes[((terrainComp.terrainData.splatPrototypes.Length-2) >=0) ? (terrainComp.terrainData.splatPrototypes.Length-2) : 0].texture;
		} else if (terrainComp.terrainData.splatPrototypes.Length<numLayers) {
			Texture2D[] splats_new=new Texture2D[terrainComp.terrainData.splatPrototypes.Length];
			for(int i=0; i<splats_new.Length; i++) splats_new[i]=splats[i];
			splats=splats_new;
		}
		numLayers=terrainComp.terrainData.splatPrototypes.Length;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public Material use_mat=null;
	public void SetShaderParam(string name, Texture2D tex) {
		if (!tex) return;
		if (use_mat) {
			use_mat.SetTexture(name, tex);
		} else {
			Shader.SetGlobalTexture(name, tex);
		}
	}
	public void SetShaderParam(string name, Cubemap tex) {
		if (!tex) return;
		if (use_mat) {
			use_mat.SetTexture(name, tex);
		} else {
			Shader.SetGlobalTexture(name, tex);
		}
	}
	public void SetShaderParam(string name, Matrix4x4 mtx) {
		if (use_mat) {
			use_mat.SetMatrix(name, mtx);
		} else {
			Shader.SetGlobalMatrix(name, mtx);
		}
	}	
	public void SetShaderParam(string name, Vector4 vec) {
		if (use_mat) {
			use_mat.SetVector(name, vec);
		} else {
			Shader.SetGlobalVector(name, vec);
		}
	}
	public void SetShaderParam(string name, float val) {
		if (use_mat) {
			use_mat.SetFloat(name, val);
		} else {
			Shader.SetGlobalFloat(name, val);
		}
	}
	public void SetShaderParam(string name, Color col) {
		if (use_mat) {
			use_mat.SetColor(name, col);
		} else {
			Shader.SetGlobalColor(name, col);
		}
	}
	
	public RTP_LODmanager Get_RTP_LODmanagerScript() {
		return _RTP_LODmanagerScript;
	}	
	
	public void ApplyGlossBakedTexture(string shaderParamName, int i) {
		if (gloss_baked==null || gloss_baked.Length==0) {
			gloss_baked=new RTPGlossBaked[12];
		}
		if (splats_glossBaked[i]==null) {
			// nie mamy przygotowanej zmodyfikowanej textury
//			if (gloss_baked[i]!=null) {
//			Debug.Log ("R"+i);
//			}
			if ( (gloss_baked[i]!=null) && (!gloss_baked[i].used_in_atlas) && gloss_baked[i].CheckSize(splats[i]) ) {
				// mamy przygotowany gloss - zrób texturę tymczasową
				splats_glossBaked[i]=gloss_baked[i].MakeTexture(splats[i]);
				// i zapodaj shaderowi
				SetShaderParam(shaderParamName, splats_glossBaked[i]);
			} else {
				// nie mamy dostępu do inf. o zmodyfikowanym gloss dla tej warstwy
				SetShaderParam(shaderParamName, splats[i]);
			}
		} else {
			SetShaderParam(shaderParamName, splats_glossBaked[i]);
		}
	}
	
	public void ApplyGlossBakedAtlas(string shaderParamName, int atlasNum) {
		if (gloss_baked==null || gloss_baked.Length==0) {
			gloss_baked=new RTPGlossBaked[12];
		}
		if (atlas_glossBaked[atlasNum]==null) {
			if (splat_atlases[atlasNum]==null) return;
			// nie mamy przygotowanego zmodyfikowanego atlasa
			//			if (gloss_baked[i]!=null) {
			//			Debug.Log ("R"+i);
			//			}
			bool somethings_baked=false;
			for(int tile=0; tile<4; tile++) {
				int i=atlasNum*4+tile;
				if ( (gloss_baked[i]!=null) && gloss_baked[i].used_in_atlas && gloss_baked[i].CheckSize(splats[i]) ) {
					somethings_baked=true;
				}
			}
			if (somethings_baked) {
				RTPGlossBaked[] glBakedTMP=new RTPGlossBaked[4];
				for(int tile=0; tile<4; tile++) {
					int i=atlasNum*4+tile;
					if ( (gloss_baked[i]!=null) && gloss_baked[i].used_in_atlas && gloss_baked[i].CheckSize(splats[i]) ) {
						glBakedTMP[tile]=gloss_baked[i];
					} else {
						glBakedTMP[tile]=ScriptableObject.CreateInstance(typeof(RTPGlossBaked)) as RTPGlossBaked;
						glBakedTMP[tile].Init(splats[i].width);
						glBakedTMP[tile].GetMIPGlossMapsFromAtlas(splat_atlases[atlasNum], tile); // gloss mipmapy bezpośrednio skopiowane z kawałka atlasa
						glBakedTMP[tile].used_in_atlas=true;
					}
				}
				// połącz elementy
				atlas_glossBaked[atlasNum]=RTPGlossBaked.MakeTexture(splat_atlases[atlasNum], glBakedTMP);
				// i zapodaj shaderowi
				SetShaderParam(shaderParamName, atlas_glossBaked[atlasNum]);
			} else {
				// żaden element atlasa nie jest zbake'owany - użyj oryginału
				SetShaderParam(shaderParamName, splat_atlases[atlasNum]);
			}
		} else {
			SetShaderParam(shaderParamName, atlas_glossBaked[atlasNum]);
		}
	}	

	private void CheckLightScriptForDefered() {
		Light[] lights = GameObject.FindObjectsOfType<Light>();
		Light aLight = null;
		for (int i=0; i<lights.Length; i++) {
			if (lights[i].type==LightType.Directional) {
				if ((lights[i].gameObject.GetComponent<ReliefShaders_applyLightForDeferred>())==null) {
					// potential light to attach the script
					aLight=lights[i];
				} else {
					// at least one light with the script attached
					return;
				}
			}
		}
		if (aLight) {
			// at least one directional light found and none of directional lights have the script attached
			// attach component then
			ReliefShaders_applyLightForDeferred comp = aLight.gameObject.AddComponent(typeof(ReliefShaders_applyLightForDeferred)) as ReliefShaders_applyLightForDeferred;
			comp.lightForSelfShadowing=aLight;
		}
	}

	public void RefreshAll() {
		Debug.Log ("RefreshALL");
		CheckLightScriptForDefered();

		ReliefTerrain[] rts=GameObject.FindObjectsOfType(typeof(ReliefTerrain)) as ReliefTerrain[];
		for(int i=0; i<rts.Length; i++) 
		{
			if (rts[i].globalSettingsHolder!=null) 
			{
				Terrain ter=rts[i].GetComponent(typeof(Terrain)) as Terrain;
				if (ter) 
				{
#if UNITY_3_5
					rts[i].globalSettingsHolder.Refresh();
#else
					rts[i].globalSettingsHolder.Refresh(ter.materialTemplate);
#endif
				} else 
				{
					Debug.Log ("Mat " + rts [i].GetComponent<Renderer> ().sharedMaterial.name);
					rts[i].globalSettingsHolder.Refresh(rts[i].GetComponent<Renderer>().sharedMaterial);
				}
				rts[i].RefreshTextures();
			}
		}
		GeometryVsTerrainBlend[] blnd=GameObject.FindObjectsOfType(typeof(GeometryVsTerrainBlend)) as GeometryVsTerrainBlend[];
		for(int i=0; i<blnd.Length; i++) {
			blnd[i].SetupValues();
		}
	}
	
	public void Refresh(Material mat=null, ReliefTerrain rt_caller=null) {
		if (splats==null) return;
		#if UNITY_EDITOR
		if (_RTP_LODmanager==null) {
			if ((_RTP_LODmanager=GameObject.Find("_RTP_LODmanager"))==null) {
				_RTP_LODmanager=new GameObject("_RTP_LODmanager");
				_RTP_LODmanager.AddComponent(typeof(RTP_LODmanager));
				_RTP_LODmanager.AddComponent(typeof(RTPFogUpdate));
				_RTP_LODmanagerScript=(RTP_LODmanager)_RTP_LODmanager.GetComponent(typeof(RTP_LODmanager));
				EditorUtility.DisplayDialog("RTP Notification", "_RTP_LODmanager object added to the scene.\nIts script handles LOD properties of RTP shaders.","OK");
				Selection.activeObject=_RTP_LODmanager;
			}
		}
		if (_RTP_LODmanagerScript==null) {
			_RTP_LODmanagerScript=(RTP_LODmanager)_RTP_LODmanager.GetComponent(typeof(RTP_LODmanager));
		}
		_4LAYERS_SHADER_USED=_RTP_LODmanagerScript.RTP_4LAYERS_MODE;
		
		colorSpaceLinear = ( UnityEditor.PlayerSettings.colorSpace==ColorSpace.Linear );
		#endif
		// switch for SetShaderParam - when use_mat defined we're injecting param into material
#if !UNITY_3_5
		if (mat==null && rt_caller!=null) {
			if (rt_caller.globalSettingsHolder==this) {
				Terrain ter=rt_caller.GetComponent(typeof(Terrain)) as Terrain;
				if (ter) {
					rt_caller.globalSettingsHolder.Refresh(ter.materialTemplate);
				} else {
					if (rt_caller.GetComponent<Renderer>()!=null && rt_caller.GetComponent<Renderer>().sharedMaterial!=null) {
						rt_caller.globalSettingsHolder.Refresh(rt_caller.GetComponent<Renderer>().sharedMaterial);
					}
				}
			}
		}
#endif
		use_mat=mat;
		
		for(int i=0; i<numLayers; i++) {
			if (i<4) {
				ApplyGlossBakedTexture("_SplatA"+i, i);
			} else if (i<8) {
				if (_4LAYERS_SHADER_USED) {
					ApplyGlossBakedTexture("_SplatC"+(i-4), i);
					// potrzebne przy sniegu (firstpass moze korzystac z koloru i bumpmap 4-7)
					ApplyGlossBakedTexture("_SplatB"+(i-4), i);
				} else {
					ApplyGlossBakedTexture("_SplatB"+(i-4), i);
				}
			} else if (i<12) {
				ApplyGlossBakedTexture("_SplatC"+(i-8), i);
			} 
		}
		
		// > RTP3.1
		// update-set to default
		if (CheckAndUpdate(ref RTP_gloss2mask, 0.5f, numLayers)) {
			for(int k=0; k<numLayers; k++) {
				Spec[k]=1; // zresetuj od razu mnożnik glossa (RTP3.1 - zmienna ma inne znaczenie)
			}
		}
		CheckAndUpdate(ref RTP_gloss_mult, 1f, numLayers);
		CheckAndUpdate(ref RTP_gloss_shaping, 0.5f, numLayers);
		CheckAndUpdate(ref _DeferredSpecDampAddPass, 1f, numLayers);
		

		CheckAndUpdate(ref LayerBrightness, 1.0f, numLayers);
		CheckAndUpdate(ref LayerBrightness2Spec, 0.0f, numLayers);
		CheckAndUpdate(ref LayerAlbedo2SpecColor, 0.0f, numLayers);
		CheckAndUpdate(ref LayerSaturation, 1.0f, numLayers);

		
		/////////////////////////////////////////////////////////////////////
		//
		// layer independent
		//
		/////////////////////////////////////////////////////////////////////
		
		// custom fog (unity's fog doesn't work with this shader - too many texture interpolators)
		if (RenderSettings.fog) {
			Shader.SetGlobalFloat("_Fdensity", RenderSettings.fogDensity);
			if (colorSpaceLinear) {
				Shader.SetGlobalColor("_FColor", RenderSettings.fogColor.linear);
			} else {
				Shader.SetGlobalColor("_FColor", RenderSettings.fogColor);
			}
			Shader.SetGlobalFloat("_Fstart", RenderSettings.fogStartDistance);
			Shader.SetGlobalFloat("_Fend", RenderSettings.fogEndDistance);
		} else {
			Shader.SetGlobalFloat("_Fdensity", 0);
			Shader.SetGlobalFloat("_Fstart", 1000000);
			Shader.SetGlobalFloat("_Fend", 2000000);
		}


		SetShaderParam("terrainTileSize", terrainTileSize);
		
		SetShaderParam("RTP_AOamp", RTP_AOamp);
		SetShaderParam("RTP_AOsharpness", RTP_AOsharpness);
		

		SetShaderParam("_RTP_MIP_BIAS", RTP_MIP_BIAS);
		//SetShaderParam("_TessStrenght0123", TessStrenghtOne);

		SetShaderParam("_FarNormalDamp", _FarNormalDamp);
		
		SetShaderParam("_SpecColor", _SpecColor);
		SetShaderParam("RTP_DeferredAddPassSpec", RTP_DeferredAddPassSpec);
		
		SetShaderParam("_blend_multiplier", blendMultiplier);
		SetShaderParam("_TERRAIN_ReliefTransform", ReliefTransform);
		
		SetShaderParam("_TERRAIN_ReliefTransformTriplanarZ", ReliefTransform.x);
		SetShaderParam("_TERRAIN_DIST_STEPS", DIST_STEPS);
		SetShaderParam("_TERRAIN_WAVELENGTH", WAVELENGTH);
		
		SetShaderParam("_TERRAIN_ExtrudeHeight", ExtrudeHeight);
		SetShaderParam("_TERRAIN_LightmapShading", LightmapShading);
		
		SetShaderParam("_TERRAIN_SHADOW_STEPS", SHADOW_STEPS);
		SetShaderParam("_TERRAIN_WAVELENGTH_SHADOWS", WAVELENGTH_SHADOWS);
		//SetShaderParam("_TERRAIN_SHADOW_SMOOTH_STEPS", SHADOW_SMOOTH_STEPS); // not used since RTP3.2d
		
		SetShaderParam("_TERRAIN_SelfShadowStrength", SelfShadowStrength);
		SetShaderParam("_TERRAIN_ShadowSmoothing", (1-ShadowSmoothing)*6); // changed range in RTP3.2d
		SetShaderParam("_TERRAIN_ShadowSoftnessFade", ShadowSoftnessFade); // new in RTP3.2d

		SetShaderParam("_TERRAIN_distance_start", distance_start);
		SetShaderParam("_TERRAIN_distance_transition", distance_transition);
		
		SetShaderParam("_TERRAIN_distance_start_bumpglobal", distance_start_bumpglobal);
		SetShaderParam("_TERRAIN_distance_transition_bumpglobal", distance_transition_bumpglobal);

		SetShaderParam("_Phong", _Phong);
		SetShaderParam("_TessSubdivisions", _TessSubdivisions);
		SetShaderParam("_TessSubdivisionsFar", _TessSubdivisionsFar);
		SetShaderParam("_TessYOffset", _TessYOffset);

		if (numLayers>0) {
			int tex_width=512;
			for(int i=0; i<numLayers; i++) {
				if (splats[i]) {
					tex_width=splats[i].width;
					break;
				}
			}
			SetShaderParam("rtp_mipoffset_color", -Mathf.Log(1024.0f/tex_width)/Mathf.Log(2) );
			if (Bump01!=null) {
				tex_width=Bump01.width;
			}
			SetShaderParam("rtp_mipoffset_bump", -Mathf.Log(1024.0f/tex_width)/Mathf.Log(2));
			if (HeightMap) {
				tex_width=HeightMap.width;
			} else if (HeightMap2) {
				tex_width=HeightMap2.width; 
			} else if (HeightMap3) {
				tex_width=HeightMap3.width;
			}
			SetShaderParam("rtp_mipoffset_height", -Mathf.Log(1024.0f/tex_width)/Mathf.Log(2));
			
			tex_width=BumpGlobalCombinedSize;
		}
		

		Shader.SetGlobalVector("rtp_customAmbientCorrection", new Vector4(rtp_customAmbientCorrection.r-0.2f, rtp_customAmbientCorrection.g-0.2f, rtp_customAmbientCorrection.b-0.2f, 0)*0.1f);
		SetShaderParam("_CubemapDiff", _CubemapDiff);
		SetShaderParam("_CubemapSpec", _CubemapSpec);
		


		/////////////////////////////////////////////////////////////////////
		//
		// layer dependent numeric
		//
		/////////////////////////////////////////////////////////////////////
		float[] tmp_RTP_gloss_mult=new float[RTP_gloss_mult.Length];
		for(int k=0; k<tmp_RTP_gloss_mult.Length; k++) {
			if (gloss_baked[k]!=null && gloss_baked[k].baked) {
				tmp_RTP_gloss_mult[k]=1;
			} else {
				tmp_RTP_gloss_mult[k]=RTP_gloss_mult[k];
			}
		}
		float[] tmp_RTP_gloss_shaping=new float[RTP_gloss_shaping.Length];
		for(int k=0; k<tmp_RTP_gloss_shaping.Length; k++) {
			if (gloss_baked[k]!=null && gloss_baked[k].baked) {
				tmp_RTP_gloss_shaping[k]=0.5f;
			} else {
				tmp_RTP_gloss_shaping[k]=RTP_gloss_shaping[k];
			}
		}
		SetShaderParam("_Spec0123", getVector(Spec, 0,3));
		SetShaderParam("_MIPmult0123", getVector(MIPmult, 0,3));
		SetShaderParam("_TessStrenght0123", getVector(TessStrenght, 0,3));

		
		// RTP3.1
		SetShaderParam("RTP_gloss2mask0123", getVector(RTP_gloss2mask, 0,3));
		SetShaderParam("RTP_gloss_mult0123", getVector(tmp_RTP_gloss_mult, 0,3));
		SetShaderParam("RTP_gloss_shaping0123", getVector(tmp_RTP_gloss_shaping, 0,3));
		// (only in deferred addpass)
		//SetShaderParam("_DeferredSpecDampAddPass0123", getVector(_DeferredSpecDampAddPass, 0,3));
		
		SetShaderParam("_LayerBrightness0123", MasterLayerBrightness*getVector(LayerBrightness, 0, 3));
		SetShaderParam("_LayerSaturation0123", MasterLayerSaturation*getVector(LayerSaturation, 0, 3));
		SetShaderParam("_LayerBrightness2Spec0123", getVector(LayerBrightness2Spec, 0, 3));
		SetShaderParam("_LayerAlbedo2SpecColor0123", getVector(LayerAlbedo2SpecColor, 0, 3));


		SetShaderParam("RTP_AO_0123", getVector(AO_strength, 0,3));

		
		if ((numLayers>4) && _4LAYERS_SHADER_USED) {
			//
			// przekieruj parametry warstw 4-7 na AddPass
			//
			SetShaderParam("_Spec89AB", getVector(Spec, 4,7));
			SetShaderParam("_MIPmult89AB", getVector(MIPmult, 4,7));
			SetShaderParam("_TessStrenght89AB", getVector(TessStrenght, 4,7));

			
			// RTP3.1
			SetShaderParam("RTP_gloss2mask89AB", getVector(RTP_gloss2mask, 4, 7));
			SetShaderParam("RTP_gloss_mult89AB", getVector(tmp_RTP_gloss_mult, 4, 7));
			SetShaderParam("RTP_gloss_shaping89AB", getVector(tmp_RTP_gloss_shaping, 4, 7));

			SetShaderParam("_LayerBrightness89AB", MasterLayerBrightness*getVector(LayerBrightness, 4, 7));
			SetShaderParam("_LayerSaturation89AB", MasterLayerSaturation*getVector(LayerSaturation, 4, 7));
			SetShaderParam("_LayerBrightness2Spec89AB", getVector(LayerBrightness2Spec, 4, 7));
			SetShaderParam("_LayerAlbedo2SpecColor89AB", getVector(LayerAlbedo2SpecColor, 4, 7));

			SetShaderParam("RTP_AO_89AB", getVector(AO_strength, 4,7));

		} else {
			SetShaderParam("_Spec4567", getVector(Spec, 4,7));
			SetShaderParam("_MIPmult4567", getVector(MIPmult, 4,7));
			SetShaderParam("_TessStrenght4567", getVector(TessStrenght, 4,7));

			// RTP3.1
			SetShaderParam("RTP_gloss2mask4567", getVector(RTP_gloss2mask, 4, 7));
			SetShaderParam("RTP_gloss_mult4567", getVector(tmp_RTP_gloss_mult, 4, 7));
			SetShaderParam("RTP_gloss_shaping4567", getVector(tmp_RTP_gloss_shaping, 4, 7));
			// only in deferred add pass
			//SetShaderParam("_DeferredSpecDampAddPass4567", getVector(_DeferredSpecDampAddPass, 4,7));

			SetShaderParam("_LayerBrightness4567", MasterLayerBrightness*getVector(LayerBrightness, 4, 7));
			SetShaderParam("_LayerSaturation4567", MasterLayerSaturation*getVector(LayerSaturation, 4, 7));

			SetShaderParam("_LayerBrightness2Spec4567", getVector(LayerBrightness2Spec, 4, 7));
			SetShaderParam("_LayerAlbedo2SpecColor4567", getVector(LayerAlbedo2SpecColor, 4, 7));

			SetShaderParam("RTP_AO_4567", getVector(AO_strength, 4,7));
					
			//
			// AddPass
			//
			SetShaderParam("_Spec89AB", getVector(Spec, 8,11));
			SetShaderParam("_MIPmult89AB", getVector(MIPmult, 8,11));
			SetShaderParam("_TessStrenght89AB", getVector(TessStrenght, 8,11));

			
			// RTP3.1
			SetShaderParam("RTP_gloss2mask89AB", getVector(RTP_gloss2mask, 8,11));
			SetShaderParam("RTP_gloss_mult89AB", getVector(tmp_RTP_gloss_mult, 8,11));
			SetShaderParam("RTP_gloss_shaping89AB", getVector(tmp_RTP_gloss_shaping, 8,11));
			SetShaderParam("_DeferredSpecDampAddPass89AB", getVector(_DeferredSpecDampAddPass, 8,11));
			
			SetShaderParam("_LayerBrightness89AB", MasterLayerBrightness*getVector(LayerBrightness, 8, 11));
			SetShaderParam("_LayerSaturation89AB", MasterLayerSaturation*getVector(LayerSaturation, 8, 11));
			SetShaderParam("_LayerBrightness2Spec89AB", getVector(LayerBrightness2Spec, 8, 11));
			SetShaderParam("_LayerAlbedo2SpecColor89AB", getVector(LayerAlbedo2SpecColor, 8, 11));


			SetShaderParam("RTP_AO_89AB", getVector(AO_strength, 8,11));

		}
		
		/////////////////////////////////////////////////////////////////////
		//
		// layer dependent textures
		//
		/////////////////////////////////////////////////////////////////////
		// update (RTP3.1)
		if (splat_atlases.Length==2) {
			Texture2D _atA=splat_atlases[0];
			Texture2D _atB=splat_atlases[1];
			splat_atlases=new Texture2D[3];
			splat_atlases[0]=_atA;
			splat_atlases[1]=_atB;
		}




		ApplyGlossBakedAtlas("_SplatAtlasA", 0);
		SetShaderParam("_BumpMap01", Bump01);
		SetShaderParam("_BumpMap23", Bump23);
		SetShaderParam("_TERRAIN_HeightMap", HeightMap);

		
		if (numLayers>4) 
		{


			ApplyGlossBakedAtlas("_SplatAtlasB", 1);
			ApplyGlossBakedAtlas("_SplatAtlasC", 1);
			SetShaderParam("_TERRAIN_HeightMap2", HeightMap2);

		}
		if (numLayers>8) {
			ApplyGlossBakedAtlas("_SplatAtlasC", 2);
		}
		if ((numLayers>4) && _4LAYERS_SHADER_USED) {
			//
			// przekieruj parametry warstw 4-7 na AddPass
			//

			SetShaderParam("_BumpMap89", Bump45);
			SetShaderParam("_BumpMapAB", Bump67);
			SetShaderParam("_TERRAIN_HeightMap3", HeightMap2);
			// potrzebne przy sniegu (firstpass moze korzystac z koloru i bumpmap 4-7)
			SetShaderParam("_BumpMap45", Bump45);
			SetShaderParam("_BumpMap67", Bump67);
		} else {
			SetShaderParam("_BumpMap45", Bump45);
			SetShaderParam("_BumpMap67", Bump67);
			
			//
			// AddPass
			//
			SetShaderParam("_BumpMap89", Bump89);
			SetShaderParam("_BumpMapAB", BumpAB);
			SetShaderParam("_TERRAIN_HeightMap3", HeightMap3);
		}
		
		use_mat=null;
	}
	
	public Vector4 getVector(float[] vec, int idxA, int idxB) {
		if (vec==null) return Vector4.zero;
		Vector4 ret=Vector4.zero;
		for(int i=idxA; i<=idxB; i++) {
			if (i<vec.Length) {
				ret[i-idxA]=vec[i];
			}
		}
		return ret;
	}
	public Vector4 getColorVector(Color[] vec, int idxA, int idxB, int channel) {
		if (vec==null) return Vector4.zero;
		Vector4 ret=Vector4.zero;
		for(int i=idxA; i<=idxB; i++) {
			if (i<vec.Length) {
				ret[i-idxA]=vec[i][channel];
			}
		}
		return ret;
	}	
	
	public Texture2D get_dumb_tex() {
		if (!dumb_tex) {
			dumb_tex=new Texture2D(32,32,TextureFormat.RGB24,false);
			Color[] cols=dumb_tex.GetPixels();
			for(int i=0; i<cols.Length; i++) {
				cols[i]=Color.white;
			}
			dumb_tex.SetPixels(cols);
			dumb_tex.Apply();
		}
		return dumb_tex;
	}	
	
	public void SyncGlobalPropsAcrossTerrainGroups() {
		ReliefTerrain[] terrainObjects=(ReliefTerrain[])(GameObject.FindObjectsOfType(typeof(ReliefTerrain)));
		ReliefTerrainGlobalSettingsHolder[] globalHolders=new ReliefTerrainGlobalSettingsHolder[terrainObjects.Length];
		int numSeparateGroups=0;
		for(int i=0; i<terrainObjects.Length; i++) {
			bool alreadyPresent=false;
			for(int j=0; j<numSeparateGroups; j++) {
				if (globalHolders[j]==terrainObjects[i].globalSettingsHolder) {
					alreadyPresent=true;
					break;
				}
			}
			if (!alreadyPresent) {
				globalHolders[numSeparateGroups++] = terrainObjects[i].globalSettingsHolder;
			}
		}
		if (numSeparateGroups>1) {
			for(int i=0; i<numSeparateGroups; i++) {
				globalHolders[i].useTerrainMaterial=true;
			}
		}
		for(int i=0; i<numSeparateGroups; i++) {
			if (globalHolders[i]!=this) {

				

				
				globalHolders[i].rtp_customAmbientCorrection=rtp_customAmbientCorrection;
				
				globalHolders[i].TERRAIN_IBL_DiffAO_Damp=TERRAIN_IBL_DiffAO_Damp;
				globalHolders[i].TERRAIN_IBLRefl_SpecAO_Damp=TERRAIN_IBLRefl_SpecAO_Damp;

			}
		}
		

	}
	
	public void RestorePreset(ReliefTerrainPresetHolder holder) {
		currentPreset = holder.PresetName;
		numLayers=holder.numLayers;
		splats=new Texture2D[holder.splats.Length];
		for(int i=0; i<holder.splats.Length; i++) {
			splats[i]=holder.splats[i];
		}
		
		splat_atlases=new Texture2D[3];
		for(int i=0; i<splat_atlases.Length; i++) {
			splat_atlases[i]=holder.splat_atlases[i];
		}
		
		gloss_baked=holder.gloss_baked;
		// actualy used textures will be rebuild on next Refresh()
		splats_glossBaked=new Texture2D[12];
		atlas_glossBaked=new Texture2D[3];		
		
		RTP_MIP_BIAS=holder.RTP_MIP_BIAS;
		_SpecColor=holder._SpecColor;
		RTP_DeferredAddPassSpec=holder.RTP_DeferredAddPassSpec;
		
		MasterLayerBrightness=holder.MasterLayerBrightness;
		MasterLayerSaturation=holder.MasterLayerSaturation;
		

		
		Bump01=holder.Bump01;
		Bump23=holder.Bump23;
		Bump45=holder.Bump45;
		Bump67=holder.Bump67;
		Bump89=holder.Bump89;
		BumpAB=holder.BumpAB;
		


		_FarNormalDamp=holder._FarNormalDamp;
		
		blendMultiplier=holder.blendMultiplier;
		
		HeightMap=holder.HeightMap;
		HeightMap2=holder.HeightMap2;
		HeightMap3=holder.HeightMap3;
		
		ReliefTransform=holder.ReliefTransform;
		DIST_STEPS=holder.DIST_STEPS;

		ReliefBorderBlend=holder.ReliefBorderBlend;
		
		ExtrudeHeight=holder.ExtrudeHeight;
		LightmapShading=holder.LightmapShading;
		
		SHADOW_STEPS=holder.SHADOW_STEPS;
		WAVELENGTH_SHADOWS=holder.WAVELENGTH_SHADOWS;
		//SHADOW_SMOOTH_STEPS=holder.SHADOW_SMOOTH_STEPS;
		SelfShadowStrength=holder.SelfShadowStrength;
		ShadowSmoothing=holder.ShadowSmoothing;
		ShadowSoftnessFade = holder.ShadowSoftnessFade;
		
		distance_start=holder.distance_start;
		distance_transition=holder.distance_transition;

		tessHeight = holder.tessHeight;
		_TessSubdivisions = holder._TessSubdivisions;
		_TessSubdivisionsFar = holder._TessSubdivisionsFar;
		_TessYOffset = holder._TessYOffset;
		

		
		// reflection
	
		

		RTP_AOsharpness=holder.RTP_AOsharpness;
		RTP_AOamp=holder.RTP_AOamp;

		
	
		//////////////////////
		// layer_dependent arrays
		//////////////////////
		Bumps=new Texture2D[holder.Bumps.Length];
		Spec=new float[holder.Bumps.Length];

		// RTP3.1
		RTP_gloss2mask=new float[holder.Bumps.Length];
		RTP_gloss_mult=new float[holder.Bumps.Length];
		RTP_gloss_shaping=new float[holder.Bumps.Length];
	
		_DeferredSpecDampAddPass=new float[holder.Bumps.Length];
		

		LayerBrightness=new float[holder.Bumps.Length];
		LayerBrightness2Spec=new float[holder.Bumps.Length];
		LayerAlbedo2SpecColor=new float[holder.Bumps.Length];
		LayerSaturation=new float[holder.Bumps.Length];

		
	
		
		AO_strength=new float[holder.Bumps.Length];

		
		Heights=new Texture2D[holder.Bumps.Length];
		

		#if !UNITY_WEB_GL || UNITY_EDITOR
		Substances=new ProceduralMaterial[holder.Bumps.Length];
		#endif

		// wet

		
		for(int i=0; i<holder.Bumps.Length; i++) {
			Bumps[i]=holder.Bumps[i];
			Spec[i]=holder.Spec[i];

			
			// RTP3.1
			// update-set to default
			if (CheckAndUpdate(ref holder.RTP_gloss2mask, 0.5f, holder.Bumps.Length)) {
				for(int k=0; k<numLayers; k++) {
					Spec[k]=1; // zresetuj od razu mnożnik glossa (RTP3.1 - zmienna ma inne znaczenie)
				}
			}
			CheckAndUpdate(ref holder.RTP_gloss_mult, 1f, holder.Bumps.Length);
			CheckAndUpdate(ref holder.RTP_gloss_shaping, 0.5f, holder.Bumps.Length);

			CheckAndUpdate(ref holder._DeferredSpecDampAddPass, 1f, holder.Bumps.Length);
			

			CheckAndUpdate(ref holder.LayerBrightness, 1.0f, holder.Bumps.Length);
			CheckAndUpdate(ref holder.LayerBrightness2Spec, 0.0f, holder.Bumps.Length);
			CheckAndUpdate(ref holder.LayerAlbedo2SpecColor, 0.0f, holder.Bumps.Length);
			CheckAndUpdate(ref holder.LayerSaturation, 1.0f, holder.Bumps.Length);

			
			RTP_gloss2mask[i]=holder.RTP_gloss2mask[i];
			RTP_gloss_mult[i]=holder.RTP_gloss_mult[i];
			RTP_gloss_shaping[i]=holder.RTP_gloss_shaping[i];

			_DeferredSpecDampAddPass[i]=holder._DeferredSpecDampAddPass[i];

			LayerBrightness[i]=holder.LayerBrightness[i];
			LayerBrightness2Spec[i]=holder.LayerBrightness2Spec[i];
			LayerAlbedo2SpecColor[i]=holder.LayerAlbedo2SpecColor[i];
			LayerSaturation[i]=holder.LayerSaturation[i];
		
		
			AO_strength[i]=holder.AO_strength[i];
			TessStrenght [i] = holder.TessStrenght [i];
			
			Heights[i]=holder.Heights[i];
			
//			_snow_strength_per_layer[i]=holder._snow_strength_per_layer[i];
//			#if !UNITY_WEBGL || UNITY_EDITOR
//			Substances[i]=holder.Substances[i];
//			#endif			

			// wet
			/*
			TERRAIN_LayerWetStrength[i]=holder.TERRAIN_LayerWetStrength[i];
			TERRAIN_WaterLevel[i]=holder.TERRAIN_WaterLevel[i];
			/TERRAIN_WaterLevelSlopeDamp[i]=holder.TERRAIN_WaterLevelSlopeDamp[i];
			TERRAIN_WaterEdge[i]=holder.TERRAIN_WaterEdge[i];
			TERRAIN_WaterSpecularity[i]=holder.TERRAIN_WaterSpecularity[i];
			TERRAIN_WaterGloss[i]=holder.TERRAIN_WaterGloss[i];
			TERRAIN_WaterGlossDamper[i]=holder.TERRAIN_WaterGlossDamper[i];
			TERRAIN_WaterOpacity[i]=holder.TERRAIN_WaterOpacity[i];
			TERRAIN_Refraction[i]=holder.TERRAIN_Refraction[i];
			TERRAIN_WetRefraction[i]=holder.TERRAIN_WetRefraction[i];
			TERRAIN_Flow[i]=holder.TERRAIN_Flow[i];
			TERRAIN_WetFlow[i]=holder.TERRAIN_WetFlow[i];
			TERRAIN_WetSpecularity[i]=holder.TERRAIN_WetSpecularity[i];
			TERRAIN_WetGloss[i]=holder.TERRAIN_WetGloss[i];
			TERRAIN_WaterColor[i]=holder.TERRAIN_WaterColor[i];
			TERRAIN_WaterIBL_SpecWetStrength[i]=holder.TERRAIN_WaterIBL_SpecWetStrength[i];
			TERRAIN_WaterIBL_SpecWaterStrength[i]=holder.TERRAIN_WaterIBL_SpecWaterStrength[i];
			TERRAIN_WaterEmission[i]=holder.TERRAIN_WaterEmission[i];
			*/
		}
	}
	
	public void SavePreset(ref ReliefTerrainPresetHolder holder) {
		holder.numLayers=numLayers;
		holder.splats=new Texture2D[splats.Length];
		for(int i=0; i<holder.splats.Length; i++) {
			holder.splats[i]=splats[i];
		}
		
		holder.splat_atlases=new Texture2D[3];
		for(int i=0; i<splat_atlases.Length; i++) {
			holder.splat_atlases[i]=splat_atlases[i];
		}
		
		holder.gloss_baked=gloss_baked;
		
		holder.RTP_MIP_BIAS=RTP_MIP_BIAS;
		holder.TessStrenght = TessStrenght;
		holder._SpecColor=_SpecColor;
		holder.RTP_DeferredAddPassSpec=RTP_DeferredAddPassSpec;
		
		holder.MasterLayerBrightness=MasterLayerBrightness;
		holder.MasterLayerSaturation=MasterLayerSaturation;

		
		holder.Bump01=Bump01;
		holder.Bump23=Bump23;
		holder.Bump45=Bump45;
		holder.Bump67=Bump67;
		holder.Bump89=Bump89;
		holder.BumpAB=BumpAB;
		

		holder._FarNormalDamp=_FarNormalDamp;
		
		holder.blendMultiplier=blendMultiplier;
		
		holder.HeightMap=HeightMap;
		holder.HeightMap2=HeightMap2;
		holder.HeightMap3=HeightMap3;
		
		holder.ReliefTransform=ReliefTransform;
		holder.DIST_STEPS=DIST_STEPS;

		holder.ReliefBorderBlend=ReliefBorderBlend;
		
		holder.ExtrudeHeight=ExtrudeHeight;
		holder.LightmapShading=LightmapShading;
		
		holder.SHADOW_STEPS=SHADOW_STEPS;
		holder.WAVELENGTH_SHADOWS=WAVELENGTH_SHADOWS;
		//holder.SHADOW_SMOOTH_STEPS=SHADOW_SMOOTH_STEPS;
		holder.SelfShadowStrength=SelfShadowStrength;
		holder.ShadowSmoothing=ShadowSmoothing;
		holder.ShadowSoftnessFade = ShadowSoftnessFade;
		
		holder.distance_start=distance_start;
		holder.distance_transition=distance_transition;


		holder.tessHeight = tessHeight;
		holder.TessStrenght = TessStrenght;
		holder._TessSubdivisions = _TessSubdivisions;
		holder._TessSubdivisionsFar = _TessSubdivisionsFar;
		holder._TessYOffset = _TessYOffset;


		holder.RTP_AOsharpness=RTP_AOsharpness;
		holder.RTP_AOamp=RTP_AOamp;

		
	
		//////////////////////
		// layer_dependent arrays
		//////////////////////
		holder.Bumps=new Texture2D[numLayers];
		holder.Spec=new float[numLayers];
	
		// RTP3.1		
		holder.RTP_gloss2mask=new float[numLayers];
		holder.RTP_gloss_mult=new float[numLayers];
		holder.RTP_gloss_shaping=new float[numLayers];

		holder._DeferredSpecDampAddPass=new float[numLayers];
		

		holder.LayerBrightness=new float[numLayers];
		holder.LayerBrightness2Spec=new float[numLayers];
		holder.LayerAlbedo2SpecColor=new float[numLayers];
		holder.LayerSaturation=new float[numLayers];


		holder.AO_strength=new float[numLayers];
		
		holder.Heights=new Texture2D[numLayers];
		
		holder._snow_strength_per_layer=new float[numLayers];
		#if !UNITY_WEBGL || UNITY_EDITOR
		holder.Substances=new ProceduralMaterial[numLayers];
		#endif		


		for(int i=0; i<numLayers; i++) {
			holder.Bumps[i]=Bumps[i];
			holder.Spec[i]=Spec[i];

			
			// >RTP3.1
			// update-set to default
			if (CheckAndUpdate(ref RTP_gloss2mask, 0.5f, numLayers)) {
				for(int k=0; k<numLayers; k++) {
					Spec[k]=1; // zresetuj od razu mnożnik glossa (RTP3.1 - zmienna ma inne znaczenie)
				}
			}
			CheckAndUpdate(ref RTP_gloss_mult, 1f, numLayers);
			CheckAndUpdate(ref RTP_gloss_shaping, 0.5f, numLayers);

			CheckAndUpdate(ref _DeferredSpecDampAddPass, 1f, numLayers);



			CheckAndUpdate(ref LayerBrightness, 1.0f, numLayers);
			CheckAndUpdate(ref LayerBrightness2Spec, 0.0f, numLayers);
			CheckAndUpdate(ref LayerAlbedo2SpecColor, 0.0f, numLayers);
			CheckAndUpdate(ref LayerSaturation, 1.0f, numLayers);


			holder.RTP_gloss2mask[i]=RTP_gloss2mask[i];
			holder.RTP_gloss_mult[i]=RTP_gloss_mult[i];
			holder.RTP_gloss_shaping[i]=RTP_gloss_shaping[i];
		
			holder._DeferredSpecDampAddPass[i]=_DeferredSpecDampAddPass[i];

			holder.LayerBrightness[i]=LayerBrightness[i];
			holder.LayerBrightness2Spec[i]=LayerBrightness2Spec[i];
			holder.LayerAlbedo2SpecColor[i]=LayerAlbedo2SpecColor[i];
			holder.LayerSaturation[i]=LayerSaturation[i];
		

			holder.AO_strength[i]=AO_strength[i];
			
			holder.Heights[i]=Heights[i];
			

			#if !UNITY_WEBGL || UNITY_EDITOR
			holder.Substances[i]=Substances[i];
			#endif

			// wet
		
		}
	}
	
	public void InterpolatePresets(ReliefTerrainPresetHolder holderA, ReliefTerrainPresetHolder holderB, float t) {
		RTP_MIP_BIAS=Mathf.Lerp(holderA.RTP_MIP_BIAS, holderB.RTP_MIP_BIAS, t);
		_SpecColor=Color.Lerp(holderA._SpecColor, holderB._SpecColor, t);
		RTP_DeferredAddPassSpec=Mathf.Lerp(holderA.RTP_DeferredAddPassSpec, holderB.RTP_DeferredAddPassSpec, t);
		
		MasterLayerBrightness=Mathf.Lerp(holderA.MasterLayerBrightness, holderB.MasterLayerBrightness, t);
		MasterLayerSaturation=Mathf.Lerp(holderA.MasterLayerSaturation, holderB.MasterLayerSaturation, t);
		

		_FarNormalDamp=Mathf.Lerp(holderA._FarNormalDamp, holderB._FarNormalDamp, t);
		
		blendMultiplier=Mathf.Lerp(holderA.blendMultiplier, holderB.blendMultiplier, t);
		
		ReliefTransform=Vector4.Lerp(holderA.ReliefTransform, holderB.ReliefTransform, t);
		DIST_STEPS=Mathf.Lerp(holderA.DIST_STEPS, holderB.DIST_STEPS, t);
		ReliefBorderBlend=Mathf.Lerp(holderA.ReliefBorderBlend, holderB.ReliefBorderBlend, t);
		
		ExtrudeHeight=Mathf.Lerp(holderA.ExtrudeHeight, holderB.ExtrudeHeight, t);
		LightmapShading=Mathf.Lerp(holderA.LightmapShading, holderB.LightmapShading, t);
		
		SHADOW_STEPS=Mathf.Lerp(holderA.SHADOW_STEPS, holderB.SHADOW_STEPS, t);
		WAVELENGTH_SHADOWS=Mathf.Lerp(holderA.WAVELENGTH_SHADOWS, holderB.WAVELENGTH_SHADOWS, t);
		//SHADOW_SMOOTH_STEPS=Mathf.Lerp(holderA.SHADOW_SMOOTH_STEPS, holderB.SHADOW_SMOOTH_STEPS, t);
		SelfShadowStrength=Mathf.Lerp(holderA.SelfShadowStrength, holderB.SelfShadowStrength, t);
		ShadowSmoothing=Mathf.Lerp(holderA.ShadowSmoothing, holderB.ShadowSmoothing, t);
		ShadowSoftnessFade = Mathf.Lerp(holderA.ShadowSoftnessFade, holderB.ShadowSoftnessFade, t);
		
		distance_start=Mathf.Lerp(holderA.distance_start, holderB.distance_start, t);
		distance_transition=Mathf.Lerp(holderA.distance_transition, holderB.distance_transition, t);

		RTP_AOsharpness=Mathf.Lerp(holderA.RTP_AOsharpness, holderB.RTP_AOsharpness, t);
		RTP_AOamp=Mathf.Lerp(holderA.RTP_AOamp, holderB.RTP_AOamp, t);
	
		
		//////////////////////
		// layer_dependent arrays
		//////////////////////
		for(int i=0; i<holderA.Spec.Length; i++) {
			if (i<Spec.Length) {
				Spec[i]=Mathf.Lerp(holderA.Spec[i], holderB.Spec[i], t);
			
				
				// RTP3.1
				RTP_gloss2mask[i]=Mathf.Lerp(holderA.RTP_gloss2mask[i], holderB.RTP_gloss2mask[i], t);
				RTP_gloss_mult[i]=Mathf.Lerp(holderA.RTP_gloss_mult[i], holderB.RTP_gloss_mult[i], t);
				RTP_gloss_shaping[i]=Mathf.Lerp(holderA.RTP_gloss_shaping[i], holderB.RTP_gloss_shaping[i], t);
			
				_DeferredSpecDampAddPass[i]=Mathf.Lerp(holderA._DeferredSpecDampAddPass[i], holderB._DeferredSpecDampAddPass[i], t);
		
				LayerBrightness[i]=Mathf.Lerp(holderA.LayerBrightness[i], holderB.LayerBrightness[i], t);
				LayerBrightness2Spec[i]=Mathf.Lerp(holderA.LayerBrightness2Spec[i], holderB.LayerBrightness2Spec[i], t);
				LayerAlbedo2SpecColor[i]=Mathf.Lerp(holderA.LayerAlbedo2SpecColor[i], holderB.LayerAlbedo2SpecColor[i], t);
				LayerSaturation[i]=Mathf.Lerp(holderA.LayerSaturation[i], holderB.LayerSaturation[i], t);

				
			
				
				AO_strength[i]=Mathf.Lerp(holderA.AO_strength[i], holderB.AO_strength[i], t);

			}
		}
	}	
	
	public void ReturnToDefaults(string what="", int layerIdx=-1) {
		// main settings
		if (what=="" || what=="main") {		
			ReliefTransform=new Vector4(3,3,0,0);
			distance_start=5f;
			distance_transition=20f;
			_SpecColor=new Color(200.0f/255.0f, 200.0f/255.0f, 200.0f/255.0f, 1);
			RTP_DeferredAddPassSpec=0.5f;
			rtp_customAmbientCorrection=new Color(0.2f, 0.2f, 0.2f, 1);
			TERRAIN_IBL_DiffAO_Damp=0.25f;
			TERRAIN_IBLRefl_SpecAO_Damp=0.5f;
		
			
			ReliefBorderBlend=6;
			LightmapShading=0f;
			RTP_MIP_BIAS=0;		
			RTP_AOsharpness=1.5f;
			RTP_AOamp=0.1f;
			
			MasterLayerBrightness=1;
			MasterLayerSaturation=1;

		}
		
		//perlin
		if (what=="" || what=="perlin") {
	
			_FarNormalDamp=0.2f;
			distance_start_bumpglobal=30f;
			distance_transition_bumpglobal=30f;

		}
		
		// global color
		if (what=="" || what=="global_color") {

			_Phong=0;
			tessHeight=300;
			_TessSubdivisions = 1;
			_TessSubdivisionsFar = 1;
			_TessYOffset = 0;
			
		
		}
		
		// uvblend
		if (what=="" || what=="uvblend") {
			blendMultiplier=1;
		}
		
		// POM/PM settings
		if (what=="" || what=="pom/pm") {
			ExtrudeHeight=0.05f;
			DIST_STEPS=20;
			WAVELENGTH=2;
			SHADOW_STEPS=20f;
			WAVELENGTH_SHADOWS=2f;
			//SHADOW_SMOOTH_STEPS=6f;
			SelfShadowStrength=0.8f;
			ShadowSmoothing=1f;
			ShadowSoftnessFade=0.8f;
		}
		
		// snow global
	
		
		// superdetail
	
		
		// water

		
		// caustics

		
		// layer
		if (what=="" || what=="layer") {
			int b=0;
			int e=numLayers<12 ? numLayers:12;
			if (layerIdx>=0) {
				b=layerIdx;
				e=layerIdx+1;
			}
			for(int j=b; j<e; j++)  {
				Spec[j]=1f; //RTP3.1 - mnożnik glossa (inne znaczenie)
				MIPmult[j]=0.0f;
				TessStrenght[j]=0.0f;

				
				// RTP3.1
				RTP_gloss2mask[j]=0.5f;
				RTP_gloss_mult[j]=1;
				RTP_gloss_shaping[j]=0.5f;
			
				_DeferredSpecDampAddPass[j]=1;
				

				LayerBrightness[j]=1.0f;
				LayerBrightness2Spec[j]=0.0f;
				LayerAlbedo2SpecColor[j]=0.0f;
				LayerSaturation[j]=1.0f;

				

				AO_strength[j]=1;
				

			}
		}
		
	}
	
	public bool CheckAndUpdate(ref float[] aLayerPropArray, float defVal, int len) {
		if (aLayerPropArray==null || aLayerPropArray.Length<len) {
			aLayerPropArray=new float[len];
			for(int k=0; k<len; k++) {
				aLayerPropArray[k]=defVal;
			}
			return true;
		}
		return false; // no update
	}
	public bool CheckAndUpdate(ref Color[] aLayerPropArray, Color defVal, int len) {
		if (aLayerPropArray==null || aLayerPropArray.Length<len) {
			aLayerPropArray=new Color[len];
			for(int k=0; k<len; k++) {
				aLayerPropArray[k]=defVal;
			}
			return true;
		}
		return false; // no update
	}	
	
	#if UNITY_EDITOR
	public bool PrepareNormals() {
		if (Bumps==null) return false;
		Texture2D[] bumps=new Texture2D[(numLayers>8) ? 12 : ((numLayers>4) ? 8 : 4)];
		int i;
		for(i=0; i<bumps.Length; i++) 	bumps[i]=(i<Bumps.Length) ? Bumps[i] : null;
		for(i=0; i<bumps.Length; i++) {
			if (!bumps[i]) {
				if ((i&1)==0) {
					if (bumps[i+1]) {
						bumps[i]=new Texture2D(bumps[i+1].width, bumps[i+1].width,TextureFormat.ARGB32,false,true);
						FillTex(bumps[i], new Color32(128,128,128,128));
					}
				} else {
					if (bumps[i-1]) {
						bumps[i]=new Texture2D(bumps[i-1].width, bumps[i-1].width,TextureFormat.ARGB32,false,true);
						FillTex(bumps[i], new Color32(128,128,128,128));
					}
				}
			}
			if (bumps[i]) {
				try { 
					bumps[i].GetPixels(0,0,4,4,0);
				} catch (Exception e) {
					Debug.LogError("Normal texture "+i+" has to be marked as isReadable...");
					Debug.LogError(e.Message);
					activateObject=bumps[i];
					return false;
				}
			} else {
				bumps[i]=new Texture2D(4,4,TextureFormat.ARGB32,false,true);
				FillTex(bumps[i], new Color32(128,128,128,128));
			}
		}
		if (bumps[0] && bumps[1] && bumps[0].width!=bumps[1].width) {
			Debug.LogError("Normal textures pair 0,1 should have the same size");
			activateObject=bumps[1];
			//Time.timeScale=0; // pause
			return false;
		}
		if (bumps[2] && bumps[3] && bumps[2].width!=bumps[3].width) {
			Debug.LogError("Normal textures pair 2,3 should have the same size");
			activateObject=bumps[3];
			//Time.timeScale=0; // pause
			return false;
		}
		Bump01=CombineNormals(bumps[0], bumps[1]);
		Bump23=CombineNormals(bumps[2], bumps[3]);
		if (bumps.Length>4) {
			if (bumps[4] && bumps[5] && bumps[4].width!=bumps[5].width) {
				Debug.LogError("Normal textures pair 4,5 should have the same size");
				activateObject=bumps[5];
				//Time.timeScale=0; // pause
				return false;
			}
			if (bumps[6] && bumps[7] && bumps[6].width!=bumps[7].width) {
				Debug.LogError("Normal textures pair 6,7 should have the same size");
				activateObject=bumps[7];
				//Time.timeScale=0; // pause
				return false;
			}
			Bump45=CombineNormals(bumps[4], bumps[5]);
			Bump67=CombineNormals(bumps[6], bumps[7]);
		}
		if (bumps.Length>8) {
			if (bumps[8] && bumps[9] && bumps[8].width!=bumps[9].width) {
				Debug.LogError("Normal textures pair 8,9 should have the same size");
				activateObject=bumps[9];
				//Time.timeScale=0; // pause
				return false;
			}
			if (bumps[10] && bumps[11] && bumps[10].width!=bumps[11].width) {
				Debug.LogError("Normal textures pair 10,11 should have the same size");
				activateObject=bumps[11];
				//Time.timeScale=0; // pause
				return false;
			}
			Bump89=CombineNormals(bumps[8], bumps[9]);
			BumpAB=CombineNormals(bumps[10], bumps[11]);
		}
		return true;
	}
	
	public void FillTex(Texture2D tex, Color32 col) {
		Color32[] cols=tex.GetPixels32();
		for(int i=0; i<cols.Length; i++) {
			cols[i].r=col.r;
			cols[i].g=col.g;
			cols[i].b=col.b;
			cols[i].a=col.a;
		}
		tex.SetPixels32(cols);
	}

	private Texture2D CombineNormals(Texture2D texA, Texture2D texB) {
		if (!texA) return null;
		Color32[] colsA=texA.GetPixels32();
		Color32[] colsB=texB.GetPixels32();
		Color32[] cols=new Color32[colsA.Length];
		for(int i=0; i<cols.Length; i++) {
			#if UNITY_WEBGL || UNITY_IPHONE || UNITY_ANDROID
			cols[i].r=colsA[i].r;
			cols[i].g=colsA[i].g;
			cols[i].b=colsB[i].r;
			cols[i].a=colsB[i].g;
			#else
			cols[i].r=colsA[i].a;
			cols[i].g=colsA[i].g;
			cols[i].b=colsB[i].a;
			cols[i].a=colsB[i].g;
			#endif
		}
		Texture2D tex=new Texture2D(texA.width, texA.width, TextureFormat.ARGB32, true, true);
		tex.SetPixels32(cols);
		tex.Apply(true,false);
		//tex.Compress(true); // may try, but quality will be bad...
		//tex.Apply(false,true); // not readable przy publishingu
		tex.filterMode=FilterMode.Trilinear;
		tex.anisoLevel=0;
		return tex;
	}
	
	public void CopyWaterParams(int src, int tgt) {
		
	}	
	
	public void RecalcControlMaps(Terrain terrainComp, ReliefTerrain rt) {
		float[,,] splatData=terrainComp.terrainData.GetAlphamaps(0,0,terrainComp.terrainData.alphamapResolution, terrainComp.terrainData.alphamapResolution);
		Color[] cols_control;
		float[,] norm_array=new float[terrainComp.terrainData.alphamapResolution,terrainComp.terrainData.alphamapResolution];
		if (rt.splat_layer_ordered_mode) {
			// ordered mode
			for(int k=0; k<terrainComp.terrainData.alphamapLayers; k++) {
				int n=rt.splat_layer_seq[k];
				// value for current layer
				if (rt.splat_layer_calc[n]) {
					int idx=0;
					if (rt.source_controls[n]) {
						cols_control=rt.source_controls[n].GetPixels();
					} else {
						cols_control=new Color[terrainComp.terrainData.alphamapResolution*terrainComp.terrainData.alphamapResolution];
						if (rt.source_controls_invert[n]) {
							for(int i=0; i<cols_control.Length; i++) cols_control[i]=Color.black;
						} else {
							for(int i=0; i<cols_control.Length; i++) cols_control[i]=Color.white;
						}
					}
					int channel_idx=(int)rt.source_controls_channels[n];
					// apply mask
					if (rt.splat_layer_masked[n] && rt.source_controls_mask[n]) {
						Color[] cols_mask=rt.source_controls_mask[n].GetPixels();
						idx=0;
						int channel_idx_mask=(int)rt.source_controls_mask_channels[n];
						for(int i=0; i<terrainComp.terrainData.alphamapResolution; i++) {
							for(int j=0; j<terrainComp.terrainData.alphamapResolution; j++) {
								cols_control[idx][channel_idx]*=cols_mask[idx][channel_idx_mask];
								idx++;
							}
						}
						idx=0;
					}
					for(int i=0; i<terrainComp.terrainData.alphamapResolution; i++) {
						for(int j=0; j<terrainComp.terrainData.alphamapResolution; j++) {
							norm_array[i,j]=cols_control[idx++][channel_idx]*rt.splat_layer_boost[n];
							if (norm_array[i,j]>1) norm_array[i,j]=1;
						}
					}
				} else {
					for(int i=0; i<terrainComp.terrainData.alphamapResolution; i++) {
						for(int j=0; j<terrainComp.terrainData.alphamapResolution; j++) {
							norm_array[i,j]=splatData[i,j,n];
							if (norm_array[i,j]>1) norm_array[i,j]=1;
						}
					}
				}
				// damp underlying layers
				for(int l=0; l<k; l++) {
					int m=rt.splat_layer_seq[l];
					for(int i=0; i<terrainComp.terrainData.alphamapResolution; i++) {
						for(int j=0; j<terrainComp.terrainData.alphamapResolution; j++) {
							splatData[i,j,m]*=(1-norm_array[i,j]);
						}
					}
				}
				// write current layer
				if (rt.splat_layer_calc[n]) {			
					int idx=0;
					if (rt.source_controls[n]) {
						cols_control=rt.source_controls[n].GetPixels();
					} else {
						cols_control=new Color[terrainComp.terrainData.alphamapResolution*terrainComp.terrainData.alphamapResolution];
						if (rt.source_controls_invert[n]) {
							for(int i=0; i<cols_control.Length; i++) cols_control[i]=Color.black;
						} else {
							for(int i=0; i<cols_control.Length; i++) cols_control[i]=Color.white;
						}
					}						
					int channel_idx=(int)rt.source_controls_channels[n];
					// apply mask
					if (rt.splat_layer_masked[n] && rt.source_controls_mask[n]) {
						Color[] cols_mask=rt.source_controls_mask[n].GetPixels();
						idx=0;
						int channel_idx_mask=(int)rt.source_controls_mask_channels[n];
						for(int i=0; i<terrainComp.terrainData.alphamapResolution; i++) {
							for(int j=0; j<terrainComp.terrainData.alphamapResolution; j++) {
								cols_control[idx][channel_idx]*=cols_mask[idx][channel_idx_mask];
								idx++;
							}
						}
						idx=0;
					}						
					for(int i=0; i<terrainComp.terrainData.alphamapResolution; i++) {
						for(int j=0; j<terrainComp.terrainData.alphamapResolution; j++) {
							splatData[i,j,n]=cols_control[idx++][channel_idx]*rt.splat_layer_boost[n];
							if (splatData[i,j,n]>1) splatData[i,j,n]=1;
						}
					}
				}
			}
		} else {
			// unordered mode
			for(int i=0; i<terrainComp.terrainData.alphamapResolution; i++) {
				for(int j=0; j<terrainComp.terrainData.alphamapResolution; j++) {
					norm_array[i,j]=0;
				}
			}
			for(int n=0; n<terrainComp.terrainData.alphamapLayers; n++) {
				if (rt.splat_layer_calc[n]) {
					int idx=0;
					if (rt.source_controls[n]) {
						cols_control=rt.source_controls[n].GetPixels();
					} else {
						cols_control=new Color[terrainComp.terrainData.alphamapResolution*terrainComp.terrainData.alphamapResolution];
						if (rt.source_controls_invert[n]) {
							for(int i=0; i<cols_control.Length; i++) cols_control[i]=Color.black;
						} else {
							for(int i=0; i<cols_control.Length; i++) cols_control[i]=Color.white;
						}
					}
					int channel_idx=(int)rt.source_controls_channels[n];
					// apply mask
					if (rt.splat_layer_masked[n] && rt.source_controls_mask[n]) {
						Color[] cols_mask=rt.source_controls_mask[n].GetPixels();
						idx=0;
						int channel_idx_mask=(int)rt.source_controls_mask_channels[n];
						for(int i=0; i<terrainComp.terrainData.alphamapResolution; i++) {
							for(int j=0; j<terrainComp.terrainData.alphamapResolution; j++) {
								cols_control[idx][channel_idx]*=cols_mask[idx][channel_idx_mask];
								idx++;
							}
						}
						idx=0;
					}
					for(int i=0; i<terrainComp.terrainData.alphamapResolution; i++) {
						for(int j=0; j<terrainComp.terrainData.alphamapResolution; j++) {
							norm_array[i,j]+=cols_control[idx++][channel_idx]*rt.splat_layer_boost[n];
						}
					}
				} else {
					for(int i=0; i<terrainComp.terrainData.alphamapResolution; i++) {
						for(int j=0; j<terrainComp.terrainData.alphamapResolution; j++) {
							norm_array[i,j]+=splatData[i,j,n];
						}
					}
				}
			}
			for(int n=0; n<terrainComp.terrainData.alphamapLayers; n++) {
				if (rt.splat_layer_calc[n]) {			
					int idx=0;
					if (rt.source_controls[n]) {
						cols_control=rt.source_controls[n].GetPixels();
					} else {
						cols_control=new Color[terrainComp.terrainData.alphamapResolution*terrainComp.terrainData.alphamapResolution];
						if (rt.source_controls_invert[n]) {
							for(int i=0; i<cols_control.Length; i++) cols_control[i]=Color.black;
						} else {
							for(int i=0; i<cols_control.Length; i++) cols_control[i]=Color.white;
						}
					}
					int channel_idx=(int)rt.source_controls_channels[n];
					// apply mask
					if (rt.splat_layer_masked[n] && rt.source_controls_mask[n]) {
						Color[] cols_mask=rt.source_controls_mask[n].GetPixels();
						idx=0;
						int channel_idx_mask=(int)rt.source_controls_mask_channels[n];
						for(int i=0; i<terrainComp.terrainData.alphamapResolution; i++) {
							for(int j=0; j<terrainComp.terrainData.alphamapResolution; j++) {
								cols_control[idx][channel_idx]*=cols_mask[idx][channel_idx_mask];
								idx++;
							}
						}
						idx=0;
					}
					for(int i=0; i<terrainComp.terrainData.alphamapResolution; i++) {
						for(int j=0; j<terrainComp.terrainData.alphamapResolution; j++) {
							splatData[i,j,n]=cols_control[idx++][channel_idx]*rt.splat_layer_boost[n]/norm_array[i,j];
						}
					}
				} else {
					for(int i=0; i<terrainComp.terrainData.alphamapResolution; i++) {
						for(int j=0; j<terrainComp.terrainData.alphamapResolution; j++) {
							splatData[i,j,n]=splatData[i,j,n]/norm_array[i,j];
						}
					}
				}			
			}
		}
		terrainComp.terrainData.SetAlphamaps(0,0, splatData);			
		
	}
	
	public void RecalcControlMapsForMesh(ReliefTerrain rt) {
		
		float[,] splatData;
		Color[] cols;
		if (numLayers>4 && rt.controlA!=null && rt.controlB!=null) {
			if (rt.controlA.width!=rt.controlB.width) {
				Debug.LogError("Control maps A&B have to be of the same size for recalculation !");
				return;				
			} else {
				bool exit=false;
				for(int k=0; k<rt.source_controls.Length; k++) {
					if (rt.splat_layer_calc[k] && rt.source_controls[k]!=null && rt.source_controls[k].width!=rt.controlA.width) {
						Debug.LogError("Source control map "+k+" should be of the control texture size ("+rt.controlA.width+") !");
						exit=true;
					}
				}
				for(int k=0; k<rt.source_controls_mask.Length; k++) {
					if (rt.splat_layer_masked[k]  && rt.source_controls_mask[k]!=null && rt.source_controls_mask[k].width!=rt.controlA.width) {
						Debug.LogError("Source mask control map "+k+" should be of the control texture size ("+rt.controlA.width+") !");
						exit=true;
					}
				}
				if (exit) return;
			}
		}
		if (rt.controlA==null) {
			rt.controlA=new Texture2D(1024, 1024, TextureFormat.ARGB32, true);
			cols=new Color[1024*1024];
			for(int i=0; i<cols.Length; i++) cols[i]=new Color(1,0,0,0);
			rt.controlA.Apply(false,false);
		} else {
			cols=rt.controlA.GetPixels(0);
		}
		splatData=new float[rt.controlA.width*rt.controlA.width, numLayers];
		for(int n=0; n<numLayers; n++) {
			if (n==4) {
				if (rt.controlB==null) {
					rt.controlB=new Texture2D(rt.controlA.width, rt.controlA.width, TextureFormat.ARGB32, true);
					cols=new Color[1024*1024];
					for(int i=0; i<cols.Length; i++) cols[i]=new Color(0,0,0,0);
					rt.controlB.Apply(false,false);
				} else {
					cols=rt.controlB.GetPixels(0);
				}
			}
			if (n==8) {
				if (rt.controlC==null) {
					rt.controlC=new Texture2D(rt.controlA.width, rt.controlA.width, TextureFormat.ARGB32, true);
					cols=new Color[1024*1024];
					for(int i=0; i<cols.Length; i++) cols[i]=new Color(0,0,0,0);
					rt.controlC.Apply(false,false);
				} else {
					cols=rt.controlC.GetPixels(0);
				}
			}
			for(int i=0; i<cols.Length; i++) {
				splatData[i,n]=cols[i][n%4];
			}
		}
		
		Color[] cols_control;
		float[] norm_array=new float[rt.controlA.width*rt.controlA.width];
		if (rt.splat_layer_ordered_mode) {
			// ordered mode
			for(int k=0; k<numLayers; k++) {
				int n=rt.splat_layer_seq[k];
				// value for current layer
				if (rt.splat_layer_calc[n]) {
					int idx=0;
					if (rt.source_controls[n]) {
						cols_control=rt.source_controls[n].GetPixels();
					} else {
						cols_control=new Color[rt.controlA.width*rt.controlA.width];
						if (rt.source_controls_invert[n]) {
							for(int i=0; i<cols_control.Length; i++) cols_control[i]=Color.black;
						} else {
							for(int i=0; i<cols_control.Length; i++) cols_control[i]=Color.white;
						}
					}
					int channel_idx=(int)rt.source_controls_channels[n];
					// apply mask
					if (rt.splat_layer_masked[n] && rt.source_controls_mask[n]) {
						Color[] cols_mask=rt.source_controls_mask[n].GetPixels();
						idx=0;
						int channel_idx_mask=(int)rt.source_controls_mask_channels[n];
						for(int i=0; i<rt.controlA.width; i++) {
							for(int j=0; j<rt.controlA.width; j++) {
								cols_control[idx][channel_idx]*=cols_mask[idx][channel_idx_mask];
								idx++;
							}
						}
						idx=0;
					}
					for(int i=0; i<rt.controlA.width*rt.controlA.width; i++) {
						norm_array[i]=cols_control[idx++][channel_idx]*rt.splat_layer_boost[n];
						if (norm_array[i]>1) norm_array[i]=1;
					}
				} else {
					for(int i=0; i<rt.controlA.width*rt.controlA.width; i++) {
						norm_array[i]=splatData[i,n];
						if (norm_array[i]>1) norm_array[i]=1;
					}
				}
				// damp underlying layers
				for(int l=0; l<k; l++) {
					int m=rt.splat_layer_seq[l];
					for(int i=0; i<rt.controlA.width*rt.controlA.width; i++) {
						splatData[i,m]*=(1-norm_array[i]);
					}
				}
				// write current layer
				if (rt.splat_layer_calc[n]) {			
					int idx=0;
					if (rt.source_controls[n]) {
						cols_control=rt.source_controls[n].GetPixels();
					} else {
						cols_control=new Color[rt.controlA.width*rt.controlA.width];
						if (rt.source_controls_invert[n]) {
							for(int i=0; i<cols_control.Length; i++) cols_control[i]=Color.black;
						} else {
							for(int i=0; i<cols_control.Length; i++) cols_control[i]=Color.white;
						}
					}						
					int channel_idx=(int)rt.source_controls_channels[n];
					// apply mask
					if (rt.splat_layer_masked[n] && rt.source_controls_mask[n]) {
						Color[] cols_mask=rt.source_controls_mask[n].GetPixels();
						idx=0;
						int channel_idx_mask=(int)rt.source_controls_mask_channels[n];
						for(int i=0; i<rt.controlA.width*rt.controlA.width; i++) {
							cols_control[idx][channel_idx]*=cols_mask[idx][channel_idx_mask];
							idx++;
						}
						idx=0;
					}						
					for(int i=0; i<rt.controlA.width*rt.controlA.width; i++) {
						splatData[i,n]=cols_control[idx++][channel_idx]*rt.splat_layer_boost[n];
					}
				}
			}
		} else {
			// unordered mode
			for(int i=0; i<rt.controlA.width*rt.controlA.width; i++) {
				norm_array[i]=0;
			}
			for(int n=0; n<numLayers; n++) {
				if (rt.splat_layer_calc[n]) {
					int idx=0;
					if (rt.source_controls[n]) {
						cols_control=rt.source_controls[n].GetPixels();
					} else {
						cols_control=new Color[rt.controlA.width*rt.controlA.width];
						if (rt.source_controls_invert[n]) {
							for(int i=0; i<cols_control.Length; i++) cols_control[i]=Color.black;
						} else {
							for(int i=0; i<cols_control.Length; i++) cols_control[i]=Color.white;
						}
					}
					int channel_idx=(int)rt.source_controls_channels[n];
					// apply mask
					if (rt.splat_layer_masked[n] && rt.source_controls_mask[n]) {
						Color[] cols_mask=rt.source_controls_mask[n].GetPixels();
						idx=0;
						int channel_idx_mask=(int)rt.source_controls_mask_channels[n];
						for(int i=0; i<rt.controlA.width*rt.controlA.width; i++) {
							cols_control[idx][channel_idx]*=cols_mask[idx][channel_idx_mask];
							idx++;
						}
						idx=0;
					}
					for(int i=0; i<rt.controlA.width*rt.controlA.width; i++) {
						norm_array[i]+=cols_control[idx++][channel_idx]*rt.splat_layer_boost[n];
					}
				} else {
					for(int i=0; i<rt.controlA.width*rt.controlA.width; i++) {
						norm_array[i]+=splatData[i,n];
					}
				}
			}
			for(int n=0; n<numLayers; n++) {
				if (rt.splat_layer_calc[n]) {			
					int idx=0;
					if (rt.source_controls[n]) {
						cols_control=rt.source_controls[n].GetPixels();
					} else {
						cols_control=new Color[rt.controlA.width*rt.controlA.width];
						if (rt.source_controls_invert[n]) {
							for(int i=0; i<cols_control.Length; i++) cols_control[i]=Color.black;
						} else {
							for(int i=0; i<cols_control.Length; i++) cols_control[i]=Color.white;
						}
					}
					int channel_idx=(int)rt.source_controls_channels[n];
					// apply mask
					if (rt.splat_layer_masked[n] && rt.source_controls_mask[n]) {
						Color[] cols_mask=rt.source_controls_mask[n].GetPixels();
						idx=0;
						int channel_idx_mask=(int)rt.source_controls_mask_channels[n];
						for(int i=0; i<rt.controlA.width*rt.controlA.width; i++) {
							cols_control[idx][channel_idx]*=cols_mask[idx][channel_idx_mask];
							idx++;
						}
						idx=0;
					}
					for(int i=0; i<rt.controlA.width*rt.controlA.width; i++) {
						splatData[i,n]=cols_control[idx++][channel_idx]*rt.splat_layer_boost[n]/norm_array[i];
					}
				} else {
					for(int i=0; i<rt.controlA.width*rt.controlA.width; i++) {
						splatData[i,n]=splatData[i,n]/norm_array[i];
					}
				}			
			}
		}
		
		for(int n=0; n<numLayers; n++) {
			if (n==0) {
				for(int i=0; i<cols.Length; i++) {
					cols[i]=new Color(0,0,0,0);
				}				
			}
			for(int i=0; i<cols.Length; i++) {
				cols[i][n%4]=splatData[i,n];
			}				
			if (n==3) {
				rt.controlA.SetPixels(cols,0);
				rt.controlA.Apply(true, false);
			} else if (n==7) {
				rt.controlB.SetPixels(cols,0);
				rt.controlB.Apply(true, false);
			} else if (n==11) {
				rt.controlC.SetPixels(cols,0);
				rt.controlC.Apply(true, false);
			} else if (n==numLayers-1) {
				if (n<4) {
					rt.controlA.SetPixels(cols,0);
					rt.controlA.Apply(true, false);
				} else if (n<8) {
					rt.controlB.SetPixels(cols,0);
					rt.controlB.Apply(true, false);
				} else {
					rt.controlC.SetPixels(cols,0);
					rt.controlC.Apply(true, false);
				}
			}
		}	
		
	}		
	public void InvertChannel(Color[] cols, int channel_idx=-1) {
		if (channel_idx<0) {
			for(int idx=0; idx<cols.Length; idx++) {
				cols[idx].r = 1-cols[idx].r;
				cols[idx].g = 1-cols[idx].g;
				cols[idx].b = 1-cols[idx].b;
				cols[idx].a = 1-cols[idx].a;
			}		
		} else {
			for(int idx=0; idx<cols.Length; idx++) {
				cols[idx][channel_idx] = 1-cols[idx][channel_idx];
			}		
		}
	}	
	public void InvertChannel(Color32[] cols, int channel_idx=-1) {
		if (channel_idx<0) {
			for(int idx=0; idx<cols.Length; idx++) {
				cols[idx].r = (byte)(255-cols[idx].r);
				cols[idx].g = (byte)(255-cols[idx].g);
				cols[idx].b = (byte)(255-cols[idx].b);
				cols[idx].a = (byte)(255-cols[idx].a);
			}		
		} else {
			if (channel_idx==0) {
				for(int idx=0; idx<cols.Length; idx++) {
					cols[idx].r = (byte)(255-cols[idx].r);
				}		
			} else if (channel_idx==1) {
				for(int idx=0; idx<cols.Length; idx++) {
					cols[idx].g = (byte)(255-cols[idx].g);
				}		
			} else if (channel_idx==2) {
				for(int idx=0; idx<cols.Length; idx++) {
					cols[idx].b = (byte)(255-cols[idx].b);
				}		
			} else {
				for(int idx=0; idx<cols.Length; idx++) {
					cols[idx].a = (byte)(255-cols[idx].a);
				}		
			}
			
		}
	}	
	#endif
	
}
