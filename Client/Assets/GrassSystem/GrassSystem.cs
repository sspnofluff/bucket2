﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Flags]
public enum PaintType
{
	None = 0,
	Add = 1,
	SetColor = 2,
	ResetColor = 3,
}



[System.Serializable]
public class GrassNode
{
	public int number;
	public int meshNumber;
	public int size;
	public Transform node;
	public CombineInstance combine;
	public Vector3 pos;
	public Quaternion rot;
	public Vector3 scale;
	public Color vertexColor;
	public bool isVertexColorSet = false;


	public GrassNode(int _number, Transform _node, Vector3 _pos, Quaternion _rot, Vector3 _scale)
	{
		node = _node;
		number = _number;
		pos = _pos;
		rot = _rot;
		scale = _scale;
		isVertexColorSet = false;
	}
}


[System.Serializable]
public class BrushPreset
{
	public string name;
	public List<GrassSource> grassSource;
	public Texture texture;
	public int parts;

	public BrushPreset(string _name)
	{
		name = _name;
		grassSource = new List<GrassSource> ();
	}

}

[System.Serializable]
public class GrassSource
{
	public GrassObject source;
	public int size;
	public int subGroup;
	public float coefficient;
}

[System.Serializable]
public class PartMesh
{
	public MeshFilter MF;
	public Transform currentTransform;
	public MeshRenderer MR;
	public int count;
	public float radius;
	public bool isFar = false;

}

public class GrassSystem : MonoBehaviour
{
	public List<BrushPreset> brushPresets;

	public List<Color> grassPresets;

	public PaintType paintType;

	public Bounds bounds;
	public BrushPreset currentPreset;
	public BlockPlane blockPlane;

	public List<GrassNode> data;
	public List<PartMesh> meshes;

	[HideInInspector]
	public MeshFilter MF;
	public Renderer MR;

	public LayersNumber layersNumber;
	public int vertexCounter;
	public Vector3 rotation;
	public Vector2 scale;
	public float density;
	public int max;
	public float radius;
	public Vector2 randomY;
	public Texture2D[] SplatTex;
	public Texture2D atlas;
	public Texture2D atlas2;
	public int chanceColor;
	public Color[] meshColors;
	public int[] meshTriangles;
	public Vector4 layerBright0123;
	public Vector4 layerBright4567;
	public Vector4 layerSatur0123;
	public Vector4 layerSatur4567;

	public AnimationCurve animCurve = new AnimationCurve(new Keyframe(0.0f, 0.7f), new Keyframe(0.0f, 0.7f));
	public AnimationCurve animCurveDown = new AnimationCurve(new Keyframe(0.0f, 0.8f), new Keyframe(1.0f, 0.4f));
	public AnimationCurve animCurveForward = new AnimationCurve(new Keyframe(0.0f, 0.7f), new Keyframe(0.0f, 0.7f));
	public AnimationCurve animCurveUp = new AnimationCurve(new Keyframe(0.0f, 0.4f), new Keyframe(1.0f, 0.8f));

	public Color color;
	public bool isColorSet = false;
	public Vector2 offset = new Vector2 (0.0f, 0.4f);
	public int selectivePresets = -1;
	public string blockName = "";
	public float colorDifference = 0.25f;

	public MeshCollider tempMeshCollider;

	void OnDrawGizmos()
	{
		Gizmos.color = Color.yellow;

		if (blockPlane != null) 
		{
			if (blockPlane.type == BlockPlaneType.Outpost)
				return;
			
			int areaCount = Mathf.CeilToInt((float)blockPlane.size / 0.17f);
			Vector3 next;
			Vector3 angle;

			Gizmos.DrawLine (new Vector3(-80,0,0), new Vector3(80,0,0)); 

			for (int i = 0; i < areaCount; i++) 
			{
				float step = (float)i / (float)areaCount;
				next = blockPlane.spline.GlobalPos (step);
				angle = blockPlane.spline.GetAngle (step);

				Gizmos.DrawLine (next + (Quaternion.Euler(0,90,0) * angle * -20), next + (Quaternion.Euler(0,90,0) * angle * 20)); 

			}

			next = blockPlane.spline.GlobalPos (1);
			angle = blockPlane.spline.GetAngle (1);

			Gizmos.DrawLine (next + (Quaternion.Euler(0,90,0) * angle * -20), next + (Quaternion.Euler(0,90,0) * angle * 20)); 
		}
	}

}


