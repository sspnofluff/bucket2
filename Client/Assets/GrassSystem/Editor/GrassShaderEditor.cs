﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

public class GrassShaderEditor : ShaderGUI
{
	override public void OnGUI (MaterialEditor materialEditor, MaterialProperty[] properties)
	{
		// render the shader properties using the default GUI
		base.OnGUI (materialEditor, properties);

		// get the current keywords from the material
		Material targetMat = materialEditor.target as Material;
		string[] keyWords = targetMat.shaderKeywords;

		// see if redify is set, then show a checkbox
		bool redify = keyWords.Contains ("TINT_ON");
		EditorGUI.BeginChangeCheck ( );
		redify = EditorGUILayout.Toggle ("Enable Tint", redify);
		if (EditorGUI.EndChangeCheck ( ))
		{
			// if the checkbox is changed, reset the shader keywords
			if (redify)
				targetMat.EnableKeyword("TINT_ON");
			else
				targetMat.DisableKeyword("TINT_ON");
		}
	}
}