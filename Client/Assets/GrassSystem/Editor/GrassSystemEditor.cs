﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

[System.Flags]
public enum CurveType
{
	Down = 0,
	Forward = 1,
	Up = 2,
}

[System.Flags]
public enum DensityType
{
	Trifle = 0,
	Low = 1,
	Competent = 2,
	Dangerous = 3,
	Custom = 4,

}




[CustomEditor(typeof(GrassSystem))]
public class GrassSystemEditor : Editor
{
 
	string[] maps = { "Draw", "Edit"};
	string[] maps2 = { "Down", "Forward", "Up" };
	string[] maps3 = { "Trifle", "Low", "Competent", "Dangerous", "Custom"};


	public int Submeshes;
	public List<Material> materials;
	public GrassSystem myTarget;// = (GrassSystem)target;



	public CurveType curveType;
	public DensityType densityType;


	private Mesh final;
	private int fullCounter;
	private int fullStop;

	private int countBlocks;
	private float transitionSliderParam;

	private bool isDistDencity;
	private float densitySide;
	private int sepX;
	private int sepY;

	private  Renderer rend;
	private  Material mat;
	private Texture2D tex;
	private Texture2D height;
	private Texture2D height2;



	
	void Awake()
	{
		
		//myTarget.rotation = new Vector3(90.0f, 0, 0);
		//myTarget.scale = new Vector2(0.08f, 0.16f);
		//myTarget.density = 0.3f;
		//myTarget.radius = 5.0f;
		//myTarget.randomY = new Vector2 (-30, 30);
		//isPainting = false;
		//offset = new Vector2 (0.0f, 0.4f);

		//myTarget.animCurveDown = new AnimationCurve(new Keyframe(0.0f, 0.8f), new Keyframe(1.0f, 0.4f));
		//myTarget.animCurveForward = new AnimationCurve(new Keyframe(0.0f, 0.7f), new Keyframe(0.0f, 0.7f));
		//myTarget.animCurveUp = new AnimationCurve(new Keyframe(0.0f, 0.4f), new Keyframe(1.0f, 0.8f));
		//blockName = "";
		//selectivePresets = -1;


	}
	
	public override void OnInspectorGUI()
	{
		
		myTarget = (GrassSystem)target;
		//SerializedProperty brushPresets;
		float width = EditorGUIUtility.currentViewWidth;
		GUIStyle _labelStyle;
		_labelStyle = new GUIStyle();
		_labelStyle.alignment = TextAnchor.MiddleCenter;
		_labelStyle.clipping = TextClipping.Overflow;
		_labelStyle.fontSize = 12;
		//GUI.skin = myTarget.skin;



		GUILayout.Label ("Presets", EditorStyles.boldLabel);
		GUILayout.Space (1);
		GUILayout.BeginVertical (GUI.skin.GetStyle ("Box"));
		GUILayout.BeginHorizontal ();


		for (int i = 0; i < myTarget.brushPresets.Count; i++) 
		{
			bool isFirst = (myTarget.currentPreset == myTarget.brushPresets[i]) ? true : false;
			if (GUILayout.Button(myTarget.brushPresets[i].texture, (isFirst) ? GUILayout.Width(90.0f) : GUILayout.Width(75.0f) , (isFirst) ? GUILayout.Height(90.0f) : GUILayout.Height(75.0f)))
				{
				if (myTarget.selectivePresets == i) 
				{
					myTarget.selectivePresets = -1;

				}
				else
				{
					myTarget.currentPreset = myTarget.brushPresets [i];
					myTarget.selectivePresets = i;
				}
				}

		}


		GUILayout.BeginVertical ();

			if (GUILayout.Button ("+", GUILayout.Width (35.0f), GUILayout.Height (35.0f))) {
				myTarget.brushPresets.Add (new BrushPreset ("New Preset"));
				myTarget.selectivePresets = myTarget.brushPresets.Count;
				myTarget.currentPreset = myTarget.brushPresets [myTarget.selectivePresets];
			}
			if (myTarget.selectivePresets!= -1)
			if (GUILayout.Button ("-", GUILayout.Width (35.0f), GUILayout.Height (35.0f))) {
				myTarget.brushPresets.Remove (myTarget.currentPreset);
				myTarget.currentPreset = myTarget.brushPresets [0];
				myTarget.selectivePresets = -1;

			}

		GUILayout.EndVertical ();
		GUILayout.EndHorizontal ();





		if (myTarget.selectivePresets != -1) {
			GUILayout.Label ("Current Preset", EditorStyles.boldLabel);
			GUILayout.Space (1);
			GUILayout.BeginHorizontal ();

			myTarget.currentPreset.name = EditorGUILayout.TextField (myTarget.currentPreset.name, GUILayout.Width (width/2 - 120.0f));
			myTarget.currentPreset.texture = (Texture)EditorGUILayout.ObjectField (myTarget.currentPreset.texture, typeof(Texture), GUILayout.Width (width/2 - 120.0f));
			myTarget.currentPreset.parts = EditorGUILayout.IntField (myTarget.currentPreset.parts, GUILayout.Width (60.0f));
			if (GUILayout.Button ("Add Prefab", GUILayout.Width (80.0f))) {
				myTarget.currentPreset.grassSource.Add (new GrassSource ());
			}
			GUILayout.EndHorizontal ();


			for (int i = 0; i < myTarget.currentPreset.grassSource.Count; i++) {
			
				GUILayout.Label ("Object: " + (i+1).ToString(), EditorStyles.boldLabel);
				GUI.backgroundColor = new Color (0.8f, 0.8f, 0.8f, 1.0f);

				GUILayout.BeginVertical (GUI.skin.GetStyle ("Box"));
				GUILayout.Space (5);
				GUILayout.BeginHorizontal ();
				myTarget.currentPreset.grassSource [i].source = (GrassObject)EditorGUILayout.ObjectField (myTarget.currentPreset.grassSource [i].source, typeof(GrassObject), GUILayout.Width (width - 120.0f));

				if (GUILayout.Button ("Delete", GUILayout.Width (80.0f)))
					myTarget.currentPreset.grassSource.Remove (myTarget.currentPreset.grassSource [i]);
				GUILayout.EndHorizontal ();

				//myTarget.currentPreset.grassSource [i].subGroup = EditorGUILayout.IntField ("Sub Group ID", myTarget.currentPreset.grassSource [i].subGroup, GUILayout.Width (width - width / 5));
				myTarget.currentPreset.grassSource [i].coefficient = EditorGUILayout.FloatField ("Coefficient", myTarget.currentPreset.grassSource [i].coefficient);
				GUILayout.Space (5);
				GUILayout.EndVertical ();
				GUI.backgroundColor = Color.white;

			}
		}

		GUILayout.EndVertical ();
		EditorGUILayout.Separator ();
			
		GUILayout.Label ("Brush Settings", EditorStyles.boldLabel);
		GUILayout.Space (1);
		GUILayout.BeginVertical (GUI.skin.GetStyle ("Box"));
		//GUILayout.Box ("", _labelStyle, GUILayout.Height (2), GUILayout.Width (width));
		//EditorGUILayout.Separator ();
		GUI.Box(GUILayoutUtility.GetRect(width, 20.0f), "Block Vertex Reserve");
		EditorGUI.ProgressBar (EditorGUILayout.GetControlRect (true, 20.0f, _labelStyle, GUILayout.Height (20.0f)), (float)myTarget.vertexCounter / 65535.0f, ""/*"Block limit: " + vertexCounter.ToString() + " / 65535"*/);
		GUILayout.Space (1);

		GUILayout.Label ("Color", EditorStyles.boldLabel);
		GUILayout.Space (1);
		myTarget.color = EditorGUILayout.ColorField (myTarget.color);
		GUILayout.BeginHorizontal ();

		for (int i = 0; i < myTarget.grassPresets.Count; i++) 
		{
			GUI.backgroundColor = myTarget.grassPresets[i];
			if (GUILayout.Button ("", GUILayout.Height(30), GUILayout.Width(30)))
			{
					myTarget.color = myTarget.grassPresets[i];
			}
		}
				GUI.backgroundColor = Color.white;
		GUILayout.EndHorizontal ();
		myTarget.colorDifference = EditorGUILayout.Slider ("Color difference", myTarget.colorDifference, 0, 0.5f);
		myTarget.chanceColor = EditorGUILayout.IntSlider ("Chance (%)", myTarget.chanceColor, 0, 100);
		GUILayout.Space (1);

		GUILayout.Label ("Density", EditorStyles.boldLabel);
		myTarget.radius = EditorGUILayout.Slider ("Radius", myTarget.radius, 1.0f, 20.0f);
		GUILayout.Space (1);


		EditorGUILayout.MinMaxSlider (new GUIContent ("Rand size (" + myTarget.scale.x.ToString ("F2") + " - " + myTarget.scale.y.ToString ("F2") + ")"), ref myTarget.scale.x, ref myTarget.scale.y, 0.01f, 0.25f, GUILayout.Height (20.0f));

		GUILayout.Space (1);
		isDistDencity = EditorGUILayout.Toggle ("Lower at border", isDistDencity);

		densityType = (DensityType)GUILayout.SelectionGrid ((int)densityType, maps3, 5, GUILayout.Height (20.0f));

		string densityString = "";
		switch (densityType) 
		{
		case DensityType.Custom:
			myTarget.density = 0.60f;
			densityString = "Liqiud density (" + myTarget.density.ToString("F2") + ")";
			break;
		case DensityType.Trifle:
			myTarget.density = 0.45f;
			densityString = "Trifle density (" + myTarget.density.ToString("F2") + ")";
			break;
		case DensityType.Low:
			myTarget.density = 0.32f;
			densityString = "Low density (" + myTarget.density.ToString("F2") + ")";
			break;
		case DensityType.Competent:
			myTarget.density = 0.27f;
			densityString = "Competent density (" + myTarget.density.ToString("F2") + ")";
			break;
		case DensityType.Dangerous:
			myTarget.density = 0.2f;
			densityString = "Dangerous density (" + myTarget.density.ToString("F2") + ")";
			break;


		}
		myTarget.density = EditorGUILayout.Slider ("Custom Density", myTarget.density, 0.1f, 1.5f);
		if (isDistDencity)
			densitySide = EditorGUILayout.Slider ("Border Density", densitySide, 0.5f, 2.0f);	
		EditorGUILayout.Separator ();

		//GUILayout.Box ("", _labelStyle, GUILayout.Height (2), GUILayout.Width (width));

		//EditorGUILayout.Separator ();
		GUILayout.Label ("Rotation", EditorStyles.boldLabel);

		myTarget.rotation = EditorGUILayout.Vector3Field ("Mandatory rotation", myTarget.rotation);
		GUILayout.Space (1);


		EditorGUILayout.MinMaxSlider (new GUIContent ("Random Y (" + myTarget.randomY.x.ToString ("F2") + " - " + myTarget.randomY.y.ToString ("F2") + ")"), ref myTarget.randomY.x, ref myTarget.randomY.y, -180, 180);
		GUILayout.Space (1);



		GUILayout.Label ("Curve", EditorStyles.boldLabel);
		GUILayout.Space (1);
		curveType = (CurveType)GUILayout.SelectionGrid ((int)curveType, maps2, 3, GUILayout.Height (20.0f));
		//EditorGUILayout.Separator ();


			switch (curveType) 
			{
		case CurveType.Down:
			myTarget.animCurveDown = EditorGUILayout.CurveField (myTarget.animCurveDown, GUILayout.Height(40.0f), GUILayout.Width(width - 25.0f));
			myTarget.animCurve = myTarget.animCurveDown;
				break;
		case CurveType.Forward:
			myTarget.animCurveForward = EditorGUILayout.CurveField (myTarget.animCurveForward, GUILayout.Height(40.0f), GUILayout.Width(width - 25.0f));
			myTarget.animCurve = myTarget.animCurveForward;
				break;
		case CurveType.Up:
			myTarget.animCurveUp = EditorGUILayout.CurveField (myTarget.animCurveUp, GUILayout.Height(40.0f), GUILayout.Width(width - 25.0f));
			myTarget.animCurve = myTarget.animCurveUp;
				break;

			}



		GUILayout.Label ("Paint Mode", EditorStyles.boldLabel);
		GUILayout.Space (1);

		GUILayout.BeginHorizontal ();

		string paintButtonName = "";
		if (myTarget.paintType == PaintType.Add)
		{
			paintButtonName = "End Paint";
			GUI.backgroundColor = Color.green;
		}
		else
		{
			paintButtonName = "Add Grass";
			GUI.backgroundColor = Color.white;
		}
		if (GUILayout.Button (paintButtonName, GUILayout.Height (50.0f), GUILayout.Width(width/3-11.0f)))
		{
			if (myTarget.paintType == PaintType.Add)
				myTarget.paintType = PaintType.None;
			else
				myTarget.paintType = PaintType.Add;

		}

		if (myTarget.paintType == PaintType.SetColor)
		{
			paintButtonName = "End Paint";
			GUI.backgroundColor = Color.green;
		}
		else
		{
			paintButtonName = "Set Color";
			GUI.backgroundColor = Color.white;
		}
		if (GUILayout.Button (paintButtonName, GUILayout.Height (50.0f), GUILayout.Width(width/3-11.0f)))
		{
			if (myTarget.paintType == PaintType.SetColor)
				myTarget.paintType = PaintType.None;
			else
				myTarget.paintType = PaintType.SetColor;

		}

		if (myTarget.paintType == PaintType.ResetColor)
		{
			paintButtonName = "End Paint";
			GUI.backgroundColor = Color.green;
		}
		else
		{
			paintButtonName = "Reset Color";
			GUI.backgroundColor = Color.white;
		}
		if (GUILayout.Button (paintButtonName, GUILayout.Height (50.0f), GUILayout.Width(width/3-11.0f)))
		{
			if (myTarget.paintType == PaintType.ResetColor)
				myTarget.paintType = PaintType.None;
			else
				myTarget.paintType = PaintType.ResetColor;

		}

		GUILayout.EndHorizontal ();
		GUI.backgroundColor = Color.white;
		GUILayout.Space (1);
		GUILayout.BeginHorizontal ();
		if (GUILayout.Button ("Clear All", GUILayout.Height (50.0f), GUILayout.Width(width/2-15.0f)))
		{
			Clean ();

		}

		if (GUILayout.Button ("Build Block", GUILayout.Height (50.0f), GUILayout.Width(width/2-15.0f)))
		{
			AddBlock ();
		}
		GUILayout.EndHorizontal ();
		GUILayout.EndVertical ();
	}

	void Clean()
	{
		myTarget.vertexCounter = 0;
		fullCounter = fullStop;

		myTarget = (GrassSystem)target;

		if (myTarget.data != null)
			myTarget.data.Clear ();

		if (myTarget.meshes != null)
			myTarget.meshes.Clear ();
		
		int childsCount = myTarget.transform.childCount;

		List<GameObject> childs = new List<GameObject>();

		foreach (Transform child in myTarget.transform) 
		{
			childs.Add (child.gameObject);
		}

		for (int i = 0; i < childsCount; i++) 
		{
			DestroyImmediate (childs[i].gameObject);
		}

	}

	void ProcedurePlaceTry(Vector3 _center)
	{
		Vector2 circle = UnityEngine.Random.insideUnitCircle * myTarget.radius;
		Ray ray = new Ray (new Vector3 (_center.x + circle.x, 50.0f, _center.z + circle.y), -Vector3.up);
		//Ray ray = new Ray (new Vector3 (_center.x, 50.0f, _center.z), -Vector3.up);
		RaycastHit hit;
		bool result = myTarget.tempMeshCollider.Raycast (ray, out hit, 150.0f);

		if (myTarget.data == null)
			myTarget.data = new List<GrassNode> ();

		if (result)
			PlaceData (hit, _center);
		else
			sepX++;
		
	}

	void OnSceneGUI()
	{
		myTarget = (GrassSystem)target;


		if (myTarget.paintType!= PaintType.None) 
		{

			RaycastHit hit;
			Ray ray = HandleUtility.GUIPointToWorldRay (Event.current.mousePosition);
			bool result = Physics.Raycast (ray, out hit);
			Handles.DrawWireDisc (hit.point, hit.normal, myTarget.radius);



			rend = myTarget.MR;
			if (rend == null)
				Debug.Log ("Renderer is Null");
			mat = rend.sharedMaterial;
			if (mat == null)
				Debug.Log ("Mat is Null");
			tex = (Texture2D)mat.GetTexture ("_Control1");
			if (tex == null)
				Debug.Log ("Texture is Null");
			
			height = (Texture2D)mat.GetTexture ("_TERRAIN_HeightMap");
			if (height == null)
				Debug.Log ("Height is Null");

			if (myTarget.layersNumber == LayersNumber._4Layers) 
			{
				myTarget.SplatTex = new Texture2D[4];
				myTarget.SplatTex [0] = (Texture2D)mat.GetTexture ("_SplatA0");
				myTarget.SplatTex [1] = (Texture2D)mat.GetTexture ("_SplatA1");
				myTarget.SplatTex [2] = (Texture2D)mat.GetTexture ("_SplatA2");
				myTarget.SplatTex [3] = (Texture2D)mat.GetTexture ("_SplatA3");
			}

			if (myTarget.layersNumber == LayersNumber._Transition) 
			{
				myTarget.atlas = (Texture2D)mat.GetTexture ("_SplatAtlasA");
				myTarget.atlas2 = (Texture2D)mat.GetTexture ("_SplatAtlasB");
				height2 = (Texture2D)mat.GetTexture ("_TERRAIN_HeightMap2");
				transitionSliderParam = mat.GetFloat ("_TransitionSlider");
			}
			if (Event.current.type == EventType.mouseUp && Event.current.button == 1 && result) {
				
				if (myTarget.blockName == "") {
					if (hit.collider.gameObject != null) {
						myTarget.blockName = hit.collider.gameObject.name;
						myTarget.MR = hit.collider.transform.parent.FindChild ("mesh").GetComponent<Renderer> ();
					}
				}

				if (myTarget.blockPlane == null) {
					myTarget.blockPlane = hit.collider.transform.parent.GetComponent<BlockPlane> ();

				}

				if (myTarget.MR == null) 
					myTarget.MR = hit.collider.transform.parent.FindChild ("mesh").GetComponent<Renderer> ();

				

				switch (myTarget.paintType) 
				{

				case PaintType.SetColor:
					{
						int currentNumber;
						if (!myTarget.isColorSet) 
						{
							for (int i = 0; i < myTarget.data.Count; i++) 
							{
								if (!myTarget.data [i].isVertexColorSet) 
								{
									currentNumber = myTarget.data [i].number;
									for (int z = 0; z < myTarget.data [i].meshNumber; z++)
										currentNumber -= myTarget.meshes [z].MF.sharedMesh.vertexCount;
									myTarget.data [i].vertexColor = myTarget.meshes [myTarget.data [i].meshNumber].MF.sharedMesh.colors [currentNumber];	
									myTarget.data [i].isVertexColorSet = true;
								}
							}
							myTarget.isColorSet = true;
						}
					
						for (int i = 0; i < myTarget.data.Count; i++) 
						{
							if (Math3d.GetChanceOf (myTarget.chanceColor)) 
							{
								float dist = Vector3.Distance (myTarget.data [i].pos, hit.point);
								if (dist < myTarget.radius) {
									currentNumber = myTarget.data [i].number;
									for (int z = 0; z < myTarget.data [i].meshNumber; z++)
										currentNumber -= myTarget.meshes [z].MF.sharedMesh.vertexCount;
									Color[] colors = myTarget.meshes [myTarget.data [i].meshNumber].MF.sharedMesh.colors;
									float ColR = Random.Range (1.0f - myTarget.colorDifference, 1.0f + myTarget.colorDifference);
									for (int j = currentNumber; j < currentNumber + 8; j++)
										colors [j] = Color.Lerp (myTarget.color * ColR, colors [j], dist / myTarget.radius);
									myTarget.meshes [myTarget.data [i].meshNumber].MF.sharedMesh.colors = colors;
								}
							}
						}
						break;
					}
				case PaintType.ResetColor:
					{
						int currentNumber;
						if (!myTarget.isColorSet) {
							for (int i = 0; i < myTarget.data.Count; i++) {
								currentNumber = myTarget.data [i].number;
								for (int z = 0; z < myTarget.data [i].meshNumber; z++)
									currentNumber -= myTarget.meshes [z].MF.sharedMesh.vertexCount;
								myTarget.data [i].vertexColor = myTarget.meshes [myTarget.data [i].meshNumber].MF.sharedMesh.colors [currentNumber];	
							}
							myTarget.isColorSet = true;
						}

						for (int i = 0; i < myTarget.data.Count; i++) 
						{
							if (Math3d.GetChanceOf (myTarget.chanceColor)) 
							{
								float dist = Vector3.Distance (myTarget.data [i].pos, hit.point);
								if (dist < myTarget.radius) {
									currentNumber = myTarget.data [i].number;
									for (int z = 0; z < myTarget.data [i].meshNumber; z++)
										currentNumber -= myTarget.meshes [z].MF.sharedMesh.vertexCount;
									Color[] colors = myTarget.meshes [myTarget.data [i].meshNumber].MF.sharedMesh.colors;
									for (int j = currentNumber; j < currentNumber + 8; j++)
										colors [j] = Color.Lerp (myTarget.data [i].vertexColor, colors [j], dist / myTarget.radius);
									myTarget.meshes [myTarget.data [i].meshNumber].MF.sharedMesh.colors = colors;
								}
							}
						}
						break;
					}
				case PaintType.Add:
					myTarget.isColorSet = false;
					for (int i = 0; i < myTarget.meshes.Count; i++) {
						if (Vector3.Distance (myTarget.meshes [i].MR.bounds.center, hit.point) < myTarget.radius * myTarget.meshes [i].radius)
							myTarget.meshes [i].isFar = false;
						else
							myTarget.meshes [i].isFar = true;
					}

					sepX = 0;
					sepY = 0;
					myTarget.max = Mathf.CeilToInt ((Mathf.PI * myTarget.radius * myTarget.radius) / (myTarget.density * ((isDistDencity) ? densitySide : myTarget.density)));

					for (int i = 0; i < myTarget.max; i++) 
					{					//while (sepX < max) 

						ProcedurePlaceTry (hit.point);

					}




					Mesh meshF = new Mesh ();

					List<CombineInstance> comb = new List<CombineInstance> ();
					int grassCount = 0;
					for (int i = 0; i < myTarget.meshes.Count; i++) {
						grassCount += myTarget.meshes [i].count;
					}


					for (int i = grassCount; i < myTarget.data.Count; i++) {
						Mesh mesh = new Mesh ();
						Transform currentTransform = myTarget.data [i].node;

						mesh.vertices = myTarget.data [i].combine.mesh.vertices;
						mesh.triangles = myTarget.data [i].combine.mesh.triangles;
						mesh.uv = myTarget.data [i].combine.mesh.uv;
						mesh.uv2 = myTarget.data [i].combine.mesh.uv2;
						mesh.normals = myTarget.data [i].combine.mesh.normals;
						mesh.tangents = myTarget.data [i].combine.mesh.tangents;
						int number = Random.Range (0, myTarget.currentPreset.parts);

						Vector3[] vertices = mesh.vertices;
						Color[] colors = new Color[mesh.vertexCount];
						Vector2[] uv = mesh.uv;
						Vector2[] uv2 = mesh.uv;
						float colR = 1.0f;
						for (int z = 0; z < mesh.vertexCount; z++) 
						{
							if (myTarget.currentPreset.parts > 1)
							{
								uv [z] = SetUv (number, z, false);
							}
							if (z % 8 == 0)
								colR = Random.Range (1.0f - myTarget.colorDifference, 1.0f + myTarget.colorDifference);

							if (z % 2 != 0) {
								vertices [z] += currentTransform.InverseTransformDirection (Vector3.forward) * Random.Range (myTarget.offset.x, myTarget.offset.y);//current [j].transform.InverseTransformDirection (myTarget.transform.forward * 5);
							}
							{
								Ray ray3 = new Ray (currentTransform.TransformPoint (vertices [z]), -Vector3.up);
								ray3.origin = new Vector3 (ray3.origin.x, ray3.origin.y + 50.0f, ray3.origin.z);

								RaycastHit hit3;
								bool result3;

								result3 = myTarget.tempMeshCollider.Raycast (ray3, out hit3, 150.0f);

								if (result3) {
									if (myTarget.layersNumber == LayersNumber._4Layers)
										colors [z] = PickColor (hit3, colR, z);
									if (myTarget.layersNumber == LayersNumber._Transition)
										colors [z] = PickColorTransparent (hit3, colR, z);	
								}
									else
									colors [z] = Color.white;
							}
						}
						mesh.vertices = vertices;
						mesh.colors = colors;
						mesh.uv = uv;
						mesh.uv2 = uv2;
						myTarget.data [i].combine.mesh = mesh;
						comb.Add (myTarget.data [i].combine);
						DestroyImmediate (myTarget.data [i].node.gameObject);
					}


					meshF.CombineMeshes (comb.ToArray (), true, true);
					PartMesh partMesh = new PartMesh ();
					GameObject part = new GameObject ();
					part.name = "Part" + myTarget.meshes.Count.ToString ();
					part.transform.parent = myTarget.transform;
					part.transform.position = Vector3.zero;
					part.transform.rotation = Quaternion.identity;

					partMesh.currentTransform = part.transform;
					partMesh.MF = part.AddComponent<MeshFilter> ();
					partMesh.MF.mesh = meshF;

					partMesh.MR = part.AddComponent<MeshRenderer> ();
					partMesh.MR.sharedMaterial = myTarget.currentPreset.grassSource [0].source.GetComponent<Renderer> ().sharedMaterial;
					partMesh.MR.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
					partMesh.count = myTarget.data.Count - grassCount;
					partMesh.radius = myTarget.radius;
					EditorUtility.SetSelectedWireframeHidden (partMesh.MR, true);
					myTarget.meshes.Add (partMesh);


					break;

				}
			}

		}



	}

	public Color PickColor(RaycastHit hit3, float colR, int z)
	{
		float uv1 = hit3.textureCoord2.x;//(float)System.Math.Truncate ((float)hit3.textureCoord2.x);
		float uv2 = hit3.textureCoord2.y;//(float)System.Math.Truncate ((float)hit3.textureCoord2.y);

		Color c = tex.GetPixelBilinear (hit3.textureCoord.x, hit3.textureCoord.y-0.004f);
		Color h = height.GetPixelBilinear (uv1, uv2);

		Vector4 splatControlA = new Vector4 (c.r, c.g, c.b, c.a);
		Vector4 heightV = new Vector4 (h.r, h.g, h.b, h.a);

		Vector4 tha = heightV;
		Vector4 splat_control1 = splatControlA;
		tha.Normalize ();
		splat_control1 = Vector4.Scale (splat_control1, tha);
		splat_control1.Normalize ();
		splat_control1 /= (splat_control1.x + splat_control1.y + splat_control1.z + splat_control1.w);

		Color result = Color.black;
		Color col = myTarget.SplatTex [0].GetPixelBilinear (uv1, uv2); result = col * splat_control1.x;// * hV.x;
		col = myTarget.SplatTex [1].GetPixelBilinear (uv1, uv2); result += col * splat_control1.y;
		col = myTarget.SplatTex [2].GetPixelBilinear (uv1, uv2); result += col * splat_control1.z;
		col = myTarget.SplatTex [3].GetPixelBilinear (uv1, uv2); result += col * splat_control1.w;

		result=Vector4.Lerp(Color.gray, result, Vector4.Dot(splat_control1, myTarget.layerSatur0123));
		result*=Vector4.Dot(splat_control1, myTarget.layerBright0123);  
		result*=colR;

		return result;
	}

	public static Vector3 GetBarycentric(Vector3 aV1, Vector3 aV2, Vector3 aV3, Vector3 aP)
	{
		Vector3 a = aV2 - aV3, b = aV1 - aV3, c = aP - aV3;
		float aLen = a.x * a.x + a.y * a.y + a.z * a.z;
		float bLen = b.x * b.x + b.y * b.y + b.z * b.z;
		float ab = a.x * b.x + a.y * b.y + a.z * b.z;
		float ca = a.x * c.x + a.y * c.y + a.z * c.z;
		float cb = b.x * c.x + b.y * c.y + b.z * c.z;
		float d = aLen * bLen - ab * ab;
		Vector3 B = new Vector3((aLen * cb - ab * ca), (bLen * ca - ab * cb)) /d;
		B.z = 1.0f - B.x - B.y;
		return B;
	}

	public Color PickColorTransparent(RaycastHit hit3, float colR, int z)
	{
		Color c = tex.GetPixelBilinear (hit3.textureCoord.x, hit3.textureCoord.y-0.004f);
		Color h = height.GetPixelBilinear (hit3.textureCoord2.x, hit3.textureCoord2.y);
		Color h2 = height2.GetPixelBilinear (hit3.textureCoord2.x, hit3.textureCoord2.y);

		float rA = myTarget.meshColors [myTarget.meshTriangles [hit3.triangleIndex * 3]].g;
		float rB = myTarget.meshColors [myTarget.meshTriangles [hit3.triangleIndex * 3 + 1]].g;
		float rC = myTarget.meshColors [myTarget.meshTriangles [hit3.triangleIndex * 3 + 2]].g;
		Vector3 baryCenter = hit3.barycentricCoordinate;
		float r = rA * baryCenter.x + rB * baryCenter.y + rC * baryCenter.z;

		float transitionParam = 1 - r * transitionSliderParam;
		Color h3 = Vector4.Lerp (h, h2, transitionParam);

		//Vector4 texSize = mat.GetVector ("_SplatAtlasA_TexelSize");

		float _off = 16 * 0;//texSize.x;
		float _mult=_off*-2+0.5f;

	
		Vector2 uv = fracV (hit3.textureCoord2) * _mult;// + new Vector2(_off,_off);

		Vector4 uvSplat01=new Vector4(uv.x,uv.y,uv.x+0.5f,uv.y);
		Vector4 uvSplat23=new Vector4(uvSplat01.x,uvSplat01.y+0.5f,uvSplat01.z+0.5f,uvSplat01.w+0.5f);

		Vector4 splatControlA = new Vector4 (c.r, c.g, c.b, c.a);
		Vector4 heightV = new Vector4 (h.r, h.g, h.b, h.a);
		Vector4 heightV2 = new Vector4 (h2.r, h2.g, h2.b, h2.a);

		Vector4 tha = Vector4.Lerp (heightV, heightV2, transitionParam);
		Vector4 splat_control1 = splatControlA;
		tha.Normalize ();
		splat_control1 = Vector4.Scale (splat_control1, tha);
		splat_control1.Normalize ();
		splat_control1 /= (splat_control1.x + splat_control1.y + splat_control1.z + splat_control1.w);

		Color result = Color.black;
		Color col = Color.black;
		col = Color.Lerp(myTarget.atlas.GetPixelBilinear (uvSplat01.x, uvSplat01.y),myTarget.atlas2.GetPixelBilinear (uvSplat01.x, uvSplat01.y), transitionParam); col.a = 1.0f; result = col * splat_control1.x;// * hV.x;
		col = Color.Lerp(myTarget.atlas.GetPixelBilinear (uvSplat01.z, uvSplat01.w),myTarget.atlas2.GetPixelBilinear (uvSplat01.z, uvSplat01.w), transitionParam); col.a = 1.0f; result += col * splat_control1.y;
		col = Color.Lerp(myTarget.atlas.GetPixelBilinear (uvSplat23.x, uvSplat23.y),myTarget.atlas2.GetPixelBilinear (uvSplat23.x, uvSplat23.y), transitionParam); col.a = 1.0f; result += col * splat_control1.z;
		col = Color.Lerp(myTarget.atlas.GetPixelBilinear (uvSplat23.z, uvSplat23.w),myTarget.atlas2.GetPixelBilinear (uvSplat23.z, uvSplat23.w), transitionParam); col.a = 1.0f; result += col * splat_control1.w;

		result=Vector4.Lerp(Color.gray, result, Vector4.Dot(splat_control1, Vector4.Lerp(myTarget.layerSatur0123, myTarget.layerSatur4567, transitionParam)));
		result*=Vector4.Dot(splat_control1, Vector4.Lerp(myTarget.layerBright0123, myTarget.layerBright4567, transitionParam));  
		result*=colR;

		return result;
	}

	Vector2 fracV(Vector2 source)
	{
		Vector2 result;
		if (source.x>=0)
			result.x = source.x-Mathf.Floor(source.x);
		else
			result.x = source.x-Mathf.Ceil(source.x);
		if (source.y>=0)
			result.y = source.y-Mathf.Floor(source.y);
		else
			result.y = source.y-Mathf.Ceil(source.y);
		return result;
	}

	public Vector2 SetUv(int number, int vertex, bool one)
	{
		if (one) {
			if (vertex % 4 == 0)
				return new Vector2 (0.5f, 0.5f);
			if (vertex % 4 == 1)
				return new Vector2 (0.0f, 1.0f);
			if (vertex % 4 == 2)
				return new Vector2 (0.5f, 1.0f);
			if (vertex % 4 == 3)
				return new Vector2 (0.0f, 0.5f);
		} else {
			switch (number) {
			case 0:
			
				if (vertex % 4 == 0)
					return new Vector2 (0.5f, 0.5f);
				if (vertex % 4 == 1)
					return new Vector2 (0.0f, 1.0f);
				if (vertex % 4 == 2)
					return new Vector2 (0.5f, 1.0f);
				if (vertex % 4 == 3)
					return new Vector2 (0.0f, 0.5f);
				break;
			case 1:

				if (vertex % 4 == 0)
					return new Vector2 (1.0f, 0.5f);
				if (vertex % 4 == 1)
					return new Vector2 (0.5f, 1.0f);
				if (vertex % 4 == 2)
					return new Vector2 (1.0f, 1.0f);
				if (vertex % 4 == 3)
					return new Vector2 (0.5f, 0.5f);
				break;
			case 2:

				if (vertex % 4 == 0)
					return new Vector2 (0.5f, 0.0f);
				if (vertex % 4 == 1)
					return new Vector2 (0.0f, 0.5f);
				if (vertex % 4 == 2)
					return new Vector2 (0.5f, 0.5f);
				if (vertex % 4 == 3)
					return new Vector2 (0.0f, 0.0f);
				break;
			case 3:

				if (vertex % 4 == 0)
					return new Vector2 (1.0f, 0.0f);
				if (vertex % 4 == 1)
					return new Vector2 (0.5f, 0.5f);
				if (vertex % 4 == 2)
					return new Vector2 (1.0f, 0.5f);
				if (vertex % 4 == 3)
					return new Vector2 (0.5f, 0.0f);
				break;
			}
		}
		return Vector2.zero;
	}

	
	void  AddBlock()
	{
		
		myTarget.vertexCounter = 0;
		fullStop = fullCounter;

		myTarget = (GrassSystem)target;
		Matrix4x4 myTransform = myTarget.transform.worldToLocalMatrix;

		materials = new List<Material> ();
		Submeshes = -1;
		for (int i = 0; i < myTarget.currentPreset.grassSource.Count; i++)
		{
			if (Submeshes < myTarget.currentPreset.grassSource [i].subGroup) 
			{
				Submeshes = myTarget.currentPreset.grassSource [i].subGroup;
				materials.Add (myTarget.currentPreset.grassSource [i].source.material);
			}
		}
		Submeshes++;

		List<CombineInstance>[] combine = new List<CombineInstance> [Submeshes];
		for (int i = 0; i < Submeshes; i++) 
		{
			combine[i] = new List<CombineInstance> ();
		}




		for (int i = 0; i < myTarget.meshes.Count; i++) 
		{
			Transform child = myTarget.meshes [i].currentTransform;
			MeshFilter current = myTarget.meshes [i].MF;

			CombineInstance combineInstance = new CombineInstance ();

			Mesh mesh = new Mesh ();

			combineInstance.mesh = myTarget.meshes [i].MF.sharedMesh;
			combineInstance.transform = myTransform * myTarget.meshes [i].currentTransform.localToWorldMatrix;

			combine[0].Add (combineInstance);

			
		}

		CombineInstance[] combineFinal = new CombineInstance[Submeshes];
		Mesh[] meshS = new Mesh[Submeshes];

		for (int i = 0; i < Submeshes; i++) 
		{
			//rend.materials [i] = new Material ();
			meshS [i] = new Mesh ();
			meshS [i].name = "Combine " + i.ToString ();
			meshS [i].CombineMeshes (combine [i].ToArray (), true, true);
			combineFinal [i] = new CombineInstance ();
			combineFinal [i].mesh = meshS [i];
			combineFinal [i].subMeshIndex = 0;
		}

		final = new Mesh ();
		final.CombineMeshes (combineFinal, false, false);

		string landString = myTarget.blockPlane.landType.ToString ();
		string roadName = myTarget.blockPlane.nominal.Substring (landString.Length, myTarget.blockPlane.nominal.Length - landString.Length);
		string pathNew = "Assets/Art/roads/" + landString + "/" + roadName;

		if (!Directory.Exists (pathNew + "/grassMeshes"))
		{
			AssetDatabase.CreateFolder (pathNew, "grassMeshes");
			AssetDatabase.Refresh ();
			AssetDatabase.SaveAssets ();
		}

		pathNew += "/grassMeshes";
		string[] files = Directory.GetFiles (pathNew);
		int countFiles = files.Length;

		GameObject block = new GameObject ();
		block.AddComponent<BoundsCorrector> ();
		block.name = "grassMesh " + countFiles.ToString();
		Debug.Log (block.name);
		Transform grassTransform = myTarget.blockPlane.transform.Find ("grass");
		if (grassTransform == null) 
		{
			GameObject grassGO = new GameObject (); 
			grassTransform = grassGO.transform;
			grassTransform.parent = myTarget.blockPlane.transform;
			grassTransform.position = Vector3.zero;
			grassTransform.rotation = Quaternion.identity;
			grassGO.name = "grass";
		}

		block.transform.parent = grassTransform;
		block.transform.localPosition = Vector3.zero;

		MeshFilter MF = block.gameObject.AddComponent<MeshFilter> ();
		MeshRenderer MR = block.gameObject.AddComponent<MeshRenderer> ();

		MF.sharedMesh = new Mesh ();
		MF.sharedMesh = final;
		MR.materials = materials.ToArray ();
		MR.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

		Clean ();



		pathNew += "/grassMesh" + countFiles.ToString() + ".asset";

		CreateOrReplaceAsset(MF.sharedMesh, pathNew);

		AssetDatabase.Refresh();
		AssetDatabase.SaveAssets();
		Debug.Log (sepX + " " + sepY);


	}

	T CreateOrReplaceAsset<T> (T asset, string path) where T:UnityEngine.Object{
		T existingAsset = AssetDatabase.LoadAssetAtPath<T>(path);

		if (existingAsset == null){
			AssetDatabase.CreateAsset(asset, path);
			existingAsset = asset;
		}
		else{
			EditorUtility.CopySerialized(asset, existingAsset);
		}

		return existingAsset;
	}
	

	bool PlaceData(RaycastHit _hit, Vector3 center)
	{
		Matrix4x4 myTransform = myTarget.transform.worldToLocalMatrix;

		float dist = Vector3.Distance (_hit.point, center);
		float density2 = (isDistDencity) ? Mathf.Lerp(densitySide, myTarget.density,myTarget.animCurve.Evaluate(dist / myTarget.radius)) : myTarget.density;
		float lerp = 1.0f;


		Color c = tex.GetPixelBilinear (_hit.textureCoord.x-0.005f, _hit.textureCoord.y-0.005f);
		Color h = height.GetPixelBilinear(_hit.textureCoord2.x-0.005f, _hit.textureCoord2.y-0.005f);

		Vector4 colorFloat = c;
		colorFloat.Normalize ();
		Vector4 heightFloat = h;
		float lenght = heightFloat.SqrMagnitude ();

		Vector4 energy = Vector4.Scale (colorFloat, heightFloat);

		if (energy.z > energy.x + energy.y + energy.w) 
		{
			sepX++;
			return false;
		}
		else
		if (energy.w > energy.z + energy.y + energy.x) 
		{
			sepX++;
			return false;
		}



		int tempCount = 0;
		for (int i = 0; i < myTarget.meshes.Count; i++) 
		{
			if (!myTarget.meshes [i].isFar) 
			{
				if (Vector3.Distance (myTarget.meshes [i].MR.bounds.center, _hit.point) < (myTarget.meshes [i].radius + dist)) 
				{
					for (int j = tempCount; j < myTarget.meshes [i].count + tempCount; j++) 
					{
						if (Vector3.Distance (myTarget.data [j].pos, _hit.point) < density2) 
						{
							sepX++;
							return false;
						}
							
					}
				}
			}
			tempCount += myTarget.meshes [i].count;


		}

		for (int j = tempCount; j < myTarget.data.Count; j++) 
		{
			if (Vector3.Distance (myTarget.data [j].pos, _hit.point) < density2) 
			{
				sepX++;
				return false;
			}
				
		}	

		float maxcoof = 0;

		for (int i = 0; i < myTarget.currentPreset.grassSource.Count; i++)
			maxcoof += myTarget.currentPreset.grassSource[i].coefficient;

		float currentCoof = Random.Range(0, maxcoof);

		float coof = 0;
		int select = -1;

		for (int i = 0; i < myTarget.currentPreset.grassSource.Count; i++)
		{
			coof += myTarget.currentPreset.grassSource[i].coefficient;
			if (coof >= currentCoof)
			{
				select = i;
				break;
			}
		}

		GrassObject Temp = Instantiate<GrassObject> (myTarget.currentPreset.grassSource [select].source);
		Temp.transform.position = _hit.point;
		Temp.transform.rotation = Quaternion.LookRotation (_hit.normal);

		Temp.transform.RotateAround (_hit.point, _hit.normal, Random.Range (myTarget.randomY.x, myTarget.randomY.y));

		float random = Random.Range (myTarget.scale.x * myTarget.animCurve.Evaluate(dist / myTarget.radius), myTarget.scale.y * myTarget.animCurve.Evaluate(dist / myTarget.radius));
		Temp.transform.localScale = new Vector3 (random, random, random);
		Temp.transform.parent = myTarget.transform;


		int currentVertex = myTarget.vertexCounter;
		myTarget.vertexCounter += Temp.vertexCount;
		fullCounter += Temp.vertexCount;

		GrassNode currentNode = new GrassNode (myTarget.currentPreset.grassSource [select].subGroup, Temp.transform, _hit.point, Temp.transform.rotation, Temp.transform.localScale);
		currentNode.combine = new CombineInstance ();
		currentNode.combine.mesh = Temp.mf.sharedMesh;
		currentNode.meshNumber = myTarget.meshes.Count;
		//currentNode.size = 
		currentNode.number = currentVertex;
		currentNode.combine.transform = myTransform * Temp.transform.localToWorldMatrix;


		sepX++;
		sepY++;
		myTarget.data.Add (currentNode);

	

		if (myTarget.vertexCounter > 64000) 
		{
			AddBlock ();

		}

		return true;
	}
	
	public static Mesh Quad(Vector3 origin, Vector3 width, Vector3 length)
	{
		var normal = Vector3.Cross(width, length).normalized;
    var mesh = new Mesh
		{
			vertices = new[] { origin - length, origin - length + width, origin + length + width, origin + length},
			//vertices = new[] { origin, origin, origin, origin},
        normals = new[] { normal, normal, normal, normal },
        uv = new[] { new Vector2(-1, 0), new Vector2(-1, 1), new Vector2(1, 1), new Vector2(1, 0) },
        triangles = new[] { 0, 1, 2, 0, 2, 3}
    };
		Color[] colors = new Color[]{ Color.white, Color.white, Color.white, Color.white };
		mesh.colors = colors;
    return mesh;
}


}