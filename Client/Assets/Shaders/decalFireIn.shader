Shader "DecalFireIn" 
{
	Properties 
	{
		_Diffuse("Diffuse color", Color) = (1,1,1,1)
		_DiffTex("Diffuse (RGBA)", 2D) = "white" {}
		_Offset("offset", Float) = 0

	}
	SubShader 
	{
		Tags 
		{ 
			"Queue"="Transparent-100"
			"RenderType"="Transparent" 
		}
		Blend SrcAlpha OneMinusSrcAlpha
		ColorMask RGB
		Lighting Off
		ZTest Always
		
		CGPROGRAM
		#pragma debug
		#pragma surface surf Custom alpha vertex:vert 

		#include "Assets/VacuumShaders/Curved World/Shaders/cginc/CurvedWorld_Base.cginc"

		sampler2D _DiffTex;
		
		float4 _Diffuse;
		float _Offset;


		struct Input 
		{
			float2 uv_DiffTex : TEXCOORD0;
			float2 uv_DiffTex2 : TEXCOORD1;
			float4 color : COLOR;
			float3 worldRefl; 
			float3 worldPos; 
			INTERNAL_DATA
		};

		void vert (inout appdata_full v) 
		{
			V_CW_TransformPoint(v.vertex);
		}

		struct SurfaceOutputCustom {
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half3 Reflection;			
			half  Specular;
			half  Gloss;
			half  Alpha;
			half  Reflectivity;
		};
		
		void surf(Input IN, inout SurfaceOutputCustom o) 
		{

			float2 uv = IN.uv_DiffTex;
			uv.y += _Offset;
			half4 diffTex = tex2D(_DiffTex, IN.uv_DiffTex);
			half4 fireTex = tex2D(_DiffTex, uv);
			half4 diffTex2 = tex2D(_DiffTex, IN.uv_DiffTex2);
			o.Albedo = _Diffuse;
			o.Alpha = diffTex.g * diffTex2.r * fireTex.b * IN.color.r;
		}
		
		half4 LightingCustom(SurfaceOutputCustom s, half3 lightDir, half3 viewDir, half atten)
		{
		
			half4 c;
			c.rgb = s.Albedo;
			c.a = s.Alpha * _Diffuse.a;
			return c;
		}
		
		ENDCG
	} 
	FallBack off
}