﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DurabilityInfoModule : MonoBehaviour
{
    public ModuleSlot slotModule;
    public RectTransform currentTransform;
    public Slider durability;
    public Text durabilityText;
    public Image durabilityImage;
    public Image durabilityFill;
}
