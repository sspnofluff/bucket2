﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BonusElements : MonoBehaviour {

    public RectTransform currentRectTransform;

    public Text coins;
    public Text damaged;
    public Text ruined;

    public int coinsCount;
    public int damagedCount;
    public int ruinedCount;

    public void SetCoins(int _data)
    {
        coinsCount += _data;
        coins.text = coinsCount.ToString();
    }

    public void SetDamaged(int _data)
    {
        damagedCount += _data;
        damaged.text = damagedCount.ToString();
    }

    public void SetRuined(int _data)
    {
        ruinedCount += _data;
        ruined.text = ruinedCount.ToString();
    }

    public void Restore()
    {
        coinsCount = 0;
        damagedCount = 0;
        ruinedCount = 0;
        coins.text = "0";
        damaged.text = "0";
        ruined.text = "0";

    }

    public void CopyFrom(BonusElements _be)
    {
        coinsCount = _be.coinsCount;
        damagedCount = _be.damagedCount;
        ruinedCount = _be.ruinedCount;
        coins.text = coinsCount.ToString();
        damaged.text = damagedCount.ToString();
        ruined.text = ruinedCount.ToString();
    }
}
