﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEditor;

public static class RoadEditorTools
{
	[MenuItem("Tools/Roads/EditRoad")]
	public static void RoadEdit()
	{
		Selection.activeGameObject.AddComponent<Road> ();
	}

	[MenuItem("Tools/Roads/MergeRoads")]
	public static bool RoadMerge()
	{
		bool _mfCheck = true;
		int count = Selection.gameObjects.Length;
		if (count < 2) 
		{
			Debug.LogError ("In selection less then two gameobjects. Merging Failure.");
			return false;
		}
		else
		{
			MeshFilter[] MF = new MeshFilter[count];
			Material[] Mat = new Material[count];
			for (int i=0; i<count; i++)
			{
				MF[i] = Selection.gameObjects[i].GetComponent<MeshFilter> ();
				if (MF [i] == null)
					_mfCheck = false;

			}

			if (!_mfCheck) 
			{
				Debug.LogError ("Non Mesh Filter Compoment in all selection gameobjects. Merging Failure.");
				return false;
			}

			List<CombineInstance> combine = new List<CombineInstance> ();

			for (int i=0; i<count; i++)
			{
				Mat[i] = Selection.gameObjects[i].GetComponent<MeshRenderer> ().sharedMaterial;
				CombineInstance combineInstance = new CombineInstance ();
				combineInstance.mesh = MF [i].sharedMesh;
				combine.Add (combineInstance);
			}

			Mesh combMesh = new Mesh ();
			combMesh.CombineMeshes (combine.ToArray (), false, false);
			BlockPlane blockPlane = Selection.gameObjects [0].transform.parent.parent.GetComponent<BlockPlane> ();
			string newName = blockPlane.blockName + "_road";
			combMesh.name = newName;
			Selection.gameObjects [0].name = newName;

			PrefabCollector.CreateOrReplaceAssetMesh(combMesh, "Assets/Art/roads/" + blockPlane.landType.ToString().ToLower() + "/" + blockPlane.blockName.ToLower() + "/road/" + newName + ".asset");

			MF[0].sharedMesh = combMesh;
			MeshRenderer MR = Selection.gameObjects [0].GetComponent<MeshRenderer> ();
			MR.sharedMaterials = Mat;

			//for (int i = 1; i < count; i++) 
			//{
			//	GameObject.DestroyImmediate(Selection.gameObjects[i]);
			//}

			Debug.Log ("Merging Success.");
			return true;
		}
	}
}
#endif