using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class MiscSource
{
	public string name;
	public List<string> resourcesName;
	public int image;
	public MiscType miscType;
	public int cost;
	public CounterTimerF dist;

	public MiscSource()
	{
		name = "New misc";
		image = 0;
		miscType = MiscType.Board;
		cost = 0;
		dist = new CounterTimerF (55, 15, true);

	}

}

[System.Serializable]
public class MiscSources : ScriptableObject
{
	public List<MiscSource> unit;

	public int GetImage(MiscType _type)
	{
		for (int i = 0; i < unit.Count; i++) 
		{
			if (unit [i].miscType == _type)
				return unit [i].image;
		}

		return 0;
	}


}