﻿using UnityEngine;
using System.Collections;
using Dest.Math;

public class PlaneSplinesController : MonoBehaviour {

    public CatmullRomSpline3[] splines;

    public CatmullRomSpline3 GetRandomSpline()
    {
        int count = splines.Length;
        int randomSelect = Random.Range(0, count);
        return splines[randomSelect];
    }
}
