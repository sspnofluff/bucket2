﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Dest.Math;

public class Quad2dMarker : MonoBehaviour
{
    public SplineType type;

    Polygon2 polygon;
    public List<Vector2> data;
    public List<GameObject> spheres;
    public int count = 0;
    public bool isShow = false;
	public bool isVisible = true;


	public bool Check(Vector3 _Pos, bool isLocal)
    {
        polygon = new Polygon2(data.ToArray());
		Vector2 point = (isLocal) ? transform.InverseTransformPoint (_Pos).ToVector2XZ () : _Pos.ToVector2XZ();
		if (polygon.ContainsSimple(point))
            return true;

        else
            return false;

    }

#if UNITY_EDITOR
    public void Add()
    {
        if (data==null)
            data = new List<Vector2>();

        if (spheres==null)
            spheres = new List<GameObject>();

        data.Add(new Vector2(0,0));
        spheres.Add(Locators.CreateLocatorLocal(Vector3.zero, Quaternion.identity, transform, count.ToString()).gameObject);
        count++;

    }

    public void Show()
    {
        isShow = true;

        if (spheres==null)
            spheres = new List<GameObject>();

        for (int i = 0; i<count; i++)
            spheres.Add(Locators.CreateLocatorLocal(data[i].ToVector3XZ(), Quaternion.identity, transform, i.ToString()).gameObject);

    }

    public void Apply()
    {
         isShow = false;

        spheres.Clear();

    }


    public virtual void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Draw();
        
    }

    protected void Draw()
    {
		if (isVisible) 
		{
			for (int i = 0; i < count - 1; i++)
				Gizmos.DrawLine (transform.TransformPoint (data [i].ToVector3XZ ()), transform.TransformPoint (data [i + 1].ToVector3XZ ()));
			if (count > 1)
				Gizmos.DrawLine (transform.TransformPoint (data [count - 1].ToVector3XZ ()), transform.TransformPoint (data [0].ToVector3XZ ()));

			if (isShow)
			if (spheres != null)
				for (int i = 0; i < spheres.Count; i++)
					if (spheres [i] != null)
						data [i] = spheres [i].transform.localPosition.ToVector2XZ ();
		}
    }
#endif

    	
}

