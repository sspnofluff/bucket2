﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AmmoSource : MonoBehaviour
{
    public RectTransform currentTransform;
    public Image ico;
    public Image back;
    public Image keyIco;
    public Text key;
    public Text data;
}
