﻿using UnityEngine;
using System.Collections;

public class Wagon: Unit
{
    public Trailer Parent;
    public Vector3 startPose;
    public WheelCollider[] wheelsColliderBack;

    public CarAntiRollBar[] Arb;

    public override void Initialize(bool activeObject)
    {
        base.Initialize(activeObject);
        Arb = GetComponents<CarAntiRollBar>();
        currentRigidbody.centerOfMass = centerOfMass.localPosition;

        //intersectRect.CreateLocals(currentRenderer.bounds);
        //intersectRect.UpdateRect(currentTransform);
    }
}
