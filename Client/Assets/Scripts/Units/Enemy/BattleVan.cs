﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleVan : EnemyVehicle
{

    public CounterTimerF changeTimer;
    float third = 40.0f;
    public EnemyMachineGun MachineGun;
    public CounterTimerF defaultTimer;
    public CounterTimerF rotateTimer;
    bool rightSide = true;
    public FollowMoving moving;
    public Roles roles;

    public Object mine;
    public CounterTimerF minerTimer;
    public CounterTimerF machineGunTimer;
    public Vector2 mineDeploy;
    public Transform mineLauncher;
    public Transform gunTurret;
    public int usedLine;

    private Vector3 RelativeHeroPosition;

    public float MaxDistance = 30.0f;
    public float AverageDistance = 25.0f;
    public float MinDistance = 20.0f;

    public float AccelerationBonus = 100.0f;
    public float BrakeBonus = 200.0f;
    public float SpeedDifference = 5.0f;


    public override void RevertObject()
    {
        base.RevertObject();
        defaultTimer = new CounterTimerF(6.0f, 6.0f);
        minerTimer.current = minerTimer.max;
        machineGunTimer.current = machineGunTimer.max;
        moving = FollowMoving.Move;
        roles = Roles.Deceive;
    }

   
    
    public override void PrepareObject()
    {
        base.PrepareObject();
        roles = Roles.Deceive;
        defaultTimer = new CounterTimerF(6.0f, 6.0f);
    }

    public override void SwitchSkillStatus()
    {

        Vector3 directionToTarget = Vector3.Normalize(Main.instance.hero.currentTransform.position - currentTransform.position);
        float yAngle = 0;
        Vector3 RelativeTargetPosition = currentTransform.InverseTransformPoint(Main.instance.hero.currentTransform.position);
        if (RelativeTargetPosition.x > 0)
            yAngle = Vector3.Angle(currentTransform.right, directionToTarget);
        else
            yAngle = Vector3.Angle(currentTransform.right * -1, directionToTarget) * -1;

        float factor = RelativeTargetPosition.z;
        MovementFunctions(Waypoint, true, true, false);

        if (healthPoints.current < healthPoints.max * 0.4f)
        {
            defaultTimer = new CounterTimerF(8.0f, 8.0f);
            roles = Roles.Dominate;
            skillStatus = SkillStatus.Restore;
        }

        switch(roles)
        {
            case Roles.Deceive:
                StateController();
                break;
            case Roles.Dominate:
                DominateController();
                break;
        }

        

        
        
    }

    private void StateController()
    {
        switch (skillStatus)
        {

            case SkillStatus.Default:

                    if (defaultTimer.isEnd(Time.deltaTime) && RelativeHeroPosition.magnitude < MaxDistance)
                    {
                        float r = Random.Range(0, 50.0f);
                        if (r < 25.0f)
                        {
                            currentTransform.GetComponent<Animation>().PlayQueued("OpenTurret", QueueMode.PlayNow);
                            skillStatus = SkillStatus.RangedAttack;
                            rotateTimer = new CounterTimerF(0, 2);
                            MachineGun.StartShoot();
                            machineGunTimer.current = machineGunTimer.max;
                            usedLine = Random.Range(0, 1) * 2 - 1;
                        }
                        else
                        {
                            currentTransform.GetComponent<Animation>().PlayQueued("OpenMines", QueueMode.PlayNow);
                            skillStatus = SkillStatus.ExplosiveAttack;
                            minerTimer.current = minerTimer.max;
                            mineDeploy.x = 0;      
                        }
                    }                
                break;

            case SkillStatus.RangedAttack:
                {
                    if (machineGunTimer.isEnd(Time.deltaTime) || CheckGround()<2)
                    {
                        currentTransform.GetComponent<Animation>().PlayQueued("CloseTurret", QueueMode.PlayNow);
                        skillStatus = SkillStatus.Default;
                        gunTurret.localEulerAngles = Vector3.zero;
                        MachineGun.StopShoot();
                    }
                    else
                    {
                        percent = spline.GetStep(currentTransform.position);
                        roadDirection = spline.GetAngle(percent);
                        gunTurret.LookAt(currentTransform.forward * -1);
                        Vector3 rotate = gunTurret.localEulerAngles;
                        
                        if (rotateTimer.isEnd(Time.deltaTime))
                            rightSide = !rightSide;

                        rotate.y += (rotateTimer.current - 1) * MachineGun.azimuth * ((rightSide == false) ? 1 : -1);
                        gunTurret.localEulerAngles = rotate;
                    }
                }

                break;

            case SkillStatus.ExplosiveAttack:
                {
                    if (mineDeploy.x < mineDeploy.y && forwardSpeed > 13.0f)
                    {
                        if (minerTimer.isEnd(Time.deltaTime))
                        {
                            StartMining();
                            mineDeploy.x++;
                        }
                    }
                    else
                    {
                        skillStatus = SkillStatus.Default;
                        currentTransform.GetComponent<Animation>().PlayQueued("CloseMines", QueueMode.PlayNow);
                    }
                }
                break;
        }
    }

    private void DominateController()
    {
        switch (skillStatus)
        {

            case SkillStatus.Restore:

                if (healthPoints.current < healthPoints.max) 
                healthPoints.current += 5.0f;
                if (defaultTimer.isEnd(Time.deltaTime))
                {
                    defaultTimer.current = defaultTimer.max;
                    skillStatus = SkillStatus.BattlePrepare;
                }
                break;

            case SkillStatus.BattlePrepare:
                {
                
                }

                break;

            case SkillStatus.ExplosiveAttack:
                {
                    
                }
                break;
        }
    }

    public void StartMining()
    {
        GameObject newMine = GameObject.Instantiate(mine, mineLauncher.position, Quaternion.identity) as GameObject;
        Mine AutoMine = newMine.GetComponent<Mine>();
        AutoMine.Initialize(this);
        AutoMine.currentRigidbody.velocity = currentRigidbody.velocity;
        AutoMine.currentRigidbody.angularVelocity = currentRigidbody.angularVelocity;
        AutoMine.currentRigidbody.AddForce(currentTransform.forward * -2000.0f, ForceMode.Impulse);

    }

    public override void CreateNewWaypoint(float wayLength, bool needNewSpline)
    {
        switch (roles)
        {
            case Roles.Deceive:
                {
                    switch (skillStatus)
                    {
                        case SkillStatus.Default:
                            offset = CalculateOffsetFromHero(0.85f);
                        break;

                        case SkillStatus.ExplosiveAttack:
                            offset = CalculateOffsetFromHero(1.25f);
                        break;

                        case SkillStatus.RangedAttack:
                        {
                            float width = spline.GetCurrentWidth(percent);
                            offset = width / 2 * usedLine;


                        }
                        break;
                    }
                }
                break;
            case Roles.Dominate:
                offset = 0;
            break;
        }
        

        base.CreateNewWaypoint(wayLength, needNewSpline);
    }

    public override void Boost()
    {
        currentRigidbody.velocity = currentTransform.TransformDirection(Vector3.forward * Math3d.GetSpeed(currentRpm, Main.instance.hero.wheelsColliderBack[0].radius));
        boost = false;
    }

    public override void DamageHit(float damage, Vector3 point)
    {


        switch (GetSideTriangle(point, currentTransform))
        {
            case SideTriangle.Top:
                damage *= SideFactors[0];
                break;
            case SideTriangle.Left:
                damage *= SideFactors[1];
                break;
            case SideTriangle.Right:
                damage *= SideFactors[2];
                break;
            case SideTriangle.Back:
                damage *= SideFactors[3];
                break;
        }

        if (armor < damage)
            damage -= armor;
        else
            return;



        float exp = 0;

        if (healthPoints.current - damage > 0)
        {
            healthPoints.current -= damage;
            exp = damage;
        }
        else
        {
            exp = healthPoints.current + expBounty;
            healthPoints.current = 0;

            DoCrush();
        }

        Main.instance.hero.AddExpierence(exp);
    }
    /*
    public override bool isTooFar(float _Dist, float _trafficDist)
    {

        float Dist = Vector3.Distance(currentTransform.position, Main.instance.mainCamera.currentTransform.position);
        if (Dist > _Dist)
            return true;

        return false;
    }
    */

    public override float Avoidance(bool isLong, float multi)
    {
        return base.Avoidance(false, 0.85f);

    }
    

    public override void AverageAuto(float clamp, out float motorTorque, out float brakeTorque)
    {
        motorTorque = 0;
        brakeTorque = 0;

        RelativeHeroPosition = currentTransform.InverseTransformPoint(Main.instance.hero.currentTransform.position);
        float difference = Main.instance.hero.forwardSpeedTact - forwardSpeed;
        float distance = -RelativeHeroPosition.z;

        if (distance < MinDistance || difference > SpeedDifference)
        {

            moving = FollowMoving.Boost;
        }
        else
            if (distance > AverageDistance)
            {
                moving = FollowMoving.Brake;
            }
            else
            {
                moving = FollowMoving.Move;

            }

            switch (moving)
            {
                case FollowMoving.Boost:

                    motorTorque = currentAccel * inputTorque;
                    brakeTorque = 0;
                    if (difference > 0)
                        motorTorque += currentAccel / 1.5f * Mathf.Clamp01(Mathf.Abs(difference));
                    if (difference < 0)
                        brakeTorque += currentBrake / 1.5f * Mathf.Clamp01(Mathf.Abs(difference));

                    break;
                case FollowMoving.Move:

                    

                    motorTorque = 0;
                    brakeTorque = 0;


                    if (difference > 0)
                        motorTorque += currentAccel / 1.5f * Mathf.Clamp01(Mathf.Abs(difference));
                    if (difference < 0)
                        brakeTorque += currentBrake / 1.5f * Mathf.Clamp01(Mathf.Abs(difference));

                    break;
                case FollowMoving.Brake:

  
                    motorTorque = 0;
                    brakeTorque = currentBrake;// +Mathf.Lerp(0, BrakeBonus, Mathf.Clamp(distance, AverageDistance, MaxDistance));
                    if (difference < 0)
                        brakeTorque += currentBrake / 1.5f * Mathf.Clamp01(Mathf.Abs(difference));
                    break;
            }

        /*
            if (difference > 0)
                motorTorque += currentAccel / 1.5f * Mathf.Clamp01(Mathf.Abs(difference));
            if (difference < 0)
                brakeTorque += currentBrake / 1.5f * Mathf.Clamp01(Mathf.Abs(difference));
        */
            //DropText(skillStatus.ToString() + " " + roles.ToString() + " " + offset.ToString());

    }



    

    
}
