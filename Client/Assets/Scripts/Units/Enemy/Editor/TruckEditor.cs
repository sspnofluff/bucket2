﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Truck))]
public class TruckEditor : Editor
{
   
    public override void OnInspectorGUI()
    {

        Truck myTarget = (Truck)target;

        if (GUILayout.Button("Create Spawn Dot"))
        {
            myTarget.CreateSpawnDot();
        }

    }
}
