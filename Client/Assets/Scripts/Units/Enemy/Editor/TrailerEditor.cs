﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Trailer))]
public class TrailerEditor : Editor
{
   
    public override void OnInspectorGUI()
    {

        Trailer myTarget = (Trailer)target;
        
        if (GUILayout.Button("Create Spawn Dot"))
        {
            myTarget.CreateSpawnDot();
        }

    }
}
