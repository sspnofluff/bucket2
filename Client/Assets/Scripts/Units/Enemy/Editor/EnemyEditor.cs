﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(EnemyVehicle))]
public class EnemyEditor : Editor
{
 
    public override void OnInspectorGUI()
    {
        

        EnemyVehicle myTarget = (EnemyVehicle)target;
        if (GUILayout.Button("Create Spawn Dot"))
        {
            myTarget.CreateSpawnDot();
        }

        

                
    }
}
