﻿using UnityEngine;
using System.Collections;

public class CoinUp : MonoBehaviour {

    public CounterTimerF timer;
    public Object effect;
    public int coins;
    public BonusType bonusType;
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (timer.isEnd(Time.deltaTime))
        {
            Main.instance = FindObjectOfType<Main>();
            GameObject effectActive = (GameObject)GameObject.Instantiate(effect, transform.position, transform.rotation);
            effectActive.transform.parent = transform.parent;
            Destroy(gameObject);
            switch (bonusType)
            {
                case BonusType.Coin:
                    Main.instance.GetExp(coins, transform.position);
                    break;
                case BonusType.HealthUp:
                    Main.instance.GetHeal(transform.position);
                    break;
            }
        }
        else
        {
            Vector3 pos = transform.position;
            pos.y += Time.deltaTime * 4;
            transform.position = pos;
        }

        
	}
}
