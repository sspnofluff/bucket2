﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using Dest.Math;



[CustomEditor (typeof(BlockPlane))]
public class BlockPlaneEditor : Editor
{
	protected GUISkin skin;

	BlockPlane myTarget;
	public bool isDelete = false;

	private bool customBorderCollapsed = true;


	public override void OnInspectorGUI ()
	{
		
		ShowBlockPlaneHeader ();
		ShowBlockPlaneSettings ();
	}

	public void ShowBlockPlaneHeader()
	{
		myTarget = (BlockPlane)target; 

		if (Main.instance == null) 
		{
			Main _Main = FindObjectOfType<Main> ();
			_Main.EditorAwake ();
		}
		Color guiColor = GUI.color;
		if (skin == null)
			skin = AssetDatabase.LoadAssetAtPath ("Assets/Resources/MapEditorGuiSkin.guiskin", typeof(GUISkin)) as GUISkin;
		//GUILayout.Label ((myTarget.nearPolygons!=null) ? myTarget.nearPolygons.Count.ToString() : "null");
		//for (int i = 0; i < myTarget.nearPolygons.Count; i++)
		//	GUILayout.Label (i.ToString() + " " + myTarget.nearPolygons [i].triangleIndex.x.ToString() + " " + myTarget.nearPolygons [i].triangleIndex.y.ToString() + " " + myTarget.nearPolygons [i].triangleIndex.z.ToString());
		//GUILayout.Label ((myTarget.data.triangles!=null) ? myTarget.data.triangles.Count.ToString() : "null");
		GUILayout.BeginHorizontal ();
		GUI.color = (myTarget.isMainSettings) ? Color.gray : Color.white;
		if (GUILayout.Button (Main.instance.generator.blockPlaneIcons[0], GUILayout.Width (64), GUILayout.Height (64)))
			myTarget.isMainSettings = !myTarget.isMainSettings;
		GUI.color = (myTarget.isColliderSettings) ? Color.gray : Color.white;
		if (GUILayout.Button (Main.instance.generator.blockPlaneIcons[1], GUILayout.Width (64), GUILayout.Height (64)))
			myTarget.isColliderSettings = !myTarget.isColliderSettings;
		GUI.color = (myTarget.isAdditionalElements) ? Color.gray : Color.white;
		if (GUILayout.Button (Main.instance.generator.blockPlaneIcons[3], GUILayout.Width (64), GUILayout.Height (64)))
			myTarget.isAdditionalElements = !myTarget.isAdditionalElements;
		GUI.color = (myTarget.isSplineSettigns) ? Color.gray : Color.white;
		if (GUILayout.Button (Main.instance.generator.blockPlaneIcons[2], GUILayout.Width (64), GUILayout.Height (64)))
			myTarget.isSplineSettigns = !myTarget.isSplineSettigns;
		GUI.color = (myTarget.isBorderSettigns) ? Color.gray : Color.white;
		if (GUILayout.Button (Main.instance.generator.blockPlaneIcons[4], GUILayout.Width (64), GUILayout.Height (64)))
			myTarget.isBorderSettigns = !myTarget.isBorderSettigns;
		
		
		GUI.color = guiColor;
		GUILayout.FlexibleSpace ();
		GUILayout.EndHorizontal ();
		GUILayout.Space (3);
	}

	void RestoreEditorCollider()
	{
		myTarget = (BlockPlane)target;

		myTarget.editedCollider = new Mesh ();
		myTarget.editedCollider.vertices = myTarget.sourceCollider.vertices;
		myTarget.editedCollider.triangles = myTarget.sourceCollider.triangles;
		myTarget.editedCollider.uv = myTarget.sourceCollider.uv;
		myTarget.editedCollider.uv2 = myTarget.sourceCollider.uv2;
		myTarget.editedCollider.name = "EditedCollider";
		myTarget.editedCollider.RecalculateNormals ();

		myTarget.editMeshCollider.sharedMesh = myTarget.editedCollider;
		myTarget.colliderStep = BlockPlane.ColliderEditStep.None;
		myTarget.PrepareToCorrection ();

		myTarget.verticesCount = myTarget.editedCollider.vertexCount;
		int[] indices = myTarget.editedCollider.triangles;
		myTarget.polygonsCount = indices.Length/3;
	}

	void UndoCorrection()
	{
		myTarget = (BlockPlane)target;

		myTarget.editMeshCollider.sharedMesh = myTarget.preparedCollider;
		myTarget.editedCollider = myTarget.editMeshCollider.sharedMesh;
	}

	public void ShowBlockPlaneSettings()
	{
		myTarget = (BlockPlane)target;


		if (myTarget.isMainSettings) {
			GUILayout.Label ("Settings", EditorStyles.boldLabel);
			GUILayout.Space (1);
			GUILayout.BeginVertical (GUI.skin.GetStyle ("Box"));
			GUILayout.Space (3);
			myTarget.nominal = EditorGUILayout.TextField ("Nominal", myTarget.nominal.ToLower());
			myTarget.ignoreCollider = EditorGUILayout.Toggle ("Ignore Collider", myTarget.ignoreCollider);
			myTarget.nonKill = EditorGUILayout.Toggle ("Non Kill", myTarget.nonKill);
			myTarget.canCreateNextPlane = EditorGUILayout.Toggle ("Can Create Next Plane", myTarget.canCreateNextPlane);
			myTarget.point = EditorGUILayout.FloatField ("Point", myTarget.point);
			GUILayout.Space (3);
			if (GUILayout.Button ("Import Bezier", GUILayout.Height(20))) {
				string data = EditorUtility.OpenFilePanel ("Bezier data", "Assets/Prefabs/BlockSources/", "txt");
				myTarget.ImportBezier (data);
			}
			GUILayout.EndVertical ();
			GUILayout.Space (3);
		}

		if (myTarget.isColliderSettings) 
		{
			GUILayout.Label ("Collider", EditorStyles.boldLabel);
			GUILayout.Space (1);

			if (!myTarget.isColliderEdited) 
			{
				
				GUI.color = Color.green;
				if (GUILayout.Button ("Start Edit Collider", GUILayout.Height (20)))
				{
					
					myTarget.isColliderEdited = true;
				}
				GUI.color = Color.white;
			} 
			else
			{
				GUILayout.BeginVertical (GUI.skin.GetStyle ("Box"));
				GUILayout.Space (3);
				if (myTarget.colliderStep == BlockPlane.ColliderEditStep.None) 
				{
					GUI.color = Color.red;
					if (GUILayout.Button ("Stop Edit Collider", GUILayout.Height (20))) {
						
						myTarget.isColliderEdited = false;
					}
				} else {
					GUI.color = Color.white;

					GUILayout.Label ("Brush Settings", EditorStyles.boldLabel);
					myTarget.isOneShot = GUILayout.Toggle (myTarget.isOneShot, "One Shot");
					myTarget.isMassive = GUILayout.Toggle (myTarget.isMassive, "MultipleSelect");

					myTarget.brushSize = EditorGUILayout.Slider (myTarget.brushSize, myTarget.brushSizeMinMax.x, myTarget.brushSizeMinMax.y);
					myTarget.brushOpacity = EditorGUILayout.Slider (myTarget.brushOpacity, 0.1f, 1.0f);

					GUILayout.Label ("Collider Settings", EditorStyles.boldLabel);
					GUI.color = Color.white;
					GUILayout.Space (3);
					if (myTarget.isTesselation) {
						GUI.color = Color.yellow;
						if (GUILayout.Button ("Tesselation: On", GUILayout.Height (20))) {
							Material mat = myTarget.transform.FindChild ("mesh").GetComponent<MeshRenderer> ().sharedMaterial;
							myTarget.tessPower = mat.GetFloat ("_TessYOffset");
							mat.SetFloat ("_TessYOffset", 0);
							myTarget.isTesselation = false;
						}
						GUI.color = Color.white;
					} else {
						GUI.color = Color.red;
						if (GUILayout.Button ("Tesselation: Off", GUILayout.Height (20))) {
							Material mat = myTarget.transform.FindChild ("mesh").GetComponent<MeshRenderer> ().sharedMaterial;
							mat.SetFloat ("_TessYOffset", myTarget.tessPower);
							myTarget.isTesselation = true;
						}
						GUI.color = Color.white;
					}

					if (!myTarget.isColliderView) {
						GUI.color = Color.yellow;
						if (GUILayout.Button ("ColliderView: Off", GUILayout.Height (20))) {
							myTarget.isColliderView = true;
							myTarget.transform.FindChild ("mesh").GetComponent<MeshRenderer> ().enabled = false;
						}
						GUI.color = Color.white;
					} else {
						GUI.color = Color.red;
						if (GUILayout.Button ("ColliderView: On", GUILayout.Height (20))) {
							myTarget.isColliderView = false;
							myTarget.transform.FindChild ("mesh").GetComponent<MeshRenderer> ().enabled = true;
						}
						GUI.color = Color.white;

					}
					GUILayout.BeginHorizontal ();
					GUILayout.Label (" Vertex count: ");
					GUILayout.Label (myTarget.verticesCount.ToString (), EditorStyles.boldLabel);
					GUILayout.EndHorizontal ();
					GUILayout.BeginHorizontal ();
					GUILayout.Label (" Polygons count: ");
					GUILayout.Label (myTarget.polygonsCount.ToString (), EditorStyles.boldLabel);
					GUILayout.EndHorizontal ();
				}


				GUILayout.Space(5);
				switch (myTarget.colliderStep) 
				{
				case BlockPlane.ColliderEditStep.None:
					GUI.color = Color.red;
					if (GUILayout.Button ("Create new Collider", GUILayout.Height (20))) 
					{
						if (myTarget.sourceCollider == null) {
							Mesh terrain = myTarget.transform.FindChild ("mesh").GetComponent<MeshFilter> ().sharedMesh;
							myTarget.sourceCollider = terrain;
						}

						myTarget.editMeshCollider = myTarget.transform.FindChild ("mesh").gameObject.AddComponent<MeshCollider> ();
						myTarget.editMeshCollider.sharedMesh = myTarget.editedCollider;

						RestoreEditorCollider ();

						for (int i = 0; i < myTarget.transform.childCount; i++) {
							if (myTarget.transform.GetChild (i).name != "mesh")
								myTarget.transform.GetChild (i).gameObject.SetActive (false);
						}

						myTarget.PrepareToCorrection ();
					}
					break;
				case BlockPlane.ColliderEditStep.Prepare:
					myTarget.tabSwitch = GUILayout.Toolbar (myTarget.tabSwitch, new string[] {"None", "Delete"});
					Optimize ();
					GUI.color = Color.green;
					//GUILayout.Label ("Layer level: Preparing", EditorStyles.boldLabel);
					GUILayout.Space(8);
					if (GUILayout.Button ("Prepare Subdivide", GUILayout.Height (20)))
						PrepareSubdivide ();
					break;
				case BlockPlane.ColliderEditStep.Tier1:
					myTarget.tabSwitch = GUILayout.Toolbar (myTarget.tabSwitch, new string[] {"None", "Subdivide", "Delete"});
					Optimize ();
					GUI.color = Color.green;
					//GUILayout.Label ("Layer level: 0", EditorStyles.boldLabel);
					GUILayout.Space(8);
					if (GUILayout.Button ("Apply Layer 0", GUILayout.Height (20)))
						SideForAll ();
					break;
				case BlockPlane.ColliderEditStep.Tier2:
					
					myTarget.tabSwitch = GUILayout.Toolbar (myTarget.tabSwitch, new string[] { "None", "Subdivide", "Delete" });
					Optimize ();

					GUI.color = Color.green;
					GUILayout.Space(8);
					if (GUILayout.Button ("Apply Layer 1", GUILayout.Height (20)))
						SideForAll02 ();
					break;
				case BlockPlane.ColliderEditStep.Correction:
					myTarget.tabSwitch = GUILayout.Toolbar (myTarget.tabSwitch, new string[] {"None", "Delete"});
					Optimize ();
					GUI.color = Color.green;
					if (GUILayout.Button ("Correction", GUILayout.Height (20))) {
						myTarget.ColliderCorrection ();
					}
					break;
				case BlockPlane.ColliderEditStep.Final:
					GUI.color = Color.red;
					GUILayout.Space(8);
					if (GUILayout.Button ("Save Collider", GUILayout.Height (20)))
					{
						
						myTarget.editedCollider = myTarget.editMeshCollider.sharedMesh;
						DestroyImmediate (myTarget.editMeshCollider);
						string meshMainPath = "Assets/Art/roads/" + myTarget.landType.ToString () + "/" + myTarget.blockName.ToLower () + "/" + myTarget.nominal + "_ColliderMesh.asset";
						PrefabCollector.CreateOrReplaceAssetMesh (myTarget.editedCollider, meshMainPath);
						myTarget.transform.FindChild ("collider").GetComponent<MeshCollider> ().sharedMesh = AssetDatabase.LoadAssetAtPath<Mesh> (meshMainPath);
						for (int i = 0; i < myTarget.transform.childCount; i++) {
							myTarget.transform.GetChild (i).gameObject.SetActive (true);
						}

						AssetDatabase.Refresh ();
						EditorUtility.SetDirty (myTarget);
						AssetDatabase.SaveAssets ();
						myTarget.isColliderEdited = false;
						myTarget.colliderStep = BlockPlane.ColliderEditStep.None;
					}
					break;
				}
				GUI.color = Color.white;




				//myTarget.dist = EditorGUILayout.FloatField ("x_offset", myTarget.dist);
				//myTarget.e1 = EditorGUILayout.FloatField ("y_offset", myTarget.e1);




				if (myTarget.colliderStep != BlockPlane.ColliderEditStep.None) {
					GUI.color = Color.red;
					GUILayout.Space (1);
					if (GUILayout.Button ("Revert Collider", GUILayout.Height (20)))
						RestoreEditorCollider ();
				}

				GUILayout.Space (1);
				GUILayout.EndVertical ();
				GUILayout.Space (3);
					


				//if (GUILayout.Button ("Draw Inside Markers", GUILayout.Height (20)))
				//	DrawInsideMarkers ();
				
			}


				/*
			myTarget.sourceCollider = (Mesh)EditorGUILayout.ObjectField (myTarget.sourceCollider, typeof(Mesh));

			
			GUILayout.Space (3);

			if (GUILayout.Button ("Collider Correction", GUILayout.Height(20)))
				myTarget.ColliderCorrection ();

			if (GUILayout.Button ("Revert Collider", GUILayout.Height(20))) {
				MeshCollider MC = myTarget.roadCollider.GetComponent<MeshCollider> ();
				MC.sharedMesh = myTarget.sourceCollider;
			}
			*/

		}

		if (myTarget.isAdditionalElements) {
			
			GUILayout.Label ("Grass Settings", EditorStyles.boldLabel);
			GUILayout.Space (1);
			//GUILayout.BeginVertical (GUI.skin.GetStyle ("Box"));
			//GUILayout.Space (3);
			GUI.color = Color.green;
			if (GUILayout.Button ("Activate GrassSystem", GUILayout.Height(20)))
				myTarget.CreateGrass ();

			GUI.color = Color.white;
			//GUILayout.EndVertical ();
			//GUILayout.Space (3);
		}

		if (myTarget.isSplineSettigns) 
		{
			GUILayout.Label ("Spline Settings", EditorStyles.boldLabel);
			GUILayout.Space (1);


			string buttonSpline = "";
			if (myTarget.isSplineOpen == false) 
			{
				GUI.color = Color.green;
				buttonSpline = "Open Spline Camera Settings";
			} 
			else 
			{
				GUILayout.BeginVertical (GUI.skin.GetStyle ("Box"));
				GUILayout.Space (3);

				GUI.color = Color.red;
				buttonSpline = "Close Spline Camera Settings";
			}


			if (GUILayout.Button(buttonSpline, GUILayout.Height (20))) 
			{
				myTarget.isSplineOpen = !myTarget.isSplineOpen;
				if (myTarget.isSplineOpen) 
				{

					Main.instance.mainCamera.transform.parent = null;
					Main.instance.mainCamera.transform.position = Vector3.zero;
					Main.instance.mainCamera.transform.rotation = Quaternion.identity;
					Main.instance.mainCamera.playerAffectCamera.localPosition = Main.instance.mainCamera.playerAffectionDistMax;
					Main.instance.mainCamera.playerAffectCamera.localRotation = Quaternion.Euler(45.0f,0.0f,0);
					Main.instance.mainCamera.roadAffectCamera.transform.localPosition = Vector3.zero;
					Main.instance.mainCamera.roadAffectCamera.transform.localRotation = Quaternion.identity;
					Main.instance.mainCamera.roadAffectCamera.GetComponent<UnityStandardAssets.CinematicEffects.TemporalAntiAliasing> ().enabled = false;
					Main.instance.mainCamera.roadAffectCamera.GetComponent<UnityStandardAssets.ImageEffects.DepthOfField> ().enabled = false;
					Main.instance.curveController.enabled = true;
				}
				else
				{

					Main.instance.mainCamera.transform.parent = Main.instance.hero.currentTransform;
					Main.instance.mainCamera.transform.localPosition = Vector3.zero;
					Main.instance.mainCamera.transform.localRotation = Quaternion.Euler(0,30.0f,0);
					Main.instance.mainCamera.playerAffectCamera.localPosition = new Vector3(-2.0f, 1.0f, 0.25f);
					Main.instance.mainCamera.playerAffectCamera.localRotation = Quaternion.identity;
					Main.instance.mainCamera.roadAffectCamera.transform.localPosition = Vector3.zero;
					Main.instance.mainCamera.roadAffectCamera.transform.localRotation = Quaternion.Euler(25.0f,90.0f,0);
					Main.instance.mainCamera.transform.parent = null;
					Main.instance.mainCamera.roadAffectCamera.GetComponent<UnityStandardAssets.CinematicEffects.TemporalAntiAliasing> ().enabled = true;
					Main.instance.mainCamera.roadAffectCamera.GetComponent<UnityStandardAssets.ImageEffects.DepthOfField> ().enabled = true;
					Main.instance.curveController.enabled = false;
				}
			}

			GUI.color = Color.white;

			if (myTarget.isSplineOpen) 
			{
				myTarget.previousPlane = (BlockPlane)EditorGUILayout.ObjectField("Previous", myTarget.previousPlane, typeof(BlockPlane));
				myTarget.nextPlane = (BlockPlane)EditorGUILayout.ObjectField("Next", myTarget.nextPlane, typeof(BlockPlane));
				myTarget.playerAffectSlider = GUILayout.HorizontalSlider (myTarget.playerAffectSlider, 0, 1.0f);
				bool isChangePosCam = false;
				myTarget.posCam = GUILayout.HorizontalSlider (myTarget.posCam, 0, 1.0f);
				if (myTarget.oldPosCam != myTarget.posCam) 
				{
					isChangePosCam = true;
					myTarget.oldPosCam = myTarget.posCam;
				}
				Vector3 globalPos = myTarget.spline.GlobalPos (myTarget.posCam);
				globalPos.y += Main.instance.mainCamera.altitude;
				Main.instance.mainCamera.currentTransform.position = globalPos;

				Main.instance.mainCamera.currentTransform.rotation = myTarget.currentTransform.rotation * Quaternion.Euler(myTarget.spline.getRot (myTarget.posCam));


				Main.instance.mainCamera.playerAffectCamera.localPosition = Vector3.Lerp (Main.instance.mainCamera.playerAffectionDistMin, Main.instance.mainCamera.playerAffectionDistMax, myTarget.playerAffectSlider);
				Main.instance.mainCamera.playerAffectCamera.localRotation = Quaternion.Euler (new Vector3(Mathf.LerpAngle (Main.instance.mainCamera.brakeRotationBattle, Main.instance.mainCamera.boostRotationBattle, myTarget.playerAffectSlider), 0, 0));

				Rect rect = EditorGUILayout.GetControlRect (GUILayout.Height (20.0f));
				CameraDot[] _camDot = myTarget.spline.GetCameraDots ();

				int count = _camDot.Length;
				for (int i = 0; i < count ; i++) 
				{
					if (i == 0) 
					{

						Rect _rect = new Rect (rect.x + _camDot [i].percent * rect.width - 10.0f, rect.y, 20.0f, rect.height);
						if (myTarget.splineSelect == i)
							GUI.color = Color.gray;
						else
							GUI.color = Color.white;
						if (GUI.Button (_rect, i.ToString()))
							myTarget.splineSelect = i;
					}
					else
					{
					Rect _rect = new Rect (rect.x + _camDot [i].percent * rect.width - 10.0f, rect.y,  20.0f, rect.height);
						if (myTarget.splineSelect == i)
						GUI.color = Color.gray;
						else
						GUI.color = Color.white;
					if (GUI.Button (_rect, i.ToString()))
							myTarget.splineSelect = i;
					}
				}
				GUI.color = Color.white;

				myTarget.camDot01 = myTarget.spline.GetPrevious (myTarget.posCam);
				myTarget.camDot02 = myTarget.spline.GetNext(myTarget.posCam);

				float percent = -1;
				if (myTarget.camDot01 == null && myTarget.previousPlane != null) 
				{
					myTarget.camDot01 = myTarget.previousPlane.spline.GetPrevious (1.0f);
					//if (myTarget.camDot01.percent>0)
					//myTarget.camDot01.percent = 1.0f - myTarget.camDot01.percent;
				}

				if (myTarget.camDot02 == null && myTarget.nextPlane != null) 
				{
					myTarget.camDot02 = myTarget.nextPlane.spline.GetNext (0.0f);
				}

				if (myTarget.camDot01 == null) {
					Debug.Log ("0 " + myTarget.posCam);
					percent = myTarget.posCam / myTarget.camDot02.percent;
					if (isChangePosCam) 
					{
						myTarget.currentAxisBendSize = new Vector2 (Mathf.Lerp (-0.7f, myTarget.camDot02.AxisBendSize.x, percent), Mathf.Lerp (-0.7f, myTarget.camDot02.AxisBendSize.y, percent));
						myTarget.currentBias = new Vector2 (Mathf.Lerp (1.0f, myTarget.camDot02.Bias.x, percent), Mathf.Lerp (1.0f, myTarget.camDot02.Bias.y, percent));
						myTarget.currentLocalPosition =  Vector3.Lerp (Vector3.zero, myTarget.camDot02.localPosition, percent);
						myTarget.currentLocalRotate = Quaternion.Lerp (Quaternion.identity, Quaternion.Euler (myTarget.camDot02.localRotate), percent).eulerAngles;
					}
					} else {
					if (myTarget.camDot02 != null) {
						Debug.Log ("1 " + myTarget.posCam + " " + myTarget.camDot01.percent + " " + myTarget.camDot02.percent);
						if (myTarget.camDot01.percent > myTarget.camDot02.percent && myTarget.posCam < 0.2f)
							percent = (myTarget.posCam - myTarget.camDot02.percent) / Mathf.Abs (myTarget.camDot02.percent - myTarget.camDot01.percent) + 1.0f;
						else
						if (myTarget.camDot01.percent > myTarget.camDot02.percent && myTarget.posCam > 0.8f)
							percent = (myTarget.posCam - myTarget.camDot02.percent) / Mathf.Abs (myTarget.camDot02.percent - myTarget.camDot01.percent) - 1.0f;
						else
							percent = (myTarget.posCam - myTarget.camDot01.percent) / (myTarget.camDot02.percent - myTarget.camDot01.percent);
						if (isChangePosCam) 
						{
							myTarget.currentAxisBendSize = new Vector2 (Mathf.Lerp (myTarget.camDot01.AxisBendSize.x, myTarget.camDot02.AxisBendSize.x, percent), Mathf.Lerp (myTarget.camDot01.AxisBendSize.y, myTarget.camDot02.AxisBendSize.y, percent));
							myTarget.currentBias = new Vector2 (Mathf.Lerp (myTarget.camDot01.Bias.x, myTarget.camDot02.Bias.x, percent), Mathf.Lerp (myTarget.camDot01.Bias.y, myTarget.camDot02.Bias.y, percent));
							myTarget.currentLocalPosition =  Vector3.Lerp (myTarget.camDot01.localPosition, myTarget.camDot02.localPosition, percent);
							myTarget.currentLocalRotate = Quaternion.Lerp (Quaternion.Euler (myTarget.camDot01.localRotate), Quaternion.Euler (myTarget.camDot02.localRotate), percent).eulerAngles;
						}
						} else {
						Debug.Log ("2 " + myTarget.posCam);
						percent = (myTarget.posCam - myTarget.camDot01.percent) / (1.0f - myTarget.camDot01.percent);
						if (isChangePosCam) 
						{
							myTarget.currentAxisBendSize = new Vector2 (Mathf.Lerp (myTarget.camDot01.AxisBendSize.x, -0.7f, percent), Mathf.Lerp (myTarget.camDot01.AxisBendSize.y, -0.7f, percent));
							myTarget.currentBias = new Vector2 (Mathf.Lerp (myTarget.camDot01.Bias.x, 1.0f, percent), Mathf.Lerp (myTarget.camDot01.Bias.y, 1.0f, percent));
							myTarget.currentLocalPosition = Vector3.Lerp (myTarget.camDot01.localPosition, Vector3.zero, percent);
							myTarget.currentLocalRotate = Quaternion.Lerp (Quaternion.Euler (myTarget.camDot01.localRotate), Quaternion.identity, percent).eulerAngles;
						}
						}
					}

				GUILayout.Space (3);
				GUILayout.Label ("Current " + myTarget.splineSelect.ToString (), EditorStyles.boldLabel);
				GUILayout.Label ("Percent " + percent.ToString ());

				myTarget.currentLocalPosition = EditorGUILayout.Vector3Field ("Position ", myTarget.currentLocalPosition);
				myTarget.currentLocalRotate = EditorGUILayout.Vector3Field ("Rotation ", myTarget.currentLocalRotate);
				myTarget.currentAxisBendSize = EditorGUILayout.Vector2Field ("Bend Size ", myTarget.currentAxisBendSize);
				myTarget.currentBias = EditorGUILayout.Vector2Field ("Bias ", myTarget.currentBias);

				Main.instance.mainCamera.roadAffectCamera.transform.localPosition = myTarget.currentLocalPosition;
				Main.instance.mainCamera.roadAffectCamera.transform.localRotation = Quaternion.Euler (myTarget.currentLocalRotate);
				Main.instance.curveController._V_CW_Bend_X = myTarget.currentAxisBendSize.x;
				Main.instance.curveController._V_CW_Bend_Z = myTarget.currentAxisBendSize.y;
				Main.instance.curveController._V_CW_Bias_X = myTarget.currentBias.x;
				Main.instance.curveController._V_CW_Bias_Z = myTarget.currentBias.y;
				Main.instance.curveController.ForceUpdate ();
				if (GUILayout.Button ("Apply"))
				{
					float thisPercent = percent - myTarget.camDot01.percent;
					myTarget.camDot01.localPosition = (myTarget.currentLocalPosition - percent * myTarget.camDot02.localPosition) / (1 - percent);
					float Ox = myTarget.currentLocalRotate.x;
					if (Ox > 180)
						Ox -= 360;
					float Oy = myTarget.currentLocalRotate.y;
					if (Oy > 180)
						Oy -= 360;
					float Oz = myTarget.currentLocalRotate.z;
					if (Oz > 180)
						Oz -= 360;
					float rX = ((Ox - percent * myTarget.camDot02.localRotate.x) / (1 - percent))%360;
					float rY = ((Oy - percent * myTarget.camDot02.localRotate.y) / (1 - percent))%360;
					float rZ = ((Oz - percent * myTarget.camDot02.localRotate.z) / (1 - percent))%360;
					myTarget.camDot01.localRotate = new Vector3 (rX, rY, rZ);
					myTarget.camDot01.AxisBendSize = (myTarget.currentAxisBendSize - percent * myTarget.camDot02.AxisBendSize) / (1 - percent);
					myTarget.camDot01.Bias = (myTarget.currentBias - percent * myTarget.camDot02.Bias) / (1 - percent);

				}
				GUILayout.Space (3);
				GUI.color = Color.white;
				if (myTarget.splineSelect > 0) {

					GUILayout.Label ("Camera Dot " + myTarget.splineSelect.ToString (), EditorStyles.boldLabel);
					_camDot [myTarget.splineSelect].percent = EditorGUILayout.FloatField ("Percent ", _camDot [myTarget.splineSelect].percent);
					_camDot [myTarget.splineSelect].localPosition = EditorGUILayout.Vector3Field ("Position ", _camDot [myTarget.splineSelect].localPosition);
					_camDot [myTarget.splineSelect].localRotate = EditorGUILayout.Vector3Field ("Rotation ", _camDot [myTarget.splineSelect].localRotate);
					_camDot [myTarget.splineSelect].AxisBendSize = EditorGUILayout.Vector2Field ("Bend Size ", _camDot [myTarget.splineSelect].AxisBendSize);
					_camDot [myTarget.splineSelect].Bias = EditorGUILayout.Vector2Field ("Bias ", _camDot [myTarget.splineSelect].Bias);
				}
				//Rect rect2 = new Rect (Mathf.Clamp(rect.x-0.5f,0,1.0f) + _camDot [count - 1].percent * rect.width , rect.y, rect.width * (1.0f - _camDot [count - 1].percent)+0.5f, rect.height);
				//GUI.Button (rect2, count.ToString());
				/*
				float width = EditorGUIUtility.currentViewWidth;
				GUILayout.BeginHorizontal ();
				GUILayout.Space (width * 0.1f);
				GUILayout.Label ("1");
				GUILayout.Space (width * 0.7f);
				GUILayout.Label ("2");
				GUILayout.Space (width * 0.2f);
				GUILayout.EndHorizontal ();
				*/


				GUILayout.EndVertical ();
				GUILayout.Space (3);
			}





		}

		if (myTarget.isBorderSettigns) 
		{
			GUILayout.Label ("Border Settings", EditorStyles.boldLabel);
			GUILayout.Space (1);

			if (myTarget.isBoardsOpen == false) {
				GUI.color = Color.green;
				if (GUILayout.Button ("Open Borders Settings", GUILayout.Height (20))) {
					myTarget.isBoardsOpen = true;
				}
				GUI.color = Color.white;
			} else {

				GUILayout.BeginVertical (GUI.skin.GetStyle ("Box"));
				GUILayout.Space (3);

				GUI.color = Color.red;
				if (GUILayout.Button ("Close Borders Settings", GUILayout.Height (20))) {
					myTarget.isBoardsOpen = false;
				}
				GUI.color = Color.white;


				myTarget.mainBorder = (BakedProps)EditorGUILayout.ObjectField ("Main Border Unit", myTarget.mainBorder, typeof(BakedProps));

				if (myTarget.customBorders != null) {
					if (myTarget.customBorders.Count > 0)
						for (int i = 0; i < myTarget.customBorders.Count; i++)
							myTarget.customBorders [i] = (BakedProps)EditorGUILayout.ObjectField ("Custom Unit " + i, myTarget.customBorders [i], typeof(BakedProps));
				
				}

				if (GUILayout.Button ("Add Border Option", GUILayout.Height (20))) {
					myTarget.customBorders.Add (null);
				}

				myTarget.width = EditorGUILayout.FloatField ("Width", myTarget.width);
				if (myTarget.type == BlockPlaneType.Standart) {
					if (GUILayout.Button ("Create Border", GUILayout.Height (20)))
						myTarget.CreateBorders ();
				} else {
					GUILayout.BeginHorizontal ();
					if (myTarget.type == BlockPlaneType.Fork) {
						if (GUILayout.Button ("Create Border Left", GUILayout.Height (20)))
							myTarget.CreateBordersOnSpline (myTarget.GetComponent<Fork> ().GetSpline (SplineType.Left));
						if (GUILayout.Button ("Create Border Right", GUILayout.Height (20)))
							myTarget.CreateBordersOnSpline (myTarget.GetComponent<Fork> ().GetSpline (SplineType.Right));
						GUILayout.EndHorizontal ();
					}
					else
						if (myTarget.type == BlockPlaneType.Connection) {
							if (GUILayout.Button ("Create Border Left", GUILayout.Height (20)))
								myTarget.CreateBordersOnSpline (myTarget.GetComponent<ForkReverse> ().GetSpline (SplineType.Left));
							if (GUILayout.Button ("Create Border Right", GUILayout.Height (20)))
								myTarget.CreateBordersOnSpline (myTarget.GetComponent<ForkReverse> ().GetSpline (SplineType.Right));
							GUILayout.EndHorizontal ();
						}
					if (GUILayout.Button ("Clean", GUILayout.Height (20)))
						myTarget.CleanBorders ();
				}

				if (GUILayout.Button ("Damage Sides", GUILayout.Height (20))) {
					myTarget.bordersHub.ChangeSides ();
				}

				if (GUILayout.Button ("Bake", GUILayout.Height (20))) {
					myTarget.ReCalculateBorderData ();
					myTarget.BakeBorders ();
				}

				GUILayout.EndVertical ();
			}
		}

	}
	

	void Optimize()
	{
		if ((myTarget.tabSwitch == 1 && (myTarget.colliderStep == BlockPlane.ColliderEditStep.Prepare || myTarget.colliderStep == BlockPlane.ColliderEditStep.Correction)) ||
			(myTarget.tabSwitch == 2 && (myTarget.colliderStep == BlockPlane.ColliderEditStep.Tier1 || myTarget.colliderStep == BlockPlane.ColliderEditStep.Tier2)))
		if (GUILayout.Button ("Optimize Mesh", GUILayout.Height (20))) 
		{
			AutoWeld (myTarget.editedCollider, 0.01f);
			KillUselessVertex (myTarget.editedCollider);
			myTarget.editMeshCollider.sharedMesh = myTarget.editedCollider;
		}
			
	}
	Vector3 CalculateTriangles(Ray ray)
	{
		RaycastHit hit;
		myTarget.isCanRayCast = myTarget.editMeshCollider.Raycast(ray, out hit, Mathf.Infinity);
		List<Vector3i> _triangles = new List<Vector3i> ();


			Transform meshTransform = myTarget.transform.FindChild ("mesh").transform;
			int length = myTarget.editedCollider.triangles.Length;

			Vector3[] vertices = myTarget.editedCollider.vertices;
			int[] indexes = myTarget.editedCollider.triangles;

			 //DELETE
			for (int i = 0; i < length; i += 3) 
			{
				Vector3 one = meshTransform.TransformPoint (vertices [indexes [i]]);
				Vector3 two = meshTransform.TransformPoint (vertices [indexes [i + 1]]);
				Vector3 three = meshTransform.TransformPoint (vertices [indexes [i + 2]]);
				float x = (one.x + two.x + three.x) / 3;
				float y = (one.z + two.z + three.z) / 3;
				
				if (Vector2.Distance (new Vector2 (x, y), hit.point.ToVector2XZ ()) < 3 * myTarget.brushSize) 
				{

				Vector3i current = new Vector3i (indexes [i], indexes [i + 1], indexes [i + 2]);

				//if (myTarget.SubdSizeCheck (current)) 
					_triangles.Add (current);
				}
			}
		//} 
		//else
		//{
		//	_triangles.Add (new Vector3i (hit.triangleIndex, hit.triangleIndex + 1, hit.triangleIndex + 2));
		//	myTarget.triangleIndexes.Add (hit.triangleIndex);
		//}

		myTarget.data = new BlockPlane.Data (myTarget.editMeshCollider.sharedMesh, _triangles);
		return hit.point;
	}

	public virtual void OnSceneGUI()
	{
		myTarget = (BlockPlane)target;


		if (myTarget.isColliderEdited && myTarget.editMeshCollider!=null) 
		{
			bool isShift = false;



			Ray ray = HandleUtility.GUIPointToWorldRay (Event.current.mousePosition);	

			Vector3 point = CalculateTriangles(ray);

			Event current = Event.current;
			int controlID = GUIUtility.GetControlID (FocusType.Passive);

			if (Event.current.button == 1) {
				HandleUtility.AddDefaultControl (controlID);
				return;
			}

			if (Event.current.alt) {
				HandleUtility.AddDefaultControl (controlID);
				return;
			}

			if (Event.current.shift) {
				isShift = true;
			}

			switch (current.GetTypeForControl (controlID)) 
			{
			case EventType.MouseDown:
				
				if (myTarget.isOneShot) 
				{
					GUIUtility.hotControl = controlID;
					Overall (point, isShift);
				}
				else
					GUIUtility.hotControl = 0;
				current.Use ();
				break;
			case EventType.MouseUp:
				
				GUIUtility.hotControl = 0;

				isDelete = false;
				current.Use ();
				break;
			case EventType.MouseDrag:
				
				if (!myTarget.isOneShot) 
				{
					GUIUtility.hotControl = controlID;
					Overall (point, isShift);
				}
				else
					GUIUtility.hotControl = 0;
					current.Use ();
				break;
			case EventType.ScrollWheel:
				if (isShift) {
					GUIUtility.hotControl = 0;
					ChangeSize (current.delta.y);
					current.Use ();

				} 
				break;
			}

			/*
			if (myTarget.isCanRayCast) {
				Handles.color = new Color (0, Mathf.Max (0.1f, myTarget.brushOpacity * 0.5f), 0, 1);
				Handles.DrawWireDisc (hit[0].point, hit[0].normal, myTarget.brushSize * 3);
				Handles.color = new Color (0, Mathf.Max (0.6f, myTarget.brushOpacity), 0, 1);
				Handles.DrawWireDisc (hit[0].point, hit[0].normal, myTarget.brushSize * Mathf.Max (0.3f, 1) * 3);

				//Check (hit.point);

			}
			*/
		}
	}

	public void SidePolygons(Color _color)
	{
		for (int i = 0; i < myTarget.data.triangles.Count; i++) 
		{
			switch(myTarget.data.triangles[i].count)
			{
			case 2: 
				{

					BlockPlane.Vertex[] dot = new BlockPlane.Vertex[6];

					for (int y = 0; y < 3; y++) {
						if (!myTarget.data.triangles[i].checkSides [y].isChecked) {

							dot [0] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles[i].checkSides [y].a, myTarget.data.triangles[i].checkSides [y].b, _color);
							dot [1] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles[i].checkSides [y].c, _color);
							dot [2] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles[i].checkSides [y].a, _color);
							dot [3] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles[i].checkSides [y].a, myTarget.data.triangles[i].checkSides [y].b, _color);
							dot [4] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles[i].checkSides [y].b, _color);
							dot [5] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles[i].checkSides [y].c, _color);

							for (int z = 0; z < 6; z++)
								myTarget.data.AddVertex (dot [z]);
						}

					}


					int select = -1;
					for (int z = 0; z < myTarget.data.fullTriangles.Count; z++)
						if ((myTarget.data.triangles [i].x == myTarget.data.fullTriangles [z].x) && (myTarget.data.triangles [i].y == myTarget.data.fullTriangles [z].y) && (myTarget.data.triangles [i].z == myTarget.data.fullTriangles [z].z))
							select = z;

					Vector3i current = myTarget.data.fullTriangles [select];
					current.isDelete = true;
					myTarget.data.fullTriangles [select] = current;


					myTarget.data.fullTriangles.Add (new Vector3i (myTarget.data.vertexCount - 6, myTarget.data.vertexCount - 5, myTarget.data.vertexCount - 4));
					myTarget.data.fullTriangles.Add (new Vector3i (myTarget.data.vertexCount - 3, myTarget.data.vertexCount - 2, myTarget.data.vertexCount - 1));

				}
				break;
			case 1: 
				{

					BlockPlane.Vertex[] dot = new BlockPlane.Vertex[9];

					for (int y = 0; y < 3; y++) 
					{
						if (myTarget.data.triangles[i].checkSides [y].isChecked) 
						{
							dot [0] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles[i].checkSides [y].a, myTarget.data.triangles[i].checkSides [y].c, _color);
							dot [1] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles[i].checkSides [y].b, myTarget.data.triangles[i].checkSides [y].c, _color);
							dot [2] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles[i].checkSides [y].c, _color);
							dot [3] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles[i].checkSides [y].a, myTarget.data.triangles[i].checkSides [y].c, _color);
							dot [4] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles[i].checkSides [y].a, _color);
							dot [5] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles[i].checkSides [y].b, myTarget.data.triangles[i].checkSides [y].c, _color);
							dot [6] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles[i].checkSides [y].b, myTarget.data.triangles[i].checkSides [y].c, _color);
							dot [7] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles[i].checkSides [y].a, _color);
							dot [8] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles[i].checkSides [y].b, _color);

							for (int z = 0; z < 9; z++)
								myTarget.data.AddVertex (dot [z]);
						}

					}


					int select = -1;
					for (int z = 0; z < myTarget.data.fullTriangles.Count; z++)
						if ((myTarget.data.triangles [i].x == myTarget.data.fullTriangles [z].x) && (myTarget.data.triangles [i].y == myTarget.data.fullTriangles [z].y) && (myTarget.data.triangles [i].z == myTarget.data.fullTriangles [z].z))
							select = z;
					Vector3i current = myTarget.data.fullTriangles [select];
					current.isDelete = true;
					myTarget.data.fullTriangles [select] = current;


					myTarget.data.fullTriangles.Add (new Vector3i (myTarget.data.vertexCount - 9, myTarget.data.vertexCount - 8, myTarget.data.vertexCount - 7));
					myTarget.data.fullTriangles.Add (new Vector3i (myTarget.data.vertexCount - 6, myTarget.data.vertexCount - 5, myTarget.data.vertexCount - 4));
					myTarget.data.fullTriangles.Add (new Vector3i (myTarget.data.vertexCount - 3, myTarget.data.vertexCount - 2, myTarget.data.vertexCount - 1));

				}
				break;
			case 0: 
				{

					BlockPlane.Vertex[] dot = new BlockPlane.Vertex[6];

					dot [0] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles [i].x, myTarget.data.triangles [i].y, _color);
					dot [1] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles [i].y, myTarget.data.triangles [i].z, _color);
					dot [2] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles [i].z, myTarget.data.triangles [i].x, _color);
					dot [3] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles [i].x, _color); 
					dot [4] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles [i].y, _color); 
					dot [5] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles [i].z, _color); 

					for (int z = 0; z < 6; z++)
						myTarget.data.AddVertex(dot [z]);


					int select = -1;
					for (int z = 0; z < myTarget.data.fullTriangles.Count; z++)
						if ((myTarget.data.triangles [i].x == myTarget.data.fullTriangles [z].x) && (myTarget.data.triangles [i].y == myTarget.data.fullTriangles [z].y) && (myTarget.data.triangles [i].z == myTarget.data.fullTriangles [z].z))
							select = z;
					Vector3i current = myTarget.data.fullTriangles [select];
					current.isDelete = true;
					myTarget.data.fullTriangles [select] = current;


					myTarget.data.fullTriangles.Add (new Vector3i (myTarget.data.vertexCount - 6, myTarget.data.vertexCount - 5, myTarget.data.vertexCount - 4));
					myTarget.data.fullTriangles.Add (new Vector3i (myTarget.data.vertexCount - 6, myTarget.data.vertexCount - 2, myTarget.data.vertexCount - 5));
					myTarget.data.fullTriangles.Add (new Vector3i (myTarget.data.vertexCount - 5, myTarget.data.vertexCount - 1, myTarget.data.vertexCount - 4));
					myTarget.data.fullTriangles.Add (new Vector3i (myTarget.data.vertexCount - 4, myTarget.data.vertexCount - 3, myTarget.data.vertexCount - 6));



				}
				break;
			case 4:
				{
				}
				break;
			}


		}
		myTarget.data.RecalcIndexes ();

		//;

		myTarget.editMeshCollider.sharedMesh = myTarget.editedCollider;

		myTarget.editMeshCollider.sharedMesh = myTarget.data.CreateMesh ("333");
		myTarget.editedCollider = myTarget.editMeshCollider.sharedMesh;

		myTarget.editedCollider.RecalculateNormals ();
		myTarget.editedCollider.RecalculateBounds ();

		AutoWeld (myTarget.editedCollider, 0.01f);

		KillUselessVertex (myTarget.editedCollider);
	}


	public void SidePolygonsPrepare(Color _color)
	{
		for (int i = 0; i < myTarget.data.triangles.Count; i++) 
		{
			if (myTarget.data.triangles[i].count == 2)
			{
					
					BlockPlane.Vertex[] dot = new BlockPlane.Vertex[6];

					for (int y = 0; y < 3; y++) {
						if (!myTarget.data.triangles[i].checkSides [y].isChecked)
						{

							dot [0] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles[i].checkSides [y].a, _color);
							dot [1] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles[i].checkSides [y].b, _color);
							dot [2] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles[i].checkSides [y].c, Color.black);


							for (int z = 0; z < 3; z++)
								myTarget.data.AddVertex (dot [z]);
						}

					}


						int select = -1;
						for (int z = 0; z < myTarget.data.fullTriangles.Count; z++)
							if ((myTarget.data.triangles [i].x == myTarget.data.fullTriangles [z].x) && (myTarget.data.triangles [i].y == myTarget.data.fullTriangles [z].y) && (myTarget.data.triangles [i].z == myTarget.data.fullTriangles [z].z))
								select = z;
					
						Vector3i current = myTarget.data.fullTriangles [select];
						current.isDelete = true;
						myTarget.data.fullTriangles [select] = current;

					myTarget.data.fullTriangles.Add (new Vector3i (myTarget.data.vertexCount - 3, myTarget.data.vertexCount - 2, myTarget.data.vertexCount - 1));

			}
		}

		myTarget.data.RecalcIndexes ();

		//;

		myTarget.editMeshCollider.sharedMesh = myTarget.editedCollider;

		myTarget.editMeshCollider.sharedMesh = myTarget.data.CreateMesh ("333");
		myTarget.editedCollider = myTarget.editMeshCollider.sharedMesh;

		myTarget.editedCollider.RecalculateNormals ();
		myTarget.editedCollider.RecalculateBounds ();

		AutoWeld (myTarget.editedCollider, 0.01f);

		KillUselessVertex (myTarget.editedCollider);
	}


	void SubdivideOverall()
	{
		myTarget = (BlockPlane)target;

		for (int i = 0; i < myTarget.data.triangles.Count; i++) 
		{

			if (!myTarget.SubdSizeCheck (myTarget.data.triangles[i]))
				continue;

			myTarget.data.color [myTarget.data.triangles [i].x] = Color.yellow;
			myTarget.data.color [myTarget.data.triangles [i].y] = Color.yellow;
			myTarget.data.color [myTarget.data.triangles [i].z] = Color.yellow;

			BlockPlane.Vertex[] dot = new BlockPlane.Vertex[6];
			Color _color = Color.black;
			dot [0] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles [i].x, myTarget.data.triangles [i].y, Color.yellow);
			dot [1] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles [i].y, myTarget.data.triangles [i].z, Color.yellow);
			dot [2] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles [i].z, myTarget.data.triangles [i].x, Color.yellow);
			dot [3] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles [i].x, Color.yellow);
			dot [4] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles [i].y, Color.yellow);
			dot [5] = new BlockPlane.Vertex (myTarget.data, myTarget.data.triangles [i].z, Color.yellow); 

			for (int z = 0; z < 6; z++)
				myTarget.data.AddVertex (dot [z]);

			myTarget.data.fullTriangles.Remove (myTarget.data.triangles [i]);

			myTarget.data.fullTriangles.Add (new Vector3i (myTarget.data.vertexCount - 6, myTarget.data.vertexCount - 5, myTarget.data.vertexCount - 4));
			myTarget.data.fullTriangles.Add (new Vector3i (myTarget.data.vertexCount - 6, myTarget.data.vertexCount - 2, myTarget.data.vertexCount - 5));
			myTarget.data.fullTriangles.Add (new Vector3i (myTarget.data.vertexCount - 5, myTarget.data.vertexCount - 1, myTarget.data.vertexCount - 4));
			myTarget.data.fullTriangles.Add (new Vector3i (myTarget.data.vertexCount - 4, myTarget.data.vertexCount - 3, myTarget.data.vertexCount - 6));

		}
		myTarget.data.RecalcIndexes ();

		myTarget.editMeshCollider.sharedMesh = myTarget.data.CreateMesh ("000");
		myTarget.editedCollider = myTarget.editMeshCollider.sharedMesh;
	}

	void DeleteOverall()
	{
		myTarget = (BlockPlane)target;

		for (int i = 0; i < myTarget.data.triangles.Count; i++)
		{
			int select = myTarget.data.fullTriangles.IndexOf (myTarget.data.triangles [i]);
			Vector3i current = myTarget.data.fullTriangles [select];
			current.isDelete = true;
			myTarget.data.fullTriangles [select] = current;
		}
		myTarget.data.RecalcIndexes ();

		myTarget.editedCollider.RecalculateNormals ();
		myTarget.editedCollider.RecalculateBounds ();

		myTarget.editMeshCollider.sharedMesh = myTarget.data.CreateMesh ("111");
		myTarget.editedCollider = myTarget.editMeshCollider.sharedMesh;
	}
	/*
	void SideSubdivideOverall()
	{
		for (int i = 0; i < myTarget.data.triangles.Count; i++) 
		{
			myTarget.data.color [myTarget.data.triangles [i].x] = Color.red;
			myTarget.data.color [myTarget.data.triangles [i].y] = Color.red;
			myTarget.data.color [myTarget.data.triangles [i].z] = Color.red;

			if (!myTarget.SubdSizeCheck (myTarget.data.triangles[i])) 
			{
				Vector3i triangle = myTarget.data.triangles [i];
				triangle.count = 4;
				myTarget.data.triangles [i] = triangle;
				continue;
			}

			BlockPlane.SubdCheckSide[] checkSides = new BlockPlane.SubdCheckSide[3];
			checkSides [0] = new BlockPlane.SubdCheckSide (myTarget.data.triangles [i].x, myTarget.data.triangles [i].y, myTarget.data.triangles [i].z);
			checkSides [1] = new BlockPlane.SubdCheckSide (myTarget.data.triangles [i].y, myTarget.data.triangles [i].z, myTarget.data.triangles [i].x);
			checkSides [2] = new BlockPlane.SubdCheckSide (myTarget.data.triangles [i].z, myTarget.data.triangles [i].x, myTarget.data.triangles [i].y);

			int count = myTarget.nearPolygons[i].triangleIndex.Count;
			//Debug.Log (myTarget.nearPolygons [i].triangleIndex.Count);
			for (int u = 0; u < myTarget.nearPolygons[i].triangleIndex.Count; u++) 
			{
				int ti = myTarget.nearPolygons [i].triangleIndex [u];

				for (int y = 0; y < 3; y++) 
				{
					if ((checkSides [y].a == myTarget.data.fullTriangles [ti].x && checkSides [y].b == myTarget.data.fullTriangles [ti].y) || (checkSides [y].b == myTarget.data.fullTriangles [ti].x && checkSides [y].a == myTarget.data.fullTriangles [ti].y)) 
						checkSides [y].isChecked = true;

					if ((checkSides [y].a == myTarget.data.fullTriangles [ti].y && checkSides [y].b == myTarget.data.fullTriangles [ti].z) || (checkSides [y].b == myTarget.data.fullTriangles [ti].y && checkSides [y].a == myTarget.data.fullTriangles [ti].z)) 
						checkSides [y].isChecked = true;

					if ((checkSides [y].a == myTarget.data.fullTriangles [ti].z && checkSides [y].b == myTarget.data.fullTriangles [ti].x) || (checkSides [y].b == myTarget.data.fullTriangles [ti].z && checkSides [y].a == myTarget.data.fullTriangles [ti].x)) 
						checkSides [y].isChecked = true;
				}

			}

			Vector3i triangle2 = myTarget.data.triangles [i];
			triangle2.checkSides = new BlockPlane.SubdCheckSide[3];
			for (int y = 0; y < 3; y++)
				triangle2.checkSides[y] = checkSides [y];
			triangle2.count = count;
			myTarget.data.triangles [i] = triangle2;

			//Debug.Log (myTarget.data.triangles [i].count);
		}

		SidePolygons (true);
	}
	*/
	public void Overall(Vector3 point, bool isShift)
	{
		
		if (!myTarget.editMeshCollider.sharedMesh)
			return;

			
		if (myTarget.isCanRayCast && !isDelete) 
		{
			if (myTarget.tabSwitch == 1 && (myTarget.colliderStep == BlockPlane.ColliderEditStep.Tier1 || myTarget.colliderStep == BlockPlane.ColliderEditStep.Tier2))
				SubdivideOverall ();

			if ((myTarget.tabSwitch == 2 && (myTarget.colliderStep == BlockPlane.ColliderEditStep.Tier1 || myTarget.colliderStep == BlockPlane.ColliderEditStep.Tier2)) || (myTarget.tabSwitch == 1 && (myTarget.colliderStep == BlockPlane.ColliderEditStep.Prepare || myTarget.colliderStep == BlockPlane.ColliderEditStep.Correction)))
				DeleteOverall ();
				//SideSubdivideOverall ();


		}

		myTarget.verticesCount = myTarget.editedCollider.vertexCount;
		int[] indices = myTarget.editedCollider.triangles;
		myTarget.polygonsCount = indices.Length/3;

	}

		/*

			switch (myTarget.tabSwitch) 
			{
			case 0:
				//AutoWeldLocal (0.01f);
				break;
			case 1:
				{
					
				}
				break;

			case 2:
				{

					
				}
				break;

			case 2:
				{
					DeleteOverall ();
				}
				break;
			
			}
			//if (myTarget.isOneShot)
			//isDelete = true;
		}
	}

	
	public void DrawInsideMarkers()
	{
		myTarget.subdSwitch = 1;
		myTarget.SelectAll ();

		List<Vector2i> newColors = new List<Vector2i> ();

		for (int i = 0; i < myTarget.data.triangles.Count; i++) 
		{
			if (!myTarget.SubdSizeCheck (i)) 
			{
				Vector3i triangle = myTarget.data.triangles [i];
				triangle.count = 4;
				myTarget.data.triangles [i] = triangle;
				continue;
			}

			for (int j = 0; j < myTarget.data.fullTriangles.Count; j++) 
			{
				if (myTarget.data.color [myTarget.data.fullTriangles [j].x] == Color.red || myTarget.data.color [myTarget.data.fullTriangles [j].y] == Color.red || myTarget.data.color [myTarget.data.fullTriangles [j].z] == Color.red) 
				{
					if (myTarget.data.triangles[i].isContain(myTarget.data.fullTriangles [j])>=1)
					{
						if (myTarget.data.fullTriangles[i].isContain(myTarget.data.triangles [j].x))
							newColors.Add (new Vector2i(i, 0));
						if (myTarget.data.fullTriangles[i].isContain(myTarget.data.triangles [j].y))
							newColors.Add (new Vector2i(i, 1));
						if (myTarget.data.fullTriangles[i].isContain(myTarget.data.triangles [j].z))
							newColors.Add (new Vector2i(i, 2));
					}
				}
			}
				
		}

		for (int i = 0; i < newColors.Count; i++) 
		{
			if (newColors[i].y == 0)
				myTarget.data.color [myTarget.data.triangles[newColors[i].x].x] = Color.red;
			if (newColors[i].y == 1)
				myTarget.data.color [myTarget.data.triangles[newColors[i].x].y] = Color.red;
			if (newColors[i].y == 2)
				myTarget.data.color [myTarget.data.triangles[newColors[i].x].z] = Color.red;
		}



		myTarget.editMeshCollider.sharedMesh = myTarget.editedCollider;

		myTarget.editMeshCollider.sharedMesh = myTarget.data.CreateMesh ("333");
		myTarget.editedCollider = myTarget.editMeshCollider.sharedMesh;

		myTarget.editedCollider.RecalculateNormals ();
		myTarget.editedCollider.RecalculateBounds ();
	}
	*/

	public void SideForAll02()
	{
		myTarget.data = new BlockPlane.Data (myTarget.editMeshCollider.sharedMesh);
		myTarget.SelectSecond ();
		/*
		for (int i = 0; i < myTarget.data.triangles.Count; i++) 
		{
			
			//if (!myTarget.SubdSizeCheck02 (i)) 
			//{
			//
			//	continue;
			//
			//}


			int select = myTarget.data.fullTriangles.IndexOf (myTarget.data.triangles [i]);
			Vector3i current = myTarget.data.fullTriangles [select];
			current.isDelete = true;
			myTarget.data.fullTriangles [select] = current;
		}

		myTarget.data.RecalcIndexes ();

		myTarget.editMeshCollider.sharedMesh = myTarget.editedCollider;

		myTarget.editMeshCollider.sharedMesh = myTarget.data.CreateMesh ("333");
		myTarget.editedCollider = myTarget.editMeshCollider.sharedMesh;

		myTarget.editedCollider.RecalculateNormals ();
		myTarget.editedCollider.RecalculateBounds ();

		*/

		for (int i = 0; i < myTarget.data.triangles.Count; i++) 
		{

			int count = myTarget.nearPolygons [i].triangleIndex.Count;

			BlockPlane.SubdCheckSide[] checkSides = new BlockPlane.SubdCheckSide[3];
			checkSides [0] = new BlockPlane.SubdCheckSide (myTarget.data.triangles [i].x, myTarget.data.triangles [i].y, myTarget.data.triangles [i].z);
			checkSides [1] = new BlockPlane.SubdCheckSide (myTarget.data.triangles [i].y, myTarget.data.triangles [i].z, myTarget.data.triangles [i].x);
			checkSides [2] = new BlockPlane.SubdCheckSide (myTarget.data.triangles [i].z, myTarget.data.triangles [i].x, myTarget.data.triangles [i].y);

			if (myTarget.nearPolygons[i].triangleIndex != null) 
			{

				//Debug.Log (myTarget.nearPolygons [i].triangleIndex.Count);
				for (int u = 0; u < myTarget.nearPolygons [i].triangleIndex.Count; u++) 
				{
					int ti = myTarget.nearPolygons [i].triangleIndex [u];

					for (int y = 0; y < 3; y++) 
					{
						if ((checkSides [y].a == myTarget.data.fullTriangles [ti].x && checkSides [y].b == myTarget.data.fullTriangles [ti].y) || (checkSides [y].b == myTarget.data.fullTriangles [ti].x && checkSides [y].a == myTarget.data.fullTriangles [ti].y))
							checkSides [y].isChecked = true;
						if ((checkSides [y].a == myTarget.data.fullTriangles [ti].y && checkSides [y].b == myTarget.data.fullTriangles [ti].z) || (checkSides [y].b == myTarget.data.fullTriangles [ti].y && checkSides [y].a == myTarget.data.fullTriangles [ti].z))
							checkSides [y].isChecked = true;
						if ((checkSides [y].a == myTarget.data.fullTriangles [ti].z && checkSides [y].b == myTarget.data.fullTriangles [ti].x) || (checkSides [y].b == myTarget.data.fullTriangles [ti].z && checkSides [y].a == myTarget.data.fullTriangles [ti].x))
							checkSides [y].isChecked = true;
					}

				}

				Vector3i triangle2 = myTarget.data.triangles [i];
				triangle2.checkSides = new BlockPlane.SubdCheckSide[3];
				for (int y = 0; y < 3; y++)
					triangle2.checkSides [y] = checkSides [y];
				triangle2.count = count;
				myTarget.data.triangles [i] = triangle2;
			}
			//Debug.Log (myTarget.data.triangles [i].count);
		}

		SidePolygons (Color.red);
		myTarget.colliderStep = BlockPlane.ColliderEditStep.Correction;
		myTarget.verticesCount = myTarget.editedCollider.vertexCount;
		int[] indices = myTarget.editedCollider.triangles;
		myTarget.polygonsCount = indices.Length/3;

	}

	public void PrepareSubdivide()
	{
		myTarget.SelectAll (false);

		for (int i = 0; i < myTarget.data.triangles.Count; i++) 
		{
			
			int count = myTarget.nearPolygons [i].triangleIndex.Count;

			BlockPlane.SubdCheckSide[] checkSides = new BlockPlane.SubdCheckSide[3];
			checkSides [0] = new BlockPlane.SubdCheckSide (myTarget.data.triangles [i].x, myTarget.data.triangles [i].y, myTarget.data.triangles [i].z);
			checkSides [1] = new BlockPlane.SubdCheckSide (myTarget.data.triangles [i].y, myTarget.data.triangles [i].z, myTarget.data.triangles [i].x);
			checkSides [2] = new BlockPlane.SubdCheckSide (myTarget.data.triangles [i].z, myTarget.data.triangles [i].x, myTarget.data.triangles [i].y);

			if (myTarget.nearPolygons[i].triangleIndex != null) 
			{
				for (int u = 0; u < myTarget.nearPolygons [i].triangleIndex.Count; u++) 
				{
					int ti = myTarget.nearPolygons [i].triangleIndex [u];

					for (int y = 0; y < 3; y++) 
					{
						if ((checkSides [y].a == myTarget.data.fullTriangles [ti].x && checkSides [y].b == myTarget.data.fullTriangles [ti].y) || (checkSides [y].b == myTarget.data.fullTriangles [ti].x && checkSides [y].a == myTarget.data.fullTriangles [ti].y))
							checkSides [y].isChecked = true;
						if ((checkSides [y].a == myTarget.data.fullTriangles [ti].y && checkSides [y].b == myTarget.data.fullTriangles [ti].z) || (checkSides [y].b == myTarget.data.fullTriangles [ti].y && checkSides [y].a == myTarget.data.fullTriangles [ti].z))
							checkSides [y].isChecked = true;
						if ((checkSides [y].a == myTarget.data.fullTriangles [ti].z && checkSides [y].b == myTarget.data.fullTriangles [ti].x) || (checkSides [y].b == myTarget.data.fullTriangles [ti].z && checkSides [y].a == myTarget.data.fullTriangles [ti].x))
							checkSides [y].isChecked = true;
					}



				}

				Vector3i triangle2 = myTarget.data.triangles [i];
				triangle2.checkSides = new BlockPlane.SubdCheckSide[3];
				for (int y = 0; y < 3; y++)
					triangle2.checkSides [y] = checkSides [y];
				triangle2.count = count;
				myTarget.data.triangles [i] = triangle2;
			}
		}

		//SidePolygons (Color.blue);
		SidePolygonsPrepare (Color.blue);
		myTarget.colliderStep = BlockPlane.ColliderEditStep.Tier1;
		myTarget.verticesCount = myTarget.editedCollider.vertexCount;
		int[] indices = myTarget.editedCollider.triangles;
		myTarget.polygonsCount = indices.Length/3;
	}

	public void SideForAll()
	{
		myTarget.SelectAll (false);

		for (int i = 0; i < myTarget.data.triangles.Count; i++) 
		{
			

			if (!myTarget.SubdSizeCheckWithoutBlue (myTarget.data.triangles[i])) 
			{
				
				Vector3i triangle = myTarget.data.triangles [i];
				triangle.count = 4;
				myTarget.data.triangles [i] = triangle;
				continue;
			}

			int count = myTarget.nearPolygons [i].triangleIndex.Count;

			BlockPlane.SubdCheckSide[] checkSides = new BlockPlane.SubdCheckSide[3];
			checkSides [0] = new BlockPlane.SubdCheckSide (myTarget.data.triangles [i].x, myTarget.data.triangles [i].y, myTarget.data.triangles [i].z);
			checkSides [1] = new BlockPlane.SubdCheckSide (myTarget.data.triangles [i].y, myTarget.data.triangles [i].z, myTarget.data.triangles [i].x);
			checkSides [2] = new BlockPlane.SubdCheckSide (myTarget.data.triangles [i].z, myTarget.data.triangles [i].x, myTarget.data.triangles [i].y);

			if (myTarget.nearPolygons[i].triangleIndex != null) 
			{
				for (int u = 0; u < myTarget.nearPolygons [i].triangleIndex.Count; u++) 
				{
					int ti = myTarget.nearPolygons [i].triangleIndex [u];

					for (int y = 0; y < 3; y++) 
					{
						if ((checkSides [y].a == myTarget.data.fullTriangles [ti].x && checkSides [y].b == myTarget.data.fullTriangles [ti].y) || (checkSides [y].b == myTarget.data.fullTriangles [ti].x && checkSides [y].a == myTarget.data.fullTriangles [ti].y))
							checkSides [y].isChecked = true;
						if ((checkSides [y].a == myTarget.data.fullTriangles [ti].y && checkSides [y].b == myTarget.data.fullTriangles [ti].z) || (checkSides [y].b == myTarget.data.fullTriangles [ti].y && checkSides [y].a == myTarget.data.fullTriangles [ti].z))
							checkSides [y].isChecked = true;
						if ((checkSides [y].a == myTarget.data.fullTriangles [ti].z && checkSides [y].b == myTarget.data.fullTriangles [ti].x) || (checkSides [y].b == myTarget.data.fullTriangles [ti].z && checkSides [y].a == myTarget.data.fullTriangles [ti].x))
							checkSides [y].isChecked = true;
					}

				}

				Vector3i triangle2 = myTarget.data.triangles [i];
				triangle2.checkSides = new BlockPlane.SubdCheckSide[3];
				for (int y = 0; y < 3; y++)
					triangle2.checkSides [y] = checkSides [y];
				triangle2.count = count;
				myTarget.data.triangles [i] = triangle2;
			}
		}

		SidePolygons (Color.red);
		myTarget.colliderStep = BlockPlane.ColliderEditStep.Tier2;
		myTarget.verticesCount = myTarget.editedCollider.vertexCount;
		int[] indices = myTarget.editedCollider.triangles;
		myTarget.polygonsCount = indices.Length/3;
	}

	private void AutoWeld(Mesh mesh, float threshold) {
		Vector3[] verts = mesh.vertices;

		// Build new vertex buffer and remove "duplicate" verticies
		// that are within the given threshold.
		List<Vector3> newVerts = new List<Vector3>();
		List<Color> newUVs = new List<Color>();

		int k = 0;

		for (int i = 0; i < verts.Length; i++)
		{
			for (int j = 0; j < newVerts.Count; j++)

				if (Vector3.Distance (newVerts [j], verts [i]) <= threshold) 
				{
					if (mesh.colors [i] == Color.red)
						newUVs[j] = Color.red;
					if (mesh.colors [i] == Color.blue)
						newUVs[j] = Color.blue;
					goto skipToNext;
				}
			// Accept new vertex!
			newVerts.Add(verts[i]);
			newUVs.Add(mesh.colors[k]);

			skipToNext:;
			++k;
		}

		// Rebuild triangles using new verticies
		int[] tris = mesh.triangles;
		for (int i = 0; i < tris.Length; ++i)
		{
			// Find new vertex point from buffer
			for (int j = 0; j < newVerts.Count; ++j) {
				if (Vector3.Distance(newVerts[j], verts[ tris[i] ]) <= threshold) {
					tris[i] = j;
					break;
				}
			}
		}

		// Update mesh!
		mesh.Clear();
		mesh.vertices = newVerts.ToArray();
		mesh.triangles = tris;
		mesh.colors = newUVs.ToArray();
		mesh.RecalculateBounds();

		myTarget.verticesCount = myTarget.editedCollider.vertexCount;
		int[] indices = myTarget.editedCollider.triangles;
		myTarget.polygonsCount = indices.Length/3;
	}



	private void KillUselessVertex(Mesh mesh)
	{
		//Vector3[] verts = mesh.vertices;
		//int[] ind = mesh.triangles;

		MeshUtility.Optimize (mesh);
		//for (int i = 0; i < verts.Length; i++) 
		//{
		//	
		//}

		myTarget.verticesCount = myTarget.editedCollider.vertexCount;
		int[] indices = myTarget.editedCollider.triangles;
		myTarget.polygonsCount = indices.Length/3;
	}



	void ChangeSize(float delta)
	{
		myTarget = (BlockPlane)target;
		if (delta > 0) {
			if (myTarget.brushSize + delta * 0.1f >= myTarget.brushSizeMinMax.y)
				myTarget.brushSize = myTarget.brushSizeMinMax.y;
			else
				myTarget.brushSize += delta * 0.1f;
		} 
		else
		{
			if (myTarget.brushSize + delta * 0.1f <= myTarget.brushSizeMinMax.x)
				myTarget.brushSize = myTarget.brushSizeMinMax.x;
			else
				myTarget.brushSize += delta * 0.1f;
		}

	}

}
