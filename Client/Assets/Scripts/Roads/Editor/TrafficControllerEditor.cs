﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(TrafficController))]
public class TrafficControllerEditor : Editor
{


    /*
    public override void OnInspectorGUI()
    {
        TrafficController myTarget = (TrafficController)target;
        myTarget.startScale = EditorGUILayout.FloatField("Start Scale", myTarget.startScale);

        if (GUILayout.Button("Clear Curve"))
        {
            myTarget.StartThis();
        }

        if (GUILayout.Button("Add Curve Part"))
        {
            myTarget.Add();
        }

        if (GUILayout.Button("Show Curve Locators"))
        {
            myTarget.Show();
        }
        EditorGUILayout.Separator();
        for (int i = 0; i < myTarget.CurveLenght; i++)
        {
            if (GUILayout.Button("Часть " + i.ToString()))
            {
                myTarget.curve[i].editorEnable = !myTarget.curve[i].editorEnable;
            }
            if (myTarget.curve[i].editorEnable)
            {
                EditorGUILayout.Separator();
                
                myTarget.curve[i].scale = EditorGUILayout.FloatField("Start Scale", myTarget.curve[i].scale);
            }
        }
        EditorGUILayout.Separator();
        if (GUILayout.Button("Clear Units"))
        {
            myTarget.StartCar();
        }
        if (GUILayout.Button("Add Unit"))
        {
            myTarget.AddCar();
        }
        for (int i = 0; i < myTarget.CarsLenght; i++)
        {
            myTarget.trafficObjects[i].name = EditorGUILayout.TextField("Unit", myTarget.trafficObjects[i].name);
            myTarget.trafficObjects[i].probability = EditorGUILayout.FloatField("Probability", myTarget.trafficObjects[i].probability);
            
            
        }
        EditorGUILayout.Separator();
        myTarget.frontLinesCount = EditorGUILayout.IntField("Front Lines", myTarget.frontLinesCount);
        myTarget.backLinesCount = EditorGUILayout.IntField("Back Lines", myTarget.backLinesCount);
        if (GUILayout.Button("Update Lines"))
        {
            myTarget.UpdateLines();
        }

        EditorGUILayout.LabelField("Top Lines");
        for (int i = 0; i < myTarget.frontLinesCount + myTarget.backLinesCount; i++)
        {
            EditorGUILayout.BeginHorizontal();
            myTarget.trafficLines[i].direction = EditorGUILayout.Toggle("Line " + i + " Direction, Timer Max", myTarget.trafficLines[i].direction);
            myTarget.trafficLines[i].timerMax = EditorGUILayout.FloatField(myTarget.trafficLines[i].timerMax);
            EditorGUILayout.EndVertical();
            myTarget.trafficLines[i].probability = EditorGUILayout.Vector2Field("Probability", myTarget.trafficLines[i].probability);
           
            

            if (i + 1 == myTarget.frontLinesCount)
            {
                EditorGUILayout.Separator();
                EditorGUILayout.LabelField("Back Lines");
            }
        }

    }
    */

}