﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor (typeof(BoundsCorrector))]
public class BoundsCorrectorEditor : Editor {

	public override void OnInspectorGUI ()
	{
		BoundsCorrector myTarget = (BoundsCorrector)target;
		myTarget.isOne = EditorGUILayout.Toggle ("is One", myTarget.isOne);
			
			
	}
}
