﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Main))]
public class MainEditor : Editor
{

    

    public override void OnInspectorGUI()
    {
        Main myTarget = (Main)target;
        myTarget.gameSpeed = EditorGUILayout.FloatField("Time Scale", myTarget.gameSpeed);
        myTarget.speedFactorActive = GUILayout.Toggle(myTarget.speedFactorActive, "Speed Factor");
        myTarget.isAll = GUILayout.Toggle(myTarget.isAll, "Full Shop");
        myTarget.moneyCount = EditorGUILayout.IntField("Money", myTarget.moneyCount);

        /*
        if (!myTarget.isBuilding)
        {
            if (GUILayout.Button("Build Level"))
            {
                myTarget.BuildLevel();
            }
            else
                if (GUILayout.Button("Save Level"))
                {
                    myTarget.SaveLevel();
                }
        }
         */

        //if (GUILayout.Button("Check"))
        //{
        //    myTarget._topLineCollider.Check();
        //}
    }
}
