﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(MainCamera))]
public class MainCameraEditor : Editor
{
	  

    public override void OnInspectorGUI()
    {
		MainCamera myTarget = (MainCamera)target;
		float width = EditorGUIUtility.currentViewWidth;

		GUIStyle _labelStyle;
		_labelStyle = new GUIStyle();
		_labelStyle.alignment = TextAnchor.MiddleCenter;
		_labelStyle.clipping = TextClipping.Overflow;
		_labelStyle.fontSize = 12;

		//GUI.Box(GUILayoutUtility.GetRect(width, 20.0f), "Cam Percent");
		EditorGUI.ProgressBar (EditorGUILayout.GetControlRect (true, 20.0f, _labelStyle, GUILayout.Height (20.0f)), myTarget.GetCamPercent() / 1.0f, "_currentCamPercent"/*"Segment limit: " + fullCounter.ToString() + " / 720885"*/);
		EditorGUILayout.Separator ();
		EditorGUI.ProgressBar (EditorGUILayout.GetControlRect (true, 20.0f, _labelStyle, GUILayout.Height (20.0f)), myTarget.GetCamPercentRotation() / 1.0f, "_currentCamPersentForRotation"/*"Segment limit: " + fullCounter.ToString() + " / 720885"*/);
		EditorGUILayout.Separator ();
		EditorGUI.ProgressBar (EditorGUILayout.GetControlRect (true, 20.0f, _labelStyle, GUILayout.Height (20.0f)), myTarget.GetHeroPercent() / 1.0f, "_currentHeroPercent"/*"Segment limit: " + fullCounter.ToString() + " / 720885"*/);
		EditorGUILayout.Separator ();
		EditorGUI.ProgressBar (EditorGUILayout.GetControlRect (true, 20.0f, _labelStyle, GUILayout.Height (20.0f)), myTarget.cameraDotCurrentPercent / 1.0f, myTarget.cameraDotCurrentPercent.ToString()/*"Segment limit: " + fullCounter.ToString() + " / 720885"*/);
		if(myTarget._currentCamPlane!=null)
		GUILayout.Label("localForkNumber " + myTarget._currentCamPlane.localForkNumber.ToString());
		base.OnInspectorGUI ();

    }
}
