﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;

[CustomEditor(typeof(OneSpline))]
public class OneSplineEditor : Editor
{
    float percent;
    public override void OnInspectorGUI()
    {


        OneSpline myTarget = (OneSpline)target;
		myTarget.type = (SplineType)EditorGUILayout.EnumPopup("Type", myTarget.type);

		GUILayout.BeginHorizontal ();
        
		GUI.color = (myTarget.isVisible) ? Color.red : Color.green;
		if (GUILayout.Button((myTarget.isVisible) ? "Hide curve" : "Show curve", GUILayout.Height(20)))
		{
			myTarget.isVisible = !myTarget.isVisible;
		}
		GUI.color = (myTarget.isOpen) ? Color.red : Color.green;
		if (GUILayout.Button((myTarget.isOpen) ? "Hide data" : "Show data", GUILayout.Height(20)))
		{
			myTarget.isOpen = !myTarget.isOpen;
		}
		GUILayout.EndHorizontal ();
		GUI.color = Color.white;
		if (myTarget.isOpen) 
		{
			GUILayout.Space (2);
			GUILayout.Label ("Spline", EditorStyles.boldLabel);
			GUILayout.BeginVertical ("Box");
			GUILayout.Space (3);
			myTarget.a = EditorGUILayout.Vector3Field ("start position (a)", myTarget.a);
			myTarget.aa = EditorGUILayout.Vector3Field ("start support pivot (aa)", myTarget.aa);
			myTarget.bb = EditorGUILayout.Vector3Field ("end support pivot (bb)", myTarget.bb);
			myTarget.b = EditorGUILayout.Vector3Field ("end position (b)", myTarget.b);

			GUILayout.Space (8);
			myTarget.startRot = EditorGUILayout.Vector3Field ("start rotation (a)", myTarget.startRot);
			myTarget.endRot = EditorGUILayout.Vector3Field ("end rotation (b)", myTarget.endRot);
			GUILayout.Space (8);
			myTarget.startWidth = EditorGUILayout.FloatField ("start road width", myTarget.startWidth);
			myTarget.endWidth = EditorGUILayout.FloatField ("end road width", myTarget.endWidth);
			GUILayout.Space (8);
			myTarget.startVerge = EditorGUILayout.FloatField ("start verge width", myTarget.startVerge);
			myTarget.endVerge = EditorGUILayout.FloatField ("end verge width", myTarget.endVerge);
			GUILayout.Space (3);
			if (!myTarget.isCreateLocators) {
				GUI.color = Color.green;
				if (GUILayout.Button ("Create locators", GUILayout.Height (20))) {
					myTarget.CreateLocators ();
					myTarget.isCreateLocators = true;
				}
			} else {
				GUI.color = Color.red;
				if (GUILayout.Button ("Apply locators", GUILayout.Height (20))) {
					myTarget.DeleteLocators ();
					myTarget.isCreateLocators = false;
				}
			}
			GUI.color = Color.white;
			GUILayout.Space (3);

			GUILayout.EndVertical ();

			GUILayout.Space (2);
			GUILayout.Label ("Camera Local Settings", EditorStyles.boldLabel);
			GUILayout.Space (1);
			GUILayout.BeginVertical ("Box");

			if (myTarget.cameraDots != null)
				for (int i = 0; i < myTarget.cameraDots.Count; i++) 
				{
					GUILayout.Label ("CameraDot " + i.ToString ());
					myTarget.cameraDots [i].percent = EditorGUILayout.Slider ("percent", myTarget.cameraDots [i].percent, 0, 1.0f);
					myTarget.cameraDots [i].AxisBendSize = EditorGUILayout.Vector2Field("AxisBendSize", myTarget.cameraDots [i].AxisBendSize);
					myTarget.cameraDots [i].Bias = EditorGUILayout.Vector2Field("Bias", myTarget.cameraDots [i].Bias);
					//myTarget.cameraDots [i].supportPercentPrevious = EditorGUILayout.Slider("SupportPercentPrevious", myTarget.cameraDots [i].supportPercentPrevious, 0, 0.5f);
					myTarget.cameraDots [i].localPosition = EditorGUILayout.Vector3Field("LocalPosition", myTarget.cameraDots [i].localPosition);
					myTarget.cameraDots [i].localRotate = EditorGUILayout.Vector3Field("LocalRotation", myTarget.cameraDots [i].localRotate);
					EditorGUILayout.Separator ();
				}
			
			if (GUILayout.Button ("Add new dot", GUILayout.Height(20))) 
			{
				if (myTarget.cameraDots == null)
					myTarget.cameraDots = new System.Collections.Generic.List<CameraDot> ();

				myTarget.cameraDots.Add (new CameraDot ());
			}

			GUILayout.EndVertical ();

		}
			
		
        /*
        if (GUILayout.Button("Dotes"))
        {
            myTarget.GetDotes();
        }
        percent = EditorGUILayout.FloatField("percent", percent);
        if (GUILayout.Button("Check"))
        {
            myTarget.CheckPercent(percent);
        }
        if (GUILayout.Button("CheckCorrect"))
        {
            myTarget.CheckCorrect(percent);
        }
        if (GUILayout.Button("5Spline"))
        {
            myTarget.Create5Bezier();
        }
        */
        
    }
}
