﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Dest.Math;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class OneSpline : Spline
{
    

	public float EditorPercent;
	public List<CameraDot> cameraDots;

	#if UNITY_EDITOR
	Transform a_sphere;
	Transform b_sphere;
	Transform aa_sphere;
	Transform bb_sphere;
	public bool isOpen = false;
	public bool isCreateLocators = false;
	#endif

	public override Vector3 getRot(float t)
	{
		return Quaternion.Lerp (Quaternion.Euler(startRot), Quaternion.Euler(endRot), t).eulerAngles;
	}

    public override Vector3 getStartRot(float t)
    {
        return startRot;
    }


	public override CameraDot[] GetCameraDots()
	{
		return cameraDots.ToArray ();
	}

    public override Spline[] getSplines()

    {
        return null;
    }

    public override Vector3 getEndRot(float t)
    {
        return endRot;
    }

    public override float CheckCamProcess(float t)
    {
        return t;
    }


    public OneSpline()
    {
        a = Vector3.zero;
        b = Vector3.zero;
        aa = Vector3.zero;
        bb = Vector3.zero;
    }

    public override Vector3 GlobalPos(float t)
    {
		
        return Math3d.RotateThis(Math3d.CubicBezierLerp(a, aa, bb, b, t), -currentTransform.rotation.eulerAngles.y, currentTransform.position);
    }

	public override Vector3 GlobalPosWidth(float t, float width, bool isLeft)
	{
		Vector3 offsetStart = Vector3.zero;
		Vector3 offsetEnd = Vector3.zero;
		float currentLenght = 0;

		if (isLeft) 
		{
			offsetStart = Math3d.RotateThis(startWidth + width, -startRot.y-currentTransform.eulerAngles.y, Vector3.zero);
			offsetEnd = Math3d.RotateThis(endWidth + width, -endRot.y-currentTransform.eulerAngles.y, Vector3.zero);
		}
		else
		{
			offsetStart = Math3d.RotateThis(-startWidth - width, -startRot.y-currentTransform.eulerAngles.y, Vector3.zero);
			offsetEnd = Math3d.RotateThis(-endWidth - width, -endRot.y-currentTransform.eulerAngles.y, Vector3.zero);
		}

		return Math3d.RotateThis(Math3d.CubicBezierLerp(a + offsetStart, aa + offsetStart, bb + offsetEnd, b + offsetEnd, t), -currentTransform.rotation.eulerAngles.y, currentTransform.position);
	}

    #if UNITY_EDITOR
    [ContextMenu ("Recalculate")]
#endif
    public override void CalculateLenght()
    {
        Length = 0;
        for (int i = 0; i < 20; i++)
            Length += Vector3.Distance(Math3d.CubicBezierLerp(a, aa, bb, b, (float)i / 20.0f), Math3d.CubicBezierLerp(a, aa, bb, b, (float)(i + 1) / 20.0f));
        _count = Mathf.CeilToInt(Length / 4);
        _step = Length / _count;
        _step_n = _step / Length;
        currentTransform = transform;
        isCalculated = true;
        Main.instance = FindObjectOfType<Main>();

		nextY = b.y - a.y;
    }

	public override float CalculateLenghtLeft(float width)
	{
		Vector3 offsetLeftStart = Math3d.RotateThis(startWidth + width, -startRot.y-currentTransform.eulerAngles.y, Vector3.zero);
		Vector3 offsetLeftEnd = Math3d.RotateThis(endWidth + width, -endRot.y-currentTransform.eulerAngles.y, Vector3.zero);

		float leftLenght = 0;
		for (int i = 0; i < 20; i++)
			leftLenght += Vector3.Distance(Math3d.CubicBezierLerp(a + offsetLeftStart, aa + offsetLeftStart, bb + offsetLeftEnd, b + offsetLeftEnd, (float)i / 20.0f), Math3d.CubicBezierLerp(a + offsetLeftStart, aa + offsetLeftStart, bb + offsetLeftEnd, b + offsetLeftEnd, (float)(i + 1) / 20.0f));
		return leftLenght;
	}

	public override float CalculateLenghtRight(float width)
	{
		Vector3 offsetRightStart = Math3d.RotateThis(-startWidth - width, -startRot.y-currentTransform.eulerAngles.y, Vector3.zero);
		Vector3 offsetRightEnd = Math3d.RotateThis(-endWidth - width, -endRot.y-currentTransform.eulerAngles.y, Vector3.zero);

		float rightLenght = 0;
		for (int i = 0; i < 20; i++)
			rightLenght += Vector3.Distance(Math3d.CubicBezierLerp(a + offsetRightStart, aa + offsetRightStart, bb + offsetRightEnd, b + offsetRightEnd, (float)i / 20.0f), Math3d.CubicBezierLerp(a + offsetRightStart, aa + offsetRightStart, bb + offsetRightEnd, b + offsetRightEnd, (float)(i + 1) / 20.0f));
		return rightLenght;
	}

    public override float GetCurrentWidth(float percent)
    {
        return Mathf.Lerp(startWidth, endWidth, percent);
    }

    public override float GetCurrentVerge(float percent)
    {
        return Mathf.Lerp(startVerge, endVerge, percent);
    }


    public override bool UniformGlobalPosAdd(float t, float dist, out Vector3 point, out float addPercent, out float _dist, bool direction)
    {
        if (!isCalculated)
            CalculateLenght();

        point = Vector3.zero;
        addPercent = 0;
        _dist = 0;
        float dist2 = dist;

        //Debug.Log(t + " " + dist);

        if (direction)
        {
            float inc = _step_n;
            for (float i = t; i <= 1.0f; i += inc)
            {
                
                float step = Vector3.Distance(Math3d.CubicBezierLerp(a, aa, bb, b, i), Math3d.CubicBezierLerp(a, aa, bb, b, i + inc));
                //Debug.Log(i + " " + inc + " " + step);
                if (_dist + step > dist)
                {
                    
                    float factor = dist2 / step;

                    //Debug.Log(_dist + step + " " + dist + " " + factor);
                    point = Math3d.RotateThis(Math3d.CubicBezierLerp(a, aa, bb, b, i + inc * factor), -currentTransform.rotation.eulerAngles.y, currentTransform.position);
                    addPercent = i + inc * factor;

                    if (i + inc * factor > 1.0f)
                    {
                        //_dist += step;
                        return false;
                    }

                    return true;
                }
                else
                {
                    _dist += step;
                    dist2 -= step;
                }


            }
        }
        else
        {
            float inc = _step_n;


            for (float i = t; i >= 0.0f; i -= inc)
            {
                float step = Vector3.Distance(Math3d.CubicBezierLerp(a, aa, bb, b, i), Math3d.CubicBezierLerp(a, aa, bb, b, i - inc));

                //Debug.Log(i + " " + inc + " " + step);
                if (_dist + step > dist)
                {
                    float factor = dist2 / step;

                    //Debug.Log(_dist + step + " " + dist + " " + factor);
                    point = Math3d.RotateThis(Math3d.CubicBezierLerp(a, aa, bb, b, i - inc * factor), -currentTransform.rotation.eulerAngles.y, currentTransform.position);
                    addPercent = i - inc * factor;

                    if (i - inc * factor < 0.0f)
                    {
                        //_dist += step;
                        return false;
                    }

                    return true;
                }
                else
                {
                    _dist += step;
                    dist2 -= step;
                }
                   
            }
        }

        return false;
    }

    public override Vector3 GetAngle(float t)
    {
        if (!isCalculated)
            CalculateLenght();

        Vector3 result = Vector3.forward;

        Vector3 _dir;
        Vector3 _dir02;
        Vector3 _point;
        Vector3 _point02;

        float _length = Length * t;
        int number = Mathf.CeilToInt(_length / _step);
        float percent = _length % _step;
        _point = GlobalPos((float)number / _count);


        if (number < _count)
        {
            _point02 = GlobalPos((float)(number + 1) / _count);
            _dir02 = _point02 - _point;
        }
        else
        {
            _point = GlobalPos((float)(number - 1) / _count);
            _point02 = GlobalPos((float)(number) / _count);
            _dir02 = _point02 - _point;
            return _dir02;
        }

        if (number > 0)
            _dir = _point - GlobalPos((float)(number - 1) / _count);
        else
            _dir = Math3d.RotateThis(Vector3.forward, -startRot.y - currentTransform.eulerAngles.y, Vector3.zero);

        result = Vector3.Lerp(_dir, _dir02, percent / _step);

        return result;
    }

	public override Vector3 GetAngleWidth(float t, float width, bool isLeft)
	{

		Vector3 result = Vector3.forward;

		Vector3 _dir;
		Vector3 _dir02;
		Vector3 _point;
		Vector3 _point02;

		float currentLenght = (isLeft) ? CalculateLenghtLeft (width) : CalculateLenghtRight (width);
		float _countWidth = Mathf.CeilToInt(currentLenght / 4);
		float _stepWidth = currentLenght / _countWidth;
		float _step_nWidth = _stepWidth / currentLenght;


		float _length = currentLenght * t;
		int number = Mathf.CeilToInt(_length / _stepWidth);
		float percent = _length % _stepWidth;
		_point = GlobalPosWidth((float)number / _countWidth, width, isLeft);


		if (number < _countWidth)
		{
			_point02 = GlobalPosWidth((float)(number + 1) / _countWidth, width, isLeft);
			_dir02 = _point02 - _point;
		}
		else
		{
			_point = GlobalPosWidth((float)(number - 1) / _countWidth, width, isLeft);
			_point02 = GlobalPosWidth((float)(number) / _countWidth, width, isLeft);
			_dir02 = _point02 - _point;
			return _dir02;
		}

		if (number > 0)
			_dir = _point - GlobalPosWidth((float)(number - 1) / _countWidth, width, isLeft);
		else
			_dir = Math3d.RotateThis(Vector3.forward, -startRot.y - currentTransform.eulerAngles.y, Vector3.zero);

		result = Vector3.Lerp(_dir, _dir02, percent / _stepWidth);

		return result;
	}


    public override Vector3 GetPointOnCurve(Vector3 _pos)
    {
        if (!isCalculated)
            CalculateLenght();

        Vector3 Point = currentTransform.InverseTransformPoint(_pos);
        Vector3 result = Vector3.zero;
        float min = 1000.0f;
        float currentStep = 0;
        for (float i = 0; i < Length; i += _step)
        {
            Vector3 central = Math3d.CubicBezierLerp(a, aa, bb, b, Mathf.Lerp(i, i + _step, 0.5f) / Length);
            float dist = Vector3.Distance(Point, central);
            if (dist < min)
            {
                min = dist;
                currentStep = i;
            }
        }

        Vector3 start = Math3d.CubicBezierLerp(a, aa, bb, b, currentStep / Length);
        Vector3 end = Math3d.CubicBezierLerp(a, aa, bb, b, (currentStep + _step) / Length);
        Vector3 Dir = start - end;
        Dir.Normalize();

        Vector3 Left = 10.0f * Vector3.Cross(Dir, Vector3.up) + Point;
        Vector3 Right = -10.0f * Vector3.Cross(Dir, Vector3.up) + Point;

        Math3d.CrossPointLine(start, end, Left, Right, out result);

        float dist1 = Vector2.Distance(new Vector2(start.z, start.x), new Vector2(end.z, end.x));
        float dist2 = Vector2.Distance(new Vector2(start.z, start.x), new Vector2(result.z, result.x));



        float step = (dist2 / dist1);

        result.y = Mathf.Lerp(start.y, end.y, step);
        return currentTransform.TransformPoint(result);
    }

	public override Vector3 UniformGlobalPos(float t, float width, bool isLeft)
	{
		Vector3 offsetStart = Vector3.zero;
		Vector3 offsetEnd = Vector3.zero;
		float currentLenght = 0;

		if (isLeft) 
		{
			offsetStart = Math3d.RotateThis(startWidth + width, -startRot.y-currentTransform.eulerAngles.y, Vector3.zero);
			offsetEnd = Math3d.RotateThis(endWidth + width, -endRot.y-currentTransform.eulerAngles.y, Vector3.zero);
			currentLenght = CalculateLenghtLeft (width);
		}
		else
		{
			offsetStart = Math3d.RotateThis(-startWidth - width, -startRot.y-currentTransform.eulerAngles.y, Vector3.zero);
			offsetEnd = Math3d.RotateThis(-endWidth - width, -endRot.y-currentTransform.eulerAngles.y, Vector3.zero);
			currentLenght = CalculateLenghtRight (width);
		}

		Vector3 result = Vector3.zero;
		float _length = currentLenght * t;
		_count = Mathf.CeilToInt(currentLenght / 4);
		_step = currentLenght / _count;

		for (float i = 0; i < currentLenght; i += _step)
		{

			float step = Vector3.Distance(Math3d.CubicBezierLerp(a + offsetStart, aa + offsetStart, bb + offsetEnd, b + offsetEnd, i / currentLenght), Math3d.CubicBezierLerp(a + offsetStart, aa + offsetStart, bb + offsetEnd, b + offsetEnd, (i + _step) / currentLenght));
			if (step < _length)
				_length -= step;
			else
			{
				float percent = _length / step;
				result = Vector3.Lerp(Math3d.CubicBezierLerp(a + offsetStart, aa + offsetStart, bb + offsetEnd, b + offsetEnd, i / currentLenght), Math3d.CubicBezierLerp(a + offsetStart, aa + offsetStart, bb + offsetEnd, b + offsetEnd, (i + _step) / currentLenght), percent);
				return Math3d.RotateThis(result, -currentTransform.rotation.eulerAngles.y, currentTransform.position);
			}
		}

		return Math3d.RotateThis(b + offsetEnd, -currentTransform.rotation.eulerAngles.y, currentTransform.position);

	}

    public override float GetStep(Vector3 _pos)
    {
        
        if (!isCalculated)
            CalculateLenght();


        Vector3 Point = currentTransform.InverseTransformPoint(_pos);
        Vector3 result = Vector3.zero;

        float min = 1000.0f;
        float currentStep = 0;
		for (float i = 0; i < Length; i += _step)
        {
            Vector3 central = Math3d.CubicBezierLerp(a, aa, bb, b, Mathf.Lerp(i, i + _step, 0.5f) / Length);
            float dist = Vector3.Distance(Point, central);
            if (dist < min)
            {
                min = dist;
                currentStep = i;

            }
        }

        Vector3 start = Math3d.CubicBezierLerp(a, aa, bb, b, currentStep / Length);
        Vector3 end = Math3d.CubicBezierLerp(a, aa, bb, b, (currentStep + _step) / Length);
        Vector3 Dir = start - end;
        Dir.Normalize();

        Vector3 Left = 10.0f * Vector3.Cross(Dir, Vector3.up) + Point;
        Vector3 Right = -10.0f * Vector3.Cross(Dir, Vector3.up) + Point;

        Math3d.CrossPointLine(start, end, Left, Right, out result);

        float dist1 = Vector2.Distance(new Vector2(start.z, start.x), new Vector2(end.z, end.x));
        float dist2 = Vector2.Distance(new Vector2(start.z, start.x), new Vector2(result.z, result.x));

        float step = (dist2 / dist1);

		float t = currentStep / Length + step * _step_n;

        return t;
    }

	#if UNITY_EDITOR
	[ContextMenu("GetStep")]
	void CheckGetStep()
	{
		Locators.CreateLocator(GlobalPos(GetStep(Selection.activeGameObject.transform.position)), "S");

	}
	#endif

	[ContextMenu("GlobalPos")]
	void CheckGetPos()
	{
		Locators.CreateLocator(GlobalPos(EditorPercent), "G");

	}

	public override CameraDot GetNext(float currentPercent)
	{
		if (cameraDots != null)
			for (int i = 0; i < cameraDots.Count; i++) 
			{
				if (cameraDots [i].percent > currentPercent)
					return cameraDots [i];
			}
		return null;
	}

	public override CameraDot GetPrevious(float currentPercent)
	{
		if (cameraDots != null) {
			for (int i = cameraDots.Count-1; i >= 0; i--) {
				Debug.Log ("F " + i.ToString() + " " + cameraDots [i].percent + " " + currentPercent);
				if (cameraDots [i].percent < currentPercent)
					return cameraDots [i];
			}

		}
		return null;
	}
	#if UNITY_EDITOR
    public override void CreateLocators()
    {
        if (!isCalculated)
            CalculateLenght();

        if (a_sphere)
            
            DestroyImmediate(a_sphere.gameObject);
        a_sphere = Locators.CreateLocatorLocal(a, Quaternion.Euler(startRot), transform, "a").transform;

        if (aa_sphere)
            DestroyImmediate(aa_sphere.gameObject);
        aa_sphere = Locators.CreateLocatorLocal(aa, Quaternion.identity, transform, "aa").transform;
        

        if (b_sphere)
            DestroyImmediate(b_sphere.gameObject);
        b_sphere = Locators.CreateLocatorLocal(b, Quaternion.Euler(endRot), transform, "b").transform;

        if (bb_sphere)
            DestroyImmediate(bb_sphere.gameObject);
        bb_sphere = Locators.CreateLocatorLocal(bb, Quaternion.identity, transform, "bb").transform;
    }

	public override void DeleteLocators()
	{
		if (a_sphere)
			DestroyImmediate(a_sphere.gameObject);
		if (aa_sphere)
			DestroyImmediate(aa_sphere.gameObject);
		if (b_sphere)
			DestroyImmediate(b_sphere.gameObject);
		if (bb_sphere)
			DestroyImmediate(bb_sphere.gameObject);
	}


    void OnDrawGizmos()
    {
        if (!isVisible)
            return;

        if (!isCalculated)
            CalculateLenght();

        Gizmos.color = Color.red;
        
        //Vector3 global_a = Main.instance.curveController.TransformPoint(currentTransform.TransformPoint(a));
        //Vector3 global_aa = Main.instance.curveController.TransformPoint(currentTransform.TransformPoint(aa));
       // Vector3 global_bb = Main.instance.curveController.TransformPoint(currentTransform.TransformPoint(bb));
        //Vector3 global_b = Main.instance.curveController.TransformPoint(currentTransform.TransformPoint(b));

        Vector3 global_a = currentTransform.TransformPoint(a);
        Vector3 global_aa = currentTransform.TransformPoint(aa);
        Vector3 global_bb = currentTransform.TransformPoint(bb);
        Vector3 global_b = currentTransform.TransformPoint(b);


        Gizmos.DrawLine(global_a,global_aa);
        Gizmos.DrawLine(global_b,global_bb);
        
        Vector3 offsetLeftStart = Math3d.RotateThis(startWidth, -startRot.y-currentTransform.eulerAngles.y, Vector3.zero);
       Vector3 offsetRightStart = Math3d.RotateThis(-startWidth, -startRot.y-currentTransform.eulerAngles.y, Vector3.zero);
       Vector3 offsetLeftEnd = Math3d.RotateThis(endWidth, -endRot.y-currentTransform.eulerAngles.y, Vector3.zero);
        Vector3 offsetRightEnd = Math3d.RotateThis(-endWidth, -endRot.y-currentTransform.eulerAngles.y, Vector3.zero);

        Vector3 vergeLeftStart = Math3d.RotateThis(startVerge, -startRot.y-currentTransform.eulerAngles.y, Vector3.zero);
       Vector3 vergeRightStart = Math3d.RotateThis(-startVerge, -startRot.y-currentTransform.eulerAngles.y, Vector3.zero);
       Vector3 vergeLeftEnd = Math3d.RotateThis(endVerge, -endRot.y-currentTransform.eulerAngles.y, Vector3.zero);
        Vector3 vergeRightEnd = Math3d.RotateThis(-endVerge, -endRot.y-currentTransform.eulerAngles.y, Vector3.zero);

        for (float i = 0; i < (Length - 0.001f); i += _step)
        {

            Gizmos.color = Color.white;
            Gizmos.DrawLine(Math3d.CubicBezierLerp(global_a, global_aa, global_bb, global_b, i / Length), Math3d.CubicBezierLerp(global_a, global_aa, global_bb, global_b, (i + _step) / Length));

            Gizmos.color = Color.blue;
    
            Gizmos.DrawLine(Math3d.CubicBezierLerp(global_a + offsetLeftStart, global_aa + offsetLeftStart, global_bb + offsetLeftEnd, global_b + offsetLeftEnd,i / Length), 
                Math3d.CubicBezierLerp(global_a + offsetLeftStart, global_aa + offsetLeftStart, global_bb + offsetLeftEnd, global_b + offsetLeftEnd, (i + _step) / Length));
           
            Gizmos.DrawLine(Math3d.CubicBezierLerp(global_a + offsetRightStart, global_aa + offsetRightStart, global_bb + offsetRightEnd, global_b + offsetRightEnd,i / Length), 
                Math3d.CubicBezierLerp(global_a + offsetRightStart, global_aa + offsetRightStart, global_bb + offsetRightEnd, global_b + offsetRightEnd, (i + _step) / Length));

            Gizmos.color = Color.yellow;
                
            Gizmos.DrawLine(Math3d.CubicBezierLerp(global_a + vergeLeftStart, global_aa + vergeLeftStart, global_bb + vergeLeftEnd, global_b + vergeLeftEnd,i / Length), 
                Math3d.CubicBezierLerp(global_a + vergeLeftStart, global_aa + vergeLeftStart, global_bb + vergeLeftEnd, global_b + vergeLeftEnd, (i + _step) / Length));
           
            Gizmos.DrawLine(Math3d.CubicBezierLerp(global_a + vergeRightStart, global_aa + vergeRightStart, global_bb + vergeRightEnd, global_b + vergeRightEnd,i / Length), 
                Math3d.CubicBezierLerp(global_a + vergeRightStart, global_aa + vergeRightStart, global_bb + vergeRightEnd, global_b + vergeRightEnd, (i + _step) / Length));
        }
        Gizmos.color = Color.white;
        if (a_sphere)
        {
            a = a_sphere.localPosition;
            startRot = a_sphere.transform.localRotation.eulerAngles;
        }
        if (b_sphere)
        {
            b = b_sphere.localPosition;
            endRot = b_sphere.transform.localRotation.eulerAngles;
        }
        if (aa_sphere)
            aa = aa_sphere.localPosition;

        if (bb_sphere)
            bb = bb_sphere.localPosition;

		if (cameraDots != null)
			for (int i = 0; i < cameraDots.Count; i++)
			{
				Gizmos.color = Color.white;	
				Gizmos.DrawWireSphere (GlobalPos (cameraDots [i].percent), 3.0f);

				//Gizmos.color = Color.green;	
				//Gizmos.DrawWireSphere (GlobalPos (cameraDots [i].percent - cameraDots [i].supportPercentPrevious), 1.5f);
			}

    }
#endif

    /*
  public Vector3 GetPointOnCurve(Vector3 _pos, float offset)
  {
      if (!isCalculated)
          CalculateLenght();

      Vector3 Point = currentTransform.InverseTransformPoint(_pos);
      Vector3 result = Vector3.zero;
      float min = 1000.0f;
      float currentStep = 0;
      for (float i = 0; i < Length; i += _step)
      {
          Vector3 central = Math3d.CubicBezierLerp(a, aa, bb, b, Mathf.Lerp(i, i + _step, 0.5f) / Length);
          float dist = Vector3.Distance(Point, central);
          if (dist < min)
          {
              min = dist;
              currentStep = i;
          }
      }

      float _dist = 0;
      float addPercent = 0;
      float Start = currentStep/2 * _step_n;
      Vector3 point = Vector3.zero;

      for (float i = Start; i > 0.001f; i -= _step_n)
      {

          float step = Vector3.Distance(Math3d.CubicBezierLerp(a, aa, bb, b, i), Math3d.CubicBezierLerp(a, aa, bb, b, i - _step_n));
          _dist += step;

          if (_dist > offset)
          {
              point = Math3d.RotateThis(Math3d.CubicBezierLerp(a, aa, bb, b, i), -currentTransform.rotation.eulerAngles.y, currentTransform.position);
              addPercent = i;
              break;
          }
      }

      Debug.Log(currentStep + " " + addPercent);

      Vector3 start = Math3d.CubicBezierLerp(a, aa, bb, b, addPercent / Length);
      Vector3 end = Math3d.CubicBezierLerp(a, aa, bb, b, (addPercent + _step) / Length);
      Vector3 Dir = start - end;
      Dir.Normalize();

      Vector3 Left = 10.0f * Vector3.Cross(Dir, Vector3.up) + point;
      Vector3 Right = -10.0f * Vector3.Cross(Dir, Vector3.up) + point;

      Math3d.CrossPointLine(start, end, Left, Right, out result);

      float dist1 = Vector2.Distance(new Vector2(start.z, start.x), new Vector2(end.z, end.x));
      float dist2 = Vector2.Distance(new Vector2(start.z, start.x), new Vector2(result.z, result.x));

      float stepF = dist2 / dist1;


      result.y = Mathf.Lerp(start.y, end.y, stepF);
      return currentTransform.TransformPoint(result);



  }
   */

    /*
   public bool GetCorrectStep(Vector3 _pos, float _param, out float percent)
   {
       if (!isCalculated)
           CalculateLenght();

       percent = 0;
       Vector3 result = Vector3.zero;

       Vector3 _dir = GetAngle(_param);
       _dir.Normalize();

       float angle = Math3d.GetAngle(_dir.z, _dir.x, Vector3.forward.z, Vector3.forward.x);
       Vector3 _right = Math3d.RotateThis(new Vector3(10.0f, 0.0f, 0.0f), angle, _pos);
       Vector3 _left = Math3d.RotateThis(new Vector3(-10.0f, 0.0f, 0.0f), angle, _pos);
       float startStep = 0.0f;
       float endStep = 0.0f;

       float step = 1.0f / _count;
       float half = step / 2;

       for (float i = half; i < 1.00f; i += step)
       {

           startStep = i - half;
           endStep = i + half;
           if (endStep > 1.0f)
               endStep = 1.0f;

           Vector3 _startStep = UniformGlobalPos(startStep);
           Vector3 _endStep = UniformGlobalPos(endStep);
           
           //Debug.DrawLine(_startStep, _endStep, Color.red);
           if (Math3d.CrossPoint(_right, _left, _startStep, _endStep, out result))
           {
               result.y = (_right.y +_left.y +_startStep.y +_endStep.y)/4;
               float dist = Vector3.Distance(_startStep, _endStep);
               float dist2 = Vector3.Distance(_startStep, result);
               //Debug.Log(i + " " + dist2 + " " + dist + " " + step);
               percent = i + dist2 / dist * step;

               

               return true;
           }
           
           

       }

       Debug.Log("False Full");
       return false;


   }
    
   public Vector3 GetPointOnCurve(Vector3 _pos, Vector3 _oldPos, float _param, bool stop)
   {
       if (!isCalculated)
           CalculateLenght();

       Vector3 result = _oldPos;
       Vector3 _dir = GetAngle(_param);
       _dir.Normalize();
 
       float angle = Math3d.GetAngle(_dir.z, _dir.x, Vector3.forward.z, Vector3.forward.x);
       Vector3 _right = Math3d.RotateThis(new Vector3(10.0f, 0.0f, 0.0f), angle, _pos);
       Vector3 _left = Math3d.RotateThis(new Vector3(-10.0f, 0.0f, 0.0f), angle, _pos);
       ////Debug.DrawLine(_right, _left, Color.gray);
       ////Debug.DrawLine(_pos, _dir * 2.0f + _pos, Color.black);


       
       float startStep = 0.0f;
       float endStep = 0.0f;

       float step = 1.0f / _count;
       float half = step / 2;

       for (float i = half; i < 1.00f; i += step)
       {

           startStep = i - half;
           endStep = i + half;
           if (endStep > 1.0f)
               endStep = 1.0f;

           Vector3 _startStep = UniformGlobalPos(startStep);
           Vector3 _endStep = UniformGlobalPos(endStep);

          // //Debug.DrawLine(_startStep, _endStep, Color.red);
              if (Math3d.CrossPoint(_right, _left, _startStep, _endStep, out result))
              {
                  //Debug.Log(result + " " + _startStep + " " + _endStep + " " + _right + " " + _left + " " + _dir + " " + angle + " " + _param + " " + i + " ");
                  //oldStep = i;

                  _oldPos.y = 0;
                  //Debug.Log(result + " " + _oldPos);
                  _oldPos.y = 0;
                  _distance = Vector3.Distance(result, _oldPos);
                  _oldPos = result;
                  _oldPos.y = 0;
                  return result;
              }
          

       }

       if (!stop)
       {

           Debug.LogWarning("Non perfect border");
           //Debug.Log(step + " " + UniformGlobalPos(startStep) + " " + UniformGlobalPos(endStep) + " " + _right + " " + _left + " " + _dir + " " + angle + " " + _param);
           Plane _block = myTransform.GetComponent<Plane>();
           Transform _nextTr = GameObject.Find("Main").GetComponent<Main>().GetBlock(_block.number + 1);
           if (_nextTr != null)
           {
               Plane _nextBlock = _nextTr.GetComponent<Plane>();
               Spline _spline = _nextBlock.GetComponent<Spline>();
               //GameObject.Find("Main").GetComponent<Main>()._timescale = 0;
               return _spline.GetPointOnCurve(_pos, _oldPos, 0.0f, true);
           }
           else
               return _oldPos;

       }
          

       
       //Debug.Log("STOP " + UniformGlobalPos(startStep) + " " + UniformGlobalPos(endStep) + " " + _right + " " + _left + " " + _dir + " " + angle + " " + _param + " " + step + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
       //Debug.Log(startStep + " " + endStep);
       //Transform _startStepT = Locators.CreateLocator(UniformGlobalPos(startStep), "startStep");
       //Transform _endStepT = Locators.CreateLocator(UniformGlobalPos(endStep), "endStep");
       //Transform _Right = Locators.CreateLocator(_right, "Right");
       //Transform _Left = Locators.CreateLocator(_left, "Left");
       //GameObject CrossTester = new GameObject();
       //CrossTester.AddComponent<testCross>();
       //CrossTester.GetComponent<testCross>().A = _Right;
       //CrossTester.GetComponent<testCross>().B = _Left;
       //CrossTester.GetComponent<testCross>().C = _startStepT;
       //CrossTester.GetComponent<testCross>().D = _endStepT;
       //transform.GetComponent<Plane>().Main.instance._timescale = 0;
       
       
       
       //Debug.Log("Procedural Path");
       //return ProceduralPosition(_oldPos, _dir);
       return _oldPos;
       
       
   }
    
   Vector3 ProceduralPosition(Vector3 _oldPos, Vector3 _direction)
   {
       Vector3 result = _oldPos + _direction * _distance;
       Debug.Log(result + " " + _oldPos + " " + _direction + " " + _distance);
       return result;

   }
   */

    /*
   public void GetDotes()
   {
       CalculateLenght();

       for (float i = 0; i < 1.00f - 0.001f; i += _step / Length)
       {
           Locators.CreateLocator(UniformGlobalPos(i), Quaternion.identity, transform, "loc " + i.ToString());
           
       }
       Locators.CreateLocator(UniformGlobalPos(1.0f), Quaternion.identity, transform, "loc 1");
   }
   
   public float GetStep2(Vector3 _pos)
    {

        if (!isCalculated)
            CalculateLenght();

        float _result = -1.0f;

        Vector2 _pos2d;

        _pos2d.x = _pos.x;
        _pos2d.y = _pos.z;

        Vector2 _left = Math3d.RotateThis2d(40.0f, -startRot.y - myTransform.eulerAngles.y, myTransform.TransformPoint(a));
        Vector2 _right = Math3d.RotateThis2d(-40.0f, -startRot.y - myTransform.eulerAngles.y, myTransform.TransformPoint(a));
        Vector2 _left2 = Math3d.RotateThis2d(40.0f, -endRot.y - myTransform.eulerAngles.y, myTransform.TransformPoint(b));
        Vector2 _right2 = Math3d.RotateThis2d(-40.0f, -endRot.y - myTransform.eulerAngles.y, myTransform.TransformPoint(b));

        float _h1 = Math3d.TriangleHigh(_left, _right, _pos2d);
        float _h2 = Math3d.TriangleHigh(_left2, _right2, _pos2d);

        float _length = _h1 + _h2;

        _result = _h1 / _length;


        return _result;

    }
    




    public void Create5Bezier()
   {
 #if UNITY_EDITOR
   
       Vector3 Pos = Selection.activeGameObject.transform.position;
       Debug.Log(GetStep(Pos));
       //Debug.Log(GetStep2(Pos));   
       
         
   
#endif
       }
    

    */

    
  
   

    /*
   public bool CamPosAdd(float t, float dist, out Vector3 point, out float addPercent, out float _dist)
   {
       if (!isCalculated)
           CalculateLenght();

       point = Vector3.zero;
       addPercent = 0;
       _dist = 0;

          float _start = Mathf.FloorToInt(_count * t) * _step_n;

           for (float i = _start; i > 0.001f; i -= _step_n)
           {

               float step = Vector3.Distance(Math3d.CubicBezierLerp(a, aa, bb, b, i), Math3d.CubicBezierLerp(a, aa, bb, b, i - _step_n));
               _dist += step;

               if (_dist > dist)
               {
                   point = Math3d.RotateThis(Math3d.CubicBezierLerp(a, aa, bb, b, i), -currentTransform.rotation.eulerAngles.y, currentTransform.position);
                   addPercent = i;
                   return true;
               }
           }
       

       return false;

   }
   */

    /*
   public void CheckPercent(float percent)
    {
    GameObject temp = GameObject.CreatePrimitive(PrimitiveType.Sphere);
    temp.transform.position = UniformGlobalPos(percent);
    }
    
    #if UNITY_EDITOR

    public void CheckCorrect(float percent)
    {
       Debug.Log(GetStep(UnityEditor.Selection.activeGameObject.transform.position));

    }
#endif

  */

    /*
      public bool GetPointOnCurve(Vector3 _pos, float _param, out Vector3 result)
      {
          if (!isCalculated)
              CalculateLenght();

          result = Vector3.zero;
          Vector3 _dir = GetAngle(_param);
          _dir.Normalize();

          float angle = Math3d.GetAngle(_dir.z, _dir.x, Vector3.forward.z, Vector3.forward.x);
          Vector3 _right = Math3d.RotateThis(new Vector3(10.0f, 0.0f, 0.0f), angle, _pos);
          Vector3 _left = Math3d.RotateThis(new Vector3(-10.0f, 0.0f, 0.0f), angle, _pos);
          float startStep = 0.0f;
          float endStep = 0.0f;

          float step = 1.0f / _count;
          float half = step / 2;

          for (float i = half; i < 1.00f; i += step)
          {

              startStep = i - half;
              endStep = i + half;
              if (endStep > 1.0f)
                  endStep = 1.0f;

              Vector3 _startStep = UniformGlobalPos(startStep);
              Vector3 _endStep = UniformGlobalPos(endStep);


              //Debug.DrawLine(_startStep, _endStep, Color.red);
              if (Math3d.CrossPoint(_right, _left, _startStep, _endStep, out result))
              {
                  RaycastHit Hit;
                  if (Math3d.RayCastGroundDown(new Vector3(result.x, 500.0f, result.z), out Hit))
                      result = Hit.point;
                  return true;
              }



          }

       
          return false;


      }

    */

}