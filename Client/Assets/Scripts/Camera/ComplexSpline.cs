﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class ComplexSpline : Spline
{
    public Spline[] splines;
    public float EditorPercent;
	public List<CameraDot> cameraDots;

	#if UNITY_EDITOR
	public bool isOpen = false;
	public bool isCreateLocators = false;
	#endif

    public override Spline[] getSplines()
    {
        return splines;
    }

	public override float CalculateLenghtLeft(float width)
	{
		float complexLeftLenght = 0;
		for (int i = 0; i < splines.Length; i++) 
		{
			complexLeftLenght += splines [i].CalculateLenghtLeft (width);
		}
		return complexLeftLenght;
	}

	public override float CalculateLenghtRight(float width)
	{
		float complexRightLenght = 0;
		for (int i = 0; i < splines.Length; i++) 
		{
			complexRightLenght += splines [i].CalculateLenghtRight (width);
		}
		return complexRightLenght;
	}

	public override CameraDot[] GetCameraDots()
	{
		return cameraDots.ToArray ();
	}
	public override Vector3 getRot(float t)
	{
		int select = 0;
		float percent = 0;
		GetLocalSpline(t, out select, out percent);

		return splines [select].getRot (percent);
	}

    public override Vector3 getStartRot(float t)
    {
        int select = 0;
        float percent = 0;
        GetLocalSpline(t, out select, out percent);

        return splines[select].startRot;
    }

    public override Vector3 getEndRot(float t)
    {
        int select = 0;
        float percent = 0;
        GetLocalSpline(t, out select, out percent);

        return splines[select].endRot;
    }

	public override CameraDot GetNext(float currentPercent)
	{
		if (cameraDots != null)
		{
			for (int i = 0; i < cameraDots.Count; i++) 
			{
				if (cameraDots [i].percent > currentPercent)
					return cameraDots [i];
			}

			/*
			//return null;
			int select = 0;
			float percent = 0;
			GetLocalSpline(currentPercent, out select, out percent);

			CameraDot temp = splines [select].GetNext (percent);
			if (temp != null)
				return temp;
			else

			for (int i = 0; i < cameraDots.Count; i++) 
			{
					if (cameraDots [i].percent > percent)
					return cameraDots [i];
			}
			*/
		}
		return null;
	}

	public override CameraDot GetPrevious(float currentPercent)
	{
		if (cameraDots != null) {
			int select = 0;
			float percent = 0;
			for (int i = cameraDots.Count - 1; i >= 0; i--) {
				if (cameraDots [i].percent < currentPercent)
					return cameraDots [i];
			}
		}
		return null;
	}

    public override float CheckCamProcess(float t)
    {
        int select = 0;
        float percent = 0;
        GetLocalSpline(t, out select, out percent);
        return percent;
    }

	public float ToGlobal(int select, float t)
	{
		float _length = 0;
		for (int i=0; i<select; i++)
		{
			_length += splines[i].Length;
		}
		_length += splines [select].Length * t;
		return _length / Length;
	}

#if UNITY_EDITOR
[ContextMenu ("Recalculate")]
#endif
    public override void CalculateLenght()
    {

        Length = 0;
        for (int i = 0; i < splines.Length; i++)
            Length += splines[i].Length;
        isCalculated = true;

        a = splines[0].a;
        startRot = splines[0].startRot;
        b = splines[splines.Length - 1].b;
        endRot = splines[splines.Length - 1].endRot;

        startWidth = splines[0].startWidth;
        endWidth = splines[splines.Length - 1].endWidth;

		startVerge = splines[0].startVerge;
		endVerge = splines[splines.Length - 1].endVerge;

		nextY = b.y - a.y;
}

    public void GetLocalSpline(float t, out int select, out float percent)
    {
        if (!isCalculated)
            CalculateLenght();


        float _length = 0;
        select = -1;
        float t_current = t;
        for (int i = 0; i < splines.Length; i++)
        {
            _length += splines[i].Length;
            if (_length / Length >= t)
            {
                select = i;
                float percentCurrent = splines[i].Length / Length;
                percent = t_current / percentCurrent;
                return;
            }
            else
                t_current -= splines[i].Length / Length;
           
        }
        select = splines.Length-1;
        percent = t_current / (splines[select].Length / Length);
        return;
    }

	public void GetLocalSplineWidth(float t, out int select, out float percent, float width, bool isLeft)
	{
		float sideLenght = (isLeft) ? CalculateLenghtLeft (width) : CalculateLenghtRight (width);
		float _length = 0;
		select = -1;
		float t_current = t;
		for (int i = 0; i < splines.Length; i++)
		{
			float currentSplineLenght = (isLeft) ? splines [i].CalculateLenghtLeft (width) : splines [i].CalculateLenghtRight (width);
			_length += currentSplineLenght;
			if (_length / sideLenght >= t)
			{
				select = i;
				float percentCurrent = currentSplineLenght / sideLenght;
				percent = t_current / percentCurrent;
				return;
			}
			else
				t_current -= currentSplineLenght / sideLenght;

		}
		select = splines.Length-1;
		percent = t_current / (((isLeft) ? splines [select].CalculateLenghtLeft (width) : splines [select].CalculateLenghtRight (width)) / sideLenght);
		return;
	}

#if UNITY_EDITOR
	[ContextMenu("GetPointOnCurve")]
	void CheckGetPointOnCurve()
    {
		Locators.CreateLocator(GetPointOnCurve(Selection.activeGameObject.transform.position), "F");

    }

	[ContextMenu("GetStep")]
	void CheckGetStep()
	{
		Locators.CreateLocator(GlobalPos(GetStep(Selection.activeGameObject.transform.position)), "S");

	}

    [ContextMenu("GlobalPos")]
    void CheckGetPos()
    {
        Locators.CreateLocator(GlobalPos(EditorPercent), "G");

    }

	[ContextMenu("UniformGlobalPosAdd")]
	void CheckUniformGlobalPosAdd()
	{
		Vector3 point = Vector3.zero;
		float addPercent = 0;
		float _dist = 0;
		if (UniformGlobalPosAdd (GetStep (Selection.activeGameObject.transform.position), 3.0f, out point, out addPercent, out _dist, true))
			Locators.CreateLocator(point, "U " + addPercent);

	}

    [ContextMenu("UniformGlobalPosAdd")]
    void CheckUniformGlobalPosAdd2()
    {
        Vector3 point = Vector3.zero;
        float addPercent = 0;
        float _dist = 0;
        if (UniformGlobalPosAdd(EditorPercent, 3.0f, out point, out addPercent, out _dist, true))
            Locators.CreateLocator(point, "UE " + addPercent);

    }

	[ContextMenu("GetAngle")]
	void CheckGetAngle()
	{
		float t = GetStep (Selection.activeGameObject.transform.position);
		Transform newT = Locators.CreateLocator(GlobalPos(t), "S");
		newT.LookAt (newT.transform.position + GetAngle (t));
	}
#endif
    public override Vector3 GlobalPos(float t)
    {
        int select = 0;
        float percent = 0;
        GetLocalSpline(t, out select, out percent);

        return splines[select].GlobalPos(percent);
    }

	public override Vector3 GlobalPosWidth(float t, float width, bool isLeft)
	{
		int select = 0;
		float percent = 0;
		GetLocalSplineWidth(t, out select, out percent, width, isLeft);

		return splines[select].GlobalPosWidth(percent, width, isLeft);
	}

	public override Vector3 UniformGlobalPos(float t, float width, bool isLeft)
	{
		int select = 0;
		float percent = 0;
		GetLocalSplineWidth(t, out select, out percent, width, isLeft);

		return splines[select].UniformGlobalPos(percent, width, isLeft);
	}

    public override float GetCurrentWidth(float t)
    {
        int select = 0;
        float percent = 0;
        GetLocalSpline(t, out select, out percent);
        return splines[select].GetCurrentWidth(percent);
    }

    public override float GetCurrentVerge(float t)
    {
        int select = 0;
        float percent = 0;
        GetLocalSpline(t, out select, out percent);
        return splines[select].GetCurrentVerge(percent);
    }

    public override Vector3 GetAngle(float t)
    {
        int select = 0;
        float percent = 0;
        GetLocalSpline(t, out select, out percent);

        return splines[select].GetAngle(percent);
    }

	public override Vector3 GetAngleWidth(float t, float width, bool isLeft)
	{
		int select = 0;
		float percent = 0;
		GetLocalSplineWidth(t, out select, out percent, width, isLeft);

		return splines[select].GetAngleWidth(percent, width, isLeft);
	}

    public override float GetStep(Vector3 _pos)
    {
        if (!isCalculated)
            CalculateLenght();
        Vector3 result = Vector3.zero;
        Vector3 Point = currentTransform.InverseTransformPoint(_pos);
        float min = 1000.0f;
        float currentStep = 0;
        int currentSpline = -1;
        for (int j = 0; j < splines.Length; j++)
        {
            for (float i = 0; i < splines[j].Length; i += splines[j]._step)
            {
                Vector3 central = Math3d.CubicBezierLerp(splines[j].a, splines[j].aa, splines[j].bb, splines[j].b, Mathf.Lerp(i, i + splines[j]._step, 0.5f) / splines[j].Length);
                float dist = Vector3.Distance(Point, central);
                if (dist < min)
                {
                    min = dist;
                    currentStep = i;
                    currentSpline = j;
                }
            }
        }

        Vector3 start = Math3d.CubicBezierLerp(splines[currentSpline].a, splines[currentSpline].aa, splines[currentSpline].bb, splines[currentSpline].b, currentStep / splines[currentSpline].Length);
        Vector3 end = Math3d.CubicBezierLerp(splines[currentSpline].a, splines[currentSpline].aa, splines[currentSpline].bb, splines[currentSpline].b, (currentStep + splines[currentSpline]._step) / splines[currentSpline].Length);
        Vector3 Dir = start - end;
        Dir.Normalize();

        Vector3 Left = 10.0f * Vector3.Cross(Dir, Vector3.up) + Point;
        Vector3 Right = -10.0f * Vector3.Cross(Dir, Vector3.up) + Point;

        Math3d.CrossPointLine(start, end, Left, Right, out result);

        float dist1 = Vector2.Distance(new Vector2(start.z, start.x), new Vector2(end.z, end.x));
        float dist2 = Vector2.Distance(new Vector2(start.z, start.x), new Vector2(result.z, result.x));

		float step = (dist2 / dist1);

        float allStep = 0;
		for (int j = 0; j < currentSpline; j++)
			allStep += splines [j].Length / Length;

		float splineFactor = splines [currentSpline].Length / Length;
		float last = ((currentStep) / splines [currentSpline].Length + splines [currentSpline]._step_n * step) * splineFactor;

		return (allStep + last) ;


    }

    public override Vector3 GetPointOnCurve(Vector3 _pos)
    {
        if (!isCalculated)
            CalculateLenght();

        Vector3 Point = currentTransform.InverseTransformPoint(_pos);
        Vector3 result = Vector3.zero;
        float min = 1000.0f;
        float currentStep = 0;
        int currentSpline = -1;
        for (int j = 0; j < splines.Length; j++)
        {
            for (float i = 0; i < splines[j].Length; i += splines[j]._step)
            {
                Vector3 central = Math3d.CubicBezierLerp(splines[j].a, splines[j].aa, splines[j].bb, splines[j].b, Mathf.Lerp(i, i + splines[j]._step, 0.5f) / splines[j].Length);
                float dist = Vector3.Distance(Point, central);
                if (dist < min)
                {
                    min = dist;
                    currentStep = i;
                    currentSpline = j;
                }
            }
        }

        Vector3 start = Math3d.CubicBezierLerp(splines[currentSpline].a, splines[currentSpline].aa, splines[currentSpline].bb, splines[currentSpline].b, currentStep / splines[currentSpline].Length);
        Vector3 end = Math3d.CubicBezierLerp(splines[currentSpline].a, splines[currentSpline].aa, splines[currentSpline].bb, splines[currentSpline].b, (currentStep + splines[currentSpline]._step) / splines[currentSpline].Length);
        Vector3 Dir = start - end;
        Dir.Normalize();

        Vector3 Left = 10.0f * Vector3.Cross(Dir, Vector3.up) + Point;
        Vector3 Right = -10.0f * Vector3.Cross(Dir, Vector3.up) + Point;

        Math3d.CrossPointLine(start, end, Left, Right, out result);

        float dist1 = Vector2.Distance(new Vector2(start.z, start.x), new Vector2(end.z, end.x));
        float dist2 = Vector2.Distance(new Vector2(start.z, start.x), new Vector2(result.z, result.x));

        float step = (dist2 / dist1);

        result.y = Mathf.Lerp(start.y, end.y, step);
        return currentTransform.TransformPoint(result);
    }


    public override bool UniformGlobalPosAdd(float t, float dist, out Vector3 point, out float addPercent, out float _dist, bool direction)
    {
        if (!isCalculated)
            CalculateLenght();


        point = Vector3.zero;
        addPercent = 0;
        _dist = 0;

        int select;
        float percent;


        GetLocalSpline(t, out select, out percent);
        //Debug.Log(splines[select] + " " + percent);
		//Debug.Log (t + " " + select + " " + percent);

		if (splines [select].UniformGlobalPosAdd (percent, dist, out point, out addPercent, out _dist, direction)) 
		{
			//Debug.Log (select + " " + percent + " " + dist + " " + point + " " + addPercent + " " + _dist + " " + direction);
			addPercent = ToGlobal (select, addPercent);
            //Debug.Log(addPercent + " True" + point);
			return true;
		} 
		else 
			if (splines.Length - 1 > select && direction || select > 0 && !direction) 
		    {
            
			select += ((direction) ? 1 : -1);
            //Debug.Log(splines[select].name + " " + select);

				splines [select].UniformGlobalPosAdd (direction ? 0 : 1.0f, dist - _dist, out point, out addPercent, out _dist, direction);
			    addPercent = ToGlobal (select, addPercent);
                //Debug.Log(addPercent + " True Second");
			return true;
		    } 
			    else 
		    {
            //Debug.Log(addPercent + " False");
			addPercent = ToGlobal (select, addPercent);
			return false;
            }
		}

    
	#if UNITY_EDITOR
	void OnDrawGizmos()
	{

		if (cameraDots != null)
			for (int i = 0; i < cameraDots.Count; i++)
			{
				Gizmos.color = Color.white;	
				Gizmos.DrawWireSphere (GlobalPos (cameraDots [i].percent), 3.0f);

				//Gizmos.color = Color.green;	
				//Gizmos.DrawWireSphere (GlobalPos (cameraDots [i].percent - cameraDots [i].supportPercentPrevious), 1.5f);
			}
	}




	public override void CreateLocators ()
	{
		for (int i = 0; i < splines.Length; i++)
			splines[i].CreateLocators ();
		isCreateLocators = true;
	}

	public override void DeleteLocators()
	{
		for (int i = 0; i < splines.Length; i++)
			splines[i].DeleteLocators ();
		isCreateLocators = false;
	}
	#endif
}
