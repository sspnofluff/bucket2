﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class CameraDot
{
	public float percent;
	public float lenght;
	public int number;
	public Vector2 AxisBendSize;
	public Vector2 Bias;
	public Vector3 localPosition;
	public Vector3 localRotate;
}

public abstract class Spline : MonoBehaviour
{
    public int _count;
    public float _step;
    public float _step_n;
    public bool isCalculated = false;
    public bool isVisible;
	public float nextY;

    public SplineType type;
    public Vector3 a;
    public Vector3 b;
    public Vector3 aa;
    public Vector3 bb;
    public Vector3 startRot;
    public Vector3 endRot;
    public float startWidth;
    public float endWidth;
    public float startVerge;
    public float endVerge;
    public float Length;



    public Transform currentTransform;

	public abstract Vector3 getRot(float t);

    public abstract Vector3 getStartRot(float t);

    public abstract Spline[] getSplines();

    public abstract Vector3 getEndRot(float t);

    public abstract float CheckCamProcess(float t);

	public abstract CameraDot GetNext(float t);

	public abstract CameraDot GetPrevious(float t);

    public abstract Vector3 GlobalPos(float t);

	public abstract Vector3 GlobalPosWidth (float t, float width, bool isLeft);

    public abstract void CalculateLenght();

    public abstract float GetCurrentWidth(float percent);

    public abstract float GetCurrentVerge(float percent);

    public abstract bool UniformGlobalPosAdd(float t, float dist, out Vector3 point, out float addPercent, out float _dist, bool direction);

    public abstract Vector3 GetAngle(float t);

	public abstract CameraDot[] GetCameraDots ();

	public abstract Vector3 GetAngleWidth(float t, float width, bool isLeft);

    public abstract Vector3 GetPointOnCurve(Vector3 _pos);

    public abstract float GetStep(Vector3 _pos);

	public abstract float CalculateLenghtLeft (float width);

	public abstract float CalculateLenghtRight (float width);

	public abstract Vector3 UniformGlobalPos(float t, float width, bool isLeft);
	#if UNITY_EDITOR
	public abstract void CreateLocators ();

	public abstract void DeleteLocators ();
	#endif
 }