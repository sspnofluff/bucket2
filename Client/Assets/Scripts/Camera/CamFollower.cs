﻿using UnityEngine;
using System.Collections;

public class CamFollower : MonoBehaviour
{


    private Transform target;
    public Transform localCam;
    public Transform currentTransform;
    public float distance = 10.0f;
    public float height = 5.0f;
    public float targetHeightRatio = 0.5f;
    public float heightDamping = 2.0f;
    public float rotationDamping = 3.0f;

    public bool followVelocity = true;
    public float velocityDamping = 0.0f;
    public float angleDamping = 0.0f;
    public float heightVelocity = 0.0f;

    public Vector3 lastPos = Vector3.zero;
    public Vector3 currentVelocity = Vector3.zero;
    private float wantedRotationAngle = 0.0f;
    private float wantedHeight = 5.0f;

    private Vector3 _localPos;
    private float _localRot;

    public Vector3 brakePosition = new Vector3(0.0f, 2.0f, 4.0f);
    public Vector3 boostPosition = new Vector3(0.0f, 0.00f, -3.0f);

    private Vector3 _currentLocalSmoothVelocityRotation = Vector3.zero;
    private Vector3 _currentLocalSmoothVelocityPosition = Vector3.zero;

    public float brakeRotation = 8.0f;
    public float boostRotation = -2.0f;

    public bool switched = false;

    void Start()
    {
        Main.instance = FindObjectOfType<Main>();
        currentTransform = transform;
        target = Main.instance.hero.currentTransform;
    }
    
    void FixedUpdate()
    {
        if (!switched)
        {
            CamTranslation();
        }
    }

    public void CamTranslation()
    {
        lastPos = target.position;
         wantedRotationAngle = target.eulerAngles.y;
        wantedHeight = target.position.y + height;

        _localPos = Vector3.Lerp(brakePosition, boostPosition, Main.instance.hero.forwardSpeed / 20.0f);
        _localRot = Mathf.Lerp(brakeRotation, boostRotation, Main.instance.hero.forwardSpeed / 20.0f);
    }
    

    void Update()
    {
        // Early out if we don't have a target
        if (!target)
            return;




        float currentRotationAngle = currentTransform.eulerAngles.y;
        float currentHeight = currentTransform.position.y;


        currentRotationAngle = Mathf.SmoothDampAngle(currentRotationAngle, wantedRotationAngle, ref angleDamping, rotationDamping);
        currentHeight = Mathf.SmoothDamp(currentHeight, wantedHeight, ref heightVelocity, heightDamping);

        Quaternion currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);

        Vector3 SmoothTargetPos = Vector3.SmoothDamp(target.position, lastPos, ref currentVelocity, velocityDamping);
        Vector3 tempPos = SmoothTargetPos;
        tempPos -= currentRotation * Vector3.forward * distance;
        tempPos.y = currentHeight;
        currentTransform.position = tempPos;

        transform.LookAt(target.position + Vector3.up * height * targetHeightRatio);
        
         

    }
}