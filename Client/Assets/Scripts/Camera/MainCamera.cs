﻿using UnityEngine;
using System.Collections;
using Dest.Math;

[System.Flags]
public enum CamMode
	{
		Free = 0,
        Battle = 1,
        Switch = 2,
        Switch02 = 3,
        Special = 4,
		End = 5,
        Base = 6,
		PreparingRotationInHangar = 7,
		SwitchBaseToFree = 8,
		EngineEffects = 9,
		Score = 10,
	}



public class MainCamera : MonoBehaviour 
{
	public enum HoodState
	{
		Close,
		Open,
		Opening,
		Closing,
	}

	HoodState hoodState = HoodState.Close;

	public bool isSmoothPosition = true;
	public bool isSmoothRotation = true;
	public bool isRoadAffective = true;
	public bool isPlayerAffective = true;
	public CamMode mode;
	public bool isSwitchToBattle = false;

	[HideInInspector]
	public Transform currentTransform;


	//Road Affection
	public float cameraDotCurrentPercent;
	public float cameraDotCurrentPercentOld;
	public int currentCameraDotLocalNumber;
	public CameraDot current;
	public CameraDot next;
    private bool isCurrentExist = false;
    private bool isCrossRotate = false;

	//hierarchy
	public Transform playerAffectCamera;
	public Camera roadAffectCamera;
	public Transform controlPoint;

	public bool isContrastStetch = false;


    
	//[HideInInspector]
	public TargetController targetController;

    //battle data
	public float distance = 10.0f;
	public float altitude;  
	public float lerp;
	//base data
	private Vector3 leftPos = new Vector3(-2.27f, 0.707f, 1.2f);//25.0f);
	private Vector3 leftRot = new Vector3(20.8f, 114.0f, 0.0f);//25.0f);
	public Vector3 baseTargetRotation;
	public Vector3 nextTargetRotation;
    //public UnityStandardAssets.ImageEffects.MotionBlur motionBlur;
	public float baseAngle = 30.0f;
	public float nextAngle = 30.0f;
	public Vector3 baseDist = new Vector3 (-2.4f, 1.2f, 0);
	public Vector3 nextDist = new Vector3 (-2.4f, 1.2f, 0);

	//private bool isLateBlock = false;

	public Vector3 playerAffectionDistMin;
	public Vector3 playerAffectionDistMax;

	public ModuleSlot baseSlot;
	public ModuleSlot nextSlot;
	public Vector3 dir = Vector3.zero;
	private CounterTimerF baseRotateTimer = new CounterTimerF (1.0f, 1.0f);
	bool isRotation = false;
	public AnimationCurve rotationCurve;
	public AnimationCurve forkBoostCurve;
	//хуита
    private float forkRotationPercent;

	private CounterTimerF switchCamTimer = new CounterTimerF (1.0f, 1.0f);
   
    private Vector3 _globalPos;
    private float _globalRot;

    private Vector3 _controlPos;
    private Vector3 _controlRot;

    private Vector3 _localPos;
    private float _localRot; 

	private Vector3 _globalPosOld;
	private float _globalRotOld;

	private Vector3 _addPosOld;
	private Vector3 _addRotOld;

	private Vector3 _controlPosOld;
	private Vector3 _controlRotOld;

	private Vector3 _localPosOld;
	private float _localRotOld; 

    private Vector3 switchPosition = Vector3.zero;

    private Vector3 oldPosition;

    public float pathDistNeutral;
    public float pathDistMisc;
    public float pathDistBonus;
	private Vector2 saveSidePercent;
	private Vector2 saveSidePercentCam;

    private Vector3 _controlDotRot;

    

	public BlockPlane _currentCamPlane;

    private float _currentCamPercent;
    private float _currentCamPersentForRotation;

    private BlockPlane _currentHeroPlane;
    private float _currentHeroPercent;

    

    //HeroReturn data
	private Vector3 camStartPos = new Vector3(0.0f, 17.0f, -0.56f);
    private Vector3 camStartRot = new Vector3(45.0f, 0.0f, 0.0f);

	//Hero Affection
	public Vector3 brakePositionBattle = new Vector3(0.0f, 1.0f, 0.0f);
	public Vector3 boostPositionBattle = new Vector3(0.0f, -1.00f, 0.0f);
	public float brakeRotationBattle = 70.0f;
	public float boostRotationBattle = 45.0f;

	public Vector3 heroInverseSourcePos;
	public Vector3 heroInverseSourceRot;
	private Vector3 heroInversePos;
	private Quaternion heroInverseRot;
	private Vector3 roadAffectedDataPos;
	private Quaternion roadAffectedDataRot;

	//Smooth dampers
    private Vector3 _smoothVelocityRotation = Vector3.zero;
    private Vector3 _smoothVelocityPosition = Vector3.zero;

    private Vector3 _smoothRoadAffectionRotation = Vector3.zero;
	private Vector3 _smoothRoadAffectionPosition = Vector3.zero;

    private Vector3 _smoothHeroAffectionRotation = Vector3.zero;
	private Vector3 _smoothHeroAffectionPosition = Vector3.zero;

	private Vector3 _smoothTargetControllerPosition = Vector3.zero;
	private float _smoothTargetControllerRotation = 0;

	private float _smoothCurveBendX = 0;
	private float _smoothCurveBiasX = 0;
	private float _smoothCurveBendZ = 0;
	private float _smoothCurveBiasZ = 0;

	//free data
    private Vector3 lastPositionFree = Vector3.zero;
    private float wantedRotationAngle = 0.0f;
    private float wantedHeight = 5.0f;
    
   

    private CounterTimerF endTimer;

	public void SetBasePos()
	{
		currentTransform = transform;

		mode = CamMode.Base;
		baseSlot = ModuleSlot.Wheels;

		currentTransform.parent = Main.instance.hero.currentTransform;
		currentTransform.localPosition = leftPos;
		currentTransform.localRotation = Quaternion.Euler(leftRot);


		//side = 0;


		//baseToPrepareTimer.max = 6.0f;
		//baseToPrepareTimer.Restore();
	}
    
    public void SetDefaultPoseBehindHero()
    {
        currentTransform.position = Main.instance.currentPlane.currentTransform.TransformPoint(camStartPos);
        currentTransform.rotation = Quaternion.Euler(camStartRot);
        lastPositionFree = currentTransform.position;
    }
    
	void Start ()
    {

        currentTransform = transform;
        targetController = FindObjectOfType<TargetController>();

        switchCamTimer.max = 1.0f;
        switchCamTimer.current = switchCamTimer.max;
	}


	public float CheckRoadWidth(float _percent)
	{
		if (_currentCamPlane == null)
			return 7.5f;
		return _currentCamPlane.CheckRoadWidth (_percent);

	}

    public void ToEndLevel()
    {

		OnBattleCam(CamMode.Free);


        endTimer.Restore();
    }

	public float ExitFromDock()
	{
		if (baseRotateTimer.isEnd (Time.deltaTime)) 
		{
			baseRotateTimer.current = 0;
		}

		return baseRotateTimer.current / baseRotateTimer.max;
	}
    public void ForkShitOperation()
    {
		forkRotationPercent = _currentCamPercent;//_currentCamPersentForRotation;
    }

    public float GetHeroPercent()
    {
        return _currentHeroPercent;
    }

    public float GetCamPercent()
    {
        return _currentCamPercent;
    }

	public float GetCamPercentRotation()
	{
		return _currentCamPersentForRotation;
	}

    public BlockPlane GetCurrentCamPlane()
    {
        return _currentCamPlane;
    }
    void Update()
    {
        
    }

   

	public void PreparingRotationInHangar()
	{
		mode = CamMode.PreparingRotationInHangar;
		isRotation = true;
		baseRotateTimer.max = 2.0f;
		baseRotateTimer.Restore();

	}

	public void SwitchBaseToFree()
	{
		mode = CamMode.SwitchBaseToFree;

		roadAffectCamera.transform.parent = Main.instance.hero.currentTransform;
		currentTransform.rotation = Quaternion.Euler (0, -90.0f, 0);
		playerAffectCamera.transform.localPosition = Vector3.zero;
		roadAffectCamera.transform.parent = playerAffectCamera;

		isRotation = true;
		baseRotateTimer.max = 4.0f;
		baseRotateTimer.Restore();

		heroInverseRot = currentTransform.rotation;
	}

	public void CamTranslationEngineEffects()
	{
		if (baseRotateTimer.isEnd(Time.deltaTime))
		{
			if (!isContrastStetch) 
			{
				isContrastStetch = true;
				Main.instance.dirLight = (GameObject)GameObject.Instantiate (Main.instance.generator.currentMap.dirLight);
				Main.instance.door.GetComponent<Animation> ().PlayQueued ("doorOpen");
				baseRotateTimer.Restore ();
				return;
			}
		}

		if (!isContrastStetch) 
		{
			if (baseRotateTimer.current / baseRotateTimer.max < 0.7f)
				Main.instance.hero.StopSignals (true);

			if (baseRotateTimer.current / baseRotateTimer.max < 0.35f) 
			{
				Main.instance.hero.StopSignals (false);

			}
		}

		currentTransform.position = Vector3.SmoothDamp (currentTransform.position, Main.instance.hero.transform.position, ref _smoothHeroAffectionPosition, 0.2f);
		currentTransform.rotation = heroInverseRot;

	}

    public void SetHeroPos()
    {
        Vector3 heroPosition = Main.instance.hero.currentTransform.position;
        Vector3 camNewPos = heroPosition;
        Vector3 camNewRot = Vector3.zero;

        Quaternion rotateStart, rotateEnd, rotate = Quaternion.identity;
        _currentCamPersentForRotation = 0.0f;

        float height;
        
        if (GetStepCam(currentTransform.position, out _currentCamPercent, out _currentCamPlane, out height))
        {
            camNewPos.y = height;

            rotateStart = _currentCamPlane.currentTransform.rotation * Quaternion.Euler(_currentCamPlane.spline.getStartRot(_currentCamPercent));
            rotateEnd = _currentCamPlane.currentTransform.rotation * Quaternion.Euler(_currentCamPlane.spline.getEndRot(_currentCamPercent));

            _currentCamPersentForRotation = _currentCamPercent;

            if (_currentCamPlane.type == BlockPlaneType.Fork)
            {

                Fork currentFork = _currentCamPlane.GetComponent<Fork>();
                if (!currentFork.isChoiceHeroSide)
                {
                    rotateStart = currentFork.camRotationFromChoice;
                    _currentCamPersentForRotation = Mathf.Lerp(0.0f, 1.0f, (_currentCamPercent - forkRotationPercent) / (1 - forkRotationPercent));
                }

            }

            rotate = Quaternion.Lerp(rotateStart, rotateEnd, _currentCamPersentForRotation);

            camNewPos.y += altitude;
            Quaternion rotateCam = currentTransform.localRotation;
            camNewRot = rotateCam.eulerAngles;
            camNewRot.y = rotate.eulerAngles.y;


            _globalPos = camNewPos;
			_globalRot = camNewRot.y;
        }


        _localPos = Vector3.Lerp(brakePositionBattle, boostPositionBattle, (Main.instance.hero.forwardSpeed - Main.instance.hero.averageSpeedMin) / (Main.instance.hero.averageSpeedMax - Main.instance.hero.averageSpeedMin));
        _localRot = Mathf.Lerp(brakeRotationBattle, boostRotationBattle, (Main.instance.hero.forwardSpeed - Main.instance.hero.averageSpeedMin) / (Main.instance.hero.averageSpeedMax - Main.instance.hero.averageSpeedMin));
    }

	public void BaseRotate(ModuleSlot _next)
	{
		
		baseRotateTimer.Restore ();
		isRotation = true;
		nextSlot = _next;

		bool openHood = false;
		if ((int)nextSlot == 6 || (int)nextSlot == 7)
			openHood = true;	
			
		if (hoodState == HoodState.Close && openHood)
			hoodState = HoodState.Opening;
		
		if (hoodState == HoodState.Open && !openHood)
			hoodState = HoodState.Closing;
	}

	public void CamTranslationPreparingRotationInHangar()
	{
		if (isRotation) 
		{	
			if (baseRotateTimer.isEnd (Time.smoothDeltaTime)) 
			{
				currentTransform.parent = null;
				heroInverseRot = currentTransform.rotation;
				mode = CamMode.EngineEffects;
				isRotation = false;
				GetComponent<AudioSource> ().PlayOneShot (GetComponent<AudioSource> ().clip);
				baseRotateTimer.Restore ();
				return;
			}


			float diff = rotationCurve.Evaluate(baseRotateTimer.current / baseRotateTimer.max);

			if (hoodState == HoodState.Open) 
				Main.instance.hero.hood.localRotation = Quaternion.Lerp(new Quaternion(0.5f, 0.0f, 0.0f, -0.866f), Quaternion.identity, diff);

			lastPositionFree = Main.instance.hero.currentTransform.position;
			currentTransform.localRotation = Quaternion.Euler (new Vector3 (0, Mathf.Lerp (Main.instance.inventory.inventorySlots[(int)baseSlot].angle, -90.0f, diff), 0));
			playerAffectCamera.transform.localPosition = Vector3.Lerp (Main.instance.inventory.inventorySlots[(int)baseSlot].dist, new Vector3(-3f,2f,0),diff);
			playerAffectCamera.transform.localRotation = Quaternion.identity;
			roadAffectCamera.transform.localRotation = Quaternion.Lerp(Quaternion.Euler(Main.instance.inventory.inventorySlots[(int)baseSlot].targetRotation), Quaternion.Euler(new Vector3(25.0f, 90.0f, 0)), diff);
			roadAffectCamera.transform.localPosition = Vector3.zero;
		}
	}

	public void CamTranslationSwitchBaseToFree()
	{
		if (isRotation) 
		{	
			if (baseRotateTimer.isEnd (Time.smoothDeltaTime)) 
			{
				mode = CamMode.Free;
				isRotation = false;
				//transform.parent = null;
				Main.instance.gameMode = Mode.Free;
				_smoothRoadAffectionPosition = Vector3.zero;

				return;
			}

			float diff = rotationCurve.Evaluate(baseRotateTimer.current / baseRotateTimer.max);

			Vector3 freePosition = Main.instance.hero.currentTransform.position;
			wantedRotationAngle = Main.instance.hero.currentTransform.eulerAngles.y;
			wantedHeight = freePosition.y + altitude;
			Quaternion currentRotation = Quaternion.Euler(0, wantedRotationAngle, 0);
			freePosition -= currentRotation * Vector3.forward * distance;
			freePosition.y = wantedHeight;
			//Vector3 SmoothPos = Vector3.SmoothDamp(currentTransform.position, freePosition, ref _smoothVelocityPosition, 0.3f);

			Vector3 direction = Vector3.Normalize((Main.instance.hero.currentTransform.position + Vector3.up * Mathf.Clamp( altitude * diff,1,Mathf.Infinity) * 0.5f) - currentTransform.position);
			Quaternion rot = Quaternion.LookRotation(direction);
			//float x = Mathf.Clamp(rot.eulerAngles.x, 45.0f, 130.0f);
			Quaternion freeRotation = rot;//Quaternion.Euler(x, rot.eulerAngles.y, rot.eulerAngles.z);   

			//Vector3 oldX = Vector3.SmoothDamp (currentTransform.position, Main.instance.hero.currentTransform.position, ref _smoothRoadAffectionPosition, 0.2f);
			currentTransform.position = Vector3.SmoothDamp(currentTransform.position, Vector3.Lerp(Main.instance.hero.currentTransform.position, freePosition, diff), ref _smoothVelocityPosition, 0.3f);

			Quaternion quat = Quaternion.Slerp(heroInverseRot,freeRotation,diff);
			Vector3 SmoothRot = Vector3.zero;

			SmoothRot.x = Mathf.SmoothDampAngle (currentTransform.eulerAngles.x, quat.eulerAngles.x, ref _smoothVelocityRotation.x, 0.05f);
			SmoothRot.y = Mathf.SmoothDampAngle (currentTransform.eulerAngles.y, quat.eulerAngles.y, ref _smoothVelocityRotation.y, 0.05f);
			SmoothRot.z = Mathf.SmoothDampAngle (currentTransform.eulerAngles.z, quat.eulerAngles.z, ref _smoothVelocityRotation.z, 0.05f);
			currentTransform.rotation = Quaternion.Euler (SmoothRot);

			roadAffectCamera.transform.localPosition = Vector3.Lerp (new Vector3(0,2f,-3f), Vector3.zero, diff);
			roadAffectCamera.transform.localRotation = Quaternion.Slerp (Quaternion.Euler (new Vector3 (25.0f, 0, 0)), Quaternion.identity, diff);


		}
	}

	public void CamTramslationBase()
	{
		

		if (!isToolTipExist())
			Main.instance.inventory.moduleTooltip.gameObject.SetActive(false);
		else
			Main.instance.inventory.moduleTooltip.gameObject.SetActive(true);

		if (isRotation) {
			if (baseRotateTimer.isEnd (Time.smoothDeltaTime)) 
			{
				baseSlot = nextSlot;
				isRotation = false;

				if (hoodState == HoodState.Opening)
					hoodState = HoodState.Open;

				if (hoodState == HoodState.Closing)
					hoodState = HoodState.Close;
			}



			float diff = rotationCurve.Evaluate(baseRotateTimer.current / baseRotateTimer.max);

			Main.instance.inventory.SetSlots (baseSlot, nextSlot, diff);

			if (hoodState == HoodState.Opening) 
				Main.instance.hero.hood.localRotation = Quaternion.Lerp(Quaternion.identity, new Quaternion(0.5f, 0.0f, 0.0f, -0.866f),  diff);

			if (hoodState == HoodState.Closing) 
				Main.instance.hero.hood.localRotation = Quaternion.Lerp(new Quaternion(0.5f, 0.0f, 0.0f, -0.866f), Quaternion.identity, diff);

			currentTransform.localPosition = Vector3.zero;
			currentTransform.localRotation = Quaternion.Euler (new Vector3 (0, Mathf.Lerp (Main.instance.inventory.inventorySlots[(int)baseSlot].angle, Main.instance.inventory.inventorySlots[(int)nextSlot].angle, diff), 0));
			playerAffectCamera.transform.localPosition = Vector3.Lerp (Main.instance.inventory.inventorySlots[(int)baseSlot].dist, Main.instance.inventory.inventorySlots[(int)nextSlot].dist,diff);
			playerAffectCamera.transform.localRotation = Quaternion.identity;
			roadAffectCamera.transform.localRotation = Quaternion.Lerp(Quaternion.Euler(Main.instance.inventory.inventorySlots[(int)baseSlot].targetRotation), Quaternion.Euler(Main.instance.inventory.inventorySlots[(int)nextSlot].targetRotation), diff);
			roadAffectCamera.transform.localPosition = Vector3.zero;
		} 
		else
		{
			currentTransform.localPosition = Vector3.zero;
			currentTransform.localRotation = Quaternion.Euler (new Vector3 (0, Main.instance.inventory.inventorySlots[(int)baseSlot].angle, 0));
			playerAffectCamera.transform.localPosition = Main.instance.inventory.inventorySlots[(int)baseSlot].dist;
			playerAffectCamera.transform.localRotation = Quaternion.identity;
			roadAffectCamera.transform.localRotation = Quaternion.Euler(Main.instance.inventory.inventorySlots[(int)baseSlot].targetRotation);
			roadAffectCamera.transform.localPosition = Vector3.zero;
		}
	}

	public void CamTranslationsFree()
	{

		lastPositionFree = Main.instance.hero.currentTransform.position;
		wantedRotationAngle = Main.instance.hero.currentTransform.eulerAngles.y;
		wantedHeight = Main.instance.hero.currentTransform.position.y + altitude;

		Quaternion currentRotation = Quaternion.Euler(0, wantedRotationAngle, 0);

		lastPositionFree -= currentRotation * Vector3.forward * distance;
		lastPositionFree.y = wantedHeight;

		Vector3 direction = Vector3.Normalize((Main.instance.hero.currentTransform.position + Vector3.up * altitude * 0.5f) - currentTransform.position);
		Quaternion rot = Quaternion.LookRotation(direction);

		float x = Mathf.Clamp(rot.eulerAngles.x, 45.0f, 90.0f);
		currentTransform.rotation = Quaternion.Euler(0, rot.eulerAngles.y, 0); 

		playerAffectCamera.localRotation = Quaternion.Euler (x, 0, 0);

		SmoothFree();

	}

	public void CamTranslationsScore()
	{

		lastPositionFree = Main.instance.hero.currentTransform.position;
		wantedRotationAngle = Main.instance.hero.currentTransform.eulerAngles.y;
		wantedHeight = Main.instance.hero.currentTransform.position.y + altitude;

		Quaternion currentRotation = Quaternion.Euler(0, wantedRotationAngle, 0);

		lastPositionFree -= currentRotation * Vector3.forward * distance;
		lastPositionFree.y = wantedHeight;

		Vector3 direction = Vector3.Normalize((Main.instance.hero.currentTransform.position + Vector3.up * altitude * 0.5f) - currentTransform.position);
		Quaternion rot = Quaternion.LookRotation(direction);

		float x = Mathf.Clamp(rot.eulerAngles.x, 45.0f, 90.0f);
		currentTransform.rotation = Quaternion.Euler(0, rot.eulerAngles.y, 0); 

		playerAffectCamera.localRotation = Quaternion.Euler (x, 0, 0);
		float height;

		if (GetStepCam(currentTransform.position, out _currentCamPercent, out _currentCamPlane, out height))
		{



			CamDotCalculate ();


		}

		SmoothFree();

		if (isRoadAffective) {
			//RoadAffectiveSmooth


			Main.instance.curveController._V_CW_Bend_X = Mathf.SmoothDamp (Main.instance.curveController._V_CW_Bend_X, Mathf.Lerp(current.AxisBendSize.x,next.AxisBendSize.x,cameraDotCurrentPercent), ref _smoothCurveBendX, 2.0f);
			Main.instance.curveController._V_CW_Bend_Z = Mathf.SmoothDamp (Main.instance.curveController._V_CW_Bend_Z, Mathf.Lerp(current.AxisBendSize.y,next.AxisBendSize.y,cameraDotCurrentPercent), ref _smoothCurveBendZ, 2.0f);
			Main.instance.curveController._V_CW_Bias_X = Mathf.SmoothDamp (Main.instance.curveController._V_CW_Bias_X, Mathf.Lerp(current.Bias.x,next.Bias.x,cameraDotCurrentPercent), ref _smoothCurveBiasX, 2.0f);
			Main.instance.curveController._V_CW_Bias_Z = Mathf.SmoothDamp (Main.instance.curveController._V_CW_Bias_Z, Mathf.Lerp(current.Bias.y,next.Bias.x,cameraDotCurrentPercent), ref _smoothCurveBiasZ, 2.0f);
			//				Debug.Log (roadAffectCamera.transform.localPosition + " " + current.localPosition);
			roadAffectCamera.transform.localPosition = Vector3.SmoothDamp (roadAffectCamera.transform.localPosition, Vector3.Lerp(current.localPosition,next.localPosition,cameraDotCurrentPercent), ref _smoothRoadAffectionPosition, 1.0f);


			Vector3 SmoothCameraDotRot = Vector3.zero;
			SmoothCameraDotRot.x = Mathf.SmoothDampAngle (roadAffectCamera.transform.localEulerAngles.x, Mathf.LerpAngle(current.localRotate.x,next.localRotate.x,cameraDotCurrentPercent), ref _smoothRoadAffectionRotation.x, 2.0f);
			SmoothCameraDotRot.y = Mathf.SmoothDampAngle (roadAffectCamera.transform.localEulerAngles.y, Mathf.LerpAngle(current.localRotate.y,next.localRotate.y,cameraDotCurrentPercent), ref _smoothRoadAffectionRotation.y, 2.0f);
			SmoothCameraDotRot.z = Mathf.SmoothDampAngle (roadAffectCamera.transform.localEulerAngles.z, Mathf.LerpAngle(current.localRotate.z,next.localRotate.z,cameraDotCurrentPercent), ref _smoothRoadAffectionRotation.z, 2.0f);
			roadAffectCamera.transform.localRotation = Quaternion.Euler (SmoothCameraDotRot);


		}
	}

    public void CamTranslationsBattle()
    {
        //motionBlur.blur = Mathf.Lerp(0.16f, 0.35f, Main.instance.hero.forwardSpeed / Main.instance.hero.maxSpeed);
        //motionBlur.blur += Mathf.Lerp(0.15f, 0.35f, Main.instance.hero.speedFactor);

        Vector3 heroPosition = Main.instance.hero.currentTransform.position;
		Quaternion rotate = Quaternion.identity;

        _globalPos = currentTransform.position;

        float side = 0.5f;
        if (GetStep(heroPosition, out _currentHeroPercent, out _currentHeroPlane))
        {
            if (_currentHeroPlane.localNumber > 0.0f)
                Main.instance.currentBlock = _currentHeroPlane.localNumber + _currentHeroPercent * _currentHeroPlane.point;
            else
                Main.instance.currentBlock = (_currentHeroPercent-0.5f) * _currentHeroPlane.point;
		
           if (_currentHeroPlane.type == BlockPlaneType.Fork)
           {
               Fork currentFork = _currentHeroPlane.GetComponent<Fork>();

               if (currentFork.isChoiceHeroSide)
               {
					
                   
					side = ((Main.instance.hero.offset / currentFork.LeftChoiceSpline.startWidth) + 1) * 0.5f;
					Debug.Log (side);
					saveSidePercent = new Vector2(side, _currentHeroPercent);
                   _globalPos = Math3d.BezierLerp(currentFork.LeftChoiceSpline.GetPointOnCurve(heroPosition), currentFork.RightChoiceSpline.GetPointOnCurve(heroPosition), side);

               }
               else
               {
					Vector3 oldPos = Math3d.BezierLerp (currentFork.LeftChoiceSpline.GetPointOnCurve (heroPosition), currentFork.RightChoiceSpline.GetPointOnCurve (heroPosition), saveSidePercent.x);
					Vector3 newPos = currentFork.spline.GetPointOnCurve(heroPosition);
					float newPercent = (_currentHeroPercent - saveSidePercent.y) / (1 - saveSidePercent.y);
					_globalPos = Math3d.BezierLerp(oldPos, newPos, forkBoostCurve.Evaluate(newPercent));

               }
           }
           else
           _globalPos = _currentHeroPlane.spline.GetPointOnCurve(heroPosition);
			
			
        }

		Main.instance.hero.offset = currentTransform.InverseTransformPoint(_globalPos).x - currentTransform.InverseTransformPoint(heroPosition).x;
		float height = _globalPos.y;

		if (GetStepCam(currentTransform.position, out _currentCamPercent, out _currentCamPlane, out height))
        {
           
            float pathDist = Vector3.Distance(oldPosition, currentTransform.position);
            pathDistNeutral += pathDist;
            pathDistMisc += pathDist;
            pathDistBonus += pathDist;
			oldPosition = currentTransform.position;
			_globalPos.y = height + altitude;




                       
            if (_currentCamPlane.type == BlockPlaneType.Fork)
            {
				Fork currentFork = _currentCamPlane.GetComponent<Fork>();

				if (currentFork.isChoiceHeroSide)
                {
					_currentCamPersentForRotation = Mathf.LerpAngle(currentFork.LeftChoiceSpline.CheckCamProcess(_currentCamPercent), currentFork.RightChoiceSpline.CheckCamProcess(_currentCamPercent), side);

						saveSidePercentCam = new Vector3 (side, _currentCamPercent);
					rotate = _currentCamPlane.currentTransform.rotation * Quaternion.Lerp(Quaternion.Euler(currentFork.LeftChoiceSpline.getRot(_currentCamPercent)),Quaternion.Euler(currentFork.RightChoiceSpline.getRot(_currentCamPercent)),side);
                  
                    _controlPos = Math3d.BezierLerp(currentFork.LeftChoiceSpline.GetPointOnCurve(controlPoint.position), currentFork.RightChoiceSpline.GetPointOnCurve(controlPoint.position), side);
                    _controlRot = Math3d.BezierLerp(currentFork.LeftChoiceSpline.GetAngle(currentFork.LeftChoiceSpline.GetStep(_controlPos)), currentFork.RightChoiceSpline.GetAngle(currentFork.RightChoiceSpline.GetStep(_controlPos)), side);
                }
                else
                {					     
                    _currentCamPersentForRotation = Mathf.Lerp(0.0f, 1.0f, (_currentCamPercent - forkRotationPercent) / (1 - forkRotationPercent));
					rotate = Quaternion.Lerp(currentFork.camRotationFromChoice,_currentCamPlane.currentTransform.rotation * Quaternion.Euler(currentFork.spline.getEndRot(_currentCamPercent)),_currentCamPersentForRotation);
			
						Vector3 oldPos = Math3d.BezierLerp (currentFork.LeftChoiceSpline.GetPointOnCurve (controlPoint.position), currentFork.RightChoiceSpline.GetPointOnCurve (controlPoint.position), saveSidePercentCam.x);
						Vector3 newPos = currentFork.spline.GetPointOnCurve(controlPoint.position);
					_controlPos = Math3d.BezierLerp(oldPos, newPos, _currentCamPersentForRotation);

						Vector3 oldRot = Math3d.BezierLerp (currentFork.LeftChoiceSpline.GetAngle(currentFork.LeftChoiceSpline.GetStep(_controlPos)), currentFork.RightChoiceSpline.GetAngle(currentFork.RightChoiceSpline.GetStep(_controlPos)), saveSidePercentCam.x);
						Vector3 newRot = currentFork.spline.GetAngle(currentFork.spline.GetStep(_controlPos));
					_controlRot = Vector3.Slerp (oldRot, newRot, _currentCamPersentForRotation);
                }

            }
            else
            {
				_currentCamPersentForRotation = _currentCamPlane.spline.CheckCamProcess(_currentCamPercent);
				rotate = _currentCamPlane.GetSplineRot (_currentCamPercent);
                _controlPos = _currentCamPlane.spline.GetPointOnCurve(controlPoint.position);
                _controlRot = _currentCamPlane.spline.GetAngle(_currentCamPlane.spline.GetStep(_controlPos));
			
            }

            _globalRot = rotate.eulerAngles.y;


            

			CamDotCalculate ();
				
			
        }

        float f = Main.instance.hero.forwardSpeed/ Main.instance.hero.averageSpeed;
		_localPos = Vector3.Lerp (playerAffectionDistMin, playerAffectionDistMax, (Main.instance.hero.verticalSlice + 1) / 2);
        _localRot = Mathf.Lerp(brakeRotationBattle, boostRotationBattle, f);
  
		SmoothBattle();
    }

	public void CamDotCalculate()
	{
		float factor = current.lenght / next.lenght;
		float factor_n = 1.0f - factor;
		cameraDotCurrentPercentOld = cameraDotCurrentPercent;

		if (_currentCamPlane.localForkNumber == current.number) 
		{
			if (next.number == current.number) 
			{
				//float full = next.percent*factor_n - current.percent*factor;
				cameraDotCurrentPercent = (_currentCamPercent - current.percent) / (next.percent - current.percent);
				//Debug.Log ("0 | currentPercent: ");// + cameraDotCurrentPercent + ", _currentCamPercent: " + _currentCamPercent + ", factor: " + factor.ToString() + ", currentCam - current.p: " + ((_currentCamPercent - current.percent)).ToString () + ", full: " + (next.percent - current.percent).ToString ());
			}
			else 
			{
				//float full = (1.0f - current.percent)*factor + next.percent*factor_n;
			cameraDotCurrentPercent = (_currentCamPercent - current.percent)/(1.0f-current.percent);//(( + next.percent)));
				//Debug.Log ("1 | currentPercent: ");// + cameraDotCurrentPercent + ", _currentCamPercent: " + _currentCamPercent + ", factor: " + factor.ToString() + ", currentCam - current.p: " + ((_currentCamPercent - current.percent)).ToString () + ", full: " + ((1.0f-current.percent) + next.percent).ToString ());
			}
		
		} 
		else
		if (_currentCamPlane.localForkNumber == next.number) 
		{
				
					//float full = (1.0f - current.percent)*factor + next.percent*factor_n;
				cameraDotCurrentPercent = 1.0f-(next.percent-_currentCamPercent)/next.percent;///(((1.0f-current.percent) + next.percent)));
				//Debug.Log ("2 | currentPercent: ");// + cameraDotCurrentPercent + ", _currentCamPercent: " + _currentCamPercent + ", factor: " + factor.ToString() + ", currentCam - current.p: " + ((_currentCamPercent - current.percent)).ToString () + ", full: " + ((1.0f-current.percent) + next.percent).ToString ());
				
		}

		//float step = Mathf.Abs (cameraDotCurrentPercent - cameraDotCurrentPercentOld);
		if (cameraDotCurrentPercent>= 1.0f) 
		{
			//Debug.Log ("GGG");
			int _selectCameraDot = -1;
			for (int i = 0; i < Main.instance.BlockPlanes.Count; i++)
				if (Main.instance.BlockPlanes [i].localForkNumber == next.number) 
				{
					_selectCameraDot = i;
					break;
				}
			if (Main.instance.BlockPlanes [_selectCameraDot].spline != null) 
			{
				float _currentPercentOld = next.percent;
				current = new CameraDot ();
				current = next;
				next = new CameraDot ();
				next = Main.instance.BlockPlanes [_selectCameraDot].spline.GetNext (_currentPercentOld);
				//Debug.Log (_currentPercentOld.ToString());

				if (next == null) 
				{
					//Debug.Log (Main.instance.BlockPlanes [_selectCameraDot].nextBlock.localForkNumber);
					if (Main.instance.BlockPlanes [_selectCameraDot].nextBlock == null)
						Debug.Log ("NULL");
					next = Main.instance.BlockPlanes [_selectCameraDot].nextBlock.spline.GetNext (0);
					next.number = Main.instance.BlockPlanes [_selectCameraDot].nextBlock.localForkNumber;
					next.lenght = Main.instance.BlockPlanes [_selectCameraDot].nextBlock.spline.Length;
				} else {
					next.number = Main.instance.BlockPlanes [_selectCameraDot].localForkNumber;
					next.lenght = Main.instance.BlockPlanes [_selectCameraDot].spline.Length;
					//Debug.Log (Main.instance.BlockPlanes [_selectCameraDot].localForkNumber);
				}
			}
			//if (current != null)
			//{
			////	isCurrentExist = false;
			//	isCrossRotate = true;
			//}
		}


		currentCameraDotLocalNumber = _currentCamPlane.localForkNumber;
	}

	public void SetCameraDots (BlockPlane _current)
	{
		current = new CameraDot ();
		current = _current.spline.GetNext (-0.1f);
		current.number = _current.localForkNumber;
		current.lenght = _current.spline.Length;
		next = new CameraDot ();
		next = _current.spline.GetNext (0.0f);
		if (next == null) {
			next = _current.nextBlock.spline.GetNext (0);
			next.number = _current.nextBlock.localForkNumber;
			next.lenght = _current.nextBlock.spline.Length;
		} else {
			next.number = _current.localForkNumber;
			next.lenght = _current.spline.Length;
		}
	}




    private void CamTranslationsSwitchBattle()
    {
        Vector3 heroPosition = Main.instance.hero.currentTransform.position;

        lastPositionFree = heroPosition;
        wantedRotationAngle = Main.instance.hero.currentTransform.eulerAngles.y;
        wantedHeight = Main.instance.hero.currentTransform.position.y + altitude;

        Quaternion currentRotation = Quaternion.Euler(0, wantedRotationAngle, 0);

        lastPositionFree -= currentRotation * Vector3.forward * distance;
        lastPositionFree.y = wantedHeight;

		//Vector3 direction = Vector3.Normalize((Main.instance.hero.currentTransform.position + Vector3.up * altitude * 0.5f) - currentTransform.position);
		//Quaternion rot = Quaternion.LookRotation(direction);

		//float x = Mathf.Clamp(rot.eulerAngles.x, 45.0f, 90.0f);
		//currentTransform.rotation = Quaternion.Euler(0, rot.eulerAngles.y, rot.eulerAngles.z); 

		//_localRotOld = new Vector3 (x, 0, 0);


        if (GetStep(heroPosition, out _currentHeroPercent, out _currentHeroPlane))
        {
			if (_currentHeroPlane.localNumber > 0.0f)
				Main.instance.currentBlock = _currentHeroPlane.localNumber + _currentHeroPercent * _currentHeroPlane.point;
			else
				Main.instance.currentBlock = (_currentHeroPercent-0.5f) * _currentHeroPlane.point;

			_globalPos = Main.instance.hero.GetSpline().GetPointOnCurve(heroPosition);          
        }

		float height = _globalPos.y;
		Quaternion rotate = Quaternion.identity;

		if (GetStepCam (currentTransform.position, out _currentCamPercent, out _currentCamPlane, out height)) {

			float pathDist = Vector3.Distance (oldPosition, currentTransform.position);
			pathDistNeutral += pathDist;
			pathDistMisc += pathDist;
			pathDistBonus += pathDist;

			oldPosition = currentTransform.position;
			_globalPos.y = height + altitude;

			_currentCamPersentForRotation = _currentCamPlane.spline.CheckCamProcess(_currentCamPercent);
			rotate = _currentCamPlane.GetSplineRot (_currentCamPercent);

			_controlPos = _currentCamPlane.spline.GetPointOnCurve(controlPoint.position);
			_controlRot = _currentCamPlane.spline.GetAngle(_currentCamPlane.spline.GetStep(_controlPos));
		}

		_globalRot = rotate.eulerAngles.y;


        Main.instance.hero.offset = currentTransform.InverseTransformPoint(heroPosition).x;
        
		float f = Main.instance.hero.forwardSpeed/ Main.instance.hero.averageSpeed;
		_localPos = Vector3.Lerp (playerAffectionDistMin, playerAffectionDistMax, (Main.instance.hero.verticalSlice + 1) / 2);
        _localRot = Mathf.Lerp(brakeRotationBattle, boostRotationBattle, f);
      	
		lerp = (isSwitchToBattle) ? switchCamTimer.current : (1 - switchCamTimer.current);

		if (!isSwitchToBattle) 
		{
			Vector3 direction = Vector3.Normalize((Main.instance.hero.currentTransform.position + Vector3.up * altitude * 0.5f) - currentTransform.position);
			Quaternion rot = Quaternion.LookRotation(direction);

			float x = Mathf.Clamp(rot.eulerAngles.x, 45.0f, 90.0f);
			_globalRotOld = rot.eulerAngles.y;

			_localRotOld = x;
		}

		_localPos = Vector3.Lerp (_localPos, _localPosOld, lerp);
		_localRot = Mathf.Lerp (_localRot,_localRotOld, lerp);

		_controlPos = Vector3.Lerp (_controlPos, _controlPosOld, lerp);
		_controlRot = Vector3.Lerp (_controlRot, _controlRotOld, lerp);

		_globalPos = Vector3.Lerp(_globalPos, lastPositionFree, lerp);

		float gTemp = Mathf.LerpAngle (_globalRot, _globalRotOld, lerp);
		//Debug.Log ("A " +  _globalRot.ToString() + " " +  _globalRotOld.ToString() + " " + lerp.ToString() + " " + gTemp.ToString());

		_globalRot = gTemp;
		CamDotCalculate ();

		SmoothSwitchBattle();

    }

	public void OnBattleCam(CamMode _mode)
	{
		//Debug.Log ("OnBattleCam");
		switchCamTimer.Restore ();
		mode = CamMode.Switch;
		if (_mode == CamMode.Battle)
		{
			SetCameraDots (Main.Placed(currentTransform.position));
			isSwitchToBattle = true;
			_controlPosOld = targetController.currentTransform.position;
			_controlRotOld = targetController.currentTransform.rotation.eulerAngles;
			_localPosOld = playerAffectCamera.localPosition;
			_localRotOld = playerAffectCamera.localEulerAngles.x;
			_globalRotOld = currentTransform.rotation.eulerAngles.y;
			_addPosOld = roadAffectCamera.transform.localPosition;
			_addRotOld = roadAffectCamera.transform.localEulerAngles;
		}
		else
		{
			_globalRotOld = currentTransform.rotation.eulerAngles.y;
			isSwitchToBattle = false;

		}
	}

    public void TrafficParams(out Vector3 _Center, out Vector3 _Right, out Vector3 _Left, out Vector3 _BackRight, out Vector3 _BackLeft)
    {
		_Center = roadAffectCamera.transform.position;
		_Right = roadAffectCamera.ScreenToWorldPoint(new Vector3(-30, Screen.height + 30, 80.0f));
		_Left = roadAffectCamera.ScreenToWorldPoint(new Vector3(Screen.width + 30, Screen.height + 30, 80.0f));
		_BackRight = roadAffectCamera.ScreenToWorldPoint(new Vector3(-30, -30, 50.0f));
		_BackLeft = roadAffectCamera.ScreenToWorldPoint(new Vector3(Screen.width + 30, -30, 50.0f));
    }

 
    
        
         
void LateUpdate()
    {
		switch (mode)
		{
		case CamMode.Base:
			CamTramslationBase ();
			break;

		case CamMode.Free:
			CamTranslationsFree();

			break;

		case CamMode.Score:
			CamTranslationsScore();

			break;

		case CamMode.PreparingRotationInHangar:
			CamTranslationPreparingRotationInHangar ();
			break;

		case CamMode.Battle:
			CamTranslationsBattle();

			break;

		case CamMode.SwitchBaseToFree:
			CamTranslationSwitchBaseToFree ();
			break;

		case CamMode.EngineEffects:
			CamTranslationEngineEffects ();
			break;

		case CamMode.End:
			CamTranslationsFree();
			break;
		case CamMode.Switch:
			{
				if (Main.instance.gameMode == Mode.Battle)
				{
					if (switchCamTimer.isEnd(Time.deltaTime))
					{
						mode = CamMode.Battle;
						switchCamTimer.Restore();
						CamTranslationsBattle();

						/*
						if (mode == CamMode.Battle || mode == CamMode.Free || mode == CamMode.Switch)
							Debug.Log("[Mode] " + ((mode != CamMode.Battle) ? ((mode == CamMode.Switch) ? ("[Switch lerp: " + switchCamTimer.current/switchCamTimer.max +  "]") : "[Free]") : "[Battle]") + 
								"\n[Last Global Cam] Pos: " + roadAffectCamera.transform.position + " Rot: " + roadAffectCamera.transform.eulerAngles +
								"\n[Main Global Cam] Pos: " + currentTransform.position + " Rot: " + currentTransform.rotation.eulerAngles + 
								"\n[Player Affe Cam] Pos: " + playerAffectCamera.localPosition + " Rot: " + playerAffectCamera.localEulerAngles +
								"\n[Road Affect Cam] Pos: " + roadAffectCamera.transform.localPosition + " Rot: " + roadAffectCamera.transform.localEulerAngles);
						*/
						return;
					}
					else
						CamTranslationsSwitchBattle();

				}
				else
				{
					if (switchCamTimer.isEnd(Time.deltaTime))
					{
						mode = CamMode.Score;
						switchCamTimer.Restore();
						CamTranslationsFree();
						/*
						if (mode == CamMode.Battle || mode == CamMode.Free || mode == CamMode.Switch)
							Debug.Log("[Mode] " + ((mode != CamMode.Battle) ? ((mode == CamMode.Switch) ? ("[Switch lerp: " + switchCamTimer.current/switchCamTimer.max +  "]") : "[Free]") : "[Battle]") + 
								"\n[Last Global Cam] Pos: " + roadAffectCamera.transform.position + " Rot: " + roadAffectCamera.transform.eulerAngles +
								"\n[Main Global Cam] Pos: " + currentTransform.position + " Rot: " + currentTransform.rotation.eulerAngles + 
								"\n[Player Affe Cam] Pos: " + playerAffectCamera.localPosition + " Rot: " + playerAffectCamera.localEulerAngles +
								"\n[Road Affect Cam] Pos: " + roadAffectCamera.transform.localPosition + " Rot: " + roadAffectCamera.transform.localEulerAngles);
						*/
						return;
					}
					else
						CamTranslationsSwitchBattle();
				}

			}
			break;

		}

            
		/*
		if (mode == CamMode.Battle || mode == CamMode.Free || mode == CamMode.Switch)
			Debug.Log("[Mode] " + ((mode != CamMode.Battle) ? ((mode == CamMode.Switch) ? ("[Switch lerp: " + switchCamTimer.current/switchCamTimer.max +  "]") : "[Free]") : "[Battle]") + 
				"\n[Last Global Cam] Pos: " + roadAffectCamera.transform.position + " Rot: " + roadAffectCamera.transform.eulerAngles +
				"\n[Main Global Cam] Pos: " + currentTransform.position + " Rot: " + currentTransform.rotation.eulerAngles + 
			    "\n[Player Affe Cam] Pos: " + playerAffectCamera.localPosition + " Rot: " + playerAffectCamera.localEulerAngles +
				"\n[Road Affect Cam] Pos: " + roadAffectCamera.transform.localPosition + " Rot: " + roadAffectCamera.transform.localEulerAngles);
		*/	    
           
    }



	private void SmoothBase()
	{
			Vector3 SmoothRot = Vector3.zero;

			SmoothRot.x = Mathf.SmoothDampAngle(currentTransform.localEulerAngles.x, 0, ref _smoothVelocityRotation.x, 25.25f);
			SmoothRot.y = Mathf.SmoothDampAngle(currentTransform.localEulerAngles.y, baseAngle, ref _smoothVelocityRotation.y, 25.25f);
			SmoothRot.z = Mathf.SmoothDampAngle(currentTransform.localEulerAngles.z, 0, ref _smoothVelocityRotation.z, 25.25f);
			currentTransform.localRotation = Quaternion.Euler(SmoothRot);

			//PlayerAffectiveSmooth
			Vector3 localPosControl = new Vector3(baseDist.x, baseDist.y, 0);
			Vector3 LocalSmoothPos = Vector3.SmoothDamp(playerAffectCamera.localPosition, localPosControl, ref _smoothHeroAffectionPosition, 25.2f);
			playerAffectCamera.localPosition = LocalSmoothPos;
	}


private void SmoothBattle()
    {

		if (isSmoothPosition) {
			Vector3 SmoothPos = Vector3.SmoothDamp (currentTransform.position, new Vector3 (_globalPos.x, _globalPos.y, _globalPos.z), ref _smoothVelocityPosition, 0.2f);
			currentTransform.position = SmoothPos;
		} else {
			currentTransform.position = new Vector3 (_globalPos.x, _globalPos.y, _globalPos.z);
		}

		if (isSmoothRotation) 
		{
			Vector3 camRot = Quaternion.LookRotation (Main.instance.hero.roadDirection).eulerAngles;
			Vector3 SmoothRot = Vector3.zero;


			SmoothRot.y = Mathf.SmoothDampAngle (currentTransform.eulerAngles.y, _globalRot, ref _smoothVelocityRotation.y, 0.25f);

			currentTransform.rotation = Quaternion.Euler (SmoothRot);
		}
		else
			currentTransform.rotation = Quaternion.Euler (new Vector3(0, _globalRot, 0));

		if (isPlayerAffective) {
			//PlayerAffectiveSmooth
			Vector3 LocalSmoothPos = Vector3.SmoothDamp (playerAffectCamera.localPosition, _localPos, ref _smoothHeroAffectionPosition, 0.2f);
			playerAffectCamera.localPosition = LocalSmoothPos;

			Vector3 LocalSmoothRot = Vector3.zero;
			LocalSmoothRot.x = Mathf.SmoothDampAngle (playerAffectCamera.localEulerAngles.x, _localRot, ref _smoothHeroAffectionRotation.x, 0.25f);
			playerAffectCamera.localRotation = Quaternion.Euler (LocalSmoothRot);

			//ControlPointSmooth
			_controlDotRot = _controlRot;

			Vector3 LocalSmoothDot = Vector3.SmoothDamp (targetController.currentTransform.position, _controlPos, ref _smoothTargetControllerPosition, 0.1f);
			targetController.currentTransform.position = LocalSmoothDot;

			float angle = Math3d.GetAngle (_controlDotRot.z, _controlDotRot.x, Vector3.forward.z, Vector3.forward.x);
			float sAngle = Mathf.SmoothDampAngle (targetController.currentTransform.eulerAngles.y, -angle, ref _smoothTargetControllerRotation, 0.6f);
			Vector3 _pointCamEuler = new Vector3 (0.0f, sAngle, 0.0f);
			targetController.currentTransform.rotation = Quaternion.Euler (_pointCamEuler);
		}


		if (isRoadAffective) {
			//RoadAffectiveSmooth

				
				Main.instance.curveController._V_CW_Bend_X = Mathf.SmoothDamp (Main.instance.curveController._V_CW_Bend_X, Mathf.Lerp(current.AxisBendSize.x,next.AxisBendSize.x,cameraDotCurrentPercent), ref _smoothCurveBendX, 2.0f);
				Main.instance.curveController._V_CW_Bend_Z = Mathf.SmoothDamp (Main.instance.curveController._V_CW_Bend_Z, Mathf.Lerp(current.AxisBendSize.y,next.AxisBendSize.y,cameraDotCurrentPercent), ref _smoothCurveBendZ, 2.0f);
				Main.instance.curveController._V_CW_Bias_X = Mathf.SmoothDamp (Main.instance.curveController._V_CW_Bias_X, Mathf.Lerp(current.Bias.x,next.Bias.x,cameraDotCurrentPercent), ref _smoothCurveBiasX, 2.0f);
				Main.instance.curveController._V_CW_Bias_Z = Mathf.SmoothDamp (Main.instance.curveController._V_CW_Bias_Z, Mathf.Lerp(current.Bias.y,next.Bias.x,cameraDotCurrentPercent), ref _smoothCurveBiasZ, 2.0f);
//				Debug.Log (roadAffectCamera.transform.localPosition + " " + current.localPosition);
				roadAffectCamera.transform.localPosition = Vector3.SmoothDamp (roadAffectCamera.transform.localPosition, Vector3.Lerp(current.localPosition,next.localPosition,cameraDotCurrentPercent), ref _smoothRoadAffectionPosition, 1.0f);


				Vector3 SmoothCameraDotRot = Vector3.zero;
				SmoothCameraDotRot.x = Mathf.SmoothDampAngle (roadAffectCamera.transform.localEulerAngles.x, Mathf.LerpAngle(current.localRotate.x,next.localRotate.x,cameraDotCurrentPercent), ref _smoothRoadAffectionRotation.x, 2.0f);
				SmoothCameraDotRot.y = Mathf.SmoothDampAngle (roadAffectCamera.transform.localEulerAngles.y, Mathf.LerpAngle(current.localRotate.y,next.localRotate.y,cameraDotCurrentPercent), ref _smoothRoadAffectionRotation.y, 2.0f);
				SmoothCameraDotRot.z = Mathf.SmoothDampAngle (roadAffectCamera.transform.localEulerAngles.z, Mathf.LerpAngle(current.localRotate.z,next.localRotate.z,cameraDotCurrentPercent), ref _smoothRoadAffectionRotation.z, 2.0f);
				roadAffectCamera.transform.localRotation = Quaternion.Euler (SmoothCameraDotRot);


		}
          
    }

    //Not using now
    private void SmoothFree()
    {
		if (isSmoothPosition) 
		{
			Vector3 SmoothPos = Vector3.SmoothDamp (currentTransform.position, lastPositionFree, ref _smoothVelocityPosition, 0.3f);
			currentTransform.position = SmoothPos;
		}
  
        



    }

	private void SmoothPrepare()
	{
			Vector3 SmoothPos = Vector3.SmoothDamp(currentTransform.position, lastPositionFree, ref _smoothVelocityPosition, 0.1f);
			currentTransform.position = SmoothPos;

//			currentTransform.position = lastPositionFree;

   
	}

    //For tests
private void NonSmooth()
{

    currentTransform.position = lastPositionFree;

    Vector3 direction = Vector3.Normalize((Main.instance.hero.currentTransform.position + Vector3.up * altitude * 0.5f) - currentTransform.position);
    Quaternion rot = Quaternion.LookRotation(direction);

    float x = Mathf.Clamp(rot.eulerAngles.x, 45.0f, 90.0f);
    currentTransform.rotation = Quaternion.Euler(x, rot.eulerAngles.y, rot.eulerAngles.z);
}

//Not using now
	private void SmoothSwitchBattle()
	{

		if (isSmoothPosition) {
			Vector3 SmoothPos = Vector3.SmoothDamp (currentTransform.position, new Vector3 (_globalPos.x, _globalPos.y, _globalPos.z), ref _smoothVelocityPosition, 0.2f);
			currentTransform.position = SmoothPos;
		} else {
			currentTransform.position = new Vector3 (_globalPos.x, _globalPos.y, _globalPos.z);
		}

		if (isSmoothRotation) 
		{
			Vector3 SmoothRot = Vector3.zero;


			SmoothRot.y = Mathf.SmoothDampAngle (currentTransform.eulerAngles.y, _globalRot, ref _smoothVelocityRotation.y, 0.25f);

			currentTransform.rotation = Quaternion.Euler (SmoothRot);
		}
		else
			currentTransform.rotation = Quaternion.Euler (new Vector3(0, _globalRot, 0));

		if (isPlayerAffective)
		{
			//PlayerAffectiveSmooth
			Vector3 LocalSmoothPos = Vector3.SmoothDamp (playerAffectCamera.localPosition, _localPos, ref _smoothHeroAffectionPosition, 0.2f);
			playerAffectCamera.localPosition = LocalSmoothPos;

			Vector3 LocalSmoothRot = Vector3.zero;
			LocalSmoothRot.x = Mathf.SmoothDampAngle (playerAffectCamera.localEulerAngles.x, _localRot, ref _smoothHeroAffectionRotation.x, 0.25f);
			playerAffectCamera.localRotation = Quaternion.Euler (LocalSmoothRot);

			//ControlPointSmooth
			Vector3 LocalSmoothDot = Vector3.SmoothDamp (targetController.currentTransform.position, _controlPos, ref _smoothTargetControllerPosition, 0.1f);
			targetController.currentTransform.position = LocalSmoothDot;

			float angle = Math3d.GetAngle (_controlRot.z, _controlRot.x, Vector3.forward.z, Vector3.forward.x);
			float sAngle = Mathf.SmoothDampAngle (targetController.currentTransform.eulerAngles.y, -angle, ref _smoothTargetControllerRotation, 0.6f);
			Vector3 _pointCamEuler = new Vector3 (0.0f, sAngle, 0.0f);
			targetController.currentTransform.rotation = Quaternion.Euler (_pointCamEuler);
		}


		if (isRoadAffective) {
			//RoadAffectiveSmooth


				Main.instance.curveController._V_CW_Bend_X = Mathf.SmoothDamp (Main.instance.curveController._V_CW_Bend_X, Mathf.Lerp(current.AxisBendSize.x,next.AxisBendSize.x,cameraDotCurrentPercent), ref _smoothCurveBendX, 2.0f);
				Main.instance.curveController._V_CW_Bend_Z = Mathf.SmoothDamp (Main.instance.curveController._V_CW_Bend_Z, Mathf.Lerp(current.AxisBendSize.y,next.AxisBendSize.y,cameraDotCurrentPercent), ref _smoothCurveBendZ, 2.0f);
				Main.instance.curveController._V_CW_Bias_X = Mathf.SmoothDamp (Main.instance.curveController._V_CW_Bias_X, Mathf.Lerp(current.Bias.x,next.Bias.x,cameraDotCurrentPercent), ref _smoothCurveBiasX, 2.0f);
				Main.instance.curveController._V_CW_Bias_Z = Mathf.SmoothDamp (Main.instance.curveController._V_CW_Bias_Z, Mathf.Lerp(current.Bias.y,next.Bias.x,cameraDotCurrentPercent), ref _smoothCurveBiasZ, 2.0f);
				//				Debug.Log (roadAffectCamera.transform.localPosition + " " + current.localPosition);
				roadAffectCamera.transform.localPosition = Vector3.SmoothDamp (roadAffectCamera.transform.localPosition, Vector3.Lerp(current.localPosition,next.localPosition,cameraDotCurrentPercent), ref _smoothRoadAffectionPosition, 1.0f);


				Vector3 SmoothCameraDotRot = Vector3.zero;
				SmoothCameraDotRot.x = Mathf.SmoothDampAngle (roadAffectCamera.transform.localEulerAngles.x, Mathf.LerpAngle(current.localRotate.x,next.localRotate.x,cameraDotCurrentPercent), ref _smoothRoadAffectionRotation.x, 2.0f);
				SmoothCameraDotRot.y = Mathf.SmoothDampAngle (roadAffectCamera.transform.localEulerAngles.y, Mathf.LerpAngle(current.localRotate.y,next.localRotate.y,cameraDotCurrentPercent), ref _smoothRoadAffectionRotation.y, 2.0f);
				SmoothCameraDotRot.z = Mathf.SmoothDampAngle (roadAffectCamera.transform.localEulerAngles.z, Mathf.LerpAngle(current.localRotate.z,next.localRotate.z,cameraDotCurrentPercent), ref _smoothRoadAffectionRotation.z, 2.0f);
				roadAffectCamera.transform.localRotation = Quaternion.Euler (SmoothCameraDotRot);


		}

	}



    

    public bool GetStep(Vector3 pos, out float percent, out BlockPlane plane)
    {
        percent = 0.0f;      
        
		plane = Main.instance.GetBlockPlane (pos, out percent);

        return true;
    }

    public bool GetStepCam(Vector3 pos, out float percent, out BlockPlane plane, out float height)
    {
        
		percent = 0.0f;      

		plane = Main.instance.GetBlockPlane (pos, out percent);
		Vector3 tempPos = plane.spline.GlobalPos (percent);

		height = tempPos.y;
		return true;
    }

    public bool GetStep(Vector3 pos, out float percent)
    {
		percent = 0.0f;      
		Main.instance.GetBlockPlane (pos, out percent);

		return true;
    }

    public Spline GetSpline(Vector3 pos)
    {
        Spline result = null;
		float _step = 0;
		BlockPlane _plane = Main.instance.GetBlockPlane (pos, out _step);

        if (_plane == null)
            return result;

        result = _plane.spline;
        return result;
    }

	bool isToolTipExist()
	{
		bool deleteTooltip = false;

		for (int i = 0; i < Main.instance.inventory.inventorySlots.Length; i++)
			if (Main.instance.inventory.inventorySlots[i].UpdateSlot())
				deleteTooltip = true;

		for (int i = 0; i < Main.instance.inventory.inventoryObjects.Length; i++)
			if (Main.instance.inventory.inventoryObjects[i].slot.UpdateSlot())
				deleteTooltip = true;



		return deleteTooltip;
	}


}
