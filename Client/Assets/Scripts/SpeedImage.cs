﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SpeedImage : MonoBehaviour {

    public Sprite speedMinUp;
    public Sprite speedAverageUp;
    public Sprite speedMaxUp;
    public Sprite speedMinDown;
    public Sprite speedAverageDown;
    public Sprite speedMaxDown;

    public Image speedImage;

    public void Set(float factor)
    {
        if (factor >= 0.5f)
        {
            
            if (factor >= 0.6667f)
            {
                if (factor >= 0.8334f)
                {
                    speedImage.sprite = speedMaxDown;
                }
                else
                    speedImage.sprite = speedAverageDown;
            }
            else
                speedImage.sprite = speedMinDown;
        }
        else
        {
            if (factor >= 0.1667f)
            {
                if (factor >= 0.3334f)
                {
                    speedImage.sprite = speedMinUp;
                }
                else
                    speedImage.sprite = speedAverageUp;
            }
            else
                speedImage.sprite = speedMaxUp;
        }
    }
}
