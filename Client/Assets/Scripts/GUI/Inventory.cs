﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[System.Flags]
public enum AmmoType
{
    None = 0,
    Bullets = 1,
    Guns = 2,
    Impuls = 3,
    Plasma = 4,
    Missiles = 5,
    Toxic = 6,
    Armor = 7,
    Steel = 8,
}

[System.Serializable]
public class AmmoInfo
{
    public AmmoType type;
    public string shellName;
    public string trunkName;
}

[System.Serializable]
public class Item
{
    public string id;
    public string name;
    public ModuleSlot slotType;
    public ModuleType moduleType;
    public Sprite sprite;
    public Sprite ammoSprite;
    public Sprite bigPicture;
    public Sprite areaEffect;
    public Sprite durabilityImage;
    public AmmoType ammoType;
    public bool defaultModule;


    public int destructionChance;
    public float durability;
    public float mass;
    public float energy;
    public float cost;

}

[System.Serializable]
public class cell
{
    public int number;
    public Slot slot;
    
    public cell(int number, Slot _slot, RectTransform parent)
    {
        slot = GameObject.Instantiate<Slot>(_slot);
        slot.current.SetParent(parent);
        slot.parent = parent;
        float x = (slot.currentImageBackground.rectTransform.sizeDelta.x / 2) + (slot.currentImageBackground.rectTransform.sizeDelta.x) * (number % 4);
        float y = (-slot.currentImageBackground.rectTransform.sizeDelta.y / 2) + (-slot.currentImageBackground.rectTransform.sizeDelta.y) * Mathf.Ceil(number / 4);
        slot.currentImageBackground.rectTransform.anchoredPosition = new Vector2(x, y);
        slot.number = number;
    }

}

[System.Serializable]
public class SlotArrangement
{
	public ModuleSlot ModuleSlot;
	public Vector2[] slot;
}

public class Inventory : MonoBehaviour {



    public GameObject selectBlock;
    public GameObject toolTip;
    public GameObject currentGameobject;
    public ModuleSystem moduleSystem;
    public RectTransform inventory;
    public Slot slotSource;
    public Shader standard;
    public Shader outline;
    public AmmoInfo[] ammoInfo;
	public RectTransform slotPanel ;
	public Material hero;
	public Material heroOutlined;

    public Item[] items;
    public cell[] inventoryObjects = new cell[32];
    public CarPathButton[] inventorySlots;
    public Mesh wheelDefaultSource;
    public Mesh hoodDefaultSource;

    public ModuleTooltip moduleTooltip;
    public ParamSliders paramSliders;
    public durabilitySlider durabilitySliderSource;
    public Shop shop;

    public Text moneyCapacity;
    public Text repairButton;
	public SlotArrangement[] slotArrangement;
    
    public AmmoInfo GetAmmoInfo(AmmoType ammoType)
    {
        for (int i = 0; i<ammoInfo.Length; i++)
        {
            if (ammoInfo[i].type == ammoType)
                return ammoInfo[i];
        }
        return null;
    }

	public void SetSlots(ModuleSlot start, ModuleSlot end, float time)
	{
		for (int i = 0; i < 8; i++) 
		{
			inventorySlots [i].current.anchoredPosition = Vector2.Lerp (slotArrangement[(int)start].slot[i], slotArrangement[(int)end].slot[i], time);
		}
	}
    //public cell[] inventoryObjects = new cell[8];
    //public cell[] inventoryObjectsActive = new cell[4];

	// Use this for initialization
	void Start () {
        //
        for (int i = 0; i < 32; i++)
        inventoryObjects[i] = new cell(i, slotSource, inventory);
        

        //Main.instance.LoadModule("wolframBumper");
       // Main.instance.LoadModule("bigGuns");
        
        //Main.instance.LoadModule("missileLauncher");
        //inventoryObjects[0].number = 0;
       // inventoryObjects[0].slot.currentImageIcon.color = new Color(1, 1, 1, 1);
        //inventoryObjects[0].slot.currentImageIcon.sprite = items[inventoryObjects[0].number].sprite;
        //inventoryObjects[0].slot.isActive = true;
        

       // inventoryObjects[0].number = 1;
       // inventoryObjects[0].slot.currentImageIcon.color = new Color(1, 1, 1, 1);
       // inventoryObjects[0].slot.currentImageIcon.sprite = items[inventoryObjects[0].number].sprite;
        //inventoryObjects[0].slot.isActive = true;
        Main.instance.LoadModule(items[9].id);
        moduleSystem.EquipModuleRoot(0);

        Main.instance.LoadModule(items[10].id);
        moduleSystem.EquipModuleRoot(0);

        Main.instance.LoadModule(items[11].id);
        moduleSystem.EquipModuleRoot(0);

        moduleSystem.defaultModules = new Module[3];
        moduleSystem.defaultModules[0] = moduleSystem.module[(int)ModuleSlot.Engine];
        moduleSystem.defaultModules[1] = moduleSystem.module[(int)ModuleSlot.Generator];
        moduleSystem.defaultModules[2] = moduleSystem.module[(int)ModuleSlot.Wheels];
        
        
        Main.instance.LoadModule(items[0].id);
        Main.instance.LoadModule(items[1].id);
        Main.instance.LoadModule(items[2].id);
        Main.instance.LoadModule(items[3].id);
        Main.instance.LoadModule(items[4].id);
        Main.instance.LoadModule(items[5].id);
        Main.instance.LoadModule(items[6].id);
        Main.instance.LoadModule(items[7].id);
        Main.instance.LoadModule(items[8].id);
        Main.instance.LoadModule(items[12].id);
        
        shop.LoadBig();
        
        
        int count = Main.instance.hero.moduleSystem.otherModules.Count;

        for (int i = 0; i < count; i++)
            Destroy(Main.instance.hero.moduleSystem.otherModules[i].gameObject);           

        Main.instance.hero.moduleSystem.otherModules.Clear();
        

        //Main.instance.LoadModule(items[5].id);

        Main.instance.hero.moduleSystem.UpdateIcons();

        if (!Main.instance.isAll)
        shop.LoadRandomBig(7, 2);
        else
        shop.LoadRandomBig(items.Length, 2);
        //moduleSystem.UnEquipModule(0);
       // 
        //moduleSystem.EquipModule(2);
            /*
            
            inventoryObjects[0] = new cell("Орудия 37 мм", "fixedGuns37mm", ModuleTypeOld.Gun, 100, 10);
            inventoryObjects[1] = new cell("Вольфрамовый бампер", "wolframBumper", ModuleTypeOld.Bumper, 100, 10);
            inventoryObjects[2] = new cell("Стальные пластины", "steelPlanes", ModuleTypeOld.Side, 100, 10);
            inventoryObjects[3] = new cell("Разлив масла", "oilSpill", ModuleTypeOld.Back, 100, 10);
            inventoryObjects[4] = new cell("Пулеметы Гатлинга", "gatlings", ModuleTypeOld.Gun, 100, 10);
            
            */
        //PrepareIcons();

        //if (moduleSystem.moduleWeapon == null)
        //    rightController.gameObject.SetActive(false);

	}
	
	// Update is called once per frame
	void Update () {

        //if (Main.instance.generator!=null)
        //if (Main.instance.generator.GeneratorTime > Main.instance.generator.MaxTime / 2)
        //    moduleSystem.EquipModule(0);

        //VisualMenu();


        
        
	}
    /*
    void VisualMenu()
    {
        nonselect = true;
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 4; j++)
            {

                if (moduleSystem.otherModules.Count > j + i * 4)
                {

                    if (Input.mousePosition.x > (Screen.width / 2) + 218 + i * 90 && Input.mousePosition.x < (Screen.width / 2) + 304 + i * 90 &&
                    Input.mousePosition.y > (Screen.height / 2) + 184 - j * 90 && Input.mousePosition.y < (Screen.height / 2) + 270 - j * 90)
                    {
                    
                    
                        if (Input.GetMouseButton(0) && Main.instance.isMenu)
                        {
                            selectBlock.GetComponent<GUITexture>().texture = GreenBlock;
                        }
                        else
                        {
                            selectBlock.GetComponent<GUITexture>().texture = YellowBlock;
                        }
                        if (Input.GetMouseButtonUp(0) && Main.instance.isMenu)
                        {
                           Equip(j + i * 4);
                        }
                        nonselect = false;
                        selectBlock.GetComponent<GUITexture>().pixelInset = new Rect(217 + i * 90, 185 - j * 90, 86, 86);
                        toolTip.GetComponent<GUITexture>().pixelInset = new Rect(-42 + i * 90, 122 - j * 90, 258, 151);
                        toolTip.transform.Find("textName").GetComponent<GUIText>().pixelOffset = new Vector2(-29 + i * 90, 264 - j * 90);
                        toolTip.transform.Find("textName").GetComponent<GUIText>().text = moduleSystem.otherModules[j + i * 4].inventoryName;
                        //toolTip.transform.Find("textParam").GetComponent<GUIText>().pixelOffset = new Vector2(-29 + i * 90, 230 - j * 90);
                        //toolTip.transform.Find("textComment").GetComponent<GUIText>().pixelOffset = new Vector2(-29 + i * 90, 210 - j * 90);
                        //toolTip.transform.Find("textHull").GetComponent<GUIText>().pixelOffset = new Vector2(74 + i * 90, 155 - j * 90);

                        if (selectionCell != j + i * 4)
                        {
                            selectionCell = j + i * 4;
                            selectionTimer = 0;
                        }
                        else
                        {
                            selectionTimer += 0.1f;
                        }

                    }



                }

            }

        for (int i = 0; i < 4; i++)
        {
            if (Input.mousePosition.x > (Screen.width / 2) - 186 + i * 90 && Input.mousePosition.x < (Screen.width / 2) - 98 + i * 90 &&
                Input.mousePosition.y > (Screen.height / 2) - 238 && Input.mousePosition.y < (Screen.height / 2) - 150)
            {
                Module module = null;
                switch (i)
                {
                    case 0:
                        module = moduleSystem.moduleWeapon;
                    break;
                    case 1:
                        module = moduleSystem.Front;
                    break;
                    case 2:
                        module = moduleSystem.Side;
                    break;
                    case 3:
                        module = moduleSystem.Back;
                    break;
                }
                if (module!=null)
                {
                    if (Input.GetMouseButton(0) && Main.instance.isMenu)
                    {
                        selectBlock.GetComponent<GUITexture>().texture = GreenBlock;
                    }
                    else
                    {
                        selectBlock.GetComponent<GUITexture>().texture = YellowBlock;
                    }
                    if (Input.GetMouseButtonUp(0) && Main.instance.isMenu)
                    {
                        UnEquip(module.type);
                    }
                    nonselect = false;
                    selectBlock.GetComponent<GUITexture>().pixelInset = new Rect(-185 + i * 90, -236, 86, 86);
                    toolTip.GetComponent<GUITexture>().pixelInset = new Rect(-444 + i * 90, -298, 258, 151);
                    toolTip.transform.Find("textName").GetComponent<GUIText>().pixelOffset = new Vector2(-431 + i * 90, -156);
                    toolTip.transform.Find("textName").GetComponent<GUIText>().text = module.inventoryName;
                    //toolTip.transform.Find("textParam").GetComponent<GUIText>().pixelOffset = new Vector2(-431 + i * 90, -192);
                    //toolTip.transform.Find("textHull").GetComponent<GUIText>().pixelOffset = new Vector2(-328 + i * 90, -264);
                    //toolTip.transform.Find("textComment").GetComponent<GUIText>().pixelOffset = new Vector2(-431 + i * 90, -212);

                    if (selectionCell != i)
                    {
                        selectionCell = i;
                        selectionTimer = 0;
                    }
                    else
                    {
                        selectionTimer += 0.1f;
                    }
                }



            }
        }


        if (nonselect == true)
        {
            selectBlock.SetActive(false);
            selectionTimer = 0;
        }
        else
            selectBlock.SetActive(true);
    }
    */
    void Equip(int number)
    {
        
        //int activeSlot = 0;
        moduleSystem.EquipModule(number);
        /*
        switch(moduleSystem.otherModules[number].type)
        {
            case ModuleType.Weapon:
            activeSlot=0;
                break;
            case ModuleType.Front:
            activeSlot=1;
                break;
            case ModuleType.Side:
            activeSlot=2;
                break;
            case ModuleType.Back:
            activeSlot=3;
                break;
            
        }
        
        if (moduleSystem.otherModules[activeSlot].type == (moduleSystem.otherModules[number]))
        {
            //transformGUI.FindChild("carPlane").FindChild(inventoryObjects[number].id).gameObject.SetActive(false);
            //Main.instance.Proto.moduleSystem.transform.FindChild(inventoryObjects[number].id).gameObject.SetActive(false);
        }
        */
        //transformGUI.FindChild("carPlane").FindChild(inventoryObjectsActive[activeSlot].id).gameObject.SetActive(true);
        //HeroObject.transform.FindChild(inventoryObjectsActive[activeSlot].id).gameObject.SetActive(true);
        //HeroObject.rigidbody.centerOfMass = HeroObject.transform.Find("centerOfMass").transform.localPosition;
        
        //SortInventory();
        //PrepareIcons();
        
        
    }

    void UnEquip(ModuleSlot _moduleType)
    {

        moduleSystem.UnEquipModule(_moduleType);
        //transformGUI.FindChild("carPlane").FindChild(inventoryObjectsActive[number].id).gameObject.SetActive(false);
        //HeroObject.transform.FindChild(inventoryObjectsActive[number].id).gameObject.SetActive(false);
        //HeroObject.rigidbody.centerOfMass = HeroObject.transform.Find("centerOfMass").transform.localPosition;
        //inventoryObjects[FreeSlot(inventoryObjects)].Assign(inventoryObjectsActive[number]);
        //SortInventory();
        //PrepareIcons();


    }
    /*
    int FreeSlot(cell[] _cells)
    {
        for (int i = 0; i < _cells.Length; i++)
        {
             if (_cells[i].exist == false)
                 return i;
        }

        return -1;
    }
    
    void CreateModuleIcon(int number, bool active)
    {
        
        GameObject ModuleIcon = new GameObject();

        string activeSuffix="";
        if (active)  
        activeSuffix = "Active";

        ModuleIcon.name = "block" + number + "Module" + activeSuffix;
        //ModuleIcon.transform.parent = transformGUI;
        ModuleIcon.transform.position = selectBlock.transform.position;
        ModuleIcon.transform.localScale = selectBlock.transform.localScale;
        ModuleIcon.AddComponent<GUITexture>();
        if (!active)
            ModuleIcon.GetComponent<GUITexture>().pixelInset = new Rect(217 + Mathf.CeilToInt(number / 4) * 90, 185 - number % 4 * 90, 86, 86);
        else
            ModuleIcon.GetComponent<GUITexture>().pixelInset = new Rect(-185 + number * 90, -236, 86, 86);

        //string _id;

        if (!active)
            ModuleIcon.GetComponent<GUITexture>().texture = moduleSystem.otherModules[number].inventoryIcon;
        else
        {
            switch (number)
            {
                case 0:
                ModuleIcon.GetComponent<GUITexture>().texture = moduleSystem.moduleWeapon.inventoryIcon;
                    break;
                    case 1:
                ModuleIcon.GetComponent<GUITexture>().texture = moduleSystem.Front.inventoryIcon;
                    break;
                    case 2:
                ModuleIcon.GetComponent<GUITexture>().texture = moduleSystem.Side.inventoryIcon;
                    break;
                    case 3:
                ModuleIcon.GetComponent<GUITexture>().texture = moduleSystem.Back.inventoryIcon;
                    break;
            }

        }
            
    }
    */
    public void Activate()
    {
        currentGameobject.SetActive(true);
        //rightController.gameObject.SetActive(false);
    }

    public void Deactivate()
    {
        currentGameobject.SetActive(false);
        //if (moduleSystem.moduleWeapon!=null)
        //rightController.gameObject.SetActive(true);
    }
    /*
    void PrepareIcons()
    {
        
        ArrayList Temp = new ArrayList();
        
        foreach (Transform moduleIcon in Temp)
        {
            DestroyImmediate(moduleIcon.gameObject);
        }

            for (int i = 0; i < 2; i++)
            for (int j = 0; j < 4; j++)
            {
            if (moduleSystem.otherModules.Count > j + i * 4)
            CreateModuleIcon(j + i * 4, false);
            }

            if (moduleSystem.moduleWeapon!=null)
            CreateModuleIcon(0, true);
            if (moduleSystem.Front!=null)
            CreateModuleIcon(1, true);
            if (moduleSystem.Side!=null)
            CreateModuleIcon(2, true);
            if (moduleSystem.Back!=null)
            CreateModuleIcon(3, true);
           
            
         
    }
    */
    void SortInventory()
    {
        moduleSystem.otherModules.Sort();
    }


}
