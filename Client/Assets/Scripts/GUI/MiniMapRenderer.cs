﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Flags]
public enum NoneType
{
    None = 0,
    Left = 1,
    Down = 2,
    DownLeft = 3,
    Right = 4,
    RightLeft = 5,
    RightDown = 6,
    RightDownLeft = 7,
    Top = 8,
    TopLeft = 9,
    TopDown = 10,
    TopDownLeft = 11,
    TopRight = 12,
    TopRightLeft = 13,
    TopRightDown = 14,
    TopRightDownLeft = 15,
    


}

[System.Serializable]
public class MiniMapData
{
    public MiniMapCellType type;
    public NoneType noneType;
    public bool isDiscovered;
    public bool isHidden;

    public MiniMapData(MiniMapCellType _type, bool _hidden)
    {
        type = _type;
        noneType = NoneType.None;
        isDiscovered = false;
        isHidden = _hidden;
    }

}

public class MiniMapRenderer : MonoBehaviour
{

    private const float srcSize = 0.12121212f;
    private const int srcCount = 8;
    private const float srcOffset = 0.000f;

    public UnityEngine.Camera camera; 
    public Material currentMat;
    public Transform carElement;
    private Mesh currentMesh;
    private GameObject miniMapMesh;
    public bool hiddenMap = false;


    private float size;
    private int count;
    private Vector3 startPosition;
    private List<MiniMapData> miniMapData; 

    private bool isInitialize = false;
    private Vector2i selectedCell;

    void Initialize()
    {
        if (isInitialize)
            return;

		Main.instance = FindObjectOfType<Main> ();
        size = Main.instance.levelDB.miniMapSize;
        count = Main.instance.levelDB.miniMapCount;
        startPosition = Main.instance.levelDB.miniMapPosition;

        miniMapData = new List<MiniMapData>();

        for (int i = 0; i < Main.instance.levelDB.miniMapSource.Count; i++)
            miniMapData.Add(new MiniMapData(Main.instance.levelDB.miniMapSource[i].type, Main.instance.levelDB.miniMapSource[i].hidden));

        currentMesh = CreateMesh();

        
        for (int x = 0; x < count; x++)
        {
            for (int y = 0; y < count; y++)
            {
                SetCellFog(x, y, 48);
            }
        }
        
        isInitialize = true;

    }

    public void WatchHidden()
    {
        if (!hiddenMap)
        {
            for (int x = 0; x < count; x++)
            {
                for (int y = 0; y < count; y++)
                {
                    if (!miniMapData[x + count * y].isDiscovered && !miniMapData[x + count * y].isHidden && miniMapData[x + count * y].type != MiniMapCellType.None)
                        SetCellFog(x, y, 8);
                }
            }
            hiddenMap = true;
        }
        else
        {
            for (int x = 0; x < count; x++)
            {
                for (int y = 0; y < count; y++)
                {
                    if (!miniMapData[x + count * y].isDiscovered && !miniMapData[x + count * y].isHidden && miniMapData[x + count * y].type != MiniMapCellType.None)
                        SetCellFog(x, y, 48);
                }
            }
            hiddenMap = false;
        }
    }

    void Update()
    {
        
        if (!isInitialize)
            Initialize();

        Vector3 position = Main.instance.hero.currentTransform.position;
        Vector3 mapPos = miniMapMesh.transform.position;
        mapPos.x = (position.x) / (size /2.0f) * -1;
        mapPos.z = (position.z) / (size / 2.0f) * -1;
        miniMapMesh.transform.position = mapPos;

        for (int horizontal = 0; horizontal < count; horizontal++)
            for (int vertical = 0; vertical < count; vertical++)
            {
                
                if (Vector2.Distance(new Vector2(position.x,position.z),new Vector2(startPosition.x + horizontal * size + size/2,startPosition.z + vertical * size + size/2))<(size/2 + 13.0f))
                    if (!miniMapData[horizontal * count + vertical].isDiscovered)
                    {
                    
                        miniMapData[horizontal * count + vertical].isDiscovered = true;
                        SetCell(vertical, horizontal, (int)Main.instance.levelDB.miniMapSource[horizontal * count + vertical].type);
                    }
                  }

        Vector3 locRot = Main.instance.hero.currentTransform.eulerAngles;
        locRot.x = 0; locRot.z = 0;
        carElement.rotation = Quaternion.Euler(locRot);

    }

    public void SetCell(int x, int y, int number)
    {
        SetCellFog(x, y, number);

        

        CheckRoundAlt(x + 1, y + 1);
        CheckRoundAlt(x - 1, y + 1);
        CheckRoundAlt(x + 1, y - 1);
        CheckRoundAlt(x - 1, y - 1);

        CheckRound(x + 1, y);
        CheckRound(x - 1, y);
        CheckRound(x, y + 1);
        CheckRound(x, y - 1);

    }

    public void SetCellFog(int x, int y, int number)
    {
    Vector2[] uvs = currentMesh.uv;
    int xN = Mathf.CeilToInt(number % srcCount);
    int yN = Mathf.CeilToInt(number / srcCount);
    float currentSize = srcSize + 0.0038f;
    uvs[x * 4 + y * count * 4].Set(0.0019f + currentSize * xN, 0.9981f - currentSize * yN);
    uvs[x * 4 + y * count * 4 + 1].Set(0.0019f + currentSize * xN, 0.9981f - currentSize * yN - srcSize);
    uvs[x * 4 + y * count * 4 + 2].Set(0.0019f + currentSize * xN + srcSize, 0.9981f - currentSize * yN - srcSize);
    uvs[x * 4 + y * count * 4 + 3].Set(0.0019f + currentSize * xN + srcSize, 0.9981f - currentSize * yN);
    currentMesh.uv = uvs;
    }

    void CheckRound(int x, int y)
    {
        Vector4i around = new Vector4i(0,0,0,0);
        int result = 0;
        int penult = count - 1;
        if (GetData(x,y).type == MiniMapCellType.None)
        {

            if (y < penult)
            if (CheckFogCell(x, y + 1))
                around.x = 1; //right
            if (y > 1)
            if (CheckFogCell(x, y - 1))
               around.y = 1; //left 
            if (x < penult)
            if (CheckFogCell(x + 1, y))
               around.w = 1; //down
            if (x > 1)
            if (CheckFogCell(x - 1, y))
                around.z = 1; //top
   
            result = around.y + around.w * 2 + around.x * 4 + around.z * 8;

               miniMapData[x + y * count].noneType = (NoneType)result;
                SetCellFog(x, y, 48 + result);
            
        }
        
    }

    void CheckRoundAlt(int x, int y)
    {
        int penult = count - 1;
        
        if (x<penult && y<penult)
            if (CheckFogCell(x + 1, y + 1) && CheckCornerFogCell(x, y + 1) && CheckCornerFogCell(x + 1, y))
            SetCellFog(x, y, 44);
        if (x > 1 && y > 1)
            if (CheckFogCell(x - 1, y - 1) && CheckCornerFogCell(x, y - 1) && CheckCornerFogCell(x - 1, y))
            SetCellFog(x, y, 46);
        
        if (x > 1 && y < penult)
            if (CheckFogCell(x - 1, y + 1) && CheckCornerFogCell(x - 1, y) && CheckCornerFogCell(x, y + 1))
            SetCellFog(x, y, 45);
         
        if (x < penult && y > 1)
            if (CheckFogCell(x + 1, y - 1) && CheckCornerFogCell(x + 1, y) && CheckCornerFogCell(x, y - 1))
            SetCellFog(x, y, 43);
    }

    private MiniMapData GetData(int x, int y)
    {
        return miniMapData[x + y * count];
    }

    private bool CheckFogCell(int x, int y)
    {
        MiniMapData data = GetData(x, y);
        if (data.type != MiniMapCellType.None && data.isDiscovered)
            return true;
        else
            return false;
    }

    private bool CheckCornerFogCell(int x, int y)
    {
        MiniMapData data = GetData(x, y);
        if (data.type == MiniMapCellType.None)
            return true;
        else
            return false;
    }

    public Mesh CreateMesh()
    {
        //Debug.Log("Create");
        Mesh mesh = new Mesh();
        mesh = Plane(new Vector3(startPosition.x * (2.0f / size), -0.25f, startPosition.z * (2.0f / size)), 2.0f, count);
        mesh.name = "ProceduralQuad";


        miniMapMesh = new GameObject();
        miniMapMesh.name = "MiniMapMesh";
        miniMapMesh.layer = LayerMask.NameToLayer("Minimap");
        MeshFilter mf = miniMapMesh.AddComponent<MeshFilter>();
        mf.mesh = mesh;
        MeshRenderer mr = miniMapMesh.AddComponent<MeshRenderer>();
        mr.sharedMaterial = currentMat;

        

        return mf.mesh;



    }

    private Mesh Quad(Vector3 origin, float size)
    {
        float currentSize = srcSize + 0.0038f;
        Vector3 normal = Vector3.up;
        Mesh mesh = new Mesh
        {
            vertices = new[] { origin, new Vector3(origin.x, origin.y, origin.z + size), new Vector3(origin.x + size, origin.y, origin.z + size), new Vector3(origin.x + size, origin.y, origin.z) },
            normals = new[] { normal, normal, normal, normal },

            uv = new[] { new Vector2(0.0019f, 0.9981f - currentSize * 5), new Vector2(0.0019f, 0.9981f - (currentSize * 6 + srcSize)), new Vector2(0.0019f + srcSize, 0.9981f - (currentSize * 6 + srcSize)), new Vector2(0.0019f + srcSize, 0.9981f - currentSize * 5) },
            triangles = new[] { 0, 1, 2, 0, 2, 3 }
        };

        

        return mesh;
    }

    private Mesh Plane(Vector3 origin, float size, int count)
    {
        CombineInstance[] combine = new CombineInstance[count * count];

        int i = 0;
        for (int x = 0; x < count; x++)
        {
            for (int y = 0; y < count; y++)
            {
                combine[i].mesh = Quad(new Vector3(origin.x + size * x, origin.y, origin.z + size * y), size);
                i++;
            }
        }

        Mesh mesh = new Mesh();
        mesh.CombineMeshes(combine, true, false);
        return mesh;
    }
}
