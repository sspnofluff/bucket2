﻿using UnityEngine;
using System.Collections;

public class TouchController : MonoBehaviour 
{
    protected Vector2 _controllerPosition;

    public GUISkin guiSkin;

    private bool onTouch = false;
    private int touchIndex = -1;
    protected Touch touch;

    protected float roundOutDist;
    protected float roundOutDistSqr;
    protected GUITexture guiTex;
    protected float size;

    public Texture Default;
    
    public Vector2 controllerPos;
    public Vector2 controllerSize;

    protected string hui = "";
       

	void Start ()
    {
        Initialize();
	}

    public virtual void Initialize()
    {
        Input.simulateMouseWithTouches = true;
		Main.instance = FindObjectOfType<Main> ();
        size = Main.instance.guiSize;

        guiTex = transform.GetComponent<GUITexture>();
        guiTex.texture = Default;
        guiTex.pixelInset = new Rect(controllerPos.x * size, controllerPos.y * size, controllerSize.x * size, controllerSize.y * size);

        
        roundOutDist = (controllerSize.x - 2.0f) * 0.5f * size;
        roundOutDistSqr = roundOutDist * roundOutDist;
    }
	

	void Update ()
    {
        if (Main.instance.isTouchPad)
            TouchPadInput();
        else
            MouseInput();
	}

    private void TouchPadInput()
    {
        if (Input.touchCount > 0)
            for (int i = 0; i < Input.touchCount; i++)
            {
                Vector2 touchPosition;
                touch = Input.GetTouch(i);
                touchPosition.x = touch.position.x - _controllerPosition.x;
                touchPosition.y = touch.position.y - _controllerPosition.y;

                if (CheckHit(touchPosition))
                {
                    if (!onTouch && touch.phase == TouchPhase.Began)
                    {
                        onTouch = true;
                        touchIndex = i;
                    }

                    if (touchIndex == i)
                    {
                       Calculate(touchPosition);
                    }
                }
                else
                    if (touchIndex == i)
                         TouchOff();

                if ((onTouch && touch.phase == TouchPhase.Ended && touchIndex == i))
                {
                    onTouch = false;
                    touchIndex = -1;
                    TouchOff();
                }

            }
    }

    private void MouseInput()
    {
        Vector2 mousePosition;
        mousePosition.x = Input.mousePosition.x - _controllerPosition.x;
        mousePosition.y = Input.mousePosition.y - _controllerPosition.y;
        
        if (CheckHit(mousePosition))
        {
            if (!onTouch)
            {
                onTouch = true;
            }
         
            Calculate(mousePosition);
        
        }
        else
        {
            if (onTouch)
            {
                onTouch = false;
                TouchOff();
            }
        }
        
            
        


    }

    public virtual bool CheckHit(Vector2 position)
    {
        if (position.x * position.x + position.y * position.y <= roundOutDistSqr)
            return true;
        else
            return false;
    }

    public virtual void TouchOff()
    {
        //not delete
    }

    public virtual void Calculate(Vector2 position)
    {
        //not delete
    }
}
