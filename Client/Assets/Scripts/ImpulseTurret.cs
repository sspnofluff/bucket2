﻿using UnityEngine;
using System.Collections;

public class ImpulseTurret : MachineGun
{
    float currentVelocitySecond = 0.0f;
    public Gun gunSecond;
    GameObject effectFireLeft;
    GameObject effectFireRight;
	// Use this for initialization
    void Update()
    {
        WeaponUpdate();
    }

    public override void WeaponUpdate()
    {
        if (gunSecond.master == null)
            gunSecond.master = Main.instance.hero;
        if (Main.instance.gameMode == Mode.Battle)
        {
            //gun.currentTransform.localRotation = Quaternion.identity;
            //gun.currentTransform.localRotation = Quaternion.Euler(new Vector3(0.0f, 0.0f/*Mathf.Clamp(Mathf.Abs(angle), 0, 180.0f)*/, 0.0f));
            //gunSecond.currentTransform.localRotation = Quaternion.Euler(new Vector3(0.0f, -Mathf.Clamp(Mathf.Abs(angle), 0, 180.0f), 0.0f));
            //gun.currentTransform.rotation = Quaternion.LookRotation(Vector3.Normalize(mousePos - gun.currentTransform.position));
            //gunSecond.currentTransform.rotation = Quaternion.LookRotation(Vector3.Normalize(mousePos - gunSecond.currentTransform.position));

            
            Vector3 relative = currentTransform.InverseTransformPoint(mousePos);
            if (relative.x > 0)
            {
                Quaternion rot = Quaternion.LookRotation(Vector3.Normalize(mousePos - gun.currentTransform.position));
                Vector3 rotV = rot.eulerAngles;
                gun.currentTransform.rotation = rot;
                gunSecond.currentTransform.rotation = rot;

                gun.currentTransform.localRotation = Quaternion.Euler(gun.currentTransform.localEulerAngles.x, Mathf.Clamp(Mathf.Abs(angle), 0, 180.0f), gun.currentTransform.localEulerAngles.z);
                gunSecond.currentTransform.localRotation = Quaternion.Euler(gunSecond.currentTransform.localEulerAngles.x, -Mathf.Clamp(Mathf.Abs(angle), 0, 180.0f), gunSecond.currentTransform.localEulerAngles.z);

                
            }
            else
            {
                Quaternion rot = Quaternion.LookRotation(Vector3.Normalize(mousePos - gunSecond.currentTransform.position));
                Vector3 rotV = rot.eulerAngles;
                gun.currentTransform.rotation = rot;
                gunSecond.currentTransform.rotation = rot;

                gun.currentTransform.localRotation = Quaternion.Euler(gun.currentTransform.localEulerAngles.x, Mathf.Clamp(Mathf.Abs(angle), 0, 180.0f), gun.currentTransform.localEulerAngles.z);
                gunSecond.currentTransform.localRotation = Quaternion.Euler(gunSecond.currentTransform.localEulerAngles.x, -Mathf.Clamp(Mathf.Abs(angle), 0, 180.0f), gunSecond.currentTransform.localEulerAngles.z);

            }
            

            ///gun.currentTransform.localRotation = Quaternion.Euler(new Vector3(0.0f, Mathf.SmoothDampAngle(gun.currentTransform.localEulerAngles.y, Mathf.Clamp(Mathf.Abs(angle), 0, 180.0f), ref currentVelocity, rotateSpeed)));
           
          // gunSecond.currentTransform.localRotation = Quaternion.Euler(new Vector3(0.0f, Mathf.SmoothDampAngle(gunSecond.currentTransform.localEulerAngles.y, -Mathf.Clamp(Mathf.Abs(angle), 0, 180.0f), ref currentVelocitySecond, rotateSpeed)));

        }
    }

    public override void CreateAutoEffect()
    {
        if (!isEffectCreated && effectFire != null)
        {
            isEffectCreated = true;
            effectFireLeft = GameObject.Instantiate(effectFire, currentTransform.TransformPoint(gun.shootPosition), currentTransform.rotation) as GameObject;
            effectFireLeft.transform.parent = currentTransform;
            effectFireRight = GameObject.Instantiate(effectFire, currentTransform.TransformPoint(gunSecond.shootPosition), currentTransform.rotation) as GameObject;
            effectFireRight.transform.parent = currentTransform;
        }

    }
    public override void StopShoot()
    {
        if (isAttack)
        {
            weaponState = WeaponState.Stop;
            isAttack = false;
            DestroyAutoEffect();
        }
    }

    public override void Reset()
    {
        base.Reset();
        gun.currentTransform.localRotation = Quaternion.identity;
        gunSecond.currentTransform.localRotation = Quaternion.identity;
        gunSecond.magazine.Restore();
        gunSecond.recharge.Restore();
        DestroyAutoEffect();
    }

    public override void DestroyAutoEffect()
    {
        if (isEffectCreated)
        {

            Destroy(effectFireLeft);
            Destroy(effectFireRight);
            isEffectCreated = false;
        }


    }

    public override void GunShoot()
    {
        Vector3 shootPosition = gun.GetShootPosition(gun.currentTransform);
        Vector3 shootRotation = gun.currentTransform.forward;
        //Unit target = Aim(shootRotation, shootPosition, aimRange);
        gun.ShootBurst(null, shootRotation, shootPosition);
        
        Vector3 shootPosition2 = gunSecond.GetShootPosition(gunSecond.currentTransform);
        Vector3 shootRotation2 = gunSecond.currentTransform.forward;
        //Unit target2 = Aim(shootRotation2, shootPosition2, aimRange);
        gunSecond.ShootBurst(null, shootRotation2, shootPosition2);

        Main.instance._temp++;

    }
}
