﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ArmorData : MonoBehaviour {

    public RectTransform currentRectTransform;

    public Text first;
    public Text two;
    public Text three;

    public Text labels;
}
