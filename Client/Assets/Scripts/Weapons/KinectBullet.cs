﻿using UnityEngine;
using System.Collections;

public class KinectBullet : Bullet
{
    public Object crater;
    
    public override void Initialize(Vector3 _position, Vector3 _direction, Vector3 _parentDir, float _parentSpeed, float _range, Unit _master, float _damage, float _mass)
    {
        attackDir = _direction;
        master = _master;

        attackDir.x += Random.Range(-offset, offset);
        attackDir.y += Random.Range(-offset, offset);
        attackDir.z += Random.Range(-offset, offset);

        parentDir = _parentDir;
        parentSpeed = _parentSpeed;
        startPosition = _position;
        range = _range;
        if (muzzleEffect != null)
        {
            muzzle = GameObject.Instantiate(muzzleEffect, _position, currentTransform.rotation) as GameObject;
            muzzle.transform.parent = master.currentTransform;
        }

    }

    public override void SelectTarget(float _distance, Vector3 _targetStartPosition, Vector3 _normal, Vector3 _endPosition, Unit _target, float _damage, float _mass)
    {

        distance = _distance;
        targetStartPosition = _targetStartPosition;
        normal = _normal;
        endPosition = _endPosition;
        if (_target != null)
            target = _target;
        bulletDirection = BulletDirection.Target;

        damage = _damage;
        mass = _mass;

        offsetDirection.x = -normal.x + Random.Range(-offset, offset);
        offsetDirection.y = -normal.y + Random.Range(-offset, offset);
        offsetDirection.z = -normal.z + Random.Range(-offset, offset);

        offsetDirection.Normalize();

    }

    public override void SelectTarget(float _distance, Vector3 _targetStartPosition, Vector3 _normal, Vector3 _endPosition, OneTarget _target, float _damage, float _mass)
    {

        distance = _distance;
        targetStartPosition = _targetStartPosition;
        normal = _normal;
        endPosition = _endPosition;
        targetDecal = _target;
        controlPosition = targetDecal.currentTransform.InverseTransformPoint(_endPosition);
        bulletDirection = BulletDirection.Position;
        

        damage = _damage;
        mass = _mass;

        offsetDirection.x = -normal.x + Random.Range(-offset, offset);
        offsetDirection.y = -normal.y + Random.Range(-offset, offset);
        offsetDirection.z = -normal.z + Random.Range(-offset, offset);

        offsetDirection.Normalize();

    }

    void Update()
    {
        if (Main.instance.gameMode != Mode.Battle)
            Destroy(this.gameObject);

        float currentDistance = 0;
        float currentTargetDistance = 0;

        switch (bulletDirection)
        {
            case BulletDirection.None:

                currentDistance = Vector3.Distance(startPosition, currentTransform.position);
                currentTargetDistance = range;

                break;

            case BulletDirection.Position:

                currentDistance = Vector3.Distance(startPosition, currentTransform.position);
                currentTargetDistance = Vector3.Distance(startPosition, endPosition);

                break;

            case BulletDirection.Target:

                currentDistance = Vector3.Distance(startPosition, currentTransform.position);
                if (!target)
                    currentTargetDistance = Vector3.Distance(startPosition, endPosition);
                else
                    currentTargetDistance = Vector3.Distance(startPosition, endPosition - (targetStartPosition - target.currentTransform.position));

                break;
        }

        if (currentDistance > currentTargetDistance)
        {
            switch (bulletDirection)
            {
                case BulletDirection.Position:

                    currentTransform.position = endPosition;

                    if (targetDecal != null)
                    {
                        Vector3 damagePosition = targetDecal.currentTransform.TransformPoint(controlPosition);

                        if (impactEffect != null)
                        {
                            impact = GameObject.Instantiate(impactEffect, damagePosition, Quaternion.LookRotation(normal)) as GameObject;
                            //impact.transform.LookAt(damagePosition + normal);
                            
                            //impact.transform.parent = Main.instance.mainCamera.controlPointDot.currentTransform;
                            if (crater!=null)
                            GameObject.Instantiate(crater, damagePosition, Quaternion.LookRotation(normal));
                            //impact.transform.LookAt(damagePosition + normal);
                            impact.transform.localRotation = Quaternion.Euler(new Vector3(impact.transform.localEulerAngles.x, Random.Range(0, 360.0f), impact.transform.localEulerAngles.z));
                        }

                        Collider[] victims = Physics.OverlapSphere(damagePosition, radiusBullet);

                        for (int i = 0; i < victims.Length; i++)
                        {
                            
                            if (victims[i].gameObject.CompareTag("Unit"))
                            {

                                Unit unit = victims[i].transform.parent.GetComponent<Unit>();
                                if (unit != null)
                                {
                                    if (master == Main.instance.hero)
                                    {
                                        unit.AttackByHero();
                                        unit.currentRigidbody.AddExplosionForce(1300.0f, currentTransform.position, radiusBullet, 100.0f, ForceMode.Impulse);
                                        Main.instance.SelectTarget(unit);
                                        Main.instance._temp++;
                                    }
                                    
                                    unit.GetDamage(damagePosition, damage * Vector3.Normalize(unit.currentTransform.position - damagePosition));

                                       
                                    
                                }

                            }
                        }
                    }

                    break;

                case BulletDirection.Target:

                    if (target != null)
                    {

                        currentTransform.position = endPosition - (targetStartPosition - target.currentTransform.position);
                        if (master == Main.instance.hero)
                            target.AttackByHero();
                        target.GetDamage(currentTransform.position, damage * currentTransform.forward);
                        target.currentRigidbody.AddForce(mass * 10.0f * Vector3.Normalize(target.currentTransform.position - targetStartPosition), ForceMode.Force);

                        if (impactEffect != null/* && Random.Range(0, 2) == 0*/)
                        {
                            impact = GameObject.Instantiate(impactEffect, currentTransform.position, Quaternion.LookRotation(normal)) as GameObject;
                            //impact.transform.LookAt(currentTransform.position + normal);
                            if (target)
                                impact.transform.parent = target.currentTransform;
                        }

                        if (master.unitType == UnitType.Hero)
                            Main.instance.SelectTarget(target);
                        
                    }
                    break;
            }

            Destroy(this.gameObject);
        }
        else
        {
            currentTransform.position += parentDir * parentSpeed * Time.smoothDeltaTime;
            currentTransform.position += attackDir * attackSpeed;
        }




    }
}
