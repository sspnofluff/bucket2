﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Dest.Math;

public class Missile : MonoBehaviour {

    public float launchSmokeTimer = 0.10f;
    public float activateTimer = 0.5f;
    public CounterTimerF targetTimer;

    bool onTarget = false;
    bool onActivate = false;
    bool onLaunch = false;

    public float range;
    public float radius;
    public float damage;
    public float upperForce;
    public float aimRange;
    public Object explosive;

    public string name;
    public string type;

    Unit target;

    public Transform currentTransform;
    public Collider currentCollider;
    public Vector3 parentDir;
    public float parentSpeed;
    public Vector3 newRotation;
    public TrailRenderer trail;
    public ParticleSystem engine;
    public ParticleSystem flash;
    public ParticleSystem flash02;
    public ParticleSystem sparks;
    public Rigidbody currentRigidbody;

    private Vector3 ExplosivePosition = Vector3.zero;

    Vector3 Dir;
    Vector3 diff;
    //Vector3 _rot = Vector3.zero;

	// Use this for initialization
	void Start () {
        onTarget = false;
        currentRigidbody.AddForce(transform.forward * 30.0f, ForceMode.Impulse);
	}

    void FixedUpdate()
    {
        Vector3 roadDirection = Main.instance.hero.roadDirection;
        if (!onTarget)
        {

            currentRigidbody.MoveRotation(Quaternion.LookRotation(Vector3.Normalize(currentRigidbody.velocity)));

            Vector3 Relative = currentTransform.InverseTransformDirection(roadDirection);
            Vector3 Velocity = currentTransform.InverseTransformDirection(currentRigidbody.velocity.normalized);
            Vector3 Result = Relative - Velocity;

            currentRigidbody.AddRelativeForce(Result * 10 * currentRigidbody.velocity.magnitude);

            currentRigidbody.AddForce(roadDirection * 1000.0f * Time.deltaTime, ForceMode.Acceleration);
        }
        else
        {
            if (target != null)
            {
                Vector3 targetDelta = (target.centerOfMass.position) - currentTransform.position;
                Dir = Vector3.Normalize(targetDelta);
                diff = (Dir - Vector3.Normalize(currentRigidbody.velocity));
                if (diff.y > 0)
                    currentRigidbody.AddForce(new Vector3(0, 1, 0) * 40.0f, ForceMode.Force);
                if (diff.y < 0)
                    currentRigidbody.AddForce(new Vector3(0, -1, 0) * 40.0f, ForceMode.Force);
                if (diff.x > 0)
                    currentRigidbody.AddForce(new Vector3(1, 0, 0) * 40.0f, ForceMode.Force);
                if (diff.x < 0)
                    currentRigidbody.AddForce(new Vector3(-1, 0, 0) * 40.0f, ForceMode.Force);
                currentRigidbody.MoveRotation(Quaternion.LookRotation(target.currentTransform.position));
                
                if (currentRigidbody.velocity.magnitude<40.0f)
                    currentRigidbody.AddForce(Dir * 700f * Time.deltaTime, ForceMode.Acceleration);
            }
        }
 

        if (!onLaunch)
        {
            launchSmokeTimer -= Time.deltaTime;
            if (launchSmokeTimer < 0)
            {
                trail.enabled = false;
                engine.enableEmission = false;
                onLaunch = true;
                flash.enableEmission = false;
                flash02.enableEmission = false;
                sparks.enableEmission = false;
            }
        }

        if (!onActivate)
        {
            activateTimer -= Time.deltaTime;
            if (activateTimer < 0)
            {
                trail.enabled = true;
                engine.enableEmission = true;
                onActivate = true;
                currentCollider.enabled = true;
                flash.enableEmission = true;
                flash02.enableEmission = true;
                sparks.enableEmission = true;
            }
        }

        
        if (!onTarget)
        {
            if (targetTimer.isEnd(Time.deltaTime))
            {
                Activate();
            }
        }

    }

	void Update()
    {
        /*
        if (target != null)
        {
           // Debug.DrawLine(currentTransform.position, currentTransform.position + Dir * 5.0f, Color.red);
           // Debug.DrawLine(currentTransform.position, currentTransform.position + currentTransform.forward * 5.0f, Color.blue);
            //Debug.Log(gameObject.GetInstanceID() + " " + diff + currentRigidbody.velocity.magnitude);
        }
         */ 
    }
	// Update is called once per frame
	

    

    #if UNITY_EDITOR
    void OnDrawGizmos()
    {
    //Gizmos.color = Color.green;
    //TrafficController.DrawCircle(ExplosivePosition, 5.0f);
    }
    #endif

    void Explosive(Vector3 Position)
    {

        Collider[] victims = Physics.OverlapSphere(Position, 5.0f);
        for (int i = 0; i < victims.Length; i++)
        {
            if (victims[i].gameObject.CompareTag("Unit"))
            {
                Unit unit = victims[i].transform.parent.GetComponent<Unit>();

                if (unit.unitType == UnitType.Enemy)
                {
					if (unit.type == EnemyType.Scorcher)
                    {
                        unit.GetComponent<Scorcher>().GetPhysic();
                    }

                    unit.GetDamage(Position, 5000.0f * Vector3.Normalize(unit.currentTransform.position - Position));
                }
                if (victims[i].attachedRigidbody!=null)
                victims[i].attachedRigidbody.AddExplosionForce(4000.0f, Position, 5.0f, 100.0F, ForceMode.Impulse);
                
            }
        }

        GameObject.Instantiate(explosive, Position, Quaternion.identity);
        GameObject.Destroy(gameObject);
    }
    

    void Activate()
    {
        Vector2 aimVec = new Vector2(currentTransform.forward.x, currentTransform.forward.z);
        //aimVec.Normalize();
        Vector2 right = new Vector2(currentTransform.right.x, currentTransform.right.z);
        Vector2 aimTriangleStartPos = new Vector2(currentTransform.position.x, currentTransform.position.z);
        aimTriangleStartPos = aimVec * 15.0f + aimTriangleStartPos;
        Vector2 aimTriangleRangePos = aimVec * range + aimTriangleStartPos;
        Vector2 aimTriangleLeftPos = aimTriangleRangePos + right * aimRange;
        Vector2 aimTriangleRightPos = aimTriangleRangePos - right * aimRange;

        Triangle2 current = new Triangle2(aimTriangleLeftPos, aimTriangleStartPos, aimTriangleRightPos);

        Debug.DrawLine(new Vector3(aimTriangleStartPos.x, currentTransform.position.y, aimTriangleStartPos.y), new Vector3(aimTriangleLeftPos.x, currentTransform.position.y, aimTriangleLeftPos.y), Color.red, 1.0f);
        Debug.DrawLine(new Vector3(aimTriangleStartPos.x, currentTransform.position.y, aimTriangleStartPos.y), new Vector3(aimTriangleRightPos.x, currentTransform.position.y, aimTriangleRightPos.y), Color.red, 1.0f);
        Debug.DrawLine(new Vector3(aimTriangleLeftPos.x, currentTransform.position.y, aimTriangleLeftPos.y), new Vector3(aimTriangleRightPos.x, currentTransform.position.y, aimTriangleRightPos.y), Color.red, 1.0f);

        List<Unit> targets = new List<Unit>();

        float minimum =10000.0f;
        int select = -1;
        int count = Main.instance.unitObjects.Count;
        for (int i = 0; i < count; i++)
        {
            if (Main.instance.unitObjects[i].currentGameObject.activeInHierarchy && Main.instance.unitObjects[i].isActive)
            {
                Box2 box = TrafficController.CreateBox2(Main.instance.unitObjects[i].currentTransform, Main.instance.unitObjects[i].currentCollider);
                Vector2[] points = box.CalcVertices();
                if (Math3d.isTriRectIntersect(aimTriangleLeftPos, aimTriangleRightPos, aimTriangleStartPos, points[0], points[1], points[2], points[3]))
                {
                    //Debug.DrawLine(Main.instance.unitObjects[i].currentTransform.position, Main.instance.unitObjects[i].currentTransform.position + Vector3.up * 8.0f, Color.magenta, 5.0f);
                    targets.Add(Main.instance.unitObjects[i]);
                    float min = Mathf.Abs(currentTransform.InverseTransformPoint(Main.instance.unitObjects[i].currentTransform.position).x);
                    if (min < minimum)
                    {
                        select = i;
                        minimum = min;
                    }

                }

            }

        }

        if (select > -1)
        {
        target = Main.instance.unitObjects[select];
        onTarget = true;

        }




    }

    void OnCollisionEnter(Collision collision)
    {
        ExplosivePosition = collision.contacts[0].point;
        Explosive(ExplosivePosition);

    }
}
