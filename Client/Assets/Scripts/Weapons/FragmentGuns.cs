﻿using UnityEngine;
using System.Collections;
using Dest.Math;

public class FragmentGuns : Weapon
{
    bool isInitializeDecal = false;
    private Vector3 inversePoint;
    
    public OneArcMachine targetDecalSource;
    public OneArcMachine targetDecal;
    public Animation currentAnimation;

    public float minDist;
    public float maxDist;
    public float azimut;
    void FixedUpdate()
    {
        if (Main.instance.gameMode == Mode.Battle)
        {
            if (!targetDecal)
            {
                targetDecal = Instantiate<OneArcMachine>(targetDecalSource);
                targetDecal.Create(20.0f, Main.instance.hero.currentTransform);
                targetDecal.currentTransform.parent = Main.instance.mainCamera.targetController.currentTransform;
                targetDecal.targetController = Main.instance.mainCamera.targetController;
            }

            targetDecal.currentTransform.localPosition = targetDecal.targetController.currentTransform.InverseTransformPoint(mousePos);
            targetDecal.currentTransform.LookAt(Main.instance.hero.currentTransform);
            Vector3 dir = Vector3.Normalize(targetDecal.currentTransform.position - Main.instance.hero.currentTransform.position);
            float angle = Math3d.GetAngle(Main.instance.hero.currentTransform.forward.ToVector2XZ(), dir.ToVector2XZ());
            float dist = Vector3.Distance(Main.instance.hero.currentTransform.position, targetDecal.currentTransform.position);
            if (Mathf.Abs(angle) < azimut && dist > minDist && dist < maxDist)
                targetDecal.currentTransform.Find("decalIn").gameObject.SetActive(true);
            else
                targetDecal.currentTransform.Find("decalIn").gameObject.SetActive(false);

            if (isAttack)
            {

                UpdateGun();

                switch (weaponState)
                {
                    case WeaponState.Start:
                        break;

                    case WeaponState.Stop:
                        currentAnimation.Stop("Shoot");
                        break;

                    case WeaponState.Attack:
                        currentAnimation.PlayQueued("Shoot", QueueMode.CompleteOthers);
                        break;

                    case WeaponState.Recharge:
                        currentAnimation.Stop("Shoot");
                        break;
                }
            }
        }

    }

    public override void Reset()
    {
        StopShoot();
        currentAnimation.Stop("Shoot");
        currentTransform.localRotation = Quaternion.identity;
    }


    public override void Remove()
    {
        if (targetDecal!=null)
        Destroy(targetDecal.gameObject);
    }
    public override void GunShoot()
    {
        Vector3 shootPosition = gun.GetShootPosition(currentTransform);
        Vector3 shootRotation = currentTransform.forward;
        Unit target = Aim(shootRotation, shootPosition, aimRange);
        Vector3 dir = Vector3.Normalize(targetDecal.currentTransform.position - Main.instance.hero.currentTransform.position);
        float angle = Math3d.GetAngle(Main.instance.hero.currentTransform.forward.ToVector2XZ(), dir.ToVector2XZ());
        float dist = Vector3.Distance(Main.instance.hero.currentTransform.position, targetDecal.currentTransform.position);
        Vector3 targetPos = targetDecal.currentTransform.position;
        //targetPos.x += Random.Range(-1, 1);
        //targetPos.y += Random.Range(-1, 1);
        //targetPos.z += Random.Range(-1, 1);
        if (Mathf.Abs(angle) < azimut && dist > minDist && dist < maxDist)
        {
            gun.ShootVisualBurst(shootRotation, shootPosition, targetPos, targetDecal);
            Main.instance._temp++;
        }
       // else
           // gun.ShootBurst(null, shootRotation, shootPosition);
    }

}
