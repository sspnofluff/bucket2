﻿using UnityEngine;
using System.Collections;

public class Bomb : Shell
{

    public override void Moving()
    {
        if (lifeTime.isEnd(Time.deltaTime))
        {
            Destroy(gameObject);
        }

        if (oneTarget != null)
        {
            float time = lifeTime.current / lifeTime.max;

            currentTransform.position = Vector3.Lerp(oneTarget.currentTransform.position, cameraController.TransformPoint(startPos), time);
            currentTransform.rotation = Quaternion.LookRotation(Vector3.Normalize(oneTarget.currentTransform.position - currentTransform.position));
        }

      }

    void Update()
    {
        Moving();

    }

}
