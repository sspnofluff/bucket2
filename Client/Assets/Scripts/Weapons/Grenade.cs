﻿using UnityEngine;
using System.Collections;
using Dest.Math;

public class Grenade : Shell {

    private float yTop;
    private float RotateAngle = 0;
    private bool isCreate = false;
    private Vector3 pos;
    private Vector3 _currentSmoothVelocityPosition = Vector3.zero;

    public void Create()
    {
        pos = cameraController.InverseTransformPoint(master.position);
        yTop = Vector2.Distance(cameraController.TransformPoint(pos).ToVector2XZ(), oneTarget.currentTransform.position.ToVector2XZ()) / Random.Range(2.7f, 3.3f);
        isCreate = true;
    }

    public override void Moving()
    {
        if (!isCreate)
            Create();

        if (lifeTime.isEnd(Time.deltaTime))
        {
            Destroy(gameObject);
        }

        if (oneTarget != null)
        {
            float time = (1-lifeTime.current / lifeTime.max);
            float height = Mathf.Sin(Mathf.PI * time) * yTop;

            RotateAngle += Time.deltaTime * 200.0f;

           // Vector3 currentPos =  Vector3.Lerp(cameraController.TransformPoint(pos), oneTarget.currentTransform.position, time) + Vector3.up * height;
           // currentTransform.position = Vector3.SmoothDamp(currentTransform.position, currentPos, ref _currentSmoothVelocityPosition, 0.025f);

            currentTransform.localPosition = Vector3.Lerp(pos, oneTarget.currentTransform.localPosition, time) + Vector3.up * height;
            currentTransform.localRotation = Quaternion.AngleAxis(RotateAngle, currentTransform.right);
        }

      }

    void Update()
    {
        Moving();

    }

}
