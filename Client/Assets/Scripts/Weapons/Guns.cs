﻿using UnityEngine;
using System.Collections;

public class Guns : Weapon
{
    public Animation currentAnimation;
    void FixedUpdate()
    {
        if (isAttack)
        {
            UpdateGun();

            switch (weaponState)
            {
                case WeaponState.Start:
                    break;

                case WeaponState.Stop:
                    currentAnimation.Stop("Shoot");
                    break;

                case WeaponState.Attack:
                    currentAnimation.PlayQueued("Shoot", QueueMode.CompleteOthers);
                    break;

                case WeaponState.Recharge:
                    currentAnimation.Stop("Shoot");
                    break;
            }
        }


    }
}
