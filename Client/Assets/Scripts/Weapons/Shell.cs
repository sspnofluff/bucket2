﻿using UnityEngine;
using System.Collections;

public abstract class Shell : MonoBehaviour {

    public Transform currentTransform;
    public Transform cameraController;
    public Transform master;
    public Vector3 startPos;
    public CounterTimerF lifeTime;
    public OneTarget oneTarget;

    public abstract void Moving();

    

}
