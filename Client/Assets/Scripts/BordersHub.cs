﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BordersHub : MonoBehaviour {

	public MeshFilter currentMeshFilter;
	public MeshFilter[] borderSources;
	public Transform currentTransform;
	public BoundsCorrector boundsCorrector;
	public BlockPlane master;

	public List<BakedProps> borderData;
#if UNITY_EDITOR
	public static void RemoveBorders()
	{
		
		GameObject[] currentBorders = Selection.gameObjects;
			int count = currentBorders.Length;
		BakedProps[] temp = new BakedProps[count];
		for (int i = 0; i < count; i++)
			temp [i] = currentBorders [i].GetComponent<BakedProps> ();

		BordersHub currentHub = temp [0].transform.parent.GetComponent<BordersHub> ();
		List<int> numbers = new List<int> ();

		for (int i = 0; i < count; i++) 
		{

			currentHub.borderData.Remove (temp [i]);
			DestroyImmediate (temp [i].gameObject);

;

		}




	}

	#endif

	public void ChangeSides()
	{

		List<BakedProps> toDelete = new List<BakedProps> ();

		for (int i = 0; i < borderData.Count; i++) 
		{
			if ((borderData [i].previous == null || borderData [i].next == null) && !borderData [i].edge) 
			{
				
				toDelete.Add (borderData [i]);
				//break;
			}
				
		}

		for (int i=0; i<toDelete.Count; i++)
			ChangeBakedProps (toDelete [i], Random.Range(1,master.customBorders.Count+1));
	}

	public void ChangeBakedProps(BakedProps current, int type)
	{
		Vector3 pos = current.transform.position;
		Quaternion rot = current.transform.rotation;

		borderData.Remove (current);
		BakedProps newProps = Instantiate<BakedProps> ((type == 0) ? master.mainBorder : master.customBorders[type-1], pos, rot);
		newProps.Create (master, type);
		newProps.transform.parent = currentTransform;
		borderData.Add (newProps);

		newProps.next = current.next;
		newProps.previous = current.previous;

		DestroyImmediate (current.gameObject);
	}
}
