﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class BonusElement
{
    public Bonus bonus;
    private Vector3 basicPosition;
    private Quaternion basicRotation;

    public BonusElement(Bonus _bonus, Vector3 _basicPosition, Quaternion _basicRotation)
    {
        bonus = _bonus;
        basicPosition = _basicPosition;
        basicRotation = _basicRotation;
    }

    public void Recreate(Object _bonusSource, Transform master)
    {
        GameObject current = (GameObject)GameObject.Instantiate(_bonusSource, master.TransformPoint(basicPosition), basicRotation) as GameObject;
        bonus = current.GetComponent<Bonus>();
        bonus.currentTransform.parent = master;
        
    }
 
}

/*
public class BonusContainer : Misc {

    public bool hasRestored = false;
    public BonusElement[] elements;
    public Object bonusSource;

    void Start()
    {
        SaveElementsPositions();
    }

    public override void Initialize(bool activeObject)
    {
        base.Initialize(activeObject);

        if (hasRestored)
        Restore();
    }

    public void SaveElementsPositions()
    {
        elements = new BonusElement[currentTransform.childCount];
        for (int i = 0; i < currentTransform.childCount; i++)
        {
            Transform current = currentTransform.GetChild(i);
            elements[i] = new BonusElement(current.GetComponent<Bonus>(), current.localPosition, current.localRotation);
        }

        hasRestored = true;
    }

    public void Restore()
    {
        for (int i = 0; i < elements.Length; i++)
        {
            if (elements[i].bonus == null)
                elements[i].Recreate(bonusSource, currentTransform);
        }
    }

    public override void DestroyMisc()
    {
        Restore();
        base.DestroyMisc();
    }

}
*/