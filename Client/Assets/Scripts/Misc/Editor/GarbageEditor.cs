﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Garbage))]
public class GarbageEditor : Editor
{

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        Garbage myTarget = (Garbage)target;
        if (GUILayout.Button("Create"))
        {
            myTarget.SaveElementsPositions();
        }

        if (GUILayout.Button("Restore"))
        {
            myTarget.Restore();
        }
    }
}
