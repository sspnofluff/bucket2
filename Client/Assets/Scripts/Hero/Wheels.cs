﻿using UnityEngine;
using System.Collections;

public class Wheels : Module
{
    public Mesh sourceWheel;

    public float addSpeed;
    public float addMaxMass;

    public string additionalName;
    public string additionalType;
    public float factorSlow;
    public float resistanceSlip;

    public override void Install()
    {
        currentTransform.parent = Main.instance.hero.currentTransform;
        currentTransform.localPosition = Vector3.zero;
        currentTransform.localRotation = Quaternion.identity;
        gameObject.SetActive(true);

		for (int i=0; i<Main.instance.hero.wheels.Length; i++)
        {
            MeshFilter mf;
			mf = Main.instance.hero.wheels[i].wheelTransform.GetComponent<MeshFilter>();
        mf.mesh = sourceWheel;
        }
        
        
        
    }

    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("FF");

    }
    
}
