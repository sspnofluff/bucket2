﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Hero))]
public class HeroEditor : VehicleEditor
{

    

    public override void OnInspectorGUI()
    {
        Hero myTarget = (Hero)target;

		base.OnInspectorGUI ();

        myTarget.maxSpeedMin = EditorGUILayout.FloatField("Max Speed Min", myTarget.maxSpeedMin);
        myTarget.averageSpeedMin = EditorGUILayout.FloatField("Average Speed Min", myTarget.averageSpeedMin);
        myTarget.minSpeedMin = EditorGUILayout.FloatField("Min Speed Min", myTarget.minSpeedMin);
        myTarget.minSpeedBreakMin = EditorGUILayout.FloatField("Break Speed Min", myTarget.minSpeedBreakMin);
        myTarget.steerMin = EditorGUILayout.FloatField("Steer Min", myTarget.steerMin);
        //myTarget.stiffnesMin = EditorGUILayout.FloatField("Stiffnes Min", myTarget.stiffnesMin);
        EditorGUILayout.Separator();
        myTarget.maxSpeedMax = EditorGUILayout.FloatField("Max Speed Max", myTarget.maxSpeedMax);
        myTarget.averageSpeedMax = EditorGUILayout.FloatField("Average Speed Max", myTarget.averageSpeedMax);
        myTarget.minSpeedMax = EditorGUILayout.FloatField("Min Speed Max", myTarget.minSpeedMax);
        myTarget.minSpeedBreakMax = EditorGUILayout.FloatField("Break Speed Max", myTarget.minSpeedBreakMax);
        myTarget.steerMax = EditorGUILayout.FloatField("Steer Max", myTarget.steerMax);
       // myTarget.stiffnesMax = EditorGUILayout.FloatField("Stiffnes Max", myTarget.stiffnesMax);
        EditorGUILayout.Separator();
        //myTarget.minAntiRollBars = EditorGUILayout.FloatField("Min AntiRoll Bars", myTarget.minAntiRollBars);
        //myTarget.maxAntiRollBars = EditorGUILayout.FloatField("Max AntiRoll Bars", myTarget.maxAntiRollBars);
        myTarget.steerClampMin = EditorGUILayout.FloatField("Min Steer Clamp", myTarget.steerClampMin);
        myTarget.steerClampMax = EditorGUILayout.FloatField("Max Steer Clamp", myTarget.steerClampMax);
        
        EditorGUILayout.Separator();
        myTarget.currentBoost = EditorGUILayout.FloatField("Boost", myTarget.currentBoost);
        myTarget.currentAccel = EditorGUILayout.FloatField("Default", myTarget.currentAccel);
        myTarget.currentBrake = EditorGUILayout.FloatField("Brake", myTarget.currentBrake);

		EditorGUILayout.Separator();
		myTarget.isAutopilot = EditorGUILayout.Toggle ("Autopilot", myTarget.isAutopilot);
		myTarget.isCalcVertical = EditorGUILayout.Toggle ("CalcVertical", myTarget.isCalcVertical);
        /*
        if (!myTarget.isBuilding)
        {
            if (GUILayout.Button("Build Level"))
            {
                myTarget.BuildLevel();
            }
            else
                if (GUILayout.Button("Save Level"))
                {
                    myTarget.SaveLevel();
                }
        }
         */

        //if (GUILayout.Button("Check"))
        //{
        //    myTarget._topLineCollider.Check();
        //}
    }
}
