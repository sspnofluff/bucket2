﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class ComplexSpline : Spline
{
    public Spline[] splines;

    public override Vector3 getStartRot(float t)
    {
        int select = 0;
        float percent = 0;
        GetLocalSpline(t, out select, out percent);

        return splines[select].startRot;
    }

    public override Vector3 getEndRot(float t)
    {
        int select = 0;
        float percent = 0;
        GetLocalSpline(t, out select, out percent);

        return splines[select].endRot;
    }

    public override float CheckCamProcess(float t)
    {
        int select = 0;
        float percent = 0;
        GetLocalSpline(t, out select, out percent);
        return percent;
    }

    public override void CalculateLenght()
    {
        Length = 0;
        for (int i = 0; i < splines.Length; i++)
            Length += splines[i].Length;
        isCalculated = true;

        a = splines[0].a;
        startRot = splines[0].startRot;
        b = splines[splines.Length - 1].b;
        endRot = splines[splines.Length - 1].endRot;

        startWidth = splines[0].startWidth;
        endWidth = splines[splines.Length - 1].endWidth;
}

    public void GetLocalSpline(float t, out int select, out float percent)
    {
        if (!isCalculated)
            CalculateLenght();


        float _length = 0;
        select = -1;
        float t_current = t;
        for (int i = 0; i < splines.Length; i++)
        {
            _length += splines[i].Length;
            if (_length / Length >= t)
            {
                select = i;
                float percentCurrent = splines[i].Length / Length;
                percent = t_current / percentCurrent;
                return;
            }
            else
                t_current -= splines[i].Length / Length;
           
        }
        select = splines.Length-1;
        percent = t_current / (splines[select].Length / Length);
        return;
    }

#if UNITY_EDITOR
    [ContextMenu("Do Something")]
    void Check()
    {
        //Locators.CreateLocator(GetPointOnCurve(sphere.transform.position), "F");

    }
#endif
    public override Vector3 GlobalPos(float t)
    {
        int select = 0;
        float percent = 0;
        GetLocalSpline(t, out select, out percent);

        return splines[select].GlobalPos(percent);
    }

    public override float GetCurrentWidth(float t)
    {
        int select = 0;
        float percent = 0;
        GetLocalSpline(t, out select, out percent);
        return splines[select].GetCurrentWidth(percent);
    }

    public override Vector3 GetAngle(float t)
    {
        int select = 0;
        float percent = 0;
        GetLocalSpline(t, out select, out percent);

        return splines[select].GetAngle(percent);
    }

    public override float GetStep(Vector3 _pos)
    {
        if (!isCalculated)
            CalculateLenght();
        Vector3 result = Vector3.zero;
        Vector3 Point = currentTransform.InverseTransformPoint(_pos);
        float min = 1000.0f;
        float currentStep = 0;
        int currentSpline = -1;
        for (int j = 0; j < splines.Length; j++)
        {
            for (float i = 0; i < splines[j].Length; i += splines[j]._step)
            {
                Vector3 central = Math3d.CubicBezierLerp(splines[j].a, splines[j].aa, splines[j].bb, splines[j].b, Mathf.Lerp(i, i + splines[j]._step, 0.5f) / splines[j].Length);
                float dist = Vector3.Distance(Point, central);
                if (dist < min)
                {
                    min = dist;
                    currentStep = i;
                    currentSpline = j;
                }
            }
        }

        Vector3 start = Math3d.CubicBezierLerp(splines[currentSpline].a, splines[currentSpline].aa, splines[currentSpline].bb, splines[currentSpline].b, currentStep / splines[currentSpline].Length);
        Vector3 end = Math3d.CubicBezierLerp(splines[currentSpline].a, splines[currentSpline].aa, splines[currentSpline].bb, splines[currentSpline].b, (currentStep + splines[currentSpline]._step) / splines[currentSpline].Length);
        Vector3 Dir = start - end;
        Dir.Normalize();

        Vector3 Left = 10.0f * Vector3.Cross(Dir, Vector3.up) + Point;
        Vector3 Right = -10.0f * Vector3.Cross(Dir, Vector3.up) + Point;

        Math3d.CrossPointLine(start, end, Left, Right, out result);

        float dist1 = Vector2.Distance(new Vector2(start.z, start.x), new Vector2(end.z, end.x));
        float dist2 = Vector3.Distance(new Vector2(start.z, start.x), new Vector2(result.z, result.x));

        float step = dist2 / dist1;

        float res = (currentStep + step) / splines[currentSpline].Length;

        float allStep = 0;
        for (int j = 0; j < currentSpline; j++)
            allStep += splines[j].Length / Length;

        return allStep + res * (splines[currentSpline].Length / Length);
    }

    public override Vector3 GetPointOnCurve(Vector3 _pos)
    {
        if (!isCalculated)
            CalculateLenght();

        Vector3 Point = currentTransform.InverseTransformPoint(_pos);
        Vector3 result = Vector3.zero;
        float min = 1000.0f;
        float currentStep = 0;
        int currentSpline = -1;
        for (int j = 0; j < splines.Length; j++)
        {
            for (float i = 0; i < splines[j].Length; i += splines[j]._step)
            {
                Vector3 central = Math3d.CubicBezierLerp(splines[j].a, splines[j].aa, splines[j].bb, splines[j].b, Mathf.Lerp(i, i + splines[j]._step, 0.5f) / splines[j].Length);
                float dist = Vector3.Distance(Point, central);
                if (dist < min)
                {
                    min = dist;
                    currentStep = i;
                    currentSpline = j;
                }
            }
        }

        Vector3 start = Math3d.CubicBezierLerp(splines[currentSpline].a, splines[currentSpline].aa, splines[currentSpline].bb, splines[currentSpline].b, currentStep / splines[currentSpline].Length);
        Vector3 end = Math3d.CubicBezierLerp(splines[currentSpline].a, splines[currentSpline].aa, splines[currentSpline].bb, splines[currentSpline].b, (currentStep + splines[currentSpline]._step) / splines[currentSpline].Length);
        Vector3 Dir = start - end;
        Dir.Normalize();

        Vector3 Left = 10.0f * Vector3.Cross(Dir, Vector3.up) + Point;
        Vector3 Right = -10.0f * Vector3.Cross(Dir, Vector3.up) + Point;

        Math3d.CrossPointLine(start, end, Left, Right, out result);

        float dist1 = Vector2.Distance(new Vector2(start.z, start.x), new Vector2(end.z, end.x));
        float dist2 = Vector2.Distance(new Vector2(start.z, start.x), new Vector2(result.z, result.x));

        float step = dist2 / dist1;

        result.y = Mathf.Lerp(start.y, end.y, step);
        return currentTransform.TransformPoint(result);
    }

    public override bool UniformGlobalPosAdd(float t, float dist, out Vector3 point, out float addPercent, out float _dist, bool direction)
    {
        if (!isCalculated)
            CalculateLenght();

        point = Vector3.zero;
        addPercent = 0;
        _dist = 0;

        int select;
        float percent;

        GetLocalSpline(t, out select, out percent);

        if (splines[select].UniformGlobalPosAdd(t, dist, out point, out addPercent, out _dist, direction))
            return true;
        else
        if (splines.Length - 1 > select && direction || select > 0 && !direction)
        {
            splines[select + ((direction) ? 1 : 0)].UniformGlobalPosAdd(t, dist, out point, out addPercent, out _dist, direction);
            return true;
        }
        else
            return false;
    }

}
