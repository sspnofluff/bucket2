﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public enum Side
{
    Top,
    Left,
    Rigth,
    Back,
}

[System.Serializable]
public class GarbagePart
{
    public bool isUsed;
    public Side side;
    private int _partsCount;

    public Vector3 lockPosition;
    public Quaternion lockRotation;

    public Transform GarbagePanel;
    public Renderer GarbageRenderer;

    public Rigidbody[] GarbageParts;
    public Vector3[] partsPositions;
    public Quaternion[] partsRotations;

    public void Create()
    {
        _partsCount = GarbageParts.Length;
        lockPosition = GarbagePanel.localPosition;
        lockRotation = GarbagePanel.localRotation;

        partsPositions = new Vector3[_partsCount];
        partsRotations = new Quaternion[_partsCount];

        for (int i = 0; i < _partsCount; i++)
        {
            partsPositions[i] = GarbageParts[i].transform.localPosition;
            partsRotations[i] = GarbageParts[i].transform.localRotation;
        }
    }

    public void Prepare(Material material)
    {
        GarbageRenderer.sharedMaterial = material;
     }

    public void Revert(Enemy enemy)
    {
        isUsed = false;
        GarbagePanel.parent = enemy.currentTransform;
        GarbagePanel.localPosition = lockPosition;
        GarbagePanel.localRotation = lockRotation;

        for (int i=0; i<_partsCount; i++)
        {
            GarbageParts[i].velocity = Vector3.zero;
            GarbageParts[i].angularVelocity = Vector3.zero;
            GarbageParts[i].transform.localPosition = partsPositions[i];
            GarbageParts[i].transform.localRotation = partsRotations[i];
        }

        GarbagePanel.gameObject.SetActive(false);
    
    }
}


[System.Serializable]
public enum Status
{
    Default,
    Follow,
    Damaged,
    StopForever,
    Impact,
}



public class Enemy : Unit
{
    public bool boost = true;
    public bool deformEnable;
    public bool garbageEnable;
    public bool avoidance = true;

    public bool activeInCamera = true;
    public float clamp;

    
    public Status status = Status.Default;

    public WheelCollider[] wheelsColliderForward;
    public WheelCollider[] wheelsColliderBack;

    //public float rpm;

    //public Vector3 startRotation;
    //public Vector3 startDiff;

    public float solidOffset;
    public Vector3[] colliderPos;

    public EnemyType type;

    private bool isTextDropping = false;
    private float textDropTimer = 0.0f;
    private Vector3 TextPos = Vector3.zero;
    private string droppingText;

    float deformRadius = 0.25f; //0.25
    float deformNoise = 0.02f; //0.02
    float maxDeform = 0.125f; //0.125
    float steerAngleMax = 45.0f;

    public float accelDirectionOffset;

    //private int roadSideChecker;
    protected Vector3 roadDirection;

    private float killTimerMax = 1.0f;
    private float killTimer = 0.0f;

    Vector3[] oldVertices;

    public bool stableZ;
    public bool LineCounted;
    public int Line;
    int solidMax = 0;
    public Material[] randomMaterial;

    public Mesh _DeformMesh;
    public Mesh _DeformationMesh;
    public Transform _DeformMeshTransform;

    //public bool selfDestroy = false;
    
    public string _name;
    public bool direction;
    public float steerSpeed;

    public bool _isDefault = true;
    public bool _isStop = false;
    public bool _isCriticalStop = false;
    public bool _isFollow = false;

    private bool _isCheckImpact = false;
 
    private float _impactTimer;
    private float _impactAngle;
    private float _checkImpactTimer;
    private float _antiImpactTimer = -1.0f;

    public float _impactTimerMax;
    public float _checkImpactTimerMax;

    float inputSteer = 0.0f;
    float inputTorque = 1.0f;

    public float maxAccel;
    public float currentAccel;
    public float startPower;

    public float maxSpeed; //1
    public float averageSpeed;
    public float speedOffset;

    public float brake;
    public float brakeOffset;

    public float criticalBrake;
    public float criticalOffset;

    public int stopTimer = 0;
    public float hp;

    public bool trafficUnit = false;

    public Object explosion;

    Vector3 RelativeWaypointPosition;

    //public Bounds AvoidanceRect;
    //public bool AvoidanceOcclusion;

    public Vector3 _frontPos;
    public Vector3 _leftPos;
    public Vector3 _rightPos;
    public Vector3 _leftSidePos;
    public Vector3 _rightSidePos;

    //private Vector3 lerpDirecton;
    //private Vector3 leftDirection;
    //private Vector3 rightDirection;

    public float currentSpeed;
    public float currentBrake;
    public float currentCritical;

    public Vector3 BaseTransform;
    public Transform centerOfMass;
   // Vector3 pointRotation;
    public Vector3 Waypoint;

    //Vector3 CurvePoint;

    public float offset;

    //private float sensorLength = 6.0f;

    public MeshFilter meshFilter;
    public Enemy followCar = null;
    public Enemy followLink = null;
    public CarAntiRollBar[] Arb;

    public GarbagePart[] garbagePart;
   

    public override void Initialize(bool activeObject)
    {
        base.Initialize(activeObject);

        CreateObject();

        if (activeObject)
        PrepareObject();
    }

    public virtual void CreateObject()
    {
        currentRigidbody.centerOfMass = centerOfMass.localPosition;

        Arb = GetComponents<CarAntiRollBar>();

        if (deformEnable)
        DeformationMeshActivate();

        SetupColliders();

        if (garbageEnable)
        for (int i=0; i<4; i++)
        garbagePart[i].Create();

    }

    public virtual void PrepareObject()
    {
        hp = 500;

        Active(true);

        if (randomMaterial.Length > 0)
        {
            int randomMaterialIndex = Random.Range(0, randomMaterial.Length);
            currentRenderer.sharedMaterial = randomMaterial[randomMaterialIndex];
            if (garbageEnable)
                for (int i = 0; i < 4; i++)
                    garbagePart[i].Prepare(randomMaterial[randomMaterialIndex]);
        }

        currentSpeed = maxSpeed + Random.Range(-speedOffset, speedOffset);
        currentBrake = brake + Random.Range(-brakeOffset, brakeOffset);
        currentCritical = criticalBrake + Random.Range(-criticalOffset, criticalOffset);
        currentAccel = maxAccel;

        if (spline = CamMove.GetSpline(currentTransform.position, trafficUnit))
        {
            percent = spline.GetStep(currentTransform.position);
            roadDirection = spline.GetAngle(percent);
            if (Vector3.Angle(currentTransform.forward, roadDirection) < 90.0f)
                direction = true;
        }
        else
        {
            Debug.LogError("Enemy: Prepare Object can't find Spline " + currentGameObject.GetInstanceID() + currentGameObject.name);
            Kill();
            return;
        }
        
        if (!trafficUnit)
        {
            Vector3 startPos = spline.GlobalPos(percent);
            offset = Vector3.Distance(startPos, currentTransform.position);
            if ((Vector3.Angle(Vector3.Cross(roadDirection, Vector3.up), Vector3.Normalize(startPos - currentTransform.position))) > 90.0f)
                offset *= -1;
        }

        BaseTransform = currentTransform.position;
        CreateNewWaypoint(10.0f, false);

        if (!direction)
           currentAccel += accelDirectionOffset;
        
    }

    public virtual void RevertObject()
    {
        currentRigidbody.velocity = Vector3.zero;
        currentRigidbody.angularVelocity = Vector3.zero;

        _isStop = false;
        _isCriticalStop = false;
        stopTimer = 0;
        status = Status.Default;
        boost = true;
        activeInCamera = true;

        if (garbageEnable)
            RevertGarbage();

        if (deformEnable)
            DeformationRevert();
    }

    void CreateGarbage(Side side)
    {

        for (int i = 0; i < garbagePart.Length; i++)
            if (garbagePart[i].side == side && !garbagePart[i].isUsed)
            {
                Debug.Log("CreateGarbage " + side.ToString());
                garbagePart[i].GarbagePanel.gameObject.SetActive(true);
                garbagePart[i].GarbagePanel.parent = null;
                for (int j = 0; j < garbagePart[i].GarbageParts.Length; j++)
                    garbagePart[i].GarbageParts[j].AddForce(Random.RandomRange(-8.0f, 8.0f), Random.RandomRange(3.0f, 8.0f), Random.RandomRange(-8.0f, 8.0f), ForceMode.Impulse);
                garbagePart[i].isUsed = true;
            }
    }

    public void RevertGarbage()
    {
        for (int i = 0; i < 4; i++)
            garbagePart[i].Revert(this);
    }


    public virtual void Active(bool active)
    {

        if (active == activeInCamera)
            return;

        activeInCamera = active;
    }
        //currentRenderer.enabled = active;
        //if (active)
        //currentRigidbody.WakeUp();
        //else
        //currentRigidbody.Sleep();
        //Arb[0].enabled = active;
        //Arb[1].enabled = active;

    




    protected void CreateNewWaypoint(float wayLength, bool needNewSpline)
    {
        if (!direction)
            wayLength *= -1;
        
        Vector3 curveposition = currentTransform.position;
        float passed = 0.0f;
        float addPercent = 0.0f;

        if (needNewSpline)
        {
            if (spline = CamMove.GetSpline(currentTransform.position, trafficUnit))
            {
                percent = spline.GetStep(currentTransform.position);
                roadDirection = spline.GetAngle(percent);
                if (Vector3.Angle(currentTransform.forward, roadDirection) < 90.0f)
                    direction = true;
            }
            else
            {
                Debug.Log("No start spline (NNSpline");
                Kill();
                return;
            }

           
        }

        if (spline.UniformGlobalPosAdd(percent, wayLength, out curveposition, out addPercent, out passed, direction))
        {
            //log += " firstAdd " + percent + " "+ wayLength + " " + curveposition + " " + addPercent + " " + passed + "\n";
            //Debug.Log(" True" + spline.transform.gameObject.name);
        }
        else
        {
            int blockNumber = spline.transform.GetComponent<Plane>().number;
            Transform current = _Main.GetBlock(blockNumber + (direction ? 1 : -1));

            if (current != null)
            {
                if (trafficUnit)
                   spline = current.GetComponent<Plane>().enemySpline;
                else
                   spline = current.GetComponent<Plane>().spline;
            }
            else
            {
                Debug.Log("No block waypoint");
                Kill();
                return;
            }

            float temp;
            if (spline != null)
            {
                if (!spline.UniformGlobalPosAdd(direction ? 0 : 1.0f, wayLength - passed, out curveposition, out addPercent, out temp, direction))
                {
                    //fff
                }
                
            }
            else
            {
                Debug.Log("No spline waypoint");
                Kill();
                return;
            }
           
        }

        roadDirection = spline.GetAngle(addPercent);
        float angle = Math3d.GetAngle(roadDirection.x, roadDirection.z, Vector3.forward.x, Vector3.forward.z);

        BaseTransform = curveposition;
        Waypoint = Math3d.RotateThis(offset, -angle, curveposition);
        percent = addPercent;
    }


    public override void Kill()
    {
        if (LineCounted)
            _MainCamera._trafficController.trafficLines[Line].UnitLimit--;

        if (followLink != null)
            followLink.CancelFollow();

        RevertObject();

        _Main.PoolSystem.Citizens.DestroyObjectPool(gameObject);
       
        return;
    }
        
    int CriticalBrake(Vector3 pos, Vector3 dir, float lenght)
    {
        pos = currentTransform.TransformPoint(pos);
        int layerMask = 1;
        
        RaycastHit hit;
        if (Physics.Raycast(pos, dir, out hit, lenght))
        {
            Debug.DrawLine(pos, hit.point, Color.white);
            Debug.DrawLine(hit.point, hit.normal * 1.0f + hit.point, Color.red);
            Transform target = hit.collider.transform;
            if (target.CompareTag("Unit"))
            {
                Unit unit = target.parent.GetComponent<Unit>();
                if (unit.unitType == UnitType.Solid || unit.unitType == UnitType.SolidWall)
                {
                    DropText("Critical Brake");
                    if (hit.distance < lenght / 3 && currentRigidbody.velocity.magnitude>4.0f)
                        return 1;
                }

                if (unit.unitType == UnitType.Hero || unit.unitType == UnitType.Enemy)
                {
                    float angle = Vector3.Angle(unit.currentTransform.forward, currentTransform.forward);

                    if (angle > 90)
                        return 1;
                    else
                        if (hit.distance < lenght / 3)
                            return 2;
                        else
                        {
                            if (status == Status.Default && unit.unitType == UnitType.Enemy)
                            {
                                if (!SetFollow(unit.currentTransform.GetComponent<Enemy>()))
                                return 1;
                            }

                        }

                }

            }

            
        }
        else
        Debug.DrawLine(pos, pos + lenght * dir, Color.white);
        return 0;
    }

    bool SetFollow(Enemy EnemyFollowObject)
    {
        if (EnemyFollowObject.type == type)
        {
            if (EnemyFollowObject.followLink == null)
            {
                status = Status.Follow;
                EnemyFollowObject.followLink = this;
                followCar = EnemyFollowObject;
                return true;
            }
            else
                return SetFollow(EnemyFollowObject.followLink);
        }
        return false;
    }

    Vector3 Avoidance(Vector3 pos, Vector3 dir, float lenght, float forwardLenght, float force, float forwardForce, bool distAmorf, out int brake, out int solid, out int Id)
    {
        solid = 0;
        Id = 0;
        brake = 0;
        pos = currentTransform.TransformPoint(pos);
        int layerMask = 1;
        RaycastHit hit;
        if (Physics.Raycast(pos, dir, out hit, lenght, layerMask))
        {
            Transform target = hit.collider.transform;
            if (target.CompareTag("Unit"))
            {
                Unit unit = target.parent.GetComponent<Unit>();
                if (unit.unitType == UnitType.Hero || unit.unitType == UnitType.Enemy)
                {
                    //Rigidbody EnemyRigidbody = TargetTransform.parent.rigidbody;
                    Id = unit.GetInstanceID();

                    float angle = Vector3.Angle(unit.currentTransform.forward, currentTransform.forward);

                    

                    if (angle < 90 && !distAmorf)
                    {
                        force = forwardForce;
                        lenght = forwardLenght;

                        if (hit.distance < lenght / 2)
                        {
                            brake = 1;
                        }
                    }
                    Debug.DrawLine(pos, hit.point, Color.white);
                    Debug.DrawLine(hit.point, hit.normal * 1.0f + hit.point, Color.red);

                    return currentTransform.right * hit.normal.x * force * lenght / hit.distance;

                }
                else
                    if (unit.unitType == UnitType.Solid)
                {
                    solid = 1;
                    Id = unit.GetInstanceID();

                    //if (!direction)
                    //   force *= -1;

                    Debug.DrawLine(pos, hit.point, Color.white);
                    Debug.DrawLine(hit.point, hit.normal * 1.0f + hit.point, Color.red);
                    //Debug.Log(hit.normal);
                    if (distAmorf)
                        return Vector3.zero;
                    //return Vector3.right * hit.normal.x * force * lenght / hit.distance;
                    //Vector3 finish = Vector3.right * hit.normal.x * force * lenght / hit.distance;
                    //Vector3 finish = Vector3.right * hit.normal.x * force * lenght / hit.distance;
                    //Vector3 finish = currentTransform.forward - (Vector3.Dot(currentTransform.forward, hit.normal)) * hit.normal * force * lenght / hit.distance;
                    //float angle = Math3d.GetAngle(dir.z, dir.x, hit.normal.z * -1, hit.normal.x * -1);

                    //Vector3 finish = Vector3.Cross(dir,Vector3.up) * force * lenght / hit.distance;
                    Vector3 finish = Vector3.right * solidOffset;
                    //DropText(hit.point,  + " " + finish.ToString("F2") + " " + (force * lenght / hit.distance).ToString("F2"), 1.0f);
                    return finish;
                }
                    else
                    if (unit.unitType == UnitType.SolidWall)
                {
                    solid = 1;
                    Id = unit.GetInstanceID();

                    force = -solidOffset;
                    float angle = Vector3.Angle(unit.currentTransform.forward, currentTransform.forward);

                    if (distAmorf)
                        return Vector3.zero;

                    if (angle < 90)
                        force *= -1;

                    Debug.DrawLine(pos, hit.point, Color.white);
                    Debug.DrawLine(hit.point, hit.normal * 1.0f + hit.point, Color.red);
                    //Debug.Log(select + " " + angle + " " + hit.normal + " " + (lenght / hit.distance).ToString("F2") + " " + (Vector3.right * hit.normal.x * force * lenght / hit.distance).ToString("F2"));
                    //return Vector3.right * hit.normal.x * force * lenght / hit.distance;
                    //Debug.Log(Vector3.Dot(myTransform.forward, hit.normal) + " " + myTransform.forward);
                    Vector3 finish = Vector3.right * force;

                    //Vector3 finish = currentTransform.forward - (Vector3.Dot(currentTransform.forward, hit.normal)) * hit.normal * force * lenght / hit.distance;
                    //Debug.Log(finish);
                    return finish;

                }
            }   
        }
        else
        Debug.DrawLine(pos, pos + lenght * dir, Color.white);
        return Vector3.zero;
    }

    bool CheckRaycastImpact(Vector3 pos, Vector3 dir, float lenght)
    {
        pos = currentTransform.TransformPoint(pos);
        int layerMask = 1;
        RaycastHit hit;
        if (Physics.Raycast(pos, dir, out hit, lenght, layerMask))
        {
            Transform target = hit.collider.transform;
            if (target.CompareTag("Unit"))
            {
                return false;
            }
        }
        return true;
        
    }

    bool CheckFrontImpact()
    {
        Vector3 frontDir = currentTransform.forward;

        if (!stableZ)
        {
            RaycastHit hit;
            if (Math3d.RayCastGroundDown(currentTransform.TransformPoint(_frontPos) + currentTransform.forward * 2.0f + Vector3.up * 3.0f, out hit))
                frontDir = Vector3.Normalize((hit.point + Vector3.up * 0.2f) - currentTransform.TransformPoint(_frontPos));
        }
         
        //else
        // DropText(currentTransform.position, "NO RAYCAST", 1.0f);
        if (CheckRaycastImpact(_frontPos, frontDir, 2.0f) && CheckRaycastImpact(_leftPos, frontDir, 2.0f) && CheckRaycastImpact(_rightPos, frontDir, 2.0f))
            return true;

        return false;
        
    }

    public virtual void Boost()
    {
        currentRigidbody.AddForce(currentTransform.forward * startPower, ForceMode.VelocityChange);
        boost = false;
        
    }
    

    public virtual void Moving()
    {
            //DropText(currentTransform.position, currentRigidbody.velocity.magnitude.ToString("F2"), 1.0f);

            if (boost) Boost();

            if (currentRigidbody.velocity.magnitude < 1.0f && status == Status.Default && _antiImpactTimer<0)
            {
                if (!_isCheckImpact)
                {
                    DropText("Check Impact");
                    _isCheckImpact = true;
                    _checkImpactTimer = _checkImpactTimerMax;
                }
                _checkImpactTimer -= Time.deltaTime;

                if (_isCheckImpact && _checkImpactTimer<0)
                {
                    _isCheckImpact = false;
                    if (!CheckFrontImpact())
                    {
                        DropText("Status - Impact");
                        _impactTimer = 1.4f;
                        status = Status.Impact;
                    }
                    else
                    {
                        _antiImpactTimer = _impactTimerMax * 3;
                        DropText("Clean, Reset to Default");
                    }
                    //DropText(currentTransform.position, "Impact", 1.0f);

                }
            }
                            

            if (status == Status.Impact)
            {
                _impactTimer -= Time.deltaTime;
                if (_impactTimer < 0)
                {
                    status = Status.Default;
                    _antiImpactTimer = _impactTimerMax*3;
                    CreateNewWaypoint(10.0f, true);
                }
            }

            if (_antiImpactTimer > 0)
                _antiImpactTimer -= Time.deltaTime;

            inputSteer = 0.0f;
            inputTorque = 1.0f;

            //status = Status.Default;
            switch (status)
            {
                /////DEFAULT
                case Status.Default:
                    {
                        averageSpeed = currentSpeed;
                        MovementFunctions(Waypoint, true, true, false);
                    }

                    break;
                /////BRAKE

                case Status.Damaged:
                    {
                        MovementFunctions(Waypoint, false, false, false);
                       
                    }
                    break;
                /////CRITICAL BRAKE
                case Status.StopForever:
                    {
                        foreach (WheelCollider collider in wheelsColliderBack)
                        {
                            collider.motorTorque = 0;
                            collider.brakeTorque = currentBrake/1.4f;
                        }
                    }
                    break;

                case Status.Follow:
                    {
                        //DropText(currentTransform.position, "Follow", 1.0f);
                        MovementFunctions(followCar.currentTransform.position, false, true, true);
                        
                    }
                    break;

                case Status.Impact:
                    {
                        foreach (WheelCollider collider in wheelsColliderForward)
                        {
                           collider.steerAngle = 0;

                        }
                        
                            foreach (WheelCollider collider in wheelsColliderBack)
                            {
                                if (currentRigidbody.velocity.magnitude < 6.0f)
                                {
                                    collider.motorTorque = -currentAccel * 1.4f;
                                    collider.brakeTorque = 0;
                                }
                                else
                                {
                                    collider.motorTorque = 0;
                                    collider.brakeTorque = currentBrake;
                                }
                                                      
                            }


                    }
                    break;
            }
        }
    

    public void CancelFollow()
    {
        DropText("Cancel Follow");
        status = Status.Default;
        if (followCar != null)
        {
            followCar.followLink = null;
            followCar = null;
        }
        CreateNewWaypoint(10.0f, true);
    }

    void MovementFunctions(Vector3 targetPosition, bool _avoidance, bool _continuance, bool _follow)
    {

     int frontBrakeSensor = 0;
     int setBrake = 0;
     int getBrake = 0;
     int brakeStrength = 0;
     int solid = 0;
     
     
     RelativeWaypointPosition = currentTransform.InverseTransformPoint(targetPosition);

     if (_avoidance)
     {
         Vector3 frontDir = currentTransform.forward;
         RaycastHit hit;

         int[] type = new int[6];

         if (!stableZ)
             if (Math3d.RayCastGroundDown(currentTransform.TransformPoint(_frontPos) + currentTransform.forward * 4.0f + Vector3.up * 3.0f, out hit))
                 frontDir = Vector3.Normalize((hit.point + Vector3.up * 0.2f) - currentTransform.TransformPoint(_frontPos));
          
         //else
            // DropText(currentTransform.position, "NO RAYCAST", 1.0f);

         if (wheelsColliderBack[0].isGrounded && wheelsColliderBack[1].isGrounded)
         {
             frontBrakeSensor = CriticalBrake(_frontPos, frontDir, 6.0f);

             RelativeWaypointPosition += Avoidance(_leftPos, frontDir, 15.0f, 2.0f, 25.0f, 4.0f, false, out getBrake, out solid, out type[0]); setBrake += getBrake; solidMax += solid;
             RelativeWaypointPosition += Avoidance(_rightPos, frontDir, 15.0f, 2.0f, 25.0f, 4.0f, false, out getBrake, out solid, out type[1]); setBrake += getBrake; solidMax += solid;
             RelativeWaypointPosition += Avoidance(_leftPos, Quaternion.Euler(0.0f, -10.0f, 0.0f) * frontDir, 11.0f, 2.0f, 20.0f, 3.0f, false, out getBrake, out solid, out type[2]); setBrake += getBrake; solidMax += solid;
             RelativeWaypointPosition += Avoidance(_rightPos, Quaternion.Euler(0.0f, 10.0f, 0.0f) * frontDir, 11.0f, 2.0f, 20.0f, 3.0f, false, out getBrake, out solid, out type[3]); setBrake += getBrake; solidMax += solid;
             RelativeWaypointPosition += Avoidance(_leftSidePos, Quaternion.Euler(0.0f, -90.0f, 0.0f) * transform.forward, 2.0f, 2.0f, 1.25f, 1.25f, true, out getBrake, out solid, out type[4]); setBrake += getBrake; solidMax += solid;
             RelativeWaypointPosition += Avoidance(_rightSidePos, Quaternion.Euler(0.0f, 90.0f, 0.0f) * transform.forward, 2.0f, 2.0f, 1.25f, 1.25f, true, out getBrake, out solid, out type[5]); setBrake += getBrake; solidMax += solid;
         }
         else
             DropText("NO WHEELS");


         List<int> tempID = new List<int>();


         for (int i = 0; i < 6; i++)
         {
             bool compare = true;
             for (int j = 0; j < tempID.Count; j++)
             {
                 if (type[i] == tempID[j])
                 {
                     compare = false;
                     break;
                 }
             }
             if (compare)
                 tempID.Add(type[i]);
             
         }
         int complexBrakes = tempID.Count - 2;
         if (complexBrakes > 0)
         {
             DropText("complex brake " + complexBrakes + " grade");
             brakeStrength += complexBrakes;
         }
         solidMax = 0;
         
     }
     
       inputSteer = RelativeWaypointPosition.x / RelativeWaypointPosition.magnitude;
                        
                        Debug.DrawLine(currentTransform.position, Waypoint, Color.magenta);
                        Debug.DrawLine(BaseTransform, Waypoint, Color.blue);
                        Debug.DrawLine(currentTransform.position, currentTransform.TransformPoint(RelativeWaypointPosition), Color.black);
                        Debug.DrawLine(currentTransform.TransformPoint(RelativeWaypointPosition), currentTransform.TransformPoint(RelativeWaypointPosition) + 1.5f * Vector3.up, Color.black);
                                                
       if (Mathf.Abs(inputSteer) < 0.5f)
           inputTorque = RelativeWaypointPosition.z / RelativeWaypointPosition.magnitude - Mathf.Abs(inputSteer);
       else
           inputTorque = 0.0f;
       
           
        
       if (RelativeWaypointPosition.magnitude < 8.0f && !_follow)
       {
               if (_continuance)
                   CreateNewWaypoint(10.0f, false);
               else
               {
                   DropText("Status.StopForever");
                   status = Status.StopForever;
                   return;
               }
       }
       if (_follow)
       {
           float angle = Vector3.Angle(Vector3.forward, Vector3.Normalize(RelativeWaypointPosition));
          // 

           if (angle > 150 || RelativeWaypointPosition.magnitude > 8.0f || followCar.rigidbody.velocity.magnitude < 2.0f)
           {
               CancelFollow();
               return;
           }




           if (RelativeWaypointPosition.magnitude > 5.0f && wheelsColliderBack[0].rpm < averageSpeed - 35.0f)
               {
                   wheelsColliderBack[0].motorTorque = currentAccel * inputTorque;
                   wheelsColliderBack[1].motorTorque = currentAccel * inputTorque;
                   wheelsColliderBack[0].brakeTorque = 0;
                   wheelsColliderBack[1].brakeTorque = 0;
                   
               }
               else
               if (RelativeWaypointPosition.magnitude < 4.0f)
                   {
                       if (followCar.averageSpeed < averageSpeed)
                           CancelFollow();
                       wheelsColliderBack[0].motorTorque = 0;
                       wheelsColliderBack[1].motorTorque = 0;
                       wheelsColliderBack[0].brakeTorque = currentBrake;
                       wheelsColliderBack[1].brakeTorque = currentBrake;
                       
                   }
                   else
                   {
                       wheelsColliderBack[0].motorTorque = 0;
                       wheelsColliderBack[1].motorTorque = 0;
                       wheelsColliderBack[0].brakeTorque = 0;
                       wheelsColliderBack[1].brakeTorque = 0;
                      
                   }
           wheelsColliderForward[0].steerAngle = inputSteer * steerSpeed;
           wheelsColliderForward[1].steerAngle = inputSteer * steerSpeed;
       }
       else
       {


           foreach (WheelCollider collider in wheelsColliderBack)
           {
               if (frontBrakeSensor>0)
               {
                   collider.motorTorque = 0;
                   collider.brakeTorque = (frontBrakeSensor==1) ? currentCritical : currentBrake;

               }
               else
                   if (brakeStrength > 0 || setBrake > 0)
                   {
                       collider.motorTorque = 0;
                       collider.brakeTorque = currentBrake + brakeStrength * 100.0f + setBrake * 100.0f;
                       //DropText(currentTransform.position, currentBrake + " " + brakeStrength * 100.0f + " " + setBrake * 100.0f, 1.0f);
                   }
                   else
                   {
                       if ((collider.rpm > averageSpeed + 35.0f))
                       {
                           collider.motorTorque = 0;
                           collider.brakeTorque = currentBrake;
                       }
                       else
                           if (collider.rpm < averageSpeed - 35.0f)
                           {
                               collider.motorTorque = currentAccel * inputTorque;
                               collider.brakeTorque = 0;
                           }
                           else
                           {
                               collider.motorTorque = 0;
                               collider.brakeTorque = 0;
                           }
                   }

           }

           foreach (WheelCollider collider in wheelsColliderForward)
               collider.steerAngle = inputSteer * steerSpeed;
       }
    }
    /*
    void OnGUI()
    {
        if (isTextDropping)
        {
            Vector3 Pos = _MainCamera._cam.WorldToScreenPoint(TextPos);
            GUI.Label(new Rect((int)Pos.x, Screen.height - (int)Pos.y, 150, 130), droppingText);


            textDropTimer -= Time.deltaTime;
            if (textDropTimer < 0.0f)
            {
                isTextDropping = false;
            }
        }
    }
    */
    void DropText(string text)
    {
        isTextDropping = true;
        TextPos = currentTransform.position;
        droppingText = text;
        textDropTimer = 1.0f;
    }

    void Update()
    {
        /*
        if (AvoidanceOcclusion)
        {
            DropText(currentTransform.position, "Enable", 1.0f);
            AvoidanceOcclusion=false;
        }
        else
        {
            DropText(currentTransform.position, "Disable", 1.0f);
        }
        
        Debug.DrawLine(currentTransform.TransformPoint(new Vector3(2.5f, currentTransform.position.y, 16.0f)), currentTransform.TransformPoint(new Vector3(-2.5f, currentTransform.position.y, 16.0f)), Color.green);
        Debug.DrawLine(currentTransform.TransformPoint(new Vector3(2.5f, currentTransform.position.y, 16.0f)), currentTransform.TransformPoint(new Vector3(2.5f, currentTransform.position.y, -1.0f)), Color.green);
        Debug.DrawLine(currentTransform.TransformPoint(new Vector3(-2.5f, currentTransform.position.y, 16.0f)), currentTransform.TransformPoint(new Vector3(-2.5f, currentTransform.position.y, -1.0f)), Color.green);
        Debug.DrawLine(currentTransform.TransformPoint(new Vector3(2.5f, currentTransform.position.y, -1.0f)), currentTransform.TransformPoint(new Vector3(-2.5f, currentTransform.position.y, -1.0f)), Color.green);
        */
        if (_Main._MainCamera.Frustrum(currentTransform.position, clamp))
        {
            Active(true);
        }
        else
        {
            Active(false);
        }
        
        if (killTimer > 0)
        {
            killTimer -= Time.deltaTime;
        }
        else
        {
            killTimer = killTimerMax;
            if (isTooFar(25.0f, 5.0f))
            {
                
                //Debug.Log("Too far");
                Kill();

            }
        }


        
    }

    void OnCollisionEnter(Collision collision)
    {
        CollisionWithUnit(collision);

    }

    void DamageWheels(Vector3 point)
    {
        Vector3 damageVector = currentTransform.InverseTransformPoint(point);
        if (damageVector.y > 0.2f)
        {
            if (damageVector.x > 0.2f)
            {
                //DropText(currentTransform.position, "damaged top left wheel", 1.0f);
                wheelsColliderForward[0].mass += Random.Range(-wheelsColliderForward[0].mass, wheelsColliderForward[0].mass);
            }
            else if (damageVector.x < 0.2f)
            {
                //DropText(currentTransform.position, "damaged top right wheel", 1.0f);
                wheelsColliderForward[1].mass += Random.Range(-wheelsColliderForward[1].mass, wheelsColliderForward[1].mass);
            }
        }
        else if (damageVector.y < 0.2f)
        {
            if (damageVector.x > 0.2f)
            {
                //DropText(currentTransform.position, "damaged back left wheel", 1.0f);
                wheelsColliderBack[0].mass += Random.Range(-wheelsColliderBack[0].mass, wheelsColliderBack[0].mass);
            }
            else if (damageVector.x < 0.2f)
            {
                //DropText(currentTransform.position, "damaged back right wheel", 1.0f);
                wheelsColliderBack[1].mass += Random.Range(-wheelsColliderBack[1].mass, wheelsColliderBack[1].mass);
            }
        }
    }

    void DamageHull(Vector3 point)
    {
        Vector3 damageVector = currentTransform.InverseTransformPoint(point);
        if (Mathf.Abs(damageVector.z) > Mathf.Abs(damageVector.x))
        {
            if (damageVector.z > 0)
            {
                CreateGarbage(Side.Top);
            }
            else
            {
                CreateGarbage(Side.Back);
            }
        }
        else
        {
        if (damageVector.x > 0)
            {
                CreateGarbage(Side.Rigth);
            }
            else
            {
                CreateGarbage(Side.Left);
            }
        }
    }

    void DamageHit(float damage, Vector3 point)
    {
        if (hp - damage > 0)
            hp -= damage;

        switch (status)
        {
            case Status.Default:
                if (hp < 65.0f && hp > 30.0f)
                {
                    DamageWheels(point);
                    status = Status.Damaged;
                }
                else
                if (hp<30.0f)
                {
                    DamageWheels(point);
                    status = Status.StopForever;
                }
            break;
                case Status.Damaged:
                if (hp < 30)
                {
                    DamageWheels(point);
                    status = Status.StopForever;
                }
            break;
        }

        

                
    }


    public virtual void CollisionWithUnit(Collision collision)
    {
        
        //Debug.Log(collision.gameObject.tag);
        //Collider target = collision.collider;
        //Debug.Log(target.gameObject.tag);
        if (collision.gameObject.CompareTag("Unit"))
        {
            //Debug.Log("Collision!");
            Unit unit = collision.transform.GetComponent<Unit>();
            //if (unit == null)
                //Debug.Log(target.gameObject.name);
            if (unit.unitType == UnitType.Enemy || unit.unitType == UnitType.Hero)
            {
                foreach (ContactPoint contact in collision.contacts)
                {
                    //Debug.Log(collision.relativeVelocity.magnitude);
                    if (collision.relativeVelocity.magnitude > 5.0f)
                    {
                        if (deformEnable)
                        DeformMesh(_DeformationMesh, oldVertices, currentTransform, contact.point, collision.relativeVelocity);
                        DamageHit(collision.relativeVelocity.magnitude, contact.point);
                        if (garbageEnable)
                        DamageHull(contact.point);
                    }
                }
                
            }
            //Debug.Log(collision.relativeVelocity.magnitude);      
        }
        /*
        if (target.CompareTag("BounceModule"))
        {
            //rigidbody.AddForce(collision.relativeVelocity*3000.0f, ForceMode.Force);
        }
        */
    }

    public void DeformMesh(Mesh mesh, Vector3[] originalMesh, Transform localTransform, Vector3 contactPoint, Vector3 contactForce)
    {
        Vector3[] vertices = mesh.vertices;
        //Vector3[] currentVertices = vertices;
        float sqrRadius = deformRadius * deformRadius;
        float sqrMaxDeform = maxDeform * maxDeform;

        Vector3 localContactPoint = localTransform.InverseTransformPoint(contactPoint);
        Vector3 localContactForce = localTransform.InverseTransformDirection(contactForce);

        //localContactForce *= 6;

        for (int i = 0; i < vertices.Length; i++)
        {
            float dist = (localContactPoint - vertices[i]).sqrMagnitude;

            if (dist < sqrRadius)
            {
                vertices[i] += (localContactForce * (deformRadius - Mathf.Sqrt(dist)) / deformRadius) + Random.onUnitSphere * deformNoise;

                Vector3 deform = vertices[i] - originalMesh[i];

                if (deform.sqrMagnitude > sqrMaxDeform)
                    vertices[i] = originalMesh[i] + deform.normalized * maxDeform;
            }
        }

        originalMesh = vertices;
        mesh.vertices = vertices;
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
    }



    protected string RandomString(int count, string root)
    {
        int _rand;
        string result;
        _rand = Random.Range(1, count);
        if (_rand == 1)
            result = root;
        else
            if (_rand < 10)
                result = root + "0" + _rand.ToString();
            else
                result = root + _rand.ToString();
        return result;

    }

    public virtual void onHit(int damage)
    {
        if (hp - damage > 0)
            hp -= damage;
        else
            SelfDestroy();
    }

    void SelfDestroy()
    {
        /*
        selfDestroy = true;
        if (trafficUnit && LineCounted)
        {
            _MainCamera._trafficController.trafficLines[Line].UnitLimit--;
        }
        GameObject expl = Instantiate(explosion, transform.position, transform.rotation) as GameObject;
        expl.transform.parent = transform;
         */
    }


    public void SetupColliders()
    {
        colliderPos = new Vector3[4];
        for (int i = 0; i < 2; i++)
            colliderPos[i] = wheelsColliderForward[i].transform.position;
        for (int i = 0; i < 2; i++)
            colliderPos[i + 2] = wheelsColliderBack[i].transform.position;
    }


    protected void DeformationMeshActivate()
    {
        oldVertices = new Vector3[_DeformMesh.vertexCount];
        oldVertices = _DeformMesh.vertices;
        _DeformationMesh = new Mesh();
        _DeformationMesh.name = "DeformationMesh";
        _DeformationMesh.vertices = new Vector3[_DeformMesh.vertexCount];
        _DeformationMesh.vertices = _DeformMesh.vertices;
        _DeformationMesh.SetIndices(_DeformMesh.GetIndices(0), _DeformMesh.GetTopology(0), 0);
        _DeformationMesh.SetTriangles(_DeformMesh.triangles, 0);
        _DeformationMesh.uv = _DeformMesh.uv;
        _DeformationMesh.uv1 = _DeformMesh.uv1;
        _DeformationMesh.normals = _DeformMesh.normals;
        _DeformationMesh.tangents = _DeformMesh.tangents;
        _DeformationMesh.RecalculateBounds();

        meshFilter.sharedMesh = _DeformationMesh;
    }

    public void DeformationRevert()
    {
        _DeformationMesh.vertices = oldVertices;
        _DeformationMesh.RecalculateBounds();
        for (int i=0; i<2; i++)
        {
            wheelsColliderForward[i].mass = 3;
            wheelsColliderBack[i].mass = 3;
        }
    }


    void FixedUpdate()
    {
        
        Moving();


    }

    public bool isTooFar(float _Dist, float _trafficDist)
    {
        Vector3 final = currentTransform.position;
        final -= _MainCamera.thisTransform.position;
        Vector3.Normalize(final);

        float result = Mathf.Acos(Vector3.Dot(final, Math3d.RotateThis(Vector3.forward, -_MainCamera.thisTransform.eulerAngles.y, Vector3.zero)) / (final.magnitude * Vector3.forward.magnitude));
        result *= 180.0f / 3.1415926f;

        if (!trafficUnit)
        {
            
            float Dist = Vector3.Distance(currentTransform.position, _MainCamera.thisTransform.position);
            //DropText(currentTransform.position, result.ToString("F2") + " " + Dist.ToString("F2"), 1.0f);
            if ((Dist > _Dist && result > 90.0f))
                return true;


            //if ((Dist > _Dist && result > 90.0f) || (_Main.Proto.myRigidbody.velocity.magnitude < 2.0f && !activeInCamera))
            //       return true;

            return false;
        }
        else
        {
            
            float Dist = Vector3.Distance(currentTransform.position, _MainCamera._trafficController.FrontIntersectPoint);
            //DropText(currentTransform.position, result.ToString("F2") + " " + Dist.ToString("F2"), 1.0f);
            if (!activeInCamera && result < 90.0f && Dist > _trafficDist)
                return true;

            if (!activeInCamera && result > 90.0f && Vector3.Distance(currentTransform.position, _MainCamera._trafficController.BackIntersectPoint) > _trafficDist)
                return true;


            return false;
        }
    }
    
}

